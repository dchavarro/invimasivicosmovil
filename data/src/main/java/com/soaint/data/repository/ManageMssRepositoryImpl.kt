package com.soaint.data.repository

import com.soaint.data.api.GenericsApi
import com.soaint.data.api.RegisterSanitaryApi
import com.soaint.data.api.SanitaryApi
import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.ManageMssDao
import com.soaint.data.room.dao.VisitasDao
import com.soaint.data.room.tables.AmsspCheckModel
import com.soaint.data.room.tables.RsModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.common.getDateHourNow
import com.soaint.domain.common.getDateHourWithOutFormat
import com.soaint.domain.common.getIp
import com.soaint.domain.model.*
import com.soaint.domain.repository.ManageMssRepository
import com.soaint.domain.repository.UserRepository
import com.soaint.sivicos_dinamico.utils.CDZVE
import com.soaint.sivicos_dinamico.utils.CD_TYPE_PRODUCT
import com.soaint.sivicos_dinamico.utils.CTIPPROD
import com.soaint.sivicos_dinamico.utils.TIPACTE
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class ManageMssRepositoryImpl @Inject constructor(
    private val genericsApi: GenericsApi,
    private val sanitaryApi: SanitaryApi,
    private val registerSanitaryApi: RegisterSanitaryApi,
    private val visitApi: VisitApi,
    private val userRepository: UserRepository,
    private val manageMssDao: ManageMssDao,
    private val visitasDao: VisitasDao,
) : ManageMssRepository, BaseRepository() {

    private val _manageMss = MutableStateFlow<ManageMssEntity?>(null)
    override val manageMss get() = _manageMss.asStateFlow()

    //Carga de estados y apliaciones de medidas sanitarias
    override suspend fun loadTypeMss(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int,
    ): Result<List<ManageMssEntity>?> = execute {
        val idTipoProducto = idTipoProducto.toString()
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val mssp = when (tipoMedidaSanitaria) {
                CDZVE -> genericsApi.getMss(
                    BodyGenericValuesDto(
                        getIp(),
                        userName,
                        tipoMedidaSanitaria
                    )
                ).objectResponse
                CTIPPROD -> genericsApi.getMss(
                    BodyGenericValuesDto(
                        getIp(),
                        userName,
                        tipoMedidaSanitaria,
                        "1"
                    )
                ).objectResponse
                else -> genericsApi.getMss(
                    BodyGenericValuesDto(
                        ip = getIp(),
                        user = userName,
                        codigo = tipoMedidaSanitaria,
                        idTipoProducto = idTipoProducto
                    )
                ).objectResponse
            }
            deleteHistoricTypeMssDao(idTipoProducto, tipoMedidaSanitaria)
            saveHistoricTypeMssDao(mssp, idTipoProducto, tipoMedidaSanitaria)
            Result.success(mssp?.mapNotNull { it.mapToDomain(idTipoProducto, tipoMedidaSanitaria) })
        }

    //Carga de estados y apliaciones de medidas sanitarias DB local
    override suspend fun loadTypeMssLocal(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int,
    ): Result<List<ManageMssEntity>> {
        val result =
            manageMssDao.selectTypeMss(idTipoProducto.toString(), tipoMedidaSanitaria)
                ?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    //Guardar estados y apliaciones de medidas sanitarias DB local
    private suspend fun saveHistoricTypeMssDao(
        data: List<ManageMssDto>?,
        idTipoProducto: String,
        tipoMedidaSanitaria: String
    ) {
        data?.onEach { mssp ->
            val model = mssp.mapToDb(idTipoProducto, tipoMedidaSanitaria)
            manageMssDao.insertTypeMss(model)
        }.orEmpty()
    }

    //Eliminar estados y apliaciones de medidas sanitarias DB local
    private suspend fun deleteHistoricTypeMssDao(
        idTipoProducto: String,
        tipoMedidaSanitaria: String
    ) {
        manageMssDao.deleteAllTypeMss(idTipoProducto, tipoMedidaSanitaria)
    }

    //Carga de Roles
    override suspend fun loadRol(tipo: String, idTipoProducto: Int): Result<List<RolEntity>?> =
        execute {
            val idTipoProducto = idTipoProducto.toString()
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val roles =
                genericsApi.getRol(BodyGenericValuesDto(getIp(), userName, tipo)).objectResponse
            deleteHistoricTypeMssDao(idTipoProducto, tipo)
            roles?.onEach { rol ->
                val model = rol.mapToDb(idTipoProducto, tipo)
                manageMssDao.insertTypeMss(model)
            }.orEmpty()
            Result.success(roles?.mapNotNull { it.mapToDomain(idTipoProducto, tipo) })
        }

    //Carga de Lista de Productos por razonSocial
    override suspend fun loadListMss(
        razonSocial: String,
        tipoMedidaSanitaria: String
    ): Result<List<ManageMsspEntity>> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val body = MssBodyDto(getIp(), userName, razonSocial, tipoMedidaSanitaria)
        val mssp = sanitaryApi.getMssp(body).objectResponse
        deleteHistoricMsspListDao(razonSocial, tipoMedidaSanitaria)
        saveHistoricMsspListDao(mssp, razonSocial, tipoMedidaSanitaria)
        Result.success(mssp.map { it.mapToDomain(razonSocial, tipoMedidaSanitaria) }.orEmpty())
    }

    override suspend fun loadListMssLocal(
        razonSocial: String,
        tipoMedidaSanitaria: String
    ): Result<List<ManageMsspEntity>> = Result.success(
        manageMssDao.selectMsspList(razonSocial, tipoMedidaSanitaria).map { it.mapToDomain() }
    )

    //Guardar Lista de Productos por razonSocial en DB local
    private suspend fun saveHistoricMsspListDao(
        data: List<ManageMsspDto>?,
        razonSocial: String,
        tipoMedidaSanitaria: String
    ) {
        data?.onEach { msspList ->
            val model = msspList.mapToDb(razonSocial, tipoMedidaSanitaria)
            manageMssDao.insertMsspList(model)
        }.orEmpty()
    }

    //Eliminar Lista de Productos por razonSocial en DB local
    private suspend fun deleteHistoricMsspListDao(
        razonSocial: String,
        tipoMedidaSanitaria: String
    ) {
        manageMssDao.deleteMsspListxRazonSocial(razonSocial, tipoMedidaSanitaria)
    }


    override suspend fun updateMsspListLocal(
        id: Int,
        codigo: String,
        tipoMs: String,
        lote: String
    ): Result<Unit> = Result.success(
        manageMssDao.updateMssp(id, codigo, tipoMs, lote, getDateHourWithOutFormat())
    )

    // Servicio actulizar medida sanitaria
    override suspend fun updateMsspList(): Result<List<Unit>> = execute {
        val query = manageMssDao.selectMsspListAll().filter { !it.isLocal }
        val result = query.map {
            sanitaryApi.updateMssp(
                listOf(
                    UpdateMssBodyDto(
                        id = it.id.orZero(),
                        idEstado = it.idEstado.orZero()
                    )
                )
            )
        }
        manageMssDao.deleteMsspList()
        Result.success(result)
    }

    // Servicio insert aplicacion medida sanitaria
    override suspend fun updateAmsspLocal(it: ManageMssEntity, idVisita: String): Result<Unit> =
        execute {
            val result = manageMssDao.updateAmssp(it, idVisita, getDateHourNow())
            Result.success(result)
        }

    // Servicio insert aplicacion medida sanitaria
    override suspend fun insertOrUpdateTypeProductLocal(it: ManageMssEntity, idVisita: String): Result<Unit> =
        execute {
            val result = manageMssDao.insertOrUpdateTypeProductLocal(it, idVisita, getDateHourNow())
            Result.success(result)
        }

    override suspend fun insertMsseListLocal(
        visit: VisitEntity,
        codigo: String,
        tipoMs: String,
        typeActivity: String,
        nombreProducto: String,
        registroSanitario: String
    ): Result<Unit> = Result.success(
        manageMssDao.insertMsseList(
            visit,
            codigo,
            tipoMs,
            typeActivity,
            nombreProducto,
            registroSanitario
        )
    )

    // Servicio actulizar medida sanitaria
    override suspend fun insertMsseList(idVisit: Int): Result<List<Unit>> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val query = manageMssDao.selectMsseListAll()
        val result = query.map {
            val item = manageMssDao.selectAmsspxCodigo(
                it.codigoMedidaSanitaria.orEmpty(),
                idVisit.toString()
            )
            if (item != null) {
                val idTypeActivity = it.tiposActividades?.split(",")?.mapNotNull { typeActivity ->
                    manageMssDao.selectEmsspxDescripcion(typeActivity.trim(), TIPACTE)?.id
                }.orEmpty()

                try {
                    sanitaryApi.insertMsse(
                        listOf(
                            InsertMssBodyDto(
                                idVisita = idVisit,
                                idTipoMedidaSanitaria = item.id.orZero(),
                                fechaAplicacion = it.fechaAplicacion.orEmpty(),
                                tiposActividades = idTypeActivity,
                                usuarioCrea = userName,
                                nombreProducto = it.nombreProducto.orEmpty(),
                                notificacionSanitariaObligatoria = it.notificacionSanitariaObligatoria.orEmpty()
                            )
                        )
                    )
                } catch (e: Exception) {

                }
            }
        }
        Result.success(result)
    }

    //Buscar de medidas sanitarias
    override suspend fun searchRegisterSanitary(search: String): Result<String> = execute {
        val nombreProducto = registerSanitaryApi.searchRegisterSanitary(search).objectResponse
            ?.mapToDomain()?.infoGeneralProducto?.nombreProducto.orEmpty()
        Result.success(nombreProducto)
    }

    //Carga de medidas sanitarias aplicadas
    override suspend fun loadMssApplied(idVisita: String): Result<List<MssAppliedEntity>> =
        execute {
            val mssp = sanitaryApi.getMssApplied(idVisita).objectResponse
            manageMssDao.deleteMssAppliedXidVisit(idVisita)
            saveMssAppliedDao(mssp)
            saveIdTypeProduct(idVisita)
            Result.success(mssp.map { it.mapToDomain() })
        }

    //Guardar medidas sanitarias aplicadas DB local
    private suspend fun saveMssAppliedDao(data: List<MssAppliedDto>?) {
        data?.onEach { mssp ->
            val item = manageMssDao.selectEmssp(mssp.codigoMedidaSanitaria.orEmpty())
            if (item != null) {
                val model = mssp.mapToDb(item.tipoMs.orEmpty())
                manageMssDao.insertMssApplied(model)
            }
        }.orEmpty()
    }

    private suspend fun saveIdTypeProduct(idVisita: String) {
        val item = manageMssDao.selectMssTipoProduct(CD_TYPE_PRODUCT)
        item?.onEach {
            val idTipoProducto = visitasDao.selectVisita(idVisita.orEmpty())?.idTipoProducto
            if (idTipoProducto == it.id) {
                manageMssDao.insertMssApplied(
                    AmsspCheckModel(
                        id = it.id,
                        codigo = it.codigo,
                        descripcion = it.descripcion,
                        idVisita = idVisita.intOrString(),
                        isSelected = true,
                        tipoMs = CD_TYPE_PRODUCT,
                    )
                )
            }
        }
    }

    //Carga de medidas sanitarias aplicadas
    override suspend fun insertMssApplied(idVisita: String): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val query = manageMssDao.selectAmsspxIdVisit(idVisita)
        var listValue: ArrayList<LstValoresMssAppliedDto> = arrayListOf()
        query?.filter { it.tipoMs != CTIPPROD }?.mapNotNull {
            val list = LstValoresMssAppliedDto(it.id.orZero(), it.isSelected ?: false)
            listValue.add(list)
        }
        val result = if (!listValue.isNullOrEmpty()) {
            sanitaryApi.insertMssApplied(
                MssAppliedBodyDto(getIp(), userName, idVisita, listValue)
            )
        } else {
            Unit
        }
        Result.success(result)
    }

    //Carga de medidas sanitarias aplicadas DB Local
    override suspend fun loadMssAppliedLocal(idVisita: String): Result<List<ManageAmssEntity>> {
        val result = manageMssDao.selectAmsspxIdVisit(idVisita)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    override suspend fun getmssUpload(): Result<Int> = Result.success(
        manageMssDao.selectCountMssListAll(true)
    )

    override suspend fun insertRsLocal(
        idVisita: Int,
        nombreProducto: String,
        registroSanitario: String
    ): Result<Unit> = Result.success(
        manageMssDao.insertRs(RsModel(registroSanitario, nombreProducto, idVisita, true))
    )

    override suspend fun loadRsLocal(idVisita: Int): Result<List<RsEntity>> {
        val result = manageMssDao.selectRsAll(idVisita)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    override suspend fun countMssLocal(idVisita: String): Result<Int?> {
        val result = manageMssDao.selectAllMsspList(idVisita)
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    override suspend fun loadMssLocalByIdVisit(idVisita: Int): Result<List<ManageMsspEntity>> {
        val result = manageMssDao.selectAllMssListByIdVisit(idVisita)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    override suspend fun loadDateMssApplied(idVisita: Int, mss: ManageAmssEntity): Result<String> {
        val result = visitApi.getDateMssApplied(
            idVisita,
            mss.id.orZero(),
            mss.createdAt.orEmpty()
        ).objectResponse
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    override suspend fun loadTypeProduct(tipo: String): Result<List<TypeProductEntity>?> =
        execute {
            val idTipoProducto = userRepository.idMisional.toString()
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val result =
                genericsApi.getTypeProduct(BodyGenericValuesDto(getIp(), userName, tipo)).objectResponse
            deleteHistoricTypeMssDao(idTipoProducto, tipo)
            result?.onEach { typeProduct ->
                val model = typeProduct.mapToDb(idTipoProducto, tipo)
                manageMssDao.insertTypeMss(model)
            }.orEmpty()
            Result.success(result?.mapNotNull { it.mapToDomain(tipo) })
        }
}