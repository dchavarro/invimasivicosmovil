package com.soaint.data.repository

import com.soaint.data.api.NotificationApi
import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.NotificationDto
import com.soaint.data.model.mapToData
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.NotificationDao
import com.soaint.data.room.tables.NotificationModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToDomainEmails
import com.soaint.domain.model.EmailsEntity
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.model.NotificationEntity
import com.soaint.domain.repository.NotificationRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class NotificationRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val visitApi: VisitApi,
    private val notificationApi: NotificationApi,
    private val notificationDao: NotificationDao,
    ) : NotificationRepository, BaseRepository() {

    private val _notification = MutableStateFlow<List<NotificationEntity>?>(null)
    override val notification get() = _notification.asStateFlow()

    //Carga de email por visita
    override suspend fun getEmails(idVisita: String): Result<List<NotificationEntity>> = execute {
        val notification = visitApi.getEmail(idVisita).objectResponse
        //deleteDao()
        saveDao(notification.orEmpty(), idVisita)
        Result.success(notification.map { it.mapToDomain() }.orEmpty())
    }

    //Carga de email por visita DB local
    override suspend fun getEmailsLocal(idVisita: String): Result<List<EmailsEntity>> = Result.success(
        notificationDao.selectEmail(idVisita).map { it.mapToDomainEmails() }
    )

    //Guardar email en DB local
    private suspend fun saveDao(data: List<NotificationDto>?, idVisita: String) {
        data?.onEach { historic ->
            val model = historic.mapToDb(idVisita)
            notificationDao.insertOrUpdateEmail(model)
        }.orEmpty()
    }

    //Eliminar emails por visita en DB local
    private suspend fun deleteDao(idVisita: String) {
        notificationDao.deleteEmailByVisita(idVisita)
       }

    //Guardar email Local with Value
    override suspend fun addEmail(idVisita: String, correo: String): Result<Unit> {
        val result = notificationDao.insertOrUpdateEmail(NotificationModel(correo, idVisita))
        return Result.success(result)
    }

    // Notificar a emails
    override suspend fun saveEmails(
        idVisita: String,
        emails: List<String>
    ): Result<Unit> = execute {
        val usuario = userRepository.userInformation.value?.usuario.toString()
        val result = visitApi.saveEmails(
            NotificationBodyEntity(
                idVisita.toInt(),
                emails,
                usuario
            ).mapToData()
        )
        deleteDao(idVisita)
        Result.success(result)
    }

    // Notificar a estableciemiento
    override suspend fun sendNotification(idVisita: String, codigo: String): Result<Unit> = execute {
        val result = notificationApi.sendNotification(idVisita, codigo)
        Result.success(result)
    }


    override suspend fun deleteEmail(id: Int?) {
        notificationDao.deleteEmailById(id)
    }

}