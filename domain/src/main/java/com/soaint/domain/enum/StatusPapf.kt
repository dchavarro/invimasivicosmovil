package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.STATUS_PAPF_AIF
import com.soaint.sivicos_dinamico.utils.STATUS_PAPF_CIS
import com.soaint.sivicos_dinamico.utils.STATUS_PAPF_RRIF


enum class StatusPapf(val status: Array<String>) {
    STATUSPAPF(
        arrayOf(
            STATUS_PAPF_CIS,
            STATUS_PAPF_RRIF,
            STATUS_PAPF_AIF
        )
    )
}