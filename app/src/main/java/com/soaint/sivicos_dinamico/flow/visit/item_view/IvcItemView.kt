package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.data.common.orZero
import com.soaint.domain.model.ModeloIvcEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemIvcBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class IvcItemView(
    private val ivc: ModeloIvcEntity,
    private val onDelete: (ModeloIvcEntity) -> Unit
) : BindableItem<ItemIvcBinding>() {

    override fun bind(
        viewBinding: ItemIvcBinding,
        position: Int
    ) = with(viewBinding) {
        textViewTypeReq.text = ivc.descripciontipoRequerimiento.orEmpty()
        textViewCritic.text = ivc.descripcionCriticidad.orEmpty()
        textViewNumber.text = ivc.numeroRequerimiento.orZero().toString()

        buttonDelete.setOnSingleClickListener {
            onDelete.invoke(ivc)
        }
    }

    override fun getLayout() = R.layout.item_ivc
}