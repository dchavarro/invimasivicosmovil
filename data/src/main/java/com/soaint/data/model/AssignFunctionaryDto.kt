package com.soaint.data.model

import com.google.gson.annotations.SerializedName

data class AssignFunctionaryBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaAssignFunctionaryDto,
    @SerializedName("Parametros") val parametros: ParametrosAssignFunctionaryDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosAssignFunctionaryDto) : this(
        AuditoriaAssignFunctionaryDto(ip, user),
        parametros
    )
}
data class AuditoriaAssignFunctionaryDto(
    @SerializedName("IP") val iP: String,
    @SerializedName("Usuario") val usuario: String
)

data class CloseAssignFunctionaryResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<CloseInspectionPapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class ParametrosAssignFunctionaryDto(
    @SerializedName("idFuncionario") val idFuncionario: Int?,
    @SerializedName("fecha") val fecha: String?,
    @SerializedName("Solicitud") val Solicitud: Array<SolicitudAssignDto>,
)

data class SolicitudAssignDto(
    @SerializedName("idSolicitud") val idSolicitud: Int,
    @SerializedName("idProgramacion") val idProgramacion: Int?,
)