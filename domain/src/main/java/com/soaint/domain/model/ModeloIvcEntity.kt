package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ModeloIvcEntity(
    val id: Int? = 0,
    val idTipoRequerimiento: Int,
    val descripciontipoRequerimiento: String,
    val numeroRequerimiento: Int,
    val idCriticidad: Int? = null,
    val descripcionCriticidad: String,
    var isDeleted: Boolean? = false,
    var isLocal: Boolean? = false
) : Parcelable

@Parcelize
data class IvcDaEntity(
    val id: Int? = 0,
    val totalRequerimientos: Int?,
    val numeralesDeficiencias: String?,
    val idDistribucion: Int?,
    val descripcionDistribucion: String?,
    val volumenBeneficio: Int?,
    val veterinarioPlanta: Boolean?,
    val idTipoActividad: String?,
    val descripcionTipoActividad: String?,
    val idProductosProcesados: String?,
    val descripcionProductosProcesados: String?,
    val idPruebasLaboratorio: String?,
    val descripcionPruebasLaboratorio: String?,
    val isLocal: Boolean? = false,
) : Parcelable