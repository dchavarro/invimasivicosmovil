package com.soaint.data.api

import com.soaint.data.model.ProcessExtBodyDto
import retrofit2.http.Body
import retrofit2.http.PUT

interface AccessDataApi {

    @PUT("v1/accesoDatosTramite/continuarProcesoExterno")
    suspend fun updateProcessExt(
        @Body body: ProcessExtBodyDto
    ): Unit
}