package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.TaskModel
import com.soaint.domain.model.TaskBodyEntity
import com.soaint.domain.model.TaskCompanyBodyEntity
import com.soaint.domain.model.TaskEntity
import com.soaint.domain.model.TaskMssBodyEntity

data class TaskResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val data: List<TaskDto>,
    @SerializedName("statusCode") val statusCode: Int,
)

data class TaskOneResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val data: TaskDto,
    @SerializedName("statusCode") val statusCode: Int,
)

data class TaskDto(
    @SerializedName("activo") val activo: Boolean,
    @SerializedName("codigoTarea") val codigoTarea: String?,
    @SerializedName("descEstado") val descEstado: String?,
    @SerializedName("descripcionActividad") val descripcionActividad: String?,
    @SerializedName("descripcionOrigen") val descripcionOrigen: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("fechaTarea") val fechaTarea: String?,
    @SerializedName("idActividadIVC") val idActividadIVC: Int,
    @SerializedName("idEstadoTarea") val idEstadoTarea: Int,
    @SerializedName("idOrigenTarea") val idOrigenTarea: Int,
    @SerializedName("idTarea") val idTarea: Int,
    @SerializedName("idTipoProducto") val idTipoProducto: Int,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("codigoEstado") val codigoEstado: String?,
    @SerializedName("codigoOrigen") val codigoOrigen: String?,
    @SerializedName("codigoActividad") val codigoActividad: String?,
)

fun TaskDto.mapToDomain() = TaskEntity(
    activo,
    codigoTarea,
    descEstado,
    descripcionActividad,
    descripcionOrigen,
    fechaCreacion,
    fechaModifica,
    fechaTarea,
    idActividadIVC,
    idEstadoTarea,
    idOrigenTarea,
    idTarea,
    idTipoProducto,
    usuarioCrea,
    usuarioModifica,
    codigoEstado,
    codigoOrigen,
    codigoActividad
)

fun TaskDto.mapToDb(usuario: String) = TaskModel(
    idTarea,
    activo,
    codigoTarea,
    descEstado,
    descripcionActividad,
    descripcionOrigen,
    fechaCreacion,
    fechaModifica,
    fechaTarea,
    idActividadIVC,
    idEstadoTarea,
    idOrigenTarea,
    idTipoProducto,
    usuarioCrea,
    usuarioModifica,
    codigoEstado,
    codigoOrigen,
    codigoActividad,
    usuario
)

fun TaskEntity.mapToData() = TaskDto(
    activo,
    codigoTarea,
    descEstado,
    descripcionActividad,
    descripcionOrigen,
    fechaCreacion,
    fechaModifica,
    fechaTarea,
    idActividadIVC,
    idEstadoTarea,
    idOrigenTarea,
    idTarea,
    idTipoProducto,
    usuarioCrea,
    usuarioModifica,
    codigoEstado,
    codigoOrigen,
    codigoActividad
)

data class TaskBodyDto(
    @SerializedName("idTarea") val idTarea: Int,
    @SerializedName("activo") val activo: Boolean,
    @SerializedName("codigoActividad") val codigoActividad: String?,
    @SerializedName("codigoEstado") val codigoEstado: String?,
    @SerializedName("codigoOrigen") val codigoOrigen: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
)

fun TaskBodyDto.mapToDomain() = TaskBodyEntity(
    idTarea,
    activo,
    codigoActividad,
    codigoEstado,
    codigoOrigen,
    usuarioModifica,
)

fun TaskBodyEntity.mapToData() = TaskBodyDto(
    idTarea,
    activo,
    codigoActividad,
    codigoEstado,
    codigoOrigen,
    usuarioModifica
)

data class TaskMssBodyDto(
    @SerializedName("idActividadIVC") val idActividadIVC: String,
    @SerializedName("idOrigenTarea") val idOrigenTarea: String,
    @SerializedName("idEstadoTarea") val idEstadoTarea: String,
    @SerializedName("idTipoProducto") val idTipoProducto: Int,
    @SerializedName("usuarioCrea") val usuarioCrea: String,
)

fun TaskMssBodyEntity.mapToData() = TaskMssBodyDto(
    idActividadIVC,
    idOrigenTarea,
    idEstadoTarea,
    idTipoProducto,
    usuarioCrea
)

data class TaskCompanyBodyDto(
    @SerializedName("idSedeEmpreTarea") val idSedeEmpreTarea: Int,
    @SerializedName("idTarea") val idTarea: Int,
    @SerializedName("usuarioCrea") val usuarioCrea: String,
)

fun TaskCompanyBodyEntity.mapToData() = TaskCompanyBodyDto(
    idSedeEmpreTarea,
    idTarea,
    usuarioCrea
)
