package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlantillaEntity(
    val activo: Boolean? = true,
    val codigo: String? = EMPTY_STRING,
    val descEstado: String? = EMPTY_STRING,
    val descripcion: String? = EMPTY_STRING,
    val fechaCreacion: String? = EMPTY_STRING,
    val fechaModifica: String? = EMPTY_STRING,
    val id: Int? = 0,
    val idEstado: Int? = 0,
    val misionales: List<MisionalEntity>? = emptyList(),
    val nombre: String? = EMPTY_STRING,
    val plantillaOriginal: String? = EMPTY_STRING,
    val usuarioCrea: String? = EMPTY_STRING,
    val usuarioModifica: String? = EMPTY_STRING,
    val version: Int? = 0,
    val idTipoDocumental: Int? = 0
) : Parcelable

@Parcelize
data class MisionalEntity(
    val id: Int?,
    val idTipoProducto: Int?,
    val descripcion: String?,
    val idPlantilla: Int?,
) : Parcelable