package com.soaint.sivicos_dinamico.utils

import android.widget.RadioGroup
import android.widget.Spinner
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.isValidEmail
import com.soaint.sivicos_dinamico.extensions.toEditable

@BindingAdapter("field_valid")
fun validateField(editText: TextInputLayout, data: String?) = editText.run {
    if (data.isNullOrBlank()) {
        editText.isErrorEnabled = true
        editText.error = context.getString(R.string.campo_obligatorio)
    } else {
        editText.isErrorEnabled = false
        editText.error = null
    }
}

@BindingAdapter("field_email_valid")
fun validateEmailField(editText: TextInputLayout, data: String?) = editText.run {
    if (data.isNullOrBlank()) {
        editText.isErrorEnabled = true
        editText.error = context.getString(R.string.campo_obligatorio)
    } else if (!data.isValidEmail()) {
        editText.isErrorEnabled = true
        editText.error = context.getString(R.string.mensaje_email_no_valido)
    } else {
        editText.isErrorEnabled = false
        editText.error = null
    }
}

@BindingAdapter("spinner_valid")
fun validateSpinner(spinner: Spinner, data: Int?) = spinner.run {
    val border = if (data == null) R.drawable.border_red else R.drawable.border_no_padding
    spinner.background = ContextCompat.getDrawable(context, border)
}

@BindingAdapter("radio_valid")
fun validateRadioGroup(radioGroup: RadioGroup, data: Boolean?) = radioGroup.run {
    radioGroup.background =
        if (data == null) ContextCompat.getDrawable(context, R.drawable.border_red)
        else null
}

@BindingAdapter("price")
fun setPrice(editText: TextInputEditText, price: String?) {
    if (!price.isNullOrBlank()) {
        val context = editText.context
        val moneyFormatter = MoneyFormatter(context)
        editText.text = moneyFormatter.format(price.orEmpty()).toEditable()
    }
}

@BindingAdapter("selected_radio")
fun selectedRadioButton(radioGroup: RadioGroup, data: Boolean?) = radioGroup.run {
    if (data != null) {
        when (data) {
            true -> radioGroup.check(R.id.yes)
            false -> radioGroup.check(R.id.no)
        }
    }
}