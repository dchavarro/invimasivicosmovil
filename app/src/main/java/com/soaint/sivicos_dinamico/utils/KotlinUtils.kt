package com.soaint.sivicos_dinamico.utils

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Environment
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.soaint.data.common.PREFIX_IMAGE
import com.soaint.data.common.REQUEST_CODE_ASK_PERMISSIONS
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.objetos.ObjectMediafile
import com.soaint.sivicos_dinamico.objetos.ObjectUri
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class KotlinUtils {

    /**
     * Función para obtener una referencia a un archivo para guardar la foto
     */
    fun getOutputMediaFileUri(activity: Activity): ObjectUri? {
        val obj = ObjectUri()
        val objFile: ObjectMediafile = getOutputMediaFile(activity.applicationContext)!!
        if (objFile.file != null) {
            obj.setUri(
                FileProvider.getUriForFile(
                    activity, BuildConfig.APPLICATION_ID.toString() + ".provider",
                    objFile.file
                )
            )
            obj.setRuta(objFile.ruta)
            return obj
        }
        return null
    }

    /**
     * Creación del archivo para guardar la foto
     */
    public fun getOutputMediaFile(context: Context, prefix: String? = PREFIX_IMAGE): ObjectMediafile? {
        val mediaStorageDir: File
        val obj = ObjectMediafile()
        return if (Environment.MEDIA_MOUNTED == Environment.getExternalStorageState()) {
            //RUNTIME PERMISSION Android M
            mediaStorageDir = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                context.getString(R.string.app_name)
            )
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    return null
                }
            }
            val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
            val mediaFile: File
            val rutaTemp = mediaStorageDir.path + File.separator + prefix + timeStamp + ".jpg"
            mediaFile = File(rutaTemp)
            obj.file = mediaFile
            obj.ruta = rutaTemp
            obj
        } else {
            null
        }
    }

    fun requestPermissionPhoto(activity: Activity) {
        val RUNTIME_PERMISSIONS = arrayOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA
        )

        // check list of permissions
        var isPermissionsGranted = true
        for (permission in RUNTIME_PERMISSIONS) {
            if (ContextCompat.checkSelfPermission(
                    activity,
                    permission
                ) !== PackageManager.PERMISSION_GRANTED
            ) {
                isPermissionsGranted = false
                break
            }
        }

        requestPermissions(activity, RUNTIME_PERMISSIONS, REQUEST_CODE_ASK_PERMISSIONS)
    }

    fun isOnline(context: Context): Boolean {

        val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                    connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    return true
                }
            }
        }

        return false
    }

    fun getAppPath(context: Context): String {
        val dir = File(
            context.getExternalFilesDir(null)!!.absolutePath
                    + File.separator
                    + context.resources.getString(R.string.app_name)
                    + File.separator
        )
        if (!dir.exists())
            dir.mkdir()
        return dir.path + File.separator
    }

}