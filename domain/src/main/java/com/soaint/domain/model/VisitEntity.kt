package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class VisitEntity(
    val activo: Boolean,
    val descClasificacion: String,
    val descFuenteInformacion: String,
    val descGrupTrab: String?,
    val descPrioridad: String,
    val descRazon: String,
    val descResponRealVisita: String,
    val descTipoProducto: String,
    val descripcionMotivo: String?,
    val empresa: VisitEmpresaEntity?,
    val fecProximaProgramacion: String,
    val fechaCreacion: String,
    val fechaModifica: String,
    val fechaRealizo: String,
    val formaRealizarVisita: VisitFormaEntity?,
    val id: Int,
    val idClasificacion: Int?,
    val idDistancia: Int?,
    val idEstado: Int?,
    val idFuenteInformacion: Int?,
    val idGrupoTrabajo: Int?,
    val idModEjecutar: Int?,
    val idOficinaGTT: Int?,
    val idPosible: Int?,
    val idPrioridad: Int?,
    val idRazon: Int?,
    val idResponsableRealizar: Int?,
    val idResultado: Int?,
    val idTarea: Int?,
    val idTipoProducto: Int?,
    val numeroVisita: String,
    val objetivo: String,
    val observacionResultado: String,
    val requiAcompa: Boolean?,
    val tramite: VisitTramiteEntity?,
    val usuarioCrea: String,
    val usuarioModifica: String,
    val fechaInicioEjecucion: String,
    val descDistancia: String,
    val posibleMotivo: String,
    val modalidadEjecutar: String,
    val aplicaMedidaSanitaria: Boolean?,
    val legalAtiende: Boolean?,
    val requiereReprogramarVisita: Boolean?,
    val fechaVerificacionRequerimiento: String,
    val idConceptoSanitario: Int?,
    val descripcionConcepto: String,
    val calificacion: String,
    val fechaCierre: String,
    val idAccionSeguir: Int?,
    val codigoRazon: String?,
    val distancia: String?,
    val idEstadoEmpresa: Int?,
    val operacionEstablecimiento: String?,
    val especie: String?,
    var isClosed: Boolean? = false
) : Parcelable

@Parcelize
data class VisitEmpresaEntity(
    val activo: Boolean,
    val codigoSucursal: String?,
    val direccion: VisitDireccionEntity?,
    val empresaAsociada: String?,
    val fechaCreacion: String?,
    val fechaModifica: String?,
    val idEmpresa: Int?,
    val idEstadoEmpresa: Int?,
    val idSede: Int?,
    val idTipoDocumento: Int?,
    val idVisita: Int?,
    val numDocumento: String?,
    val razonSocial: String?,
    val rolSucursal: List<VisitCompanyRolEntity>?,
    val tipoDocumento: String?,
    val usuarioCrea: String?,
    val usuarioModifica: String?,
    val telefono: String?,
    val celular: String?,
    val correo: String?,
    val representanteLegal: List<VisitRepresentanteEntity>?,
    val paginaweb: String?,
    val digitoVerificacion: Int?,
    val sigla: String?,
    val personasVinculadas: List<VisitCompanyPersonEntity>?,
) : Parcelable

@Parcelize
data class VisitRepresentanteEntity(
    val nombre: String?,
    val correoElectronico: String?,
    val telefono: String?,
    val celular: String?,
) : Parcelable

@Parcelize
data class VisitDireccionEntity(
    val descDepartamento: String,
    val descMunicipio: String,
    val descPais: String,
    val descripcion: String,
    val idDepartamento: Int? = 0,
    val idMunicipio: Int? = 0,
    val idPais: Int? = 0,
) : Parcelable

@Parcelize
data class VisitCompanyRolEntity(
    val rol: String?,
) : Parcelable

@Parcelize
data class VisitCompanyPersonEntity(
    val codigoTipoDocumentoPersona: Int?,
    val tipoDocumento: String?,
    val numeroDocumentoPersona: String?,
    val primerNombre: String?,
    val segundoNombre: String? = EMPTY_STRING,
    val primerApellido: String? = EMPTY_STRING,
    val segundoApellido: String? = EMPTY_STRING,
    val idRolPersona: Int?,
    val descripcionRolPersona: String?,
    val correoElectronicoP: String?,
) : Parcelable

@Parcelize
data class VisitFormaEntity(
    val id: Int,
    val codigo: String,
    val descripcion: String,
    val idVisita: Int,
) : Parcelable

@Parcelize
data class VisitTramiteEntity(
    val id: Int?,
    val idTipo: Int?,
    val descTipoTramite: String?,
    val idSubTipo: Int?,
    val descSubTipoTramite: String?,
    val fecRadicacion: String?,
    val numeroRadicacion: String?,
    val idTramite: Int?,
    val indNotificarResultado: Boolean?,
    val indReprogramada: Boolean?,
    val expediente: String?,
    val idVisita: Int,
) : Parcelable

@Parcelize
data class VisitOfficialEntity(
    val usuario: String?,
    val nombreCompleto: String?,
    val tipoDocumento: String?,
    val numeroDocumento: String?,
    val cargo: String?,
    val idVisita: Int?
) : Parcelable

@Parcelize
data class VisitGroupEntity(
    val nombre: String?,
    val idVisita: Int?,
    val id: Int?,
) : Parcelable

@Parcelize
data class VisitUpdateBodyEntity(
    val id: Int,
    val aplicaMedidaSanitaria: Boolean,
    val legalAtiende: Boolean
) : Parcelable

@Parcelize
data class LocationBodyEntity(
    val latitud: String,
    val longitud: String,
    val idPersona: Int,
    val fechaCreacion: String,
    val usuarioCrea: String,
) : Parcelable