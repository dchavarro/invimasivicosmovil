package com.soaint.domain.use_case

import com.soaint.domain.model.BlockSaveBodyEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.ActRepository
import kotlinx.coroutines.delay
import retrofit2.HttpException
import java.net.SocketTimeoutException
import javax.inject.Inject


class ActUseCase @Inject constructor(
    private val actRepository: ActRepository,
) {

    suspend fun saveBlock(
        idPlantilla: String,
        idVisita: String,
        body: BlockSaveBodyEntity
    ) = actRepository.saveBlock(idPlantilla, idVisita, body)

    suspend fun getPdf(
        idPlantilla: String,
        visita: VisitEntity,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String> {
        val result = actRepository.getPdf(idPlantilla, visita, indVistaPrevia, body)
        val exception = result.exceptionOrNull()
        return when {
            exception is SocketTimeoutException || exception is KotlinNullPointerException || (exception as? HttpException)?.response()
                ?.code() == 504 -> {
                delay(5000)
                retry(visita.id.toString(), idPlantilla)
            }
            else -> result
        }
    }

    suspend fun retry(idVisita: String, idPlantilla: String) =
        actRepository.retry(idVisita, idPlantilla)

    suspend fun getBlockSync(
        idPlantilla: String,
        idVisita: String,
    ) = actRepository.getBlockSync(idPlantilla, idVisita)

    suspend fun getBlockValue(
        idPlantilla: String,
        idVisita: String,
    ) = actRepository.getBlockValue(idPlantilla, idVisita)

    suspend fun updateBlockValue(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String,
        value: Any?,
        idTipoAtributo: Int?,
    ) = actRepository.updateBlockValue(
        idPlantilla,
        idVisita,
        idBlock,
        idBlockAtribute,
        value,
        idTipoAtributo
    )

    suspend fun getBlockValueLocal(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String
    ) = actRepository.getBlockValueLocal(idPlantilla, idVisita, idBlock, idBlockAtribute)

    // PAPF
    suspend fun getPdfPapf(
        idPlantilla: String,
        idSolicitud: Int,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String> = actRepository.getPdfPapf(idPlantilla, idSolicitud, indVistaPrevia, body)

    suspend fun getBlockValuePapf(
        idPlantilla: String,
        idSolicitud: String,
    ) = actRepository.getBlockValuePapf(idPlantilla, idSolicitud)

    suspend fun getNameAtribute(
        idBlock: String,
        idBlockAtribute: String
    ) = actRepository.getNameAtribute( idBlock, idBlockAtribute)
}