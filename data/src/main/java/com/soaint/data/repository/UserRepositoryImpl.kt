package com.soaint.data.repository

import com.soaint.data.api.AuthApi
import com.soaint.data.api.TransversalApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.model.LoginBodyDto
import com.soaint.data.model.LoginRequestDto
import com.soaint.data.model.UserInformationBodyDto
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.UserDao
import com.soaint.data.room.tables.CredentialModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.common.BadCredentialException
import com.soaint.domain.common.getIp
import com.soaint.domain.common.orEmpty
import com.soaint.domain.common.safeLet
import com.soaint.domain.model.UserEntity
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.UserRolesEntity
import com.soaint.domain.repository.PreferencesRepository
import com.soaint.domain.repository.SecretPreferencesRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject
import javax.inject.Singleton

private val USER_NAME_KEY = "USER_NAME_KEY"

@Singleton
class UserRepositoryImpl @Inject constructor(
    private val userDao: UserDao,
    private val preferencesRepository: PreferencesRepository,
    private val secretPreferencesRepository: SecretPreferencesRepository,
    private val userApi: AuthApi,
    private val userInformationApi: TransversalApi,
) : UserRepository, BaseRepository(), CoroutineScope by MainScope() {

    private val _isLogged = MutableStateFlow(false)
    override val isLogged get() = _isLogged.asStateFlow()

    private val _user = MutableStateFlow<UserEntity?>(null)
    override val user get() = _user.asStateFlow()

    private val _userInformation = MutableStateFlow<UserInformationEntity?>(null)
    override val userInformation get() = _userInformation.asStateFlow()

    private val _userRoles = MutableStateFlow<List<UserRolesEntity>?>(null)
    override val userRoles get() = _userRoles.asStateFlow()

    override val idMisional get() = _userInformation.value?.idMisional.orEmpty()

    override val idDependencia get() = _userInformation.value?.idDependencia.orEmpty()

    init {
        launch(Dispatchers.Main) { getLastUser() }
    }

    private suspend fun getLastUser() {
        val userName = preferencesRepository.getString(USER_NAME_KEY).orEmpty()
        val localUser = userDao.selectUser(userName)?.mapToDomain()
        val localInformation = userDao.selectUserInformation(userName)?.mapToDomain()
        val localRoles =
            userDao.selectUserRoles(localInformation?.idUsuario.orEmpty())?.map { it.mapToDomain() }
        safeLet(localUser, localInformation, localRoles) { user, information, roles ->
            successGetProfile(
                user,
                information,
                roles
            )
        }
    }

    private fun successGetProfile(
        profile: UserEntity,
        information: UserInformationEntity,
        roles: List<UserRolesEntity>?
    ) {
        _user.value = profile
        _userInformation.value = information
        _userRoles.value = roles
    }

    override suspend fun getToken(
        userName: String,
        password: String
    ): Result<UserEntity> = execute {
        val user =
            userApi.getToken(LoginBodyDto(LoginRequestDto(userName, password))).user.mapToDomain()
        val data = userInformationApi.getUserInformation(UserInformationBodyDto(getIp(), userName))
        val information = data.mapToDomain()
        val roles = data.userData.roles
        userDao.deleteCredential(userName)
        userDao.insertCredential(CredentialModel(userName, password))
        preferencesRepository.setString(USER_NAME_KEY, userName)
        userDao.deleteUser(userName)
        userDao.insertUser(user.mapToModel(information.usuario))
        userDao.insertUserInformation(information.mapToModel())
        roles.onEach { userDao.insertUserRoles(it.mapToDomain().mapToModel(information.idUsuario)) }
        getLastUser()
        Result.success(user)
    }

    override suspend fun getLocalToken(
        userName: String,
        password: String
    ): Result<UserEntity> {
        val credentialModel = userDao.selectCredential(userName, password)
        val secretUserName = credentialModel?.userName.orEmpty()
        val secretUserPass = credentialModel?.password.orEmpty()
        val user = userDao.selectUser(userName)?.mapToDomain()
        val information = userDao.selectUserInformation(userName)?.mapToDomain()
        val roles =
            information?.idUsuario?.let { userDao.selectUserRoles(it)?.map { it.mapToDomain() } }
        return if (
            secretUserName.isNotBlank() &&
            secretUserPass.isNotBlank() &&
            secretUserName == userName &&
            secretUserPass == password &&
            user != null &&
            information != null &&
            roles != null
        ) {
            preferencesRepository.setString(USER_NAME_KEY, userName)
            getLastUser()
            Result.success(user)
        } else {
            Result.failure(BadCredentialException())
        }
    }

    override suspend fun updateUserInformation(
        userName: String,
        ip: String
    ): Result<UserInformationEntity> = execute {
        val data = userInformationApi.getUserInformation(UserInformationBodyDto(getIp(), userName))
        val information = data.mapToDomain()
        val roles = data.userData.roles

        userDao.deleteUserInformation(userName)
        userDao.insertUserInformation(information.mapToModel())
        userDao.deleteUserRoles(information.idUsuario)
        roles.onEach { userDao.insertUserRoles(it.mapToDomain().mapToModel(information.idUsuario)) }
        getLastUser()
        Result.success(information)
    }

    override fun logout() {
        _user.value = null
        _userInformation.value = null
        _userRoles.value = null
        _isLogged.value = false
        preferencesRepository.setString(USER_NAME_KEY, EMPTY_STRING)
    }
}