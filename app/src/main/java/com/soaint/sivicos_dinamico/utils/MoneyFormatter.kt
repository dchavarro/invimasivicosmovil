package com.soaint.sivicos_dinamico.utils

import android.content.Context
import com.soaint.data.common.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import java.math.BigDecimal
import java.math.BigDecimal.ZERO
import java.math.BigInteger
import java.text.NumberFormat
import java.util.*
import javax.inject.Inject
import kotlin.math.floor
import kotlin.math.pow
import kotlin.math.roundToInt

const val REMOVE_CURRENCY_REGEX = "[$., ]"
const val REMOVE_CURRENCY_REGEX_DECIMALS = "[$,]"
const val DECIMAL_PLACES = 2
const val NO_DECIMAL_PLACES = 0
const val DECIMAL_SYMBOL = "."

class MoneyFormatter @Inject constructor(
    val context: Context
) {

    private val locale = Locale.GERMANY

    fun format(value: Number) =
        context.resources.getString(
            R.string.format_money,
            NumberFormat.getIntegerInstance(locale).format(value)
        )

    fun format(value: String) =
        context.resources.getString(
            R.string.format_money,
            NumberFormat.getIntegerInstance(locale).format(value.toBigDecimal())
        )

    fun formatDecimals(value: Double): String {
        val numberFormat = NumberFormat.getNumberInstance(locale)
        numberFormat.minimumFractionDigits = DECIMAL_PLACES
        numberFormat.maximumFractionDigits = DECIMAL_PLACES
        return context.resources.getString(
            R.string.format_money,
            numberFormat.format(value)
        )
    }

    fun formatWithDecimalsNonZero(value: BigDecimal): String {
        return if (value.stripTrailingZeros().scale() <= 0) {
            formatDecimalsWithoutPlaces(value)
        } else {
            formatDecimals(value)
        }
    }

    fun formatDecimals(value: BigDecimal): String {
        val numberFormat = NumberFormat.getNumberInstance(locale)
        if (decimalPartOnCeroDecimals(value)) {
            numberFormat.minimumFractionDigits = DECIMAL_PLACES
            numberFormat.maximumFractionDigits = DECIMAL_PLACES
        } else {
            numberFormat.minimumFractionDigits = NO_DECIMAL_PLACES
            numberFormat.maximumFractionDigits = NO_DECIMAL_PLACES
        }
        return context.resources.getString(
            R.string.format_money,
            numberFormat.format(value)
        )
    }

    fun formatDecimalsWithoutPlaces(value: BigDecimal): String {
        val numberFormat = NumberFormat.getNumberInstance(locale)
        numberFormat.minimumFractionDigits = NO_DECIMAL_PLACES
        numberFormat.maximumFractionDigits = NO_DECIMAL_PLACES
        return context.resources.getString(
            R.string.format_money,
            numberFormat.format(floor(value.toDouble()))
        )
    }

    fun decimalPartOnCeroDecimals(bigdecimal: BigDecimal): Boolean {
        val zero = BigDecimal(BigInteger.ZERO)
        val intValue = bigdecimal.toInt()
        val decimal = bigdecimal.subtract(BigDecimal(intValue))
        return decimal > zero
    }

    fun formatDecimalsNegative(value: BigDecimal): String {
        val numberFormat = NumberFormat.getNumberInstance(locale)
        numberFormat.minimumFractionDigits = DECIMAL_PLACES
        numberFormat.maximumFractionDigits = DECIMAL_PLACES
        return context.resources.getString(
            R.string.format_negative_money,
            numberFormat.format(value)
        )
    }

    fun removeFormatDecimals(value: String): String {
        val finalValue = value.substringBefore(DECIMAL_SYMBOL)
        return finalValue.replace(REMOVE_CURRENCY_REGEX_DECIMALS.toRegex(), EMPTY_STRING).trim()
    }

    fun formatTransaction(value: Double): String {
        return if (value >= 0) {
            context.resources.getString(
                R.string.format_money_positive,
                NumberFormat.getIntegerInstance(locale).format(value)
            )
        } else {
            context.resources.getString(
                R.string.format_negative_money,
                NumberFormat.getIntegerInstance(locale).format(value * -1)
            )
        }
    }

    fun formatDecimalsTransaction(value: BigDecimal): String {
        val numberFormat = NumberFormat.getNumberInstance(locale)
        numberFormat.minimumFractionDigits = DECIMAL_PLACES
        numberFormat.maximumFractionDigits = DECIMAL_PLACES
        return if (value >= ZERO) {
            return context.resources.getString(
                R.string.format_money,
                numberFormat.format(value)
            )
        } else {
            context.resources.getString(
                R.string.format_negative_money,
                numberFormat.format(value * BigDecimal(-1))
            )
        }
    }

    fun roundToDecimals(number: Double, numDecimalPlaces: Int): Double {
        val factor = 10.0.pow(numDecimalPlaces.toDouble())
        return (number * factor).roundToInt() / factor
    }
}