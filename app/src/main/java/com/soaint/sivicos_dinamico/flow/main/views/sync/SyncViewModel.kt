package com.soaint.sivicos_dinamico.flow.main.views.sync

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.repository.TaskPapfRepository
import com.soaint.domain.use_case.SyncDataUseCase
import com.soaint.domain.use_case.TaskUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import kotlinx.coroutines.flow.collectLatest
import javax.inject.Inject

sealed class OnSyncViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val message: Int) : OnSyncViewModelEvent()
    object OnViewModelReady : OnSyncViewModelEvent()
    data class OnSyncError(val error: Throwable?) : OnSyncViewModelEvent()
    object OnViewModelCount : OnSyncViewModelEvent()
}

class SyncViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
    private val syncDataUseCase: SyncDataUseCase,
    private val taskUseCase: TaskUseCase,
    private val taskPapfRepository: TaskPapfRepository,
) : BaseViewModel() {

    /**
     * Events
     */

    private val _user = MutableLiveData<String?>()
    val user = _user.asLiveData()

    private val _event = SingleMutableLiveData<OnSyncViewModelEvent>()
    val event = _event.asLiveData()

    private val _syncDate = MutableLiveData<String?>(null)
    val syncDate = _syncDate.asLiveData()

    private val _count = MutableLiveData<Int>()
    val count = _count.asLiveData()

    private val _isEnableTask = MutableLiveData<Boolean>()
    val isEnableTask = _isEnableTask.asLiveData()

    /**
     * Init
     */

    fun init() {
        isVisibleTask()
        countQuerys()
        observe {
            _user.value = userUseCase.fullname()
        }
        observe {
            syncDataUseCase.syncDate.collectLatest { syncDate ->
                syncDate?.let {
                    _syncDate.value = syncDate
                }
            }
        }
    }

    /**
     * Actions
     */

    fun isVisibleTask() = execute {
        _isEnableTask.value = if (userUseCase.userIsPapf()) {
            !taskPapfRepository.loadTasksLocal().getOrNull().isNullOrEmpty()
        } else {
            !taskUseCase.loadTasksLocal().getOrNull().isNullOrEmpty()
        }
    }

    fun onSync() = execute {
        _event.value = OnSyncViewModelEvent.OnChangeLabelPreloader(R.string.cargando_sincronizacion)
        if (userUseCase.userIsPapf()) syncPapf() else syncExecution()
    }

    fun syncExecution() = execute {
        val result = syncDataUseCase.load()
        //TODO: Validar todos los estados de consultas y validar failed
        val plantillasResult = result.plantillas.firstOrNull()
        _event.value = if (plantillasResult?.plantillasResult?.isSuccess == true) {
            OnSyncViewModelEvent.OnViewModelReady
        } else {
            OnSyncViewModelEvent.OnSyncError(plantillasResult?.plantillasResult?.exceptionOrNull())
        }
    }

    fun syncPapf() = execute {
        val result = syncDataUseCase.loadPapf()
        //TODO: Validar todos los estados de consultas y validar failed
        _event.value = if (result.papf.tasksResult.isSuccess) {
            OnSyncViewModelEvent.OnViewModelReady
        } else {
            OnSyncViewModelEvent.OnSyncError(result.papf.tasksResult.exceptionOrNull())
        }
    }

    fun countQuerys() = execute {
        _count.value =
            if (userUseCase.userIsPapf()) syncDataUseCase.syncPapfPending() else syncDataUseCase.syncPending()
    }

    fun logout() {
        userUseCase.logout()
    }
}