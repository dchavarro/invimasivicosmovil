package com.soaint.domain.repository

import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity
import kotlinx.coroutines.flow.StateFlow

interface TransportRepository {

    val transport: StateFlow<TransportEntity?>

    suspend fun loadTransport(idVisit: String): Result<TransportEntity>

    suspend fun loadTransportLocal(idVisit: String): Result<List<TransportEntity>>

    suspend fun loadTransportOfficialLocal(idVisita: String): Result<List<OfficialEntity>>

    suspend fun saveTransport(body: TransportBodyEntity): Result<Unit>

    suspend fun saveTransportLocal(body: TransportBodyEntity): Result<Unit>

    suspend fun getTransportBodyLocal(idVisit: String): Result<List<TransportBodyEntity>>

    suspend fun getTransportUpload(): Result<Int>

    suspend fun getAllTransport(idVisit: String): Result<List<TransportBodyEntity>>

    suspend fun getAllTransportLocal(idVisit: String): Result<List<TransportBodyEntity>>

}