package com.soaint.data.api

import com.soaint.data.model.BodyUpdateCertificateDto
import retrofit2.http.Body
import retrofit2.http.POST

interface ProcedureApi {

    @POST("v1/Tramite/actualizarEstadoCertificado")
    suspend fun updateCertificate(
        @Body body: BodyUpdateCertificateDto
    ): Unit
}