package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.MisionalEntity

@Entity(tableName = "misional_table")
class MisionalModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun MisionalModel.mapToDomain(idPlantilla: Int?) = MisionalEntity(
    id,
    idTipoProducto,
    descripcion,
    idPlantilla
)