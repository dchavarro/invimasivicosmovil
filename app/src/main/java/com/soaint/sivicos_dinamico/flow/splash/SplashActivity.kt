package com.soaint.sivicos_dinamico.flow.splash

import android.os.Bundle
import androidx.activity.viewModels
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivitySplashBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.flow.login.LoginActivity
import com.soaint.sivicos_dinamico.properties.activityBinding

class SplashActivity : BaseActivity() {

    private val viewModel by viewModels<SplashViewModel> { viewModelFactory }

    private val binding by activityBinding<ActivitySplashBinding>(R.layout.activity_splash)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initView()
        subscribeViewModel()
    }

    private fun initView() = with(binding) {
        viewModel = this@SplashActivity.viewModel
        lifecycleOwner = this@SplashActivity
        buttonRetry.setOnSingleClickListener { this@SplashActivity.viewModel.init() }
        textViewVersion.text = getString(R.string.copy_version, BuildConfig.VERSION_NAME)
    }

    private fun subscribeViewModel() {
        viewModel.event.observe(this, ::stateHandle)
        viewModel.init()
    }

    private fun stateHandle(state: SplashViewModelEvent) {
        when (state) {
            is SplashViewModelEvent.UpgradeAppSplashEvent -> upgradeAppStateHandle()
            is SplashViewModelEvent.OpenHomeSplashEvent -> startActivity(MainActivity::class.java)
            is SplashViewModelEvent.OpenLoginSplashEvent -> startActivity(LoginActivity::class.java)
            is SplashViewModelEvent.ErrorSplashEvent -> errorSplashStateHandler(state.error)
            else -> {}
        }
    }

    private fun upgradeAppStateHandle() {
        finish()
    }

    private fun errorSplashStateHandler(error: Throwable?) {
        /*val errorRes = when (error) {
            is ConnectError -> R.string.copy_without_internet_connection
            else -> R.string.copy_generic_error
        }
        showError(binding.root, errorRes)*/
    }
}