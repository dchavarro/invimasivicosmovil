package com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.soaint.data.common.CHOOSE_FILE_REQUEST
import com.soaint.data.common.RESULT_VISIT_ID
import com.soaint.domain.common.*
import com.soaint.domain.model.DocumentNameEntity
import com.soaint.domain.model.GroupDocumentEntity
import com.soaint.domain.model.GroupEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityAddDocumentBinding
import com.soaint.sivicos_dinamico.extensions.chooseFile
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument.OnAddDocumentViewModelEvent.*
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding
import com.soaint.sivicos_dinamico.utils.ShowAlert
import com.soaint.sivicos_dinamico.utils.validateField

const val KEY_VISIT = "KEY_VISIT"

class AddDocumentActivity : BaseActivity() {

    companion object {
        fun getStartActivityEvent(
            visit: VisitEntity,
        ) = StartActivityEvent(
            AddDocumentActivity::class.java,
            Bundle().apply { putParcelable(KEY_VISIT, visit) },
            code = CHOOSE_FILE_REQUEST
        )
    }

    private val binding by activityBinding<ActivityAddDocumentBinding>(R.layout.activity_add_document)

    private val viewModel by viewModels<AddDocumentViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.root
        initView()
        observerViewModelEvents()
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        isVisible(false)
        binding.buttonDocument.isEnabled = false
        binding.buttonDocument.setOnSingleClickListener { chooseFile() }
    }

    private fun isVisible(visible: Boolean) {
        binding.textViewTitleName.isVisible = visible
        binding.spinnerName.isVisible = visible
        binding.textInputFolio.isVisible = visible
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(this, ::eventHandler)
        val visit: VisitEntity? = intent.getParcelableExtra(KEY_VISIT)
        viewModel.init(visit)
    }

    private fun eventHandler(event: OnAddDocumentViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnLoadingError -> onError()
            is OnViewModelGroupReady -> onViewModelGroupReady(event.group)
            is OnViewModelDocumentNameReady -> onDocumentNameReady(event.documentName)
            is OnValidateData -> validateErrorData(event.folio, event.path, event.name, event.extension)
            is OnViewModelSuccess -> onSuccess(event.idVisit)
        }
    }

    private fun validateErrorData(folio: String, base64: String, name: String, extension: String) {
        validateField(binding.textInputFolio, folio)
        if (base64.isBlank() || name.isBlank() || extension.isBlank()) onError()
    }

    private fun onError(message: String? = getString(R.string.copy_generic_error)) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                message.orEmpty(),
                titleBtnPositive = getString(R.string.cerrar))
            { finish() }
        )
    }

    private fun onSuccess(idVisit: String) {
        setResult(
            RESULT_OK,
            Intent().apply {
                putExtra(RESULT_VISIT_ID, idVisit)
            }
        )
        finish()
    }

    private fun onViewModelGroupReady(group: List<GroupDocumentEntity>) {
        val array = group.toMutableList()
        array.add(0, GroupDocumentEntity(nombreDependencia = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.nombreDependencia }

        binding.spinnerGroup.adapter =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        binding.spinnerGroup.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setGroupSelected(itemSelected)
                    isVisible(true)
                } else {
                    isVisible(false)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onDocumentNameReady(documentName: List<DocumentNameEntity>) {
        val array = documentName.toMutableList()
        array.add(0, DocumentNameEntity(nombreTipoDocumental = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.nombreTipoDocumental }

        binding.spinnerName.adapter =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        binding.spinnerName.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    binding.buttonDocument.isEnabled = true
                    viewModel.setDocumentNameSelected(itemSelected)
                } else {
                    binding.buttonDocument.isEnabled = false
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == CHOOSE_FILE_REQUEST) {
                if (data != null ){
                    val uri = data.data
                    val path = uri?.let { getFilePathFromUri(this, it) }
                    val name = getFileName(this, uri)
                    val size = getFileSize(this, uri)
                    val extension = uri?.let { getFileExtension(this, it).toString() }
                    if (!isFileLessThan5MB(size)) {
                        viewModel.checkData(path.orEmpty(), name.orEmpty(), extension.orEmpty())
                    } else{
                        onError(getString(R.string.copy_document_max_size))
                    }
                }
            }
        }
    }
}