package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ScheduleEntity
import com.soaint.domain.model.ScheduleTempEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemScheduleBinding
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.xwray.groupie.databinding.BindableItem

class ScheduleItemView(
    private val schedule: List<ScheduleTempEntity>,
) : BindableItem<ItemScheduleBinding>() {

    override fun bind(
        viewBinding: ItemScheduleBinding,
        position: Int
    ) {
        with(viewBinding) {
            schedule.onEach {
                val cantidadHoras = it.cantidadHoras.orEmpty().toString().toEditable()
                when (it.codigo) {
                    "TH_FAC" -> editTextHoras.text = cantidadHoras
                    "TH_NFES" -> editTextFeriadosNoc.text = cantidadHoras
                    "TH_DFES" -> editTextFeriadosDiu.text = cantidadHoras
                    "TH_DDIU" -> editTextDiu.text = cantidadHoras
                    "TH_DNOC" -> editTextNoc.text = cantidadHoras
                    "TH_TOT" -> editTextHorasServicio.text = cantidadHoras
                    "TH_CNFES" -> editTextFeriadosNocServicio.text = cantidadHoras
                    "TH_CDFES" -> editTextFeriadosDiuServicio.text = cantidadHoras
                    "TH_CDIU" -> editTextDiuServicio.text = cantidadHoras
                    "TH_CNOC" -> editTextNocServicio.text = cantidadHoras
                }
            }
        }
    }

    override fun getLayout() = R.layout.item_schedule
}