package com.soaint.data.di

import com.soaint.data.repository.*
import com.soaint.domain.repository.*
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {

    @Binds
    abstract fun provideUserRepository(
        impl: UserRepositoryImpl
    ): UserRepository

    @Binds
    abstract fun providePreferencesRepository(
        impl: PreferencesRepositoryImpl
    ): PreferencesRepository

    @Binds
    abstract fun provideSecretPreferencesRepository(
        impl: SecretPreferencesRepositoryImpl
    ): SecretPreferencesRepository

    @Binds
    abstract fun provideVersionRepository(
        impl: VersionRepositoryImpl
    ): VersionRepository

    @Binds
    abstract fun providePlantillasRepository(
        impl: PlantillasRepositoryImpl
    ): PlantillasRepository

    @Binds
    abstract fun provideTasksRepository(
        impl: TasksRepositoryImpl
    ): TaskRepository

    @Binds
    abstract fun provideVisitsRepository(
        impl: VisitsRepositoryImpl
    ): VisitRepository

    @Binds
    abstract fun provideHistoricRepository(
        impl: HistoricRepositoryImpl
    ): HistoricRepository

    @Binds
    abstract fun provideManageRepository(
        impl: ManageRepositoryImpl
    ): ManageRepository

    @Binds
    abstract fun provideHistoricMssRepository(
        impl: HistoricMssRepositoryImpl
    ): HistoricMssRepository

    @Binds
    abstract fun provideActRepository(
        impl: ActRepositoryImpl
    ): ActRepository

    @Binds
    abstract fun provideTransportRepository(
        impl: TransportRepositoryImpl
    ): TransportRepository

    @Binds
    abstract fun provideDocumentRepository(
        impl: DocumentRepositoryImpl
    ): DocumentRepository

    @Binds
    abstract fun provideNotificationRepository(
        impl: NotificationRepositoryImpl
    ): NotificationRepository

    @Binds
    abstract fun provideManageMssRepository(
        impl: ManageMssRepositoryImpl
    ): ManageMssRepository

    @Binds
    abstract fun provideModeloIvcRepository(
        impl: ModeloIvcRepositoryImpl
    ): ModeloIvcRepository

    @Binds
    abstract fun provideTechnicalRepository(
        impl: TechnicalRepositoryImpl
    ): TechnicalRepository

    @Binds
    abstract fun provideValidationRepository(
        impl: ValidationRepositoryImpl
    ): ValidationRepository

    @Binds
    abstract fun provideEquipmentRepository(
        impl: EquipmentRepositoryImpl
    ): EquipmentRepository

    @Binds
    abstract fun provideLocationRepository(
        impl: LocationRepositoryImpl
    ): LocationRepository

    @Binds
    abstract fun provideTransversalDaoRepository(
        impl: TransversalDaoRepositoryImpl
    ): TransversalDaoRepository

    @Binds
    abstract fun provideRegisterCompanyRepository(
        impl: RegisterCompanyRepositoryImpl
    ): RegisterCompanyRepository

    @Binds
    abstract fun provideTasksPapfRepository(
        impl: TasksPapfRepositoryImpl
    ): TaskPapfRepository

    @Binds
    abstract fun providePapfRepository(
        impl: PapfRepositoryImpl
    ): PapfRepository

    @Binds
    abstract fun provideFirmaRepository(
        impl: FirmaRepositoryImpl
    ): FirmaRepository
}