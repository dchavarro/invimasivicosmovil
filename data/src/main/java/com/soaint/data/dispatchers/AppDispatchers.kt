package com.soaint.data.dispatchers

import com.soaint.domain.dispatchers.AppDispatchers
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

class AppDispatchersImpl @Inject constructor(

) : AppDispatchers {

    override fun mainDispatcher() = Dispatchers.Main

    override fun networkDispatcher() = Dispatchers.IO
}