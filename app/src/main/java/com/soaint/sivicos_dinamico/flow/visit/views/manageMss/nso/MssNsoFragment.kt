package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.nso

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.orZero
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentManageMssNsoBinding
import com.soaint.sivicos_dinamico.extensions.hideKeyboard
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssSearchItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.MsspListItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class MssNsoFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentManageMssNsoBinding>(R.layout.fragment_manage_mss_nso)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: MssNsoViewModel by viewModels { viewModelFactory }

    private val adapterCheck by lazy { GroupieAdapter() }

    private val adapterList by lazy { GroupieAdapter() }

    private val adapterSearch by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_manage_mss_other))

        adapterCheck.spanCount = 2
        binding.rvCheck.layoutManager =
            GridLayoutManager(context, adapterCheck.spanCount).apply {
                spanSizeLookup = adapterCheck.spanSizeLookup
            }
        binding.rvCheck.adapter = adapterCheck

        binding.rvSearch.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvSearch.adapter = adapterSearch

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterList


        binding.buttonAdd.setOnSingleClickListener {
            viewModel.onInsert()
        }

        binding.buttonUpdate.setOnSingleClickListener {
            viewModel.onUpdate()
            requireActivity().hideKeyboard()
        }

        binding.buttonSearch.setOnSingleClickListener {
            if(binding.editText.text.toString().isNotEmpty()) {
                viewModel.onSearch(binding.editText.text.toString())
                binding.textInputLayout.isErrorEnabled = false
                binding.textInputLayout.error = null
            }else {
                binding.textInputLayout.isErrorEnabled = true
                binding.textInputLayout.error = getString(R.string.campo_obligatorio)
            }
            requireActivity().hideKeyboard()
        }

        binding.textInputLayout.setEndIconOnClickListener {
            binding.editText.text?.clear()
            binding.cardView.isVisible = false
        }
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: onMssNsoViewModelEvent) {
        showLoading(false)
        when (event) {
            is onMssNsoViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onMssNsoViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onMssNsoViewModelEvent.onSnackLoadingError -> onSnackLoadingError(event.message)
            is onMssNsoViewModelEvent.onViewModelReadyAmssp -> onViewModelReadyAmssp(event.amssp)
            is onMssNsoViewModelEvent.onViewModelReadyEmssp -> onViewModelReadyEmssp(event.emssp)
            is onMssNsoViewModelEvent.onViewModelReadyMsspList -> onViewModelReadyMsspList(event.msspList)
            is onMssNsoViewModelEvent.onViewModelUpdateMsspList -> onViewModelUpdateMsspSuccess()
            is onMssNsoViewModelEvent.onViewModelInsertMsspList -> onViewModelInsertMsspList()
            is onMssNsoViewModelEvent.onViewModelReadySearch -> onViewModelReadySearch(event.search)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReadyAmssp(mssp: List<ManageMssEntity>) {
        adapterCheck.clear()
        val items = mutableListOf<Group>()

        if (!mssp.isNullOrEmpty()) {
            items.addAll(mssp.map { MssItemView(it, ::onAmsspSelected) })
        }

        val isVisible = mssp.isNullOrEmpty()
        binding.rvCheck.isVisible = !isVisible
        adapterCheck.updateAsync(items)
    }

    private fun onAmsspSelected(mssp: ManageMssEntity, isSelected: Boolean) {
        viewModel.setAmsspSelected(mssp.codigo.orEmpty(), isSelected)
    }

    private fun onViewModelReadyEmssp(mssp: List<ManageMssEntity>) {
        val array = mssp.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerEmssp.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerEmssp.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) viewModel.setEmsspSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyMsspList(msspList: List<ManageMsspEntity>) {
        adapterList.clear()
        val items = mutableListOf<Group>()

        if (!msspList.isNullOrEmpty()) {
            items.addAll(msspList.map { MsspListItemView(it, ::onMsspListSelected, true, onChangeLote = ::onChangeLote) })
        }

        val isVisible = msspList.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
        adapterList.updateAsync(items)
    }

    private fun onMsspListSelected(msspList: ManageMsspEntity, isSelected: Boolean) {
        viewModel.setMsspSelected(msspList.id?.toInt()?: 0, isSelected)
    }

    private fun onChangeLote(id: Int, lote: String, isSelected: Boolean) {
        viewModel.setLote(id, lote, isSelected)
    }

    private fun onViewModelReadySearch(nombreProducto: String) {
        val isVisible = nombreProducto.isNullOrEmpty()
        viewModel.onGetAmssp()
        adapterSearch.clear()
        val items = mutableListOf<Group>()
        items.addAll(listOf(MssSearchItemView(nombreProducto, binding.editText.text.toString(), ::onSearchSelected)))
        binding.cardView.isVisible = !isVisible
        adapterSearch.updateAsync(items)
    }

    private fun onSearchSelected(nombreProducto: String, registroSanitario: String) {
        viewModel.setSearchSelected(nombreProducto, registroSanitario)
    }

    private fun onViewModelUpdateMsspSuccess() {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(R.string.copy_update_success),
            msj = EMPTY_STRING
        ) { }
    }

    private fun onViewModelInsertMsspList() {
        onViewModelUpdateMsspSuccess()
        binding.editText.text?.clear()
        binding.cardView.isVisible = false
    }

    private fun onSnackLoadingError(@StringRes message: Int) {
        requireActivity().showSnackBar(
            type = AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}