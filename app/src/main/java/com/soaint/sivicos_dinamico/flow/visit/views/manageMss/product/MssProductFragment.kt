package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.orZero
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentManageMssProductBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.MsspListItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class MssProductFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentManageMssProductBinding>(R.layout.fragment_manage_mss_product)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: MssProductViewModel by viewModels { viewModelFactory }

    private val adapterCheck by lazy { GroupieAdapter() }

    private val adapterList by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_manage_mss_product_title))
        adapterCheck.spanCount = 2
        binding.rvCheck.layoutManager =
            GridLayoutManager(context, adapterCheck.spanCount).apply {
                spanSizeLookup = adapterCheck.spanSizeLookup
            }
        binding.rvCheck.adapter = adapterCheck

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterList

        binding.buttonUpdate.setOnSingleClickListener {
            viewModel.onUpdate()
        }
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: onMsspViewModelEvent) {
        showLoading(false)
        when (event) {
            is onMsspViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onMsspViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onMsspViewModelEvent.onViewModelReadyEmssp -> onViewModelReadyEmssp(event.emssp)
            is onMsspViewModelEvent.onViewModelReadyAmssp -> onViewModelReadyAmssp(event.amssp)
            is onMsspViewModelEvent.onViewModelReadyMsspList -> onViewModelReadyMsspList(event.msspList)
            is onMsspViewModelEvent.onViewModelUpdateMsspList -> onViewModelUpdateMsspSuccess()
            is onMsspViewModelEvent.onViewModelReadyAmsspCheck -> onViewModelUpdateMsspSuccess()
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReadyAmssp(mssp: List<ManageMssEntity>) {
        adapterCheck.clear()
        val items = mutableListOf<Group>()

        if (!mssp.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_manage_mss_title_apply_product)))
            items.addAll(mssp.map { MssItemView(it, ::onAmsspSelected) })
            items.add(TitleItemView(getString(R.string.copy_manage_mss_title_def_product)))
        }

        val isVisible = mssp.isNullOrEmpty()
        binding.rvCheck.isVisible = !isVisible
        adapterCheck.addAll(items)
    }

    private fun onAmsspSelected(mssp: ManageMssEntity, isSelected: Boolean) {
        viewModel.setAmsspSelected(mssp.codigo.orEmpty(), isSelected)
    }

    private fun onViewModelReadyEmssp(mssp: List<ManageMssEntity>) {
        val array = mssp.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerEmssp.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerEmssp.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) viewModel.setEmsspSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyMsspList(msspList: List<ManageMsspEntity>) {
        adapterList.clear()
        val items = mutableListOf<Group>()

        if (!msspList.isNullOrEmpty()) {
            items.addAll(msspList.map { MsspListItemView(it, ::onMsspListSelected, onChangeLote = ::onChangeLote) })
        }
        adapterList.addAll(items)
        val isVisible = msspList.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
    }

    private fun onMsspListSelected(msspList: ManageMsspEntity, isSelected: Boolean) {
        viewModel.setMsspSelected(msspList.id?.toInt()?: 0, isSelected)
    }

    private fun onChangeLote(id: Int, lote: String, isSelected: Boolean) { }

    private fun onViewModelUpdateMsspSuccess() {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(R.string.copy_update_success),
            msj = EMPTY_STRING
        ) {}
    }

}