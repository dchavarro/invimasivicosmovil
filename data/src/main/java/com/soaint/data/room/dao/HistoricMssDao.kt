package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.HistoricMssModel
import com.soaint.data.room.tables.MssModel

@Dao
interface HistoricMssDao {

    //Historicos Medidas sanitarias
    @Query("SELECT * FROM historic_mss_table WHERE razonSocial = :razonSocial ")
    suspend fun selectHistoricMss(razonSocial: String): HistoricMssModel?

    @Insert(entity = HistoricMssModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHistoricMss(razonSocial: HistoricMssModel)

    @Query("DELETE FROM historic_mss_table WHERE razonSocial = :razonSocial")
    suspend fun deleteHistoricMssByRazonSocial(razonSocial: String)

    //Medidas sanitarias
    @Query("SELECT * FROM mss_table WHERE razonSocial = :razonSocial ")
    suspend fun selectMss(razonSocial: String): List<MssModel>?

    @Insert(entity = MssModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMss(razonSocial: List<MssModel>)

    @Query("DELETE FROM mss_table WHERE razonSocial = :razonSocial")
    suspend fun deleteMssByRazonSocial(razonSocial: String)
}