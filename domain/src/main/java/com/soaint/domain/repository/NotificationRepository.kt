package com.soaint.domain.repository

import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.EmailsEntity
import com.soaint.domain.model.HistoricEntity
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.model.NotificationEntity
import kotlinx.coroutines.flow.StateFlow

interface NotificationRepository {

    val notification: StateFlow<List<NotificationEntity>?>

    suspend fun getEmails(idVisita: String): Result<List<NotificationEntity>>

    suspend fun getEmailsLocal(idVisita: String): Result<List<EmailsEntity>>

    suspend fun addEmail(idVisita: String, correo: String): Result<Unit>

    suspend fun saveEmails(idVisita: String, emails: List<String>): Result<Unit>

    suspend fun sendNotification(idVisita: String, codigo: String): Result<Unit>
    suspend fun deleteEmail(id:Int?)
}