package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.EquipmentModel

@Dao
interface EquipmentDao {

    //Equipos
    @Query("SELECT * FROM equipment_table WHERE idSede = :idSede ORDER BY id")
    suspend fun selectEquipment(idSede: Int): List<EquipmentModel>

    @Query("SELECT * FROM equipment_table WHERE id = :id")
    suspend fun selectEquipmentById(id: Int): EquipmentModel?

    @Query("SELECT * FROM equipment_table ORDER BY id")
    suspend fun selectEquipmentAll(): List<EquipmentModel>?

    @Insert(entity = EquipmentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEquipment(equipmentModel: EquipmentModel)

    @Query("DELETE FROM equipment_table")
    suspend fun deleteAllEquipment()

    @Query("DELETE FROM equipment_table WHERE idSede = :idSede")
    suspend fun deleteEquipmentBySede(idSede: Int)

    @Query("DELETE FROM equipment_table WHERE id = :id")
    suspend fun deleteEquipmentById(id: Int)

    @Update
    suspend fun updateEquipment(equipmentModel: EquipmentModel)

    @Transaction
    suspend fun insertOrUpdateEquipment(equipmentModel: EquipmentModel) {
        val item :EquipmentModel? = selectEquipmentById(equipmentModel.id.orZero())
        if (item == null) {
            val newId = selectEquipmentAll()?.lastOrNull()?.id?.orZero()
            equipmentModel.id = newId.orZero() + 1
            equipmentModel.isLocal = true
            insertEquipment(equipmentModel)
        } else {
            equipmentModel.isUpdate = true
            equipmentModel.isLocal = item.isLocal
            equipmentModel.id = item.id
            updateEquipment(equipmentModel)
        }
    }

    @Query("SELECT * FROM equipment_table WHERE isUpdate = :isUpdate")
    suspend fun selectEquipmentUpdate(isUpdate: Boolean = true): List<EquipmentModel>?

    @Query("SELECT * FROM equipment_table WHERE isLocal = :isLocal")
    suspend fun selectEquipmentByLocal(isLocal: Boolean = true): List<EquipmentModel>?
    @Query("SELECT MIN(id) as id, * from equipment_table WHERE idResultado = 2 GROUP BY idZonaVerificar")
    suspend fun selectEquipmentUnfinished(): List<EquipmentModel>
}
