package com.soaint.data.di

import android.content.Context
import androidx.room.Room
import com.soaint.data.room.SivicoDatabase
import com.soaint.data.room.dao.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DataBaseModule {

    @Provides
    @Singleton
    fun providerSivicoDatabase(context: Context): SivicoDatabase =
        Room.databaseBuilder(
            context,
            SivicoDatabase::class.java,
            SivicoDatabase::javaClass.name
        )
            .fallbackToDestructiveMigration()
            .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    fun providerActasDao(sivicoDatabase: SivicoDatabase): ActasDao =
        sivicoDatabase.actasDao()

    @Provides
    @Singleton
    fun providerPlantillasDao(sivicoDatabase: SivicoDatabase): PlantillasDao =
        sivicoDatabase.plantillasDao()

    @Provides
    @Singleton
    fun providerTasksDao(sivicoDatabase: SivicoDatabase): TasksDao =
        sivicoDatabase.tasksDao()

    @Provides
    @Singleton
    fun providerVisitDao(sivicoDatabase: SivicoDatabase): VisitasDao =
        sivicoDatabase.visitDao()

    @Provides
    @Singleton
    fun providerHistoricDao(sivicoDatabase: SivicoDatabase): HistoricDao =
        sivicoDatabase.historicDao()

    @Provides
    @Singleton
    fun providerHistoricMssDao(sivicoDatabase: SivicoDatabase): HistoricMssDao =
        sivicoDatabase.historicMssDao()

    @Provides
    @Singleton
    fun providerUserDao(sivicoDatabase: SivicoDatabase): UserDao =
        sivicoDatabase.userDao()

    @Provides
    @Singleton
    fun providerTransportDao(sivicoDatabase: SivicoDatabase): TransportDao =
        sivicoDatabase.transportDao()

    @Provides
    @Singleton
    fun providerDocumentDao(sivicoDatabase: SivicoDatabase): DocumentDao =
        sivicoDatabase.documentDao()

    @Provides
    @Singleton
    fun providerNotificationDao(sivicoDatabase: SivicoDatabase): NotificationDao =
        sivicoDatabase.notificationDao()

    @Provides
    @Singleton
    fun providerBlocksDao(sivicoDatabase: SivicoDatabase): BlocksDao =
        sivicoDatabase.blocksDao()

    @Provides
    @Singleton
    fun providerManageMssDao(sivicoDatabase: SivicoDatabase): ManageMssDao =
        sivicoDatabase.manageMssDao()

    @Provides
    @Singleton
    fun providerModeloIvcDao(sivicoDatabase: SivicoDatabase): ModeloIvcDao =
        sivicoDatabase.modeloIvcDao()

    @Provides
    @Singleton
    fun providerTechnicalDao(sivicoDatabase: SivicoDatabase): TechnicalDao =
        sivicoDatabase.technicalDao()

    @Provides
    @Singleton
    fun providerCloseDao(sivicoDatabase: SivicoDatabase): CloseDao =
        sivicoDatabase.closeDao()

    @Provides
    @Singleton
    fun providerEquipmentDao(sivicoDatabase: SivicoDatabase): EquipmentDao =
        sivicoDatabase.equipmentDao()

    @Provides
    @Singleton
    fun providerTransversalDaoDao(sivicoDatabase: SivicoDatabase): TransversalDaoDao =
        sivicoDatabase.transversalDaoDao()

    @Provides
    @Singleton
    fun providerRegisterCompanyDao(sivicoDatabase: SivicoDatabase): RegisterCompanyDao =
        sivicoDatabase.registerCompanyDao()

    @Provides
    @Singleton
    fun providerTasksPapfDao(sivicoDatabase: SivicoDatabase): TasksPapfDao =
        sivicoDatabase.tasksPapfDao()

    @Provides
    @Singleton
    fun providerPapfDao(sivicoDatabase: SivicoDatabase): PapfDao =
        sivicoDatabase.papfDao()
}