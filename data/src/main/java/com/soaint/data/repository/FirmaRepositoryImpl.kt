package com.soaint.data.repository

import com.soaint.data.api.FirmaApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.BodyFirmaDto
import com.soaint.data.model.FirmaBodyDto
import com.soaint.domain.common.getIp
import com.soaint.domain.repository.FirmaRepository
import com.soaint.domain.repository.UserRepository
import javax.inject.Inject

class FirmaRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val firmaApi: FirmaApi,
) : FirmaRepository, BaseRepository() {

    //Firma certificada
    override suspend fun firmaCertificada(
        idSolicitud: Int,
        pdfFileBase64: String
    ): Result<String?> =
        execute {
            val userName = userRepository.userInformation.value?.usuario.toString()
            val idFuncionario = userRepository.userInformation.value?.idFuncionario.toString()
            val body = BodyFirmaDto(
                pdfFileBase64 = pdfFileBase64,
                idTramite = idSolicitud.orZero().toString(),
                idFuncionarioFirmante = idFuncionario
            )
            val result =
                firmaApi.firmaCertificada(FirmaBodyDto(getIp(), userName, body)).objectResponse
            Result.success(result?.orEmpty())
        }

}