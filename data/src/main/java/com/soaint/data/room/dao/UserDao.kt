package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.CredentialModel
import com.soaint.data.room.tables.UserInformationModel
import com.soaint.data.room.tables.UserModel
import com.soaint.data.room.tables.UserRolesModel

@Dao
interface UserDao {

    @Query("SELECT * FROM credential_table WHERE userName = :userName AND password = :password")
    suspend fun selectCredential(userName: String, password: String): CredentialModel?

    @Insert(entity = CredentialModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCredential(model: CredentialModel)

    @Query("DELETE FROM credential_table WHERE userName = :userName")
    suspend fun deleteCredential(userName: String)

    @Query("SELECT * FROM user_table WHERE userName = :userName")
    suspend fun selectUser(userName: String): UserModel?

    @Insert(entity = UserModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUser(model: UserModel)

    @Query("DELETE FROM user_table WHERE userName = :userName")
    suspend fun deleteUser(userName: String)

    @Query("SELECT * FROM user_information_table WHERE userName = :userName")
    suspend fun selectUserInformation(userName: String): UserInformationModel?

    @Insert(entity = UserInformationModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserInformation(model: UserInformationModel)

    @Query("DELETE FROM user_information_table WHERE userName = :userName")
    suspend fun deleteUserInformation(userName: String)

    @Query("SELECT * FROM user_roles_table WHERE idUsuario = :idUsuario")
    suspend fun selectUserRoles(idUsuario: Int): List<UserRolesModel>?

    @Insert(entity = UserRolesModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUserRoles(model: UserRolesModel)

    @Query("DELETE FROM user_roles_table WHERE idUsuario = :idUsuario")
    suspend fun deleteUserRoles(idUsuario: Int)
}