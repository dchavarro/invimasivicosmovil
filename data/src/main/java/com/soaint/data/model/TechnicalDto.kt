package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.TechnicalModel
import com.soaint.domain.model.TechnicalEntity

data class TechnicalBodyDto(
    @SerializedName("Auditoria") val auditoria: AuditoriaDto,
    @SerializedName("Parametros") val parametros: InputVisitDto
) {

    constructor(ip: String, user: String, idVisita: Int) : this(
        AuditoriaDto(ip, user), InputVisitDto(idVisita)
    )
}

data class TechnicalResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val objectResponse: List<TechnicalDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class TechnicalDto(
    @SerializedName("idPersona") val idPersona: Int?,
    @SerializedName("idRolPersona") val idRolPersona: Int?,
    @SerializedName("idEmpresaAsociada") val idEmpresaAsociada: Int?,
    @SerializedName("correoElectronico") val correoElectronico: String?,
    @SerializedName("telefono") val telefono: String?,
    @SerializedName("nombreCompleto") val nombreCompleto: String?,
    @SerializedName("numeroDocumento") val numeroDocumento: String?,
    @SerializedName("rolPersona") val rolPersona: String?,
    @SerializedName("celular") val celular: String?,
    @SerializedName("tipoDocumento") val tipoDocumento: String?,
    @SerializedName("codigoTipoDocumento") val codigoTipoDocumento: String?,
    @SerializedName("NumeroVisita") val numeroVisita: String?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("confirmado") val confirmado: Boolean? = false,
    @SerializedName("profesion") val profesion: String?,
    @SerializedName("seleccionado") val seleccionado: Boolean? = false,
)

fun TechnicalDto.mapToDomain() = TechnicalEntity(
    idPersona,
    idRolPersona,
    idEmpresaAsociada,
    correoElectronico,
    telefono,
    nombreCompleto,
    numeroDocumento,
    rolPersona,
    celular,
    tipoDocumento,
    codigoTipoDocumento,
    numeroVisita,
    idVisita,
    confirmado,
    profesion,
    seleccionado,
)

fun TechnicalDto.mapToDb() = TechnicalModel(
    idPersona,
    idRolPersona,
    idEmpresaAsociada,
    correoElectronico,
    telefono,
    nombreCompleto,
    numeroDocumento,
    rolPersona,
    celular,
    tipoDocumento,
    codigoTipoDocumento,
    numeroVisita,
    idVisita,
    confirmado,
    profesion,
    seleccionado,
)

fun TechnicalEntity.mapToData() = TechnicalDto(
    idPersona,
    idRolPersona,
    idEmpresaAsociada,
    correoElectronico,
    telefono,
    nombreCompleto,
    numeroDocumento,
    rolPersona,
    celular,
    tipoDocumento,
    codigoTipoDocumento,
    numeroVisita,
    idVisita,
    confirmado,
    profesion,
    seleccionado,
)

data class BodyTechnicalDto(
    @SerializedName("Auditoria") val auditoria: AuditoriaDto,
    @SerializedName("Parametros") val parametros: TechnicalDto
) {
    constructor(ip: String, user: String, technical: TechnicalDto) : this(
        AuditoriaDto(ip, user),
        technical
    )
}