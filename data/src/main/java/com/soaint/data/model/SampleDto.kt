package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.SampleAnalysisModel
import com.soaint.data.room.tables.SampleModel
import com.soaint.data.room.tables.SamplePlanModel
import com.soaint.domain.model.SampleAnalysisEntity
import com.soaint.domain.model.SampleEntity
import com.soaint.domain.model.SamplePlanEntity

data class SampleResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<SampleDto>,
    @SerializedName("statusCode") val statusCode: Int?
)

data class SampleDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("descGrupoProducto") val descGrupoProducto: String?,
    @SerializedName("descParametroEvaluar") val descParametroEvaluar: String?,
    @SerializedName("descSubGrupoProducto") val descSubGrupoProducto: String?,
    @SerializedName("descTipoProducto") val descTipoProducto: String?,
    @SerializedName("expedienteProducto") val expedienteProducto: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("idGrupoProducto") val idGrupoProducto: Int?,
    @SerializedName("idSubGrupoProducto") val idSubGrupoProducto: Int?,
    @SerializedName("idTipoProducto") val idTipoProducto: Int?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("identificacionMuestra") val identificacionMuestra: String?,
    @SerializedName("muestraTomarProducto") val muestraTomarProducto: String?,
    @SerializedName("nombreGenericoProducto") val nombreGenericoProducto: String?,
    @SerializedName("nombreProducto") val nombreProducto: String?,
    @SerializedName("numLoteProducto") val numLoteProducto: String?,
    @SerializedName("numRegisSaniProducto") val numRegisSaniProducto: String?,
    @SerializedName("referencia") val referencia: String?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?
)

fun SampleDto.mapToDomain() = SampleEntity(
    id, activo, descGrupoProducto, descParametroEvaluar, descSubGrupoProducto, descTipoProducto, expedienteProducto, fechaCreacion, fechaModifica, idGrupoProducto, idSubGrupoProducto, idTipoProducto, idVisita, identificacionMuestra, muestraTomarProducto, nombreGenericoProducto, nombreProducto, numLoteProducto, numRegisSaniProducto, referencia, usuarioCrea
)

fun SampleDto.mapToDb() = SampleModel(
    id, activo, descGrupoProducto, descParametroEvaluar, descSubGrupoProducto, descTipoProducto, expedienteProducto, fechaCreacion, fechaModifica, idGrupoProducto, idSubGrupoProducto, idTipoProducto, idVisita, identificacionMuestra, muestraTomarProducto, nombreGenericoProducto, nombreProducto, numLoteProducto, numRegisSaniProducto, referencia, usuarioCrea
)

data class SampleAnalysisResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<SampleAnalysisDto>,
    @SerializedName("statusCode") val statusCode: Int?
)

data class SampleAnalysisDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("idMuestra") val idMuestra: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("parametroEvaluar") val parametroEvaluar: String?,
)

fun SampleAnalysisDto.mapToDomain() = SampleAnalysisEntity(
    id, nombre, idMuestra, activo, usuarioCrea, fechaModifica, parametroEvaluar
)

fun SampleAnalysisDto.mapToDb() = SampleAnalysisModel(
    id, nombre, idMuestra, activo, usuarioCrea, fechaModifica, parametroEvaluar
)

data class SamplePlanResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<SamplePlanDto>,
    @SerializedName("statusCode") val statusCode: Int?
)

data class SamplePlanDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("idMuestra") val idMuestra: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
)

fun SamplePlanDto.mapToDomain() = SamplePlanEntity(
    id, descripcion, idMuestra, activo, usuarioCrea, fechaModifica
)

fun SamplePlanDto.mapToDb() = SamplePlanModel(
    id, descripcion, idMuestra, activo, usuarioCrea, fechaModifica
)
