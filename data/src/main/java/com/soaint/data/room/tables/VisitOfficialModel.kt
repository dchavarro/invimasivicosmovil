package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.domain.model.VisitOfficialEntity

@Entity(tableName = "visit_official_table",
    foreignKeys = [
        ForeignKey(
            entity = VisitModel::class,
            parentColumns = ["id"],
            childColumns = ["idVisita"],
            onDelete = ForeignKey.CASCADE
        )
    ])
class VisitOfficialModel(
    @ColumnInfo(name = "usuario") val usuario: String?,
    @ColumnInfo(name = "nombreCompleto") val nombreCompleto: String?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "cargo") val cargo: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    )

fun VisitOfficialModel.mapToDomain(idVisita: Int) = VisitOfficialEntity(
    usuario, nombreCompleto, tipoDocumento, numeroDocumento, cargo, idVisita
)