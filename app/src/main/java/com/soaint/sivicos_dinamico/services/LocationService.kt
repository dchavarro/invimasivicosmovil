package com.soaint.sivicos_dinamico.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.soaint.domain.use_case.StartLocationUpdatesUseCase
import com.soaint.domain.use_case.StopLocationUpdatesUseCase
import com.soaint.domain.use_case.VisitUseCase
import dagger.android.DaggerService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

const val SERVICE_NOTIFICATION_ID = 12984

class LocationService : DaggerService(), CoroutineScope by MainScope() {

    @Inject
    lateinit var startLocationUpdates: StartLocationUpdatesUseCase

    @Inject
    lateinit var stopLocationUpdates: StopLocationUpdatesUseCase

    @Inject
    lateinit var visitUseCase: VisitUseCase

    private var job: Job? = null

    override fun onBind(intent: Intent): IBinder? = null

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
        observeLocationUpdates()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= 26) {
            val CHANNEL_ID = "channel_location"
            val channel = NotificationChannel(
                CHANNEL_ID,
                "Channel Location",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            (getSystemService(Context.NOTIFICATION_SERVICE) as? NotificationManager)?.createNotificationChannel(
                channel
            )
            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("")
                .setContentText("").build()
            startForeground(1, notification)
        }
    }

    private fun observeLocationUpdates() {
        stopLocation()
        job = launch {
            startLocationUpdates().collectLatest {
                if (it.latitude != 0.0 && it.longitude != 0.0) {
                    visitUseCase.saveLocation(it.latitude.toString(), it.longitude.toString())
                }
            }
        }
    }

    override fun onDestroy() {
        stopLocation()
        super.onDestroy()
    }

    private fun stopLocation() {
        job?.cancel()
        stopLocationUpdates()
    }
}