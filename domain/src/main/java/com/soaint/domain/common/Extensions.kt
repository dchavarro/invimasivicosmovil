package com.soaint.domain.common

import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

fun Int?.orEmpty(): Int =
    this?:0

fun Double?.orEmpty(): Double =
    this?:0.0

fun String.formatDate(): String {
    try {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        val newDate: Date = inputFormat.parse(this.orEmpty())
        return outputFormat.format(newDate)
    } catch (e: ParseException) {
        return this
    }
}

fun String.formatDateHour(): String {
    try {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.getDefault())
        val newDate: Date = inputFormat.parse(this.orEmpty())
        return outputFormat.format(newDate)
    } catch (e: ParseException) {
        return this
    }
}

fun String.formatDate12Hour(): String {
    try {
        val inputFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())
        val outputFormat: DateFormat = SimpleDateFormat("dd/MM/yyyy hh:mm a", Locale.getDefault())
        val newDate: Date = inputFormat.parse(this.orEmpty())
        return outputFormat.format(newDate)
    } catch (e: ParseException) {
        return this
    }
}