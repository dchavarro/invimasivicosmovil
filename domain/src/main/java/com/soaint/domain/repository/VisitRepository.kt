package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface VisitRepository {

    val visitas: StateFlow<List<VisitEntity>?>

    val groupsVisit: StateFlow<List<VisitGroupEntity>?>

    suspend fun loadVisitas(idTask: String): Result<List<VisitEntity>>

    suspend fun loadVisitLocal(idTask: Int): Result<List<VisitEntity>>

    suspend fun loadVisitLocalById(idVisit: Int): Result<VisitEntity?>

    suspend fun loadAntecedent(idVisit: String): Result<List<AntecedentEntity>>

    suspend fun loadAntecedentLocal(idVisit: String): Result<List<AntecedentEntity>>
    suspend fun loadAntecedentLocalForTask(idTask: Int?): Result<List<AntecedentEntity>>

    suspend fun loadSamples(idVisit: String): Result<List<SampleEntity>>

    suspend fun loadSamplesLocal(idVisit: String): Result<List<SampleEntity>>

    suspend fun loadSamplesAnalysisLocal(idMuestra: String): Result<List<SampleAnalysisEntity>>

    suspend fun loadSamplesPlanLocal(idMuestra: String): Result<List<SamplePlanEntity>>

    suspend fun loadOfficial(idVisit: String): Result<List<VisitOfficialEntity>>

    suspend fun loadOfficialLocal(idVisit: String): Result<List<VisitOfficialEntity>>

    suspend fun loadGroup(idVisit: String): Result<List<VisitGroupEntity>>

    suspend fun loadGroupLocal(idVisit: String): Result<List<VisitGroupEntity>>

    suspend fun loadSubGroup(idVisit: String): Result<List<VisitGroupEntity>>

    suspend fun loadSubGroupLocal(idVisit: String): Result<List<VisitGroupEntity>>

    suspend fun updateVisitLocal(visita: VisitEntity, isClosed: Boolean): Result<Unit>

    suspend fun updateVisit(body: VisitEntity): Result<Unit>

    suspend fun loadAntecedentLocalByPqr(idVisit: String): Result<List<AntecedentEntity>>

    suspend fun updateAntecedentLocal(antecedent: AntecedentEntity?): Result<Unit>

    suspend fun updateAntecedent(body: List<AntecedentEntity>): Result<Unit>

    suspend fun loadInteraction(idVisit: String): Result<InteractionEntity>

    suspend fun loadInteractionLocal(idVisit: String): Result<InteractionEntity?>

    suspend fun updateInteraction(body: InteractionEntity): Result<Unit>

    suspend fun updateInteractionLocal(body: InteractionEntity?): Result<Unit>

    suspend fun copyVisit(idVisit: String): Result<Unit>

    suspend fun copyVisitInper(idVisit: Int): Result<Unit>

    suspend fun loadSchedule(idTramite: Int): Result<List<ScheduleEntity>?>

    suspend fun loadScheduleLocal(idTramite: Int): Result<List<ScheduleTempEntity>>

    suspend fun syncLocation(): Result<Unit>

    suspend fun saveLocation(latitude: String, longitude: String): Result<Unit>

    suspend fun saveLocationLocal(latitude: String, longitude: String): Result<Unit>

    suspend fun createAntecedent(body: AntecedentBodyEntity): Result<Unit>
    suspend fun loadHolidays(): Result<List<HolidaysEntity>>
    suspend fun loadShiftType(): Result<List<ShiftTypeEntity>>
    suspend fun loadTypeHoursBilling(): Result<List<TypeHoursBillingEntity>>
    suspend fun getIdTramite(idVisit: Int): Int?
    suspend fun getJordana(jornada: String):  String?
    suspend fun existsEventDate(fecha: String): Boolean
    suspend fun saveScheduleTemp(idTramite: Int)
    suspend fun getQuantityHoursByCode(code: String):  Double?
    suspend fun saveSchedule(list: List<ScheduleEntity>)
    suspend fun getTypeHoursBilling(idType: Int): TypeHoursBillingEntity
    suspend fun deleteAllScheduleTemp()
    suspend fun updateSchedule(): Result<List<Unit>>

}