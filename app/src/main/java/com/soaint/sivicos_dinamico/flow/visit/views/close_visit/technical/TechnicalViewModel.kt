package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.technical

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.model.TechnicalEntity
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.TechnicalUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class onTechnicalViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onTechnicalViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onTechnicalViewModelEvent()
    data class OnViewModelReady(val technical: List<TechnicalEntity>?) : onTechnicalViewModelEvent()
    data class OnViewModelSuccess(@StringRes val message: Int) : onTechnicalViewModelEvent()
}

class TechnicalViewModel @Inject constructor(
    private val technicalUseCase: TechnicalUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onTechnicalViewModelEvent>()
    val event = _event.asLiveData()

    private val _visit = MutableLiveData<VisitEntity>()
    val visit = _visit.asLiveData()

    private val _user = MutableLiveData<UserInformationEntity>()
    val user = _user.asLiveData()

    private val _isSelected = MutableLiveData<Boolean>(null)
    val isSelected = _isSelected.asLiveData()

    private val _technicalSelected = MutableLiveData<TechnicalEntity>(null)
    val technicalSelected = _technicalSelected.asLiveData()


    fun init(visitData: List<VisitEntity>) {
        _visit.value = visitData.firstOrNull()
        onLoadTechnical()
    }

    private fun onLoadTechnical() = execute {
        val result = technicalUseCase.getTechnicalsLocal(_visit.value?.id.orZero())
        _event.value = if (result.isSuccess) {
            onTechnicalViewModelEvent.OnViewModelReady(result.getOrNull())
        } else {
            onTechnicalViewModelEvent.onLoadingError(null)
        }
    }

    fun setIsSelected(isSelected: Boolean, technical: TechnicalEntity) {
        _technicalSelected.value = technical.copy(confirmado = isSelected)
        updateTechnical(_technicalSelected.value)
    }


    fun updateTechnical(technical: TechnicalEntity?) = execute {
        val result = technical?.let { technicalUseCase.updateTechnicalLocal(it) }
        if (result != null) {
            _event.value = if (result.isSuccess) {
                onTechnicalViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
            } else {
                onTechnicalViewModelEvent.onLoadingError(null)
            }
        }
        onLoadTechnical()
    }
}