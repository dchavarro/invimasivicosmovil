package com.soaint.data.repository

import com.google.gson.Gson
import com.soaint.data.api.GenericsApi
import com.soaint.data.api.ParametricApi
import com.soaint.data.api.ProxyApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.intOrString
import com.soaint.data.model.PdfResponseDto
import com.soaint.data.model.mapToData
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.ActasDao
import com.soaint.data.room.dao.PlantillasDao
import com.soaint.data.room.tables.ActasPdfModel
import com.soaint.data.room.tables.BlockValueModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.common.TiposControl
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.BlockSaveBodyEntity
import com.soaint.domain.model.BlockValueEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.provider.FileProvider
import com.soaint.domain.repository.ActRepository
import com.soaint.domain.repository.UserRepository
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.utils.ID_MISIONAL_SANITARIAS
import javax.inject.Inject

class ActRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val parametricApi: ParametricApi,
    private val proxyApi: ProxyApi,
    private val genericsApi: GenericsApi,
    private val actasDao: ActasDao,
    private val plantillasDao: PlantillasDao,
    private val fileProvider: FileProvider,
    private val papfUseCase: PapfUseCase,
    private val userUseCase: UserUseCase
) : ActRepository, BaseRepository() {

    //Guardar Block with Value
    override suspend fun saveBlock(
        idPlantilla: String,
        idVisita: String,
        body: BlockSaveBodyEntity
    ): Result<BlockSaveBodyEntity> = execute {
        val usuarioCrea = userRepository.userInformation.value?.usuario.toString()

        val result = if (!userUseCase.userIsPapf()) {
            parametricApi.saveBlock(idPlantilla, idVisita, usuarioCrea, body.mapToData())
        } else {
            // Guardar popup de registrar productos
            papfUseCase.saveDataReqInsSan(idVisita.intOrString())
            papfUseCase.saveDataReqActSample(idVisita.intOrString())
            // Sincronizacion
            parametricApi.saveBlockPapf(idPlantilla, idVisita, usuarioCrea, body.mapToData())
        }
        Result.success(result.objectResponse!!.mapToDomain())
    }

    override suspend fun getPdf(
        idPlantilla: String,
        visita: VisitEntity,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String> = execute {
        val usuarioCrea = userRepository.userInformation.value?.usuario.toString()
        val result = proxyApi.getPdf(
            idPlantilla,
            visita.id.toString(),
            indVistaPrevia,
            usuarioCrea,
            visita.tramite?.idTramite,
            body as HashMap<String, @JvmSuppressWildcards Any?>
        )
        savePdf(result, idPlantilla, visita.id.toString())
        Result.success(result.objectResponse.orEmpty())
    }

    //Guardar PDF acta DB local
    private suspend fun savePdf(
        data: PdfResponseDto,
        idPlantilla: String,
        idVisita: String? = null
    ) {
        actasDao.insertPdf(
            ActasPdfModel(
                pdf = data.objectResponse.orEmpty(),
                idPlantilla = idPlantilla,
                idVisita = idVisita
            )
        )
    }

    override suspend fun getBlockSync(
        idPlantilla: String,
        idVisita: String,
    ): Result<BlockSaveBodyEntity> = execute {
        val result = parametricApi.getBlockSync(idPlantilla, idVisita).objectResponse
        Result.success(result!!.mapToDomain())
    }

    override suspend fun retry(
        idVisita: String,
        idPlantilla: String,
    ): Result<String> = execute {
        val result = genericsApi.retry(idVisita, idPlantilla)
        savePdf(result, idPlantilla, idVisita)
        Result.success(result.objectResponse.orEmpty())
    }

    override suspend fun getBlockValue(
        idPlantilla: String,
        idVisita: String,
    ): Result<List<BlockEntity>> = execute {
        val result = parametricApi.getBlockValue(idPlantilla, idVisita).objectResponse
        Result.success(result!!.map { it.mapToDomain() })
    }

    override suspend fun updateBlockValue(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String,
        value: Any?,
        idTipoAtributo: Int?,
    ) {
        if (value is String) {
            val isImageValue = idTipoAtributo in arrayOf(TiposControl.FIRMA, TiposControl.FOTO)
            val valor = if (isImageValue) fileProvider.saveFile(
                System.currentTimeMillis().toString(),
                value
            ) else value
            plantillasDao.insertOrUpdateBlockValue(
                BlockValueModel(
                    idVisita = idVisita,
                    idPlantilla = idPlantilla,
                    idBloque = idBlock,
                    idBloqueAtributo = idBlockAtribute,
                    valorString = valor,
                    idTipoAtributo = idTipoAtributo,
                )
            )
        } else if (value is ArrayList<*>) {
            val valor = value as? ArrayList<ArrayList<String>>
            if (valor == null) {
                plantillasDao.insertOrUpdateBlockValue(
                    BlockValueModel(
                        idVisita = idVisita,
                        idPlantilla = idPlantilla,
                        idBloque = idBlock,
                        idBloqueAtributo = idBlockAtribute,
                        valorArray = Gson().toJson(value),
                        idTipoAtributo = idTipoAtributo,
                    )
                )
            } else {
                plantillasDao.insertOrUpdateBlockValue(
                    BlockValueModel(
                        idVisita = idVisita,
                        idPlantilla = idPlantilla,
                        idBloque = idBlock,
                        idBloqueAtributo = idBlockAtribute,
                        valorArrayArray = Gson().toJson(value),
                        idTipoAtributo = idTipoAtributo,
                    )
                )
            }
        }
    }

    override suspend fun getBlockValueLocal(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String,
    ): Result<BlockValueEntity?> = execute {
        val result = plantillasDao.selectBlockValue(
            idVisita = idVisita,
            idPlantilla = idPlantilla,
            idBloque = idBlock,
            idBlockAtribute = idBlockAtribute,
        )
        //val isImageValue = result?.idTipoAtributo in arrayOf(TiposControl.FIRMA, TiposControl.FOTO)
        //val valor =
        //    if (isImageValue) fileProvider.getFile(result?.valorString) else result?.valorString
        //val newResult = result?.mapToDomain()?.copy(valueString = valor)
        Result.success(result?.mapToDomain())
    }

    // Papf

    override suspend fun getPdfPapf(
        idPlantilla: String,
        idSolicitud: Int,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String> = execute {
        val usuarioCrea = userRepository.userInformation.value?.usuario.toString()
        val result = proxyApi.getPdfPapf(
            idPlantilla,
            idSolicitud,
            indVistaPrevia,
            usuarioCrea,
            body as HashMap<String, @JvmSuppressWildcards Any?>
        )
        savePdf(result, idPlantilla)
        Result.success(result.objectResponse.orEmpty())
    }

    override suspend fun getBlockValuePapf(
        idPlantilla: String,
        idSolicitud: String,
    ): Result<List<BlockEntity>> = execute {
        // Guardar popup de registrar productos
        papfUseCase.saveDataReqInsSan(idSolicitud.intOrString())
        papfUseCase.saveDataReqActSample(idSolicitud.intOrString())
        // Sincronizacion
        val result = parametricApi.getBlockValuePapf(idPlantilla, idSolicitud).objectResponse
        Result.success(result!!.map { it.mapToDomain() })
    }

    override suspend fun getNameAtribute(
        idBlock: String,
        idBlockAtribute: String,
    ): Result<BlockAtributePlantillaEntity> = execute {
        val result = plantillasDao.getNameAtribute(idBlock, idBlockAtribute)
        Result.success(result.mapToDomain(null))
    }
}