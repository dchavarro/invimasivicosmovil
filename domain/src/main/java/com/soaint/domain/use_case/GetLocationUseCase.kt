package com.soaint.domain.use_case

import com.soaint.domain.repository.LocationRepository
import javax.inject.Inject

class GetLocationUseCase @Inject constructor(
    private val locationRepository: LocationRepository
) {

    suspend operator fun invoke() = locationRepository.getLocation()
}