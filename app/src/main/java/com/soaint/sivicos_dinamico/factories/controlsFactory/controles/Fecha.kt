package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.app.DatePickerDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface
import java.text.DecimalFormat
import java.util.*


class Fecha: ControlInterface {
    var valorSeleccionado: String = ""
    lateinit var btn: Button
    lateinit var classContext: Context
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context
        valorSeleccionado = valorAlmacenado

        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_lista, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        lblText.text = control.descripcion
        btn = view.findViewById<Button>(R.id.btnLista)
        if(valorAlmacenado.isEmpty() || valorAlmacenado == "false") {
            btn.text = context.getString(R.string.seleccione)
        } else {
            btn.text = valorAlmacenado
        }
        btn.setOnClickListener {
            val c: Calendar = Calendar.getInstance()
            val yearCalendar = c.get(Calendar.YEAR)
            val dayMonth = c.get(Calendar.DAY_OF_MONTH)
            val month = c.get(Calendar.MONTH)
            val timePickerDialog = DatePickerDialog(context, { view, yearResponse, month, day_month ->
                val monthSelected = DecimalFormat("00").format(month+1).toString()
                val daySelected = DecimalFormat("00").format(day_month).toString()
                btn.text = "$daySelected/$monthSelected/$yearResponse"
                valorSeleccionado = "$daySelected/$monthSelected/$yearResponse"
            }, yearCalendar, month, dayMonth)
            timePickerDialog.setCancelable(false)
            timePickerDialog.show()
        }
        btn.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)
        return view
    }

    override fun getData(): String {
        return valorSeleccionado
    }

    override fun showError(mensaje: String?) {
        btn.setBackgroundColor(classContext.getColor(R.color.colorRed))
    }

    override fun clearError() {
        btn.setBackgroundColor(classContext.getColor(R.color.colorPrimary))
    }
}