package com.soaint.sivicos_dinamico.flow.visit.item_view

import androidx.core.view.isVisible
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.common.formatDate
import com.soaint.domain.model.HistoricEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemHistoricBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class HistoricItemView(
    private val historic: HistoricEntity,
    private val onSelected: (HistoricEntity) -> Unit,
    private val onView: (String) -> Unit,
    private var isVisible: Boolean = false
) : BindableItem<ItemHistoricBinding>() {

    override fun bind(
        viewBinding: ItemHistoricBinding,
        position: Int
    ) = with(viewBinding) {
        textViewNumber.text = historic.numero
        textViewDate.text = historic.fechaRealizo?.formatDate()
        textViewReason1.text = historic.descRazon

        relativeLayoutLeft.isVisible = false
        relativeLayoutRight.isVisible = false
        buttonView2.isVisible = false
        textViewTitleAct.isVisible = false

        //RelativeLeft
        textViewState.text = historic.estado
        textViewDateStart.text = historic.fechaRealizo?.formatDate()
        textViewLocation.text = historic.departamento
        textViewCity.text = historic.municipio
        textViewAddress.text = historic.descDireccion
        textViewTramit.text = historic.tipoTramite
        textViewSubtramit.text = historic.subTipoTramite
        textViewMeasure.text = historic.medidasSanitarias
        textViewClassification.text = historic.clasificacion

        //RelativeRigth
        textViewCompanyAssociet.text = historic.razonSocial
        textViewCompanyName.text = historic.razonSocial
        textViewResponsable.text = historic.descRespRealizar
        textViewInspector.text = historic.inspector
        textViewReason.text = historic.descRazon
        textViewInfo.text = historic.descFuenteInfo
        textViewConcept.text = historic.conceptoSanitario
        textViewResult.text = historic.resultado
        textViewObservation.text = historic.observacionResultado

        buttonView2.setOnSingleClickListener { onView.invoke(historic.idVisita.orEmpty()) }

        constraintLayoutPricipal.setOnClickListener {
            isVisible = !isVisible
            relativeLayoutLeft.isVisible = isVisible
            relativeLayoutRight.isVisible = isVisible
            buttonView2.isVisible = !historic.idVisita.toString().isNullOrEmpty() && isVisible
            textViewTitleAct.isVisible = !historic.idVisita.toString().isNullOrEmpty() && isVisible
        }
        root.setOnClickListener {
            //onSelected.invoke(antecedent)
        }
    }

    override fun getLayout() = R.layout.item_historic
}