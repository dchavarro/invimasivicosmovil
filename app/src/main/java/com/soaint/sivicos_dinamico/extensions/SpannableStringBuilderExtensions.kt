package com.soaint.sivicos_dinamico.extensions

import android.content.Context
import android.text.Layout
import android.text.SpannableStringBuilder
import android.text.style.AbsoluteSizeSpan
import android.text.style.AlignmentSpan
import android.text.style.ForegroundColorSpan
import android.text.style.TextAppearanceSpan
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.StyleRes
import androidx.core.content.ContextCompat.getColor
import androidx.core.text.inSpans
import androidx.core.view.TintableBackgroundView

inline fun SpannableStringBuilder.color(
    context: Context,
    @ColorRes color: Int,
    builderAction: SpannableStringBuilder.() -> Unit
) = inSpans(ForegroundColorSpan(getColor(context, color)), builderAction = builderAction)

inline fun SpannableStringBuilder.fontSize(
    context: Context,
    @DimenRes fontSizeRes: Int,
    builderAction: SpannableStringBuilder.() -> Unit
) = inSpans(AbsoluteSizeSpan(context.resources.getDimensionPixelSize(fontSizeRes)), builderAction)

inline fun SpannableStringBuilder.style(
    context: Context,
    @StyleRes styleRes: Int,
    builderAction: SpannableStringBuilder.() -> Unit
) = inSpans(TextAppearanceSpan(context, styleRes), builderAction)

inline fun SpannableStringBuilder.alignment(
    alignment: Layout.Alignment,
    builderAction: SpannableStringBuilder.() -> Unit
) = inSpans(AlignmentSpan.Standard(alignment), builderAction)