package com.soaint.domain.provider

interface NetworkProvider {

    val isAvailable: Boolean
}