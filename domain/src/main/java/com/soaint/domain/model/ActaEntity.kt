package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

val EMPTY_STRING: String = ""

@Parcelize
data class GroupEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val descripcion: String? = EMPTY_STRING,
    val idTipoProducto: Int? = 0,
) : Parcelable

@Parcelize
data class GroupDocumentEntity(
    val idGrupo: Int? = 0,
    val nombreDependencia: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class RulesBussinessEntity(
    val id: Int? = 0,
    val nombre: String? = EMPTY_STRING,
    val codigoPlantilla: String? = EMPTY_STRING,
    val idTipoDocumental: Int? = 0,
    val idGroup: Int? = 0,
    val idTipoProducto: Int? = 0,
) : Parcelable


data class BlockSaveBodyEntity(
    val documento: BlockDocumentSaveEntity,
    val bloques: List<BlockSaveEntity>
)

@Parcelize
data class BlockDocumentSaveEntity(
    val nombre: String,
    val titulo: String,
    val usuario: String,
    val fecha: String,
    val idTipoDocumental: Int,
    val detalle: String = EMPTY_STRING,
    val cantidadFolios: Int = 0,
    val codigoClasificacion: String = "EJE",
    val codigoEstado: String = "ESPL_ENPR",
) : Parcelable

data class BlockSaveEntity(
    val id: Int?,
    val codigo: String?,
    val nombre: String?,
    val descripcion: String?,
    val obligatorio: Boolean?,
    val configuracion: BlockConfigAtributeEntity?,
    val idTipoAtributo: Int?,
    val descTipoAtributo: String?,
    val idBloque: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val usuarioModifica: String?,
    val fechaModifica: String?,
    val valor: Any?,
) {

    constructor(block: BlockAtributePlantillaEntity, value: Any?) : this(
        block.id,
        block.codigo,
        block.nombre,
        block.descripcion,
        block.obligatorio,
        block.configuracion,
        block.idTipoAtributo,
        block.descTipoAtributo,
        block.idBloque,
        block.activo,
        block.usuarioCrea,
        block.fechaCreacion,
        block.usuarioModifica,
        block.fechaModifica,
        value
    )
}


data class BlockBodyEntity(
    val nombre: String?,
    val atributosPlantilla: List<BlockDataBodyEntity>?,
)

data class BlockDataBodyEntity(
    val nombre: String?,
    val valor: String?,
)

data class PdfBlockResponseEntity(
    val nombre: String?,
    val contenido: String?,
    val tipo: String?,
)