package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.SampleEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemOfficialBinding
import com.soaint.sivicos_dinamico.databinding.ItemSampleBinding
import com.xwray.groupie.databinding.BindableItem

class OfficialItemView(
    private val official: OfficialEntity,
    private val onSelected: (OfficialEntity) -> Unit
) : BindableItem<ItemOfficialBinding>() {

    override fun bind(
        viewBinding: ItemOfficialBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = official.nombreApellidos?.uppercase()
        textViewEmail.text = official.correoElectronico?.lowercase()
        textViewPhone.text = official.celular

        root.setOnClickListener {
            onSelected.invoke(official)
        }
    }

    override fun getLayout() = R.layout.item_official
}