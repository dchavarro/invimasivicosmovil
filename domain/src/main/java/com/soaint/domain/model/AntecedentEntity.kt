package com.soaint.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class AntecedentEntity(
    val id: Int,
    val idTipo: Int?,
    val descTipo: String?,
    val referencia: String?,
    val numeroAsociado: String?,
    val codigoTipo: String?,
    val idDenuncia: Int?,
    val aplicaDenuncia: Boolean?,
    val idTarea: Int?,
    val idVisita: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val fechaModifica: String?,
) : Parcelable

@Parcelize
data class AntecedentBodyEntity(
    val idTipo: Int?,
    val referencia: String?,
    val numeroAsociado: String?,
    val idTarea: Int?,
    val usuarioCrea: String?,
) : Parcelable