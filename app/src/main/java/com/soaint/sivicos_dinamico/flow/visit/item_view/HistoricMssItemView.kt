package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.domain.common.formatDate
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.MssEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemHistoricMssBinding
import com.xwray.groupie.databinding.BindableItem

class HistoricMssItemView(
    private val historic: MssEntity,
    private val onSelected: (MssEntity) -> Unit,
) : BindableItem<ItemHistoricMssBinding>() {

    override fun bind(
        viewBinding: ItemHistoricMssBinding,
        position: Int
    ) = with(viewBinding) {

        //RelativeLeft
        textViewNumber.text = historic.numeroMSS.toString()
        textViewIdVisit.text = historic.numeroVisita
        textViewDateEnd.text = historic.fechaCierreVisita?.formatDate()
        textViewReason.text = historic.razonVisita
        textViewTypeProduct.text = historic.tipoProducto

        //RelativeRigth
        textViewApply.text = historic.aplicado
        textViewType.text = historic.tipoMedidaSanita
        textViewProduct.text = historic.producto
        textViewState.text = historic.estadoMSS

        root.setOnClickListener {
            //onSelected.invoke(antecedent)
        }
    }

    override fun getLayout() = R.layout.item_historic_mss
}