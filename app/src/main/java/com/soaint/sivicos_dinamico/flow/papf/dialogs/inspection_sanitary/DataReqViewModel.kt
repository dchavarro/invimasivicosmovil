package com.soaint.sivicos_dinamico.flow.papf.dialogs.inspection_sanitary

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.DataReqInsSanPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CD_CONCEPT
import com.soaint.sivicos_dinamico.utils.CD_EMPAQ
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) : OnViewModelEvent()
    data class onViewModelPacks(val packs: List<DinamicQuerysPapfEntity>) : OnViewModelEvent()
    data class onViewModelConcepts(val concepts: List<DinamicQuerysPapfEntity>) : OnViewModelEvent()
    data class onViewModelData(val data: DataReqInsSanPapfEntity?) : OnViewModelEvent()
    data class OnValidate(
        val packId: Int?,
        val conceptId: Int?,
        val observation: String?,
        val condition: String?,
    ) : OnViewModelEvent()
}

class DataReqViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _packs = MutableLiveData<List<DinamicQuerysPapfEntity>>()
    val packs = _packs.asLiveData()

    private val _packSelected = MutableLiveData<DinamicQuerysPapfEntity>(null)
    val packSelected = _packSelected.asLiveData()

    private val _concepts = MutableLiveData<List<DinamicQuerysPapfEntity>>()
    val concepts = _concepts.asLiveData()

    private val _conceptSelected = MutableLiveData<DinamicQuerysPapfEntity>(null)
    val conceptSelected = _conceptSelected.asLiveData()

    private val _condition = MutableLiveData(EMPTY_STRING)
    val condition get() = _condition.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _idProductDetail = MutableLiveData<Int>()
    val idProductDetail = _idProductDetail.asLiveData()

    private val _idRequest = MutableLiveData<Int>()
    val idRequest = _idRequest.asLiveData()


    fun init(idDetalleProducto: Int, idSolicitud: Int) = execute {
        _idProductDetail.value = idDetalleProducto
        _idRequest.value = idSolicitud
        getEmpaque()
        getConcept()
        getData()
    }

    fun getData() = execute {
        val result = papfUseCase.loadDataReqInsSanLocal(
            _idRequest.value.orZero(),
            _idProductDetail.value.orZero()
        )
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.onViewModelData(result.getOrNull())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun getEmpaque() = execute {
        val result = papfUseCase.loadDinamicQuerysLocal(CD_EMPAQ)
        _event.value = if (result.isSuccess) {
            _packs.value = result.getOrNull().orEmpty()
            OnViewModelEvent.onViewModelPacks(result.getOrNull().orEmpty())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun getConcept() = execute {
        val result = papfUseCase.loadDinamicQuerysLocal(CD_CONCEPT)
        _event.value = if (result.isSuccess) {
            _concepts.value = result.getOrNull().orEmpty()
            OnViewModelEvent.onViewModelConcepts(result.getOrNull().orEmpty())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }


    fun onConditionChange(data: Editable?) {
        _condition.value = data.toString()
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }


    fun setPack(itemSelected: String) {
        _packs.value?.find { it.descripcion.equals(itemSelected, true) }?.let {
            _packSelected.value = it
        }
    }

    fun setConcept(itemSelected: String) {
        _concepts.value?.find { it.descripcion.equals(itemSelected, true) }?.let {
            _conceptSelected.value = it
        }
    }

    fun checkData() {
        val packId = _packSelected.value?.id
        val conceptId = _conceptSelected.value?.id
        val observation = _observation.value!!
        val condition = _condition.value!!

        if (packId == null || packId == 0 || conceptId == null || conceptId == 0 || observation.isNullOrBlank() || condition.isNullOrBlank())
            _event.value = OnViewModelEvent.OnValidate(packId, conceptId, observation, condition)
        else onSaveLocal()

    }

    fun onSaveLocal() = execute {
        val result = papfUseCase.insertOrUpdateDataReqInsSanLocal(
            DataReqInsSanPapfEntity(
                _packSelected.value?.id,
                _packSelected.value?.descripcion,
                _conceptSelected.value?.id,
                _conceptSelected.value?.descripcion,
                _condition.value!!,
                _observation.value!!,
                _idProductDetail.value!!,
                _idRequest.value!!,
            )
        )
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.onSnack(true, R.string.copy_update_success)
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }
}