package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SampleEntity(
    val id: Int?,
    val activo: Boolean?,
    val descGrupoProducto: String?,
    val descParametroEvaluar: String?,
    val descSubGrupoProducto: String?,
    val descTipoProducto: String?,
    val expedienteProducto: String?,
    val fechaCreacion: String?,
    val fechaModifica: String?,
    val idGrupoProducto: Int?,
    val idSubGrupoProducto: Int?,
    val idTipoProducto: Int?,
    val idVisita: Int?,
    val identificacionMuestra: String?,
    val muestraTomarProducto: String?,
    val nombreGenericoProducto: String?,
    val nombreProducto: String?,
    val numLoteProducto: String?,
    val numRegisSaniProducto: String?,
    val referencia: String?,
    val usuarioCrea: String?
) : Parcelable

@Parcelize
data class SampleAnalysisEntity(
    val id: Int?,
    val nombre: String?,
    val idMuestra: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaModifica: String?,
    val parametroEvaluar: String?
) : Parcelable

@Parcelize
data class SamplePlanEntity(
    val id: Int?,
    val descripcion: String?,
    val idMuestra: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaModifica: String?
) : Parcelable