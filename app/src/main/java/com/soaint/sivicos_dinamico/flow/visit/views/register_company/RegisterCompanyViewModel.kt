package com.soaint.sivicos_dinamico.flow.visit.views.register_company

import android.os.Bundle
import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.domain.use_case.RegisterCompanyUseCase
import com.soaint.domain.use_case.TransversalDaoUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.extensions.isValidEmail
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CD_TYPE_PRODUCT
import com.soaint.sivicos_dinamico.utils.ROL_PERSONA
import com.soaint.sivicos_dinamico.utils.TIPO_DOCUMENTO
import javax.inject.Inject

private const val KEY_DOC = "KEY_DOC"

sealed class OnRegisterCompanyViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnRegisterCompanyViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnRegisterCompanyViewModelEvent()
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyCompany(val company: RegisterCompanyBodyEntity?) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyTypeDoc(val typeDoc: List<TypeDaoEntity>) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyCountries(
        val countries: List<CountriesEntity>? = null,
        val countrySelected: CountriesEntity? = null,
        val department: List<DepartmentEntity>? = null,
        val departmentSelected: DepartmentEntity? = null,
        val town: List<TownEntity>? = null,
        val townSelected: TownEntity? = null,
    ) : OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyDepartment(val department: List<DepartmentEntity>) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyTown(val town: List<TownEntity>) : OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyTypeProduct(val typeProduct: List<ManageMssEntity>) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyRol(val rol: List<ManageMssEntity>) :
        OnRegisterCompanyViewModelEvent()

    data class onViewModelReadyPerson(val person: List<VisitCompanyPersonEntity>) :
        OnRegisterCompanyViewModelEvent()

    data class OnValidatePersonData(
        val namePerson: String?,
        val idTipoDocumentoPerson: Int?,
        val numeroDocumentoPerson: String?,
        val rolId: Int?,
        val email: String?,
    ) : OnRegisterCompanyViewModelEvent()

    data class OnValidateCompanyData(
        val idTipoDocumento: Int?,
        val numeroDocumento: String?,
        val departamentId: Int?,
        val townId: Int?,
        val address: String?,
    ) : OnRegisterCompanyViewModelEvent()

}

class RegisterCompanyViewModel @Inject constructor(
    private val transversalDaoUseCase: TransversalDaoUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val registerCompanyUseCase: RegisterCompanyUseCase,
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnRegisterCompanyViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<VisitEntity>()
    val visitData = _visitData.asLiveData()

    private var typeDoc: List<TypeDaoEntity> = emptyList()

    private var typeDocSelected: TypeDaoEntity? = null

    private var typeDocPersonSelected: TypeDaoEntity? = null

    var countries: List<CountriesEntity> = emptyList()
        private set

    var countrySelected: CountriesEntity? = null
        private set

    var department: List<DepartmentEntity> = emptyList()
        private set

    var departmentSelected: DepartmentEntity? = null
        private set

    var town: List<TownEntity> = emptyList()
        private set

    var townSelected: TownEntity? = null
        private set

    private var typeProduct: List<ManageMssEntity> = emptyList()

    private var rol: List<ManageMssEntity> = emptyList()

    private var rolSelected: ManageMssEntity? = null

    private val _doc = MutableLiveData(EMPTY_STRING)
    val doc get() = _doc.asLiveData()

    private val _digit = MutableLiveData(EMPTY_STRING)
    val digit get() = _digit.asLiveData()

    private val _companyName = MutableLiveData(EMPTY_STRING)
    val companyName get() = _companyName.asLiveData()

    private val _comercialName = MutableLiveData(EMPTY_STRING)
    val comercialName get() = _comercialName.asLiveData()

    private val _pageWeb = MutableLiveData(EMPTY_STRING)
    val pageWeb get() = _pageWeb.asLiveData()

    private val _email = MutableLiveData(EMPTY_STRING)
    val email get() = _email.asLiveData()

    private val _phone = MutableLiveData(EMPTY_STRING)
    val phone get() = _phone.asLiveData()

    private val _cellPhone = MutableLiveData(EMPTY_STRING)
    val cellPhone get() = _cellPhone.asLiveData()

    private val _address = MutableLiveData(EMPTY_STRING)
    val address get() = _address.asLiveData()

    private val _personName = MutableLiveData(EMPTY_STRING)
    val personName get() = _personName.asLiveData()

    private val _personDoc = MutableLiveData(EMPTY_STRING)
    val personDoc get() = _personDoc.asLiveData()

    private val _personEmail = MutableLiveData(EMPTY_STRING)
    val personEmail get() = _personEmail.asLiveData()

    val idTipoProducto get() = _visitData.value?.idTipoProducto.orZero()


    fun init(visitData: List<VisitEntity>) = execute {
        _visitData.value = visitData.firstOrNull()
        onGetCountries()
        reloadData()
    }

    fun reloadData() {
        onGetTypeDoc()
        onGetCompany()
        onGetTypeProduct()
        onGetRol()
        setTypeDocSelected(_visitData.value?.empresa?.idTipoDocumento)
        onGetPersonList()
    }

    // obtener company local
    fun onGetCompany() = execute {
        val result = registerCompanyUseCase.loadCompanyLocal(_visitData.value?.id.orZero())
        _event.value = if (result.isSuccess) {
            OnRegisterCompanyViewModelEvent.onViewModelReadyCompany(result.getOrNull())
        } else {
            OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
    }

    // tipos de docuemntos
    fun onGetTypeDoc() = execute {
        val result = transversalDaoUseCase.loadTypeDaoLocal(TIPO_DOCUMENTO).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeDoc = result.orEmpty()
            OnRegisterCompanyViewModelEvent.onViewModelReadyTypeDoc(typeDoc)
        } else {
            OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
    }

    fun setTypeDocSelected(id: Int?): TypeDaoEntity? {
        if (id != null) {
            typeDoc.find { it.id == id }?.let {
                typeDocSelected = it
            }
            return typeDocSelected
        } else {
            typeDocSelected = null
            return null
        }
    }

    fun setTypeDocSelected(itemSelected: String) {
        typeDoc.find { it.descripcion.equals(itemSelected, true) }?.let {
            typeDocSelected = it
        }
    }

    fun setTypeDocPersonSelected(itemSelected: String) {
        typeDoc.find { it.descripcion.equals(itemSelected, true) }?.let {
            typeDocPersonSelected = it
        }
    }

    // Paises
    fun onGetCountries() = execute {
        val resultCountries = transversalDaoUseCase.loadCountriesLocal()
        _event.value = if (resultCountries.isSuccess) {
            countries = resultCountries.getOrNull().orEmpty()
            val countryIdSelected = _visitData.value?.empresa?.direccion?.idPais
            countryIdSelected?.let { countryId ->
                countrySelected = countries.find { it.id == countryId }
                department =
                    transversalDaoUseCase.loadDepartmentLocal(countryId).getOrNull().orEmpty()
                val departamentIdSelected = _visitData.value?.empresa?.direccion?.idDepartamento
                departamentIdSelected?.let { departamentId ->
                    departmentSelected = department.find { it.id == departamentId }
                    town = transversalDaoUseCase.loadTownLocal(departamentId).getOrNull().orEmpty()
                    val cityIdSelected = _visitData.value?.empresa?.direccion?.idMunicipio
                    cityIdSelected?.let { cityId ->
                        townSelected = town.find { it.id == cityId }
                        OnRegisterCompanyViewModelEvent.onViewModelReadyCountries(
                            countries,
                            countrySelected,
                            department,
                            departmentSelected,
                            town,
                            townSelected
                        )
                    } ?: OnRegisterCompanyViewModelEvent.onViewModelReadyCountries(
                        countries,
                        countrySelected,
                        department,
                        departmentSelected
                    )
                } ?: OnRegisterCompanyViewModelEvent.onViewModelReadyCountries(
                    countries,
                    countrySelected,
                    department
                )
            } ?: OnRegisterCompanyViewModelEvent.onViewModelReadyCountries(countries)
        } else {
            OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
    }

    /*
        fun setCountrySelected(id: Int?): CountriesEntity? {
            countrySelected = if (id != null) countries.find { it.id == id } else null
            return countrySelected
        }
    */
    fun setCountrySelected(position: Int): CountriesEntity? {
        countrySelected = try {
            countries.get(position)
        } catch (e: Exception) {
            null
        }
        return countrySelected
    }

    fun setCountrySelected(itemSelected: String) {
        countries.find { it.nombre.equals(itemSelected, true) }?.let {
            countrySelected = it
        }
    }

    // Departamentos
    fun onGetDepartment() = execute {
        val result = transversalDaoUseCase.loadDepartmentLocal(countrySelected?.id.orZero())
        department = result.getOrNull().orEmpty()
        _event.value = OnRegisterCompanyViewModelEvent.onViewModelReadyDepartment(department)
    }

    fun setDepartmentSelected(position: Int): DepartmentEntity? {
        departmentSelected = try {
            department.get(position)
        } catch (e: Exception) {
            null
        }
        return departmentSelected
    }

    fun setDepartmentSelected(itemSelected: String) {
        department.find { it.nombre.equals(itemSelected, true) }?.let {
            departmentSelected = it
        }
    }

    // Municipios
    fun onGetTown() = execute {
        val result = transversalDaoUseCase.loadTownLocal(departmentSelected?.id.orZero())
        town = result.getOrNull().orEmpty()
        _event.value = OnRegisterCompanyViewModelEvent.onViewModelReadyTown(town)
    }

    fun setTownSelected(position: Int): TownEntity? {
        townSelected = try {
            town.get(position)
        } catch (e: Exception) {
            null
        }
        return townSelected
    }

    fun setTownSelected(itemSelected: String) {
        town.find { it.nombre.equals(itemSelected, true) }?.let {
            townSelected = it
        }
    }

    fun onGetTypeProduct() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(
            CD_TYPE_PRODUCT,
            userUseCase.getUserMisional().orZero()
        ).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeProduct = result.orEmpty()
            OnRegisterCompanyViewModelEvent.onViewModelReadyTypeProduct(typeProduct)
        } else {
            OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
        onGetTypeProductSelect()
    }

    fun setTypeProductSelected(id: Int, isSelected: Boolean, update: Boolean = true) {
        typeProduct.find { it.id == id }?.let {
            it.isSelected = isSelected
            if (update) onUpdateTypeProduct(it)
        }
    }

    fun onUpdateTypeProduct(it: ManageMssEntity) = execute {
        val amsspCheck =
            manageMssUseCase.insertOrUpdateTypeProductLocal(it, _visitData.value?.id.toString())
        if (amsspCheck.isFailure) {
            _event.value = OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
    }

    fun onGetTypeProductSelect() = execute {
        val amssp_selected = manageMssUseCase.loadMssAppliedLocal(_visitData.value?.id.toString())
        val result = amssp_selected.getOrNull()
        result?.mapNotNull {
            setTypeProductSelected(
                it.id.orZero(),
                it.isSelected ?: false,
                false
            )
        }
        _event.value = OnRegisterCompanyViewModelEvent.onViewModelReadyTypeProduct(typeProduct)
    }

    fun onGetRol() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(ROL_PERSONA, idTipoProducto).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            rol = result.orEmpty().filter {
                it.id == 0 || it.id == 1 || it.id == 2 || it.id == 5 || it.id == 7 || it.id == 15 || it.id == 16 || it.id == 17 || it.id == 18
            }
            OnRegisterCompanyViewModelEvent.onViewModelReadyRol(rol)
        } else {
            OnRegisterCompanyViewModelEvent.OnLoadingError(null)
        }
    }

    fun setRolSelected(itemSelected: String) {
        rol.find { it.descripcion.equals(itemSelected, true) }?.let {
            rolSelected = it
        }
    }

    fun checkPersonData() {
        val namePerson = _personName.value
        val idTipoDocumentoPerson = typeDocPersonSelected?.id
        val numeroDocumentoPerson = typeDocPersonSelected?.descripcion
        val rolId = rolSelected?.id
        val email = _personEmail.value
        if (namePerson.isNullOrBlank() || numeroDocumentoPerson.isNullOrBlank() || email.isNullOrBlank() || !email.isValidEmail() || rolId == null
            || idTipoDocumentoPerson == null || idTipoDocumentoPerson == 0
        )
            _event.value = OnRegisterCompanyViewModelEvent.OnValidatePersonData(
                namePerson,
                idTipoDocumentoPerson,
                numeroDocumentoPerson,
                rolId,
                email
            )
        else {
            onInsert()
        }
    }

    fun onInsert() = execute {
        val query = registerCompanyUseCase.insertPersonLocal(
            VisitCompanyPersonEntity(
                codigoTipoDocumentoPersona = typeDocPersonSelected?.id,
                tipoDocumento = typeDocPersonSelected?.descripcion,
                numeroDocumentoPersona = _personDoc.value.orEmpty(),
                primerNombre = _personName.value.orEmpty(),
                idRolPersona = rolSelected?.id,
                descripcionRolPersona = rolSelected?.descripcion,
                correoElectronicoP = _personEmail.value.orEmpty()
            ),
            _visitData.value?.id.orZero(),
        )
        _event.value = if (query.isSuccess) {
            OnRegisterCompanyViewModelEvent.onSnack(true, R.string.copy_update_success)
        } else {
            OnRegisterCompanyViewModelEvent.onSnack(false, R.string.error)
        }
        onGetPersonList()
    }

    fun onGetPersonList() = execute {
        val visit = visitData.value
        val result = registerCompanyUseCase.loadPersonLocal(visit?.id.orZero())
        if (result.isSuccess) {
            _event.value =
                OnRegisterCompanyViewModelEvent.onViewModelReadyPerson(result.getOrNull().orEmpty())
        }
    }

    fun onDeletePerson(it: VisitCompanyPersonEntity) = execute {
        val result =
            registerCompanyUseCase.deletePersonLocal(
                it.numeroDocumentoPersona.orEmpty(),
                _visitData.value?.id.orZero()
            )
        if (result.isFailure) {
            _event.value = OnRegisterCompanyViewModelEvent.onSnack(false, R.string.error)
        }
        onGetPersonList()
    }

    fun clean() {
        typeDocSelected = null
        countrySelected = null
        departmentSelected = null
        townSelected = null
        reloadData()
    }

    fun onDocChange(data: Editable?) {
        _doc.value = data.toString()
    }

    fun onDigitChange(data: Editable?) {
        _digit.value = data.toString()
    }

    fun onCompanyNameChange(data: Editable?) {
        _companyName.value = data.toString()
    }

    fun onComercialNameChange(data: Editable?) {
        _comercialName.value = data.toString()
    }

    fun onPageWebChange(data: Editable?) {
        _pageWeb.value = data.toString()
    }

    fun onEmailChange(data: Editable?) {
        _email.value = data.toString()
    }

    fun onTelephoneChange(data: Editable?) {
        _phone.value = data.toString()
    }

    fun onCellphoneChange(data: Editable?) {
        _cellPhone.value = data.toString()
    }

    fun onAddressChange(data: Editable?) {
        _address.value = data.toString()
    }

    fun onPersonNameChange(data: Editable?) {
        _personName.value = data.toString()
    }

    fun onPersonDocChange(data: Editable?) {
        _personDoc.value = data.toString()
    }

    fun onPersonEmailChange(data: Editable?) {
        _personEmail.value = data.toString()
    }

    fun checkCompanyData() {
        val idTipoDocumento = typeDocSelected?.id
        val numeroDocumento = typeDocSelected?.descripcion
        val address = _address.value
        val departamentId = departmentSelected?.id
        val townId = townSelected?.id
        if (address.isNullOrBlank() || numeroDocumento.isNullOrBlank() || departamentId == null || departamentId == 0
            || idTipoDocumento == null || idTipoDocumento == 0 || townId == null || townId == 0
        )
            _event.value = OnRegisterCompanyViewModelEvent.OnValidateCompanyData(
                idTipoDocumento,
                numeroDocumento,
                departamentId,
                townId,
                address
            )
        else {
            onSave()
        }
    }

    fun onSave() = execute {
        val body = RegisterCompanyBodyEntity(
            typeDocSelected?.id.orZero(),
            typeDocSelected?.descripcion.orEmpty(),
            _doc.value.orEmpty(),
            _digit.value.intOrString().orZero(),
            _companyName.value.orEmpty(),
            _comercialName.value.orEmpty(),
            _pageWeb.value.orEmpty(),
            countrySelected?.id.orZero(),
            countrySelected?.nombre.orEmpty(),
            departmentSelected?.id.orZero(),
            departmentSelected?.nombre.orEmpty(),
            townSelected?.id.orZero(),
            townSelected?.nombre.orEmpty(),
            _email.value,
            _address.value,
            _phone.value,
            _cellPhone.value,
            _visitData.value?.empresa?.idSede
        )
        val result =
            registerCompanyUseCase.insertOrUpdateCompanyLocal(body, _visitData.value?.id.orZero())
        _event.value = if (result.isSuccess) {
            OnRegisterCompanyViewModelEvent.onSnack(true, R.string.copy_update_success)
        } else {
            OnRegisterCompanyViewModelEvent.onSnack(false, R.string.error)
        }
    }

    /**
     * Overrides
     */

    override fun onSaveInstanceState() = Bundle().apply {
        _doc.value?.let { putString(KEY_DOC, it) }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        _doc.value = savedInstanceState.getString(KEY_DOC).orEmpty()
    }

}