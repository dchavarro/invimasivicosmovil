package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.technical

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.TechnicalEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentTechnicalBinding
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.TechnicalItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class TechnicalFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentTechnicalBinding>(R.layout.fragment_technical)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: TechnicalViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_close_technical))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter

    }

    private fun observerViewModelEvents() {
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
    }

    private fun eventHandler(event: onTechnicalViewModelEvent) {
        showLoading(false)
        when (event) {
            is onTechnicalViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onTechnicalViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onTechnicalViewModelEvent.OnViewModelReady -> OnViewModelReady(event.technical)
            is onTechnicalViewModelEvent.OnViewModelSuccess -> onSuccess(event.message)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun OnViewModelReady(technicals: List<TechnicalEntity>?) {
        adapter.clear()
        val items = mutableListOf<Group>()
        val isVisible = technicals.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        if (!technicals.isNullOrEmpty()) {
            items.addAll(technicals.map {
                TechnicalItemView(
                    it,
                    ::onCheckSelected
                )
            })
        }
        adapter.updateAsync(items)
    }

    private fun onCheckSelected(technical: TechnicalEntity, isSelected: Boolean) {
        viewModel.setIsSelected(isSelected, technical)
    }

    private fun onSuccess(@StringRes message: Int) {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}