package com.soaint.sivicos_dinamico.flow.act

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.soaint.data.common.CODE_DOCUMENTARY_CLASSIFICATION_EJECU
import com.soaint.data.common.ID_DOCUMENTARY_CLASSIFICATION_EJECU
import com.soaint.data.common.PDF_EXTENSION
import com.soaint.data.common.orZero
import com.soaint.data.utils.getBase64FromPath
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.data.utils.resizeImage
import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.common.calculateHourDifference
import com.soaint.domain.common.comparisonHours
import com.soaint.domain.common.converterDate
import com.soaint.domain.common.deleteFileByPath
import com.soaint.domain.common.getDateNow
import com.soaint.domain.common.hourFormatter
import com.soaint.domain.common.isRangeGreaterThan24Hours
import com.soaint.domain.common.isWeekendDay
import com.soaint.domain.common.orEmpty
import com.soaint.domain.enum.ComparisonResult
import com.soaint.domain.model.*
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.TiposControl.Companion.CHECK_LIST
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.TiposControl.Companion.FIRMA
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.TiposControl.Companion.FOTO
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.TiposControl.Companion.LISTA_TABLA
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CD_REQUERIMIENTOS
import com.soaint.sivicos_dinamico.utils.CODE_RESULT_EQUIPMENT_REEQ_CUMP
import com.soaint.sivicos_dinamico.utils.CODE_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.DESCRIP_RESULT_EQUIPMENT_REEQ_CUMP
import com.soaint.sivicos_dinamico.utils.DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.DIURNA
import com.soaint.sivicos_dinamico.utils.ID_RESULT_EQUIPMENT_REEQ_CUMP
import com.soaint.sivicos_dinamico.utils.ID_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM012
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM015
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM030
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM055
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM156
import com.soaint.sivicos_dinamico.utils.NOCTURNA
import com.soaint.sivicos_dinamico.utils.TH_CDFES
import com.soaint.sivicos_dinamico.utils.TH_CDIU
import com.soaint.sivicos_dinamico.utils.TH_CNFES
import com.soaint.sivicos_dinamico.utils.TH_CNOC
import com.soaint.sivicos_dinamico.utils.TH_DDIU
import com.soaint.sivicos_dinamico.utils.TH_DFES
import com.soaint.sivicos_dinamico.utils.TH_DNOC
import com.soaint.sivicos_dinamico.utils.TH_NFES
import com.soaint.sivicos_dinamico.utils.TH_TOT
import com.soaint.sivicos_dinamico.utils.ZVEQ_OPBE
import com.soaint.sivicos_dinamico.utils.ZVEQ_OPSA
import com.soaint.sivicos_dinamico.utils.ZVEQ_POBE
import com.soaint.sivicos_dinamico.utils.ZVEQ_PROP
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.runBlocking
import javax.inject.Inject
import org.json.JSONArray
sealed class OnActViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnActViewModelEvent()
    data class OnViewModelReady(
        val pdfBase64: String?, val plantilla: PlantillaEntity?, val indVistaPrevia: Boolean
    ) : OnActViewModelEvent()

    data class OnTokenError(
        @StringRes val title: Int = R.string.error,
        @StringRes val message: Int = R.string.copy_generic_error,
        val finished: Boolean = false
    ) : OnActViewModelEvent()

    object OnViewModelDocument : OnActViewModelEvent()
    data class OnGetIdBlockValue(
        val id: Int
    ) : OnActViewModelEvent()

}

class ActViewModel @Inject constructor(
    private val actUseCase: ActUseCase,
    private val userUseCase: UserUseCase,
    private val visitUseCase: VisitUseCase,
    private val plantillasUseCase: PlantillasUseCase,
    private val documentUseCase: DocumentUseCase,
    private val papfUseCase: PapfUseCase,
    val equipmentUseCase: EquipmentUseCase,
) : BaseViewModel() {

    /**
     * Events
     */

    private val _event = SingleMutableLiveData<OnActViewModelEvent>()
    val event = _event.asLiveData()

    private val _map = MutableLiveData(EMPTY_STRING)
    val map = _map.asLiveData()

    val equipo = getEquipments().getOrNull()
    var productos : List<DataReqInsSanPapfEntity?> = emptyList()
    var muestras : List<DataReqActSamplePapfEntity?> = emptyList()
    /**
     * Action
     */

    fun mapSaveBlock(
        idVisita: Int?,
        map: Map<String, String>,
        blocks: List<BlockEntity>,
        idTipoDocumental: Int?,
        indVistaPrevia: Boolean,
        id: Int
    ) = execute {
        val atributes = blocks.mapNotNull { it.atributosPlantilla }.flatten()
        val idPlantilla = blocks.firstOrNull()?.idPlantilla?.toString().orEmpty()
        val body = map.mapNotNull { data ->
            val block = atributes.find { it.nombre == data.key }
            var newValue: Any? = when (block?.idTipoAtributo) {
                FIRMA -> {
                    try {
                        val newImage = resizeImage(data.value)
                        getBase64FromPath(newImage).orEmpty()
                    } catch (e: Exception) {
                        e.printStackTrace()
                        null
                    }
                }
                LISTA_TABLA -> {
                    val newData =
                        GsonBuilder().create()
                            .fromJson<ArrayList<ArrayList<String>>>(data.value, object :
                                TypeToken<ArrayList<ArrayList<String>>>() {}.type)
                    val dataItems = newData
                    dataItems
                }
                FOTO -> {
                    try {
                        val photos = data.value.replace("[", "").replace("]", "").split(", ")
                        val list: ArrayList<String> = arrayListOf()
                        photos.filter { it != "" }
                            .mapNotNull { getBase64FromPath(it)?.let { item -> list.add(item) } }
                        list
                    } catch (e: Exception) {
                        e.printStackTrace()
                        null
                    }
                }
                CHECK_LIST -> {
                    try {
                        val data = data.value.replace("[", "").replace("]", "")
                        data
                    } catch (e: Exception) {
                        e.printStackTrace()
                        null
                    }
                }
                else -> data.value
            }
            block?.let { BlockSaveEntity(block, newValue) }
        }
        val plantilla =
            plantillasUseCase.loadPlantillasLocal(idPlantilla).getOrNull()?.firstOrNull()
        _event.value = OnActViewModelEvent.OnChangeLabelPreloader(R.string.copy_generate_act)

        if (indVistaPrevia) {
            val map = body.map { it.nombre.orEmpty() to it.valor }.toMap().orEmpty()
            getPdfView(plantilla, idVisita.orZero(), true, idTipoDocumental.orZero(), map, id)
        } else {
            saveBlock(plantilla, idVisita.orZero(), idTipoDocumental.orZero(), body, id)
        }
    }

    private fun saveBlock(
        plantilla: PlantillaEntity?,
        idVisita: Int,
        idTipoDocumental: Int,
        block: List<BlockSaveEntity>,
        id: Int
    ) = execute {
        val body = BlockSaveBodyEntity(
            documento = BlockDocumentSaveEntity(
                nombre = plantilla?.nombre.orEmpty(),
                titulo = plantilla?.nombre.orEmpty(),
                usuario = userUseCase.getUserName(),
                fecha = getDateNow(),
                idTipoDocumental = idTipoDocumental
            ), bloques = block
        )
        val result = actUseCase.saveBlock(plantilla?.id.toString(), idVisita.toString(), body)
        saveFinishedActa(idVisita.toString(), plantilla?.id.toString(), id)
        papfUseCase.updateRequerimientoIF(idVisita,extractValue(block))
        if (result.isSuccess) {
        val map = result.map { it.bloques.map { it.nombre.orEmpty() to it.valor }.toMap() }
            .getOrNull().orEmpty()
        getPdfView(plantilla, idVisita, false, idTipoDocumental, map, id)
        } else {
            _event.value = when (result.exceptionOrNull()) {
                is WithoutConnectionException -> OnActViewModelEvent.OnTokenError(
                    R.string.no_internet, R.string.copy_error_without_acta, true
                )
                else -> OnActViewModelEvent.OnTokenError(finished = true)
            }
        }
        createOrUpdateDocument(plantilla, idTipoDocumental, idVisita.toString(), true)
    }

    fun getPdfView(
        plantilla: PlantillaEntity?,
        idVisita: Int,
        indVistaPrevia: Boolean,
        idTipoDocumental: Int,
        map: Map<String, Any?>,
        id: Int
    ) = execute {
        val resultPdf = if (userUseCase.userIsPapf()) {
            actUseCase.getPdfPapf(plantilla?.id.toString(), idVisita, indVistaPrevia, map)
        } else {
            val visita = visitUseCase.loadVisitLocalById(idVisita).getOrNull()
            visita?.let { actUseCase.getPdf(plantilla?.id.toString(), it, indVistaPrevia, map) }
        }

        if (resultPdf?.isSuccess == true) {
           if (userUseCase.userIsPapf()) saveActsRequest(plantilla, idVisita)
            if (!indVistaPrevia) {
                saveFinishedActa(idVisita.toString(), plantilla?.id.toString(), id)
                plantillasUseCase.deleteBlockBody(plantilla?.id.toString(), idVisita.toString(), id)
            }
            _event.value = OnActViewModelEvent.OnViewModelReady(
                resultPdf.getOrNull(), plantilla, indVistaPrevia
            )
        } else {
            _event.value = OnActViewModelEvent.OnTokenError(
                title = R.string.copy_finished,
                message = R.string.copy_error_without_acta, finished = true
            )
        }

        //Update Document Firmado
        createOrUpdateDocument(plantilla, idTipoDocumental, idVisita.toString(), true)
    }

    suspend fun saveActsRequest(plantilla: PlantillaEntity?, idVisita: Int){
        val clasificationTramit =  papfUseCase.loadDataCloseInsLocal(idVisita).getOrNull()
        val data = SaveInfoActsPapfEntity(
            idPlantilla = plantilla?.id,
            idSolicitud = idVisita.toString(),
            expedicionCIS = "",
            otros = "",
            cual = "",
            consecutivo = papfUseCase.selectConsecutivePapfById(idVisita).getOrNull(),
            fechaRegistro = "",
            funcionarioInvima = userUseCase.fullNameOrdered(),
            fechaInicio = getDateNow(),
            fechaFin = getDateNow(),
            tramiteRequerimiento =if (clasificationTramit?.respuestaRequerimientoIF == true) 1 else 0,
            descripcionRequerimiento = "",
            nombreUsuario = userUseCase.fullNameOrdered(),
            nombreLaboratorio = ""
        )
        papfUseCase.saveActsRequest(data)
    }

    fun saveActa(
        blocks: List<BlockEntity>,
        map: Map<String, String>,
        idVisita: String,
        idPlantilla: String,
        idBlockValue: Int
    ) = execute {
        val encodedMapForCheckG = encodeMap4CheckG(blocks, map)

        val serializedMap = Gson().toJson(encodedMapForCheckG)

        plantillasUseCase.saveBlockBody(idPlantilla, idVisita, serializedMap, idBlockValue)
        setIdBlockValue(idPlantilla, idVisita)
    }

    fun updateEquipment(newMap: Map<String, String>) = runBlocking {
        equipmentUseCase.updateEquipmentBatch(getEquipmentUpdates(newMap))
    }
    fun getConsecutivo(idVisita: Int) = runBlocking {
        papfUseCase.selectConsecutivePapfById(idVisita).getOrNull()
    }

    fun selectProductInspSanitary(idSolicitud: Int, codigo: String) = runBlocking {
        if(codigo in setOf(IVC_INS_FM055, IVC_INS_FM156)){
            productos = papfUseCase.selectProductInspSanitary(idSolicitud).getOrNull()?.let { it } ?: emptyList()
        }else{
            muestras = papfUseCase.selectProductActSample(idSolicitud).getOrNull()?.let { it } ?: emptyList()
        }

    }
    fun extractValue(list: List<BlockSaveEntity>): Boolean? {
        val element = list.find { it.nombre == "tramite" || it.nombre == "tramiten" || it.nombre == "tramitesn"}
        return element?.valor?.toString()?.let { value ->
            when (value) {
                "Si" -> true
                "No" -> false
                else -> null
            }
        }
    }
    fun setIdBlockValue(idPlantilla: String, idVisita: String){
        _event.value = OnActViewModelEvent.OnGetIdBlockValue(
            getBlockBodyUnfinished(idPlantilla, idVisita).getOrNull()?.id.orEmpty()
        )
    }
    fun getActa(body: String): Map<String, String> = runBlocking {
        try {
            val map = HashMap<String, String>()
            Gson().fromJson(body, map.javaClass).orEmpty()
        } catch (e: Exception) {
            mapOf<String, String>()
        }
    }

    fun getBlockBodyUnfinished( idPlantilla: String, idVisita: String) = runBlocking  {
        plantillasUseCase.selectBodyBlockUnfinished(idPlantilla,idVisita)
    }

    fun createOrUpdateDocument(
        plantilla: PlantillaEntity?,
        idTipoDocumental: Int?,
        idVisit: String?,
        indDocumentoFirmado: Boolean
    ) = execute {
        val body = DocumentEntity(
            descripcionTipoDocumental = "-",
            descripcionClasificacionDocumental = CODE_DOCUMENTARY_CLASSIFICATION_EJECU,
            detalle = "Versión ${plantilla?.version}",
            descripcionDocumento = plantilla?.descripcion,
            usuarioCrea = userUseCase.getUserInformation()?.usuario.toString(),
            clasificacionDocumental = ID_DOCUMENTARY_CLASSIFICATION_EJECU,
            nombreDocumento = plantilla?.nombre,
            fechaDocumento = getDateHourWithOutFormat(),
            nombreTipoDocumental = "-",
            idTipoDocumental = idTipoDocumental!!,
            idPlantilla = plantilla?.id?.toInt(),
            indDocumentoFirmado = indDocumentoFirmado,
            numeroFolios = "-",
            codigoPlantilla = plantilla?.codigo,
            extension = PDF_EXTENSION
        )
        val result = documentUseCase.addPLantilla2DocumentLocal(body, idVisit.toString())
        _event.value = if (result.isSuccess) {
            OnActViewModelEvent.OnViewModelDocument
        } else {
            OnActViewModelEvent.OnTokenError()
        }
    }

    fun getBlockValue(
        idPlantilla: String, idVisita: String, idBloque: String, idBloqueAtributo: String
    ): String? = runBlocking {
        actUseCase.getBlockValueLocal(idPlantilla, idVisita, idBloque, idBloqueAtributo).getOrNull()
            ?.let { block ->
                if (block.valor is String) {
                    block.valueString.toString().orEmpty()
                } else if (block.valor is ArrayList<*>) {
                    val valor = block.valor as? ArrayList<ArrayList<String>>
                    if (valor == null) {
                        try {
                            Gson().toJson(block.valueArray)
                        } catch (e: Exception) {
                            EMPTY_STRING
                        }
                    } else {
                        try {
                            Gson().toJson(block.valueArrayArray)
                        } catch (e: Exception) {
                            EMPTY_STRING
                        }
                    }
                } else {
                    null
                }
            }
    }

    /**
     * Función para cambiar el valor de un atributo según ciertas condiciones y valores obtenidos desde la base de datos.
     *
     * @param attributeName El nombre del atributo.
     * @param valueActual El valor actual del atributo.
     * @param plantilla
     * @return El nuevo valor del atributo o el valor actual si no se cumple ninguna condición.
     */
    fun changeValue(attributeName: String?, valueActual: String?, plantilla: List<PlantillaEntity>, consecutivo: String = ""): String? {
        val codigo = plantilla.firstOrNull()?.codigo
        val attributeMap = mutableMapOf<String, () -> String>(
            "nombre1" to ::getFullName,
            "cargo1" to ::getJobTitle
        )

        // Verifica si el código de la plantilla es "IVC-INS-FM030"
        if (codigo.equals(IVC_INS_FM030)) {
            // Mapa que relaciona los atributos "super" con sus respectivos códigos en la base de datos
            val attributeCodes = mapOf(
                "super1" to ZVEQ_POBE,
                "super2" to ZVEQ_OPBE,
                "super3" to ZVEQ_PROP,
                "super4" to ZVEQ_OPSA
            )

            // Asigna los valores de los atributos "super" según los códigos obtenidos de la base de datos
            for ((attributeNameSuper, code) in attributeCodes) {
                // Busca en la lista de entidades la que tenga el código correspondiente y obtiene el valor deseado
                val attributeValue = equipo?.find { it.codigoZonaVerificar == code }?.equipo
                attributeMap[attributeNameSuper] = { attributeValue ?: "" }
            }
        }

        // Verifica si el código de la plantilla es "IVC_INS_FM055"
        if (codigo in setOf(IVC_INS_FM055, IVC_INS_FM156, IVC_INS_FM012)) {
            if (attributeName.equals("productos")) {
                if (codigo.equals(IVC_INS_FM012)){
                    attributeMap["productos"] = { updateProductJsonWithDatabaseData(valueActual.toString(), true) }
                }else{
                    attributeMap["productos"] = { updateProductJsonWithDatabaseData(valueActual.toString(), false) }
                }

            }
        }

        if (codigo in setOf(IVC_INS_FM055, IVC_INS_FM156)) {
            attributeMap["acta"] = {consecutivo}
        }

        // Retorna el valor del atributo solicitado o el valor actual si no se cumple ninguna condición
        return attributeMap[attributeName?.lowercase()]?.invoke() ?: valueActual
    }
    fun deleteValueBefore(
        attributeName: String?,
        valueActual: String?,
        plantilla: List<PlantillaEntity>
    ): String? {
        var newValue = valueActual

        //se eliminan los datos
        if (plantilla.firstOrNull()?.codigo.equals(IVC_INS_FM030)) {
            val valoresAEliminar = setOf(
                "observacion5",
                "observacion4",
                "observacion7",
                "observacion8",
                "super1",
                "super2",
                "super3",
                "super4"
            )
            if (attributeName in valoresAEliminar) {
                newValue = ""
            }
        }
        return newValue
    }


    fun deleteValueAfter(
        attributeName: String?,
        valueActual: String?,
        plantilla: List<PlantillaEntity>
    ): String? {
        var newValue = valueActual
        // se elimina la ultima posiciomn de cada array debido a que no debe mostrarse, pero
        // el campo es necesario para validaciones internas
        val codigo = plantilla.firstOrNull()?.codigo
        if (codigo in setOf(IVC_INS_FM055, IVC_INS_FM156, IVC_INS_FM012)) {
            if (attributeName.equals("productos")) {
                val gson = Gson()
                val listType = object : TypeToken<List<List<String>>>() {}.type
                val jsonArray: List<List<String>> = gson.fromJson(valueActual, listType)
                val modifiedArray = jsonArray.map { sublist ->
                    sublist.dropLast(1)
                }
                newValue = gson.toJson(modifiedArray)
            }
        }
        return newValue
    }

    fun deleteFile(map: Map<String, Any?>, block: List<BlockSaveEntity>) {
        block.mapNotNull {
            map.mapNotNull { data ->
                if (it.nombre == data.key && it.idTipoAtributo == FIRMA) {
                    deleteFileByPath(data.value as String)
                }
            }
        }
    }

    fun isEditableActa(idVisita: String, idPlantilla: String, id: Int): Boolean = runBlocking {
        plantillasUseCase.isEditableActa(idPlantilla, idVisita, id) == true
    }

    suspend fun saveFinishedActa(idVisita: String, idPlantilla: String, id: Int) {
        plantillasUseCase.saveFinishedActa(idPlantilla, idVisita, id)
    }

    fun encodeMap4CheckG(blocks: List<BlockEntity>, map: Map<String, String>): Map<String, String> {
        val mapChecks = map.filterKeys { it.startsWith("checkG") }
        val newMap = map.filterNot { it.key.startsWith("checkG") }.toMutableMap()

        val checks = blocks.mapNotNull { it.atributosPlantilla }.flatten()
            .filter { it.isCheckRadioButton }
            .groupBy { it.nombre?.dropLast(1) }

        mapChecks.onEach { check ->
            val actualKey = check.key.dropLast(1)
            val actualChecks = checks[actualKey]?.firstOrNull()
            val attributes = actualChecks?.configuracion?.lista
            val dataCheck = attributes?.find { it.nombre == check.value }
            val checkId = dataCheck?.id
            val currentIndex = attributes?.indexOfFirst { it.id == checkId }

            attributes?.onEachIndexed { index, att ->
                val data = if (currentIndex == index) "X" else ""
                newMap.put(actualKey + att.id?.toInt()?.orZero(), data)
            }
        }
        return newMap
    }

    fun decodeMap4CheckG(map: Map<String, String>): Map<String, String> {
        val newMap =
            map.filterNot { it.key.startsWith("checkG") }.toMutableMap() // Todos menos los checkG
        val mapChecks = map.filterKeys { it.startsWith("checkG") }
            .filterValues { it.isNotBlank() }// Todos los checkG

        mapChecks.onEach { check ->
            val actualKey = check.key.dropLast(1)
            val valueSelected = check.value
            newMap.put(actualKey + "1", valueSelected)
        }
        return newMap
    }

    suspend fun getRequerimientos(): List<DinamicQuerysPapfEntity>? {
        // show loading
        val data = papfUseCase.loadDinamicQuerysLocal(CD_REQUERIMIENTOS).getOrNull()
        // hidden loading
        return data
    }

    fun getFullName(): String {
      return userUseCase.fullNameOrdered();
    }
    fun getJobTitle(): String {
        return userUseCase.getJobTitle()
    }

     fun getEquipments() : Result<List<EquipmentEntity>?> = runBlocking {
        equipmentUseCase.selectEquipmentUnfinished()
    }

    /**
     * Para las actas IVC-INS-FM030 Obtiene los campos que se van actualizar de los equipos.
     *
     * @param jsonObject Objeto JSON que contiene los datos de actualización de los equipos.
     * @return Lista de entidades `EquipmentUpdateEntity` con las actualizaciones de los equipos.
     */
    fun getEquipmentUpdates(jsonObject: Map<String, String>): List<EquipmentUpdateEntity> {
        // Mapa que define los campos de verificación y las correspondientes constantes
        val checks = mapOf(
            "checkGo1" to Pair(ZVEQ_POBE, "observacion4"),
            "checkGo2" to Pair(ZVEQ_POBE,"observacion4"),
            "checkGp1" to Pair(ZVEQ_OPBE, "observacion5"),
            "checkGp2" to Pair(ZVEQ_OPBE, "observacion5"),
            "checkGq1" to Pair(ZVEQ_PROP, "observacion7"),
            "checkGq2" to Pair(ZVEQ_PROP, "observacion7"),
            "checkGr1" to Pair(ZVEQ_OPSA, "observacion8"),
            "checkGr2" to Pair(ZVEQ_OPSA, "observacion8")
        )

        val equipmentUpdates = mutableListOf<EquipmentUpdateEntity>()

        // Mapa que define los campos de verificación y las correspondientes constantes
        for ((checkField, pair) in checks) {
            val (codigo, observacionField) = pair
            val checkValue = jsonObject[checkField]
            if (!checkValue.isNullOrEmpty()) {
                val idResultado = if (checkField.endsWith("1")) ID_RESULT_EQUIPMENT_REEQ_CUMP else ID_RESULT_EQUIPMENT_REEQ_NOCU
                val codigoResultado = if (idResultado == 1) CODE_RESULT_EQUIPMENT_REEQ_CUMP else CODE_RESULT_EQUIPMENT_REEQ_NOCU
                val descripcionResultado = if (idResultado == 1) DESCRIP_RESULT_EQUIPMENT_REEQ_CUMP else DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU
                val observacion = jsonObject[observacionField]

                val equipment = equipo?.firstOrNull { it.codigoZonaVerificar == codigo }

                val equipmentUpdate = EquipmentUpdateEntity(equipment?.id, codigo, idResultado, codigoResultado, descripcionResultado, observacion, equipment?.isLocal, equipment?.isUpdate)
                equipmentUpdates.add(equipmentUpdate)
            }
        }

        return equipmentUpdates
    }
    private fun updateProductJsonWithDatabaseData(json: String, isMuestras: Boolean): String {
        val jsonArray = JSONArray(json)

        val databaseData = if (isMuestras) muestras else productos

        val headersArray = jsonArray.getJSONArray(0)
        val headersMap = mutableMapOf<String, Int>()
        for (i in 0 until headersArray.length()) {
            headersMap[headersArray.getString(i)] = i
        }

        for (i in 1 until jsonArray.length()) {
            val rowArray = jsonArray.getJSONArray(i)
            val detalleProducto =
                rowArray.optString(headersMap["Detalle Producto"] ?: continue)?.toIntOrNull()

            val matchingData = databaseData.find { (it as? DataReqInsSanPapfEntity)?.detalleProducto == detalleProducto || (it as? DataReqActSamplePapfEntity)?.idProducto == detalleProducto }

            matchingData?.let { data ->
                if (isMuestras) {
                    headersMap["No U M"]?.let { index ->
                        rowArray.put(index, (data as DataReqActSamplePapfEntity).unidadesMedida)
                    }
                    headersMap["Presentación"]?.let { index ->
                        rowArray.put(index, (data as DataReqActSamplePapfEntity).descripcionPresentacion)
                    }
                    headersMap["Contenido neto gr o cc por unidad"]?.let { index ->
                        rowArray.put(index, (data as DataReqActSamplePapfEntity).contenidoNeto)
                    }
                } else {
                    headersMap["Condiciones de almacenamiento y conservación"]?.let { index ->
                        rowArray.put(index, (data as DataReqInsSanPapfEntity).condicionesAlmacenamiento)
                    }
                    headersMap["Observaciones"]?.let { index ->
                        rowArray.put(index, (data as DataReqInsSanPapfEntity).observaciones)
                    }
                    headersMap["Estado Empaque"]?.let { index ->
                        rowArray.put(index, (data as DataReqInsSanPapfEntity).descripcionEmpaque)
                    }
                }
            }
        }
        return jsonArray.toString()
    }
     fun validateHours(idVisita: String, jsonObject: Map<String, String>): Pair<Boolean,String?>  = runBlocking {
       val fechaInicioJornada = converterDate(jsonObject.get("fechaInicio").toString())
       val fechaFinJornada =  converterDate(jsonObject.get("fechaFin").toString())
       val horaInicio = hourFormatter(jsonObject.get("horaini").toString())
       val horaFin = hourFormatter(jsonObject.get("horafIna").toString())
       val idVisita = idVisita
       val usuario = getFullName()
       val horaFinDia = "23:59:00"
       val horainicioDia = "00:00:00"
       var horasNocturnas = 0
       var horasDiurnas = 0
       var horasNocturnasDia1 = 0
       var horasDiurnasDia1 = 0
       var horasNocturnasDia2 = 0
       var horasDiurnasDia2 = 0
       var horasSuma = 0.0
       var horasDisponibilidad = 0.0
       var horasDiferencia = 0.0
       var Ejecutar: Int = 0
       val inicioHorarioDiurno = visitUseCase.getJordana(DIURNA)
       val inicioHorarioNocturno = visitUseCase.getJordana(NOCTURNA)
       val existeFechaEvento = visitUseCase.existsEventDate(fechaInicioJornada)

       var response: Pair<Boolean, String?> = Pair(false, "Ocurrió un error")
       val idTramite = visitUseCase.getIdTramite(idVisita.toInt().orEmpty())
       val isRangeGreaterThan24Hours = isRangeGreaterThan24Hours(fechaInicioJornada, horaInicio, fechaFinJornada, horaFin)
       if (idTramite != null) {
           if (idTramite > 0){
               //si no es rango mayor a 24 horas
               if (!isRangeGreaterThan24Hours) {
                   visitUseCase.saveScheduleTemp(idTramite)
                   if (fechaInicioJornada == fechaFinJornada) {
                       if (comparisonHours(
                               horaInicio.toString(),
                               inicioHorarioDiurno.toString()
                           ) in listOf(ComparisonResult.MENOR, ComparisonResult.IGUAL)
                       ) {
                           horasNocturnas = horasNocturnas + calculateHourDifference(
                               horaInicio.toString(),
                               inicioHorarioDiurno.toString()
                           ).coerceAtLeast(0)
                           if (comparisonHours(
                                   horaFin.toString(),
                                   inicioHorarioNocturno.toString()
                               ) == (ComparisonResult.MENOR)
                           ) {
                               horasDiurnas = horasDiurnas + calculateHourDifference(
                                   inicioHorarioDiurno.toString(),
                                   horaFin.toString()
                               ).coerceAtLeast(0)
                           } else {
                               horasDiurnas = horasDiurnas + calculateHourDifference(
                                   inicioHorarioDiurno.toString(),
                                   inicioHorarioNocturno.toString()
                               ).coerceAtLeast(0)
                               horasNocturnas = horasNocturnas + calculateHourDifference(
                                   inicioHorarioNocturno.toString(),
                                   inicioHorarioNocturno.toString()
                               ).coerceAtLeast(0)
                           }
                       } else {
                           horasDiurnas = calculateHourDifference(
                               horaInicio.toString(),
                               horaFin.toString()
                           ).coerceAtLeast(0) - calculateHourDifference(
                               inicioHorarioNocturno.toString(),
                               horaFin.toString()
                           ).coerceAtLeast(0)
                           if (comparisonHours(
                                   horaFin.toString(),
                                   inicioHorarioNocturno.toString()
                               ) == (ComparisonResult.MAYOR)
                           ) {
                               horasNocturnas = horasNocturnas + calculateHourDifference(
                                   inicioHorarioNocturno.toString(),
                                   horaFin.toString()
                               ).coerceAtLeast(0)
                           }
                       }

                       if (!isWeekendDay(fechaInicioJornada) && !existeFechaEvento) {
                           horasSuma =
                               (horasDiurnas.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                   TH_CDIU
                               ).orEmpty())
                           horasDisponibilidad =
                               visitUseCase.getQuantityHoursByCode(TH_DDIU).orEmpty()
                           Ejecutar = 1
                           if (horasSuma >= horasDisponibilidad) {
                               horasDiferencia =
                                   (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                       TH_CDIU
                                   ).orEmpty())
                               response = Pair(
                                   false,
                                   "Las horas diurnas a registrar más las consumidas superan la disponibilidad de horas diurnas, cuenta con ${horasDiferencia * 60} minutos."
                               )
                               Ejecutar = 0
                           } else {
                               response = Pair(true, "")
                           }

                           if (Ejecutar == 1) {
                               horasSuma =
                                   (horasNocturnas.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CNOC
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_DNOC).orEmpty()
                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       horasDisponibilidad - -visitUseCase.getQuantityHoursByCode(
                                           TH_CNOC
                                       ).orEmpty()
                                   response = Pair(
                                       false,
                                       "Las horas nocturnas a registrar mas las consumidas superan la disponibilidad de horas nocturnas, cuenta con ${horasDiferencia * 60} minutos."
                                   )

                               } else {
                                   response = Pair(true, "")
                               }
                           }
                       } else {
                           horasSuma =
                               (horasDiurnas.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                   TH_CDFES
                               ).orEmpty())
                           horasDisponibilidad =
                               visitUseCase.getQuantityHoursByCode(TH_DFES).orEmpty()
                           Ejecutar = 1

                           if (horasSuma >= horasDisponibilidad) {
                               horasDiferencia =
                                   (horasDisponibilidad - (visitUseCase.getQuantityHoursByCode(
                                       TH_CDFES
                                   ).orEmpty()))
                               response = Pair(
                                   false,
                                   "Las horas diurnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas diurnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                               )
                               Ejecutar = 0
                           } else {
                               response = Pair(true, "")
                           }

                           if (Ejecutar == 1) {
                               horasSuma =
                                   (horasNocturnas.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CNFES
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_NFES).orEmpty()

                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                           TH_CNFES
                                       ).orEmpty())
                                   response = Pair(
                                       false,
                                       "'Las horas nocturnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas nocturnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                                   )
                               } else {
                                   response = Pair(true, "")
                               }

                           }

                       }
                   }
                   else
                   {
                    if (fechaInicioJornada < fechaFinJornada) {

                           if (comparisonHours(
                                   horaInicio.toString(),
                                   inicioHorarioNocturno.toString()
                               ) in listOf(ComparisonResult.MENOR)
                           ) {
                               if (comparisonHours(
                                       horaInicio.toString(),
                                       inicioHorarioDiurno.toString()
                                   ) in listOf(ComparisonResult.MAYOR, ComparisonResult.IGUAL)
                               ) {
                                   horasDiurnasDia1 = horasDiurnasDia1 + calculateHourDifference(
                                       horaInicio.toString(),
                                       inicioHorarioNocturno.toString()
                                   ).coerceAtLeast(0)
                                   horasNocturnasDia1 = calculateHourDifference(
                                       inicioHorarioNocturno.toString(),
                                       horaFinDia.toString()
                                   ).coerceAtLeast(0) + 1
                               } else {
                                   if (comparisonHours(
                                           horaInicio.toString(),
                                           inicioHorarioDiurno.toString()
                                       ) in listOf(ComparisonResult.MENOR)
                                   ) {
                                       horasNocturnasDia1 =
                                           horasNocturnasDia1 + +calculateHourDifference(
                                               horaInicio.toString(),
                                               inicioHorarioDiurno.toString()
                                           ).coerceAtLeast(0)
                                       horasDiurnasDia1 =
                                           horasDiurnasDia1 + calculateHourDifference(
                                               inicioHorarioDiurno.toString(),
                                               inicioHorarioNocturno.toString()
                                           ).coerceAtLeast(0)
                                       horasNocturnasDia1 =
                                           horasNocturnasDia1 + calculateHourDifference(
                                               inicioHorarioNocturno.toString(),
                                               horaFinDia.toString()
                                           ).coerceAtLeast(0) + 1

                                   }
                               }
                           } else {
                               if (comparisonHours(
                                       horaInicio.toString(),
                                       inicioHorarioNocturno.toString()
                                   ) in listOf(ComparisonResult.MAYOR, ComparisonResult.IGUAL)
                               ) {
                                   horasNocturnasDia1 =
                                       horasNocturnasDia1 + calculateHourDifference(
                                           horaInicio.toString(),
                                           horaFinDia.toString()
                                       ).coerceAtLeast(0) + 1
                               }
                           }
                           if (comparisonHours(
                                   horaFin.toString(),
                                   inicioHorarioDiurno.toString()
                               ) in listOf(ComparisonResult.MAYOR)
                           ) {
                               horasNocturnasDia2 = horasNocturnasDia2 + calculateHourDifference(
                                   horainicioDia.toString(),
                                   inicioHorarioDiurno.toString()
                               ).coerceAtLeast(0)
                               if (comparisonHours(
                                       horaFin.toString(),
                                       inicioHorarioNocturno.toString()
                                   ) in listOf(ComparisonResult.MAYOR)
                               ) {
                                   horasDiurnasDia2 = horasDiurnasDia2 + calculateHourDifference(
                                       inicioHorarioDiurno.toString(),
                                       inicioHorarioNocturno.toString()
                                   ).coerceAtLeast(0)
                                   horasNocturnasDia2 =
                                       horasNocturnasDia2 + calculateHourDifference(
                                           inicioHorarioNocturno.toString(),
                                           horaFin.toString()
                                       ).coerceAtLeast(0)
                               } else {
                                   horasDiurnasDia2 = horasDiurnasDia2 + calculateHourDifference(
                                       inicioHorarioDiurno.toString(),
                                       horaFin.toString()
                                   ).coerceAtLeast(0)
                               }
                           } else {
                               horasNocturnasDia2 = horasNocturnasDia2 + calculateHourDifference(
                                   horainicioDia.toString(),
                                   horaFin.toString()
                               ).coerceAtLeast(0)
                           }

                           if (!isWeekendDay(fechaInicioJornada) && !existeFechaEvento) {
                               horasSuma =
                                   (horasDiurnasDia1.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CDIU
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_DDIU).orEmpty()
                               Ejecutar = 1
                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                           TH_CDIU
                                       ).orEmpty())
                                   response = Pair(
                                       false,
                                       "Las horas diurnas a registrar mas las consumidas superan la disponibilidad de horas diurnas, cuenta con ${horasDiferencia * 60} minutos."
                                   )
                                   Ejecutar = 0
                               } else {
                                   response = Pair(true, "")
                               }

                               if (Ejecutar == 1) {
                                   horasSuma =
                                       (horasNocturnasDia1.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                           TH_CNOC
                                       ).orEmpty())
                                   horasDisponibilidad =
                                       visitUseCase.getQuantityHoursByCode(TH_DNOC).orEmpty()
                                   if (horasSuma >= horasDisponibilidad) {
                                       horasDiferencia =
                                           (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                               TH_CNOC
                                           ).orEmpty())
                                       response = Pair(
                                           false,
                                           "Las horas nocturnas a registrar mas las consumidas superan la disponibilidad de horas nocturnas, cuenta con ${horasDiferencia * 60} minutos."
                                       )
                                   } else {
                                       response = Pair(true, "")
                                   }
                               }
                           } else {
                               horasSuma =
                                   (horasDiurnasDia1.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CDFES
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_DFES).orEmpty()
                               Ejecutar = 1

                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                           TH_CDFES
                                       ).orEmpty())
                                   response = Pair(
                                       false,
                                       "Las horas diurnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas diurnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                                   )
                                   Ejecutar = 0
                               } else {
                                   response = Pair(true, "")
                               }

                               if (Ejecutar == 1) {
                                   horasSuma =
                                       (horasNocturnasDia1.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                           TH_CNFES
                                       ).orEmpty())
                                   horasDisponibilidad =
                                       visitUseCase.getQuantityHoursByCode(TH_NFES).orEmpty()
                                   if (horasSuma >= horasDisponibilidad) {
                                       horasDiferencia =
                                           (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                               TH_CNFES
                                           ).orEmpty())
                                       response = Pair(
                                           false,
                                           "Las horas nocturnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas nocturnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                                       )
                                   } else {
                                       response = Pair(true, "")
                                   }

                               }
                           }

                           if (!isWeekendDay(fechaFinJornada) && !existeFechaEvento) {
                               horasSuma =
                                   (horasDiurnasDia2.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CDIU
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_DDIU).orEmpty()
                               Ejecutar = 1
                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                           TH_CDIU
                                       ).orEmpty()
                                   response = Pair(
                                       false,
                                       "Las horas diurnas a registrar mas las consumidas superan la disponibilidad de horas diurnas, cuenta con ${horasDiferencia * 60} minutos."
                                   )
                                   Ejecutar = 0
                               } else {
                                   response = Pair(true, "")
                               }

                               if (Ejecutar == 1) {
                                   horasSuma =
                                       (horasNocturnasDia2.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                           TH_CNOC
                                       ).orEmpty())
                                   horasDisponibilidad =
                                       visitUseCase.getQuantityHoursByCode(TH_DNOC).orEmpty()
                                   if (horasSuma >= horasDisponibilidad) {
                                       horasDiferencia =
                                           horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                               TH_CNOC
                                           ).orEmpty()
                                       response = Pair(
                                           false,
                                           "Las horas nocturnas a registrar mas las consumidas superan la disponibilidad de horas nocturnas, cuenta con ${horasDiferencia * 60} minutos."
                                       )
                                   } else {
                                       response = Pair(true, "")
                                   }
                               }

                           } else {
                               horasSuma =
                                   (horasDiurnasDia2.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                       TH_CDFES
                                   ).orEmpty())
                               horasDisponibilidad =
                                   visitUseCase.getQuantityHoursByCode(TH_CDFES).orEmpty()
                               Ejecutar = 1
                               if (horasSuma >= horasDisponibilidad) {
                                   horasDiferencia =
                                       horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                           TH_CDFES
                                       ).orEmpty()
                                   response = Pair(
                                       false,
                                       "Las horas diurnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas diurnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                                   )
                                   Ejecutar = 0
                               } else {
                                   response = Pair(true, "")
                               }

                               if (Ejecutar == 1) {
                                   horasSuma =
                                       (horasNocturnasDia2.toDouble() / 60.0) + (visitUseCase.getQuantityHoursByCode(
                                           TH_CNFES
                                       ).orEmpty())
                                   horasDisponibilidad =
                                       visitUseCase.getQuantityHoursByCode(TH_NFES).orEmpty()
                                   if (horasSuma >= horasDisponibilidad) {
                                       horasDiferencia =
                                           (horasDisponibilidad - visitUseCase.getQuantityHoursByCode(
                                               TH_CNFES
                                           ).orEmpty())
                                       response = Pair(
                                           false,
                                           "Las horas nocturnas domingos y festivos a registrar mas las consumidas superan la disponibilidad de horas nocturnas domingos y festivos, cuenta con ${horasDiferencia * 60} minutos."
                                       )
                                   } else {
                                       response = Pair(true, "")
                                   }
                               }
                           }
                       }
                    else
                    {
                           response = Pair(false, "La fecha de inicio no puede ser mayor a fecha de finalizacion.")
                       }
                   }
               }else{
                   response = Pair(false, "El rango de fechas supera las 24 horas.")
               }
           }
       }
         visitUseCase.deleteAllScheduleTemp()
         return@runBlocking response
   }
     fun insertHours(idVisita: String, jsonObject: Map<String, String>)  = execute {
        val fechaInicioJornada = converterDate(jsonObject.get("fechaInicio").toString())
        val fechaFinJornada =  converterDate(jsonObject.get("fechaFin").toString())
        val horaInicio = hourFormatter(jsonObject.get("horaini").toString())
        val horaFin = hourFormatter(jsonObject.get("horafIna").toString())
        val idVisita = idVisita
        val usuario = getFullName()
        val horaFinDia = "23:59:00"
        val horainicioDia = "00:00:00"
        var horasNocturnas = 0
        var horasDiurnas = 0
        var horasNocturnasDia1 = 0
        var horasDiurnasDia1 = 0
        var horasNocturnasDia2 = 0
        var horasDiurnasDia2 = 0
        val inicioHorarioDiurno = visitUseCase.getJordana(DIURNA)
        val inicioHorarioNocturno = visitUseCase.getJordana(NOCTURNA)
        val existeFechaEvento = visitUseCase.existsEventDate(fechaInicioJornada)

        val idTramite = visitUseCase.getIdTramite(idVisita.toInt().orEmpty())

        if (idTramite != null) {
            if (idTramite > 0 ){
                if(fechaInicioJornada == fechaFinJornada){
                    if (comparisonHours(horaInicio.toString(), inicioHorarioDiurno.toString()) in listOf(ComparisonResult.MENOR, ComparisonResult.IGUAL)){
                        horasNocturnas = horasNocturnas + calculateHourDifference(horaInicio.toString(), inicioHorarioDiurno.toString()).coerceAtLeast(0)
                        if (comparisonHours(horaFin.toString() , inicioHorarioNocturno.toString()) == (ComparisonResult.MENOR)){
                            horasDiurnas = horasDiurnas + calculateHourDifference(inicioHorarioDiurno.toString(), horaFin.toString()).coerceAtLeast(0)
                        }else{
                            horasDiurnas = horasDiurnas + calculateHourDifference(inicioHorarioDiurno.toString(), inicioHorarioNocturno.toString()).coerceAtLeast(0)
                            horasNocturnas = horasNocturnas + calculateHourDifference(inicioHorarioNocturno.toString(), inicioHorarioNocturno.toString()).coerceAtLeast(0)
                        }
                    }else{
                        horasDiurnas = calculateHourDifference(horaInicio.toString(), horaFin.toString()).coerceAtLeast(0) - calculateHourDifference(inicioHorarioNocturno.toString(), horaFin.toString()).coerceAtLeast(0)
                        if (comparisonHours(horaFin.toString() , inicioHorarioNocturno.toString()) == (ComparisonResult.MAYOR)){
                            horasNocturnas = horasNocturnas + calculateHourDifference(inicioHorarioNocturno.toString(), horaFin.toString()).coerceAtLeast(0)
                        }
                    }

                    if(!isWeekendDay(fechaInicioJornada) && !existeFechaEvento){

                        val listSchedule = mutableListOf(
                            ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnas + horasNocturnas).toDouble()),
                            ScheduleEntity(idTramite, 7, visitUseCase.getTypeHoursBilling(7).codigo, visitUseCase.getTypeHoursBilling(7).descripcion, horasDiurnas.toDouble()),
                            ScheduleEntity(idTramite, 8,visitUseCase.getTypeHoursBilling(8).codigo, visitUseCase.getTypeHoursBilling(8).descripcion, horasNocturnas.toDouble())
                        )
                        visitUseCase.saveSchedule(listSchedule)
                    }else{
                        val listSchedule = mutableListOf(
                        ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnas + horasNocturnas).toDouble()),
                        ScheduleEntity(idTramite, 9, visitUseCase.getTypeHoursBilling(9).codigo, visitUseCase.getTypeHoursBilling(9).descripcion, horasDiurnas.toDouble()),
                        ScheduleEntity(idTramite, 10,visitUseCase.getTypeHoursBilling(10).codigo, visitUseCase.getTypeHoursBilling(10).descripcion, horasNocturnas.toDouble())
                        )
                        visitUseCase.saveSchedule(listSchedule)
                    }

                }else{
                    if (fechaInicioJornada != fechaFinJornada){
                        if(comparisonHours(horaInicio.toString(), inicioHorarioNocturno.toString()) in listOf(ComparisonResult.MENOR)){
                            if (comparisonHours(horaInicio.toString(), inicioHorarioDiurno.toString()) in listOf(ComparisonResult.MAYOR,ComparisonResult.IGUAL)){
                                horasDiurnasDia1 = horasDiurnasDia1 +  calculateHourDifference(horaInicio.toString(), inicioHorarioNocturno.toString()).coerceAtLeast(0)
                                horasNocturnasDia1 =  calculateHourDifference(inicioHorarioNocturno.toString(), horaFinDia.toString()).coerceAtLeast(0)+ 1
                            }else{
                                if ( comparisonHours(horaInicio.toString(), inicioHorarioDiurno.toString()) in listOf(ComparisonResult.MENOR)){
                                    horasNocturnasDia1 = horasNocturnasDia1 + +  calculateHourDifference(horaInicio.toString(), inicioHorarioDiurno.toString()).coerceAtLeast(0)
                                    horasDiurnasDia1 = horasDiurnasDia1 +  calculateHourDifference(inicioHorarioDiurno.toString(), inicioHorarioNocturno.toString()).coerceAtLeast(0)
                                    horasNocturnasDia1 = horasNocturnasDia1 + calculateHourDifference(inicioHorarioNocturno.toString(), horaFinDia.toString()).coerceAtLeast(0)+ 1

                                }
                            }
                        }else{
                            if (comparisonHours(horaInicio.toString(), inicioHorarioNocturno.toString()) in listOf(ComparisonResult.MAYOR,ComparisonResult.IGUAL)){
                                horasNocturnasDia1 = horasNocturnasDia1 +  calculateHourDifference(horaInicio.toString(), horaFinDia.toString()).coerceAtLeast(0)+ 1
                            }
                        }
                        if (comparisonHours(horaFin.toString(), inicioHorarioDiurno.toString()) in listOf(ComparisonResult.MAYOR)){
                            horasNocturnasDia2 = horasNocturnasDia2 + calculateHourDifference(horainicioDia.toString(), inicioHorarioDiurno.toString()).coerceAtLeast(0)
                            if (comparisonHours(horaFin.toString(), inicioHorarioNocturno.toString()) in listOf(ComparisonResult.MAYOR)){
                                horasDiurnasDia2 = horasDiurnasDia2 +  calculateHourDifference(inicioHorarioDiurno.toString(), inicioHorarioNocturno.toString()).coerceAtLeast(0)
                                horasNocturnasDia2 = horasNocturnasDia2 + calculateHourDifference(inicioHorarioNocturno.toString(), horaFin.toString()).coerceAtLeast(0)

                            }else{
                                horasDiurnasDia2 = horasDiurnasDia2 + calculateHourDifference(inicioHorarioDiurno.toString(), horaFin.toString()).coerceAtLeast(0)
                            }
                        }else{
                            horasNocturnasDia2 = horasNocturnasDia2 + calculateHourDifference(horainicioDia.toString(), horaFin.toString()).coerceAtLeast(0)
                        }

                        if(!isWeekendDay(fechaInicioJornada) && !existeFechaEvento){
                            val listSchedule = mutableListOf(
                                ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnasDia1 + horasNocturnasDia1).toDouble()),
                                ScheduleEntity(idTramite, 7, visitUseCase.getTypeHoursBilling(7).codigo, visitUseCase.getTypeHoursBilling(7).descripcion, horasDiurnasDia1.toDouble()),
                                ScheduleEntity(idTramite, 8,visitUseCase.getTypeHoursBilling(8).codigo, visitUseCase.getTypeHoursBilling(8).descripcion, horasNocturnasDia1.toDouble())
                            )
                            visitUseCase.saveSchedule(listSchedule)
                        }else{
                            val listSchedule = mutableListOf(
                                ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnasDia1 + horasNocturnasDia1).toDouble()),
                                ScheduleEntity(idTramite, 9, visitUseCase.getTypeHoursBilling(9).codigo, visitUseCase.getTypeHoursBilling(9).descripcion, horasDiurnasDia1.toDouble()),
                                ScheduleEntity(idTramite, 10,visitUseCase.getTypeHoursBilling(10).codigo, visitUseCase.getTypeHoursBilling(10).descripcion, horasNocturnasDia1.toDouble())
                            )
                            visitUseCase.saveSchedule(listSchedule)
                        }

                        if(!isWeekendDay(fechaFinJornada) && !existeFechaEvento){
                            val listSchedule = mutableListOf(
                                ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnasDia2 + horasNocturnasDia2).toDouble()),
                                ScheduleEntity(idTramite, 7, visitUseCase.getTypeHoursBilling(7).codigo, visitUseCase.getTypeHoursBilling(7).descripcion, horasDiurnasDia2.toDouble()),
                                ScheduleEntity(idTramite, 8,visitUseCase.getTypeHoursBilling(8).codigo, visitUseCase.getTypeHoursBilling(8).descripcion, horasNocturnasDia2.toDouble())
                            )
                            visitUseCase.saveSchedule(listSchedule)

                        }else{
                            val listSchedule = mutableListOf(
                                ScheduleEntity(idTramite, 6, visitUseCase.getTypeHoursBilling(6).codigo, visitUseCase.getTypeHoursBilling(6).descripcion, (horasDiurnasDia2 + horasNocturnasDia2).toDouble()),
                                ScheduleEntity(idTramite, 9, visitUseCase.getTypeHoursBilling(9).codigo, visitUseCase.getTypeHoursBilling(9).descripcion, horasDiurnasDia2.toDouble()),
                                ScheduleEntity(idTramite, 10,visitUseCase.getTypeHoursBilling(10).codigo, visitUseCase.getTypeHoursBilling(10).descripcion, horasNocturnasDia2.toDouble())
                            )
                            visitUseCase.saveSchedule(listSchedule)
                        }
                    }
                }
            }
        }

    }



}