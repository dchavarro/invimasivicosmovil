package com.soaint.data.repository

import com.soaint.data.api.GenericsApi
import com.soaint.data.api.MasterApi
import com.soaint.data.api.ParametricApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.ActasDao
import com.soaint.data.room.dao.PlantillasDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.common.getIp
import com.soaint.domain.model.GroupDocumentEntity
import com.soaint.domain.model.GroupEntity
import com.soaint.domain.model.RulesBussinessEntity
import com.soaint.domain.repository.ManageRepository
import com.soaint.domain.repository.UserRepository
import com.soaint.sivicos_dinamico.utils.GRUPO_PAPF
import com.soaint.sivicos_dinamico.utils.ID_MISIONAL_SANITARIAS
import javax.inject.Inject

class ManageRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val masterApi: MasterApi,
    private val parametricApi: ParametricApi,
    private val genericsApi: GenericsApi,
    private val actasDao: ActasDao,
    private val plantillasDao: PlantillasDao,
) : ManageRepository, BaseRepository() {

    //Carga de Grupo Papf
    override suspend fun loadGroupPapf(): Result<List<GroupEntity>> = execute {
        val idMisional = userRepository.idMisional.orZero()
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val body = BodyGenericValuesDto(getIp(), userName, GRUPO_PAPF)
        val result = genericsApi.getGroupPapf(body).objectResponse
        val newMap = result?.mapNotNull {
            GroupDto(
                id = it.id,
                descripcion = it.nombreDependencia,
                idTipoProducto = idMisional,
            )
        }
        deleteActDao(idMisional)
        saveGroupDao(newMap, idMisional.toInt())
        Result.success(newMap!!.map { it.mapToDomain() })
    }

    //Carga de Grupo Visitas
    override suspend fun loadGroup(idTipoProducto: Int): Result<List<GroupEntity>> = execute {
        val result = masterApi.getGroup(idTipoProducto.toString()).objectResponse
        deleteActDao(idTipoProducto)
        saveGroupDao(result, idTipoProducto)
        Result.success(result!!.map { it.mapToDomain() })
    }

    //Guardar Grupo Visitas en DB local
    private suspend fun saveGroupDao(data: List<GroupDto>?, idTipoProducto: Int) {
        data?.onEach { result ->
            val model = result.mapToDb(idTipoProducto)
            actasDao.insertGroupVisit(model)
        }
    }

    //Carga de Grupo DB local
    override suspend fun loadGroupLocal(idTipoProducto: Int?): Result<List<GroupEntity>> = execute {
        val result = if (idTipoProducto != null) {
            actasDao.selectGroupVisit(idTipoProducto).map { it.mapToDomain() }
        } else {
            actasDao.selectAllGroupVisit().map { it.mapToDomain() }
        }
        Result.success(result)
    }


/////////////////

    //Carga de SubGrupo Visitas
    override suspend fun loadGroupDocument(): Result<List<GroupDocumentEntity>> = execute {
        val result = masterApi.getGroupDocument().objectResponse
        saveSubGroupDao(result)
        Result.success(result!!.map { it.mapToDomain() })
    }

    //Guardar SubGrupo Visitas en DB local
    private suspend fun saveSubGroupDao(data: List<GroupDocumentDto>?) {
        actasDao.deleteAllGroupDocument()
        data?.onEach { result ->
            val model = result.mapToDb()
            actasDao.insertGroupDocument(model)
        }
    }

    //Carga de SubGrupo DB local
    override suspend fun loadGroupDocumentLocal(): Result<List<GroupDocumentEntity>> =
        Result.success(
            actasDao.selectGroupDocument().map { it.mapToDomain() }
        )

    /////////////////

    //Carga de Regla de Negocio
    override suspend fun loadRulesBussiness(
        idGroup: Int,
        idTipoProducto: Int
    ): Result<List<RulesBussinessEntity>> =
        execute {
            val result = parametricApi.getRulesBussiness(
                idTipoProducto.toString(),
                idGroup.toString()
            ).objectResponse
            actasDao.deleteActaVisitByIdGroup(idGroup)
            saveRulesBussinessDao(result)
            Result.success(result!!.map { it.mapToDomain() })
        }

    //Guardar Regla de Negocio en DB local
    private suspend fun saveRulesBussinessDao(
        data: List<RulesBussinessDto>?,
        idTipoProductoPapf: Int? = null,
    ) {
        data?.onEach { result ->
            val model = result.mapToDb(idTipoProductoPapf)
            actasDao.insertActaVisit(model)

            if (idTipoProductoPapf != null) {
                plantillasDao.insertPlantilla(
                    PlantillaDto(
                        id = model.id.orZero(),
                        codigo = model.codigoPlantilla.orEmpty(),
                        nombre = model.nombre.orEmpty(),
                        descripcion = model.nombre.orEmpty(),
                        idTipoDocumental = model.idTipoDocumental.orZero(),
                    ).mapToDb()
                )
            }
        }


    }

    //Carga de SubGrupo DB local
    override suspend fun loadRulesBussinessLocal(
        idGroup: Int,
        idTipoProductoPapf: Int?
    ): Result<List<RulesBussinessEntity>> =
        Result.success(
            if (idTipoProductoPapf == null) actasDao.selectActaVisit(idGroup)
                .map { it.mapToDomain() }
            else actasDao.selectActaVisit(idGroup, idTipoProductoPapf).map { it.mapToDomain() }
        )

    //Eliminar Documentos Asociados en DB local
    private suspend fun deleteActDao(idTipoProducto: Int) {
        actasDao.deleteGroupVisitByIdTipoProducto(idTipoProducto)
    }

    override suspend fun loadRulesBussinessLocal(idPlantilla: String): Result<RulesBussinessEntity?> =
        Result.success(
            actasDao.selectActaVisitByPlantilla(idPlantilla)?.mapToDomain()
        )

    //Carga de Regla de Negocio PAPF
    override suspend fun loadRulesBussinessPapf(idTipoProductoPapf: Int, filter: String): Result<List<RulesBussinessEntity>?> =
        execute {
            val result =
                parametricApi.getRulesBussinessPapf(idTipoProductoPapf = idTipoProductoPapf, codigoFiltroActa = filter).objectResponse
            actasDao.deleteAllActaXidTipoPapf(idTipoProductoPapf)
            saveRulesBussinessDao(result, idTipoProductoPapf)
            Result.success(result!!.map { it.mapToDomain() })
        }
}