package com.soaint.domain.repository

import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity
import kotlinx.coroutines.flow.StateFlow

interface HistoricMssRepository {

    val historicoMss: StateFlow<HistoricMssEntity?>

    suspend fun loadHistoricMss(razonSocial: String): Result<HistoricMssEntity>

    suspend fun loadHistoricMssLocal(razonSocial: String): Result<HistoricMssEntity>

    suspend fun loadMssLocal(razonSocial: String): Result<List<MssEntity>>
}