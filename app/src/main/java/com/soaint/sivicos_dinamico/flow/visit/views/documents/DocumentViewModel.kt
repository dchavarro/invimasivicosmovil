package com.soaint.sivicos_dinamico.flow.visit.views.documents

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.ACTA_PREF
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.domain.repository.PreferencesRepository
import com.soaint.domain.use_case.DocumentUseCase
import com.soaint.domain.use_case.PlantillasUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import java.util.*
import javax.inject.Inject

sealed class DocumentViewModelEvent {
    data class ChangeLabelPreloader(@StringRes val label: Int) : DocumentViewModelEvent()
    data class ViewModelReady(val document: List<DocumentEntity>) : DocumentViewModelEvent()
    data class ViewModelDelete(val visitId: String, val plantillaId: String) :
        DocumentViewModelEvent()

    data class ViewModelViewApi(
        val documentBase64: String?,
        val nameDocument: String?,
    ) : DocumentViewModelEvent()

    data class ViewModelViewLocal(
        val documentBodyEntity: DocumentBodyEntity? = null,
        val pdfAct: String? = null
    ) : DocumentViewModelEvent()

    data class LoadingError(val error: Throwable?) : DocumentViewModelEvent()
    data class ViewModelBlockReady(
        val visit: VisitEntity?,
        val plantilla: List<PlantillaEntity>,
        val block: List<BlockEntity>
    ) : DocumentViewModelEvent()

    data class ViewModelSnackBar(@StringRes val message: Int, val isSuccess: Boolean) :
        DocumentViewModelEvent()

}

class DocumentViewModel @Inject constructor(
    private val documentUseCase: DocumentUseCase,
    private val plantillasUseCase: PlantillasUseCase,
    private val preferencesRepository: PreferencesRepository
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<DocumentViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<VisitEntity>()
    val visitData = _visitData.asLiveData()

    private var block: List<BlockEntity> = emptyList()

    fun init(visitData: List<VisitEntity>) {
        _visitData.value = visitData.firstOrNull()
        onloadDocument(_visitData.value?.id.toString())
    }

    fun onloadDocument(idVisita: String) = execute {
        _event.value =
            DocumentViewModelEvent.ChangeLabelPreloader(R.string.cargando_document)
        val result = documentUseCase.loadDocumentLocal(idVisita)
        _event.value = if (result.isSuccess) {
            val resultDocument = result.getOrNull().orEmpty()
            DocumentViewModelEvent.ViewModelReady(resultDocument)
        } else {
            DocumentViewModelEvent.LoadingError(null)
        }
    }

    fun deleteActaLocal(idVisita: String, idPlantilla: String) {
        val map: Map<String, Any> = Collections.emptyMap()
        preferencesRepository.setObject("$ACTA_PREF/$idPlantilla/$idVisita", map)
    }

    fun onDelete(documento: DocumentEntity) = execute {
        val result = documento.id?.let {
            documentUseCase.deleteDocument(
                it,
                documento.urlSharedPoint,
                _visitData.value?.id.toString(),
                documento.idPlantilla.orZero().toString(),
            )
        }
        if (result != null) {
            _event.value = if (result.isSuccess) {
                DocumentViewModelEvent.ViewModelDelete(
                    _visitData.value?.id.toString(),
                    documento.idPlantilla.toString()
                )
            } else {
                DocumentViewModelEvent.LoadingError(null)
            }
        }
        onloadDocument(visitData.value?.id.toString())
    }

    fun onViewLocal(id: Int?) = execute {
        _event.value =
            DocumentViewModelEvent.ChangeLabelPreloader(R.string.cargando)
        val result = documentUseCase.getDocumentBodyLocalById(id.toString())
        _event.value = if (result.isSuccess) {
            DocumentViewModelEvent.ViewModelViewLocal(
                result.getOrNull()?.firstOrNull()
            )
        } else {
            DocumentViewModelEvent.LoadingError(null)
        }
    }

    fun onViewApi(idDocumento: Int, nameDocument: String) = execute {
        _event.value =
            DocumentViewModelEvent.ChangeLabelPreloader(R.string.cargando)
        val result = documentUseCase.viewDocument(idDocumento)
        _event.value = if (result.isSuccess) {
            DocumentViewModelEvent.ViewModelViewApi(result.getOrNull(), nameDocument)
        } else {
            DocumentViewModelEvent.ViewModelSnackBar(R.string.no_internet, false)
        }
    }

    fun onViewActLocal(idPlantilla: Int) = execute {
        _event.value = DocumentViewModelEvent.ChangeLabelPreloader(R.string.cargando)
        val result = _visitData.value?.id?.let {
            documentUseCase.getDocumentActLocal(idPlantilla, it)
        }
        _event.value = if (result?.isSuccess == true) {
            val resultPlantilla = plantillasUseCase.loadPlantillasLocal(idPlantilla.toString())
            val plantilla = resultPlantilla.getOrNull().orEmpty()
            DocumentViewModelEvent.ViewModelViewLocal(
                DocumentBodyEntity(nombreDocumento = plantilla.firstOrNull()?.nombre),
                pdfAct = result.getOrNull()
            )
        } else {
            DocumentViewModelEvent.LoadingError(null)
        }
    }

    fun onEdit(idPlantilla: String) = execute {
        _event.value =
            DocumentViewModelEvent.ChangeLabelPreloader(R.string.cargando_acta)
        val result = plantillasUseCase.loadBlocksLocal(idPlantilla)
        _event.value = if (result.isSuccess) {
            block = result.getOrNull().orEmpty()
            val visit = _visitData.value
            val resultPlantilla = plantillasUseCase.loadPlantillasLocal(idPlantilla)
            val plantilla = resultPlantilla.getOrNull().orEmpty()
            DocumentViewModelEvent.ViewModelBlockReady(
                visit,
                plantilla,
                result.getOrNull().orEmpty()
            )
        } else {
            DocumentViewModelEvent.LoadingError(null)
        }
    }

}