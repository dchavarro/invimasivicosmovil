package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTitleBinding
import com.xwray.groupie.databinding.BindableItem

class SubTitleItemView(
    private val title: String,
) : BindableItem<ItemTitleBinding>() {

    override fun bind(
        viewBinding: ItemTitleBinding,
        position: Int
    ) = with(viewBinding) {
        viewBinding.text = title.uppercase()
    }

    override fun getLayout() = R.layout.item_subtitle
}