package com.soaint.sivicos_dinamico.flow.papf.item_view

import androidx.core.view.isVisible
import com.soaint.domain.model.DestinatarioPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemOtherProductPapfBinding
import com.soaint.sivicos_dinamico.utils.LUGAR_DESTINO
import com.soaint.sivicos_dinamico.utils.PUERTO_CONTROL_FRONTERIZO
import com.xwray.groupie.databinding.BindableItem

class OtherDetailProductPapfItemView(
    private val item: DestinatarioPapfEntity,
    private val title: String,
) : BindableItem<ItemOtherProductPapfBinding>() {

    override fun bind(
        viewBinding: ItemOtherProductPapfBinding,
        position: Int
    ) = with(viewBinding) {
        viewBinding.data = item
        viewBinding.text = title.uppercase()

        textInputPlanta.isVisible = item.typeList == LUGAR_DESTINO
        textInputNombre.isVisible = item.typeList != PUERTO_CONTROL_FRONTERIZO
        textInputDireccion.isVisible = item.typeList != PUERTO_CONTROL_FRONTERIZO
        textInputTelefono.isVisible = item.typeList != PUERTO_CONTROL_FRONTERIZO
        textInputCodigoPostal.isVisible = item.typeList != PUERTO_CONTROL_FRONTERIZO
        textInputCodigoIso.isVisible = item.typeList != PUERTO_CONTROL_FRONTERIZO
        textInputAutoridad.isVisible = item.typeList == PUERTO_CONTROL_FRONTERIZO
    }

    override fun getLayout() = R.layout.item_other_product_papf
}