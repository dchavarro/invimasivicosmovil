package com.soaint.sivicos_dinamico

import androidx.appcompat.app.AppCompatDelegate
import com.soaint.sivicos_dinamico.di.DaggerSivicoComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

class SivicoApplication : DaggerApplication(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    override fun onCreate() {
        super.onCreate()
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> =
        DaggerSivicoComponent
            .builder()
            .application(this)
            .build()

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

}