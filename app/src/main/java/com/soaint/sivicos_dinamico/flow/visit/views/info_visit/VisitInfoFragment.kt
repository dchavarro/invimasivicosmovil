package com.soaint.sivicos_dinamico.flow.visit.views.info_visit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentVisitInfoBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.*
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.REASON_RAVI_INPER
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class VisitInfoFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentVisitInfoBinding>(R.layout.fragment_visit_info)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: VisitInfoViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showHamburgerIcon()
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            requireActivity().finish()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnVisitInfoViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnVisitInfoViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnVisitInfoViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnVisitInfoViewModelEvent.OnViewModelReady -> onViewModelReady(
                event.visit,
                event.antecedent,
                event.sample,
                event.official,
                event.group,
                event.subgroup,
                event.schedule,
                event.sampleAnalysis,
                event.samplePlan,
            )
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReady(
        visit: List<VisitEntity>,
        antecedent: List<AntecedentEntity>,
        sample: List<SampleEntity>,
        official: List<VisitOfficialEntity>,
        group: List<VisitGroupEntity>,
        subgroup: List<VisitGroupEntity>,
        schedule: List<ScheduleTempEntity>?,
        sampleAnalysis: List<SampleAnalysisEntity>,
        samplePlan: List<SamplePlanEntity>,
    ) {
        adapter.clear()
        val items = mutableListOf<Group>()
        if (!visit.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_information_visit)))
            items.addAll(visit.map { VisitInfoItemView(it, ::openRegisterCompany) })
            items.add(TitleItemView(getString(R.string.copy_reason)))
            items.addAll(visit.map { ReasonItemView(it, group, subgroup) })
        }
        if (!antecedent.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_antecedent)))
            items.addAll(antecedent.map { AntecedentItemView(it) })
        }
        if (!sample.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_sample)))
            items.addAll(sample.map { SampleItemView(it, sampleAnalysis, samplePlan) })
        }

        if (!official.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_official_visit)))
            items.addAll(official.map { VisitOfficialItemView(it) })
        }

        if (!schedule.isNullOrEmpty() && visit.first().codigoRazon == REASON_RAVI_INPER) {
            items.add(TitleItemView(getString(R.string.copy_schedule_visit)))
            items.add(ScheduleItemView(schedule))
        }

        val isVisible =
            visit.isNullOrEmpty() && antecedent.isNullOrEmpty() && sample.isNullOrEmpty() && official.isNullOrEmpty() && schedule.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }

    private fun openRegisterCompany() {
        findNavController().navigate(VisitInfoFragmentDirections.informacionVisitaToRegistroEmpresa())
    }

}