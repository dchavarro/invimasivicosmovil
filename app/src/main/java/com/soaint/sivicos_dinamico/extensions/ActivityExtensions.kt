package com.soaint.sivicos_dinamico.extensions

import android.app.Activity
import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import com.soaint.data.common.CHOOSE_FILE_REQUEST
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import org.aviran.cookiebar2.CookieBar
import org.aviran.cookiebar2.CookieBarDismissListener

fun Activity.chooseFile() {
    val intent = Intent()
    intent.action = Intent.ACTION_GET_CONTENT
    intent.type = "*/*"
    val extraMimeTypes = arrayOf("application/pdf", "image/png", "image/jpg", "image/jpeg")
    intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMimeTypes)
    intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true)
    startActivityForResult(intent, CHOOSE_FILE_REQUEST)
}

fun Activity.copyToClipboard(text: String){
    val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
    val clip = ClipData.newPlainText("label",text)
    clipboard.setPrimaryClip(clip)
}

fun Activity.showSnackBar(
    count: Int = 0,
    autoDismiss: Boolean = true,
    type: AlertTypes = AlertTypes.SYNC,
    actionText: String = resources.getString(R.string.sincronizar),
    title: String = getString(R.string.copy_sync_pendient_title, count.toString()),
    msj: String = resources.getString(R.string.copy_sync_pendient_subtitle),
    action: () -> Unit,
) {

    val iconAlert = when (type) {
        AlertTypes.ERROR -> R.drawable.ic_action_clear
        AlertTypes.SUCCESS -> R.drawable.ic_action_done
        AlertTypes.HELP -> R.drawable.ic_info
        AlertTypes.INFORMATION -> R.drawable.ic_info
        AlertTypes.SYNC -> R.drawable.ic_sync
    }

    val colorAlert = when (type) {
        AlertTypes.ERROR -> R.color.colorRed
        AlertTypes.SUCCESS -> R.color.colorGreen
        AlertTypes.HELP -> R.color.colorPrimary
        AlertTypes.INFORMATION -> R.color.colorYellow
        AlertTypes.SYNC -> R.color.colorYellow
    }

    val colorText = when (type) {
        AlertTypes.ERROR -> R.color.white
        AlertTypes.SUCCESS -> R.color.white
        AlertTypes.HELP -> R.color.white
        AlertTypes.INFORMATION -> R.color.black
        AlertTypes.SYNC -> R.color.black
    }

    CookieBar.build(this)
        .setEnableAutoDismiss(autoDismiss)
        .setSwipeToDismiss(autoDismiss)
        .setDuration(3500)
        .setTitle(title)
        .setIcon(iconAlert)
        .setMessage(msj)
        .setBackgroundColor(colorAlert)
        .setActionColor(colorText)
        .setTitleColor(colorText)
        .setMessageColor(colorText)
        .setCookiePosition(CookieBar.BOTTOM)
        .setAction(actionText.uppercase()) { action.invoke() }
        .show()
}

interface OnAction {
    fun onClick()
}

fun Activity.openUrl(url: String) {
    val defaultBrowser = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_BROWSER)
    defaultBrowser.data = Uri.parse(url)
    startActivity(defaultBrowser)
}