package com.soaint.sivicos_dinamico.factories.controlsFactory.types

class TiposControl {
    companion object {
        const val TEXTO_CORTO: Int = 1
        const val EMAIL: Int = 2
        const val NUMERICO: Int = 3
        const val LISTA: Int = 4
        const val LISTA_TABLA: Int = 5
        const val FECHA: Int = 6
        const val FECHA_HORA: Int = 7
        const val RADIO_GROUP: Int = 8
        const val CHECK: Int = 9
        const val CHECK_LIST: Int = 10
        const val FIRMA: Int = 11
        const val TEXTO_LARGO: Int = 12
        const val FOTO: Int = 13
    }
}