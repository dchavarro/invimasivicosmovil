package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.DocumentNameEntity
import com.soaint.domain.model.MetadataEntity

@Entity(tableName = "document_table")
class DocumentModel(
    @ColumnInfo(name = "extension") val extension: String?,
    @ColumnInfo(name = "descripcionTipoDocumental") val descripcionTipoDocumental: String?,
    @ColumnInfo(name = "urlSesuite") val urlSesuite: String?,
    @ColumnInfo(name = "descripcionClasificacionDocumental") val descripcionClasificacionDocumental: String?,
    @ColumnInfo(name = "detalle") val detalle: String?,
    @ColumnInfo(name = "urlSharedPoint") val urlSharedPoint: String?,
    @ColumnInfo(name = "descripcionDocumento") val descripcionDocumento: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "clasificacionDocumental") val clasificacionDocumental: Int?,
    @ColumnInfo(name = "idDocumento") val idDocumento: Int?,
    @ColumnInfo(name = "nombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name = "consecutivoSesuite") val consecutivoSesuite: String?,
    @ColumnInfo(name = "fechaDocumento") val fechaDocumento: String?,
    @ColumnInfo(name = "nombreTipoDocumental") val nombreTipoDocumental: String?,
    @ColumnInfo(name = "idTipoDocumental") val idTipoDocumental: Int? = 0,
    @ColumnInfo(name = "numeroFolios") val numeroFolios: String?,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: Int? = 0,
    @ColumnInfo(name = "codigoPlantilla") val codigoPlantilla: String?,
    @ColumnInfo(name = "indDocumentoFirmado") val indDocumentoFirmado: Boolean? = false,
    @ColumnInfo(name = "idVisita") val idVisita: String?,
    @ColumnInfo(name = "idDocumentBody") val idDocumentBody: Int?,
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
)

fun DocumentModel.mapToDomain() = DocumentEntity(
    extension,
    descripcionTipoDocumental,
    urlSesuite,
    descripcionClasificacionDocumental,
    detalle,
    urlSharedPoint,
    descripcionDocumento,
    usuarioCrea,
    clasificacionDocumental,
    idDocumento,
    nombreDocumento,
    consecutivoSesuite,
    fechaDocumento,
    nombreTipoDocumental,
    idTipoDocumental,
    numeroFolios,
    idPlantilla,
    codigoPlantilla,
    indDocumentoFirmado,
    idDocumentBody,
    id
)

@Entity(tableName = "document_name_table")
class DocumentNameModel(
    @ColumnInfo(name = "idTipoDocumental") val idTipoDocumental: Int?,
    @ColumnInfo(name = "nombreTipoDocumental") val nombreTipoDocumental: String?,
    @ColumnInfo(name = "idGrupo") val idGrupo: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0
)

fun DocumentNameModel.mapToDomain() = DocumentNameEntity(
    idTipoDocumental,
    nombreTipoDocumental,
)

@Entity(tableName = "document_body_table")
class DocumentBodyModel(
    @ColumnInfo(name ="idTipoDocumental") val idTipoDocumental: Int,
    @ColumnInfo(name ="idDocumento") val idDocumento: String?,
    @ColumnInfo(name ="nombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name ="descripcionDocumento") val descripcionDocumento: String?,
    @ColumnInfo(name ="extension") val extension: String?,
    @ColumnInfo(name ="archivoBase64") val archivoBase64: String?,
    @ColumnInfo(name ="idSolicitud") val idSolicitud: String?,
    @ColumnInfo(name ="idTramite") val idTramite: String?,
    @ColumnInfo(name ="idExpediente") val idExpediente: String?,
    @ColumnInfo(name ="numeroRadicado") val numeroRadicado: String?,
    @ColumnInfo(name ="numeroActaSala") val numeroActaSala: String?,
    @ColumnInfo(name ="idSala") val idSala: String?,
    @ColumnInfo(name ="idVisita") val idVisita: String?,
    @ColumnInfo(name ="numeroAgenda") val numeroAgenda: String?,
    @ColumnInfo(name ="idEmpresa") val idEmpresa: String?,
    @ColumnInfo(name ="numeroFolios") val numeroFolios: Int?,
    @ColumnInfo(name ="usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name ="idClasificacionDocumental") val idClasificacionDocumental: Int?,
    @ColumnInfo(name ="fechaCreacion") val fechaCreacion: String? = getDateHourNow(),
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun DocumentBodyModel.mapToDomain(metadata: MetadataEntity?) = DocumentBodyEntity(
    idTipoDocumental,
    idDocumento,
    nombreDocumento,
    descripcionDocumento,
    extension,
    archivoBase64,
    idSolicitud,
    idTramite,
    idExpediente,
    numeroRadicado,
    numeroActaSala,
    idSala,
    idVisita,
    numeroAgenda,
    idEmpresa,
    numeroFolios,
    usuarioCrea,
    idClasificacionDocumental,
    metadata
)

fun DocumentBodyEntity.mapToModel() = DocumentBodyModel(
    idTipoDocumental,
    idDocumento,
    nombreDocumento,
    descripcionDocumento,
    extension,
    archivoBase64,
    idSolicitud,
    idTramite,
    idExpediente,
    numeroRadicado,
    numeroActaSala,
    idSala,
    idVisita,
    numeroAgenda,
    idEmpresa,
    numeroFolios,
    usuarioCrea,
    idClasificacionDocumental
)

@Entity(tableName = "document_metadata_body_table",
    foreignKeys = [
        ForeignKey(
            entity = DocumentBodyModel::class,
            parentColumns = ["id"],
            childColumns = ["idDocumentBody"],
            onDelete = ForeignKey.CASCADE
        )
    ])
class MetadataDocumentBodyModel(
    @ColumnInfo(name = "tipologiaDocumental") val tipologiaDocumental: String?,
    @ColumnInfo(name = "tituloDocumento") val tituloDocumento: String?,
    @ColumnInfo(name = "autor") val autor: String?,
    @ColumnInfo(name = "autorFirmante") val autorFirmante: String?,
    @ColumnInfo(name = "clasificacionAcceso") val clasificacionAcceso: String?,
    @ColumnInfo(name = "folio_gd") val folio_gd: String?,
    @ColumnInfo(name = "formato") val formato: String?,
    @ColumnInfo(name = "estadoDoc") val estadoDoc: String?,
    @ColumnInfo(name = "macroProceso") val macroProceso: String?,
    @ColumnInfo(name = "proceso") val proceso: String?,
    @ColumnInfo(name = "documental") val documental: String?,
    @ColumnInfo(name = "tecnologico") val tecnologico: String?,
    @ColumnInfo(name = "nroVisita") val nroVisita: String?,
    @ColumnInfo(name = "nombreEmpresa") val nombreEmpresa: String?,
    @ColumnInfo(name = "nitEmpresa") val nitEmpresa: String?,
    @ColumnInfo(name = "fechaCreacionDocumento") val fechaCreacionDocumento: String?,
    @ColumnInfo(name = "idDocumentBody") val idDocumentBody: Int?,
    @PrimaryKey(autoGenerate = true) val id: Int? = 0,
)

fun MetadataEntity.mapToModel(idDocumentBody: Int?) = MetadataDocumentBodyModel(
    tipologiaDocumental,
    tituloDocumento,
    autor,
    autorFirmante,
    clasificacionAcceso,
    folio_gd,
    formato,
    estadoDoc,
    macroProceso,
    proceso,
    documental,
    tecnologico,
    nroVisita,
    nombreEmpresa,
    nitEmpresa,
    fechaCreacionDocumento,
    idDocumentBody
)

fun MetadataDocumentBodyModel.mapToDomain(idDocumentBody: Int?) = MetadataEntity(
    tipologiaDocumental,
    tituloDocumento,
    autor,
    autorFirmante,
    clasificacionAcceso,
    folio_gd,
    formato,
    estadoDoc,
    macroProceso,
    proceso,
    documental,
    tecnologico,
    nroVisita,
    nombreEmpresa,
    nitEmpresa,
    fechaCreacionDocumento,
    idDocumentBody
)