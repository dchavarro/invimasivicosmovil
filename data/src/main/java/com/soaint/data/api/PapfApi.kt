package com.soaint.data.api

import com.soaint.data.model.InfoTramitePapfBodyDto
import com.soaint.data.model.InfoTramitePapfResponseDto
import retrofit2.http.Body
import retrofit2.http.POST

interface PapfApi {

    //SERVICIO CONSULTA INFORMACION TRAMITE
    @POST("v1/papf/informacion-tramite")
    suspend fun getInfoTramite(
        @Body body: InfoTramitePapfBodyDto
    ): InfoTramitePapfResponseDto

}