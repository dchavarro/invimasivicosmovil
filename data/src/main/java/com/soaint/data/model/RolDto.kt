package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.LocationModel
import com.soaint.data.room.tables.TypeMssModel
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.RolEntity

data class RolResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<RolDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class RolDto(
    @SerializedName("IdRolPersona") val id: Int,
    @SerializedName("DescripcionRolPersona") val descripcion: String?
)

fun RolDto.mapToDomain(idMisional: String, tipo: String) = RolEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idMisional
)

fun RolDto.mapToDb(idMisional: String, tipo:String) = TypeMssModel(
    id = id,
    null,
    descripcion = descripcion,
    null,
    null,
    tipoMs = tipo,
    idTipoProducto = idMisional
)
