package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.AmsspCheckModel
import com.soaint.data.room.tables.MsspListModel
import com.soaint.data.room.tables.TypeMssModel
import com.soaint.domain.model.LstValoresMssAppliedEntity
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.domain.model.MssAppliedEntity
import com.soaint.sivicos_dinamico.utils.*

data class MssResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ManageMssDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class ManageMssDto(
    @SerializedName("id") val id: Int,
    @SerializedName("codigo") val codigo: String?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("codigoPlantillas") val codigoPlantillas: String?,
    @SerializedName("codigoPlantillasActualizada") val codigoPlantillasActualizada: String?,
)

fun ManageMssDto.mapToDomain(idTipoProducto: String, tipoMs: String) = ManageMssEntity(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
    codigoPlantillas,
    codigoPlantillasActualizada,
    tipoMs,
    idTipoProducto
)

fun ManageMssDto.mapToDb(idTipoProducto: String, tipoMs: String) = TypeMssModel(
    id,
    codigo,
    descripcion,
    codigoPlantillas,
    codigoPlantillasActualizada,
    tipoMs,
    idTipoProducto
)

data class BodyGenericValuesDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: InputValueDto
) {
    constructor(
        ip: String,
        user: String,
        codigo: String,
        idTipoProducto: String? = null,
        idTipoTramitePapf: Int? = null,
        idSolicitudPapf: Int? = null
    ) : this(
        AuditoriaDto(ip, user),
        InputValueDto(
            codigo,
            when (codigo) {
                CDZVE, GRUPO_PAPF, CD_EMPAQ, CD_CONCEPT, CD_UNITY, CD_PRESENTATION, CD_MPIG, CD_FIRMATE, CD_RESULT_CIS, CD_TYPE_CERTIFICATE, CD_TYPE_PRODUCT -> null
                CD_IDIOM -> listOf(
                    LstValoresDto(
                        "Tipo",
                        if (idTipoTramitePapf == ID_TYPE_IMPORTER) "'IMP'" else "'EXP'"
                    )
                )
                CD_OBSERVATION_PAPF -> listOf(
                    LstValoresDto(
                        "idTipoProductoPapf",
                        idTipoTramitePapf.orZero().toString()
                    )
                )
                CD_REQUERIMIENTOS -> listOf(LstValoresDto("1", "1"))
                CD_INFO_ACTAS_EMITIR, CD_DATE_INSPECTION -> listOf(
                    LstValoresDto(
                        "IdSolicitud",
                        idSolicitudPapf.orZero().toString()
                    )
                )
                CTIPPROD -> listOf(LstValoresDto("idDependenciaPadre", idTipoProducto))
                ROL_PERSONA -> listOf()
                else -> listOf(LstValoresDto("IdDependencia", idTipoProducto))
            }
        )
    )
}

data class InputValueDto(
    @SerializedName("codigo") val codigo: String,
    @SerializedName("lstValores") val lstValores: List<LstValoresDto>?
)

data class LstValoresDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("valor") val valor: String?
)

data class MsspResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ManageMsspDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class ManageMsspDto(
    @SerializedName("id") val id: Int,
    @SerializedName("numeroVisita") val numeroVisita: String?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("numeroMss") val numeroMss: String?,
    @SerializedName("nombreProducto") val nombreProducto: String?,
    @SerializedName("tipoMedidaSanitaria") val tipoMedidaSanitaria: String?,
    @SerializedName("estado") val estado: String?,
    @SerializedName("idEstado") val idEstado: Int?,
    @SerializedName("fechaAplicacion") val fechaAplicacion: String?,
    @SerializedName("notificacionSanitariaObligatoria") val notificacionSanitariaObligatoria: String?,
    @SerializedName("lote") val lote: String?,
    @SerializedName("codigoMedidaSanitaria") val codigoMedidaSanitaria: String?,
    @SerializedName("tiposActividades") val tiposActividades: String?,
)

fun ManageMsspDto.mapToDomain(razonSocial: String, tipoMs: String) = ManageMsspEntity(
    id,
    numeroVisita.orEmpty(),
    idVisita.orZero(),
    numeroMss.orEmpty(),
    nombreProducto.orEmpty(),
    tipoMedidaSanitaria.orEmpty(),
    estado.orEmpty(),
    idEstado.orZero(),
    fechaAplicacion.orEmpty(),
    notificacionSanitariaObligatoria.orEmpty(),
    lote.orEmpty(),
    codigoMedidaSanitaria.orEmpty(),
    tiposActividades.orEmpty(),
    razonSocial,
    tipoMs
)

fun ManageMsspDto.mapToDb(razonSocial: String, tipoMs: String) = MsspListModel(
    id,
    numeroVisita.orEmpty(),
    idVisita.orZero(),
    numeroMss.orEmpty(),
    nombreProducto.orEmpty(),
    tipoMedidaSanitaria.orEmpty(),
    estado.orEmpty(),
    idEstado.orZero(),
    fechaAplicacion.orEmpty(),
    notificacionSanitariaObligatoria.orEmpty(),
    lote.orEmpty(),
    codigoMedidaSanitaria.orEmpty(),
    tiposActividades.orEmpty(),
    razonSocial,
    tipoMs
)


data class UpdateMssBodyDto(
    @SerializedName("id") val id: Int,
    @SerializedName("idEstado") val idEstado: Int
)

data class InsertMssBodyDto(
    @SerializedName("idVisita") val idVisita: Int,
    @SerializedName("idTipoMedidaSanitaria") val idTipoMedidaSanitaria: Int,
    @SerializedName("idEstado") val idEstado: Int = 1,
    @SerializedName("fechaAplicacion") val fechaAplicacion: String,
    @SerializedName("tiposActividades") val tiposActividades: List<Int>,
    @SerializedName("usuarioCrea") val usuarioCrea: String,
    @SerializedName("nombreProducto") val nombreProducto: String,
    @SerializedName("notificacionSanitariaObligatoria") val notificacionSanitariaObligatoria: String,
)

//MSS APLICADAS
data class MssAppliedResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<MssAppliedDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class MssAppliedDto(
    @SerializedName("idMedidaSanitaria") val idMedidaSanitaria: Int,
    @SerializedName("codigoMedidaSanitaria") val codigoMedidaSanitaria: String,
    @SerializedName("descripcionMedida") val descripcionMedida: String,
    @SerializedName("valorAplicado") val valorAplicado: Boolean,
    @SerializedName("idVisita") val idVisita: Int,
)

fun MssAppliedDto.mapToDomain() = MssAppliedEntity(
    idMedidaSanitaria,
    codigoMedidaSanitaria,
    descripcionMedida,
    valorAplicado,
    idVisita
)

fun MssAppliedDto.mapToDb(tipoMs: String) = AmsspCheckModel(
    idMedidaSanitaria.orZero(),
    codigoMedidaSanitaria.orEmpty(),
    descripcionMedida.orEmpty(),
    idVisita,
    valorAplicado,
    tipoMs
)

data class MssAppliedBodyDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: InputMssAppliedDto
) {
    constructor(
        ip: String,
        user: String,
        idVisita: String,
        value: List<LstValoresMssAppliedDto>
    ) : this(
        AuditoriaDto(ip, user),
        InputMssAppliedDto(idVisita, value)
    )
}

data class InputMssAppliedDto(
    @SerializedName("idVisita") val idVisita: String,
    @SerializedName("medidasSanitariasAplicada") val medidasSanitariasAplicada: List<LstValoresMssAppliedDto>
)

data class LstValoresMssAppliedDto(
    @SerializedName("id") val id: Int,
    @SerializedName("valor") val valor: Boolean
)

fun LstValoresMssAppliedDto.mapToDomain() = LstValoresMssAppliedEntity(
    id,
    valor,
)

data class MssBodyDto(
    @SerializedName("Auditoria") val auditoria: AuditoriaCapitalDto,
    @SerializedName("entrada") val entrada: InputMssDto
) {
    constructor(
        ip: String,
        user: String,
        razonSocial: String,
        tipoMedidaSanitaria: String
    ) : this(
        AuditoriaCapitalDto(ip, user),
        InputMssDto(razonSocial, tipoMedidaSanitaria)
    )
}

data class InputMssDto(
    @SerializedName("razonSocial") val razonSocial: String,
    @SerializedName("tipoMedidaSanitaria") val tipoMedidaSanitaria: String
)