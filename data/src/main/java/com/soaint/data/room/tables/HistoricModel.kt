package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.HistoricEntity

@Entity(tableName = "historic_table")
class HistoricModel(
    @PrimaryKey val id: Int? = 0,
    @ColumnInfo(name = "numero") val numero: String?,
    @ColumnInfo(name = "codigoSucursal") val codigoSucursal: String?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "fechaRegistro") val fechaRegistro: String?,
    @ColumnInfo(name = "descDireccion") val descDireccion: String?,
    @ColumnInfo(name = "fechaRealizo") val fechaRealizo: String?,
    @ColumnInfo(name = "clasificacion") val clasificacion: String?,
    @ColumnInfo(name = "descRespRealizar") val descRespRealizar: String?,
    @ColumnInfo(name = "descFuenteInfo") val descFuenteInfo: String?,
    @ColumnInfo(name = "descRazon") val descRazon: String?,
    @ColumnInfo(name = "resultado") val resultado: String?,
    @ColumnInfo(name = "observacionResultado") val observacionResultado: String?,
    @ColumnInfo(name = "estado") val estado: String?,
    @ColumnInfo(name = "inspector") val inspector: String?,
    @ColumnInfo(name = "idVisita") val idVisita: String?,
    @ColumnInfo(name = "departamento") val departamento: String?,
    @ColumnInfo(name = "municipio") val municipio: String?,
    @ColumnInfo(name = "medidasSanitarias") val medidasSanitarias: String?,
    @ColumnInfo(name = "conceptoSanitario") val conceptoSanitario: String?,
    @ColumnInfo(name = "empresaAsociada") val empresaAsociada: String?,
    @ColumnInfo(name = "nombreEmpresaAsociada") val nombreEmpresaAsociada: String?,
    @ColumnInfo(name = "tipoTramite") val tipoTramite: String?,
    @ColumnInfo(name = "subTipoTramite") val subTipoTramite: String?,
    @ColumnInfo(name = "activo") val activo: Boolean? = false,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "idEstadoHistVisita") val idEstadoHistVisita: Int?
)

fun HistoricModel.mapToDomain() = HistoricEntity(
    id,
    numero.orEmpty(),
    codigoSucursal.orEmpty(),
    razonSocial.orEmpty(),
    fechaRegistro.orEmpty(),
    descDireccion.orEmpty(),
    fechaRealizo.orEmpty(),
    clasificacion.orEmpty(),
    descRespRealizar.orEmpty(),
    descFuenteInfo.orEmpty(),
    descRazon.orEmpty(),
    resultado.orEmpty(),
    observacionResultado.orEmpty(),
    estado.orEmpty(),
    inspector.orEmpty(),
    idVisita.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    medidasSanitarias.orEmpty(),
    conceptoSanitario.orEmpty(),
    empresaAsociada.orEmpty(),
    nombreEmpresaAsociada.orEmpty(),
    tipoTramite.orEmpty(),
    subTipoTramite.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    usuarioModifica.orEmpty(),
    idEstadoHistVisita
)