package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.SanitaryCertificatePapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.SanitaryCertificatePapfEntity

data class SanitaryCertificatePapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<SanitaryCertificatePapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class SanitaryCertificatePapfDto(
    @SerializedName("idCertificado") val idCertificado: String?,
    @SerializedName("fechaExpedicion") val fechaExpedicion: String?,
    @SerializedName("pais") val pais: String?,
    @SerializedName("archivoSharePoint") val archivoSharePoint: String?,
    @SerializedName("archivoSesuite") val archivoSesuite: String?,
    @SerializedName("idDocumento") val idDocumento: Int? = 0,
    @SerializedName("IdTipo") val idTipo: Int? = 0,
    @SerializedName("TipoCertificado") val tipoCertificado: String?,
    @SerializedName("NombreDocumento") val nombreDocumento: String?,
)

fun SanitaryCertificatePapfDto.mapToDomain() = SanitaryCertificatePapfEntity(
    idCertificado.orEmpty(),
    fechaExpedicion.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idDocumento.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
)

fun SanitaryCertificatePapfDto.mapToDb(idSolicitud: Int) = SanitaryCertificatePapfModel(
    idCertificado.orEmpty(),
    fechaExpedicion.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idDocumento.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
    idSolicitud
)