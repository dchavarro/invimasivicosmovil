package com.soaint.sivicos_dinamico.common

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.soaint.data.common.intOrString
import com.soaint.data.utils.resizeImage
import com.soaint.domain.common.getFilePathFromUri
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.controles.Firma
import com.soaint.sivicos_dinamico.factories.controlsFactory.controles.Foto
import com.soaint.sivicos_dinamico.factories.controlsFactory.controles.OnFirmaEvent
import com.soaint.sivicos_dinamico.factories.controlsFactory.controles.OnObservacionesEvent
import com.soaint.sivicos_dinamico.factories.controlsFactory.factories.ControlFactory
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.ControlsTypes
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.TiposControl
import com.soaint.sivicos_dinamico.flow.act.ActActivity
import com.soaint.sivicos_dinamico.flow.act.ActViewModel
import com.soaint.sivicos_dinamico.flow.firm.FirmaActivity
import com.soaint.sivicos_dinamico.objetos.ObjectUri
import com.soaint.sivicos_dinamico.utils.KotlinUtils
import java.io.IOException
import java.util.*

class ControlBaseFragment : BaseFragment(), OnFirmaEvent, OnObservacionesEvent {

    var blocks: ArrayList<BlockAtributePlantillaEntity>? = null
    var blocksWithoutChecks: ArrayList<BlockAtributePlantillaEntity>? = null

    var contenedor: LinearLayout? = null
    var arrayFactories: ArrayList<ControlInterface> = arrayListOf()
    var txtRutaFirmaSelected: TextView? = null
    var imgFirmaSelected: ImageView? = null
    lateinit var kotlinUtils: KotlinUtils
    var bundle: Bundle? = null
    var controlObservacionEsperandoFoto: Foto? = null
    var fileUri: Uri? = null
    var rutaTemp: String? = null

    private val firmaRequired: Int = 1000
    private val fotoRequired: Int = 1001
    private val galleryRequired: Int = 1002

    private val viewModel by activityViewModels<ActViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        kotlinUtils = KotlinUtils()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_base, container, false)
        contenedor = view.findViewById(R.id.contenedor)

        val txtTitulo = view.findViewById<TextView>(R.id.txtTitulo)
        val txtSubTitulo = view.findViewById<TextView>(R.id.txtSubTitulo)
        txtTitulo.text = requireArguments().getString("titulo")
        txtSubTitulo.text =
            requireArguments().getString("subTitulo")?.capitalize(Locale.getDefault())
        txtSubTitulo.isVisible = !requireArguments().getString("subTitulo").isNullOrEmpty()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        bundle = arguments
        if (bundle != null) {
            blocks = Gson().fromJson(
                bundle!!.getString("blocks"),
                object : TypeToken<List<BlockAtributePlantillaEntity>>() {}.type
            )
            createViews()
        }
    }

    fun createViews() {
        arrayFactories = arrayListOf()

        // Validate radiogroup tipo checkG
        val checks: List<BlockAtributePlantillaEntity> = blocks?.filter { it.isCheckRadioButton }
            ?.groupBy { it.nombre?.dropLast(1) }?.mapValues { it.value.drop(1) }?.values?.flatten()
            .orEmpty()
        blocksWithoutChecks = ArrayList(blocks.orEmpty() - checks)

        for (control in blocksWithoutChecks!!) {
            var valorAlmacenado = ""
            if (bundle?.getString(control.nombre) != null) {
                valorAlmacenado = bundle?.getString(control.nombre).orEmpty()
            }

            when (control.idTipoAtributo) {
                TiposControl.TEXTO_CORTO -> {
                    val factory = ControlFactory.createControl(ControlsTypes.TEXTO_CORTO)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.EMAIL -> {
                    val factory = ControlFactory.createControl(ControlsTypes.TEXTO_EMAIL)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.TEXTO_LARGO -> {
                    val factory = ControlFactory.createControl(ControlsTypes.TEXTO_LARGO)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.RADIO_GROUP -> {
                    val factory = ControlFactory.createControl(ControlsTypes.RADIO_GROUP)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.LISTA -> {
                    val factory = ControlFactory.createControl(ControlsTypes.LISTA)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.LISTA_TABLA -> {
                    val factory = ControlFactory.createControl(ControlsTypes.LISTA_TABLA)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true },
                        action = { viewModel.getRequerimientos() }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.FECHA -> {
                    val factory = ControlFactory.createControl(ControlsTypes.FECHA)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.FECHA_HORA -> {
                    val factory = ControlFactory.createControl(ControlsTypes.FECHA_HORA)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.CHECK_LIST -> {
                    val factory = ControlFactory.createControl(ControlsTypes.CHECK_LIST)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.NUMERICO -> {
                    val factory = ControlFactory.createControl(ControlsTypes.NUMERICO)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.FIRMA -> {
                    val factory = ControlFactory.createControl(ControlsTypes.FIRMA) as Firma
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    factory.listener = this
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.FOTO -> {
                    val factory = ControlFactory.createControl(ControlsTypes.FOTO) as Foto
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    factory.listener = this
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                TiposControl.CHECK -> {
                    val factory = ControlFactory.createControl(ControlsTypes.CHECK)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
                else -> {
                    val factory = ControlFactory.createControl(ControlsTypes.TEXTO_LARGO)
                    val view = factory.createControl(
                        requireContext(),
                        control,
                        valorAlmacenado,
                        isEditable = { (activity as? ActActivity)?.isEditable == true }
                    )
                    arrayFactories.add(factory)
                    contenedor?.addView(view)
                }
            }
        }
    }

    fun retornarDatos(): Pair<Boolean, Map<String, String>> {
        var map = mutableMapOf<String, String>()
        var validado = true
        for ((index, factory) in arrayFactories.withIndex()) {
            val data = factory.getData()
            when (blocksWithoutChecks!![index].idTipoAtributo) {
                TiposControl.TEXTO_CORTO, TiposControl.TEXTO_LARGO, TiposControl.EMAIL -> {
                    if (blocksWithoutChecks!![index].configuracion?.requerido == true && data.isEmpty()) {
                        factory.showError(getString(R.string.campo_obligatorio))
                        validado = false
                    } else {
                        factory.clearError()
                        map[blocksWithoutChecks!![index].nombre.orEmpty()] = data
                    }
                }
                TiposControl.NUMERICO -> {
                    if (blocksWithoutChecks!![index].configuracion?.requerido == true && data.isEmpty()) {
                        factory.showError(getString(R.string.campo_obligatorio))
                        validado = false
                    } else if ((!blocksWithoutChecks!![index].configuracion?.rangoInicial.isNullOrBlank()) && data.intOrString() < blocksWithoutChecks!![index].configuracion?.rangoInicial?.toInt() ?: 0) {
                        factory.showError(
                            getString(
                                R.string.mensaje_number_no_valido_mayor,
                                blocksWithoutChecks!![index].configuracion?.rangoInicial.orEmpty()
                            )
                        )
                        validado = false
                    } else if ((!blocksWithoutChecks!![index].configuracion?.rangoFinal.isNullOrBlank()) && data.intOrString() > blocksWithoutChecks!![index].configuracion?.rangoFinal?.toInt() ?: 0) {
                        factory.showError(
                            getString(
                                R.string.mensaje_number_no_valido_menor,
                                blocksWithoutChecks!![index].configuracion?.rangoFinal.orEmpty()
                            )
                        )
                        validado = false
                    } else {
                        factory.clearError()
                        map[blocksWithoutChecks!![index].nombre.orEmpty()] = data
                    }
                }
                TiposControl.FOTO -> {
                    if (blocksWithoutChecks!![index].configuracion?.requerido == true && data == "[]") {
                        factory.showError(null)
                        validado = false
                    } else {
                        factory.clearError()
                        val photos = data.replace("[", "").replace("]", "").split(", ")
                        val path = photos.joinToString(prefix = "[", postfix = "]")
                        map[blocksWithoutChecks!![index].nombre.orEmpty()] = path.orEmpty()
                    }
                }
                TiposControl.LISTA_TABLA -> {
                    val newData =
                        GsonBuilder().create().fromJson<ArrayList<ArrayList<String>>>(data, object :
                            TypeToken<ArrayList<ArrayList<String>>>() {}.type)

                    newData.removeAt(0)
                    val dataItems = newData
                    val isValidate =
                        !dataItems.isNullOrEmpty() //&& dataItems.all { it.isNotBlank() }
                    if (blocksWithoutChecks!![index].configuracion?.requerido == true && !isValidate) {
                        factory.showError(null)
                        validado = false
                    } else {
                        factory.clearError()
                        map[blocksWithoutChecks!![index].nombre.orEmpty()] = data
                    }
                }
                else -> {
                    if (blocksWithoutChecks!![index].configuracion?.requerido == true && data.isEmpty()) {
                        factory.showError(null)
                        validado = false
                    } else {
                        factory.clearError()
                        map[blocksWithoutChecks!![index].nombre.orEmpty()] = data
                    }
                }
            }
        }

        return Pair(validado, map)
    }

    override fun onOpenDialogRequested(txt: TextView, img: ImageView) {
        txtRutaFirmaSelected = txt
        imgFirmaSelected = img
        val items = arrayOf("Firma Manual", "Firma desde Galería")
        showListDialog(R.string.seleccione, items) { position ->
            if (position == 0) onFirmaRequested(txt, img)
            else openGallery()
        }
    }

    override fun onFirmaRequested(txt: TextView, img: ImageView) {
        val intent = Intent(context, FirmaActivity::class.java)
        startActivityForResult(intent, firmaRequired)
        txtRutaFirmaSelected = txt
        imgFirmaSelected = img
    }

    override fun onFotoRequested(control: ControlInterface) {
        controlObservacionEsperandoFoto = control as Foto

        try {
            val i = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            fileUri = (kotlinUtils.getOutputMediaFileUri(requireActivity()) as ObjectUri).uri
            rutaTemp = (kotlinUtils.getOutputMediaFileUri(requireActivity()) as ObjectUri).ruta
            if (fileUri != null) {
                i.putExtra(MediaStore.EXTRA_OUTPUT, fileUri)
                startActivityForResult(i, fotoRequired)
            } else {
                return
            }
        } catch (e: ActivityNotFoundException) {
            val toast = Toast.makeText(
                context,
                requireContext().getString(R.string.mensaje_no_camara),
                Toast.LENGTH_SHORT
            )
            toast.show()
        }
    }

    override fun onMaxFotosReached() {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.mensaje_maximo_fotos),
                titleBtnPositive = getString(R.string.cerrar),
            ) { dismissAlertDialog() }
        )
    }

    fun openGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(
            Intent.createChooser(intent, getString(R.string.seleccione)),
            galleryRequired
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && requestCode == firmaRequired) {
            val ruta = data!!.getStringExtra("bmp")
            txtRutaFirmaSelected!!.text = ruta!!
            val bmp = BitmapFactory.decodeFile(ruta)
            if (bmp != null) {
                imgFirmaSelected!!.setImageBitmap(bmp)
            } else {
                showAlertDialog(
                    showDialog(
                        getString(R.string.error),
                        getString(R.string.guardado_firma_error),
                        titleBtnPositive = getString(R.string.cerrar),
                    ) { dismissAlertDialog() }
                )
            }
        } else if (resultCode == AppCompatActivity.RESULT_OK && requestCode == fotoRequired) {
            if (!rutaTemp.isNullOrEmpty()) {
                var bmp = BitmapFactory.decodeFile(rutaTemp!!)
                val ratio: Float = Math.min(650f / bmp.getWidth(), 650f / bmp.getHeight())
                val width = Math.round(ratio * bmp.getWidth()).toInt()
                val height = Math.round(ratio * bmp.getHeight()).toInt()
                val image = resizeImage(rutaTemp!!, width, height)

                controlObservacionEsperandoFoto!!.addFoto(image, requireContext())
            }
        } else if (resultCode == AppCompatActivity.RESULT_OK && requestCode == galleryRequired) {
            if (data != null) {
                try {
                    val bitmap: Bitmap? =
                        MediaStore.Images.Media.getBitmap(
                            requireActivity().contentResolver,
                            data.data
                        )
                    val path = getFilePathFromUri(requireContext(), data.data!!)
                    txtRutaFirmaSelected!!.text = path
                    imgFirmaSelected!!.setImageBitmap(bitmap)
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
}