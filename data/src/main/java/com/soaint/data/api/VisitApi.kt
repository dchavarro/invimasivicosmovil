package com.soaint.data.api

import com.soaint.data.common.CODE_DOCUMENTARY_CLASSIFICATION_EJECU
import com.soaint.data.model.*
import retrofit2.http.*

interface VisitApi {

    @GET("v1/visita")
    suspend fun getVisits(
        @Query("idTarea") idTask: String
    ): VisitResponseDto

    @GET("v1/visita/antecedente")
    suspend fun getAntecedent(
        @Query("idVisita") idVisita: String,
    ): AntecedentResponseDto

    @GET("v1/visita/historico")
    suspend fun getHistorico(
        @Query("razonSocial") razonSocial: String,
        @Query("orderBy") orderBy: String = "-fechaCreacion",
        @Query("page") page: String = "0",
        @Query("pageSize") pageSize: String = "2",
    ): HistoricResponseDto

    @GET("v1/visita/muestra")
    suspend fun getSamples(
        @Query("id") idVisita: String,
    ): SampleResponseDto

    @GET("v1/visita/{idVisita}/transporte/datos-adicionales")
    suspend fun getInfoTransport(
        @Path("idVisita") idVisita: String,
    ): TransportResponseDto

    @POST("v1/visita/transporte")
    suspend fun sendTransport(
        @Body body: TransportBodyDto
    ): Unit

    @GET("v1/visita/{idVisita}/transporte?codigoTipoTransporte=TTRA_DEC")
    suspend fun getAllTransport(
        @Path("idVisita") idVisita: String,
    ): TransportBodyResponseDto

    @GET("v1/visita/{idVisita}/documentos?codigoClasificacion=EJECU")
    suspend fun getDocument(
        @Path("idVisita") idVisita: String,
    ): DocumentResponseDto

    @GET("v1/tipos-documentales/grupos")
    suspend fun getNameDocument(
        @Query("idGrupo") idGrupo: Int,
        @Query("codigoClasificacion") codigoClasificacion: String= CODE_DOCUMENTARY_CLASSIFICATION_EJECU,
    ): DocumentNameResponseDto

    @GET("v1/visita/empresa/notificar")
    suspend fun getEmail(
        @Query("idVisita") idVisita: String,
    ): NotificationResponseDto

    @POST("v1/visita/empresa/notificar")
    suspend fun saveEmails(
        @Body body: NotificationBodyDto
    ): Unit

    @POST("v1/visita/funcionarios")
    suspend fun getOfficialVisit(
        @Body body: BodyGenericVisitDto
    ): VisitOfficialResponseDto

    @GET("v1/visita/{idVisita}/grupo")
    suspend fun getGroup(
        @Path("idVisita") idVisita: String,
    ): VisitGroupResponseDto

    @GET("v1/visita/{idVisita}/categoria")
    suspend fun getSubGroup(
        @Path("idVisita") idVisita: String,
    ): VisitSubGroupResponseDto

    @PUT("v1/visita")
    suspend fun updateVisit(
        @Body body: VisitDto
    ): Unit

    @PUT("v1/visita/antecedente")
    suspend fun updateAntecedent(
        @Body body: List<AntecedentDto>
    ): Unit

    @POST("v1/visita/antecedente")
    suspend fun createAntecedent(
        @Body body: AntecedentBodyDto
    ): Unit

    @GET("v1/visita/{idVisita}/interacciones")
    suspend fun getInteraction(
        @Path("idVisita") idVisita: String,
    ): InteractionResponseDto

    @POST("v1/visita/interacciones")
    suspend fun updateInteraction(
        @Body body: BodyGenericInteractionDto
    ): Unit

    @POST("v1/visita/copia")
    suspend fun copyVisit(
        @Body body: BodyGenericVisitDto
    ): Unit

    @POST("v1/visita/funcionario/ubicacion")
    suspend fun location(
        @Body body: LocationBodyDto
    ): Unit

    @GET("v1/visita/muestra/{idMuestra}/analisis-requerido")
    suspend fun getAnalysisRequired(
        @Path("idMuestra") idMuestra: String,
    ): SampleAnalysisResponseDto

    @GET("v1/visita/muestra/{idMuestra}/plan-muestreo")
    suspend fun getSamplingPlan(
        @Path("idMuestra") idMuestra: String,
    ): SamplePlanResponseDto

    @GET("v1/visita/fecha/medida/sanitaria")
    suspend fun getDateMssApplied(
        @Query("idVisita") idVisita: Int,
        @Query("idMedida") idMedida: Int,
        @Query("fecha") fecha: String
    ): DateMssAppliedResponseDto

}