package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.MisionalModel
import com.soaint.data.room.tables.PlantillaModel
import com.soaint.domain.model.MisionalEntity
import com.soaint.domain.model.PlantillaEntity

data class PlantillasResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val data: List<PlantillaDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class PlantillaDto(
    @SerializedName("activo") val activo: Boolean? = true,
    @SerializedName("codigo") val codigo: String? = null,
    @SerializedName("descEstado") val descEstado: String? = null,
    @SerializedName("descripcion") val descripcion: String? = null,
    @SerializedName("fechaCreacion") val fechaCreacion: String? = null,
    @SerializedName("fechaModifica") val fechaModifica: String? = null,
    @SerializedName("id") val id: Int,
    @SerializedName("idEstado") val idEstado: Int? = null,
    @SerializedName("misionales") val misionales: List<MisionalDto>? = null,
    @SerializedName("nombre") val nombre: String? = null,
    @SerializedName("plantillaOriginal") val plantillaOriginal: String? = null,
    @SerializedName("usuarioCrea") val usuarioCrea: String? = null,
    @SerializedName("usuarioModifica") val usuarioModifica: String? = null,
    @SerializedName("version") val version: Int? = null,
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int? = null,
)

data class MisionalDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("idTipoProducto") val idTipoProducto: Int?,
    @SerializedName("descripcion") val descripcion: String?,
)

fun PlantillaDto.mapToDomain() = PlantillaEntity(
    activo,
    codigo,
    descEstado,
    descripcion,
    fechaCreacion,
    fechaModifica,
    id,
    idEstado,
    misionales?.map { it.mapToDomain(id) },
    nombre,
    plantillaOriginal,
    usuarioCrea,
    usuarioModifica,
    version,
    idTipoDocumental
)

fun MisionalDto.mapToDomain(idPlantilla: Int) = MisionalEntity(
    id,
    idTipoProducto,
    descripcion,
    idPlantilla
)

fun PlantillaDto.mapToDb() = PlantillaModel(
    id,
    nombre,
    activo,
    codigo,
    descEstado,
    descripcion,
    fechaCreacion,
    fechaModifica,
    idEstado,
    plantillaOriginal,
    usuarioCrea,
    usuarioModifica,
    version,
    idTipoDocumental
)

fun MisionalDto.mapToDb(idPlantilla: Int) = MisionalModel(
    id,
    idTipoProducto,
    descripcion,
    idPlantilla
)
