plugins {
    id(Pluggins.androidLibrary)
    kotlin(Pluggins.kotlinAndroid)
    id(Pluggins.kotlinParcelizeExtensions)
    kotlin(Pluggins.kotlinKapt)
}

android {
    compileSdkVersion(Application.compileSdk)
    buildToolsVersion = Application.buildToolsVersion

    defaultConfig {
        minSdk = Application.minSdk
        targetSdk = Application.targetSdk
        versionCode = 1
        versionName = "1.0"
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("debug") {
            isDebuggable = true
        }
        create("qa") {
            initWith(buildTypes.getByName("debug"))
            signingConfig = signingConfigs.getByName("debug")
            isDebuggable = true
        }
        create("qaInvima") {
            initWith(buildTypes.getByName("debug"))
            signingConfig = signingConfigs.getByName("debug")
            isDebuggable = true
        }
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    //Kotlin
    Libraries.kotlinLibraries.forEach {
        implementation(it)
    }

    //Kotlin KTX
    Libraries.kotlinKtx.forEach {
        implementation(it)
    }

    //Google
    Libraries.googleMaps.forEach {
        implementation(it)
    }

    //Retrofit
    Libraries.retrofitLibraries.forEach { implementation(it) }

    //Hilt
    Libraries.daggerLibrary.forEach { implementation(it) }
    Libraries.daggerKaptLibrary.forEach { kapt(it) }
}