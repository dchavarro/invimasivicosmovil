package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.room.tables.TaskModel
import com.soaint.data.room.tables.TaskPapfModel

@Dao
interface TasksPapfDao {

    @Query("SELECT * FROM task_papf_table WHERE usuarioRed = :usuarioRed and estado IN (:statusPapf)")
    suspend fun selectTask(usuarioRed: String,  statusPapf: List<String>): List<TaskPapfModel>

    @Insert(entity = TaskPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTask(task: TaskPapfModel)

    @Query("DELETE FROM task_papf_table")
    suspend fun deleteAllTask()

    @Update(entity = TaskPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTask(visita: TaskPapfModel)

    @Query("SELECT * FROM task_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectTaskById(idSolicitud: Int): TaskPapfModel?
}




