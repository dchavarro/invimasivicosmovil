package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.*

enum class ActValidate(val act: Array<String>) {
    RAVI_INPER(
        arrayOf(
            IVC_VIG_FM003,
            IVC_VIG_FM004,
            IVC_VIG_FM005,
        )
    ),
}