package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.ROLS_AIFI
import com.soaint.sivicos_dinamico.utils.ROLS_ASGPAPF
import com.soaint.sivicos_dinamico.utils.ROLS_FIPAPF
import com.soaint.sivicos_dinamico.utils.ROLS_INPAPF
import com.soaint.sivicos_dinamico.utils.ROLS_VERIDOC


enum class Roles(val rol: Array<String>) {
    ROLESPAPF(
        arrayOf(
            ROLS_FIPAPF,
            ROLS_ASGPAPF,
            ROLS_AIFI,
            ROLS_INPAPF,
            ROLS_VERIDOC
        )
    )
}