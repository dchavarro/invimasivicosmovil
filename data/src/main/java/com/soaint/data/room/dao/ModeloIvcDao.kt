package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.IvcDaModel
import com.soaint.data.room.tables.ModeloIvcModel

@Dao
interface ModeloIvcDao {


    @Query("SELECT * FROM ivc_req_table")
    suspend fun selectAllIvc(): List<ModeloIvcModel>?

    @Query("SELECT * FROM ivc_req_table WHERE idVisita = :idVisita AND isDeleted = :isDeleted ORDER BY id DESC")
    suspend fun selectIvc(idVisita: Int, isDeleted: Boolean = false): List<ModeloIvcModel>?

    @Query("SELECT * FROM ivc_req_table WHERE isDeleted = :isDeleted AND isLocal = :isLocal")
    suspend fun selectIvcAll(isDeleted: Boolean, isLocal: Boolean): List<ModeloIvcModel>?

    @Insert(entity = ModeloIvcModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertIvc(model: ModeloIvcModel)

    @Query("UPDATE ivc_req_table SET isDeleted = :isDeleted WHERE id = :id ")
    suspend fun updateIvc(id: Int, isDeleted: Boolean)

    @Query("DELETE FROM ivc_req_table WHERE id = :id")
    suspend fun deleteIvcById(id: Int)

    @Query("DELETE FROM ivc_req_table WHERE idVisita = :idVisita")
    suspend fun deleteIvc(idVisita: String)


// DATOS ADICIONALES REQUERIMIENTOS
    @Insert(entity = IvcDaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertIvcDa(model: IvcDaModel)

    @Query("DELETE FROM ivc_da_table WHERE idVisita = :idVisita")
    suspend fun deleteIvcDa(idVisita: String)

    @Query("DELETE FROM ivc_da_table WHERE id = :id")
    suspend fun deleteIvcDaById(id: Int)

    @Query("SELECT * FROM ivc_da_table WHERE isLocal = :isLocal")
    suspend fun selectIvcDaLocal(isLocal: Boolean): List<IvcDaModel>?

    @Query("SELECT * FROM ivc_da_table WHERE idVisita = :idVisita")
    suspend fun selectIvcDaByVisit(idVisita: String): IvcDaModel?

    @Query("SELECT COUNT(id) FROM ivc_req_table WHERE isLocal = :isLocal")
    suspend fun selectCountIvcReqLocal(isLocal: Boolean = true): Int

    @Transaction
    suspend fun insertOrUpdateDA(model: IvcDaModel) {
        val item = selectIvcDa(model.idVisita.orZero().toString().orEmpty())
        if (!item.isNullOrEmpty()) {
            model.id = item.firstOrNull()?.id.orZero()
            updateDA(model)
        }
        else insertDA(model)
    }

    @Query("SELECT * FROM ivc_da_table WHERE idVisita = :idVisita AND isLocal = :isLocal")
    suspend fun selectIvcDa(idVisita: String, isLocal: Boolean = true): List<IvcDaModel>

    @Update(entity = IvcDaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateDA(model: IvcDaModel)

    @Insert(entity = IvcDaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDA(model: IvcDaModel)
}