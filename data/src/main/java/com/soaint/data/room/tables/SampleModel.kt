package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.SampleAnalysisEntity
import com.soaint.domain.model.SampleEntity
import com.soaint.domain.model.SamplePlanEntity

@Entity(tableName = "sample_visit_table")
class SampleModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "descGrupoProducto") val descGrupoProducto: String?,
    @ColumnInfo(name = "descParametroEvaluar") val descParametroEvaluar: String?,
    @ColumnInfo(name = "descSubGrupoProducto") val descSubGrupoProducto: String?,
    @ColumnInfo(name = "descTipoProducto") val descTipoProducto: String?,
    @ColumnInfo(name = "expedienteProducto") val expedienteProducto: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "idGrupoProducto") val idGrupoProducto: Int?,
    @ColumnInfo(name = "idSubGrupoProducto") val idSubGrupoProducto: Int?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "identificacionMuestra") val identificacionMuestra: String?,
    @ColumnInfo(name = "muestraTomarProducto") val muestraTomarProducto: String?,
    @ColumnInfo(name = "nombreGenericoProducto") val nombreGenericoProducto: String?,
    @ColumnInfo(name = "nombreProducto") val nombreProducto: String?,
    @ColumnInfo(name = "numLoteProducto") val numLoteProducto: String?,
    @ColumnInfo(name = "numRegisSaniProducto") val numRegisSaniProducto: String?,
    @ColumnInfo(name = "referencia") val referencia: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,

    )

fun SampleModel.mapToDomain() = SampleEntity(
    id, activo, descGrupoProducto, descParametroEvaluar, descSubGrupoProducto, descTipoProducto,
    expedienteProducto, fechaCreacion, fechaModifica, idGrupoProducto, idSubGrupoProducto,
    idTipoProducto, idVisita, identificacionMuestra, muestraTomarProducto, nombreGenericoProducto,
    nombreProducto, numLoteProducto, numRegisSaniProducto, referencia, usuarioCrea
)

@Entity(tableName = "sample_analysis_table")
class SampleAnalysisModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idMuestra") val idMuestra: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "parametroEvaluar") val parametroEvaluar: String?,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)

fun SampleAnalysisModel.mapToDomain() = SampleAnalysisEntity(
    id, nombre, idMuestra, activo, usuarioCrea, fechaModifica, parametroEvaluar
)

@Entity(tableName = "sample_plan_table")
class SamplePlanModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "idMuestra") val idMuestra: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)

fun SamplePlanModel.mapToDomain() = SamplePlanEntity(
    id, descripcion, idMuestra, activo, usuarioCrea, fechaModifica
)