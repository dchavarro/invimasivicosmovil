package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.UserRolesEntity

data class UserInformationBodyDto(
    @SerializedName("Auditoria") val auditoria: AuditoriaDto,
    @SerializedName("Parametros") val parametros: ParametrosDto
) {

    constructor(ip: String, user: String) : this(
        AuditoriaDto(ip, user), ParametrosDto(user)
    )
}

data class AuditoriaDto(
    @SerializedName("ip") val iP: String,
    @SerializedName("usuario") val usuario: String
)

data class AuditoriaCapitalDto(
    @SerializedName("Ip") val iP: String,
    @SerializedName("Usuario") val usuario: String
)

data class InputVisitDto(
    @SerializedName("idVisita") val idVisita: Int,
)

data class ParametrosDto(
    @SerializedName("Usuario") val usuario: String
)

data class ParametrosPapfDto(
    @SerializedName("tamanio") val tamanio: String = EMPTY_STRING,
    @SerializedName("pagina") val pagina: String = EMPTY_STRING,
)

data class UserInformationDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val userData: UserDataObjectDto,
    @SerializedName("statusCode") val statusCode: Int
)

fun UserInformationDto.mapToDomain() = UserInformationEntity(
    userData.user.firstOrNull()?.ciudad.orEmpty(),
    userData.user.firstOrNull()?.correoElectronico.orEmpty(),
    userData.user.firstOrNull()?.departamento.orEmpty(),
    userData.user.firstOrNull()?.direccion.orEmpty(),
    userData.user.firstOrNull()?.direccionMisional.orEmpty(),
    userData.user.firstOrNull()?.idMisional.intOrString(),
    userData.user.firstOrNull()?.numeroIdentidad.orEmpty(),
    userData.user.firstOrNull()?.primerApellido.orEmpty(),
    userData.user.firstOrNull()?.primerNombre.orEmpty(),
    userData.user.firstOrNull()?.segundoApellido.orEmpty(),
    userData.user.firstOrNull()?.segundoNombre.orEmpty(),
    userData.user.firstOrNull()?.tipoIdentidad.orEmpty(),
    userData.user.firstOrNull()?.usuario.orEmpty(),
    userData.user.firstOrNull()?.idPersona.orZero(),
    userData.user.firstOrNull()?.idUsuario.orZero(),
    userData.user.firstOrNull()?.grupoDependencia.orEmpty(),
    userData.user.firstOrNull()?.idDependencia.intOrString(),
    userData.user.firstOrNull()?.idFuncionario.orZero(),
    userData.user.firstOrNull()?.cargo.orEmpty(),
)

fun UserRolesDto.mapToDomain() = UserRolesEntity(
    idRol.orZero(),
    codigo.orEmpty(),
    rol.orEmpty(),
)

data class UserDataDto(
    @SerializedName("Ciudad") val ciudad: String?,
    @SerializedName("CorreoElectronico") val correoElectronico: String?,
    @SerializedName("Departamento") val departamento: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("DireccionMisional") val direccionMisional: String?,
    @SerializedName("idMisional") val idMisional: String?,
    @SerializedName("NumeroIdentidad") val numeroIdentidad: String?,
    @SerializedName("PrimerApellido") val primerApellido: String?,
    @SerializedName("PrimerNombre") val primerNombre: String?,
    @SerializedName("SegundoApellido") val segundoApellido: String?,
    @SerializedName("SegundoNombre") val segundoNombre: String?,
    @SerializedName("TipoIdentidad") val tipoIdentidad: String?,
    @SerializedName("Usuario") val usuario: String?,
    @SerializedName("idPersona") val idPersona: Int?,
    @SerializedName("IdUsuario") val idUsuario: Int?,
    @SerializedName("grupoDependencia") val grupoDependencia: String?,
    @SerializedName("IdDependencia") val idDependencia: String?,
    @SerializedName("idFuncionario") val idFuncionario: Int?,
    @SerializedName("Cargo") val cargo: String?
)

data class UserRolesDto(
    @SerializedName("idRol") val idRol: Int?,
    @SerializedName("Codigo") val codigo: String?,
    @SerializedName("Rol") val rol: String?
)

data class UserDataObjectDto(
    @SerializedName("Usuario") val user: List<UserDataDto>,
    @SerializedName("Roles") val roles: List<UserRolesDto>
)

data class MessageDto(
    @SerializedName("cantidadRegistros") val cantidadRegistros: String,
    @SerializedName("codigoConfirmacion") val codigoConfirmacion: String,
    @SerializedName("mensajeConfirmacion") val mensajeConfirmacion: String
)