package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InfoTramitePapfSolicitudEntity(
    val solicitud: List<InfoTramitePapfEntity>,
) : Parcelable

@Parcelize
data class InfoTramitePapfEntity(
    val idSolicitud: Int? = 0,
    val numeroSolicitud: Int? = 0,
    val radicado: String?,
    val idCategoriaTramite: Int? = 0,
    val descripcionCategoria: String?,
    val idTipoTramite: Int? = 0,
    val tipoTramite: String?,
    val tipoImportacion: String?,
    val idTipoProducto: Int? = 0,
    val tipoProducto: String?,
    val importador: String?,
    val idOficinaInvima: String?,
    val nombreDependencia: String?,
    val estado: Int? = 0,
    val direccion: String?,
    val telefono: String?,
    val numeroDocumento: String?,
    val tipoDocumento: String?,
    val descripcionTipoInspeccion: String?,
    val idSolicitudTramite: Int? = 0,
    val codigoPapf: String?,
) : Parcelable

@Parcelize
data class InvoicePapfEntity(
    val idSolicitud: Int? = 0,
    val numeroFactura: String?,
    val fechaExpedicion: String?,
    val idDocumento: Int? = 0,
    val numeroDocumento: String?,
    val nombreEmpresa: String?,
    val idPais: Int?,
    val pais: String?,
) : Parcelable

@Parcelize
data class TransportObjectPapfEntity(
    val empresa: List<TransportComPapfEntity>,
    val documentos: List<TransportDocPapfEntity>,
) : Parcelable

@Parcelize
data class TransportComPapfEntity(
    val nombreEmpresa: String?,
    val numeroDocumento: String?,
) : Parcelable

@Parcelize
data class TransportDocPapfEntity(
    val idSolicitud: Int? = 0,
    val tipoDocumento: String?,
    val numeroDocumento: String?,
    val idDocumento: Int? = 0,
    val nombreDocumento: String?,
    val identificacionTipoTransporte: String?,
    val contenedor: String?,
) : Parcelable

@Parcelize
data class RegisterProductObjectPapfEntity(
    val registroProductos: List<RegisterProductPapfEntity>?,
    val destinoLoteSolicitud: List<DestinoLotePapfEntity>?,
    val documentos: List<DocumentPapfEntity>?,
) : Parcelable

@Parcelize
data class RegisterProductPapfEntity(
    val idSolicitud: Int? = 0,
    val idProductoSolicitud: Int? = 0,
    val idClasificacionProducto: Int? = 0,
    val clasificacionProducto: String?,
    val producto: String?,
    val registroSanitario: String?,
    val idEmpresaFabricante: Int? = 0,
    val origenFabricante: String?,
    val idDetalleProducto: Int? = 0,
    val lote: String?,
    val cantidad: String?,
    val presentacionComercial: String?,
    val pesoUnidadMedida: Double? = 0.0,
    val unidadDeMedida: String?,
    val pesoNeto: String?,
    val marca: String?,
    val temperaturaConservacion: String?,
    val fechaVencimiento: String?
) : Parcelable


@Parcelize
data class DestinoLotePapfEntity(
    val numeroDocumento: String?,
    val idSedeEmpresaDestino: Int? = 0,
    val razonSocial: String?,
    val sucursal: String?,
    val departamento: String?,
    val municipio: String?,
    val direccion: String?,
    val telefono: String?,
    val contacto: String?,
    val idDestinoProducto: Int? = 0,
    val descripcionDestinoProducto: String?,
    val lote: String?,
    val cantidad: Int? = 0,
    val idMPIG: Int? = 0,
    val descripcionUsoMPIG: String?,
) : Parcelable

@Parcelize
data class DocumentPapfEntity(
    val idDocumento: Int? = 0,
    val nombreDocumento: String?,
) : Parcelable


@Parcelize
data class SanitaryCertificatePapfEntity(
    val idCertificado: String?,
    val fechaExpedicion: String?,
    val pais: String?,
    val archivoSharePoint: String?,
    val archivoSesuite: String?,
    val idDocumento: Int? = 0,
    val idTipo: Int? = 0,
    val tipoCertificado: String?,
    val nombreDocumento: String?,
) : Parcelable

@Parcelize
data class DocumentationPapfEntity(
    val idDocumento: Int? = 0,
    val idSolicitud: Int? = 0,
    val idTipoDocumento: Int? = 0,
    val tipoDocumento: String?,
    val numero: String?,
    val tipoAdicional: String?,
    val fechaExpedicion: String?,
    val numeroPlanta: String?,
    val contenedor: String?,
    val pais: String?,
    val archivoSharePoint: String?,
    val archivoSesuite: String?,
    val idTipo: Int? = 0,
    val tipoCertificado: String?,
    val nombreDocumento: String?,
    val folios: String?,
    val tipo: String?,
    val clasificacion: String?,
) : Parcelable

@Parcelize
data class RegisterDateBodyEntity(
    val fecha: String?,
    val observaciones: String?,
    val idSolicitud: Int,
) : Parcelable

@Parcelize
data class InfoLegalPapfEntity(
    val informacionNotificacion: List<InfoNotificationPapfEntity>,
) : Parcelable

@Parcelize
data class InfoNotificationPapfEntity(
    val tipoNotificacion: String?,
    val direccion: String?,
    val correoElectronico: String?
) : Parcelable

@Parcelize
data class DinamicQuerysPapfEntity(
    val id: Int? = 0,
    val descripcion: String? = EMPTY_STRING,
    val codeQuery: String? = EMPTY_STRING,
    val idTipoTramite: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class DataReqInsSanPapfEntity(
    val idEstadoEmpaque: Int? = 0,
    val descripcionEmpaque: String? = EMPTY_STRING,
    val idConcepto: Int? = 0,
    val descripcionConcepto: String? = EMPTY_STRING,
    val condicionesAlmacenamiento: String? = EMPTY_STRING,
    val observaciones: String? = EMPTY_STRING,
    val detalleProducto: Int? = 0,
    val idSolicitud: Int? = 0,
) : Parcelable

@Parcelize
data class DataReqActSamplePapfEntity(
    val nroUnidades: Int? = 0,
    val descripcionUnidades: String? = EMPTY_STRING,
    val unidadesMedida: Int? = 0,
    val presentacion: Int? = 0,
    val descripcionPresentacion: String? = EMPTY_STRING,
    val contenidoNeto: Int? = 0,
    val idProducto: Int? = 0,
    val idSolicitud: Int? = 0,
) : Parcelable

@Parcelize
data class CloseInspectionPapfEntity(
    val clasificacionTramite: ClasificationTramitPapfEntity?,
) : Parcelable

@Parcelize
data class ClasificationTramitPapfEntity(
    val idTipoProductoPapf: Int? = 0,
    val tipoProductoPapf: String? = null,
    val idActividad: Int? = 0,
    val actividad: String? = null,
    val idTipoActividad: Int? = 0,
    val tipoActividad: String? = null,
    val idIdiomaCis: Int? = 0,
    val idiomaCis: String? = null,
    val certificadoExportacion: Boolean? = null,
    val idSolicitud: Int? = 0,
    val idIdioma: Int? = 0,
    val idioma: String? = null,
    val idFirmante: Int? = 0,
    val firmante: String? = null,
    val idResultadoCIS: Int? = 0,
    val resultadoCIS: String? = null,
    val idUsoMPIG: Int? = 0,
    val usoMPIG: String? = null,
    val justificacion: String? = null,
    val respuestaRequerimientoIF: Boolean? = null,
) : Parcelable

@Parcelize
data class CloseInsFisBodyEntity(
    val observaciones: List<CloseInsFisObservationBodyEntity>? = null,
    val idTipoProducto: Int? = 0,
    val idActividad: Int? = 0,
    val idIdioma: Int? = 0,
    val idFirmante: Int? = 0,
    val idResultadoCIS: Int? = 0,
    val idUsoMPIG: Int? = 0,
    val justificacion: String?,
    val respuestaRequerimientoIF: Boolean?,
    val idSolicitud: Int? = 0,
) : Parcelable

@Parcelize
data class CloseInsFisObservationBodyEntity(
    val descripcion: String?,
) : Parcelable

@Parcelize
data class CloseInspObservationPapfEntity(
    val descripcion: String?,
    val idSolicitud: Int? = 0,
    val id: Int? = 0,
    val isDefault: Boolean = false
) : Parcelable

@Parcelize
data class EmitCisPapfEntity(
    val cierre: EmitCisClosePapfEntity?,
    val observaciones: List<EmitCisObservationPapfEntity>?,
) : Parcelable

@Parcelize
data class EmitCisClosePapfEntity(
    val idSolicitud: Int? = 0,
    val idUsoMPIG: Int? = 0,
    val descripcionUsoMPIG: String? = null,
    val idResultadoCIS: Int? = 0,
    val descripcionResultadoCis: String? = null,
    val idFirmante: Int? = 0,
    val firmante: String? = null,
    val idIdioma: Int? = 0,
    val descripcionIdioma: String? = null,
    val idCertificadoExporta: Int? = null,
    val descripcionCertificadoExporta: String? = null,
    val idTipoCertificado: Int? = null,
    val descripcionTipoCertificado: String? = null,
    val idReembarque: Int? = null,
    val descripcionReembarque: String? = null,
    val idMSS: Int? = null,
    val descripcionMSS: String? = null,
) : Parcelable

@Parcelize
data class EmitCisObservationPapfEntity(
    val descripcion: String?,
    val idSolicitud: Int? = 0,
    val id: Int? = 0,
    val isDefault: Boolean? = false
) : Parcelable

@Parcelize
data class InfoEmitirPapfEntity(
    val funcionarioInvima: String?,
    val nombreLaboratorio: String?,
) : Parcelable

@Parcelize
data class CheckListEntity(
    val idListaChequeo: Int?,
    val listaChequeo: String?,
    val observacionListaChequeo: String?,
    val verificacionInformacion: String?,
    val requerimiento: String?,
    val contenedorListaChequeo: String?,
    val idSolicitud: Int?,
    val nombreGeneraListaChequeo: String?,
    val fechaCreacion: String?
) : Parcelable


// DETALLE PRODUCTO PAPF
@Parcelize
data class DetailProductObjectPapfEntity(
    val informacionProducto: List<DetailProductPapfEntity>?,
    val subpartida: List<SubPartidaPapfEntity>?,
    val expedidor: List<ExpedidorPapfEntity>?,
    val fabricante: List<FabricantePapfEntity>?,
    val informacionComplementaria: List<InfoComplementariaPapfEntity>?,
    val destinatario: List<DestinatarioPapfEntity>?,
    val lugarDestino: List<DestinatarioPapfEntity>?,
    val operadorResponsable: List<DestinatarioPapfEntity>?,
    val puertoControlFronterizo: List<DestinatarioPapfEntity>?,
    val licenciasImportacion: List<DestinatarioPapfEntity>?,
) : Parcelable

@Parcelize
data class DetailProductPapfEntity(
    val idSolicitud: Int? = 0,
    val idProductoSolicitud: Int? = 0,
    val expediente: String?,
    val tieneRegistro: Boolean?,
    val registroSanitario: String?,
    val fechaRegistro: String?,
    val estado: String?,
    val idClasificacionProducto: Int? = 0,
    val clasificacionProducto: String?,
    val nombreProducto: String?,
    val grupoProducto: String?,
    val categoriaAlimento: String?,
    val subCategoriaAlimento: String?,
) : Parcelable

@Parcelize
data class SubPartidaPapfEntity(
    val subPartidaPapf: String?,
    val descripcion: String?,
) : Parcelable

@Parcelize
data class ExpedidorPapfEntity(
    val idExpedidor: Int? = 0,
    val razonSocial: String?,
    val nombre: String?,
    val direccion: String?,
    val pais: String?,
    val departamento: String?,
    val municipio: String?,
    val telefono: String?,
    val codigoISOExpedidor: String?,
    val codigoPostalExpedidor: String?,
) : Parcelable

@Parcelize
data class FabricantePapfEntity(
    val idEmpresaFabricante: Int? = 0,
    val fabricante: String?,
    val nombre: String?,
    val direccion: String?,
    val pais: String?,
    val departamento: String?,
    val municipio: String?,
    val telefono: String?,
    val codigoISOFabricante: String?,
    val codigoPostalFabricante: String?,
    val idPlantaAutorizada: String?,
    val plantaAutorizada: String?,
) : Parcelable

@Parcelize
data class InfoComplementariaPapfEntity(
    val requiereInformacionComplementaria: Boolean?,
    val especie: String?,
    val estadoMateria: String?,
    val codigoProductoSA: String?,
    val certificadoComo: String?,
    val fechaFabricacion: String?,
    val consumidorFinal: Boolean?,
    val naturalezaMercancia: String?,
) : Parcelable

@Parcelize
data class DestinatarioPapfEntity(
    val pais: String?,
    val ciudad: String?,
    val nombre: String?,
    val direccion: String?,
    val telefono: String?,
    val codigoPostal: String?,
    val codigoISO: String?,
    val plantaAutorizadaDestino: String?,
    val autoridad: String?,
    val typeList: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class SignatoriesPapfEntity(
    val idUsuario: Int? = 0,
    val idFuncionario: Int?= 0,
    val nombre: String? = EMPTY_STRING
) : Parcelable

@Parcelize
data class ConsecutivePapfEntity(
    val consecutivo: String? = EMPTY_STRING,
    val idSolicitud: Int = 0
) : Parcelable

@Parcelize
data class SaveInfoActsPapfEntity(
    val idPlantilla: Int?,
    val idSolicitud: String?,
    val expedicionCIS: String?,
    val otros: String?,
    val cual: String?,
    val consecutivo: String?,
    val fechaRegistro: String?,
    val funcionarioInvima: String?,
    val fechaInicio: String?,
    val fechaFin: String?,
    val tramiteRequerimiento: Int?,
    val descripcionRequerimiento: String?,
    val nombreUsuario: String?,
    val nombreLaboratorio: String?,
) : Parcelable