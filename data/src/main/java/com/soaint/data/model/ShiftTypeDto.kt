package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.ShiftTypeModel
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ShiftTypeEntity

data class ShiftTypeResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ShiftTypeDto>,
    @SerializedName("statusCode") val statusCode: Int
)
data class ShiftTypeDto (
    @SerializedName ("id") var id: Int?,
    @SerializedName("jornada") val jornada: String,
    @SerializedName("turno") val turno: String,
    @SerializedName("activo") val activo: Boolean,
    @SerializedName("usuarioCrea") val usuarioCrea: String? = EMPTY_STRING,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("codigoJornada") val codigoJornada: String,
    @SerializedName("horaInicio") val horaInicio: String?,
    @SerializedName("horaFin") val horaFin: String?
)

fun ShiftTypeDto.mapToDomain() = ShiftTypeEntity(
    id, jornada, turno, activo,usuarioCrea, fechaCreacion, codigoJornada, horaInicio, horaFin
)

fun ShiftTypeDto.mapToDb() = ShiftTypeModel(
    id, jornada, turno, activo, usuarioCrea, fechaCreacion, codigoJornada, horaInicio, horaFin
)