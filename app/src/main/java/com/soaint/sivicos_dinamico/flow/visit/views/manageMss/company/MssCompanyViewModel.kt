package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.company

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class onMsspViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onMsspViewModelEvent()
    data class onViewModelReadyAmssp(val amssp: List<ManageMssEntity>): onMsspViewModelEvent()
    object onViewModelReadyAmsspCheck: onMsspViewModelEvent()
    data class onViewModelReadyEmssp(val emssp: List<ManageMssEntity>): onMsspViewModelEvent()
    data class onViewModelReadyMssActivity(val typeActivity: List<ManageMssEntity>): onMsspViewModelEvent()
    data class onViewModelReadyMsspList(val msspList: List<ManageMsspEntity>): onMsspViewModelEvent()
    object onViewModelUpdateMsspList: onMsspViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onMsspViewModelEvent()
    data class onSnackLoadingError(@StringRes val message: Int) : onMsspViewModelEvent()
}

class MssCompanyViewModel @Inject constructor(
    private val manageMssUseCase: ManageMssUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onMsspViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _amsspSelected = MutableLiveData<ManageMssEntity?>()
    var amsspSelected = _amsspSelected.asLiveData()

    private var amssp: List<ManageMssEntity> = emptyList()

    //private var amsspSelected: ManageMssEntity? = null

    private var emssp: List<ManageMssEntity> = emptyList()

    private var emsspSelected: ManageMssEntity? = null

    private var msspList: List<ManageMsspEntity> = emptyList()

    private var typeActivity: List<ManageMssEntity> = emptyList()

    private var typeActivityAdd : ArrayList<String> = arrayListOf()

    private var typeActivitySelected: String = EMPTY_STRING

    val idTipoProducto get() = _visitData.value?.firstOrNull()?.idTipoProducto.orZero()


    fun init(visitData: List<VisitEntity>) = execute {
        _visitData.value = visitData
        onGetAmssp()
        onGetEmssp()
        onGetMsspList()
        onGetTypeActivity()
    }

    fun onGetAmssp() = execute {
        _event.value =
            onMsspViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        val resultAmssp = manageMssUseCase.loadTypeMssLocal(MSSE, idTipoProducto).getOrNull()
        _event.value = if (!resultAmssp.isNullOrEmpty()) {
            amssp = resultAmssp
            onMsspViewModelEvent.onViewModelReadyAmssp(resultAmssp)
        } else {
            onMsspViewModelEvent.onLoadingError(null)
        }
    }

    fun setAmsspSelected(code: String, isSelected: Boolean) {
        amssp.find { it.codigo == code }?.let {
            it.isSelected = isSelected
            _amsspSelected.value = it
            onUpdateAmssp(it)
        }
    }

    fun onUpdateAmssp(it: ManageMssEntity) = execute {
        val amsspCheck =
            manageMssUseCase.updateAmsspLocal(it, _visitData.value?.firstOrNull()?.id.toString())
        if (amsspCheck.isFailure) {
            _event.value = onMsspViewModelEvent.onSnackLoadingError(R.string.error)
        }
    }

    fun onGetEmssp() = execute {
        val resultMssp = manageMssUseCase.loadTypeMssLocal(EMSSE, idTipoProducto).getOrNull()
        _event.value = if (!resultMssp.isNullOrEmpty()) {
            emssp = resultMssp
            onMsspViewModelEvent.onViewModelReadyEmssp(resultMssp)
        } else {
            onMsspViewModelEvent.onLoadingError(null)
        }
    }

    fun setEmsspSelected(groupSelected: String) {
        emssp.find { it.descripcion.equals(groupSelected, true) }?.let {
            emsspSelected = it
        }
    }

    fun onGetMsspList() = execute {
        val msspList_ = _visitData.value?.filter { it.empresa != null }?.mapNotNull {
            manageMssUseCase.loadListMssLocal(
                it.empresa?.razonSocial.orEmpty(),
                MSSE
            )
        }
        val resultMsspList =
            msspList_?.filter { it.isSuccess }?.mapNotNull { it.getOrNull() }?.flatten()
        msspList = resultMsspList.orEmpty().filter { it.codigoMedidaSanitaria != ID_CODE_MSS_EJECUTADA }
        _event.value = onMsspViewModelEvent.onViewModelReadyMsspList(resultMsspList.orEmpty())
    }

    fun setMsspSelected(id: Int, isSelected: Boolean) {
        msspList.find { it.id == id }?.let {
            it.isSelected = isSelected
        }
    }

    fun onUpdate() = execute {
        msspList.map {
            if (it.isSelected) {
                val msspList = manageMssUseCase.updateMsspListLocal(
                    it.id.orZero(),
                    emsspSelected?.codigo.orEmpty(),
                    emsspSelected?.tipoMs.orEmpty(),
                    it.lote.orEmpty()
                )
                if (msspList.isSuccess) {
                    _event.value = onMsspViewModelEvent.onViewModelUpdateMsspList
                } else {
                    onMsspViewModelEvent.onLoadingError(null)
                }
            }
        }
        onGetEmssp()
        onGetMsspList()
    }

    fun onInsert() = execute {
        amssp.filter { it.isSelected }.onEach {
            val msspList = manageMssUseCase.insertMsseListLocal(
                _visitData.value?.firstOrNull()!!,
                it.codigo.orEmpty(),
                MSSE,
                typeActivitySelected,
                EMPTY_STRING,
                EMPTY_STRING
            )
            if (msspList.isSuccess) {
                _event.value = onMsspViewModelEvent.onViewModelUpdateMsspList
            } else {
                onMsspViewModelEvent.onLoadingError(null)
            }
        }
        onGetAmssp()
        onGetEmssp()
        onGetMsspList()
        onGetTypeActivity()
        typeActivitySelected = EMPTY_STRING
        typeActivityAdd.clear()
    }

    fun onGetTypeActivity() = execute {
        val mssp = manageMssUseCase.loadTypeMssLocal(TIPACTE, idTipoProducto)
        val resultMssp = mssp.getOrNull()
        _event.value = if (!resultMssp.isNullOrEmpty()) {
            typeActivity = mssp.getOrNull().orEmpty()
            onMsspViewModelEvent.onViewModelReadyMssActivity(resultMssp)
        } else {
            onMsspViewModelEvent.onLoadingError(null)
        }
    }

    fun setTypeActivitySelected(id: Int) {
        typeActivitySelected = if (id == 0) {
            EMPTY_STRING
        } else {
            typeActivity.find { it.id == id }?.let {
                typeActivityAdd.add(it.descripcion.orEmpty())
            }
             typeActivityAdd.joinToString()
        }
    }
}