package com.soaint.domain.repository

import com.soaint.domain.model.TaskPapfEntity
import kotlinx.coroutines.flow.StateFlow

interface TaskPapfRepository {

    val tasks: StateFlow<List<TaskPapfEntity>?>

    suspend fun loadTasks(): Result<List<TaskPapfEntity>>

    suspend fun loadTasksLocal(): Result<List<TaskPapfEntity>>

    suspend fun loadTaskLocal(idSolicitud: Int): Result<TaskPapfEntity>
/*
    suspend fun loadTasksLocalOnlyActive(): Result<List<TaskPapfEntity>>

    suspend fun updateTask(body: TaskPapfEntity): Result<Unit>

    suspend fun updateTaskLocal(task: TaskPapfEntity?): Result<Unit>*/

}