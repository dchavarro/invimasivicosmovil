package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.*

@Dao
interface TransportDao {

    //Transporte decomiso
    @Query("SELECT * FROM transport_table WHERE idVisita = :idVisita ")
    suspend fun selectTransport(idVisita: String): List<TransportModel>

    @Insert(entity = TransportModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransport(transportModel: TransportModel)

    @Query("DELETE FROM transport_table WHERE idVisita = :idVisita")
    suspend fun deleteTransportByVisit(idVisita: String)

    //Funcionarios Transporte decomiso
    @Query("SELECT * FROM transport_official_table WHERE idVisita = :idVisita ")
    suspend fun selectTransportOfficial(idVisita: String): List<TransportOffcialModel>

    @Insert(entity = TransportOffcialModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransportOfficial(transportOffcialModel:  List<TransportOffcialModel>)

    @Query("DELETE FROM transport_official_table WHERE idVisita = :idVisita")
    suspend fun deleteTransportOfficialByVisit(idVisita: String)

    //Transporte decomiso DATA
    @Query("SELECT * FROM transport_body_table WHERE idVisita = :idVisita AND isLocal = :isLocal ORDER BY fechaCreacion")
    suspend fun selectTransportDecomiso(idVisita: String, isLocal: Boolean = true): List<TransportBodyModel>

    @Transaction
    suspend fun insertOrUpdateTransportDecomiso(transportBodyModel: TransportBodyModel) {
        val item = selectTransportDecomiso(transportBodyModel.idVisita.orEmpty())
        if (!item.isNullOrEmpty()) {
            transportBodyModel.index = item.firstOrNull()?.index.orZero()
            updateTransportDecomiso(transportBodyModel)
        }
        else insertTransportDecomiso(transportBodyModel)
    }

    @Update(entity = TransportBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTransportDecomiso(transportBodyModel: TransportBodyModel)

    @Insert(entity = TransportBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransportDecomiso(transportBodyModel: TransportBodyModel)

    @Query("DELETE FROM transport_body_table WHERE `index` IN (SELECT `index` FROM transport_body_table WHERE idVisita = :idVisita ORDER BY fechaCreacion LIMIT 1)")
    suspend fun deleteTransportDecomiso(idVisita: String)

    @Query("SELECT COUNT(`index`) FROM transport_body_table WHERE isLocal = :isLocal")
    suspend fun selectAllTransportDecomiso(isLocal: Boolean = true): Int

    @Query("SELECT * FROM transport_body_table WHERE idVisita = :idVisita ORDER BY fechaCreacion DESC")
    suspend fun selectTransportDecomisoAll(idVisita: String): List<TransportBodyModel>

    @Query("DELETE FROM transport_body_table WHERE idVisita = :idVisita")
    suspend fun deleteAllTransportDecomiso(idVisita: String)
}