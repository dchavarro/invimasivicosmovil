package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TechnicalEntity(
    val idPersona: Int?,
    val idRolPersona: Int?,
    val idEmpresaAsociada: Int?,
    val correoElectronico: String?,
    val telefono: String?,
    val nombreCompleto: String?,
    val numeroDocumento: String?,
    val rolPersona: String?,
    val celular: String?,
    val tipoDocumento: String?,
    val codigoTipoDocumento: String?,
    val numeroVisita: String?,
    val idVisita: Int?,
    val confirmado: Boolean?,
    val profesion: String?,
    val seleccionado: Boolean?
) : Parcelable
