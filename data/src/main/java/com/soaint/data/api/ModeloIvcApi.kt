package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.POST
import retrofit2.http.Query

interface ModeloIvcApi {

    @POST("v1/visita/modelo-ivc-soa/consultar-requerimientos")
    suspend fun getRequerimientos(
        @Body body: BodyGenericVisitDto
    ): ModeloIvcResponseDto

    @POST("v1/visita/modelo-ivc-soa/guardar-requerimientos")
    suspend fun saveRequerimiento(
        @Body body: BodyModeloIvcDto
    ): Unit

    @DELETE("v1/visita/modelo-ivc-soa/requerimiento")
    suspend fun deleteRequerimiento(
        @Query("idRequerimiento") idRequerimiento: Int,
    ): Unit

    @POST("v1/visita/modelo-ivc-soa/consultar-datos-adicionales")
    suspend fun getReqDA(
        @Body body: BodyGenericVisitDto
    ): IvcDaResponseDto

    @POST("v1/visitas/modelo-ivc-soa/guardar")
    suspend fun saveReqDa(
        @Body body: BodyGenericIvcDaDto
    ): Unit
}