package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.common.orZero
import com.soaint.domain.model.IvcDaEntity
import com.soaint.domain.model.ModeloIvcEntity

@Entity(tableName = "ivc_req_table")
class ModeloIvcModel(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name ="idTipoRequerimiento") val idTipoRequerimiento: Int,
    @ColumnInfo(name ="descripciontipoRequerimiento") val descripciontipoRequerimiento: String,
    @ColumnInfo(name ="numeroRequerimiento") val numeroRequerimiento: Int,
    @ColumnInfo(name ="idCriticidad") val idCriticidad: Int? = null,
    @ColumnInfo(name ="descripcionCriticidad") val descripcionCriticidad: String,
    @ColumnInfo(name ="idVisita") val idVisita: Int,
    @ColumnInfo(name ="isDeleted") val isDeleted: Boolean? = false,
    @ColumnInfo(name ="isLocal") val isLocal: Boolean? = false,
)

fun ModeloIvcModel.mapToDomain() = ModeloIvcEntity(
    id.orZero(),
    idTipoRequerimiento,
    descripciontipoRequerimiento,
    numeroRequerimiento,
    idCriticidad,
    descripcionCriticidad,
    isDeleted,
    isLocal
)

fun ModeloIvcEntity.mapToModel(idVisita: Int, isDeleted: Boolean) = ModeloIvcModel(
    id,
    idTipoRequerimiento,
    descripciontipoRequerimiento,
    numeroRequerimiento,
    idCriticidad,
    descripcionCriticidad,
    idVisita,
    isDeleted,
    isLocal
)

@Entity(tableName = "ivc_da_table")
class IvcDaModel(
    @PrimaryKey(autoGenerate = true) var id: Int?,
    @ColumnInfo(name ="totalRequerimientos") val totalRequerimientos: Int? = null,
    @ColumnInfo(name ="numeralesDeficiencias") val numeralesDeficiencias: String? = null,
    @ColumnInfo(name ="idDistribucion") val idDistribucion: Int? = null,
    @ColumnInfo(name ="descripcionDistribucion") val descripcionDistribucion: String? = null,
    @ColumnInfo(name ="volumenBeneficio") val volumenBeneficio: Int? = null,
    @ColumnInfo(name ="veterinarioPlanta") val veterinarioPlanta: Boolean? = null,
    @ColumnInfo(name ="idTipoActividad") val idTipoActividad: String? = null,
    @ColumnInfo(name ="descripcionTipoActividad") val descripcionTipoActividad: String? = null,
    @ColumnInfo(name ="idProductosProcesados") val idProductosProcesados: String? = null,
    @ColumnInfo(name ="descripcionProductosProcesados") val descripcionProductosProcesados: String? = null,
    @ColumnInfo(name ="idPruebasLaboratorio") val idPruebasLaboratorio: String? = null,
    @ColumnInfo(name ="descripcionPruebasLaboratorio") val descripcionPruebasLaboratorio: String? = null,
    @ColumnInfo(name ="idVisita") val idVisita: Int? = null,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean? = false,
)

fun IvcDaEntity.mapToModel(idVisita: Int) = IvcDaModel(
    id,
    totalRequerimientos,
    numeralesDeficiencias,
    idDistribucion,
    descripcionDistribucion,
    volumenBeneficio,
    veterinarioPlanta,
    idTipoActividad,
    descripcionTipoActividad,
    idProductosProcesados,
    descripcionProductosProcesados,
    idPruebasLaboratorio,
    descripcionPruebasLaboratorio,
    idVisita,
    isLocal
)

fun IvcDaModel.mapToDomain() = IvcDaEntity(
    id,
    totalRequerimientos,
    numeralesDeficiencias,
    idDistribucion,
    descripcionDistribucion,
    volumenBeneficio,
    veterinarioPlanta,
    idTipoActividad,
    descripcionTipoActividad,
    idProductosProcesados,
    descripcionProductosProcesados,
    idPruebasLaboratorio,
    descripcionPruebasLaboratorio,
    isLocal
)