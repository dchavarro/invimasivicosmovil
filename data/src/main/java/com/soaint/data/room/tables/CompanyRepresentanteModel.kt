package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.data.model.VisitFormaDto
import com.soaint.domain.model.VisitDireccionEntity
import com.soaint.domain.model.VisitRepresentanteEntity

@Entity(tableName = "company_representante_table")
class CompanyRepresentanteModel(
    @ColumnInfo(name = "nombre") val nombre: String,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String,
    @ColumnInfo(name = "telefono") val telefono: String,
    @ColumnInfo(name = "celular") val celular: String,
    @ColumnInfo(name = "empresaId") val empresaId: Int,
    @ColumnInfo(name = "idVisit") val idVisit: Int,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun CompanyRepresentanteModel.mapToDomain() = VisitRepresentanteEntity(
    nombre,
    correoElectronico,
    telefono,
    celular,
)