package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.*

enum class Types(val type: Array<String>) {
    MSS(
        arrayOf(
            MSSP,
            EMSSP,
            MSSE,
            EMSSE,
            TIPACTE,
            CDASS,
            CDRESV,
            CDCS,
            CDTR,
            CDC,
            CDDPBA,
            CDTAG,
            CDPP,
            CDPL,
            CDDC,
            CDZVE,
            CDEE,
            CTIPPROD,
        )
    ),
    MSSLIST(
        arrayOf(
            MSSP,
            MSSE,
        )
    ),
    DAO(
        arrayOf(
            TIPO_DOCUMENTO,
            TIPO_DIRECCION,
            TIPO_INMUEBLE,
            COMPLEMENTO,
            TIPO_VIA,
        )
    ),
    CDPAPF(
        arrayOf(
            CD_EMPAQ,
            CD_CONCEPT,
            CD_UNITY,
            CD_PRESENTATION,
            CD_MPIG,
            CD_RESULT_CIS,
            CD_IDIOM,
            CD_FIRMATE,
            CD_OBSERVATION_PAPF,
            CD_REQUERIMIENTOS,
            CD_TYPE_CERTIFICATE,
            YES_NO_MSS,
            YES_NO_REEMBARQUE,
            YES_NO_CERTIFICATE_EXPORTA,
        )
    )
}