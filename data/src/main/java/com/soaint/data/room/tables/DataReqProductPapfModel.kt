package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.DataReqActSamplePapfEntity
import com.soaint.domain.model.DataReqInsSanPapfEntity

@Entity(tableName = "data_req_insp_sanitary_papf_table")
class DataReqInsSanPapfModel(
    @ColumnInfo(name = "idEstadoEmpaque") val idEstadoEmpaque: Int?,
    @ColumnInfo(name = "descripcionEmpaque") val descripcionEmpaque: String?,
    @ColumnInfo(name = "idConcepto") val idConcepto: Int?,
    @ColumnInfo(name = "descripcionConcepto") val descripcionConcepto: String?,
    @ColumnInfo(name = "condicionesAlmacenamiento") val condicionesAlmacenamiento: String?,
    @ColumnInfo(name = "observaciones") val observaciones: String?,
    @ColumnInfo(name = "detalleProducto") val detalleProducto: Int?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DataReqInsSanPapfModel.mapToDomain() = DataReqInsSanPapfEntity(
    idEstadoEmpaque,
    descripcionEmpaque,
    idConcepto,
    descripcionConcepto,
    condicionesAlmacenamiento,
    observaciones,
    detalleProducto,
    idSolicitud,
)

@Entity(tableName = "data_req_act_sample_papf_table")
class DataReqActSamplePapfModel(
    @ColumnInfo(name = "nroUnidades") val nroUnidades: Int?,
    @ColumnInfo(name = "descripcionUnidades") val descripcionUnidades: String?,
    @ColumnInfo(name = "unidadesMedida") val unidadesMedida: Int?,
    @ColumnInfo(name = "presentacion") val presentacion: Int?,
    @ColumnInfo(name = "descripcionPresentacion") val descripcionPresentacion: String?,
    @ColumnInfo(name = "contenidoNeto") val contenidoNeto: Int?,
    @ColumnInfo(name = "producto") val idProducto: Int?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DataReqActSamplePapfModel.mapToDomain() = DataReqActSamplePapfEntity(
    nroUnidades,
    descripcionUnidades,
    unidadesMedida,
    presentacion,
    descripcionPresentacion,
    contenidoNeto,
    idProducto,
    idSolicitud,
)