package com.soaint.domain.use_case

import com.soaint.domain.model.*
import com.soaint.domain.repository.ModeloIvcRepository
import com.soaint.domain.repository.NotificationRepository
import javax.inject.Inject

class ModeloIvcUseCase @Inject constructor(
    private val modeloIvcRepository: ModeloIvcRepository
) {

    val ivc get() = modeloIvcRepository.ivc

    suspend fun getRequerimientos(idVisita: String) = modeloIvcRepository.getRequerimientos(idVisita)

    suspend fun getRequerimientosLocal(idVisita: Int) = modeloIvcRepository.getRequerimientosLocal(idVisita)

    suspend fun deleteRequerimientoLocal(it: ModeloIvcEntity, idVisita: Int) = modeloIvcRepository.deleteRequerimientoLocal(it, idVisita)

    suspend fun deleteRequerimiento() = modeloIvcRepository.deleteRequerimiento()

    suspend fun addRequerimiento() = modeloIvcRepository.addRequerimiento()

    suspend fun addRequerimientoLocal(body: ModeloIvcEntity, idVisita: String)  = modeloIvcRepository.addRequerimientoLocal(body, idVisita)

    suspend fun getReqDa(idVisita: String) = modeloIvcRepository.getReqDa(idVisita)

    suspend fun getReqDaLocal(idVisita: String) = modeloIvcRepository.getReqDaLocal(idVisita)

    suspend fun addReqDA() = modeloIvcRepository.addReqDA()

    suspend fun addReqDALocal(body: IvcDaEntity, idVisita: String)  = modeloIvcRepository.addReqDALocal(body, idVisita)

    suspend fun getCountIvcReq() = modeloIvcRepository.getCountIvcReq()

}