package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.AmsspCheckModel
import com.soaint.data.room.tables.MsspListModel
import com.soaint.data.room.tables.RsModel
import com.soaint.data.room.tables.TypeMssModel
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.utils.MSSE
import com.soaint.sivicos_dinamico.utils.MSSP

@Dao
interface ManageMssDao {

    //Medidas sanitarias Check
    @Query("SELECT * FROM mss_type_table WHERE idTipoProducto = :idTipoProducto AND tipoMs = :tipoMedidaSanitaria")
    suspend fun selectTypeMss(
        idTipoProducto: String,
        tipoMedidaSanitaria: String
    ): List<TypeMssModel>?

    @Insert(entity = TypeMssModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTypeMss(model: TypeMssModel)

    @Query("DELETE FROM mss_type_table WHERE idTipoProducto = :idTipoProducto AND tipoMs = :tipoMedidaSanitaria")
    suspend fun deleteAllTypeMss(idTipoProducto: String, tipoMedidaSanitaria: String)

    @Query("DELETE FROM mss_type_table WHERE tipoMs = :tipoMedidaSanitaria")
    suspend fun deleteAllTypeMss(tipoMedidaSanitaria: String)


    //Medidas sanitarias Lista
    @Query("SELECT * FROM mss_list_table WHERE razonSocial = :razonSocial AND tipoMs = :tipoMedidaSanitaria ORDER BY fechaAplicacion DESC")
    suspend fun selectMsspList(
        razonSocial: String,
        tipoMedidaSanitaria: String
    ): List<MsspListModel>

    @Query("SELECT * FROM mss_list_table WHERE idVisita = :idVisita ")
    suspend fun selectAllMssListByIdVisit(idVisita: Int): List<MsspListModel>?

    @Insert(entity = MsspListModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMsspList(model: MsspListModel)

    @Query("DELETE FROM mss_list_table WHERE razonSocial = :razonSocial AND tipoMs = :tipoMedidaSanitaria")
    suspend fun deleteMsspListxRazonSocial(razonSocial: String, tipoMedidaSanitaria: String)

    @Transaction
    suspend fun updateMssp(
        id: Int,
        codigo: String,
        tipoMs: String,
        lote: String,
        fechaAplicacion: String
    ) {
        val item = selectEmsspxCodigo(codigo, tipoMs)
        if (item != null) updateMsspList(
            id,
            item.descripcion.orEmpty(),
            item.id.orZero(),
            lote,
            fechaAplicacion
        )
    }

    @Query("SELECT * FROM mss_type_table WHERE tipoMs =:tipo")
    suspend fun selectMssTipoProduct(tipo: String): List<TypeMssModel>?

    @Query("SELECT * FROM mss_type_table WHERE codigo = :codigo AND (tipoMs =:mssp OR  tipoMs =:msse)")
    suspend fun selectEmssp(codigo: String, mssp: String = MSSP, msse: String = MSSE): TypeMssModel?

    @Query("SELECT * FROM mss_type_table WHERE codigo = :codigo AND tipoMs = :tipoMs")
    suspend fun selectEmsspxCodigo(codigo: String, tipoMs: String): TypeMssModel?

    @Query("SELECT * FROM mss_type_table WHERE descripcion = :descripcion AND tipoMs = :tipoMs")
    suspend fun selectEmsspxDescripcion(descripcion: String, tipoMs: String): TypeMssModel?

    @Query("UPDATE mss_list_table SET estado = :estado, idEstado = :idEstado, lote = :lote, isUpdate = :isUpdate, fechaAplicacion = :fechaAplicacion WHERE id = :id ")
    suspend fun updateMsspList(
        id: Int,
        estado: String,
        idEstado: Int,
        lote: String,
        fechaAplicacion: String,
        isUpdate: Boolean = true
    )

    @Query("SELECT * FROM mss_list_table WHERE isUpdate = :isUpdate")
    suspend fun selectMsspListAll(isUpdate: Boolean = true): List<MsspListModel>

    @Query("DELETE FROM mss_list_table")
    suspend fun deleteMsspList()

    @Transaction
    suspend fun insertOrUpdateTypeProductLocal(it: ManageMssEntity, idVisita: String, date: String) {
        val item = selectAmsspxId(it.id.orZero(), idVisita, it.tipoMs.orEmpty())
        if (item != null) updateAmsspCheckById(item.id.orZero(), idVisita, it.isSelected, date, item.tipoMs.orEmpty())
        else insertAmsspCheck(AmsspCheckModel(it.id, it.codigo, it.descripcion, idVisita.intOrString(), it.isSelected, it.tipoMs, date))
    }

    @Transaction
    suspend fun updateAmssp(it: ManageMssEntity, idVisita: String, date: String) {
        val item = selectAmsspxCodigo(it.codigo.orEmpty(), idVisita)
        if (item != null) updateAmsspCheck(item.codigo.orEmpty(), idVisita, it.isSelected, date)
        else insertAmsspCheck(AmsspCheckModel(it.id, it.codigo, it.descripcion, idVisita.intOrString(), it.isSelected, it.tipoMs, date))
    }

    @Query("SELECT * FROM amssp_check_table WHERE idVisita = :idVisita AND tipoMs = :tipo AND isSelected =:isSelected")
    suspend fun selectAmsspXvisit(
        tipo: String,
        idVisita: Int,
        isSelected: Boolean = true
    ): List<AmsspCheckModel>?

    @Query("SELECT * FROM amssp_check_table WHERE idVisita = :idVisita")
    suspend fun selectAmsspxIdVisit(idVisita: String): List<AmsspCheckModel>?

    @Query("SELECT * FROM amssp_check_table WHERE codigo = :codigo AND idVisita = :idVisita")
    suspend fun selectAmsspxCodigo(codigo: String, idVisita: String): TypeMssModel?

    @Query("SELECT * FROM amssp_check_table WHERE id = :id AND idVisita = :idVisita AND tipoMs = :tipoMs")
    suspend fun selectAmsspxId(id: Int, idVisita: String, tipoMs: String): TypeMssModel?

    @Query("UPDATE amssp_check_table SET isSelected = :isSelected, createdAt = :date WHERE codigo = :codigo AND idVisita = :idVisita")
    suspend fun updateAmsspCheck(codigo: String, idVisita: String, isSelected: Boolean, date: String)

    @Query("UPDATE amssp_check_table SET isSelected = :isSelected, createdAt = :date WHERE id = :id AND idVisita = :idVisita AND tipoMs = :tipoMs")
    suspend fun updateAmsspCheckById(id: Int, idVisita: String, isSelected: Boolean, date: String, tipoMs: String)

    @Insert(entity = AmsspCheckModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAmsspCheck(model: AmsspCheckModel)

    @Query("DELETE FROM amssp_check_table WHERE codigo = :codigo")
    suspend fun deleteAllAmsspCheck(codigo: String)

    @Query("DELETE FROM amssp_check_table WHERE tipoMs = :tipoMs")
    suspend fun deleteAmsspCheckByTipo(tipoMs: String)


    @Insert(entity = MsspListModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMssListLocal(model: MsspListModel)

    @Transaction
    suspend fun insertMsseList(
        visit: VisitEntity,
        codigo: String,
        tipoMs: String,
        typeActivity: String,
        nombreProducto: String,
        registroSanitario: String
    ) {
        val item = selectEmsspxCodigo(codigo, tipoMs)
        if (item != null) {
            val newId = selectMssListAll()?.lastOrNull()?.id?.orZero()
            insertMssListLocal(
                MsspListModel(
                    id = newId.orZero() + 1,
                    numeroVisita = visit.numeroVisita,
                    idVisita = visit.id,
                    numeroMss = EMPTY_STRING,
                    nombreProducto = nombreProducto,
                    tipoMedidaSanitaria = item.descripcion,
                    estado = "Aplicada",
                    idEstado = 1,
                    fechaAplicacion = getDateHourWithOutFormat(),
                    notificacionSanitariaObligatoria = registroSanitario,
                    lote = EMPTY_STRING,
                    codigoMedidaSanitaria = codigo,
                    tiposActividades = typeActivity,
                    razonSocial = visit.empresa?.razonSocial,
                    tipoMs = tipoMs,
                    isLocal = true
                )
            )
        }
    }

    @Query("SELECT * FROM mss_list_table WHERE tipoMs = :tipoMs AND isLocal = :isLocal")
    suspend fun selectMsseListAll(
        tipoMs: String = MSSE,
        isLocal: Boolean = true
    ): List<MsspListModel>

    @Query("SELECT * FROM mss_list_table WHERE isLocal = :isLocal")
    suspend fun selectMssListAll(isLocal: Boolean = true): List<MsspListModel>?


    @Query("DELETE FROM amssp_check_table WHERE idVisita = :idVisita ")
    suspend fun deleteMssAppliedXidVisit(idVisita: String)

    @Insert(entity = AmsspCheckModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMssApplied(model: AmsspCheckModel)

    @Query("SELECT COUNT(id) FROM mss_list_table WHERE isLocal = :isLocal")
    suspend fun selectCountMssListAll(isLocal: Boolean = true): Int

    // Buscador registro sanitario
    @Query("SELECT * FROM rs_table WHERE idVisita = :idVisita ")
    suspend fun selectRsAll(idVisita: Int): List<RsModel>?

    @Query("SELECT * FROM rs_table WHERE isLocal = :isLocal")
    suspend fun selectRsAllLocal(isLocal: Boolean = true): List<RsModel>?

    @Insert(entity = RsModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRs(model: RsModel)

    //Medidas sanitarias check Lista
    @Query("SELECT COUNT(i) FROM amssp_check_table WHERE idVisita = :idVisita AND (tipoMs = 'MSSP' OR tipoMs = 'MSSE') AND isSelected =:isSelected")
    suspend fun selectAllMsspList(idVisita: String, isSelected: Boolean = true): Int?
}