package com.soaint.data.api

import com.soaint.data.model.*
import com.soaint.domain.model.BlockSaveEntity
import com.soaint.sivicos_dinamico.utils.ID_MISIONAL_SANITARIAS
import com.soaint.sivicos_dinamico.utils.ID_TYPE_PRODUCT_PAPF
import retrofit2.http.*

interface ParametricApi {

    @GET("v1/plantilla")
    suspend fun getPlantillas(
        @Query("idTipoProducto") idTipoProducto: String
    ): PlantillasResponseDto

    @GET("v1/plantilla/{idPlantilla}/bloque")
    suspend fun getBlockByPlantilla(
        @Path("idPlantilla") idPlantilla: String,
    ): BlockResponseDto

    @GET("v1/plantilla/regla-negocio")
    suspend fun getRulesBussiness(
        @Query("idDependencia") idTipoProducto: String,
        @Query("idGrupo") idGrupo: String,
    ): ResultRulesBussinessResponseDto

    @POST("v1/plantilla/sincronizacion")
    suspend fun saveBlock(
        @Query("idPlantilla") idPlantilla: String,
        @Query("idVisita") idVisita: String,
        @Query("usuarioCrea") usuarioCrea: String,
        @Body body: BlockSaveBodyDto
    ): ResultSaveBlockResponseDto

    @GET("v1/plantilla/sincronizacion")
    suspend fun getBlockSync(
        @Query("idPlantilla") idPlantilla: String,
        @Query("idVisita") idVisita: String,
    ): ResultSaveBlockResponseDto

    @GET("v1/plantilla/{idPlantilla}/bloque")
    suspend fun getBlockValue(
        @Path("idPlantilla") idPlantilla: String,
        @Query("idVisita") idVisita: String,
    ): ResultGetBlockValueResponseDto

    @GET("v1/plantilla/{idPlantilla}/bloque")
    suspend fun getBlockValuePapf(
        @Path("idPlantilla") idPlantilla: String,
        @Query("idSolicitudPapf") idSolicitudPapf: String,
        @Query("esMobile") esMobile: Boolean = true,
    ): ResultGetBlockValueResponseDto

    @POST("v1/plantilla/sincronizacion")
    suspend fun saveBlockPapf(
        @Query("idPlantilla") idPlantilla: String,
        @Query("idSolicitudPapf") idSolicitudPapf: String,
        @Query("usuarioCrea") usuarioCrea: String,
        @Body body: BlockSaveBodyDto
    ): ResultSaveBlockResponseDto

    @GET("v1/plantilla/regla-negocio-papf")
    suspend fun getRulesBussinessPapf(
        @Query("idDependencia") idTipoProducto: Int = ID_MISIONAL_SANITARIAS,
        @Query("idGrupo") idGrupo: Int = ID_TYPE_PRODUCT_PAPF,
        @Query("idTipoProductoPapf") idTipoProductoPapf: Int,
        @Query("codigoFiltroActa") codigoFiltroActa: String,
    ): ResultRulesBussinessResponseDto

    @GET("v1/plantilla")
    suspend fun getPlantilla(
        @Query("codigo") codigo: String
    ): PlantillasResponseDto
}