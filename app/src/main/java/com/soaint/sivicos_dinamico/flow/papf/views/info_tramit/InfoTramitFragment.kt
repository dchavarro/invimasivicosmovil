package com.soaint.sivicos_dinamico.flow.papf.views.info_tramit

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentInfoTramitBinding
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ID_TYPE_IMPORTER
import com.xwray.groupie.GroupieAdapter

class InfoTramitFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentInfoTramitBinding>(R.layout.fragment_info_tramit)

    private val viewModel by activityViewModels<InfoTramitViewModel> { viewModelFactory }

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.run {
        initView()
        subscribeViewModel()
        root
    }

    private fun initView(): View {
        binding.viewModel = viewModel
        binding.root.isVisible = false
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    private fun subscribeViewModel() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(viewLifecycleOwner, ::stateHandler)
        mainViewModel.idRequest.observe(viewLifecycleOwner, viewModel::init)
        viewModel.infoTramitData.observe(viewLifecycleOwner, mainViewModel::getInfoTramit)
    }

    private fun stateHandler(state: OnViewModelEvent) {
        showLoading(false)
        when (state) {
            is OnViewModelEvent.OnViewModelReady -> onSuccess(state.infoTramit)
            is OnViewModelEvent.OnLoadingError -> onTokenError(state.error)
        }
    }

    private fun onSuccess(infoTramit: InfoTramitePapfEntity) {
        binding.root.isVisible = true
        if (infoTramit.idTipoTramite == ID_TYPE_IMPORTER) {
            binding.textViewTitleTypeExporta.text = getString(R.string.copy_type_importa)
            binding.textViewTitleExportator.text = getString(R.string.copy_importador)
        } else {
            binding.textViewTitleTypeExporta.text = getString(R.string.copy_type_exporta)
            binding.textViewTitleExportator.text = getString(R.string.copy_exportator)
        }

        binding.textViewNumber.text = infoTramit.radicado.orEmpty().toString()
        binding.textViewCategoryTramit.text = infoTramit.descripcionCategoria.orEmpty()
        binding.textViewTypeProductPapf.text = infoTramit.tipoProducto.orEmpty()
        binding.textViewPort.text = infoTramit.nombreDependencia.orEmpty()
        binding.textViewTypeTramit.text = infoTramit.tipoTramite.orEmpty()
        binding.textViewTypeExporta.text = infoTramit.tipoImportacion.orEmpty()
        binding.textViewExportator.text = infoTramit.importador.orEmpty()
        binding.textViewTypeInspection.text = infoTramit.descripcionTipoInspeccion.orEmpty()

    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }
}