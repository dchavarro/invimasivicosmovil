package com.soaint.sivicos_dinamico.factories.controlsFactory.types

enum class ControlsTypes {
    TEXTO_CORTO,
    TEXTO_EMAIL,
    TEXTO_LARGO,
    RADIO_GROUP,
    LISTA,
    LISTA_TABLA,
    NUMERICO,
    FECHA,
    FECHA_HORA,
    FIRMA,
    CHECK_LIST,
    FOTO,
    CHECK
}