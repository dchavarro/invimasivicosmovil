package com.soaint.sivicos_dinamico.flow.papf.views.info_tramit

import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnViewModelReady(val infoTramit: InfoTramitePapfEntity) : OnViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class InfoTramitViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _infoTramitData = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramitData = _infoTramitData.asLiveData()


    fun init(id: Int) {
        loadInfoTramit(id)
    }

    fun loadInfoTramit(requestId: Int) = execute {
        _infoTramitData.value =
            papfUseCase.loadInfoTramitLocal(requestId).getOrNull().orEmpty().firstOrNull()
        _event.value = if (_infoTramitData.value != null) {
            infoTramitData.value?.let { OnViewModelEvent.OnViewModelReady(it) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }

}
