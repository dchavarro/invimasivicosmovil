package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.TypeHoursBillingModel
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.TypeHoursBillingEntity

data class TypeHoursBillingResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<TypeHoursBillingDto>,
    @SerializedName("statusCode") val statusCode: Int
)
data class TypeHoursBillingDto(
    @SerializedName ("idTipoHoraFacturacion") val idTipoHoraFacturacion: Int,
    @SerializedName( "codigo") val codigo: String,
    @SerializedName( "descripcion") val descripcion: String,
    @SerializedName( "activo") val activo: Boolean,
    @SerializedName( "fechaCreacion") val fechaCreacion:String? = getDateHourNow(),
    @SerializedName( "usuarioCrea") val usuarioCrea: String?,
    @SerializedName( "fechaModifica") val fechaModifica: String?,
    @SerializedName( "usuarioModifica") val usuarioModifica: String?
)


fun TypeHoursBillingDto.mapToDomain() = TypeHoursBillingEntity(
idTipoHoraFacturacion,codigo, descripcion, activo, fechaCreacion, usuarioCrea, fechaModifica, usuarioModifica
)

fun TypeHoursBillingDto.mapToDb() = TypeHoursBillingModel(
idTipoHoraFacturacion, codigo, descripcion, activo, fechaCreacion, usuarioCrea, fechaModifica, usuarioModifica
)