package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ShiftTypeEntity

@Entity(tableName = "shift_type_table")
data class ShiftTypeModel(
    @PrimaryKey var id: Int?,
    @ColumnInfo(name = "jornada") val jornada: String,
    @ColumnInfo(name = "turno") val turno: String,
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String? = getDateHourNow(),
    @ColumnInfo(name = "codigoJornada") val codigoJornada: String?,
    @ColumnInfo(name = "horaInicio") val horaInicio: String?,
    @ColumnInfo(name = "horaFin") val horaFin: String?
)

fun ShiftTypeModel.mapToDomain() = ShiftTypeEntity(
    id, jornada, turno, activo, usuarioCrea,fechaCreacion, codigoJornada, horaInicio, horaFin
)