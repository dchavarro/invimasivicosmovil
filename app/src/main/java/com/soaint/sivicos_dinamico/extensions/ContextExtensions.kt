package com.soaint.sivicos_dinamico.extensions

import android.app.ActivityManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.provider.Settings
import com.soaint.sivicos_dinamico.services.LocationService

fun Context.isServiceRunning(serviceClass: Class<out Service>): Boolean {
    val manager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val nameServiceClass = serviceClass.name
    for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
        if (nameServiceClass == service.service.className) {
            return true
        }
    }
    return false
}

fun Context.stopService(serviceClass: Class<out Service>) {
    if (isServiceRunning(serviceClass)) {
        stopService(Intent(this, serviceClass))
    }
}

fun Context.startService(serviceClass: Class<out Service>) {
    if (!isServiceRunning(serviceClass)) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForegroundService(Intent(this, serviceClass))
        } else {
            startService(Intent(this, serviceClass))
        }
    }
}

fun Context.stopLocationService() {
    stopService(LocationService::class.java)
}

fun Context.startLocationService() {
    startService(LocationService::class.java)
}

fun Context.goToAppSettings() {
    val PACKAGE = "package"
    val intent = Intent(
        Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
        Uri.fromParts(PACKAGE, packageName, null)
    )
    startActivity(intent)
}

fun Context.goToLocationSettings() {
    val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
    startActivity(intent)
}

fun Context.isEnabledLocation(): Boolean {
    val locationManager = getSystemService(Context.LOCATION_SERVICE) as? LocationManager
    return locationManager?.isProviderEnabled(LocationManager.GPS_PROVIDER) == true
}