package com.soaint.domain.use_case

import com.soaint.domain.model.TaskPapfEntity
import com.soaint.domain.repository.TaskPapfRepository
import javax.inject.Inject

class TaskPapfUseCase @Inject constructor(
    private val repository: TaskPapfRepository
) {

    val tasks get() = repository.tasks

    suspend fun loadTask() = repository.loadTasks()

    suspend fun loadTasksLocal() = repository.loadTasksLocal()

    suspend fun loadTaskLocal(idSolicitud: Int)= repository.loadTaskLocal(idSolicitud)
/*
    suspend fun loadTasksLocalOnlyActive() = repository.loadTasksLocalOnlyActive()

    suspend fun updateTask(body: TaskPapfEntity): Result<Unit> { return repository.updateTask(body) }

    suspend fun updateTaskLocal(task: TaskPapfEntity?)= repository.updateTaskLocal(task)*/
}