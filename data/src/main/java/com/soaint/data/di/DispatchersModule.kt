package com.soaint.data.di

import com.soaint.domain.dispatchers.AppDispatchers
import com.soaint.data.dispatchers.AppDispatchersImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class DispatchersModule {

    @Binds
    @Singleton
    abstract fun bindsAppDispatchers(
        impl: AppDispatchersImpl
    ): AppDispatchers
}