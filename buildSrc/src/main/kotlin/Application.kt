object Application {

    const val appId = "com.soaint.sivicos_dinamico"
    const val versionCode = 2
    const val versionName = "0.0.1"
    const val QAVersion = 21
    const val QAInvimaVersion = 34

    const val minSdk = 23
    const val compileSdk = 31
    const val targetSdk = compileSdk
    const val buildToolsVersion = "30.0.2"
}