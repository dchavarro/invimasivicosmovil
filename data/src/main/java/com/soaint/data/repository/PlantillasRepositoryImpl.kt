package com.soaint.data.repository

import android.util.Log
import com.soaint.data.api.ParametricApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.BlockDto
import com.soaint.data.model.PlantillaDto
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.ActasDao
import com.soaint.data.room.dao.BlocksDao
import com.soaint.data.room.dao.PlantillasDao
import com.soaint.data.room.tables.BlockBodyModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.BlockBodyValueEntity
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.PlantillaEntity
import com.soaint.domain.model.TechnicalEntity
import com.soaint.domain.repository.PlantillasRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class PlantillasRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val plantillasApi: ParametricApi,
    private val plantillasDao: PlantillasDao,
    private val blocksDao: BlocksDao,
    private val actasDao: ActasDao,
) : PlantillasRepository, BaseRepository() {

    private val _plantillas = MutableStateFlow<List<PlantillaEntity>?>(null)
    override val plantillas get() = _plantillas.asStateFlow()

    //Cargar Plantillas
    override suspend fun loadPlantillas(idTipoProducto: Int): Result<List<PlantillaEntity>> = execute {
        val planillas = plantillasApi.getPlantillas(idTipoProducto.toString()).data
        //deleteDao()
        savePlantillas(planillas)
        Result.success(planillas.map { it.mapToDomain() })
    }

    //Carga de Plantillas DB Local
    override suspend fun loadPlantillasLocal(idPlantilla: String): Result<List<PlantillaEntity>> =
        execute {
            val plantilla = plantillasDao.selectPlantilla(idPlantilla)
            Result.success(plantilla.map { it.mapToDomain(null) })
        }

    //Carga de Plantillas DB Local
    override suspend fun loadPlantillasLocal(): Result<List<PlantillaEntity>> = execute {
        val plantilla = plantillasDao.selectAllPlantilla()
        Result.success(plantilla.map { it.mapToDomain(null) })
    }

    //Guardar Plantillas en DB local
    private suspend fun savePlantillas(data: List<PlantillaDto>) {
        data.onEach { plantilla ->
            val model = plantilla.mapToDb()
            val misionales = plantilla.misionales?.map { it.mapToDb(plantilla.id) }
            plantillasDao.insertPlantilla(model)
            misionales?.let { plantillasDao.insertMisionales(it) }
        }
    }

    //Eliminar Plantillas en DB local
    private suspend fun deleteDao() {
        plantillasDao.deleteAllPlantillas()
        plantillasDao.deleteAllMisionales()
        plantillasDao.deleteAllBlock()
        plantillasDao.deleteAllBlockAtribute()
        plantillasDao.deleteAllBlockAtributeConfig()
        plantillasDao.deleteAllBlockAtributeConfigList()
    }

    //Cargar bloques
    override suspend fun loadBlocks(idPlantilla: String): Result<List<BlockEntity>> = execute {
        val blocks = plantillasApi.getBlockByPlantilla(idPlantilla).objectResponse
        saveBlockDao(blocks)
        Result.success(blocks!!.map { it.mapToDomain() })
    }

    //Carga de bloques DB Local
    override suspend fun loadBlockLocal(idPlantilla: String): Result<List<BlockEntity>> {
        val block = plantillasDao.selectBlock(idPlantilla)
        val result = block.map { block ->
            val idBlock = block.id
            val blockAtribute =
                plantillasDao.selectBlockAtribute(idBlock.toString()).map { atribute ->
                    val idAtribute = atribute.id
                    val configAtribute =
                        plantillasDao.selectBlockConfigAtribute(idAtribute.toString())
                            ?.let { config ->
                                val idConfigAtribute = config.id
                                val listAttribute =
                                    plantillasDao.selectBlockListConfigAtribute(idConfigAtribute.toString())
                                        .map { it.mapToDomain() }
                                config.mapToDomain(listAttribute)
                            }
                    atribute.mapToDomain(configAtribute)
                }
            block.mapToDomain(blockAtribute)
        }
        return Result.success(result)
    }


    //Guardar bloques en DB local
    private suspend fun saveBlockDao(data: List<BlockDto>?) {
        data?.onEach { block ->
            //Guardar Bloque
            val blockModel = block.mapToDb()
            plantillasDao.insertBlock(blockModel)

            //Guardar - Bloque - Atributo
            block.atributosPlantilla.orEmpty().onEach { plantilla ->
                //Guardar - Plantilla
                val plantillaModel = plantilla.mapToDb()
                plantillasDao.insertBlockAtribute(plantillaModel)

                //Guardar - Bloque - Atributo - Configuración
                plantilla.configuracion?.let { config ->
                    //Guardar - Configuración
                    val configModel = config.mapToDb(plantillaModel.id)
                    val id = plantillasDao.insertBlockConfigAtribute(configModel).toInt()

                    //Guardar  - Bloque - Atributo - Configuración - Lista
                    config.lista?.onEach { lista ->
                        //Guardar - Lista
                        val listaModel = lista.mapToDb(id)
                        plantillasDao.insertBlockListConfigAtribute(listaModel)
                    }
                }
            }
        }
    }

    override suspend fun saveBlockBody(
        idPlantilla: String,
        idVisita: String,
        body: String,
        id: Int
    ) {
        blocksDao.insertOrUpdateBodyBlock(
            BlockBodyModel(
                idVisita,
                idPlantilla,
                body,
                id = id
            )
        )
    }

    override suspend fun getBlockBody(idPlantilla: String, idVisita: String): String? {
        return blocksDao.selectBodyBlock(idPlantilla, idVisita)?.body
    }

    override suspend fun selectBodyBlockUnfinished(idPlantilla: String, idVisita: String): Result<BlockBodyValueEntity?> = execute {
        val block = blocksDao.selectBodyBlockUnfinished(idPlantilla, idVisita)
       Result.success(block?.mapToDomain())
    }
    override suspend fun isFinishedAct(idPlantilla: String, idVisita: String): Boolean? {
        return blocksDao.selectBodyBlock(idPlantilla, idVisita)?.isFinished
    }
    override suspend fun isEditableActa(idPlantilla: String, idVisita: String, id: Int): Boolean? {
        return blocksDao.isFinishedAct(idPlantilla, idVisita,id)?.isFinished
    }

    override suspend fun saveFinishedActa(idPlantilla: String, idVisita: String, id: Int) {
        blocksDao.updateFinishedBlock(true, idPlantilla, idVisita, id)
    }

    override suspend fun getAllBlockBody(): List<BlockBodyValueEntity> {
        return blocksDao.selectAllBodyBlock().map {
            BlockBodyValueEntity(it.idVisita, it.idPlantilla, it.body, it.id)
        }
    }

    override suspend fun deleteBlockBody(idPlantilla: String, idVisita: String, id: Int) {
        blocksDao.deleteBodyBlock(idPlantilla, idVisita, id)
    }
}