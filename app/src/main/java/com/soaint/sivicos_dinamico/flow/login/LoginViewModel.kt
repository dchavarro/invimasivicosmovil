package com.soaint.sivicos_dinamico.flow.login

import androidx.annotation.StringRes
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnLoginViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnLoginViewModelEvent()
    object OnViewModelReady : OnLoginViewModelEvent()
    data class OnTokenError(val error: Throwable?) : OnLoginViewModelEvent()
}

class LoginViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    /**
     * Events
     */

    private val _event = SingleMutableLiveData<OnLoginViewModelEvent>()
    val event = _event.asLiveData()

    /**
     * Action
     */

    fun onDoLogin(userName: String?, password: String?) {
        if (!userName.isNullOrBlank() && !password.isNullOrBlank()) {
            getToken(userName, password)
        }
    }

    private fun getToken(userName: String, password: String) = execute {
        _event.value = OnLoginViewModelEvent.OnChangeLabelPreloader(R.string.cargando_login)
        val result = userUseCase.login(userName, password)
        _event.value = if (result.isSuccess) {
            OnLoginViewModelEvent.OnViewModelReady
        } else {
            OnLoginViewModelEvent.OnTokenError(result.exceptionOrNull())
        }
    }
}