package com.soaint.data.store
import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.PlantillaEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

private val Context.dataStore by preferencesDataStore("my_preferences")

class PreferencesManager(private val context: Context) {
    private val dataStore = context.dataStore
    private val gson = Gson()

    val idVisita: Flow<Int> = dataStore.data.map { preferences ->
        preferences[intPreferencesKey("idVisita")] ?: 0
    }


    val plantilla: Flow<List<PlantillaEntity>> = dataStore.data.map { preferences ->
        val plantillaJson = preferences[stringPreferencesKey("plantilla")] ?: "[]"
        gson.fromJson(plantillaJson, object : TypeToken<List<PlantillaEntity>>() {}.type)
    }

    val bloques: Flow<List<BlockEntity>> = dataStore.data.map { preferences ->
        val bloquesJson = preferences[stringPreferencesKey("bloques")] ?: "[]"
        gson.fromJson(bloquesJson, object : TypeToken<List<BlockEntity>>() {}.type)
    }


    val idTipoDocumental: Flow<Int> = dataStore.data.map { preferences ->
        preferences[intPreferencesKey("idTipoDocumental")] ?: 0
    }
    suspend fun saveData(id: Int, plantilla: List<PlantillaEntity>, bloques: List<BlockEntity>, idTipoDocumental: Int) {
        val plantillaJson = gson.toJson(plantilla)
        val bloquesJson = gson.toJson(bloques)
        dataStore.edit { preferences ->
            preferences[intPreferencesKey("idVisita")] = id
            preferences[stringPreferencesKey("plantilla")] = plantillaJson
            preferences[stringPreferencesKey("bloques")] = bloquesJson
            preferences[intPreferencesKey("idTipoDocumental")] = idTipoDocumental
        }
    }

}
