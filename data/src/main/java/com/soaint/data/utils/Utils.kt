package com.soaint.data.utils

import android.graphics.Bitmap
import android.media.ExifInterface
import android.os.Environment
import android.util.Base64
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.formatDateHour
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset
import java.util.*

// Obtener Base64 desde un path
fun getBase64FromPath(path: String?): String? {
    var base64: String? = EMPTY_STRING
    try {
        val file = File(path)
        val buffer = ByteArray(file.length().toInt() + 100)
        val length = FileInputStream(file).read(buffer)
        base64 = Base64.encodeToString(buffer, 0, length, Base64.DEFAULT)
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return base64.orEmpty()
        .replace(" ", "")
        .replace("\n", "")
        .replace("\r", "")
}

// OBTENER FECHA Y HORA ACTUAL
fun getDateHourNow(): String{
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString().formatDateHour()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString().formatDateHour()
}

// OBTENER FECHA
fun getDateNow(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString().formatDate()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString().formatDate()
}

// OBTENER FECHA Y HORA ACTUAL sin formatear
fun getDateHourWithOutFormat(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString()
}

// ESCALAR IMAGEN
fun resizeImage(path: String, newWidth: Int = 150, newHeight: Int = 80): String {
    val file = File(path)
    val scaledBitmap: Bitmap
    var finalFile: File? = null
    try {
        val unscaledBitmap: Bitmap = decodeFile(path, newWidth, newHeight, ScalingLogic.FIT)
        if (!(unscaledBitmap.width <= newWidth && unscaledBitmap.height <= newHeight)) {
            scaledBitmap = createScaledBitmap(
                unscaledBitmap, newWidth, newHeight,
                ScalingLogic.FIT
            )
            finalFile = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "REDUCED_" + file.name.trim { it <= ' ' })
            val fileOutputStream = FileOutputStream(finalFile)
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fileOutputStream)
            fileOutputStream.flush()
            fileOutputStream.close()
            scaledBitmap.recycle()
        } else {
            unscaledBitmap.recycle()
        }
    } catch (e: Throwable) {
        e.printStackTrace()
    }
    if (finalFile == null) {
        finalFile = file
    } else {
        saveImageOrientation(file, finalFile)
    }
    return finalFile.absolutePath.orEmpty()
}

fun deleteFile(path: String) {
    val file = File(path)
    file.delete()
}

private fun saveImageOrientation(file: File, finalFile: File) {
    try {
        val orientation = ExifInterface(file.path).getAttribute(ExifInterface.TAG_ORIENTATION)
        val exifNew = ExifInterface(finalFile.path)
        exifNew.setAttribute(ExifInterface.TAG_ORIENTATION, orientation)
        exifNew.saveAttributes()
    } catch (e: IOException) {
        e.printStackTrace()
    }
}
