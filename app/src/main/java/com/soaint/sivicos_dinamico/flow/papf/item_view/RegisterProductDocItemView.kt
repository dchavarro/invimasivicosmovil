package com.soaint.sivicos_dinamico.flow.papf.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.DocumentPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemRegisterProductDocBinding
import com.xwray.groupie.databinding.BindableItem

class RegisterProductDocItemView(
    private val item: DocumentPapfEntity,
) : BindableItem<ItemRegisterProductDocBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemRegisterProductDocBinding,
        position: Int
    ) = with(viewBinding) {

        textView.text = item.nombreDocumento

    }

    override fun getLayout() = R.layout.item_register_product_doc
}