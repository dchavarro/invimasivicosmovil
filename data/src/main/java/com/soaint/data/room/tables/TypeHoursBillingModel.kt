package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.TypeHoursBillingEntity

@Entity(tableName = "type_hours_billing_table")
data class TypeHoursBillingModel(
    @PrimaryKey val idTipoHoraFacturacion: Int,
    @ColumnInfo(name = "codigo") val codigo: String,
    @ColumnInfo(name = "descripcion") val descripcion: String,
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion:String? = getDateHourNow(),
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?
)

fun TypeHoursBillingModel.mapToDomain() = TypeHoursBillingEntity(
    idTipoHoraFacturacion, codigo, descripcion, activo, fechaCreacion, usuarioCrea, fechaModifica, usuarioModifica
)