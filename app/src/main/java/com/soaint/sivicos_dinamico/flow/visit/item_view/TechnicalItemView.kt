package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.TechnicalEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTechnicalBinding
import com.xwray.groupie.databinding.BindableItem

class TechnicalItemView(
    private val technical: TechnicalEntity,
    private val onCheck: (TechnicalEntity, Boolean) -> Unit,
) : BindableItem<ItemTechnicalBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemTechnicalBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = technical.nombreCompleto
        textViewTypeDoc.text = technical.codigoTipoDocumento + " - " + technical.tipoDocumento
        textViewNumberDoc.text = technical.numeroDocumento

        textViewProfesion.text = technical.profesion.orEmpty()
        textViewCargo.text = technical.rolPersona
        textViewEmail.text = technical.correoElectronico

        checkBox.isChecked = technical.confirmado == true
        radioButton.isSelected = technical.seleccionado == true

        checkBox.setOnClickListener {
            checkBox.isChecked = !checkBox.isChecked
            onCheck.invoke(technical, checkBox.isChecked)
        }
    }

    override fun getLayout() = R.layout.item_technical
}