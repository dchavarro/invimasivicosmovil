package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.HolidaysModel
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.HolidaysEntity

data class HolidayResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<HolidayDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class HolidayDto(
    @SerializedName("idCalendario") val idCalendario: Int?,
    @SerializedName("nombre") val nombre: String,
    @SerializedName("fechaEvento") val fechaEvento: String,
    @SerializedName("activo") val activo: Boolean,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?
)

fun HolidayDto.mapToDomain() = HolidaysEntity(
    idCalendario,
    nombre,
    fechaEvento,
    activo,
    fechaCreacion,
    usuarioCrea,
    fechaModifica,
    usuarioModifica
)

fun HolidayDto.mapToDb() = HolidaysModel(
    idCalendario,nombre, fechaEvento, activo, fechaCreacion, usuarioCrea, fechaModifica, usuarioModifica
)