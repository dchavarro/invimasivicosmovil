package com.soaint.sivicos_dinamico.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.soaint.sivicos_dinamico.flow.act.ActViewModel
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceViewModel
import com.soaint.sivicos_dinamico.flow.login.LoginViewModel
import com.soaint.sivicos_dinamico.flow.main.views.sync.SyncViewModel
import com.soaint.sivicos_dinamico.flow.main.views.task.TasksViewModel
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.dialogs.act_sample.DataReqActSampleViewModel
import com.soaint.sivicos_dinamico.flow.papf.dialogs.inspection_sanitary.DataReqViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.certificate.CertificateViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.close_insp.CloseInspectionViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.detail_product.DetailProductViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.documentation.DocumentationViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.emit_cis.EmitCisViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.invoice.InvoiceViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.products.RegisterProductsViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.registerDate.RegisterDateViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.transport.TransportPapfViewModel
import com.soaint.sivicos_dinamico.flow.splash.SplashViewModel
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.address.AddressViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.CloseViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.close.CloseVisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.denunciation.DenunciationViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.interaction.InteractionViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.ivc.CloseIvoViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.technical.TechnicalViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.documents.DocumentViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument.AddDocumentViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.historic.HistoricViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.historicMSS.HistoricMssViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.info_visit.VisitInfoViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.equipment.EquipmentViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.ManageMssViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.company.MssCompanyViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.nso.MssNsoViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.product.MssProductViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.notification.NotificationViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.register_company.RegisterCompanyViewModel
import com.soaint.sivicos_dinamico.flow.visit.views.transport.TransportViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    internal abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    internal abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SyncViewModel::class)
    internal abstract fun bindSyncViewModel(viewModel: SyncViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TasksViewModel::class)
    internal abstract fun bindTasksViewModel(viewModel: TasksViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VisitViewModel::class)
    internal abstract fun bindVisitViewModel(viewModel: VisitViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VisitInfoViewModel::class)
    internal abstract fun bindVisitInfoViewModel(viewModel: VisitInfoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoricViewModel::class)
    internal abstract fun bindHistoricViewModel(viewModel: HistoricViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(HistoricMssViewModel::class)
    internal abstract fun bindHistoricMssViewModel(viewModel: HistoricMssViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActViewModel::class)
    internal abstract fun bindActViewModel(viewModel: ActViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DocumentViewModel::class)
    internal abstract fun bindDocumentViewModel(viewModel: DocumentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddDocumentViewModel::class)
    internal abstract fun bindAddDocumentViewModel(viewModel: AddDocumentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransportViewModel::class)
    internal abstract fun bindTransportViewModel(viewModel: TransportViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NotificationViewModel::class)
    internal abstract fun bindNotificationViewModel(viewModel: NotificationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ManageMssViewModel::class)
    internal abstract fun bindManageMssViewModel(viewModel: ManageMssViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MssProductViewModel::class)
    internal abstract fun bindMssProductViewModel(viewModel: MssProductViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MssCompanyViewModel::class)
    internal abstract fun bindMssCompanyViewModel(viewModel: MssCompanyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MssNsoViewModel::class)
    internal abstract fun bindMssNsoViewModel(viewModel: MssNsoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CloseViewModel::class)
    internal abstract fun bindCloseViewModel(viewModel: CloseViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CloseVisitViewModel::class)
    internal abstract fun bindCloseVisitViewModel(viewModel: CloseVisitViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CloseIvoViewModel::class)
    internal abstract fun bindCloseIvoViewModel(viewModel: CloseIvoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DenunciationViewModel::class)
    internal abstract fun bindDenunciationViewModel(viewModel: DenunciationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TechnicalViewModel::class)
    internal abstract fun bindTechnicalViewModel(viewModel: TechnicalViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InteractionViewModel::class)
    internal abstract fun bindInteractionViewModel(viewModel: InteractionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EquipmentViewModel::class)
    internal abstract fun bindEquipmentViewModel(viewModel: EquipmentViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterCompanyViewModel::class)
    internal abstract fun bindRegisterCompanyViewModel(viewModel: RegisterCompanyViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(AddressViewModel::class)
    internal abstract fun bindAddressViewModel(viewModel: AddressViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PapfViewModel::class)
    internal abstract fun bindPapfViewModel(viewModel: PapfViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InfoTramitViewModel::class)
    internal abstract fun bindInfoTramitViewModel(viewModel: InfoTramitViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(InvoiceViewModel::class)
    internal abstract fun bindInvoiceViewModel(viewModel: InvoiceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(TransportPapfViewModel::class)
    internal abstract fun bindTransportPapfViewModel(viewModel: TransportPapfViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterProductsViewModel::class)
    internal abstract fun bindRegisterProductsViewModel(viewModel: RegisterProductsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CertificateViewModel::class)
    internal abstract fun bindCertificateViewModel(viewModel: CertificateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DocumentationViewModel::class)
    internal abstract fun bindDocumentationViewModel(viewModel: DocumentationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RegisterDateViewModel::class)
    internal abstract fun bindRegisterDateViewModel(viewModel: RegisterDateViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DiligenceViewModel::class)
    internal abstract fun bindDiligenceViewModel(viewModel: DiligenceViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DataReqViewModel::class)
    internal abstract fun bindDataReqViewModel(viewModel: DataReqViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DataReqActSampleViewModel::class)
    internal abstract fun bindDataReqActSampleViewModel(viewModel: DataReqActSampleViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CloseInspectionViewModel::class)
    internal abstract fun bindCloseInspectionViewModel(viewModel: CloseInspectionViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EmitCisViewModel::class)
    internal abstract fun bindEmitCisViewModel(viewModel: EmitCisViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailProductViewModel::class)
    internal abstract fun bindDetailProductViewModel(viewModel: DetailProductViewModel): ViewModel
}