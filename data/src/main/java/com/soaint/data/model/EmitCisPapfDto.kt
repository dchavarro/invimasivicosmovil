package com.soaint.data.model

import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.model.*

data class EmitCisPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: EmitCisPapfDto? = null,
    @SerializedName("statusCode") val statusCode: Int,
)

data class EmitCisPapfDto(
    @SerializedName("Cierre") val cierre: List<EmitCisClosePapfDto>? = null,
    @SerializedName("Observaciones") val observaciones: List<EmitCisObservationPapfDto>? = null,
)

fun EmitCisPapfDto.mapToDomain() = EmitCisPapfEntity(
    cierre?.firstOrNull()?.mapToDomain(),
    observaciones?.map { it.mapToDomain() },
)

data class EmitCisClosePapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("IdUsoMPIG") val idUsoMPIG: Int? = 0,
    @SerializedName("DescripcionUsoMPIG") val descripcionUsoMPIG: String?,
    @SerializedName("IdResultadoCIS") val idResultadoCIS: Int? = 0,
    @SerializedName("DescripcionResultadoCis") val descripcionResultadoCis: String?,
    @SerializedName("IdFirmante") val idFirmante: Int? = 0,
    @SerializedName("Firmante") val firmante: String?,
    @SerializedName("IdIdioma") val idIdioma: Int? = 0,
    @SerializedName("DescripcionIdioma") val descripcionIdioma: String?,
    @SerializedName("IdTipoCertificado") val idTipoCertificado: Int? = 0,
    @SerializedName("DescripcionTipoCertificado") val descripcionTipoCertificado: String?,
)

fun EmitCisClosePapfDto.mapToDomain() = EmitCisClosePapfEntity(
    idSolicitud,
    idUsoMPIG,
    descripcionUsoMPIG,
    idResultadoCIS,
    descripcionResultadoCis,
    idFirmante,
    firmante,
    idIdioma,
    descripcionIdioma,
    idTipoCertificado = idTipoCertificado,
    descripcionTipoCertificado = descripcionTipoCertificado
)

fun EmitCisClosePapfDto.mapToDb() = EmitCisClosePapfModel(
    idSolicitud,
    idUsoMPIG,
    descripcionUsoMPIG,
    idResultadoCIS,
    descripcionResultadoCis,
    idFirmante,
    firmante,
    idIdioma,
    descripcionIdioma,
    null,
    null,
    idTipoCertificado = idTipoCertificado,
    descripcionTipoCertificado = descripcionTipoCertificado,
    null,
    null,
    null,
    null,
)

fun EmitCisClosePapfEntity.mapToData() = EmitCisClosePapfDto(
    idSolicitud,
    idUsoMPIG,
    descripcionUsoMPIG,
    idResultadoCIS,
    descripcionResultadoCis,
    idFirmante,
    firmante,
    idIdioma,
    descripcionIdioma,
    idTipoCertificado = idTipoCertificado,
    descripcionTipoCertificado = descripcionTipoCertificado
)

data class EmitCisObservationPapfDto(
    @SerializedName("descripcion") val descripcion: String?,
)

fun EmitCisObservationPapfDto.mapToDomain() = EmitCisObservationPapfEntity(
    descripcion,
)

fun EmitCisObservationPapfDto.mapToDb(idSolicitud: Int, isDefault: Boolean) = EmitCisObservationPapfModel(
    descripcion,
    idSolicitud,
    isDefault
)

fun EmitCisObservationPapfEntity.mapToData() = EmitCisObservationPapfDto(
    descripcion,
)


data class InfoEmitirCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<InfoEmitirCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class InfoEmitirCDDto(
    @SerializedName("FuncionarioInvima") val funcionarioInvima: String?,
    @SerializedName("NombreLaboratorio") val nombreLaboratorio: String?
)

fun InfoEmitirCDDto.mapToDomain() = InfoEmitirPapfEntity(
    funcionarioInvima,
    nombreLaboratorio,
)

fun InfoEmitirCDDto.mapToDb(idSolicitud: Int) = EmitCisInfoPapfModel(
    funcionarioInvima,
    nombreLaboratorio,
    idSolicitud
)

data class CheckListCDResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val objectResponse: List<CheckListDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class CheckListDto(
    @SerializedName("IdListaChequeo") val idListaChequeo: Int?,
    @SerializedName("ListaChequeo") val listaChequeo: String?,
    @SerializedName("Observacion_ListaChequeo") val observacionListaChequeo: String?,
    @SerializedName("VerificacionInformacion") val verificacionInformacion: String?,
    @SerializedName("requerimiento") val requerimiento: String?,
    @SerializedName("Contenedor_ListaChequeo") val contenedorListaChequeo:  List<CheckDto>?,
    @SerializedName("IdSolicitud") val idSolicitud: Int?,
    @SerializedName("NombreGeneraListaChequeo") val nombreGeneraListaChequeo: String?,
    @SerializedName("FechaCreacion") val fechaCreacion: String?,
)

data class CheckDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("identificacion") val identificacion: String?,
)
fun CheckListDto.mapToDomain(): CheckListEntity {
    val gson = Gson()
    val contenedorListaChequeoJson: String? = gson.toJson(contenedorListaChequeo)

    return CheckListEntity(
        idListaChequeo,
        listaChequeo,
        observacionListaChequeo,
        verificacionInformacion,
        requerimiento,
        contenedorListaChequeoJson,
        idSolicitud,
        nombreGeneraListaChequeo,
        fechaCreacion,
    )
}


fun CheckListDto.mapToDb() : EmitCisCheckPapfModel {
        val gson = Gson()
        val contenedorListaChequeoJson: String? = gson.toJson(contenedorListaChequeo)
        return EmitCisCheckPapfModel(
            idListaChequeo,
            listaChequeo,
            observacionListaChequeo,
            verificacionInformacion,
            requerimiento,
            contenedorListaChequeoJson,
            idSolicitud,
            nombreGeneraListaChequeo,
            fechaCreacion,
        )
    }
