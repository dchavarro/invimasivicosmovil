package com.soaint.domain.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class TypeDaoEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val descripcion: String? = EMPTY_STRING,
    val tipoDao: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class CountriesEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val nombre: String? = EMPTY_STRING,
    val idZonaGeografica: Int? = 0,
) : Parcelable

@Parcelize
data class DepartmentEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val nombre: String? = EMPTY_STRING,
    val idPais: Int? = 0,
) : Parcelable

@Parcelize
data class TownEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val nombre: String? = EMPTY_STRING,
    val idDepartamento: Int? = 0,
) : Parcelable