package com.soaint.sivicos_dinamico.flow.visit.views.historicMSS

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentHistoricMssBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.HistoricMssItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class HistoricMssFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentHistoricMssBinding>(R.layout.fragment_historic_mss)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: HistoricMssViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.historico_mss))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        binding.cardView.isVisible = false
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(HistoricMssFragmentDirections.toInfo())
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnHistoricMssViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnHistoricMssViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnHistoricMssViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnHistoricMssViewModelEvent.OnViewModelReady -> onViewModelReady(event.historicMss, event.mss)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReady(historic: List<HistoricMssEntity>?, mss: List<List<MssEntity>>?) {
        adapter.clear()
        val items = mutableListOf<Group>()

        if (!historic.isNullOrEmpty()) {
            binding.cardView.isVisible = true
            binding.textViewNumber.text = historic.firstOrNull()?.numeroExpediente
            binding.textViewName.text = historic.firstOrNull()?.razonSocial
            binding.textViewAddress.text = historic.firstOrNull()?.direccion
            binding.textViewCity.text = historic.firstOrNull()?.municipio
        }

        if (!mss.isNullOrEmpty()) {
            mss.first().map { HistoricMssItemView(it, ::onHistoricSelected) }.let { items.addAll(it) }
        }

        val isVisible = mss?.firstOrNull().isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }

    private fun onHistoricSelected(historic: MssEntity) { }
}