package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface ModeloIvcRepository {

    val ivc: StateFlow<List<ModeloIvcEntity>?>

    suspend fun getRequerimientos(idVisita: String): Result<List<ModeloIvcEntity>?>

    suspend fun getRequerimientosLocal(idVisita: Int): Result<List<ModeloIvcEntity>>

    suspend fun deleteRequerimientoLocal(it: ModeloIvcEntity, idVisita: Int): Result<Unit>

    suspend fun deleteRequerimiento(): Result<Unit>

    suspend fun addRequerimiento(): Result<Unit>

    suspend fun addRequerimientoLocal(body: ModeloIvcEntity, idVisita: String): Result<Unit>

    suspend fun getReqDa(idVisita: String): Result<List<IvcDaEntity>>

    suspend fun getReqDaLocal(idVisita: String): Result<IvcDaEntity?>

    suspend fun addReqDA(): Result<Unit>

    suspend fun addReqDALocal(body: IvcDaEntity, idVisita: String): Result<Unit>

    suspend fun getCountIvcReq(): Result<Int>

}