package com.soaint.data.repository

import com.google.gson.Gson
import com.soaint.data.api.ModeloIvcApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.ModeloIvcDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.common.getIp
import com.soaint.domain.model.IvcDaEntity
import com.soaint.domain.model.ModeloIvcEntity
import com.soaint.domain.repository.ModeloIvcRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class ModeloIvcRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val modeloIvcApi: ModeloIvcApi,
    private val modeloIvcDao: ModeloIvcDao,
    ) : ModeloIvcRepository, BaseRepository() {

    private val _ivc = MutableStateFlow<List<ModeloIvcEntity>?>(null)
    override val ivc get() = _ivc.asStateFlow()
    val gson = Gson()
    //Carga de requirimetos por visita
    override suspend fun getRequerimientos(idVisita: String): Result<List<ModeloIvcEntity>?> = execute {
        val userName = userRepository.userInformation.value?.usuario.toString()
        val ivc = modeloIvcApi.getRequerimientos(
            BodyGenericVisitDto(getIp(), userName, idVisita.toInt())
        ).objectResponse
        modeloIvcDao.deleteIvc(idVisita)
        saveDao(ivc.orEmpty(), idVisita)
        Result.success(ivc?.mapNotNull { it.mapToDomain() }.orEmpty())
    }

    //Carga de requirimetos por visita DB local
    override suspend fun getRequerimientosLocal(idVisita: Int): Result<List<ModeloIvcEntity>> {
        val result = modeloIvcDao.selectIvc(idVisita)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) }?: Result.failure(Throwable())
    }

    //Guardar requirimetos en DB local
    private suspend fun saveDao(data: List<ModeloIvcDto>?, idVisita: String) {
        data?.onEach { ivc ->
            val model = ivc.mapToDb(idVisita.toInt(), false)
            modeloIvcDao.insertIvc(model)
        }.orEmpty()
    }

    // Servicio insert aplicacion medida sanitaria
    override suspend fun deleteRequerimientoLocal(it: ModeloIvcEntity, idVisita: Int): Result<Unit> = execute {
        val result = if (it.isLocal == true) {
            modeloIvcDao.deleteIvcById(it.id.orZero())
        } else {
            modeloIvcDao.updateIvc(it.id.orZero(), true)
        }

        Result.success(result)
    }

    // Servicio insert aplicacion medida sanitaria
    override suspend fun deleteRequerimiento(): Result<Unit> = execute {
        val item = modeloIvcDao.selectIvcAll(true, false).orEmpty()
        val result = if (item != null) {
            item.map { modeloIvcApi.deleteRequerimiento(it.id.orZero()) }
        } else { Unit }
        Result.success(result)
    } as Result<Unit>



    // Servicio Documento with Value
    override suspend fun addRequerimiento(): Result<Unit> = execute {
        val item = modeloIvcDao.selectIvcAll(false, true).orEmpty()
        val result = if (item != null) {
            item.map {
                modeloIvcApi.saveRequerimiento(
                    BodyModeloIvcDto(
                        it.idTipoRequerimiento,
                        it.numeroRequerimiento,
                        it.idCriticidad,
                        it.idVisita
                    )
                )
                modeloIvcDao.deleteIvcById(it.id.orZero())
            }
        } else { Unit }
        Result.success(result)
    } as Result<Unit>

    //Guardar Documento Local with Value
    override suspend fun addRequerimientoLocal(body: ModeloIvcEntity, idVisita: String): Result<Unit> {
        val newId = modeloIvcDao.selectAllIvc()?.lastOrNull()?.id?.orZero().orZero()
        val result = modeloIvcDao.insertIvc(body.copy(id = newId + 1).mapToModel(idVisita.toInt(), false))
        return Result.success(result)
    }


    //Carga de requirimetos datos adicional por visita
    override suspend fun getReqDa(idVisita: String): Result<List<IvcDaEntity>> = execute {
        val userName = userRepository.userInformation.value?.usuario.toString()
        val ivc = modeloIvcApi.getReqDA(BodyGenericVisitDto(getIp(), userName, idVisita.toInt())).objectResponse
        modeloIvcDao.deleteIvcDa(idVisita)
        saveDaDao(ivc.orEmpty(), idVisita)
        Result.success(ivc?.map { it.mapToDomain(false) }.orEmpty())
    }

    //Guardar requirimetos datos adicional en DB local
    private suspend fun saveDaDao(data: List<IvcDaDto>?, idVisita: String) {
        data?.onEach { ivc ->
            val model = ivc.mapToDb(idVisita.toInt(), false)
            modeloIvcDao.insertIvcDa(model)
        }.orEmpty()
    }

    //Guardar Documento Local with Value
    override suspend fun addReqDALocal(body: IvcDaEntity, idVisita: String): Result<Unit> {
        val result = modeloIvcDao.insertOrUpdateDA(body.mapToModel(idVisita.toInt()))
        return Result.success(result)
    }

    // Servicio Documento with Value
    override suspend fun addReqDA(): Result<Unit> = execute {
        val item = modeloIvcDao.selectIvcDaLocal(true).orEmpty()
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val result = if (item != null) {
            item.map {
                val idTipoActividadList: List<Int> = gson.fromJson(it.idTipoActividad ?: "[]", Array<Int>::class.java).toList()
                val idPruebasLaboratorio: List<Int> = gson.fromJson(it.idPruebasLaboratorio ?: "[]", Array<Int>::class.java).toList()
                val idProductosProcesados: List<Int> = gson.fromJson(it.idProductosProcesados ?: "[]", Array<Int>::class.java).toList()

                modeloIvcApi.saveReqDa(
                    BodyGenericIvcDaDto(
                        getIp(),
                        userName,
                        BodyIvcDaDto(it.totalRequerimientos,
                        it.numeralesDeficiencias,
                        it.idDistribucion,
                        it.volumenBeneficio,
                        it.veterinarioPlanta,
                        idTipoActividadList,
                        idProductosProcesados,
                        idPruebasLaboratorio,
                        it.idVisita)
                    )
                )
                modeloIvcDao.deleteIvcDaById(it.id.orZero())
            }
        } else { Unit }
        Result.success(result)
    } as Result<Unit>

    //Carga de requirimetos por visita DB local
    override suspend fun getReqDaLocal(idVisita: String): Result<IvcDaEntity?> {
        val result = modeloIvcDao.selectIvcDaByVisit(idVisita)?.mapToDomain()
        return result.let { Result.success(it) }
    }

    override suspend fun getCountIvcReq(): Result<Int> = Result.success(
        modeloIvcDao.selectCountIvcReqLocal(true)
    )
}