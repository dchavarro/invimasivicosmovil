package com.soaint.sivicos_dinamico.flow.visit.item_view

import androidx.core.view.isVisible
import com.soaint.domain.model.CloseInspObservationPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EmitCisObservationPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemObservationPapfBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class ObservationEmitPapfItemView(
    private val item: EmitCisObservationPapfEntity,
    private val onEdit: (EmitCisObservationPapfEntity) -> Unit,
    private val onDelete: (EmitCisObservationPapfEntity) -> Unit,
) : BindableItem<ItemObservationPapfBinding>() {

    override fun bind(
        viewBinding: ItemObservationPapfBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = item.descripcion
        buttonEdit.isVisible = item.isDefault == false
        buttonDelete.isVisible = item.isDefault == false
        buttonEdit.setOnSingleClickListener { onEdit.invoke(item) }
        buttonDelete.setOnSingleClickListener { onDelete.invoke(item) }
    }

    override fun getLayout() = R.layout.item_observation_papf
}