package com.soaint.sivicos_dinamico.flow.papf.views.detail_product

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import com.soaint.domain.model.DetailProductObjectPapfEntity
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentDetailProductBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.item_view.OtherDetailProductPapfItemView
import com.soaint.sivicos_dinamico.flow.papf.item_view.SubpartidaPapfItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ID_TYPE_EXPORTER
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class DetailProductFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentDetailProductBinding>(R.layout.fragment_detail_product)

    private val viewModel by activityViewModels<DetailProductViewModel> { viewModelFactory }

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val adapterSubpartida by lazy { GroupieAdapter() }

    private val adapterOther by lazy { GroupieAdapter() }

    private val args by navArgs<DetailProductFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = binding.run {
        initView()
        subscribeViewModel()
        root
    }

    private fun initView(): View {
        binding.viewModel = viewModel
        binding.root.isVisible = false
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_information_product))
        initAdapter()
        return binding.root
    }

    fun initAdapter() {
        binding.recyclerViewSubpartida.adapter = adapterSubpartida
        binding.recyclerViewOther.adapter = adapterOther
        adapterSubpartida.clear()
        adapterOther.clear()
    }

    private fun subscribeViewModel() {
        subscribeViewModel(viewModel, binding.root)
        mainViewModel.infoTramit.observe(viewLifecycleOwner, viewModel::loadInfoTramit)
        viewModel.event.observe(viewLifecycleOwner, ::stateHandler)

        val product = args.product
        product?.idSolicitud?.let { idSolicitud ->
            product.idProductoSolicitud?.let { idProductoSolicitud ->
                product.idClasificacionProducto?.let { idClasificacionProducto ->
                    viewModel.init(idSolicitud, idProductoSolicitud, idClasificacionProducto)
                }
            }
        }
    }

    private fun stateHandler(state: OnViewModelEvent) {
        showLoading(false)
        when (state) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true)
            is OnViewModelEvent.OnViewModelReady -> onSuccess(state.detailProduct, state.infoTramit)
            is OnViewModelEvent.OnLoadingError -> onTokenError(state.error)
        }
    }

    private fun onSuccess(
        detailProduct: DetailProductObjectPapfEntity,
        infoTramit: InfoTramitePapfEntity?
    ) {
        binding.root.isVisible = true
        val items = mutableListOf<Group>()
        val itemsOther = mutableListOf<Group>()
        if (!detailProduct.subpartida.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_subpartida)))
            items.addAll(detailProduct.subpartida!!.mapNotNull { SubpartidaPapfItemView(it) })
        }
        adapterSubpartida.addAll(items)
        val isVisible = detailProduct.subpartida.isNullOrEmpty()
        binding.recyclerViewSubpartida.isVisible = !isVisible
        adapterSubpartida.updateAsync(items)

        if (infoTramit?.idTipoTramite == ID_TYPE_EXPORTER) {
            itemsOther.add(TitleItemView(getString(R.string.copy_destino)))
            if (!detailProduct.destinatario.isNullOrEmpty()) {
                itemsOther.add(
                    OtherDetailProductPapfItemView(
                        detailProduct.destinatario!!.first(),
                        getString(R.string.copy_destinity)
                    )
                )
            }
            if (!detailProduct.lugarDestino.isNullOrEmpty()) {
                itemsOther.add(
                    OtherDetailProductPapfItemView(
                        detailProduct.lugarDestino!!.first(),
                        getString(R.string.copy_place_destinity)
                    )
                )
            }
            if (!detailProduct.operadorResponsable.isNullOrEmpty()) {
                itemsOther.add(
                    OtherDetailProductPapfItemView(
                        detailProduct.operadorResponsable!!.first(),
                        getString(R.string.copy_operator_responsability)
                    )
                )
            }
            if (!detailProduct.puertoControlFronterizo.isNullOrEmpty()) {
                itemsOther.add(
                    OtherDetailProductPapfItemView(
                        detailProduct.puertoControlFronterizo!!.first(),
                        getString(R.string.copy_port_control)
                    )
                )
            }

            adapterOther.addAll(itemsOther)
            adapterOther.updateAsync(itemsOther)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }
}