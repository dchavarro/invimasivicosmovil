package com.soaint.data.api

import com.soaint.data.model.CountriesResponseDto
import com.soaint.data.model.DepartmentResponseDto
import com.soaint.data.model.TownResponseDto
import com.soaint.data.model.TypeDaoResponseDto
import retrofit2.http.GET
import retrofit2.http.Path

interface MasterTransApi {

    @GET("v1/maestro/listarTodos/{tipo}")
    suspend fun getTypesDao(
        @Path("tipo") tipo: String,
    ): TypeDaoResponseDto

    @GET("v1/maestro/listarTodos/PaisDAO")
    suspend fun getCountries(
    ): CountriesResponseDto

    @GET("v1/maestro/listarTodos/DepartamentoDAO")
    suspend fun getDepartments(
    ): DepartmentResponseDto

    @GET("v1/maestro/listarTodos/MunicipioDAO")
    suspend fun getTown(
    ): TownResponseDto
}