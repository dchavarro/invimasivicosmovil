package com.soaint.data.common

import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.dispatchers.AppDispatchers
import com.soaint.domain.provider.NetworkProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import javax.inject.Inject

abstract class BaseRepository {

    @Inject
    lateinit var appDispatchers: AppDispatchers

    @Inject
    lateinit var networkProvider: NetworkProvider

    protected suspend fun <T> execute(
        coroutineDispatcher: CoroutineDispatcher = appDispatchers.networkDispatcher(),
        action: suspend () -> Result<T>
    ) = withContext(coroutineDispatcher) {
        try {
            action()
/*            if (networkProvider.isAvailable) {
                action()
            } else {
                Result.failure(WithoutConnectionException())
            }*/
        } catch (e: Exception) {
            e.printStackTrace()
            val error = when (e) {
                is ConnectException, is SocketTimeoutException, is UnknownHostException -> WithoutConnectionException()
                else -> e
            }
            Result.failure(error)
        }
    }
}