package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HolidaysEntity(
    val idCalendario: Int?,
    val nombre: String,
    val fechaEvento: String,
    val activo: Boolean,
    val fechaCreacion: String?,
    val usuarioCrea: String?,
    val fechaModifica: String?,
    val usuarioModifica: String?
) : Parcelable
