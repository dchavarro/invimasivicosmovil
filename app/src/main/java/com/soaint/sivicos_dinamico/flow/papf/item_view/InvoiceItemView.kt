package com.soaint.sivicos_dinamico.flow.papf.item_view

import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.InvoicePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemInvoiceBinding
import com.soaint.sivicos_dinamico.utils.ID_TYPE_EXPORTER
import com.soaint.sivicos_dinamico.utils.ID_TYPE_IMPORTER
import com.xwray.groupie.databinding.BindableItem

class InvoiceItemView(
    private val invoice: InvoicePapfEntity,
    private val idTipoTramite: Int?
) : BindableItem<ItemInvoiceBinding>() {

    override fun bind(
        viewBinding: ItemInvoiceBinding,
        position: Int
    ) = with(viewBinding) {
        textViewDoc.text = invoice.numeroDocumento
        textViewName.text = invoice.nombreEmpresa
        textViewInvoice.text = invoice.numeroFactura
        textViewDate.text = invoice.fechaExpedicion
        textViewCountry.text = invoice.pais

        when(idTipoTramite){
            ID_TYPE_IMPORTER->{
                textViewTitleDoc.isVisible = false
                textViewDoc.isVisible = false
            }
            ID_TYPE_EXPORTER->{
                textViewCountry.isVisible = false
                textViewTitleCountry.isVisible = false
            }

        }

    }

    override fun getLayout() = R.layout.item_invoice
}