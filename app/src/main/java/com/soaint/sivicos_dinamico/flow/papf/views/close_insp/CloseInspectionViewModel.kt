package com.soaint.sivicos_dinamico.flow.papf.views.close_insp

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.ClasificationTramitPapfEntity
import com.soaint.domain.model.CloseInspObservationPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.ValidationUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelSuccess(@StringRes val message: Int) : OnViewModelEvent()
    data class validateMaximiumObservations(@StringRes val message: Int) : OnViewModelEvent()
    data class OnViewModelReady(val clasificationTramit: ClasificationTramitPapfEntity?) :
        OnViewModelEvent()

    data class OnViewModelObservationReady(val observation: List<CloseInspObservationPapfEntity>?) :
        OnViewModelEvent()

    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
    data class onViewModelValidateActs(val acts: List<String>) : OnViewModelEvent()

    data class OnValidateDataImport(
        val mpigId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateDataReq(
        val requisitos: Boolean?,
        val justification: String?,
    ) : OnViewModelEvent()

    data class OnValidateData(
        val firmatId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateObservation(
        val observation: String?,
    ) : OnViewModelEvent()
}

class CloseInspectionViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
    private val userUseCase: UserUseCase,
    private val validationUseCase: ValidationUseCase
) : BaseViewModel() {

    private val _query = MutableLiveData<List<DinamicQuerysPapfEntity>>()
    val query = _query.asLiveData()

    private var mpigSelected: DinamicQuerysPapfEntity? = null

    private var resultCisSelected: DinamicQuerysPapfEntity? = null

    private var firmatSelected: DinamicQuerysPapfEntity? = null

    private var idiomSelected: DinamicQuerysPapfEntity? = null

    private val _visibleReq = MutableLiveData<Boolean>()
    val visibleReq = _visibleReq.asLiveData()

    private val _infoTramitData = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramitData = _infoTramitData.asLiveData()

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _clasificationTramit = MutableLiveData<ClasificationTramitPapfEntity?>()
    val clasificationTramit = _clasificationTramit.asLiveData()

    private val _itemObservation = MutableLiveData<CloseInspObservationPapfEntity?>()
    val itemObservation = _itemObservation.asLiveData()

    private val _requisitos = MutableLiveData<Boolean>()
    val requisitos = _requisitos.asLiveData()

    private val _justification = MutableLiveData(EMPTY_STRING)
    val justification get() = _justification.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    fun init(infoTramit: InfoTramitePapfEntity) = execute {
        _infoTramitData.value = infoTramit
        _visibleReq.value = infoTramit.estado == STATUS_INFO_TRAMIT_12
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        loadDataCloseIns()
        getObservation()
    }

    private suspend fun loadDataCloseIns() {
        _query.value = papfUseCase.loadDinamicQuerysLocal().getOrNull().orEmpty()
        _clasificationTramit.value = _infoTramitData.value?.idSolicitud?.let {
            papfUseCase.loadDataCloseInsLocal(it).getOrNull()
        }
        if (_clasificationTramit.value != null) _event.value =
            OnViewModelEvent.OnViewModelReady(_clasificationTramit.value)
    }

    fun setItemSelected(itemSelected: String, codeQuery: String) {
        val query = _query.value
        when (codeQuery) {
            CD_MPIG -> this.mpigSelected = query?.find {
                it.descripcion.equals(
                    itemSelected,
                    true
                ) && it.codeQuery == codeQuery
            }
            CD_RESULT_CIS -> this.resultCisSelected = query?.find {
                it.descripcion.equals(
                    itemSelected,
                    true
                ) && it.codeQuery == codeQuery
            }
            CD_FIRMATE -> this.firmatSelected = query?.find {
                it.descripcion.equals(
                    itemSelected,
                    true
                ) && it.codeQuery == codeQuery
            }
            CD_IDIOM -> this.idiomSelected = query?.find {
                it.descripcion.equals(
                    itemSelected,
                    true
                ) && it.codeQuery == codeQuery && _infoTramitData.value?.idTipoTramite.toString() == it.idTipoTramite
            }
        }
    }

    fun setRespondeReq(data: Boolean?) {
        _requisitos.value = data
    }

    fun setObservation(itemObservation: CloseInspObservationPapfEntity) {
        _itemObservation.value = itemObservation
    }

    fun onJustificationChange(data: Editable?) {
        _justification.value = data.toString()
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun validateActs() = execute {
        val codigoPlantilla = when (_infoTramitData.value?.idTipoProducto) {
            ID_MISIONAL_PAPF_ALIMENTOS -> IVC_INS_FM055
            ID_MISIONAL_PAPF_BEBIDAS -> IVC_INS_FM156
            else -> EMPTY_STRING
        }
        val result =
            _infoTramitData.value?.idSolicitud?.let {
                validationUseCase.validationActs(
                    it,
                    listOf(codigoPlantilla)
                )
            }
        _event.value =
            OnViewModelEvent.onViewModelValidateActs(result?.getOrNull().orEmpty().distinct())
    }


    fun checkData() {
        val mpigId = mpigSelected?.id
        val firmatId = firmatSelected?.id
        val responseReq = _requisitos.value
        val justification = _justification.value

        // Validar campos obligatorios
        if (_clasificationTramit.value?.idTipoActividad == ID_TYPE_IMPORTER) {
            if (mpigId == null || mpigId == 0)
                _event.value = OnViewModelEvent.OnValidateDataImport(mpigId)
        }

        if (_clasificationTramit.value?.respuestaRequerimientoIF == true) {
            if (justification.isNullOrBlank() || responseReq == null)
                _event.value = OnViewModelEvent.OnValidateDataReq(responseReq, justification)
        }

        if (firmatId == null || firmatId == 0)
            _event.value = OnViewModelEvent.OnValidateData(firmatId)
        else insertOrUpdate()
    }

    fun insertOrUpdate() = execute {
        val body = _clasificationTramit.value?.copy(
            idIdioma = idiomSelected?.id,
            idioma = idiomSelected?.descripcion,
            idFirmante = firmatSelected?.id,
            firmante = firmatSelected?.descripcion,
            idResultadoCIS = resultCisSelected?.id,
            resultadoCIS = resultCisSelected?.descripcion,
            idUsoMPIG = mpigSelected?.id,
            usoMPIG = mpigSelected?.descripcion,
            justificacion = _justification.value,
            respuestaRequerimientoIF = _requisitos.value,
            idSolicitud = _clasificationTramit.value?.idSolicitud,
        )
        val result = papfUseCase.insertOrUpdateCloseInsFisLocal(body)
        _event.value = if (result.isSuccess) {
            validateObservation(true)
            loadDataCloseIns()
            OnViewModelEvent.OnViewModelSuccess(R.string.copy_add_success)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }

    fun validateObservation(saveCloseIns: Boolean = false) = execute {
        val idSolicitud = _clasificationTramit.value?.idSolicitud.orZero()
        val isMaximum = papfUseCase.hasReachedMaximumObservationsInspe(idSolicitud)
        if (isMaximum){
            val res = if (saveCloseIns) R.string.limit_observations_save else R.string.limit_observations
            if (!_observation.value.isNullOrBlank()) _event.value = OnViewModelEvent.validateMaximiumObservations(res)
            _observation.value = null
        }else{
            addObservation()
        }
    }

    fun addObservation() = execute {
        if (!_observation.value.isNullOrBlank()) {
            val result = papfUseCase.insertOrUpdateObservationLocal(
                CloseInspObservationPapfEntity(
                    _observation.value,
                    _clasificationTramit.value?.idSolicitud,
                    _itemObservation.value?.id,
                )
            )
            _event.value = if (result.isSuccess) {
                _itemObservation.value = null
                _observation.value = EMPTY_STRING
                getObservation()
                OnViewModelEvent.OnViewModelSuccess(R.string.copy_add_success)
            } else {
                OnViewModelEvent.OnLoadingError(null)
            }
        } else _event.value = OnViewModelEvent.OnValidateObservation(_observation.value)
    }

    fun getObservation() = execute {
        var observationsDefault: ArrayDeque<CloseInspObservationPapfEntity> = ArrayDeque()
        val idSolicitud = _clasificationTramit.value?.idSolicitud.orZero()
        val idActividad = _clasificationTramit.value?.idActividad.orZero()
        val result = papfUseCase.loadObservationCloseIns(idSolicitud)
        _event.value = if (result.isSuccess) {
            _query.value?.filter {
                it.codeQuery == CD_OBSERVATION_PAPF && it.idTipoTramite == _clasificationTramit.value?.idActividad.orZero()
                    .toString() && idActividad == ID_TYPE_IMPORTER
            }?.mapNotNull {
                observationsDefault.add(
                    CloseInspObservationPapfEntity(
                        it.descripcion.orEmpty(),
                        isDefault = true
                    )
                )
            }
            val observations =
                (observationsDefault + result.getOrNull().orEmpty()).distinctBy { it.descripcion }
            OnViewModelEvent.OnViewModelObservationReady(observations)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }

    fun deleteObservation(itemObservation: CloseInspObservationPapfEntity) = execute {
        val result = papfUseCase.deleteObservationLocal(itemObservation)
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.OnViewModelSuccess(R.string.copy_delete_success)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
        getObservation()
    }
}
