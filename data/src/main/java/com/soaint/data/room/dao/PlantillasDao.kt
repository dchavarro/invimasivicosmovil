package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.room.tables.*

@Dao
interface PlantillasDao {

    //Plantillas
    @Query("SELECT * FROM plantilla_table")
    suspend fun selectAllPlantilla(): List<PlantillaModel>

    @Query("SELECT * FROM plantilla_table WHERE id = :idPlanilla")
    suspend fun selectPlantilla(idPlanilla: String): List<PlantillaModel>

    @Insert(entity = PlantillaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlantilla(plantillas: PlantillaModel)

    @Query("DELETE FROM plantilla_table")
    suspend fun deleteAllPlantillas()


    //Misionales
    @Query("SELECT * FROM misional_table WHERE idPlantilla = :idPlanilla")
    suspend fun selectMisionales(idPlanilla: String): List<MisionalModel>

    @Insert(entity = MisionalModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMisionales(misionales: List<MisionalModel>)

    @Query("DELETE FROM misional_table")
    suspend fun deleteAllMisionales()


    //Bloques
    @Query("SELECT * FROM block_table WHERE idPlantilla = :idPlanilla ")
    suspend fun selectBlock(idPlanilla: String): List<BlockModel>

    @Insert(entity = BlockModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBlock(blocks: BlockModel)

    @Query("DELETE FROM block_table")
    suspend fun deleteAllBlock()


    //Bloques Atributos
    @Query("SELECT * FROM block_atribute_table WHERE idBloque = :idBloque ")
    suspend fun selectBlockAtribute(idBloque: String): List<BlockAtributePlantillaModel>

    @Insert(entity = BlockAtributePlantillaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBlockAtribute(blocksAtribute: BlockAtributePlantillaModel)

    @Query("DELETE FROM block_atribute_table")
    suspend fun deleteAllBlockAtribute()

    ///////
    @Transaction
    suspend fun insertOrUpdateBlockValue(blockValueModel: BlockValueModel) {
        val items = selectBlockValue(
            blockValueModel.idPlantilla,
            blockValueModel.idVisita,
            blockValueModel.idBloque,
            blockValueModel.idBloqueAtributo
        )
        if (items == null) insertBlockValue(blockValueModel)
        else updateBlockValue(
            blockValueModel.idPlantilla,
            blockValueModel.idVisita,
            blockValueModel.idBloque,
            blockValueModel.idBloqueAtributo,
            blockValueModel.valorString,
            blockValueModel.valorArray,
            blockValueModel.valorArrayArray,
            blockValueModel.idTipoAtributo,
        )
    }

    @Query("SELECT * FROM block_value_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND idBloque = :idBloque AND idBloqueAtributo = :idBlockAtribute")
    suspend fun selectBlockValue(
        idPlantilla: String,
        idVisita: String,
        idBloque: String,
        idBlockAtribute: String
    ): BlockValueModel?

    @Insert(entity = BlockValueModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBlockValue(blockValueModel: BlockValueModel)

    @Query("UPDATE block_value_table SET valorString = :valorString, valorArray = :valorArray, valorArrayArray = :valorArrayArray WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND idBloque = :idBloque AND idBloqueAtributo = :idBlockAtribute AND idTipoAtributo = :idTipoAtributo")
    suspend fun updateBlockValue(
        idPlantilla: String,
        idVisita: String,
        idBloque: String,
        idBlockAtribute: String,
        valorString: String?,
        valorArray: String?,
        valorArrayArray: String?,
        idTipoAtributo: Int?
    )

    @Query("UPDATE block_value_table SET valorString = :valorString WHERE idBloque = :idBloque AND idBloqueAtributo = :idBlockAtribute ")
    suspend fun updateBlockValueString(
        valorString: String?,
        idBloque: String,
        idBlockAtribute: String
    )

    @Query("UPDATE block_value_table SET valorArray = :valorArray WHERE idBloque = :idBloque AND id = :idBlockAtribute ")
    suspend fun updateBlockValueArray(
        valorArray: String?,
        idBloque: String,
        idBlockAtribute: String
    )

    @Query("UPDATE block_value_table SET valorArrayArray = :valorArrayArray WHERE idBloque = :idBloque AND id = :idBlockAtribute ")
    suspend fun updateBlockValueArrayArray(
        valorArrayArray: String?,
        idBloque: String,
        idBlockAtribute: String
    )


    //Bloques ConfigAtributos
    @Query("SELECT * FROM block_config_atribute_table WHERE idAtribute = :idAtribute ")
    suspend fun selectBlockConfigAtribute(idAtribute: String): BlockConfigAtributeModel?

    @Insert(entity = BlockConfigAtributeModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBlockConfigAtribute(blocksConfigAtribute: BlockConfigAtributeModel): Long

    @Query("DELETE FROM block_config_atribute_table")
    suspend fun deleteAllBlockAtributeConfig()


    //Bloques ListConfigAtributos
    @Query("SELECT * FROM block_list_atribute_table WHERE idConfig = :idConfig ")
    suspend fun selectBlockListConfigAtribute(idConfig: String): List<BlockListAtributeModel>

    @Insert(entity = BlockListAtributeModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBlockListConfigAtribute(blockListAtribute: BlockListAtributeModel)

    @Query("DELETE FROM block_list_atribute_table")
    suspend fun deleteAllBlockAtributeConfigList()

    @Query("SELECT * FROM block_atribute_table WHERE idBloque = :idBloque AND id = :idBlockAtribute")
    suspend fun getNameAtribute(
        idBloque: String,
        idBlockAtribute: String
    ):BlockAtributePlantillaModel
}