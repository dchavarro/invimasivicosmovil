package com.soaint.data.model

import com.google.gson.annotations.SerializedName

data class FirmaResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: String?,
    @SerializedName("statusCode") val statusCode: Int
)

data class FirmaBodyDto(
    @SerializedName("objAuditoria") val objAuditoria: AuditoriaDto,
    @SerializedName("objOperacion") val objOperacion: BodyFirmaDto,
) {
    constructor(ip: String, user: String, parametros: BodyFirmaDto) : this(
        AuditoriaDto(ip, user),
        parametros
    )
}

data class BodyFirmaDto(
    @SerializedName("pdfFileBase64") val pdfFileBase64: String?,
    @SerializedName("idTramite") val idTramite: String?,
    @SerializedName("idFuncionarioFirmante") val idFuncionarioFirmante: String?,
    @SerializedName("idFuncionarioRevisor") val idFuncionarioRevisor: String? = null,
    @SerializedName("idFuncionarioProyector") val idFuncionarioProyector: String? = null,
)
