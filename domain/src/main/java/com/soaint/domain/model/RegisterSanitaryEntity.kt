package com.soaint.domain.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RegisterSanitaryEntity(
    val infoGeneralProducto: InfoProductEntity?,
) : Parcelable

@Parcelize
data class InfoProductEntity(
    val nombreProducto: String?,
) : Parcelable