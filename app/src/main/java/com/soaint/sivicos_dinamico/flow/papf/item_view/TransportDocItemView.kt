package com.soaint.sivicos_dinamico.flow.papf.item_view

import com.soaint.domain.model.TransportDocPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTransportDocPapfBinding
import com.xwray.groupie.databinding.BindableItem

class TransportDocItemView(
    private val doc: TransportDocPapfEntity,
) : BindableItem<ItemTransportDocPapfBinding>() {

    override fun bind(
        viewBinding: ItemTransportDocPapfBinding,
        position: Int
    ) = with(viewBinding) {
        textViewDoc.text = doc.tipoDocumento
        textViewName.text = doc.numeroDocumento
        textViewFile.text = doc.nombreDocumento
        textViewInvoice.text = doc.identificacionTipoTransporte
        textViewDate.text = doc.contenedor
    }

    override fun getLayout() = R.layout.item_transport_doc_papf
}