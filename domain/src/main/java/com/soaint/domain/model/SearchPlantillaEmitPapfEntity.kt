package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SearchPlantillaEmitEntity(
    val idActividad: Int?,
    val idTipoCertificado: Int?,
    val idResultadoCis: Int?,
    val reembarque: Int?,
    val mss: Int?,
    val idIdioma: Int?,
) : Parcelable

@Parcelize
data class SearchPlantillaEmitPapfEntity(
    val codigoPlantilla: String?,
    val estado: Int?,
) : Parcelable