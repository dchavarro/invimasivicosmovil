package com.soaint.data.common

fun Int?.orZero(): Int =
    this?:0

fun String?.intOrString(): Int =
    this?.toIntOrNull()?.let { this.toInt() } ?: 0
