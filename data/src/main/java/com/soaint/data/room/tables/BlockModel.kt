package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.Gson
import com.soaint.domain.model.*

@Entity(tableName = "block_table")
class BlockModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombre") val nombre: String,
    @ColumnInfo(name = "descripcion") val descripcion: String,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String,
)

fun BlockModel.mapToDomain(atributosPlantilla: List<BlockAtributePlantillaEntity>?) = BlockEntity(
    id,
    codigo,
    nombre,
    descripcion,
    idPlantilla,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica,
    atributosPlantilla,
)

@Entity(tableName = "block_atribute_table")
class BlockAtributePlantillaModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "obligatorio") val obligatorio: Boolean?,
    @ColumnInfo(name = "idTipoAtributo") val idTipoAtributo: Int?,
    @ColumnInfo(name = "descTipoAtributo") val descTipoAtributo: String?,
    @ColumnInfo(name = "idBloque") val idBloque: Int?,
    @ColumnInfo(name = "numeroOrden") val numeroOrden: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String
)

fun BlockAtributePlantillaModel.mapToDomain(configuracion: BlockConfigAtributeEntity?) =
    BlockAtributePlantillaEntity(
        id,
        codigo,
        nombre,
        descripcion,
        obligatorio,
        configuracion,
        idTipoAtributo,
        descTipoAtributo,
        idBloque,
        numeroOrden,
        activo,
        usuarioCrea,
        fechaCreacion,
        usuarioModifica,
        fechaModifica,
        null,
        null,
        null,
    )

@Entity(
    tableName = "block_config_atribute_table",
    foreignKeys = [
        ForeignKey(
            entity = BlockAtributePlantillaModel::class,
            parentColumns = ["id"],
            childColumns = ["idAtribute"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
class BlockConfigAtributeModel(
    @ColumnInfo(name = "idAtribute") val idAtribute: Int?,
    @ColumnInfo(name = "longitud") val longitud: String?,
    @ColumnInfo(name = "rangoFinal") val rangoFinal: String?,
    @ColumnInfo(name = "rangoInicial") val rangoInicial: String?,
    @ColumnInfo(name = "requerido") val requerido: Boolean?,
    @ColumnInfo(name = "ocultar") val ocultar: Boolean?,
    @ColumnInfo(name = "seleccionado") val seleccionado: Boolean?,
    @ColumnInfo(name = "bloquearConInformacion") val bloquearConInformacion: Boolean?,
    @ColumnInfo(name = "mensajeAyuda") val mensajeAyuda: String?,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun BlockConfigAtributeModel.mapToDomain(list: List<BlockListAtributeEntity>?) =
    BlockConfigAtributeEntity(
        list,
        longitud,
        rangoFinal,
        rangoInicial,
        requerido,
        ocultar,
        seleccionado,
        bloquearConInformacion,
        mensajeAyuda
    )

@Entity(tableName = "block_list_atribute_table")
class BlockListAtributeModel(
    @ColumnInfo(name = "id") val id: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "seleccionado") val seleccionado: Boolean?,
    @ColumnInfo(name = "idConfig") val idConfig: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun BlockListAtributeModel.mapToDomain() = BlockListAtributeEntity(
    id,
    nombre,
    seleccionado
)

@Entity(tableName = "block_body_table")
class BlockBodyModel(
    @ColumnInfo(name = "idVisita") val idVisita: String,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: String,
    @ColumnInfo(name = "body") val body: String,
    @ColumnInfo(name = "isFinished") val isFinished: Boolean? = false,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

@Entity(tableName = "block_value_table")
class BlockValueModel(
    @ColumnInfo(name = "idVisita") val idVisita: String,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: String,
    @ColumnInfo(name = "idBloque") val idBloque: String,
    @ColumnInfo(name = "idBloqueAtributo") val idBloqueAtributo: String,
    @ColumnInfo(name = "valorString") val valorString: String? = null,
    @ColumnInfo(name = "valorArray") val valorArray: String? = null,
    @ColumnInfo(name = "valorArrayArray") val valorArrayArray: String? = null,
    @ColumnInfo(name = "idTipoAtributo") val idTipoAtributo: Int? = 0,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun BlockValueModel.mapToDomain() = BlockValueEntity(
    idVisita.orEmpty(),
    idPlantilla.orEmpty(),
    idBloque.orEmpty(),
    idBloqueAtributo.orEmpty(),
    valorString,
    try {
        Gson().fromJson(valorArray, ArrayList<String>().javaClass)
    } catch (e: Exception) {
        null
    },
    try {
        Gson().fromJson(valorArrayArray, ArrayList<ArrayList<String>>().javaClass)
    } catch (e: Exception) {
        null
    },
)

fun BlockBodyModel.mapToDomain() = BlockBodyValueEntity(
    idVisita.orEmpty(),
    idPlantilla.orEmpty(),
    body.orEmpty(),
    id
)
