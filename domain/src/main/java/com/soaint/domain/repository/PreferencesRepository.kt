package com.soaint.domain.repository

interface PreferencesRepository {

    fun setString(preferenceKey: String, value: String)

    fun getString(preferenceKey: String, defaultValue: String = ""): String?

    fun <T> getObject(preferenceKey: String, classOfT: Class<T>): T?

    fun setObject(preferenceKey: String, data: Any)
}
