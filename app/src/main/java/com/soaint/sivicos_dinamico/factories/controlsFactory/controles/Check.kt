package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface


class Check: ControlInterface {

    lateinit var classContext: Context
    lateinit var checkBox: CheckBox
    lateinit var cardView: CardView
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_check, null)
        cardView = view.findViewById<CardView>(R.id.cardView)
        checkBox = view.findViewById(R.id.checkBox)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        checkBox.text = control.descripcion

        checkBox.isChecked = control.configuracion?.seleccionado == true || valorAlmacenado == "true"

        checkBox.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)

        return view
    }

    override fun getData(): String {
        return checkBox.isChecked.toString()
    }

    override fun showError(mensaje: String?) {
        cardView.setBackgroundResource(R.drawable.border_red)
    }

    override fun clearError() {
        cardView.setBackgroundResource(R.drawable.border_white)
    }
}