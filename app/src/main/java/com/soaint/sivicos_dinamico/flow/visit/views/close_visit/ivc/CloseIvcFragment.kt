package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.ivc

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidbuts.multispinnerfilter.KeyPairBoolData
import com.google.gson.Gson
import com.soaint.data.common.orZero
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.IvcDaEntity
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ModeloIvcEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCloseIvcBinding
import com.soaint.sivicos_dinamico.extensions.selectValue
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.IvcItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateRadioGroup
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class CloseIvcFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCloseIvcBinding>(R.layout.fragment_close_ivc)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: CloseIvoViewModel by viewModels { viewModelFactory }

    private val adapterList by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_close_model_ivc))
        binding.includeMedic.yes.setOnSingleClickListener { viewModel.setMedic(true) }
        binding.includeMedic.no.setOnSingleClickListener { viewModel.setMedic(false) }
        binding.buttonSend.setOnSingleClickListener { viewModel.checkData() }
        binding.buttonAdd.setOnSingleClickListener { viewModel.checkAdd() }

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterList
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: onCloseIvoViewModelEvent) {
        showLoading(false)
        when (event) {
            is onCloseIvoViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onCloseIvoViewModelEvent.onLoadingError -> onError()
            is onCloseIvoViewModelEvent.OnViewModelTypeReqReady -> OnViewModelTypeReqReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelCriticReady -> OnViewModelCriticReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelIvcReady -> OnViewModelIvcReady(event.ivc)
            is onCloseIvoViewModelEvent.OnViewModelIvcDaReady -> OnViewModelIvcDaReady(event.ivcDa)
            is onCloseIvoViewModelEvent.OnViewModelPbaReady -> OnViewModelPbaReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelBankReady -> OnViewModelBankReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelProductReady -> OnViewModelProductReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelLabReady -> OnViewModelLabReady(event.result)
            is onCloseIvoViewModelEvent.OnViewModelSuccess -> onSuccess(event.message)
            is onCloseIvoViewModelEvent.onValidateTypeEvent -> {
                val type = event.validateData
                when (type) {
                    is ValidateType.OnValidateCritic -> binding.apply {
                        textViewTitleCritic.isVisible = type.isVisible
                        spinnerCritic.isVisible = type.isVisible
                        validateSpinner(spinnerCritic, type.critic)
                    }
                    is ValidateType.OnValidateDeficit -> binding.apply {
                        textInputDeficit.isVisible = type.isVisible
                        validateField(textInputDeficit, type.deficit)
                    }
                    is ValidateType.OnValidatePba -> binding.apply {
                        textViewTitlePba.isVisible = type.isVisible
                        spinnerPba.isVisible = type.isVisible
                        validateSpinner(spinnerPba, type.pba)
                    }
                    is ValidateType.OnValidateMedic -> binding.apply {
                        includeMedic.root.isVisible = type.isVisible
                        validateRadioGroup(includeMedic.radiogroup, type.medic)
                    }
                    is ValidateType.OnValidateVolumen -> binding.apply {
                        textInputVolumen.isVisible = type.isVisible
                        validateField(textInputVolumen, type.volumen)
                    }
                    is ValidateType.OnValidateBank -> binding.apply {
                        textViewTitleBank.isVisible = type.isVisible
                        binding.spinnerBank.isVisible = type.isVisible
                        binding.spinnerBank.hintText = getString(R.string.copy_option)
                        binding.spinnerBank.setClearText(getString(R.string.copy_clear))
                        binding.spinnerBank.setSearchHint(getString(R.string.copy_search))
                        validateSpinner(spinnerBank, type.bank?.firstOrNull())
                    }
                    is ValidateType.OnValidateProduct -> binding.apply {
                        textViewTitleProduct.isVisible = type.isVisible
                        binding.spinnerProduct.isVisible = type.isVisible
                        binding.spinnerProduct.hintText = getString(R.string.copy_option)
                        binding.spinnerProduct.setClearText(getString(R.string.copy_clear))
                        binding.spinnerProduct.setSearchHint(getString(R.string.copy_search))
                        validateSpinner(spinnerProduct, type.product?.firstOrNull())
                    }
                    is ValidateType.OnValidateLab -> binding.apply {
                        textViewTitleLab.isVisible = type.isVisible
                        binding.spinnerLab.isVisible = type.isVisible
                        binding.spinnerLab.hintText = getString(R.string.copy_option)
                        binding.spinnerLab.setClearText(getString(R.string.copy_clear))
                        binding.spinnerLab.setSearchHint(getString(R.string.copy_search))
                        validateSpinner(spinnerLab, type.lab?.firstOrNull())
                    }
                }
            }
            is onCloseIvoViewModelEvent.OnValidateAdd -> validateErrorAdd(
                event.typeId,
                event.number,
            )
        }
    }

    private fun onError() {
        showAlertDialog(showDialog(
            getString(R.string.error),
            getString(R.string.copy_generic_error),
            titleBtnPositive = getString(R.string.cerrar)
        ) { dismissAlertDialog() })
    }

    private fun OnViewModelTypeReqReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerType.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerType.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View, position: Int, id: Long
            ) {
                val itemSelected = items[position]
                viewModel.setTypeReqSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
        OnViewModelNumberReady()
    }

    private fun OnViewModelCriticReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerCritic.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerCritic.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View, position: Int, id: Long
            ) {
                val itemSelected = items[position]
                viewModel.setCriticSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelNumberReady() {
        val array = ArrayList<ManageMssEntity>()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        for (i in 1..200) {
            array.add(i, ManageMssEntity(descripcion = i.toString()))
        }
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerNumber.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerNumber.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View, position: Int, id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) viewModel.setNumberSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelIvcDaReady(ivc: IvcDaEntity?) {
        if (ivc != null) {
            binding.editTextDeficit.setText(
                ivc.numeralesDeficiencias.orEmpty().toString().orEmpty()
            )
            viewModel.setDeficitChange(
                ivc.numeralesDeficiencias.orEmpty().toString().orEmpty().toEditable()
            )

            ivc.descripcionDistribucion?.let {
                viewModel.setPbaSelected(it)
                binding.spinnerPba.selectValue(it)
            }

            binding.editTextVolumen.setText(ivc.volumenBeneficio.orZero().toString().orEmpty())
            viewModel.setVolumenChange(
                ivc.volumenBeneficio.orZero().toString().orEmpty().toEditable()
            )

            viewModel.setMedic(ivc.veterinarioPlanta)

          /*  ivc.descripcionTipoActividad?.let {
                viewModel.setBankSelected(it)
                binding.spinnerBank.selectValue(it)
            }

            ivc.descripcionProductosProcesados?.let {
                viewModel.setProductSelected(it)
                binding.spinnerProduct.selectValue(it)
            }

            ivc.descripcionPruebasLaboratorio?.let {
                viewModel.setLabSelected(it)
                binding.spinnerLab.selectValue(it)
            }*/
        }
    }


    private fun OnViewModelIvcReady(ivc: List<ModeloIvcEntity>) {
        adapterList.clear()
        val items = mutableListOf<Group>()

        if (!ivc.isNullOrEmpty()) {
            items.addAll(ivc.map { IvcItemView(it, ::onDelete) })
        }

        val isVisible = ivc.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
        adapterList.updateAsync(items)
    }

    private fun OnViewModelPbaReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerPba.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerPba.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>, view: View, position: Int, id: Long
            ) {
                val itemSelected = items[position]
                viewModel.setPbaSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelBankReady(result: List<ManageMssEntity>) {
        val listArray1: MutableList<KeyPairBoolData> = ArrayList()
        for (i in 0 until result.size) {
            val h = KeyPairBoolData()
            h.id = result.get(i).id?.toLong() ?: 0
            h.name = result.get(i).descripcion
            h.isSelected = viewModel.selectedBankList.contains(h.id.toInt())
            listArray1.add(h)
        }
        val selectedIdArray = mutableListOf<Int>()
        binding.spinnerBank.setItems(listArray1) { items ->
            for (i in items.indices) {
                if (items[i].isSelected) {
                    println(items[i].id.toInt())
                    selectedIdArray.add(items[i].id.toInt())
                   // viewModel.setBankSelected(itemSelected)
                }
            }
          viewModel.setBankSelected(selectedIdArray)
        }
    }

    private fun OnViewModelProductReady(result: List<ManageMssEntity>) {
        val listArray1: MutableList<KeyPairBoolData> = ArrayList()
        for (i in 0 until result.size) {
            val h = KeyPairBoolData()
            h.id = result.get(i).id?.toLong() ?: 0
            h.name = result.get(i).descripcion
            h.isSelected = viewModel.selectedProductList.contains(h.id.toInt())
            listArray1.add(h)
        }
        val selectedIdArray = mutableListOf<Int>()
        binding.spinnerProduct.setItems(listArray1) { items ->
            for (i in items.indices) {
                if (items[i].isSelected) {
                    println(items[i].id.toInt())
                    selectedIdArray.add(items[i].id.toInt())
                }
            }
            viewModel.setProductSelected(selectedIdArray)
        }
    }

    private fun OnViewModelLabReady(result: List<ManageMssEntity>) {
        val listArray1: MutableList<KeyPairBoolData> = ArrayList()
        for (i in 0 until result.size) {
            val h = KeyPairBoolData()
            h.id = result.get(i).id?.toLong() ?: 0
            h.name = result.get(i).descripcion
            h.isSelected = viewModel.selectedLabList.contains(h.id.toInt())
            listArray1.add(h)
        }
        val selectedIdArray = mutableListOf<Int>()
        binding.spinnerLab.setItems(listArray1) { items ->
            for (i in items.indices) {
                if (items[i].isSelected) {
                    println(items[i].id.toInt())
                    selectedIdArray.add(items[i].id.toInt())
                }
            }
            viewModel.setLabSelected(selectedIdArray)
        }
    }

    private fun onDelete(ivc: ModeloIvcEntity) {
        viewModel.onDelete(ivc.id.orZero())
    }

    private fun validateErrorAdd(
        typeId: Int,
        number: Int,
    ) {
        binding.apply {
            validateSpinner(spinnerType, typeId)
            validateSpinner(spinnerNumber, number)
        }
    }

    private fun onSuccess(@StringRes message: Int) {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}