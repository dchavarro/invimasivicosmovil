package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.HistoricMssModel
import com.soaint.data.room.tables.MssModel
import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity

data class HistoricMssResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: HistoricMssDto,
    @SerializedName("statusCode") val statusCode: Int
)

data class HistoricMssDto(
    @SerializedName("numeroExpediente") val numeroExpediente: String?,
    @SerializedName("razonSocial") val razonSocial: String?,
    @SerializedName("empresaAsociada") val empresaAsociada: String?,
    @SerializedName("direccionComercial") val direccion: String?,
    @SerializedName("nombreMunicipio") val municipio: String?,
    @SerializedName("medidasSanitarias") val medidasSanitarias: List<MssDto>? = emptyList(),
    @SerializedName("activo") val activo: Boolean? = true,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
)

fun HistoricMssDto.mapToDomain() = HistoricMssEntity(
    numeroExpediente.orEmpty(),
    razonSocial.orEmpty(),
    empresaAsociada.orEmpty(),
    direccion.orEmpty(),
    municipio.orEmpty(),
    medidasSanitarias?.map { it.mapToDomain() },
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty()
)

fun HistoricMssDto.mapToDb() = HistoricMssModel(
    numeroExpediente.orEmpty(),
    razonSocial.orEmpty(),
    empresaAsociada.orEmpty(),
    direccion.orEmpty(),
    municipio.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty()
)

data class MssDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("numeroMSS") val numeroMSS: String?,
    @SerializedName("numeroVisita") val numeroVisita: String?,
    @SerializedName("razonVisita") val razonVisita: String?,
    @SerializedName("aplicado") val aplicado: String?,
    @SerializedName("idTipoMedidaSanita") val idTipoMedidaSanita: Int?,
    @SerializedName("tipoMedidaSanita") val tipoMedidaSanita: String?,
    @SerializedName("producto") val producto: String?,
    @SerializedName("idEstadoMedidaSanita") val idEstadoMedidaSanita: Int?,
    @SerializedName("estadoMSS") val estadoMSS: String?,
    @SerializedName("fechaCierreVisita") val fechaCierreVisita: String?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("tipoProducto") val tipoProducto: String?,
)

fun MssDto.mapToDomain() = MssEntity(
    id,
    numeroMSS,
    numeroVisita,
    razonVisita,
    aplicado,
    idTipoMedidaSanita,
    tipoMedidaSanita,
    producto,
    idEstadoMedidaSanita,
    estadoMSS,
    fechaCierreVisita,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    tipoProducto.orEmpty()
)

fun MssDto.mapToDb(razonSocial: String?) = MssModel(
    id,
    numeroMSS,
    numeroVisita,
    razonVisita,
    aplicado,
    idTipoMedidaSanita,
    tipoMedidaSanita,
    producto,
    idEstadoMedidaSanita,
    estadoMSS,
    fechaCierreVisita,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    tipoProducto,
    razonSocial,
)