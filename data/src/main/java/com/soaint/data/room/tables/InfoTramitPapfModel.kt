package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.TaskPapfEntity

@Entity(tableName = "info_tramit_papf_table")
class InfoTramitPapfModel(
    @PrimaryKey val idSolicitud: Int,
    @ColumnInfo(name = "numeroSolicitud") val numeroSolicitud: Int?,
    @ColumnInfo(name = "radicado") val radicado: String?,
    @ColumnInfo(name = "idCategoriaTramite") val idCategoriaTramite: Int?,
    @ColumnInfo(name = "descripcionCategoria") val descripcionCategoria: String?,
    @ColumnInfo(name = "idTipoTramite") val idTipoTramite: Int?,
    @ColumnInfo(name = "tipoTramite") val tipoTramite: String?,
    @ColumnInfo(name = "tipoImportacion") val tipoImportacion: String?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int?,
    @ColumnInfo(name = "tipoProducto") val tipoProducto: String?,
    @ColumnInfo(name = "importador") val importador: String?,
    @ColumnInfo(name = "idOficinaInvima") val idOficinaInvima: String?,
    @ColumnInfo(name = "nombreDependencia") val nombreDependencia: String?,
    @ColumnInfo(name = "estado") val estado: Int?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "descripcionTipoInspeccion") val descripcionTipoInspeccion: String?,
    @ColumnInfo(name = "idSolicitudTramite") val idSolicitudTramite: Int?,
    @ColumnInfo(name = "codigoPapf") val codigoPapf: String?,
)

fun InfoTramitPapfModel.mapToDomain() = InfoTramitePapfEntity(
    idSolicitud,
    numeroSolicitud,
    radicado,
    idCategoriaTramite,
    descripcionCategoria,
    idTipoTramite,
    tipoTramite,
    tipoImportacion,
    idTipoProducto,
    tipoProducto,
    importador,
    idOficinaInvima,
    nombreDependencia,
    estado,
    direccion,
    telefono,
    numeroDocumento,
    tipoDocumento,
    descripcionTipoInspeccion,
    idSolicitudTramite,
    codigoPapf,
)