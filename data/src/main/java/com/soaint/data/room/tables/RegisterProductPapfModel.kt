package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.DestinoLotePapfEntity
import com.soaint.domain.model.DocumentPapfEntity
import com.soaint.domain.model.RegisterProductPapfEntity

@Entity(tableName = "register_product_papf_table")
class RegisterProductPapfModel(
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @ColumnInfo(name = "clasificacionProducto") val clasificacionProducto: String?,
    @ColumnInfo(name = "producto") val producto: String?,
    @ColumnInfo(name = "registroSanitario") val registroSanitario: String?,
    @ColumnInfo(name = "idEmpresaFabricante") val idEmpresaFabricante: Int?,
    @ColumnInfo(name = "origenFabricante") val origenFabricante: String?,
    @ColumnInfo(name = "idDetalleProducto") val idDetalleProducto: Int?,
    @ColumnInfo(name = "lote") val lote: String?,
    @ColumnInfo(name = "cantidad") val cantidad: String?,
    @ColumnInfo(name = "presentacionComercial") val presentacionComercial: String?,
    @ColumnInfo(name = "pesoUnidadMedida") val pesoUnidadMedida: Double?,
    @ColumnInfo(name = "unidadDeMedida") val unidadDeMedida: String?,
    @ColumnInfo(name = "pesoNeto") val pesoNeto: String?,
    @ColumnInfo(name = "marca") val marca: String?,
    @ColumnInfo(name = "temperaturaConservacion") val temperaturaConservacion: String?,
    @ColumnInfo(name = "fechaVencimiento") val fechaVencimiento: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun RegisterProductPapfModel.mapToDomain() = RegisterProductPapfEntity(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto,
    producto.orEmpty(),
    registroSanitario.orEmpty(),
    idEmpresaFabricante.orEmpty(),
    origenFabricante.orEmpty(),
    idDetalleProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    presentacionComercial.orEmpty(),
    pesoUnidadMedida.orEmpty(),
    unidadDeMedida.orEmpty(),
    pesoNeto.orEmpty(),
    marca.orEmpty(),
    temperaturaConservacion.orEmpty(),
    fechaVencimiento.orEmpty(),
)

@Entity(tableName = "register_product_destino_papf_table")
class DestinoLotePapfModel(
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "idSedeEmpresaDestino") val idSedeEmpresaDestino: Int?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "sucursal") val sucursal: String?,
    @ColumnInfo(name = "departamento") val departamento: String?,
    @ColumnInfo(name = "municipio") val municipio: String?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "contacto") val contacto: String?,
    @ColumnInfo(name = "idDestinoProducto") val idDestinoProducto: Int?,
    @ColumnInfo(name = "descripcionDestinoProducto") val descripcionDestinoProducto: String?,
    @ColumnInfo(name = "lote") val lote: String?,
    @ColumnInfo(name = "cantidad") val cantidad: Int?,
    @ColumnInfo(name = "idMPIG") val idMPIG: Int?,
    @ColumnInfo(name = "descripcionUsoMPIG") val descripcionUsoMPIG: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DestinoLotePapfModel.mapToDomain() = DestinoLotePapfEntity(
    numeroDocumento.orEmpty(),
    idSedeEmpresaDestino.orEmpty(),
    razonSocial.orEmpty(),
    sucursal.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    contacto.orEmpty(),
    idDestinoProducto.orEmpty(),
    descripcionDestinoProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    idMPIG.orEmpty(),
    descripcionUsoMPIG.orEmpty(),
)

@Entity(tableName = "register_product_document_papf_table")
class DocumentPapfModel(
    @ColumnInfo(name = "idDocumento") val idDocumento: Int?,
    @ColumnInfo(name = "nombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DocumentPapfModel.mapToDomain() = DocumentPapfEntity(
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty(),
)