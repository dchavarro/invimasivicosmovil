package com.soaint.domain.use_case

import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.TechnicalEntity
import com.soaint.domain.repository.TechnicalRepository
import javax.inject.Inject

class TechnicalUseCase @Inject constructor(
    private val technicalRepository: TechnicalRepository
) {

    val technical get() = technicalRepository.technical

    suspend fun getTechnicals(idVisita: Int) = technicalRepository.getTechnicals(idVisita)

    suspend fun getTechnicalsLocal(idVisita: Int) = technicalRepository.getTechnicalsLocal(idVisita)

    suspend fun updateTechnicalLocal(technical: TechnicalEntity) = technicalRepository.updateTechnicalLocal(technical)

    suspend fun updateTechnical() = technicalRepository.updateTechnical()

    suspend fun getCertificate(nitEmpresa: String) = technicalRepository.getCertificate(nitEmpresa)

    suspend fun getCertificateLocal(nitEmpresa: String) = technicalRepository.getCertificateLocal(nitEmpresa)

    suspend fun updateCertificate() = technicalRepository.updateCertificate()

    suspend fun updateCertificateLocal(body: CertificateEntity) = technicalRepository.updateCertificateLocal(body)

}