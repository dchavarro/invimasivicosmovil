package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.model.SaveInfoActsPapfEntity
import com.soaint.domain.model.TaskEntity

data class SaveInfoActsPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaSaveInfoActsPapfDto,
    @SerializedName("Parametros") val parametros: ParametrosSaveInfoActsPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosSaveInfoActsPapfDto) : this(
        AuditoriaSaveInfoActsPapfDto(ip, user),
        parametros
    )
}
data class AuditoriaSaveInfoActsPapfDto(
    @SerializedName("IP") val iP: String,
    @SerializedName("Usuario") val usuario: String
)

data class SaveInfoActsPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("statusCode") val statusCode: Int,
)

data class ParametrosSaveInfoActsPapfDto(
    @SerializedName("idPlantilla") val idPlantilla: Int?,
    @SerializedName("idSolicitud") val idSolicitud: String? = EMPTY_STRING,
    @SerializedName("expedicionCIS") val expedicionCIS: String? = EMPTY_STRING,
    @SerializedName("otros") val otros: String? = EMPTY_STRING,
    @SerializedName("cual") val cual: String? = EMPTY_STRING,
    @SerializedName("consecutivo") val consecutivo: String? = EMPTY_STRING,
    @SerializedName("fechaRegistro") val fechaRegistro: String? = EMPTY_STRING,
    @SerializedName("funcionarioInvima") val funcionarioInvima: String? = EMPTY_STRING,
    @SerializedName("fechaInicio") val fechaInicio: String? = EMPTY_STRING,
    @SerializedName("fechaFin") val fechaFin: String? = EMPTY_STRING,
    @SerializedName("tramiteRequerimiento") val tramiteRequerimiento: Int?,
    @SerializedName("descripcionRequerimiento") val descripcionRequerimiento: String? = EMPTY_STRING,
    @SerializedName("nombreUsuario") val nombreUsuario: String? = EMPTY_STRING,
    @SerializedName("nombreLaboratorio") val nombreLaboratorio: String? = EMPTY_STRING
)


fun SaveInfoActsPapfEntity.mapToData() = ParametrosSaveInfoActsPapfDto(
    idPlantilla,
    idSolicitud,
    expedicionCIS,
    otros,
    cual,
    consecutivo,
    fechaRegistro,
    funcionarioInvima,
    fechaInicio,
    fechaFin,
    tramiteRequerimiento,
    descripcionRequerimiento,
    nombreUsuario,
    nombreLaboratorio,
)
