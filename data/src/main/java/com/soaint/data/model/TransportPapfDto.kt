package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.TransportComPapfModel
import com.soaint.data.room.tables.TransportDocPapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.TransportComPapfEntity
import com.soaint.domain.model.TransportDocPapfEntity
import com.soaint.domain.model.TransportObjectPapfEntity

data class TransportPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: TransportObjectPapfDto?,
    @SerializedName("statusCode") val statusCode: Int,
)
data class TransportObjectPapfDto(
    @SerializedName("empresa") val empresa: List<TransportComPapfDto>?,
    @SerializedName("documentos") val documentos: List<TransportDocPapfDto>?,
)

fun TransportObjectPapfDto.mapToDomain() = TransportObjectPapfEntity (
    empresa?.map { it.mapToDomain() }.orEmpty(),
    documentos?.map { it.mapToDomain() }.orEmpty(),
)

data class TransportComPapfDto(
    @SerializedName("NombreEmpresa") val nombreEmpresa: String?,
    @SerializedName("NumeroDocumento") val numeroDocumento: String?,
)

fun TransportComPapfDto.mapToDomain() = TransportComPapfEntity (
    nombreEmpresa.orEmpty(),
    numeroDocumento.orEmpty(),
)

fun TransportComPapfDto.mapToDb(idSolicitud: Int?) = TransportComPapfModel(
    nombreEmpresa.orEmpty(),
    numeroDocumento.orEmpty(),
    idSolicitud.orEmpty()
)

data class TransportDocPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("TipoDocumento") val tipoDocumento: String?,
    @SerializedName("NumeroDocumento") val numeroDocumento: String?,
    @SerializedName("IdDocumento") val idDocumento: Int? = 0,
    @SerializedName("NombreDocumento") val nombreDocumento: String?,
    @SerializedName("IdentificacionTipoTransporte") val identificacionTipoTransporte: String?,
    @SerializedName("Contenedor") val contenedor: String?,
)

fun TransportDocPapfDto.mapToDomain() = TransportDocPapfEntity(
    idSolicitud,
    tipoDocumento.orEmpty(),
    numeroDocumento.orEmpty(),
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty(),
    identificacionTipoTransporte.orEmpty(),
    contenedor.orEmpty(),
)

fun TransportDocPapfDto.mapToDb() = TransportDocPapfModel(
    idSolicitud.orEmpty(),
    tipoDocumento.orEmpty(),
    numeroDocumento.orEmpty(),
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty(),
    identificacionTipoTransporte.orEmpty(),
    contenedor.orEmpty(),
)