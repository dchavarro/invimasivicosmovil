package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TypeHoursBillingEntity(
    val id: Int,
    val codigo: String,
    val descripcion: String,
    val activo: Boolean,
    val fechaCreacion: String?,
    val usuarioCrea: String?,
    val fechaModifica: String?,
    val usuarioModifica: String?
) : Parcelable