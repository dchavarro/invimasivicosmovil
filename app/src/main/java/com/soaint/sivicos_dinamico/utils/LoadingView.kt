package com.soaint.sivicos_dinamico.utils

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.RelativeLayout
import androidx.annotation.AttrRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.soaint.data.common.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ViewLoadingBinding

class LoadingView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    @AttrRes defStyleAttr: Int = 0
) : RelativeLayout(context, attrs, defStyleAttr) {

    private val binding = ViewLoadingBinding.inflate(context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater)

    init {
        val layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT)
        layoutParams.addRule(CENTER_IN_PARENT, TRUE)
        addView(binding.root, layoutParams)
        setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhiteTransparent))
        isClickable = true
    }

    fun setState(state: Boolean, @StringRes text: Int) {
        if (state) {
            showProgressBar(text)
        } else {
            hideProgressBar()
        }
    }

    fun showProgressBar(@StringRes text: Int) {
        this.isVisible = true
        binding.textView.text = context.getString(text)
        binding.root.isVisible = true
    }

    fun hideProgressBar() {
        this.isVisible = false
        binding.textView.text = EMPTY_STRING
        binding.root.isVisible = false
    }
}