package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.content.DialogInterface
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface
import com.soaint.sivicos_dinamico.utils.ShowAlert

interface OnObservacionesEvent {
    fun onFotoRequested(control: ControlInterface)
    fun onMaxFotosReached()
}

class Foto : ControlInterface {

    lateinit var contFotos: LinearLayout
    var listener: OnObservacionesEvent? = null
    lateinit var inflater: LayoutInflater
    var arrayRutasFotos: ArrayList<String> = arrayListOf()
    lateinit var btnTomarFoto: ImageView
    lateinit var classContext: Context
    var isVisible: Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context
        inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_foto, null)
        val lblHint = view.findViewById<TextView>(R.id.lblTitulo)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        btnTomarFoto = view.findViewById<ImageView>(R.id.btnFoto)
        lblHint.text = control.descripcion
        contFotos = view.findViewById(R.id.contFotos)

        if (valorAlmacenado.isNotEmpty()) {
            val string = valorAlmacenado.replace("[", "").replace("]", "")
            val arrayAlmacenado: List<String> = listOf(*string.split(", ").toTypedArray())

            if (arrayAlmacenado.size > 0) {
                for (ruta in arrayAlmacenado) {
                    if (ruta.isNotEmpty()) addFoto(ruta, context)
                }
            }
        }

        btnTomarFoto.setOnSingleClickListener {
            if (contFotos.childCount < 3) {
                if (listener != null) {
                    listener!!.onFotoRequested(this)
                }
            } else {
                if (listener != null) {
                    listener!!.onMaxFotosReached()
                }
            }
        }
        btnTomarFoto.isEnabled =
            isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)
        return view
    }

    override fun getData(): String {
        return arrayRutasFotos.toString()
    }

    override fun showError(mensaje: String?) {
        btnTomarFoto.backgroundTintList = classContext.resources.getColorStateList(R.color.colorRed)
    }

    override fun clearError() {
        btnTomarFoto.backgroundTintList =
            classContext.resources.getColorStateList(R.color.colorPrimary)
    }

    fun addFoto(ruta: String, context: Context) {
        arrayRutasFotos.add(ruta)
        val viewFoto = inflater.inflate(R.layout.layout_image_preview, null)
        val imgView = viewFoto.findViewById<ImageView>(R.id.imgFoto)
        val btnDeleteFoto = viewFoto.findViewById<FrameLayout>(R.id.btnDeleteFoto)
        btnDeleteFoto.setOnClickListener {
            ShowAlert(context,
                context.getString(R.string.confirmar),
                context.getString(R.string.confirmar_borrar_foto),
                context.getString(R.string.si),
                context.getString(R.string.no),
                object : ShowAlert.OnClickAlert {
                    override fun OnClickBotonIzq(dialog: DialogInterface) {
                        arrayRutasFotos.remove(ruta)
                        contFotos.removeView(viewFoto)
                        dialog.dismiss()
                    }

                    override fun OnClickBotonDer(dialog: DialogInterface) {
                        dialog.dismiss()
                    }
                })
        }

        var bmp = BitmapFactory.decodeFile(ruta)
        if (bmp != null) {
            if (bmp.getWidth() > bmp.getHeight()) {
                val matrix = Matrix()
                matrix.postRotate(90F)
                bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true)
            }
            imgView!!.setImageBitmap(bmp)
            contFotos.addView(viewFoto)
        } else {
            ShowAlert(context,
                context.getString(R.string.error),
                context.getString(R.string.mensaje_foto_error),
                context.getString(R.string.cerrar),
                null,
                object : ShowAlert.OnClickAlert {
                    override fun OnClickBotonIzq(dialog: DialogInterface) {
                        dialog.dismiss()
                    }

                    override fun OnClickBotonDer(dialog: DialogInterface?) {}
                })
        }
    }
}