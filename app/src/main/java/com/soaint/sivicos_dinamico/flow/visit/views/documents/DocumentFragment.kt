package com.soaint.sivicos_dinamico.flow.visit.views.documents

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.CHOOSE_FILE_REQUEST
import com.soaint.data.common.RESULT_VISIT_ID
import com.soaint.data.common.orZero
import com.soaint.domain.common.getFileFromBase64AndSave
import com.soaint.domain.common.openFile
import com.soaint.domain.model.*
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentDocumentBinding
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.act.ActActivity
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.DocumentItemView
import com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument.AddDocumentActivity
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.GroupieAdapter


class DocumentFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentDocumentBinding>(R.layout.fragment_document)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: DocumentViewModel by viewModels { viewModelFactory }

    private val args by navArgs<DocumentFragmentArgs>()

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = initView()

    private fun initView(): View {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.documentos_visita))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(DocumentFragmentDirections.toInfo())
        }
        observerViewModelEvents()
        return binding.root
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        if (args.idVisita.isNullOrEmpty()) {
            setHasOptionsMenu(true)
            (activity as AppCompatActivity?)!!.supportActionBar!!.show()
            mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        } else {
            viewModel.onloadDocument(args.idVisita.toString())
        }
    }

    private fun eventHandler(event: DocumentViewModelEvent) {
        showLoading(false)
        when (event) {
            is DocumentViewModelEvent.ChangeLabelPreloader -> showLoading(true, event.label)
            is DocumentViewModelEvent.LoadingError -> onTokenError(event.error)
            is DocumentViewModelEvent.ViewModelReady -> onViewModelReady(event.document)
            is DocumentViewModelEvent.ViewModelBlockReady -> OnBlockReady(
                event.visit,
                event.plantilla,
                event.block
            )
            is DocumentViewModelEvent.ViewModelViewLocal -> viewModelViewLocal(
                event.documentBodyEntity,
                event.pdfAct
            )
            is DocumentViewModelEvent.ViewModelViewApi -> openBase64(
                event.documentBase64,
                event.nameDocument
            )
            is DocumentViewModelEvent.ViewModelDelete -> onViewModelDelete(
                event.visitId,
                event.plantillaId
            )
            is DocumentViewModelEvent.ViewModelSnackBar -> showSnackBar(event.isSuccess, event.message)

        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun viewModelViewLocal(documentBody: DocumentBodyEntity?, pdf: String?) {
        if (pdf.isNullOrEmpty()) {
            documentBody?.let {
                openFile(requireContext(), it.archivoBase64.orEmpty(), BuildConfig.APPLICATION_ID)
            }
        } else {
            openBase64(pdf, documentBody?.nombreDocumento)
        }
    }

    private fun openBase64(documentBase64: String?, nameDocument: String?) {
        val path = documentBase64?.let {
            getFileFromBase64AndSave(
                requireContext(),
                it,
                nameDocument.orEmpty(),
                "pdf"
            )
        }
        openFile(requireContext(), path.orEmpty(), BuildConfig.APPLICATION_ID)
    }

    private fun onViewModelDelete(visitId: String, plantillaId: String) {
        viewModel.deleteActaLocal(visitId, plantillaId)
    }

    private fun onViewModelReady(document: List<DocumentEntity>) {
        alertDialog?.dismiss()
        adapter.clear()
        binding.rv.isVisible = !document.isNullOrEmpty()
        binding.textViewNotFound.isVisible = document.isNullOrEmpty()
        adapter.updateAsync(document.sortedByDescending { it.fechaDocumento }.map {
            DocumentItemView(it, ::onDocumentSelected, ::onView, ::onDelete, ::OnEdit)
        }
        )
    }

    private fun OnBlockReady(
        visit: VisitEntity?,
        plantilla: List<PlantillaEntity>,
        blocks: List<BlockEntity>
    ) {
        val intent = Intent(context, ActActivity::class.java)
        val bundle = Bundle().apply {
            putInt("idVisita", visit?.id.orZero())
            putParcelableArrayList("plantilla", ArrayList(plantilla))
            putParcelableArrayList("bloques", ArrayList(blocks))
            putInt("idTipoDocumental", plantilla.firstOrNull()?.idTipoDocumental.orZero())
        }
        intent.putExtras(bundle)
        startActivity(intent)
    }

    private fun onDocumentSelected(document: DocumentEntity) {}

    private fun OnEdit(idPlantilla: String) {
        viewModel.onEdit(idPlantilla)
    }

    private fun onView(document: DocumentEntity) {
        if (document.idDocumentBody != null) {
            viewModel.onViewLocal(document.idDocumentBody!!)
        } else if (document.idPlantilla != null) {
            viewModel.onViewActLocal(document.idPlantilla!!)
        } else if (document.idDocumento != 0 || document.idDocumento != null) {
            viewModel.onViewApi(document.idDocumento!!, document.nombreDocumento.orEmpty())
        } else {
            showSnackBar(false, R.string.copy_document_error)
        }
    }

    private fun onDelete(document: DocumentEntity) {
        showAlertDialog(
            showDialog(
                getString(R.string.adv),
                getString(R.string.copy_document_delete_message),
                titleBtnPositive = getString(R.string.confirmar),
                titleBtnNegative = getString(R.string.cerrar)
            )
            { viewModel.onDelete(document) }
        )
    }

    fun showSnackBar(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_document, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_add) {
            viewModel.visitData.value?.let {
                startActivity(AddDocumentActivity.getStartActivityEvent(it))
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onResume() {
        super.onResume()
        alertDialog?.dismiss()
        mainViewModel.visitData.value?.let { viewModel.init(it) }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                CHOOSE_FILE_REQUEST -> {
                    data?.getStringExtra(RESULT_VISIT_ID).orEmpty()
                        .let { viewModel.onloadDocument(it) }
                }
            }
        }
    }
}