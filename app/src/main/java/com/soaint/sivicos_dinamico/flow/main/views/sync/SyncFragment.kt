package com.soaint.sivicos_dinamico.flow.main.views.sync

import android.os.Bundle
import android.view.*
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.model.UserInformationEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentSyncBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.stopLocationService
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.splash.SplashActivity
import com.soaint.sivicos_dinamico.properties.fragmentBinding

class SyncFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentSyncBinding>(R.layout.fragment_sync)

    private val viewModel: SyncViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        setHasOptionsMenu(true)
        (activity as AppCompatActivity?)!!.supportActionBar!!.show()
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        (requireActivity() as MainActivity).supportActionBar?.title = getString(R.string.app_name)
        binding.btnDiligenciar.setOnSingleClickListener {
            findNavController().navigate(R.id.action_sincronizacionFragment_to_tareasAsignadasFragment)
        }
        binding.btnSincronizar.setOnSingleClickListener {
            viewModel.onSync()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.user.observe(viewLifecycleOwner, ::loadUserData)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        viewModel.syncDate.observe(viewLifecycleOwner, ::syncDate)
        viewModel.count.observe(viewLifecycleOwner, ::countQuerys)
        viewModel.init()
        viewModel.isEnableTask.observe(viewLifecycleOwner, ::isEnableTask)
    }

    private fun countQuerys(count: Int) {
        if (count > 0) requireActivity().showSnackBar(count) { viewModel.onSync() }
    }

    private fun syncDate(syncDate: String?) {
        val isSyncDate = !(syncDate == null || syncDate == EMPTY_STRING)
        binding.btnDiligenciar.isEnabled = isSyncDate
        binding.lblFechaSincronizacion.text =
            if (isSyncDate) getString(
                R.string.copy_sync_date,
                syncDate
            )
            else getString(R.string.copy_not_sync_date)
    }

    private fun loadUserData(fullName: String?) {
        binding.lblNombre.text = "${fullName.orEmpty()}"
    }

    private fun eventHandler(event: OnSyncViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnSyncViewModelEvent.OnChangeLabelPreloader -> onChangeLabelPreloader(event.message)
            is OnSyncViewModelEvent.OnSyncError -> onTokenError(event.error)
            is OnSyncViewModelEvent.OnViewModelReady -> onViewModelReady()
            else -> {}
        }
    }

    private fun onChangeLabelPreloader(@StringRes message: Int) {
        showLoading(true, message)
        binding.btnSincronizar.isEnabled = false
    }

    private fun onTokenError(error: Throwable?) {
        binding.btnSincronizar.isEnabled = true
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReady() {
        binding.btnSincronizar.isEnabled = true
    }

    private fun isEnableTask(isEnable: Boolean) {
        binding.btnDiligenciar.isEnabled = isEnable
    }

    private fun logout() {
        viewModel.logout()
        requireContext().stopLocationService()
        startClearStackActivity(requireActivity(), SplashActivity::class.java)
        requireActivity().finish()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu_main, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_logout) {
            showAlertDialog(
                showDialog(
                    getString(R.string.cerrar_sesion),
                    getString(R.string.copy_logout),
                    getString(R.string.si),
                    getString(R.string.no),
                ) { logout() }
            )
        }
        return super.onOptionsItemSelected(item)
    }
}