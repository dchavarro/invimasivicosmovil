package com.soaint.sivicos_dinamico.flow.visit.views.transport

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.core.text.buildSpannedString
import androidx.core.text.underline
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.SPACE_STRING
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentTransportBinding
import com.soaint.sivicos_dinamico.extensions.color
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.OfficialItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter
import java.text.DecimalFormat
import java.util.*

class TransportFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentTransportBinding>(R.layout.fragment_transport)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: TransportViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.transporte_visita))

        binding.recyclerViewOfficial.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.recyclerViewOfficial.adapter = adapter

        binding.editTextDate.setOnSingleClickListener { setDate() }
        binding.textInputDate.setOnSingleClickListener { setDate() }
        binding.buttonSend.setOnSingleClickListener { viewModel.checkData() }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(TransportFragmentDirections.toInfo())
        }

        // Validate Email separated by comma
        /*binding.editTextEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {}
            override fun afterTextChanged(editable: Editable) {
                binding.textInputEmail.isErrorEnabled = false
                binding.textInputEmail.error = null
                val email: List<String> = binding.editTextEmail.text.toString()
                    .trim()
                    .replace(SPACE_STRING, EMPTY_STRING)
                    .split(",")
                for (i in email.indices) {
                    if (!(email[i].isValidEmail())) {
                        binding.textInputEmail.isErrorEnabled = true
                        binding.textInputEmail.error = getString(R.string.mensaje_email_no_valido)
                        break
                    }
                }
            }
        })*/
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnTransportViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnTransportViewModelEvent.OnViewModelSuccess -> onSuccess(event.isSuccess, event.message)
            is OnTransportViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnTransportViewModelEvent.OnLoadingError -> onTokenError(event.error, event.isEmpty)
            is OnTransportViewModelEvent.OnViewModelReady -> onViewModelReady(event.transport, event.official, event.decomiso)
            is OnTransportViewModelEvent.OnValidateData -> validateErrorData(
                event.mms,
                event.size,
                event.parts,
                event.volumen,
                event.peso,
                event.merca,
                event.aux,
                event.value,
                event.date,
                //event.email
            )
        }
    }

    private fun validateErrorData(
        mms: String,
        size:String,
        parts:String,
        volumen:String,
        peso:String,
        merca:String,
        aux:String,
        value:String,
        date:String,
    ) {
        binding.apply {
            validateField(textInputMms, mms)
            validateField(textInputSize, size)
            validateField(textInputParts, parts)
            validateField(textInputVolumen, volumen)
            validateField(textInputPeso, peso)
            validateField(textInputMerca, merca)
            validateField(textInputAux, aux)
            validateField(textInputValue, value)
            validateField(textInputDate, date)
        }
    }

    private fun onSuccess(isSuccess: Boolean, @StringRes message: Int) {
        showSnack(isSuccess, message)
        requireActivity().onBackPressed()
    }

    private fun onTokenError(error: Throwable?, isEmpty: Boolean) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); if (isEmpty) requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReady(transport: List<TransportEntity>, official: List<List<OfficialEntity>>, decomiso: List<TransportBodyEntity>) {
        adapter.clear()
        binding.scrollView.isVisible = !transport.isNullOrEmpty()
        binding.textViewNotFound.isVisible = transport.isNullOrEmpty()
        val items = mutableListOf<Group>()
        if (!transport.isNullOrEmpty()) {
            if (!official.first().isNullOrEmpty()) {
                items.addAll(official.first().map { OfficialItemView(it, ::onSelected) })
            }
            adapter.updateAsync(items)

            // Text Direccion Misional
            binding.textViewMisional.text = buildSpannedString {
                append(getString(R.string.copy_misional_la_transport))
                color(requireContext(), R.color.colorGray) {
                    append(SPACE_STRING)
                    underline { append(transport.firstOrNull()?.direccionMisional) }
                    append(SPACE_STRING)
                }
                append(getString(R.string.copy_misional_transport))
            }
        }

        if (!decomiso.isNullOrEmpty()) {
            binding.editTextDate.setText(decomiso.firstOrNull()?.fechaRecogida?.formatDateHour())
            viewModel.onDateChange(decomiso.firstOrNull()?.fechaRecogida?.toEditable())
        }
    }

    private fun setDate() {
        val c: Calendar = Calendar.getInstance()
        val yearCalendar = c.get(Calendar.YEAR)
        val dayMonth = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val timePickerDialog = DatePickerDialog(requireContext(), { _, yearResponse, month, day_month ->
            val monthSelected = DecimalFormat("00").format(month + 1).toString()
            val daySelected = DecimalFormat("00").format(day_month).toString()
            setHour("$yearResponse-$monthSelected-$daySelected")
        }, yearCalendar, month, dayMonth)
        timePickerDialog.setCancelable(false)
        timePickerDialog.show()
    }

    fun setHour(fechaString: String) {
        val c: Calendar = Calendar.getInstance()
        val mHour = c.get(Calendar.HOUR_OF_DAY)
        val mMinute = c.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(requireContext(), { _, hourOfDay, minute ->
            val hourSelected = DecimalFormat("00").format(hourOfDay).toString()
            val minuteSelected = DecimalFormat("00").format(minute).toString()
            val dateString = "$fechaString" + "T" +"$hourSelected:$minuteSelected:00"
            viewModel.onDateChange(dateString.toString().toEditable())
            binding.textInputDate.editText?.text = dateString.formatDateHour().toEditable()
        },
            mHour,
            mMinute,
            true
        )
        timePickerDialog.setCancelable(false)
        timePickerDialog.show()
    }

    private fun onSelected(official: OfficialEntity) {}
}