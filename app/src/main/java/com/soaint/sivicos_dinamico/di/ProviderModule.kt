package com.soaint.sivicos_dinamico.di

import com.soaint.domain.provider.FileProvider
import com.soaint.domain.provider.LocationProvider
import com.soaint.domain.provider.NetworkProvider
import com.soaint.sivicos_dinamico.manager.FileProviderImpl
import com.soaint.sivicos_dinamico.manager.LocationProviderImpl
import com.soaint.sivicos_dinamico.manager.NetworkProviderImpl
import dagger.Binds
import dagger.Module

@Module
abstract class ProviderModule {

    @Binds
    abstract fun networkProviderProvider(
        impl: NetworkProviderImpl
    ): NetworkProvider

    @Binds
    abstract fun networkLocationProvider(
        impl: LocationProviderImpl
    ): LocationProvider

    @Binds
    abstract fun fileProviderProvider(
        impl: FileProviderImpl
    ): FileProvider
}