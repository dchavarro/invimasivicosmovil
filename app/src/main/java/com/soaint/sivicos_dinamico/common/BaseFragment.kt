package com.soaint.sivicos_dinamico.common

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.papf.dialogs.act_sample.DataReqActSample
import com.soaint.sivicos_dinamico.flow.papf.dialogs.inspection_sanitary.DataReqInspSanitary
import com.soaint.sivicos_dinamico.flow.visit.views.address.DialogAddress
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.personal.DialogPersonal
import com.soaint.sivicos_dinamico.flow.web.WebViewActivity
import com.soaint.sivicos_dinamico.model.*
import com.soaint.sivicos_dinamico.utils.KotlinUtils
import com.soaint.sivicos_dinamico.utils.LoadingView
import dagger.android.support.DaggerFragment
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import javax.inject.Inject


abstract class BaseFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected val utils = KotlinUtils()

    protected var alertDialog: AlertDialog? = null

    private var loadingBar: LoadingView? = null

    open fun getName(): String? = javaClass.name

    open fun getEnter(): Int = 0

    open fun getExit(): Int = 0

    open fun getPopEnter(): Int = 0

    open fun getPopExit(): Int = 0

    protected fun subscribeViewModel(viewModel: BaseViewModel, view: View) {
        viewModel.navigationEvent.observe(viewLifecycleOwner, Observer(::navigateFragment))
    }

    /**
     * Actions
     */

    protected fun startActivity(activityClass: Class<*>, bundle: Bundle? = null) {
        val intent = Intent(context, activityClass)
        bundle?.let { intent.extras?.putAll(it) }
        startActivity(intent)
        activity?.finish()
    }

    protected fun startActivity(startActivityEvent: StartActivityEvent) {
        val intent = Intent(context, startActivityEvent.activity).apply {
            startActivityEvent.bundle?.let { putExtras(it) }
            flags = startActivityEvent.flag
        }
        startActivityForResult(intent, startActivityEvent.code)
    }

    protected fun startClearStackActivity(
        currentActivity: Activity,
        newTopActivityClass: Class<out Activity?>?
    ) {
        val intent = Intent(currentActivity, newTopActivityClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        currentActivity.startActivity(intent)
    }

    open fun close() {
        requireActivity().supportFragmentManager.beginTransaction().remove(this).commit()
        requireActivity().supportFragmentManager.popBackStack()
    }

    protected open fun initWebView(title: String, url: String) {
        startActivity(WebViewActivity.getStartActivityEvent(url, title))
    }

    protected fun navigateFragment(navigation: NavigationEvents) {
        when (navigation) {
            is StartActivityEvent -> startActivity(navigation)
            is FinishResultEvent -> finishActivity(navigation)
            is NavigationToDirectionEvent -> findNavController().navigate(navigation.navDirections)
            is NavigationToActionIdEvent -> findNavController().navigate(
                navigation.actionId,
                navigation.arguments
            )
            is NavigationUpEvent -> findNavController().navigateUp()
            is PopBackStackEvent -> findNavController().popBackStack()
            is OnBackPressedEvent -> onBackPressed(navigation)
            is AddFragmentEvent -> addFragment(navigation)
            is ReplaceFragmentEvent -> replaceFragment(navigation)
        }
    }

    protected fun finishActivity(finishResultEvent: FinishResultEvent) {
        activity?.let { activity ->
            with(finishResultEvent) {
                bundle?.let { activity.setResult(resultCode, Intent().apply { putExtras(it) }) }
                    ?: activity.setResult(resultCode)
            }
            activity.finish()
        }
    }

    protected fun onBackPressed(onBackPressedEvent: OnBackPressedEvent) {
        requireActivity().setResult(onBackPressedEvent.resultCode)
        requireActivity().onBackPressed()
    }

    /**
     * Fragments
     */

    protected fun replaceFragment(
        event: ReplaceFragmentEvent
    ) {
        replaceFragment(event.fragment, event.bundle)
    }

    protected fun replaceFragment(
        fragment: BaseFragment,
        bundle: Bundle? = null,
        @IdRes container: Int = R.id.fragment_container
    ) {
        bundle?.let { fragment.arguments = it }
        childFragmentManager.beginTransaction()
            .replace(container, fragment, fragment.tag)
            .commit()
    }

    protected open fun replaceFragment(
        fragment: BaseFragment,
        container: Int
    ) {
        val supportFragmentManager = requireActivity().supportFragmentManager
        val ft = childFragmentManager.beginTransaction()
        if (supportFragmentManager.backStackEntryCount >= 0) {
            ft.setCustomAnimations(
                fragment.getEnter(),
                fragment.getExit(),
                fragment.getPopEnter(),
                fragment.getPopExit()
            )
        }
        ft.replace(container, fragment, fragment.getName())
        ft.commit()
    }

    protected fun addFragment(
        event: AddFragmentEvent
    ) {
        addFragment(event.fragment, event.bundle, event.addToStack)
    }

    protected fun addFragment(
        fragment: BaseFragment,
        bundle: Bundle? = null,
        addToStack: Boolean = true,
        @IdRes container: Int = R.id.fragment_container
    ) {
        bundle?.let { fragment.arguments = it }
        val fragmentTransition = childFragmentManager.beginTransaction()
            .add(container, fragment, fragment.tag)
        if (addToStack) {
            fragmentTransition.addToBackStack(fragment.tag)
        }
        fragmentTransition.commit()
    }

    protected fun showListDialog(
        @StringRes title: Int,
        items: Array<String>,
        action: (position: Int) -> Unit
    ) {
        val builder = AlertDialog.Builder(requireContext()).apply {
            setTitle(title)
            setItems(items) { _, position -> action(position) }
            setCancelable(false)
        }
        showAlertDialog(builder)
    }

    protected fun showAlertDialog(alertDialogBuilder: AlertDialog.Builder) {
        if (activity?.isFinishing == false) {
            alertDialog = alertDialogBuilder.create()
            alertDialog!!.show()
        }
    }

    protected fun dismissAlertDialog() {
        if (activity?.isFinishing == false) {
            alertDialog?.dismiss()
        }
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

    protected fun showDialog(
        title: String,
        message: String,
        titleBtnPositive: String = EMPTY_STRING,
        titleBtnNegative: String = EMPTY_STRING,
        action: () -> Unit,
    ) =
        AlertDialog.Builder(requireContext())
            .setMessage(message)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(titleBtnPositive) { _, _ ->
                action.invoke()
            }
            .setNegativeButton(titleBtnNegative) { _, _ ->
                dismissAlertDialog()
            }

    protected fun showDialogPersonal() {
        if (activity?.isFinishing == false) {
            val dialogFragment = DialogPersonal.getInstance()
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

    protected fun showDialogAddress() {
        if (activity?.isFinishing == false) {
            val dialogFragment = DialogAddress.getInstance()
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

    protected fun showDialogDataReqInsSanitary(idDetalleProducto: Int, idSolicitud: Int) {
        if (activity?.isFinishing == false) {
            val dialogFragment = DataReqInspSanitary.getInstance(idDetalleProducto, idSolicitud)
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

    protected fun showDialogDataReqActSample(idProducto: Int, idSolicitud: Int) {
        if (activity?.isFinishing == false) {
            val dialogFragment = DataReqActSample.getInstance(idProducto, idSolicitud)
            dialogFragment.show(requireActivity().supportFragmentManager, null)
        }
    }

    protected fun showToolTips(
        view: View,
        text: String
    ) {
        SimpleTooltip.Builder(requireContext())
            .anchorView(view)
            .text(text)
            .gravity(Gravity.END)
            .animated(true)
            .transparentOverlay(false)
            .build()
            .show()
    }

    /**
     * Loading
     */

    private fun initLoading() {
        removeLoading()
        if (loadingBar == null) {
            loadingBar = LoadingView(requireContext())
            requireActivity().addContentView(
                loadingBar,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
        }
    }

    protected open fun showLoading(showing: Boolean, @StringRes text: Int = R.string.cargando) {
        initLoading()
        loadingBar?.setState(showing, text)
    }

    private fun removeLoading() {
        if (loadingBar != null) {
            val viewParent = loadingBar?.parent
            if (viewParent != null && viewParent is ViewManager) {
                (viewParent as ViewManager).removeView(loadingBar)
            }
        }
        loadingBar = null
    }

    override fun onDetach() {
        removeLoading()
        super.onDetach()
    }
}