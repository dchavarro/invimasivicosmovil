package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.room.tables.*
import com.soaint.domain.model.BlockBodyValueEntity

@Dao
interface BlocksDao {

    //Plantillas
    @Query("SELECT * FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita")
    suspend fun selectBodyBlock(idPlantilla: String, idVisita: String): BlockBodyModel?
    @Query("SELECT * FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND id = :id")
    suspend fun isFinishedAct(idPlantilla: String, idVisita: String, id: Int): BlockBodyModel?

    @Query("SELECT * FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND isFinished = 0")
    suspend fun selectBodyBlockUnfinished(idPlantilla: String, idVisita: String): BlockBodyModel?

    @Query("SELECT * FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND id = :id")
    suspend fun selectBlock(idPlantilla: String, idVisita: String, id: Int): BlockBodyModel?

    @Query("SELECT * FROM block_body_table")
    suspend fun selectAllBodyBlock(): List<BlockBodyModel>

    @Insert(entity = BlockBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBodyBlock(plantillas: BlockBodyModel)

    @Query("DELETE FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND id = :id")
    suspend fun deleteBodyBlock(idPlantilla: String, idVisita: String, id: Int)

    @Transaction
    suspend fun insertOrUpdateBodyBlock(blockBodyModel: BlockBodyModel) {
        val items = selectBlock(blockBodyModel.idPlantilla.toString(), blockBodyModel.idVisita.toString(), blockBodyModel.id)
        if (items == null) insertBodyBlock(blockBodyModel) else updateBodyBlock(blockBodyModel.body.toString(), blockBodyModel.idPlantilla.toString(), blockBodyModel.idVisita.toString(), blockBodyModel.id)
    }

    @Query("UPDATE block_body_table SET body = :body WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND id = :id")
    suspend fun updateBodyBlock(body: String, idPlantilla: String, idVisita: String, id: Int)

    @Query("UPDATE block_body_table SET isFinished = :isFinishedAct WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND id = :id")
    suspend fun updateFinishedBlock(isFinishedAct: Boolean, idPlantilla: String, idVisita: String, id: Int)

}