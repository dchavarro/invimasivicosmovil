package com.soaint.domain.use_case

import com.soaint.domain.common.BadCredentialException
import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.common.getIp
import com.soaint.domain.enum.Roles
import com.soaint.domain.model.UserEntity
import com.soaint.domain.repository.UserRepository
import com.soaint.sivicos_dinamico.utils.ID_MISIONAL_SANITARIAS
import com.soaint.sivicos_dinamico.utils.ROLS_ASGPAPF
import com.soaint.sivicos_dinamico.utils.ROLS_FIPAPF
import retrofit2.HttpException
import javax.inject.Inject

class UserUseCase @Inject constructor(
    private val userRepository: UserRepository
) {

    val user get() = userRepository.user

    fun getUser() = userRepository.user.value

    fun getUserInformation() = userRepository.userInformation.value

    fun getUserRoles() = userRepository.userRoles.value

    fun getUserName() = userRepository.userInformation.value?.usuario.orEmpty()

    fun getUserMisional(): Int? = userRepository.userInformation.value?.idMisional

    fun userIsPapf() =
        userRepository.userInformation.value?.idMisional == ID_MISIONAL_SANITARIAS
                && getUserRoles()?.any { Roles.ROLESPAPF.rol.contains(it.codigo)  } == true

    fun userIsPapfCis() =
        userRepository.userInformation.value?.idMisional == ID_MISIONAL_SANITARIAS
                && userRepository.userInformation.value?.idDependencia == 61 &&
                getUserRoles()?.any { it.codigo == ROLS_FIPAPF } == true

    fun fullname() =
        getUserInformation()?.primerApellido + " " + getUserInformation()?.segundoApellido.toString() + ", " + getUserInformation()?.primerNombre.toString() + " " + getUserInformation()?.segundoNombre.toString()

    suspend fun login(userName: String, password: String): Result<UserEntity> {
        val result = userRepository.getToken(userName, password)
        return when (result.exceptionOrNull()) {
            is WithoutConnectionException, is HttpException -> getLocalToken(userName, password)
            is BadCredentialException -> Result.failure(BadCredentialException())
            else -> result
        }
    }

    private suspend fun getLocalToken(userName: String, password: String): Result<UserEntity> {
        val result = userRepository.getLocalToken(userName, password)
        return when (result.exceptionOrNull()) {
            is BadCredentialException, is HttpException -> Result.failure(BadCredentialException())
            else -> result
        }
    }

    suspend fun updateUserInformation(userName: String) =
        userRepository.updateUserInformation(userName, getIp())

    fun logout() {
        userRepository.logout()
    }

    fun fullNameOrdered() =
        getUserInformation()?.primerNombre.toString() + " " + getUserInformation()?.segundoNombre.toString() + " " + getUserInformation()?.primerApellido + " " + getUserInformation()?.segundoApellido.toString()
    fun getJobTitle() = getUserInformation()?.cargo.toString()

}