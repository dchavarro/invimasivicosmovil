package com.soaint.sivicos_dinamico.flow.visit.views.manageMss

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentManageMssBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding

class ManageMssFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentManageMssBinding>(R.layout.fragment_manage_mss)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: ManageMssViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.gestionar_mss))

        binding.includeAplicaMedidaSanitaria.yes.setOnSingleClickListener {
            viewModel.setAplicaMedidaSanitaria(
                true
            )
        }
        binding.includeAplicaMedidaSanitaria.no.setOnSingleClickListener {
            viewModel.setAplicaMedidaSanitaria(
                false
            )
        }

        binding.includeLegalAtiende.yes.setOnSingleClickListener { viewModel.setLegalAtiende(true) }
        binding.includeLegalAtiende.no.setOnSingleClickListener { viewModel.setLegalAtiende(false) }

        binding.buttonProduct.setOnSingleClickListener {
            findNavController().navigate(ManageMssFragmentDirections.toProduct())
        }
        binding.buttonCompany.setOnSingleClickListener {
            findNavController().navigate(ManageMssFragmentDirections.toCompany())
        }
        binding.buttonOther.setOnSingleClickListener {
            findNavController().navigate(ManageMssFragmentDirections.toNso())
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(ManageMssFragmentDirections.toInfo())
        }

    }

    private fun observerViewModelEvents() {
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.setAplicaMedidaSanitaria(viewModel.visitData.value?.firstOrNull()?.aplicaMedidaSanitaria?:true)
        viewModel.setLegalAtiende(viewModel.visitData.value?.firstOrNull()?.legalAtiende?:false)
    }

}