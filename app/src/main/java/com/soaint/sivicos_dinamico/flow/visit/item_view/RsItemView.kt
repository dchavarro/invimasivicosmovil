package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.domain.model.RsEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemMsspListBinding
import com.soaint.sivicos_dinamico.databinding.ItemRsBinding
import com.xwray.groupie.databinding.BindableItem

class RsItemView(
    private val rs: RsEntity,
    private val onSelected: (RsEntity) -> Unit,
) : BindableItem<ItemRsBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemRsBinding,
        position: Int
    ) = with(viewBinding) {

        textViewName.text = rs.nombreProducto
        textViewNumber.text = rs.codigo

        root.setOnClickListener {
            onSelected.invoke(rs)
        }
    }

    override fun getLayout() = R.layout.item_rs
}