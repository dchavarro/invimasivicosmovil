package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.BodyObservationEntity

data class BodyObservationDto(
    @SerializedName("descripcion") val descripcion: String,
    @SerializedName("idTipoProducto") val idTipoProducto: Int,
    @SerializedName("idVisita") val idVisita: Int,
)

fun BodyObservationEntity.mapToData() = BodyObservationDto(
    descripcion,
    idTipoProducto,
    idVisita,
)