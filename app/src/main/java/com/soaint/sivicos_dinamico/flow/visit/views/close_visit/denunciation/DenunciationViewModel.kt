package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.denunciation

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.domain.model.*
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CDDC
import javax.inject.Inject

sealed class onDenunciationViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onDenunciationViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onDenunciationViewModelEvent()
    data class OnViewModelApplyReady(val result: List<ManageMssEntity>) : onDenunciationViewModelEvent()
    data class OnViewModelReady(val denunciation: List<AntecedentEntity>?) : onDenunciationViewModelEvent()
    data class OnViewModelSuccess(@StringRes val message: Int): onDenunciationViewModelEvent()
}

class DenunciationViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
    private val manageMssUseCase: ManageMssUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onDenunciationViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _user = MutableLiveData<UserInformationEntity>()
    val user = _user.asLiveData()

    private val _isSelected = MutableLiveData<Boolean>(null)
    val isSelected = _isSelected.asLiveData()

    private val _apply = MutableLiveData<List<ManageMssEntity>>()
    val apply = _apply.asLiveData()

    private val _applySelected = MutableLiveData<ManageMssEntity>(null)
    val applySelected = _applySelected.asLiveData()

    private val _antecedentSelected = MutableLiveData<AntecedentEntity>(null)
    val antecedentSelected = _antecedentSelected.asLiveData()

    val idTipoproducto get() = _visitData.value?.firstOrNull()?.idTipoProducto.orZero()

    fun init(visitData: List<VisitEntity>) {
        _visitData.value = visitData
        onLoadAplied()
        onLoadDenunciation()
    }

    private fun onLoadDenunciation() = execute {
        val result = visitUseCase.loadAntecedentLocalByPqr(_visitData.value?.firstOrNull()?.id.orZero().toString())
        _event.value = if (result.isSuccess) {
            onDenunciationViewModelEvent.OnViewModelReady(result.getOrNull())
        } else {
            onDenunciationViewModelEvent.onLoadingError(null)
        }
    }

    private fun onLoadAplied() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDDC, idTipoproducto)
        _event.value = if (result.isSuccess) {
            _apply.value = result.getOrNull()
            onDenunciationViewModelEvent.OnViewModelApplyReady(result.getOrNull().orEmpty())
        } else {
            onDenunciationViewModelEvent.onLoadingError(null)
        }

    }

    fun setIsSelected(isSelected: Boolean, antecedent: AntecedentEntity) {
        _antecedentSelected.value = antecedent.copy(
            aplicaDenuncia = isSelected,
            fechaModifica = getDateHourWithOutFormat()
        )
        updateAntecedent(_antecedentSelected.value)
    }

    fun setApplySelected(applySelected: String, antecedent: AntecedentEntity) {
        _apply.value?.find { it.descripcion.equals(applySelected, true) }?.let {
            _antecedentSelected.value = antecedent.copy(
                idDenuncia = it.id,
                fechaModifica = getDateHourWithOutFormat()
            )
            updateAntecedent(_antecedentSelected.value)
        }
    }

    fun setApplySelectedByID(id: Int) : String {
        val text = _apply.value?.find { it.id == id }?.let {
             it.descripcion
        }
        return text.orEmpty()
    }

    fun updateAntecedent(antecedent: AntecedentEntity?) = execute {
        val result = antecedent?.let { visitUseCase.updateAntecedentLocal(it) }
        if (result!= null) {
            _event.value = if (result.isSuccess) {
                onDenunciationViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
            } else {
                onDenunciationViewModelEvent.onLoadingError(null)
            }
        }
        onLoadDenunciation()
    }
}