package com.soaint.sivicos_dinamico.flow.visit.views.historic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.HistoricEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentHistoricBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.HistoricItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class HistoricFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentHistoricBinding>(R.layout.fragment_historic)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: HistoricViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = initView()

    private fun initView(): View {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.historico_visita))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(HistoricFragmentDirections.toInfo())
        }
        subscribeViewModel()
        return binding.root
    }

    private fun subscribeViewModel() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnHistoricViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnHistoricViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnHistoricViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnHistoricViewModelEvent.OnViewModelReady -> onViewModelReady(event.historic)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReady(historic: List<HistoricEntity>) {
        adapter.clear()
        val items = mutableListOf<Group>()

        if (!historic.isNullOrEmpty()) {
            items.addAll(historic.map { HistoricItemView(it, ::onHistoricSelected, ::onViewSelected) })
        }

        val isVisible = historic.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible
        adapter.updateAsync(items)
    }

    private fun onHistoricSelected(historic: HistoricEntity) { }

    private fun onViewSelected(idVisita: String) {
        viewModel.goToDocument(idVisita)
    }
}