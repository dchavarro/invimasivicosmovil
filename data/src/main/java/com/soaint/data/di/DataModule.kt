package com.soaint.data.di

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.EncryptedSharedPreferences.PrefKeyEncryptionScheme.AES256_SIV
import androidx.security.crypto.EncryptedSharedPreferences.PrefValueEncryptionScheme.AES256_GCM
import androidx.security.crypto.MasterKeys
import androidx.security.crypto.MasterKeys.AES256_GCM_SPEC
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import java.io.IOException
import java.security.GeneralSecurityException
import javax.inject.Named
import javax.inject.Singleton

const val SHARED_PREFERENCES = "SHARED_PREFERENCES"
const val SECRET_SHARED_PREFERENCES = "SECRET_SHARED_PREFERENCES"

private const val API_TOKEN_HEADER = "api_token"
private const val API_REGISTER_TOKEN_HEADER = "register_api_token"

@Module(
    includes = [
        ApiModule::class,
        DispatchersModule::class,
        ProviderModule::class,
        RepositoryModule::class,
        AppNetworkModule::class,
        DataBaseModule::class,
    ]
)
class DataModule {

    @Provides
    @Singleton
    fun providerGson(): Gson = GsonBuilder().create()

    @Provides
    @Singleton
    @Named(SHARED_PREFERENCES)
    fun providerSharedPreferences(
        context: Context
    ): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    @Provides
    @Singleton
    @Named(SECRET_SHARED_PREFERENCES)
    fun providerSharedPreferencesEncrypted(
        context: Context
    ): SharedPreferences? {
        val keystoreAlias: String = try {
            MasterKeys.getOrCreate(AES256_GCM_SPEC)
        } catch (e: GeneralSecurityException) {
            ""
        } catch (e: IOException) {
            ""
        }
        return try {
            EncryptedSharedPreferences.create(
                "crypted",
                keystoreAlias,
                context,
                AES256_SIV,
                AES256_GCM
            )
        } catch (e: GeneralSecurityException) {
            null
        } catch (e: IOException) {
            null
        }
    }
}