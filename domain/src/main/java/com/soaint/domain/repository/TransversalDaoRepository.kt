package com.soaint.domain.repository

import com.soaint.domain.model.*

interface TransversalDaoRepository {

    suspend fun loadTypeDao(tipoDao: String): Result<List<TypeDaoEntity>?>

    suspend fun loadTypeDaoLocal(tipoDao: String): Result<List<TypeDaoEntity>>

    suspend fun loadCountries(): Result<List<CountriesEntity>?>

    suspend fun loadCountriesLocal(): Result<List<CountriesEntity>>

    suspend fun loadDepartment(): Result<List<DepartmentEntity>?>

    suspend fun loadDepartmentLocal(idPais: Int): Result<List<DepartmentEntity>>

    suspend fun loadTown(): Result<List<TownEntity>?>

    suspend fun loadTownLocal(idDepartament: Int): Result<List<TownEntity>>

}