package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.model.NotificationEntity
import com.soaint.domain.model.RegisterDateBodyEntity

@Entity(tableName = "register_date_papf_table")
class RegisterDateModel(
    @ColumnInfo(name = "fecha") val fecha: String?,
    @ColumnInfo(name = "observaciones") val observaciones: String?,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean = false,
    @PrimaryKey val idSolicitud: Int,
)

fun RegisterDateModel.mapToDomain() = RegisterDateBodyEntity(
    fecha,
    observaciones,
    idSolicitud
)