package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TransportEntity(
    val direccionMisional: String?,
    val numeroOficioComisorio: String?,
    val nombreEmpresa: String?,
    val nitEmpresa: String?,
    val direccionEmpresa: String?,
    val funcionarios: List<OfficialEntity>?,
) : Parcelable

@Parcelize
data class OfficialEntity(
    val nombreApellidos: String?,
    val celular: String?,
    val correoElectronico: String?,
) : Parcelable

@Parcelize
data class TransportBodyEntity(
    val idTipoTransporte: Int,
    val tipoTransporte: String,
    val motivoAppMediSanitaria: String,
    val dimensionesAproxMercancia: String,
    val numPiezasAproximadamente: String,
    val volumenAproximado: String,
    val pesoAproximado: String,
    val tipoMercancia: String,
    val numAuxNecesaCargue: String,
    val valorAproxDecomiso: String,
    val fechaRecogida: String,
    val correoElectronico: String,
    val idVisita: String,
    val activo: Boolean = true,
    val usuarioCrea: String,
    val fechaCreacion: String,
    val fechaModifica: String,
    val isLocal: Boolean,
) : Parcelable