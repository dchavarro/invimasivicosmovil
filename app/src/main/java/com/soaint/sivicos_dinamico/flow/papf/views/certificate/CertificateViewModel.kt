package com.soaint.sivicos_dinamico.flow.papf.views.certificate

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.SanitaryCertificatePapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(val certificate: List<SanitaryCertificatePapfEntity>) :
        OnViewModelEvent()

    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class CertificateViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _certificateData = MutableLiveData<List<SanitaryCertificatePapfEntity>>()
    val certificateData = _certificateData.asLiveData()


    fun init(id: Int) {
        loadCertificate(id)
    }

    fun loadCertificate(requestId: Int) = execute {
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _certificateData.value =
            papfUseCase.loadSanitaryCertificateLocal(requestId).getOrNull().orEmpty()
        _event.value = if (_certificateData.value != null) {
            certificateData.value?.let { OnViewModelEvent.OnViewModelReady(it) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
