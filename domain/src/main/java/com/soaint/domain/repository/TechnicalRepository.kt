package com.soaint.domain.repository

import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.TechnicalEntity
import kotlinx.coroutines.flow.StateFlow

interface TechnicalRepository {

    val technical: StateFlow<List<TechnicalEntity>?>

    suspend fun getTechnicals(idVisita: Int): Result<List<TechnicalEntity>>

    suspend fun getTechnicalsLocal(idVisita: Int): Result<List<TechnicalEntity>>

    suspend fun updateTechnicalLocal(technical: TechnicalEntity): Result<Unit>

    suspend fun updateTechnical(): Result<Unit>

    suspend fun getCertificate(nitEmpresa: String): Result<List<CertificateEntity>>

    suspend fun getCertificateLocal(nitEmpresa: String): Result<List<CertificateEntity>>

    suspend fun updateCertificate(): Result<List<Unit>>

    suspend fun updateCertificateLocal(body: CertificateEntity): Result<Unit>

}