package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface ManageMssRepository {

    val manageMss: StateFlow<ManageMssEntity?>

    //Producto
    suspend fun loadTypeMss(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int
    ): Result<List<ManageMssEntity>?>

    suspend fun loadTypeMssLocal(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int
    ): Result<List<ManageMssEntity>>

    suspend fun loadRol(tipo: String, idTipoProducto: Int): Result<List<RolEntity>?>

    suspend fun countMssLocal(idVisita: String): Result<Int?>

    suspend fun loadListMss(razonSocial: String, tipoMedidaSanitaria: String): Result<List<ManageMsspEntity>>

    suspend fun loadListMssLocal(razonSocial: String, tipoMedidaSanitaria: String): Result<List<ManageMsspEntity>>

    suspend fun updateMsspListLocal(id: Int, codigo: String, tipoMs: String, lote: String): Result<Unit>

    suspend fun insertMsseListLocal(visit: VisitEntity, codigo: String, tipoMs: String, typeActivity: String, nombreProducto: String, registroSanitario: String): Result<Unit>

    suspend fun updateMsspList(): Result<List<Unit>>

    suspend fun insertMsseList(idVisit: Int): Result<List<Unit>>

    suspend fun updateAmsspLocal(it: ManageMssEntity, idVisita: String): Result<Unit>

    suspend fun insertOrUpdateTypeProductLocal(it: ManageMssEntity, idVisita: String): Result<Unit>

    suspend fun searchRegisterSanitary(search: String): Result<String>

    suspend fun loadMssApplied(idVisit: String): Result<List<MssAppliedEntity>>

    suspend fun insertMssApplied(idVisit: String): Result<Unit>

    suspend fun loadMssAppliedLocal(idVisit: String): Result<List<ManageAmssEntity>>

    suspend fun getmssUpload(): Result<Int>

    suspend fun insertRsLocal(idVisit: Int, nombreProducto: String, registroSanitario: String): Result<Unit>

    suspend fun loadRsLocal(idVisit: Int): Result<List<RsEntity>>

    suspend fun loadMssLocalByIdVisit(idVisit: Int): Result<List<ManageMsspEntity>>

    suspend fun loadDateMssApplied(idVisit: Int, mss: ManageAmssEntity): Result<String>

    suspend fun loadTypeProduct(tipo: String): Result<List<TypeProductEntity>?>

}