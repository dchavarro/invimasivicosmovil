package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.model.DataReqActSamplePapfEntity
import com.soaint.domain.model.DataReqInsSanPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity

data class EmpaqueResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<EmpaqueDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class EmpaqueDto(
    @SerializedName("IdEstadoEmpaque") val id: Int?,
    @SerializedName("DescripcionEstadoEmpaque") val descripcion: String?
)

fun EmpaqueDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun EmpaqueDto.mapToDb(codeQuery: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = codeQuery,
    idTipoTramite = idTipoTramite
)

data class ConceptCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ConceptCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class ConceptCDDto(
    @SerializedName("IdConcepto") val id: Int?,
    @SerializedName("DescripcionConcepto") val descripcion: String?
)

fun ConceptCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun ConceptCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class UnityCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<UnityCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class UnityCDDto(
    @SerializedName("IdUnidadMedida") val id: Int?,
    @SerializedName("DescripcionUnidadMedida") val descripcion: String?
)

fun UnityCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun UnityCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class PresentationCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<PresentationCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class PresentationCDDto(
    @SerializedName("IdPresentacionComercial") val id: Int?,
    @SerializedName("PresentacionComercial") val descripcion: String?
)

fun PresentationCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun PresentationCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class DataReqInsSanPapfDto(
    @SerializedName("idEstadoEmpaque") val idEstadoEmpaque: Int?,
    @SerializedName("descripcionEmpaque") val descripcionEmpaque: String?,
    @SerializedName("idConcepto") val idConcepto: Int?,
    @SerializedName("descripcionConcepto") val descripcionConcepto: String?,
    @SerializedName("condicionesAlmacenamiento") val condicionesAlmacenamiento: String?,
    @SerializedName("observaciones") val observaciones: String?,
    @SerializedName("detalleProducto") val detalleProducto: Int?,
    @SerializedName("idSolicitud") val idSolicitud: Int?,
)

fun DataReqInsSanPapfEntity.mapToData() = DataReqInsSanPapfDto(
    idEstadoEmpaque,
    descripcionEmpaque,
    idConcepto,
    descripcionConcepto,
    condicionesAlmacenamiento,
    observaciones,
    detalleProducto,
    idSolicitud,
)

fun DataReqInsSanPapfDto.mapToDb() = DataReqInsSanPapfModel(
    idEstadoEmpaque,
    descripcionEmpaque,
    idConcepto,
    descripcionConcepto,
    condicionesAlmacenamiento,
    observaciones,
    detalleProducto,
    idSolicitud,
)

data class DataReqInsSanPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosDataReqInsSanPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosDataReqInsSanPapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosDataReqInsSanPapfDto(
    @SerializedName("condicionesAlmacenamiento") val condicionesAlmacenamiento: String,
    @SerializedName("observaciones") val observaciones: String,
    @SerializedName("idEstadoEmpaque") val idEstadoEmpaque: Int,
    @SerializedName("idConcepto") val idConcepto: Int,
    @SerializedName("detalleProducto") val detalleProducto: Int,
)

data class DataReqActSamplePapfDto(
    @SerializedName("nroUnidades") val nroUnidades: Int?,
    @SerializedName("descripcionUnidades") val descripcionUnidades: String?,
    @SerializedName("unidadesMedida") val unidadesMedida: Int?,
    @SerializedName("presentacion") val presentacion: Int?,
    @SerializedName("descripcionPresentacion") val descripcionPresentacion: String?,
    @SerializedName("contenidoNeto") val contenidoNeto: Int?,
    @SerializedName("producto") val idProducto: Int?,
    @SerializedName("idSolicitud") val idSolicitud: Int?,
)

fun DataReqActSamplePapfEntity.mapToData() = DataReqActSamplePapfDto(
    nroUnidades,
    descripcionUnidades,
    unidadesMedida,
    presentacion,
    descripcionPresentacion,
    contenidoNeto,
    idProducto,
    idSolicitud,
)

fun DataReqActSamplePapfDto.mapToDb() = DataReqActSamplePapfModel(
    nroUnidades,
    descripcionUnidades,
    unidadesMedida,
    presentacion,
    descripcionPresentacion,
    contenidoNeto,
    idProducto,
    idSolicitud,
)

data class DataReqActSamplePapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosDataReqActSamplePapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosDataReqActSamplePapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosDataReqActSamplePapfDto(
    @SerializedName("nroUnidades") val nroUnidades: Int,
    @SerializedName("unidadesMedida") val unidadesMedida: Int,
    @SerializedName("presentacion") val presentacion: Int,
    @SerializedName("contenidoNeto") val contenidoNeto: Int,
    @SerializedName("producto") val producto: Int,
)

data class MpigCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<MpigCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class MpigCDDto(
    @SerializedName("IdUsoMPIG") val id: Int?,
    @SerializedName("DescripcionUsoMPIG") val descripcion: String?
)

fun MpigCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun MpigCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class ResultCisCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ResultCisCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class ResultCisCDDto(
    @SerializedName("IdResultadoCis") val id: Int?,
    @SerializedName("DescripcionResultadoCis") val descripcion: String?
)

fun ResultCisCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun ResultCisCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class IdiomCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<IdiomCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class IdiomCDDto(
    @SerializedName("IdIdioma") val id: Int?,
    @SerializedName("DescripcionIdioma") val descripcion: String?
)

fun IdiomCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun IdiomCDDto.mapToDb(idTipoTramite: String, tipo: String) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

data class FirmatCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<FirmatCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class FirmatCDDto(
    @SerializedName("IdUsuario") val id: Int?,
    @SerializedName("IdFuncionario") val idFuncionario: Int?,
    @SerializedName("Firmante") val descripcion: String?
)

fun FirmatCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun FirmatCDDto.mapToDb(tipo: String, idTipoTramite: String? = null) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = tipo,
    idTipoTramite = idTipoTramite
)

fun FirmatCDDto.mapSignatoriesToDb() = SignatoriesPapfModel(
    idUsuario = id,
    idFuncionario=idFuncionario,
    nombre = descripcion
)

data class ObservationDefaultCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ObservationDefaultCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class ObservationDefaultCDDto(
    @SerializedName("IdObservaciones") val id: Int?,
    @SerializedName("DescripcionObservacion") val descripcion: String?
)

fun ObservationDefaultCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun ObservationDefaultCDDto.mapToDb(idTipoTramite: String? = null, codeQuery: String) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = codeQuery,
    idTipoTramite = idTipoTramite
)

data class RequerimientosCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<RequerimientosCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class RequerimientosCDDto(
    @SerializedName("IdTipoRequerimiento") val id: Int?,
    @SerializedName("DescripcionTipoRequerimiento") val descripcion: String?
)

fun RequerimientosCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun RequerimientosCDDto.mapToDb(codeQuery: String) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = codeQuery,
    idTipoTramite = null
)

data class CertificadosCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<CertificadosCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class CertificadosCDDto(
    @SerializedName("IdTipoCertificado") val id: Int?,
    @SerializedName("DescripcionTipoCertificado") val descripcion: String?
)

fun CertificadosCDDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun CertificadosCDDto.mapToDb(codeQuery: String) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = codeQuery,
    idTipoTramite = null
)

data class YesNoDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("descripcion") val descripcion: String?
)


fun YesNoDto.mapToDomain(idTipoTramite: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    tipo,
    idTipoTramite
)

fun YesNoDto.mapToDb(codeQuery: String) = TypePapfModel(
    id = id,
    descripcion = descripcion,
    codeQuery = codeQuery,
    idTipoTramite = null
)