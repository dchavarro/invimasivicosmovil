package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.data.model.VisitFormaDto

@Entity(tableName = "company_address_table")
class CompanyAddressModel(
    @ColumnInfo(name = "descDepartamento") val descDepartamento: String,
    @ColumnInfo(name = "descMunicipio") val descMunicipio: String,
    @ColumnInfo(name = "descPais") val descPais: String,
    @ColumnInfo(name = "descripcion") val descripcion: String,
    @ColumnInfo(name = "idDepartamento") val idDepartamento: Int? =0,
    @ColumnInfo(name = "idMunicipio") val idMunicipio: Int? =0,
    @ColumnInfo(name = "idPais") val idPais: Int? =0,
    @ColumnInfo(name = "idEmpresa") var idEmpresa: Int? =0,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)