package com.soaint.domain.use_case

import android.os.Build
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.soaint.domain.common.TiposControl.Companion.CHECK_LIST
import com.soaint.domain.common.TiposControl.Companion.FIRMA
import com.soaint.domain.common.TiposControl.Companion.FOTO
import com.soaint.domain.common.TiposControl.Companion.LISTA_TABLA
import com.soaint.domain.common.getBase64FromPath
import com.soaint.domain.common.getDateNow
import com.soaint.domain.common.orEmpty
import com.soaint.domain.enum.ActsFilter
import com.soaint.domain.enum.TypeProductPapf
import com.soaint.domain.enum.Types
import com.soaint.domain.model.*
import com.soaint.domain.repository.PreferencesRepository
import com.soaint.sivicos_dinamico.utils.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject
import javax.inject.Singleton

private val SYNC_DATE_KEY = "DATE_KEY"

@Singleton
class SyncDataUseCase @Inject constructor(
    private val preferencesRepository: PreferencesRepository,
    private val plantillasUseCase: PlantillasUseCase,
    private val taskUseCase: TaskUseCase,
    private val visitUseCase: VisitUseCase,
    private val historicUseCase: HistoricUseCase,
    private val historicMssUseCase: HistoricMssUseCase,
    private val manageUseCase: ManageUseCase,
    private val transportUseCase: TransportUseCase,
    private val documentUseCase: DocumentUseCase,
    private val userUseCase: UserUseCase,
    private val notificationUseCase: NotificationUseCase,
    private val actUseCase: ActUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val modeloIvcUseCase: ModeloIvcUseCase,
    private val technicalUseCase: TechnicalUseCase,
    private val validationUseCase: ValidationUseCase,
    private val equipmentUseCase: EquipmentUseCase,
    private val transversalDaoUseCase: TransversalDaoUseCase,
    private val registerCompanyUseCase: RegisterCompanyUseCase,

    private val taskPapfUseCase: TaskPapfUseCase,
    private val papfUseCase: PapfUseCase,
) : CoroutineScope by MainScope() {

    private var _syncDate = MutableStateFlow<String?>(null)
    val syncDate get() = _syncDate.asStateFlow()

    init {
        _syncDate.value = preferencesRepository.getString(SYNC_DATE_KEY, "")
    }

    // Sincronizacion de servicios PAPF
    suspend fun loadPapf(): SyncPapfDataResult {
        val papfResult = loadDataPapf()
        val plantillaResult = loadPlantillaPapf()
        loadBlockValuePapf()
        val result = SyncPapfDataResult(plantillaResult, papfResult)
        return result.also {
            updateSyncDate()
            syncPending()
        }
    }

    // Sincronizacion de servicios Ejecucion
    suspend fun load(): SyncDataResult {
        val taskResult = loadTask()
        val plantillaResults = getIdTipoProductos().mapNotNull { loadPlantilla(it) }
        loadBlockValue()
        val result = SyncDataResult(plantillaResults, taskResult)

        return result.also {
            updateSyncDate()
            syncPending()
        }
    }

    // Obtener plantilla y lo relacionado a actas
    private suspend fun loadPlantilla(
        idTipoProducto: Int,
    ): SyncPlantillaDataResult {
        val plantillasResult = plantillasUseCase.loadPlantillas(idTipoProducto)

        val groupDocumentResult = manageUseCase.loadGroupDocument()
        val groupDocumentId = groupDocumentResult.getOrNull()?.mapNotNull { it.idGrupo }
        val documentNameResult =
            groupDocumentId?.mapNotNull { documentUseCase.loadDocumentName(it) }

        return if (plantillasResult.isSuccess) {
            val plantillaId = plantillasResult.getOrNull().orEmpty().map { it.id }
            val blockResult = plantillaId.map { plantillasUseCase.loadBlocks(it.toString()) }
            val groupResult = manageUseCase.loadGroup(idTipoProducto)

            if (groupResult.isSuccess) {
                val groupId = groupResult.getOrNull().orEmpty().map { it.id }
                val actaResult =
                    groupId.map { manageUseCase.loadRulesBussiness(it!!, idTipoProducto) }
                SyncPlantillaDataResult(
                    plantillasResult,
                    groupDocumentResult,
                    documentNameResult,
                    blockResult,
                    groupResult,
                    actaResult
                )
            } else {
                SyncPlantillaDataResult(
                    plantillasResult,
                    groupDocumentResult,
                    documentNameResult,
                    blockResult,
                    groupResult
                )
            }
        } else SyncPlantillaDataResult(
            plantillasResult,
            groupDocumentResult,
            documentNameResult
        )
    }

    // Obtener plantilla y lo relacionado a actas
    private suspend fun loadPlantillaPapf(): SyncPapfPlantillaDataResult {
        val plantillasResult = TypeProductPapf.TYPEPRODUCTPAPF.id.flatMap { idTipoProductoPapf ->
            ActsFilter.ACTSFILTERPAPF.filter.mapNotNull { filter ->
                manageUseCase.loadRulesBussinessPapf(idTipoProductoPapf, filter)
            }
        }

        val plantillas = plantillasResult.mapNotNull { it.getOrNull() }.flatten()
            .distinctBy { it.codigoPlantilla }

        return if (!plantillas.isNullOrEmpty()) {
            val plantillaId = plantillas.map { it.id }
            val blockResult = plantillaId.map { plantillasUseCase.loadBlocks(it.toString()) }
            val groupResult = manageUseCase.loadGroupPapf()
            if (groupResult.isSuccess) {
                val groupId = groupResult.getOrNull().orEmpty().map { it.id }
                val documentNameResult = groupId.map { documentUseCase.loadDocumentName(it!!) }
                SyncPapfPlantillaDataResult(
                    plantillasResult,
                    blockResult,
                    groupResult,
                    documentNameResult
                )
            } else {
                SyncPapfPlantillaDataResult(
                    plantillasResult, blockResult, groupResult
                )
            }
        } else SyncPapfPlantillaDataResult(plantillasResult)
    }

    // Obtener tareas y lo relacionado a visitas
    private suspend fun loadTask(): SyncTaskDataResult {
        val userInformationResult =
            userUseCase.getUser()?.let { userUseCase.updateUserInformation(it.preferredUsername) }

// ****************** Sync Upload ****************** //
        val taskLocal = taskUseCase.loadTasksLocal()
        val taskIdLocal = taskLocal.getOrNull().orEmpty().map { it.idTarea }

        val getVisit = taskIdLocal.map { visitUseCase.loadVisitLocal(it) }
        val visits = getVisit.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()

        //Sync Upload Location
        val locationResult = visitUseCase.syncLocation()

        //Sync Upload Empresa Registrada GURI
        val companyRegisterResult = visits.map { registerCompanyUseCase.saveCompany(it.id) }

        // Sync Upload Transporte Decomiso
        val getDecomiso = visits.map { transportUseCase.getTransportBodyLocal(it.id.toString()) }
        val decomisos = getDecomiso.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        val decomisosResult = decomisos.map { transportUseCase.saveTransport(it) }

        // Sync Upload Documentos Asociados
        val getDocument =
            visits.map { documentUseCase.getDocumentBodyLocalByVisit(it.id.toString()) }
        val documents = getDocument.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        val documentsResult = documents.map { documentUseCase.addDocument(it) }

        // Sync Upload save Emails
        val emailsResult = visits.map { visit ->
            val getEmails = notificationUseCase.getEmailsLocal(visit.id.toString())
            val emails = getEmails.getOrNull().orEmpty()
            if (!emails.isNullOrEmpty()) {
                notificationUseCase.saveEmails(visit.id.toString(), emails.map { it.correo.toString() })
            }
        }

        // Sync Upload Sync Block
        plantillasUseCase.getAllBlockBody().onEach {
            val body = try {
                Gson().fromJson(it.body, HashMap<String, String>().javaClass).orEmpty()
            } catch (e: Exception) {
                mapOf<String, String>()
            }
            mapBlockBody(it.idPlantilla, it.idVisita, body, it.id)
        }

        // Sync Upload mssp
        val updateMsspResult = manageMssUseCase.updateMsspList()

        // Sync Upload msse
        val insertMsseResult = visits.map { manageMssUseCase.insertMsseList(it.id) }

        // Sync Upload Mss Aplicada
        val insertMssApplied = visits.map { manageMssUseCase.insertMssApplied(it.id.toString()) }

        // Sync Upload Delete Requerimiento
        val deleteIvc = modeloIvcUseCase.deleteRequerimiento()

        // Sync Upload Add Requerimiento
        val insertIvc = modeloIvcUseCase.addRequerimiento()

        // Sync Upload Add Requerimiento Datos Adiccionales
        val insertIvcDa = modeloIvcUseCase.addReqDA()

        // Sync Upload Denuncias
        val getAntecedent = visits.map { visitUseCase.loadAntecedentLocal(it.id.toString()) }
        val antecedents =
            getAntecedent.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        val updateAntecedent =
            visitUseCase.updateAntecedent(antecedents.filter { it.codigoTipo == AMTE_PQRS })

        // Sync Upload Technical
        val updateTechnical = technicalUseCase.updateTechnical()

        // Sync Upload Interaction
        val getInteraction = visits.map { visitUseCase.loadInteractionLocal(it.id.toString()) }
        val interactions = getInteraction.filter { it.isSuccess }.mapNotNull { it.getOrNull() }
        val updateInteraction = interactions.map { visitUseCase.updateInteraction(it) }

        // Sync Upload Observation
        val getObservation = visits.map {
            validationUseCase.getObservationLocal(
                it.id.toString(),
                it.idTipoProducto.orEmpty()
            )
        }
        val observation = getObservation.filter { it.isSuccess }.mapNotNull { it.getOrNull() }
        val updateObservation = observation.map { validationUseCase.updateObservation(it) }

        // Sync Upload Certificado
        val updateCertificateResult = technicalUseCase.updateCertificate()

        // Sync Insert Equipos
        val insertEquipmentResult = equipmentUseCase.addEquipment()

        // Sync Upload Equipos
        val updateEquipmentResult = equipmentUseCase.updateEquipment()

        // Sync Horas Inspeccion permanente
        val updateScheduleResult = visitUseCase.updateSchedule()

        // Sync Upload Validation and Notification
        var isReportable: Boolean = false
        val visitCopyResult = visits.map {
            var visit = it
            val razonSocial = it.empresa?.razonSocial.orEmpty()

            // NUEVAS VALIDACIONES
            //* C0. Accion a seguir = Solicitar tiempo/personal adicional a la visita
            if (it.idAccionSeguir == ID_ACTION_ACSE_STPAV) {
                visit = it.copy(idEstado = ID_STATUS_PROGRAMACION)
                updateCodeTask(visit)
                isReportable = true
            }
            //* C1. Resultado = No competencia del invima, accion a seguir = cerrar Visita (Notificacion establecimiento) SI
            if (it.idResultado == ID_RESULT_NO_INVIMA && it.idAccionSeguir == ID_ACTION_ACSE_CEVI) {
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                inActiveTask(visit)
                isReportable = true
            }

            // * C2. Resultado = reprogramada. Accion a seguir = solicitar prog visita, Requiere visita en misma direccion = true
            if (it.idResultado == ID_RESULT_REPROGRAMADA && it.idAccionSeguir == ID_ACTION_ACSE_SPVI && it.requiereReprogramarVisita == true) {
                visitUseCase.copyVisit(it.id.orEmpty().toString())
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                inActiveTask(visit)
                isReportable = true
            }

            //* C3. Resultado = reprogramada. Accion a seguir = solicitar programacion visita. Requiere visita en misma direccion = false
            if (it.idResultado == ID_RESULT_REPROGRAMADA && it.idAccionSeguir == ID_ACTION_ACSE_SPVI && it.requiereReprogramarVisita == false) {
                visit = it.copy(idEstado = ID_STATUS_PROGRAMACION)
                updateCodeTask(visit)
                isReportable = true
            }

            //* C4. Resultado = no ejecutada. Accion a seguir = solicitar programacion visita.
            if (it.idResultado == ID_RESULT_NO_EJECUTADA && it.idAccionSeguir == ID_ACTION_ACSE_SPVI) {
                visit = it.copy(idEstado = ID_STATUS_PROGRAMACION)
                inActiveTask(visit)
                isReportable = true
            }

            //* C5. Resultado = ejecutada. Accion a seguir = cierre. Misional Dispositivos
            if (it.idResultado == ID_RESULT_EJECUTADA && it.idAccionSeguir == ID_ACTION_ACSE_CNVRC && it.idTipoProducto == ID_MISIONAL_DISPOSITIVOS) {
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                inActiveTask(visit)
                isReportable = true
            }

            //* C6. Resultado  = ejecutada. Accion a seguir = cierre. Razon: RAVI_IVC, RAVI_IVCTM, RAVI_IVCTMC, RAVI_ACOM
            if (it.idResultado == ID_RESULT_EJECUTADA && it.idAccionSeguir == ID_ACTION_ACSE_CEVI && (it.codigoRazon == REASON_RAVI_IVC || it.codigoRazon == REASON_RAVI_IVCTM || it.codigoRazon == REASON_RAVI_IVCTMC || it.codigoRazon == REASON_RAVI_ACOM)) {
                //TODO: Revisar -> QH se llama el servicio de generar radicados (seasuite) con el idvisita se actualiza el estado de la visita a ejecutada y la tarea pasa a el codigo
                //     de la actividad a GESTION DE RESULTADOS  el origen es IVC y el estado es pendiente (falta actualizar)
                validationUseCase.generateRadicals(it.id.orEmpty().toString())
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                updateCodeTask(visit)
                isReportable = true
            }

            //* C7. Resultado = ejecutada. Accion a seguir = cierre. Razon: RAVI_ACOM, aplica Medida sanitaria SI
            if (it.idResultado == ID_RESULT_EJECUTADA && it.idAccionSeguir == ID_ACTION_ACSE_CEVI && (it.codigoRazon == REASON_RAVI_AYCVC || it.codigoRazon == REASON_RAVI_AYCVC || it.codigoRazon == REASON_RAVI_AYCVVR || it.codigoRazon == REASON_RAVI_ACOM) && it.aplicaMedidaSanitaria == true) {
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                updateCodeTask(visit)
                isReportable = true
            }

            //* C8. Accion a seguir = crear nueva visita requerimiento, cierre visita. hay tramite notificar resultado
            if (it.tramite?.indNotificarResultado == true && (it.idAccionSeguir == ID_ACTION_ACSE_CNVRC || it.idAccionSeguir == ID_ACTION_ACSE_CEVI)) {
                validationUseCase.updateProcessExt(visit)
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA)
                //isReportable = true
            }

            visitUseCase.updateVisit(visit)

            if (isReportable && it.isClosed == true) {
                if (it.idAccionSeguir == ID_ACTION_ACSE_STPAV) {
                    //NOTIFICAR CNSTA
                    notificationUseCase.sendNotification(
                        it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNSTA
                    )
                }

                if (it.idResultado == ID_RESULT_NO_INVIMA || it.idResultado == ID_RESULT_REPROGRAMADA || it.idResultado == ID_RESULT_EJECUTADA) {
                    //NOTIFICAR CNE
                    notificationUseCase.sendNotification(
                        it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNE
                    )
                }

                val interaction = visitUseCase.loadInteractionLocal(it.id.toString()).getOrNull()
                if (interaction != null) {
                    if (interaction.requiereRetiroProducto == true && it.id == interaction.idVisita) {
                        //NOTIFICAR CNRRPM
                        notificationUseCase.sendNotification(
                            it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNRRPM
                        )
                    }

                    if (interaction.realizoSometimiento == true || !interaction.identificadorRecall.isNullOrBlank()) {
                        //NOTIFICAR CNRRPM
                        notificationUseCase.sendNotification(
                            it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNRRPM
                        )
                    }

                    if (interaction.perdidaCertificacion == true && it.id == interaction.idVisita) {
                        //NOTIFICAR CNPC
                        notificationUseCase.sendNotification(
                            it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNPC
                        )
                    }
                }

                val getMsse = manageMssUseCase.loadListMssLocal(razonSocial, MSSE)
                val msse = getMsse.getOrNull().orEmpty()
                msse.mapNotNull {
                    if (it.id == it.idVisita) {
                        //NOTIFICAR CNMSSE
                        notificationUseCase.sendNotification(
                            it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNMSSE
                        )
                    }
                }

                // MSS CONGELACION
                val getAmssCheck = manageMssUseCase.loadMssAppliedLocal(it.id.orEmpty().toString())
                val mssCongelation = getAmssCheck.getOrNull().orEmpty()
                    .filter { it.codigo == ID_CODE_MSS_CONGELACION && it.isSelected == true && it.tipoMs == MSSP }
                    .firstOrNull()

                if (mssCongelation != null) {
                    val newTask = createTaskMss(it)
                    newTask?.let { task ->
                        val dateMss = dateAppliedMss(it, mssCongelation)
                        createAntecedent(it, task, dateMss)
                        createTaskCompany(it, task)
                    }
                }

                isReportable = false
            }

            // Copy Visit Inper
            if (it.codigoRazon == REASON_RAVI_INPER) {
                visit = it.copy(idEstado = ID_STATUS_EJECUTADA, idResultado = ID_RESULT_EJECUTADA)
                //Copy Visit
                visitUseCase.copyVisitInper(it.id.orEmpty())
                //InActive Tarea
                inActiveTask(visit)
                //NOTIFICAR CNE
                notificationUseCase.sendNotification(
                    it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNE
                )
                //NOTIFICAR CNSCPOCPCHIP
                notificationUseCase.sendNotification(
                    it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNSCPOCPCHIP
                )
                //NOTIFICAR CNAPFM024
                notificationUseCase.sendNotification(
                    it.id.orEmpty().toString(), TYPE_NOTIFICATION_CNAPFM024
                )
            }
        }

// ****************** Sync Download ****************** //
        val tasksResult = taskUseCase.loadTask()
        return if (tasksResult.isSuccess) {
            val taskId = tasksResult.getOrNull().orEmpty().map { it.idTarea }.toSet()
            // Sync Download Visit
            val visitResult = taskId.map { visitUseCase.loadVisitas(it.toString()) }
            val visits = visitResult.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()

            // Horas Inspeccion permanente
            val scheduleResult = visits.filter { it.tramite != null }
                .map { visitUseCase.loadSchedule(it.tramite?.idTramite.orEmpty()) }

            // Obtener jornadas, festivos y tipos de hora
            var holidaysResult = visitUseCase.loadHolidays()
            var shiftTypeResult = visitUseCase.loadShiftType()
            var typeHoursBillingResult = visitUseCase.loadTypeHoursBilling()

            // Sync Download Relation-Visit
            val antecedentesResult = visits.map { visitUseCase.loadAntecedent(it.id.toString()) }
            val historicResult = visits.filter { it.empresa != null }
                .map { historicUseCase.loadHistorico(it.empresa?.razonSocial.orEmpty()) }
            val historicMssResult = visits.filter { it.empresa != null }
                .map { historicMssUseCase.loadHistoricoMss(it.empresa?.razonSocial.orEmpty()) }
            val sampleResult = visits.map { visitUseCase.loadSamples(it.id.toString()) }
            val officialResult = visits.map { visitUseCase.loadOfficial(it.id.toString()) }
            val transportResult = visits.map { transportUseCase.loadTransport(it.id.toString()) }
            val getAlltransportResult =
                visits.map { transportUseCase.getAllTransport(it.id.toString()) }
            val documentHistoricResult =
                historicResult.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
                    .map { documentUseCase.loadDocument(it.idVisita.toString()) }
            val documentResult = visits.map { documentUseCase.loadDocument(it.id.toString()) }
            val notificationResult = visits.map { notificationUseCase.getEmails(it.id.toString()) }
            val groupResult = visits.map { visitUseCase.loadGroup(it.id.toString()) }
            val subgroupResult = visits.map { visitUseCase.loadSubGroup(it.id.toString()) }
            val mssTypes = Types.MSS.type
            val typeMssResult = mssTypes.map { mss ->
                getIdTipoProductos().mapNotNull {
                    manageMssUseCase.loadTypeMss(mss, it)
                }
            }
            val rolResult = getIdTipoProductos().mapNotNull {
                manageMssUseCase.loadRol(ROL_PERSONA, it)
            }
            val typeProductResult = manageMssUseCase.loadTypeProduct(CD_TYPE_PRODUCT)

            val msspAppliedResult = visits.map { manageMssUseCase.loadMssApplied(it.id.toString()) }

            val razonSocials = visits.map { it.empresa?.razonSocial }.orEmpty().distinct()
            val msspListResult = razonSocials.mapNotNull { razonSocial ->
                val mssListTypes = Types.MSSLIST.type
                mssListTypes.map { type ->
                    razonSocial?.let {
                        manageMssUseCase.loadListMss(it, type)
                    }
                }
            }
            val ivcResult = visits.map { modeloIvcUseCase.getRequerimientos(it.id.toString()) }

            val ivcDaResult = visits.map { modeloIvcUseCase.getReqDa(it.id.toString()) }

            val technicalResult = visits.map { technicalUseCase.getTechnicals(it.id) }

            val interactionResult = visits.map { visitUseCase.loadInteraction(it.id.toString()) }

            val productInteractionResult = visits.filter { it.empresa != null }
                .map { technicalUseCase.getCertificate(it.empresa?.numDocumento.orEmpty()) }

            val equipmentResult = visits.filter { it.empresa != null }
                .map { equipmentUseCase.loadEquipment(it.empresa?.idSede.orEmpty()) }

            val daoTypes = Types.DAO.type
            val typeDaoResult = daoTypes.map { transversalDaoUseCase.loadTypeDao(it) }

            val countriesResult = transversalDaoUseCase.loadCountries()

            val departmentResult = transversalDaoUseCase.loadDepartment()

            val townResult = transversalDaoUseCase.loadTown()

            SyncTaskDataResult(
                userInformationResult,
                tasksResult,
                locationResult,
                companyRegisterResult,
                updateCertificateResult,
                updateAntecedent,
                updateTechnical,
                updateInteraction,
                updateObservation,
                visitCopyResult,
                visitResult,
                decomisosResult,
                documentsResult,
                emailsResult,
                insertMssApplied,
                updateMsspResult,
                insertMsseResult,
                insertIvc,
                deleteIvc,
                insertIvcDa,
                insertEquipmentResult,
                updateEquipmentResult,
                antecedentesResult,
                updateScheduleResult,
                scheduleResult,
                historicResult,
                historicMssResult,
                sampleResult,
                officialResult,
                transportResult,
                getAlltransportResult,
                documentHistoricResult,
                documentResult,
                notificationResult,
                groupResult,
                subgroupResult,
                typeMssResult,
                rolResult,
                typeProductResult,
                msspListResult,
                msspAppliedResult,
                ivcResult,
                ivcDaResult,
                technicalResult,
                interactionResult,
                productInteractionResult,
                equipmentResult,
                typeDaoResult,
                countriesResult,
                departmentResult,
                townResult,
                holidaysResult,
                shiftTypeResult,
                typeHoursBillingResult
            )
        } else SyncTaskDataResult(userInformationResult, tasksResult)
    }

    private suspend fun getIdTipoProductos(): List<Int> {
/*        return if (userUseCase.getUserMisional() == ID_MISIONAL_SANITARIAS)
            taskUseCase.loadTasksLocal().getOrNull()?.map { it.idTipoProducto }.orEmpty().distinct()
        else listOf(userUseCase.getUserMisional().orEmpty())*/
        return taskUseCase.loadTasksLocal().getOrNull()?.map { it.idTipoProducto }.orEmpty()
            .distinct()
    }

    private suspend fun inActiveTask(visit: VisitEntity) {
        if (visit.idTarea != null) {
            val result = taskUseCase.loadTaskLocal(visit.idTarea).getOrNull()
            val body = result?.let {
                TaskBodyEntity(
                    idTarea = it.idTarea,
                    activo = false,
                    codigoActividad = it.codigoActividad,
                    codigoEstado = it.codigoEstado,
                    codigoOrigen = it.codigoOrigen,
                    usuarioModifica = userUseCase.getUserName()
                )
            }
            if (body != null) taskUseCase.updateTask(body)
        }
    }

    private suspend fun updateCodeTask(visit: VisitEntity) {
        if (visit.idTarea != null) {
            val result = taskUseCase.loadTaskLocal(visit.idTarea).getOrNull()
            val body = result?.let {
                TaskBodyEntity(
                    idTarea = it.idTarea,
                    activo = true,
                    codigoActividad = CODE_ACTIVITY_TACT_GERE, //Gestion de resultados
                    codigoEstado = CODE_STATUS_ESTA_PEDI,
                    codigoOrigen = it.codigoOrigen,
                    usuarioModifica = userUseCase.getUserName()
                )
            }
            if (body != null) taskUseCase.updateTask(body)
        }
    }

    private suspend fun createTaskMss(visit: VisitEntity): TaskEntity? {
        val result = visit.idTarea?.let { taskUseCase.loadTaskLocal(it).getOrNull() }
        val body = result?.let {
            TaskMssBodyEntity(
                idActividadIVC = "7",
                idOrigenTarea = "17",
                idEstadoTarea = "3",
                idTipoProducto = it.idTipoProducto,
                usuarioCrea = userUseCase.getUserName()
            )
        }
        val resultPost = body?.let { taskUseCase.createTask(it).getOrNull() }
        return resultPost
    }

    private suspend fun createAntecedent(
        visit: VisitEntity,
        task: TaskEntity,
        dateMss: String
    ) {
        visitUseCase.createAntecedent(
            AntecedentBodyEntity(
                idTarea = task.idTarea,
                idTipo = 9,
                referencia = "MSS Congelación definición: " + dateMss,
                numeroAsociado = visit.numeroVisita,
                usuarioCrea = userUseCase.getUserName()
            )
        )
    }

    private suspend fun dateAppliedMss(visit: VisitEntity, mss: ManageAmssEntity): String {
        return manageMssUseCase.loadDateMssApplied(visit.id, mss).getOrDefault(EMPTY_STRING)
    }

    private suspend fun createTaskCompany(visit: VisitEntity, task: TaskEntity) {
        taskUseCase.createTaskCompany(
            TaskCompanyBodyEntity(
                idSedeEmpreTarea = visit.empresa?.idSede.orEmpty(),
                idTarea = task.idTarea,
                usuarioCrea = userUseCase.getUserName()
            )
        )
    }

    private suspend fun loadBlockValue() {
        val taskId = taskUseCase.loadTasksLocal().getOrDefault(emptyList()).map { it.idTarea }
        val visits = taskId.mapNotNull { visitUseCase.loadVisitLocal(it).getOrNull() }.flatten()
        visits.onEach { visit ->
            val plantillas = plantillasUseCase.loadAllPlantillasLocal().getOrDefault(emptyList())
            plantillas.onEach { plantilla ->
                val values = actUseCase.getBlockValue(plantilla.id.toString(), visit.id.toString())
                values.getOrNull()?.onEach { block ->
                    block.atributosPlantilla?.onEach { blockAtribute ->
                        actUseCase.updateBlockValue(
                            plantilla.id.toString(),
                            visit.id.toString(),
                            block.id.toString(),
                            blockAtribute.id.toString(),
                            blockAtribute.valor,
                            blockAtribute.idTipoAtributo
                        )
                    }
                }
            }
        }
    }

    private suspend fun loadBlockValuePapf() {
        taskPapfUseCase.loadTasksLocal().getOrDefault(emptyList()).mapNotNull {
            val plantillas = plantillasUseCase.loadAllPlantillasLocal().getOrDefault(emptyList())
            plantillas.onEach { plantilla ->
                val values =
                    actUseCase.getBlockValuePapf(plantilla.id.toString(), it.idSolicitud.toString())
                values.getOrNull()?.onEach { block ->
                    block.atributosPlantilla?.onEach { blockAtribute ->
                        actUseCase.updateBlockValue(
                            plantilla.id.toString(),
                            it.idSolicitud.toString(),
                            block.id.toString(),
                            blockAtribute.id.toString(),
                            blockAtribute.valor,
                            blockAtribute.idTipoAtributo
                        )
                    }
                }
            }
        }
    }

    //Update De la fecha de sincronizacion con formateo
    private fun updateSyncDate() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            preferencesRepository.setString(
                SYNC_DATE_KEY, LocalDateTime.now().format(
                    DateTimeFormatter.ofPattern("dd 'de' MMMM 'de' yyyy 'a las' HH:mm")
                )
            )
        }
        _syncDate.value = preferencesRepository.getString(SYNC_DATE_KEY, "")
    }

    private suspend fun mapBlockBody(
        idPlantilla: String, idVisita: String, map: Map<String, String>, id: Int
    ) {
        val blocks = plantillasUseCase.loadBlocksLocal(idPlantilla).getOrNull().orEmpty()
        val atributes = blocks.mapNotNull { it.atributosPlantilla }.flatten()
        val idPlantilla = blocks.firstOrNull()?.idPlantilla?.toString().orEmpty()
        val body = map.mapNotNull { data ->
            val block = atributes.find { it.nombre == data.key }
            var newValue: Any? = data.value
            when (block?.idTipoAtributo) {
                FIRMA -> {
                    try {
                        newValue = getBase64FromPath(data.value).orEmpty()
                    } catch (e: Exception) {
                    }
                }
                LISTA_TABLA -> {
                    val newData =
                        GsonBuilder().create()
                            .fromJson<ArrayList<ArrayList<String>>>(data.value, object :
                                TypeToken<ArrayList<ArrayList<String>>>() {}.type)
                    val dataItems = newData
                    newValue = dataItems
                }
                FOTO -> {
                    try {
                        val photos = data.value.replace("[", "").replace("]", "").split(", ")
                        val list: ArrayList<String> = arrayListOf()
                        photos.filter { it != "" }.mapNotNull {
                            getBase64FromPath(it)?.let { item -> list.add(item) }
                        }
                        newValue = list
                    } catch (e: Exception) {
                    }
                }
                CHECK_LIST -> {
                    try {
                        val data = data.value.replace("[", "").replace("]", "")
                        newValue = data
                    } catch (e: Exception) {
                    }
                }
            }
            block?.let { BlockSaveEntity(block, newValue) }
        }
        val plantilla =
            plantillasUseCase.loadPlantillasLocal(idPlantilla).getOrNull()?.firstOrNull()
        val idTipoDocumental =
            manageUseCase.loadRulesBussinessLocal(idPlantilla).getOrNull()?.idTipoDocumental
        saveBlock(plantilla, idVisita.orEmpty(), idTipoDocumental ?: 0, body, id)
    }

    private suspend fun saveBlock(
        plantilla: PlantillaEntity?,
        idVisita: String,
        idTipoDocumental: Int,
        block: List<BlockSaveEntity>,
        id: Int
    ) {
        val body = BlockSaveBodyEntity(
            documento = BlockDocumentSaveEntity(
                nombre = plantilla?.nombre.orEmpty(),
                titulo = plantilla?.nombre.orEmpty(),
                usuario = userUseCase.getUserName(),
                fecha = getDateNow(),
                idTipoDocumental = idTipoDocumental
            ), bloques = block
        )
        val result = actUseCase.saveBlock(plantilla?.id.toString(), idVisita, body)
        val map = result.map { it.bloques.map { it.nombre.orEmpty() to it.valor }.toMap() }
            .getOrNull().orEmpty()
        setPdfView(plantilla, idVisita.toIntOrNull()?:0, false, map)
        if (result.isSuccess) {
            plantillasUseCase.deleteBlockBody(plantilla?.id.toString(), idVisita, id)
        }
    }

    private suspend fun setPdfView(
        plantilla: PlantillaEntity?,
        idVisita: Int,
        indVistaPrevia: Boolean,
        map: Map<String, Any?>,
    ) {
        val resultPdf = if (userUseCase.userIsPapf()) {
            actUseCase.getPdfPapf(plantilla?.id.toString(), idVisita, indVistaPrevia, map)
        } else {
            val visita = visitUseCase.loadVisitLocalById(idVisita).getOrNull()
            visita?.let { actUseCase.getPdf(plantilla?.id.toString(), it, indVistaPrevia, map) }
        }
        if (resultPdf?.isSuccess == true) {
            if (userUseCase.userIsPapf()) {
                val clasificationTramit = papfUseCase.loadDataCloseInsLocal(idVisita).getOrNull()
                val data = SaveInfoActsPapfEntity(
                    idPlantilla = plantilla?.id,
                    idSolicitud = idVisita.toString(),
                    expedicionCIS = "",
                    otros = "",
                    cual = "",
                    consecutivo = papfUseCase.selectConsecutivePapfById(idVisita).getOrNull(),
                    fechaRegistro = "",
                    funcionarioInvima = userUseCase.fullNameOrdered(),
                    fechaInicio = getDateNow(),
                    fechaFin = getDateNow(),
                    tramiteRequerimiento = if (clasificationTramit?.respuestaRequerimientoIF == true) 1 else 0,
                    descripcionRequerimiento = "",
                    nombreUsuario = userUseCase.fullNameOrdered(),
                    nombreLaboratorio = ""
                )
                papfUseCase.saveActsRequest(data)
            }
        }
    }

    suspend fun syncPending(): Int {
        val countTransport = transportUseCase.getTransportUpload().getOrNull() ?: 0
        val countDocument = documentUseCase.getDocumentUpload().getOrNull() ?: 0
        val countMss = manageMssUseCase.getmssUpload().getOrNull() ?: 0
        val countIvc = modeloIvcUseCase.getCountIvcReq().getOrNull() ?: 0
        val result = countTransport + countDocument + countMss + countIvc
        return result
    }

    suspend fun syncPapfPending(): Int {
        return papfUseCase.getCountPapfLocal().getOrNull() ?: 0
    }

    // Obtener lo relacionado a papf
    private suspend fun loadDataPapf(): SyncPapfResult {
        val userInformationResult =
            userUseCase.getUser()?.let { userUseCase.updateUserInformation(it.preferredUsername) }

// ****************** Sync Upload ****************** //
        val taskLocal = taskPapfUseCase.loadTasksLocal()
        var idSolicitud = taskLocal.getOrNull().orEmpty().mapNotNull { it.idSolicitud }

        //Sync Upload Location
        val locationResult = visitUseCase.syncLocation()

        //Sync Save Date Register Inspection
        val dateRegisterResult = idSolicitud.mapNotNull { papfUseCase.saveRegisterDate(it) }

        //Sync Send Notification Email
        val notificationResult = idSolicitud.mapNotNull { papfUseCase.sendNotification(it) }

        //Sync Save Acta Sanitary
        val dataInspSanitaryResult = idSolicitud.mapNotNull { papfUseCase.saveDataReqInsSan(it) }

        //Sync Save Acta Sanitary
        val dataActSampleResult = idSolicitud.mapNotNull { papfUseCase.saveDataReqActSample(it) }

        // Sync Upload Sync Block
        plantillasUseCase.getAllBlockBody().onEach {
            val body = try {
                Gson().fromJson(it.body, HashMap<String, String>().javaClass).orEmpty()
            } catch (e: Exception) {
                mapOf<String, String>()
            }
            mapBlockBody(it.idPlantilla, it.idVisita, body, it.id)
        }

        //Sync Close Inspection
        val closeInspectionResult = idSolicitud.mapNotNull { papfUseCase.saveCloseInspection(it) }

        val closeEmitResult = idSolicitud.mapNotNull { papfUseCase.saveEmit(it) }

// ****************** Sync Download ****************** //
        val tasksResult = taskPapfUseCase.loadTask()
        return if (tasksResult.isSuccess) {
            val idSolicitud =
                tasksResult.getOrNull().orEmpty().mapNotNull { it.idSolicitud }.distinct()
            val infoTramitResult = idSolicitud.mapNotNull { papfUseCase.loadInfoTramit(it) }
            val tramit = infoTramitResult.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
            val consecutivePapf = tramit.mapNotNull { papfUseCase.loadConsecutivePapf(it.idOficinaInvima, it.idSolicitud) }
            val invoiceResult = idSolicitud.mapNotNull { papfUseCase.loadInvoice(it) }
            val transportResult = idSolicitud.mapNotNull { papfUseCase.loadTransport(it) }
            val registerProductResult =
                idSolicitud.mapNotNull { papfUseCase.loadRegisterProduct(it) }
            val sanitaryCertificateResult =
                idSolicitud.mapNotNull { papfUseCase.loadSanitaryCertificate(it) }
            val documentationResult = idSolicitud.mapNotNull { papfUseCase.loadDocumentation(it) }
            val dateResult = idSolicitud.mapNotNull { papfUseCase.loadRegisterDate(it) }
            val emailNotificationResult =
                idSolicitud.mapNotNull { papfUseCase.loadEmailNotification(it) }
            val dinamicQuerys = Types.CDPAPF.type
            val dinamicQuerysResult = dinamicQuerys.map { papfUseCase.loadDinamicQuerys(it) }
            val dataBasicResult = idSolicitud.mapNotNull { papfUseCase.loadDataCloseIns(it) }

            val emitCisResult = idSolicitud.mapNotNull { papfUseCase.loadDataEmitCis(it) }
            val infoEmitirResult = idSolicitud.mapNotNull { papfUseCase.loadInfoEmitir(it) }
            val infoCheckResult = idSolicitud.mapNotNull { papfUseCase.loadInfoCheck(it) }

            SyncPapfResult(
                userInformationResult,
                tasksResult,
                locationResult,
                dateRegisterResult,
                notificationResult,
                dataInspSanitaryResult,
                dataActSampleResult,
                closeInspectionResult,
                closeEmitResult,
                infoTramitResult,
                consecutivePapf,
                invoiceResult,
                transportResult,
                registerProductResult,
                sanitaryCertificateResult,
                documentationResult,
                dateResult,
                emailNotificationResult,
                dinamicQuerysResult,
                dataBasicResult,
                emitCisResult,
                infoEmitirResult,
                infoCheckResult
            )

        } else {
            SyncPapfResult(userInformationResult, tasksResult)
        }
    }

}

data class SyncDataResult(
    val plantillas: List<SyncPlantillaDataResult>,
    val tasks: SyncTaskDataResult,
)

data class SyncPlantillaDataResult(
    val plantillasResult: Result<List<PlantillaEntity>>,
    val groupDocumentResult: Result<List<GroupDocumentEntity>>,
    val documentNameResult: List<Result<List<DocumentNameEntity>>>? = emptyList(),
    val blockResult: List<Result<List<BlockEntity>>> = emptyList(),
    val groupResult: Result<List<GroupEntity>>? = null,
    val actResult: List<Result<List<RulesBussinessEntity>?>> = emptyList(),
)

data class SyncTaskDataResult(
    val userInformationResult: Result<UserInformationEntity>?,
    val tasksResult: Result<List<TaskEntity>>,
    val locationResult: Result<Unit>? = null,
    val companyRegisterResult: List<Result<Unit>> = emptyList(),
    val updateCertificateResult: Result<List<Unit>>? = null,
    val updateAntecedent: Result<Unit>? = null,
    val updateTechnical: Result<Unit>? = null,
    val updateInteraction: List<Result<Unit>> = emptyList(),
    val updateObservation: List<Result<Unit?>> = emptyList(),
    val visitCopyResult: List<Unit> = emptyList(),
    val visitResult: List<Result<List<VisitEntity>>> = emptyList(),
    val decomisosResult: List<Result<Unit>> = emptyList(),
    val documentsResult: List<Result<Unit>> = emptyList(),
    val emailsResult: List<Unit> = emptyList(),
    val insertMssApplied: List<Result<Unit>> = emptyList(),
    val updateMsspResult: Result<List<Unit>>? = null,
    val insertMsseResult: List<Result<List<Unit>>>? = null,
    val insertIvc: Result<Unit>? = null,
    val deleteIvc: Result<Unit>? = null,
    val insertIvcDa: Result<Unit>? = null,
    val insertEquipmentResult: Result<List<Unit>>? = null,
    val updateEquipmentResult: Result<List<Unit>>? = null,
    val antecedentesResult: List<Result<List<AntecedentEntity>>> = emptyList(),
    val updateScheduleResult: Result<List<Unit>>? = null,
    val scheduleResult: List<Result<List<ScheduleEntity>?>> = emptyList(),
    val historicResult: List<Result<List<HistoricEntity>>> = emptyList(),
    val historicMssResult: List<Result<HistoricMssEntity>> = emptyList(),
    val sampleResult: List<Result<List<SampleEntity>>> = emptyList(),
    val officialResult: List<Result<List<VisitOfficialEntity>>> = emptyList(),
    val transportResult: List<Result<TransportEntity>> = emptyList(),
    val getAlltransportResult: List<Result<List<TransportBodyEntity>>> = emptyList(),
    val documentHistoricResult: List<Result<List<DocumentEntity>>> = emptyList(),
    val documentResult: List<Result<List<DocumentEntity>>> = emptyList(),
    val notificationResult: List<Result<List<NotificationEntity>>> = emptyList(),
    val groupResult: List<Result<List<VisitGroupEntity>>> = emptyList(),
    val subgroupResult: List<Result<List<VisitGroupEntity>>> = emptyList(),
    val typeMssResult: List<List<Result<List<ManageMssEntity>?>>> = emptyList(),
    val rolResult: List<Result<List<RolEntity>?>> = emptyList(),
    val typeProductResult: Result<List<TypeProductEntity>?>? = null,
    val msspListResult: List<List<Result<List<ManageMsspEntity>>?>> = emptyList(),
    val msspAppliedResult: List<Result<List<MssAppliedEntity>>> = emptyList(),
    val ivcResult: List<Result<List<ModeloIvcEntity>?>> = emptyList(),
    val ivcDaResult: List<Result<List<IvcDaEntity>>> = emptyList(),
    val technicalResult: List<Result<List<TechnicalEntity>>> = emptyList(),
    val interactionResult: List<Result<InteractionEntity>> = emptyList(),
    val productInteractionResult: List<Result<List<CertificateEntity>>> = emptyList(),
    val equipmentResult: List<Result<List<EquipmentEntity>>> = emptyList(),
    val typeDaoResult: List<Result<List<TypeDaoEntity>?>> = emptyList(),
    val countriesResult: Result<List<CountriesEntity>?>? = null,
    val departmentResult: Result<List<DepartmentEntity>?>? = null,
    val townResult: Result<List<TownEntity>?>? = null,
    val holidaysResult: Result<List<HolidaysEntity>?>? = null,
    val shiftTypeResult: Result<List<ShiftTypeEntity>?>? = null,
    val typeHoursBillingResult: Result<List<TypeHoursBillingEntity>?>? = null,
)

data class SyncPapfDataResult(
    val plantillas: SyncPapfPlantillaDataResult,
    val papf: SyncPapfResult,
)

data class SyncPapfPlantillaDataResult(
    val plantillasResult: List<Result<List<RulesBussinessEntity>?>> = emptyList(),
    val blockResult: List<Result<List<BlockEntity>>> = emptyList(),
    val resultGroup: Result<List<GroupEntity>>? = null,
    val documentNameResult: List<Result<List<DocumentNameEntity>>> = emptyList(),
)

data class SyncPapfResult(
    val userInformationResult: Result<UserInformationEntity>?,
    val tasksResult: Result<List<TaskPapfEntity>>,
    val locationResult: Result<Unit>? = null,
    val dateRegisterResult: List<Result<Unit>> = emptyList(),
    val notificationResult: List<Result<Unit>> = emptyList(),
    val dataInspSanitaryResult: List<Result<Unit>> = emptyList(),
    val dataActSampleResult: List<Result<Unit>> = emptyList(),
    val closeInspectionResult: List<Result<Unit>> = emptyList(),
    val closeEmitResult: List<Result<Unit>> = emptyList(),

    val infoTramitResult: List<Result<List<InfoTramitePapfEntity>?>> = emptyList(),
    val consecutivePapf: List<Result<ConsecutivePapfEntity?>> = emptyList(),
    val invoiceResult: List<Result<List<InvoicePapfEntity>?>> = emptyList(),
    val transportResult: List<Result<TransportObjectPapfEntity?>> = emptyList(),
    val registerProductResult: List<Result<RegisterProductObjectPapfEntity?>> = emptyList(),
    val sanitaryCertificateResult: List<Result<List<SanitaryCertificatePapfEntity>?>> = emptyList(),
    val documentationResult: List<Result<List<DocumentationPapfEntity>?>> = emptyList(),
    val dateResult: List<Result<List<RegisterDateBodyEntity>?>> = emptyList(),
    val emailNotificationResult: List<Result<List<InfoNotificationPapfEntity>?>> = emptyList(),
    val dinamicQuerysResult: List<Result<Unit>> = emptyList(),
    val dataBasicResult: List<Result<ClasificationTramitPapfEntity?>> = emptyList(),
    val emitCisResult: List<Result<EmitCisPapfEntity?>?> = emptyList(),
    val infoEmitirResult: List<Result<InfoEmitirPapfEntity?>?> = emptyList(),
    val infoCheckResult: List<Result<CheckListEntity?>?> = emptyList(),
)
