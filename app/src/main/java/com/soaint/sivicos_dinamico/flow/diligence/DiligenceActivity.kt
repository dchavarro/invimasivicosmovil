package com.soaint.sivicos_dinamico.flow.diligence

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.core.view.isVisible
import com.soaint.data.store.PreferencesManager
import com.soaint.domain.model.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityDiligenceBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.flow.act.ActActivity
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.visit.views.equipment.EquipmentActivity
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding
import kotlinx.coroutines.runBlocking

const val KEY_ID = "KEY_ID"

class DiligenceActivity : BaseActivity() {

    companion object {
        fun getStartActivityEvent(
            id: Int? = null,
        ) = StartActivityEvent(
            DiligenceActivity::class.java,
            Bundle().apply { id?.let { putInt(KEY_ID, it) } }
        )
    }

    private val binding by activityBinding<ActivityDiligenceBinding>(R.layout.activity_diligence)

    private val viewModel by viewModels<DiligenceViewModel> { viewModelFactory }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.root
        initView()
        observerViewModelEvents()
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = getString(R.string.diligenciar_acta)
        isVisibleActa(false)
        binding.buttonActa.isEnabled = false
        binding.buttonActa.setOnSingleClickListener { viewModel.validateDilgenceActa() }
        binding.buttonEquipo.setOnSingleClickListener {
            viewModel.visitData.value?.let {
                startActivity(EquipmentActivity.getStartActivityEvent(it))
            }
        }
    }

    private fun isVisibleActa(visible: Boolean) {
        binding.textViewTitleActa.isVisible = visible
        binding.spinnerActa.isVisible = visible
    }

    private fun observerViewModelEvents() {
        val id: Int = intent.getIntExtra(KEY_ID, 0)
        viewModel.event.observe(this, ::eventHandler)
        viewModel.count.observe(this, ::countQuerys)
        viewModel.init(id)
    }

    private fun eventHandler(event: OnManageViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnManageViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnManageViewModelEvent.OnLoadingError -> onError()
            is OnManageViewModelEvent.OnViewModelGroupReady -> OnViewModelGroupReady(event.group)
            is OnManageViewModelEvent.OnViewModelPlantillaReady -> OnPlantillaReady(event.plantilla)
            is OnManageViewModelEvent.OnViewModelBlockReady -> OnBlockReady(
                event.idVisita,
                event.plantilla,
                event.block,
                event.idTipoDocumental
            )
            is OnManageViewModelEvent.onViewModelReadyAct076 -> onViewModelReadyAct076(event.isValid)
            is OnManageViewModelEvent.OnValidateDiligenceActaReady -> OnValidateDiligenceActa()

        }
    }

    private fun OnValidateDiligenceActa() {
        showAlertDialog(
            showDialog(
                getString(R.string.adv),
                getString(R.string.copy_adv_acta_diligence),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun countQuerys(count: Int) {
        if (count > 0) showSnackBar(count) {
            startActivity(MainActivity::class.java); finish()
        }
    }

    private fun onError() {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun OnViewModelGroupReady(group: List<GroupEntity>) {
        val array = group.toMutableList()
        array.add(0, GroupEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerGroup.adapter =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        binding.spinnerGroup.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setGroupSelected(itemSelected)
                    isVisibleActa(true)
                } else {
                    isVisibleActa(false)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnPlantillaReady(plantilla: List<RulesBussinessEntity>) {
        val items = plantilla.toMutableList().mapNotNull { it.nombre }

        binding.spinnerActa.item = items
        binding.spinnerActa.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                binding.buttonActa.isEnabled = true
                viewModel.setRulesBussinessSelected(items[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnBlockReady(
        id: Int?,
        plantilla: List<PlantillaEntity>,
        blocks: List<BlockEntity>,
        idTipoDocumental: Int
    ) {
        if (idTipoDocumental != 0 && id != null) {
            val intent = Intent(this, ActActivity::class.java)
          /*  val bundle = Bundle().apply {
                putInt("idVisita", id)
                putParcelableArrayList("plantilla", ArrayList(plantilla))
                putParcelableArrayList("bloques", ArrayList(blocks))
                putInt("idTipoDocumental", idTipoDocumental)
            }
            intent.putExtras(bundle)*/
            val preferencesManager = PreferencesManager(this)
            runBlocking {
                preferencesManager.saveData(id, ArrayList(plantilla), ArrayList(blocks), idTipoDocumental)
            }
            startActivity(intent)
        } else {
            onError()
        }
    }

    fun onViewModelReadyAct076(isValid: List<String>) {
        if (!isValid.isNullOrEmpty()) {
            val data = isValid.toString().replace("[", "").replace("]", "")
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_act076_error, data),
                    titleBtnPositive = getString(R.string.cerrar)
                )
                { viewModel.clean(); binding.buttonActa.isEnabled = false }
            )
        } else {
            viewModel.onLoadBlock()
        }
    }

    override fun onResume() {
        super.onResume()
        showLoading(false)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}