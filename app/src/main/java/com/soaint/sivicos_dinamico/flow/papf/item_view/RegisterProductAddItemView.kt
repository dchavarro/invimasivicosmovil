package com.soaint.sivicos_dinamico.flow.papf.item_view

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.RegisterProductPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemRegisterProductAddBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class RegisterProductAddItemView(
    private val item: RegisterProductPapfEntity,
    private val onSample: (RegisterProductPapfEntity) -> Unit,
    private val onView: (RegisterProductPapfEntity) -> Unit,
    private val onEdit: (RegisterProductPapfEntity) -> Unit,
    private val isTaskCis: Boolean
) : BindableItem<ItemRegisterProductAddBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemRegisterProductAddBinding,
        position: Int
    ) = with(viewBinding) {

        textViewClassification.text = item.clasificacionProducto.orEmpty()
        textViewProduct.text = item.producto.orEmpty()
        textViewRs.text = item.registroSanitario.orEmpty()
        textViewOrigin.text = item.origenFabricante.orEmpty()
        textViewLote.text = item.lote.orEmpty()
        textViewQuantity.text = item.cantidad.orEmpty()
        textViewPresentation.text = item.presentacionComercial.orEmpty()

        textViewPeso.text = item.pesoUnidadMedida.orEmpty().toString()
        textViewUnity.text = item.unidadDeMedida.orEmpty()
        textViewNeto.text = item.pesoNeto.orEmpty()
        textViewMarca.text = item.marca.orEmpty()
        textViewTemp.text = item.temperaturaConservacion.orEmpty()
        textViewDate.text = item.fechaVencimiento?.formatDate()

        buttonEdit.isVisible = !isTaskCis
        buttonSample.isVisible = !isTaskCis

        buttonEdit.setOnSingleClickListener { onEdit.invoke(item) }
        buttonView.setOnSingleClickListener { onView.invoke(item) }
        buttonSample.setOnSingleClickListener { onSample.invoke(item) }
    }

    override fun getLayout() = R.layout.item_register_product_add
}