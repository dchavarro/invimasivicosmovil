package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.company

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidbuts.multispinnerfilter.KeyPairBoolData
import com.androidbuts.multispinnerfilter.MultiSpinnerListener
import com.soaint.data.common.orZero
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentManageMssCompanyBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.MsspListItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ID_CODE_MSS_SUSPENSION_PARCIAL_SERVICIO
import com.soaint.sivicos_dinamico.utils.ID_CODE_MSS_SUSPENSION_PARCIAL_TRABAJO
import com.soaint.sivicos_dinamico.utils.ID_CODE_MSS_SUSPENSION_TOTAL_SERVICIO
import com.soaint.sivicos_dinamico.utils.ID_CODE_MSS_SUSPENSION_TOTAL_TRABAJO
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class MssCompanyFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentManageMssCompanyBinding>(R.layout.fragment_manage_mss_company)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: MssCompanyViewModel by viewModels { viewModelFactory }

    private val adapterCheck by lazy { GroupieAdapter() }

    private val adapterList by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_manage_mss_product_title_companny))

        adapterCheck.spanCount = 2
        binding.rvCheck.layoutManager =
            GridLayoutManager(context, adapterCheck.spanCount).apply {
                spanSizeLookup = adapterCheck.spanSizeLookup
            }
        binding.rvCheck.adapter = adapterCheck

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterList

        binding.spinnerMultiple.hintText = getString(R.string.copy_option)
        binding.spinnerMultiple.setClearText(getString(R.string.copy_clear))
        binding.spinnerMultiple.setSearchHint(getString(R.string.copy_search))

        binding.buttonUpdate.setOnSingleClickListener {
            viewModel.onUpdate()
        }
        binding.buttonAdd.setOnSingleClickListener {
            showAlertDialog(
                showDialog(
                    getString(R.string.adv),
                    getString(R.string.copy_nso_alert),
                    titleBtnPositive = getString(R.string.si),
                    titleBtnNegative = getString(R.string.no))
                { viewModel.onInsert() }
            )
        }
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.amsspSelected.observe(viewLifecycleOwner, ::showTypeActivity)
    }

    private fun eventHandler(event: onMsspViewModelEvent) {
        showLoading(false)
        when (event) {
            is onMsspViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onMsspViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onMsspViewModelEvent.onViewModelReadyEmssp -> onViewModelReadyEmssp(event.emssp)
            is onMsspViewModelEvent.onViewModelReadyAmssp -> onViewModelReadyAmssp(event.amssp)
            is onMsspViewModelEvent.onViewModelReadyMsspList -> onViewModelReadyMsspList(event.msspList)
            is onMsspViewModelEvent.onViewModelReadyMssActivity -> onViewModelReadyMssActivity(event.typeActivity)
            is onMsspViewModelEvent.onViewModelUpdateMsspList -> showSnack(true , R.string.copy_update_success)
            is onMsspViewModelEvent.onViewModelReadyAmsspCheck -> showSnack(true , R.string.copy_update_success)
            is onMsspViewModelEvent.onSnackLoadingError -> showSnack(false, event.message)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReadyAmssp(mssp: List<ManageMssEntity>) {
        adapterCheck.clear()
        val items = mutableListOf<Group>()

        if (!mssp.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_manage_mss_company)))
            items.addAll(mssp.map { MssItemView(it, ::onAmsspSelected) })
        }

        val isVisible = mssp.isNullOrEmpty()
        binding.rvCheck.isVisible = !isVisible
        adapterCheck.updateAsync(items)
    }

    private fun onAmsspSelected(mssp: ManageMssEntity, isSelected: Boolean) {
        viewModel.setAmsspSelected(mssp.codigo.orEmpty(), isSelected)
    }

    private fun onViewModelReadyMssActivity(mssp: List<ManageMssEntity>) {
        val listArray1: MutableList<KeyPairBoolData> = ArrayList()
        for (i in 0 until mssp.size) {
            val h = KeyPairBoolData()
            h.id = mssp.get(i).id?.toLong() ?: 0
            h.name = mssp.get(i).descripcion
            h.isSelected = mssp.get(i).isSelected
            listArray1.add(h)
        }

        binding.spinnerMultiple.setItems(listArray1,
            MultiSpinnerListener { items ->
                for (i in items.indices) {
                    if (items[i].isSelected) {
                        viewModel.setTypeActivitySelected(items[i].id.toInt())
                    }
                }
            })
    }

    private fun onViewModelReadyEmssp(mssp: List<ManageMssEntity>) {
        val array = mssp.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerEmssp.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerEmssp.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) viewModel.setEmsspSelected(itemSelected)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyMsspList(msspList: List<ManageMsspEntity>) {
        adapterList.clear()
        val items = mutableListOf<Group>()

        if (!msspList.isNullOrEmpty()) {
            items.addAll(msspList.map { MsspListItemView(it, ::onMsspListSelected, onChangeLote = ::onChangeLote) })
        }

        val isVisible = msspList.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
        adapterList.updateAsync(items)
    }

    private fun onMsspListSelected(msspList: ManageMsspEntity, isSelected: Boolean) {
        viewModel.setMsspSelected(msspList.id?.toInt()?: 0, isSelected)
    }

    private fun onChangeLote(id: Int, lote: String, isSelected: Boolean) { }

    private fun showTypeActivity(amsspSelected: ManageMssEntity?) {
        val isvisible = amsspSelected != null
                && (amsspSelected.codigo == ID_CODE_MSS_SUSPENSION_TOTAL_SERVICIO
                || amsspSelected.codigo == ID_CODE_MSS_SUSPENSION_PARCIAL_SERVICIO
                || amsspSelected.codigo == ID_CODE_MSS_SUSPENSION_TOTAL_TRABAJO
                || amsspSelected.codigo == ID_CODE_MSS_SUSPENSION_PARCIAL_TRABAJO)
                && amsspSelected.isSelected
        binding.spinnerMultiple.isVisible =  isvisible
        binding.textViewTitleActivity.isVisible =  isvisible
        if (!isvisible) { viewModel.setTypeActivitySelected(0) }
    }

}