package com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces

import android.content.Context
import android.view.View
import com.soaint.domain.model.BlockAtributePlantillaEntity

interface ControlInterface {
    fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)? = null,
    ): View

    fun getData(): String
    fun showError(mensaje: String?)
    fun clearError()
}