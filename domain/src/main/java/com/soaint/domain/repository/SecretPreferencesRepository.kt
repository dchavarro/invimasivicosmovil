package com.soaint.domain.repository

interface SecretPreferencesRepository {

    fun setString(preferenceKey: String, value: String)

    fun getString(preferenceKey: String, defaultValue: String = ""): String
}
