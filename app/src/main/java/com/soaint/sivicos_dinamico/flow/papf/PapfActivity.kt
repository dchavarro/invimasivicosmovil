package com.soaint.sivicos_dinamico.flow.papf

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityPapfBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.stopLocationService
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceActivity
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.papf.views.close_insp.OnViewModelEvent
import com.soaint.sivicos_dinamico.flow.splash.SplashActivity
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding
import com.soaint.sivicos_dinamico.utils.REQUEST_LOAD_TASK_PAPF

const val KEY_ID = "KEY_ID"

class PapfActivity : BaseActivity(), HideShowIconInterface {

    companion object {
        fun getStartActivityEvent(
            id: Int,
            code: Int = REQUEST_LOAD_TASK_PAPF
        ) = StartActivityEvent(
            PapfActivity::class.java,
            Bundle().apply {
                putInt(KEY_ID, id)
            },
            code = code
        )
    }

    private val viewModel by viewModels<PapfViewModel> { viewModelFactory }

    private val binding by activityBinding<ActivityPapfBinding>(R.layout.activity_papf)

    val actionBarDrawerToggle by lazy {
        object : ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(binding.toolbar)
        initView()
        observerViewModelEvents()
        binding.root
    }

    private fun initView() {
        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.drawerArrowDrawable.color = resources.getColor(R.color.white)
        actionBarDrawerToggle.syncState()
        binding.fragmentMenu.buttonInvoice.setOnSingleClickListener { openInfo() }
        binding.fragmentMenu.buttonTransport.setOnSingleClickListener { openTransport() }
        binding.fragmentMenu.buttonRegisterProduct.setOnSingleClickListener { openRegisterProduct() }
        binding.fragmentMenu.buttonCertificationConditional.setOnSingleClickListener { openCertificate() }
        binding.fragmentMenu.buttonDocuments.setOnSingleClickListener { openDocumentation() }
        binding.fragmentMenu.buttonRegisterDateInspectionPapf.setOnSingleClickListener { openRegisterDate() }
        binding.fragmentMenu.buttonCis.setOnSingleClickListener { openEmitirCis() }
        binding.fragmentMenu.buttonManage.setOnSingleClickListener { openDiligence() }
        binding.fragmentMenu.buttonClose.setOnSingleClickListener { viewModel.validateResponseRequestIF() }
        binding.fragmentMenu.buttonSync.setOnSingleClickListener { sync() }
        binding.fragmentMenu.buttonLogout.setOnSingleClickListener { logout() }
        binding.fragmentMenu.textViewVersion.text =
            getString(R.string.copy_version, BuildConfig.VERSION_NAME)
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(this, ::eventHandler)
        viewModel.init(intent.getIntExtra(KEY_ID, 0))
        viewModel.isTaskCis.observe(this, ::menuPapfCis)
    }

    private fun menuPapfCis(isVisible: Boolean) {
        binding.fragmentMenu.buttonCis.isVisible = isVisible
        binding.fragmentMenu.buttonRegisterDateInspectionPapf.isVisible = !isVisible
        binding.fragmentMenu.buttonManage.isVisible = !isVisible
        binding.fragmentMenu.buttonClose.isVisible = !isVisible
    }

    private fun eventHandler(event: OnPapfViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnPapfViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnPapfViewModelEvent.OnLoadingError -> onTokenError()
            is OnPapfViewModelEvent.OnViewModelReady -> onViewModelReady(event.infoTramit)
            is OnPapfViewModelEvent.OnViewModelValidateActs -> onViewModelValidateActs(event.acts)
            is OnPapfViewModelEvent.onViewResponseRequestIF -> onViewResponseRequestIF()

            else -> {}
        }
    }

    private fun onTokenError() {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { finish() }
        )
    }

    private fun onViewModelReady(info: InfoTramitePapfEntity) {}

    fun onViewModelValidateActs(acts: List<String>) {
        if (!acts.isNullOrEmpty()) {
            val data = acts.toString().replace("[", "").replace("]", "")
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_close_inspection_error, data),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                ) { openDiligence() }
            )
        } else {
            openClose()
        }
    }

    fun onViewResponseRequestIF() {
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_close_inspection_responseIF),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                ) {  closeMenu() }
            )
    }

    private fun openMenu() {
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeMenu() {
        binding.drawerLayout.closeDrawers()
    }

    private fun openInfo() {
        findNavController(binding.navHost.id).navigate(R.id.invoice_papf)
        closeMenu()
    }

    private fun openTransport() {
        findNavController(binding.navHost.id).navigate(R.id.transport_papf)
        closeMenu()
    }

    private fun openRegisterProduct() {
        findNavController(binding.navHost.id).navigate(R.id.register_products_papf)
        closeMenu()
    }

    private fun openCertificate() {
        findNavController(binding.navHost.id).navigate(R.id.certificate_papf)
        closeMenu()
    }

    private fun openDocumentation() {
        findNavController(binding.navHost.id).navigate(R.id.documentation_papf)
        closeMenu()
    }

    private fun openRegisterDate() {
        findNavController(binding.navHost.id).navigate(R.id.register_date_papf)
        closeMenu()
    }

    private fun openDiligence() {
        closeMenu()
        startActivity(DiligenceActivity.getStartActivityEvent(viewModel.idRequest.value))
    }

    private fun openClose() {
        findNavController(binding.navHost.id).navigate(R.id.close_inspection)
        closeMenu()
    }

    private fun openEmitirCis() {
        findNavController(binding.navHost.id).navigate(R.id.emit_cis)
        closeMenu()
    }

    private fun logout() {
        showAlertDialog(
            showDialog(
                getString(R.string.cerrar_sesion),
                getString(R.string.copy_logout),
                getString(R.string.si),
                getString(R.string.no),
            ) {
                viewModel.logout()
                stopLocationService()
                startClearStackActivity(this@PapfActivity, SplashActivity::class.java)
                finish()
            }
        )
    }

    private fun sync() {
        startClearStackActivity(this@PapfActivity, MainActivity::class.java)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        invalidateOptionsMenu()
        return when (item.getItemId()) {
            android.R.id.home -> {
                if (actionBarDrawerToggle.isDrawerIndicatorEnabled()) {
                    openMenu()
                } else {
                    onBackPressed()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                super.onBackPressed();
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun showHamburgerIcon() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true)
        supportActionBar!!.title = getString(R.string.app_name)
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    override fun showBackIcon(title: String?) {
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title =
            if (title.isNullOrEmpty()) getString(R.string.app_name) else title
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }
}