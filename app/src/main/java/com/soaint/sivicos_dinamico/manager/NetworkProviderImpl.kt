package com.soaint.sivicos_dinamico.manager

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import com.soaint.domain.provider.NetworkProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NetworkProviderImpl @Inject constructor(
    private val context: Context
) : NetworkProvider {

    override val isAvailable: Boolean
        get() {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
            val isAvailable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                connectivityManager?.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                        hasTransport(NetworkCapabilities.TRANSPORT_WIFI) ||
                                hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) ||
                                hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) ||
                                hasTransport(NetworkCapabilities.TRANSPORT_VPN)
                    }
            } else {
                connectivityManager?.activeNetworkInfo?.run {
                    type == ConnectivityManager.TYPE_WIFI || type == ConnectivityManager.TYPE_MOBILE
                }
            }
            return isAvailable == true
        }
}