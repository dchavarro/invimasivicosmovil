package com.soaint.sivicos_dinamico.flow.visit.views.close_visit

import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import javax.inject.Inject

class CloseViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
) : BaseViewModel() {

    private val _visit = MutableLiveData<VisitEntity>()
    val visit= _visit.asLiveData()
    private val _isVisitNTP = MutableLiveData<Boolean>(false)
    val isVisitNTP = _isVisitNTP.asLiveData()


    fun init(visitData: List<VisitEntity>) {
        _visit.value = visitData.firstOrNull()
        getIsVisitNTP(visitData.firstOrNull()?.idTarea)
    }

    private fun getIsVisitNTP(idTarea: Int?) = execute {
        val antecedents = visitUseCase.loadAntecedentLocalForTask(idTarea).getOrNull()
       _isVisitNTP.value = antecedents?.any { it.codigoTipo == "ANTE_RANP" } ?: false
    }
}