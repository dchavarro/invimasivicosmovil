package com.soaint.sivicos_dinamico.flow.splash

import com.soaint.data.common.PAUSE
import com.soaint.domain.use_case.StartAppResult
import com.soaint.domain.use_case.StartAppResult.*
import com.soaint.domain.use_case.StartAppUseCase
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import kotlinx.coroutines.delay
import javax.inject.Inject

sealed class SplashViewModelEvent {
    object LoadingSplashEvent : SplashViewModelEvent()
    object UpgradeAppSplashEvent : SplashViewModelEvent()
    object OpenHomeSplashEvent : SplashViewModelEvent()
    object OpenLoginSplashEvent : SplashViewModelEvent()
    data class ErrorSplashEvent(val error: Throwable?) : SplashViewModelEvent()
}

class SplashViewModel @Inject constructor(
    private val startAppUseCase: StartAppUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<SplashViewModelEvent>()
    val event get() = _event.asLiveData()

    /**
     * Init
     */

    fun init() = execute {
        delay(PAUSE)
        _event.value = SplashViewModelEvent.LoadingSplashEvent
        val result = startAppUseCase.start(BuildConfig.VERSION_CODE.toString())
        _event.value = getSplashState(result)
    }

    /**
     * Validations
     */

    private fun getSplashState(result: StartAppResult) = when (result) {
        is UpgradeAppResult -> SplashViewModelEvent.UpgradeAppSplashEvent
        is OpenHomeResult -> SplashViewModelEvent.OpenHomeSplashEvent
        is OpenLoginResult -> SplashViewModelEvent.OpenLoginSplashEvent
        is ErrorResult -> SplashViewModelEvent.ErrorSplashEvent(result.error)
    }
}