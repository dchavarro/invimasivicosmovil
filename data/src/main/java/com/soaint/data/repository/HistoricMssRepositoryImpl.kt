package com.soaint.data.repository

import com.soaint.data.api.SanitaryApi
import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.HistoricMssDto
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.HistoricMssDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity
import com.soaint.domain.repository.HistoricMssRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class HistoricMssRepositoryImpl @Inject constructor(
    private val visitApi: VisitApi,
    private val sanitaryApi: SanitaryApi,
    private val historicMssDao: HistoricMssDao,
    ) : HistoricMssRepository, BaseRepository() {

    private val _historicoMss = MutableStateFlow<HistoricMssEntity?>(null)
    override val historicoMss get() = _historicoMss.asStateFlow()

    //Carga de Historico por razonSocial
    override suspend fun loadHistoricMss(razonSocial: String): Result<HistoricMssEntity> = execute {
        val historic = sanitaryApi.getHistoricoMss(razonSocial).objectResponse
        deleteHistoricMssDao(razonSocial)
        saveHistoricMssDao(historic)
        Result.success(historic.mapToDomain())
    }

    //Carga de Historico por razonSocial DB local
    override suspend fun loadHistoricMssLocal(razonSocial: String): Result<HistoricMssEntity> {
        val result = historicMssDao.selectHistoricMss(razonSocial)?.mapToDomain()
            return result?.let { Result.success(it) }?: Result.failure(Throwable())
    }

    override suspend fun loadMssLocal(razonSocial: String): Result<List<MssEntity>> {
        val result = historicMssDao.selectMss(razonSocial)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) }?: Result.failure(Throwable())
    }

    //Guardar Historico en DB local
    private suspend fun saveHistoricMssDao(data: HistoricMssDto) {
        val model = data.mapToDb()
        val mss = data.medidasSanitarias?.map { it.mapToDb(data.razonSocial) }.orEmpty()
        historicMssDao.insertHistoricMss(model)
        historicMssDao.insertMss(mss)
    }

    //Eliminar Historico en DB local
    private suspend fun deleteHistoricMssDao(razonSocial: String) {
        historicMssDao.deleteHistoricMssByRazonSocial(razonSocial)
        historicMssDao.deleteMssByRazonSocial(razonSocial)
       }


}