package com.soaint.domain.use_case

import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.model.AntecedentBodyEntity
import com.soaint.domain.model.AntecedentEntity
import com.soaint.domain.model.InteractionEntity
import com.soaint.domain.model.ScheduleEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.VisitRepository
import javax.inject.Inject

class VisitUseCase @Inject constructor(
    private val visitRepository: VisitRepository
) {

    val visitas get() = visitRepository.visitas

    fun getGroupsVisit() = visitRepository.groupsVisit.value

    suspend fun loadVisitas(idTask: String) = visitRepository.loadVisitas(idTask)

    suspend fun loadVisitLocal(idTask: Int) = visitRepository.loadVisitLocal(idTask)

    suspend fun loadVisitLocalById(idVisit: Int) = visitRepository.loadVisitLocalById(idVisit)

    suspend fun loadAntecedent(idVisit: String) = visitRepository.loadAntecedent(idVisit)

    suspend fun loadAntecedentLocal(idVisit: String) = visitRepository.loadAntecedentLocal(idVisit)
    suspend fun loadAntecedentLocalForTask(idTask: Int?) = visitRepository.loadAntecedentLocalForTask(idTask)

    suspend fun loadSamples(idVisit: String) = visitRepository.loadSamples(idVisit)

    suspend fun loadSamplesLocal(idVisit: String) = visitRepository.loadSamplesLocal(idVisit)

    suspend fun loadSamplesAnalysisLocal(idMuestra: String) =
        visitRepository.loadSamplesAnalysisLocal(idMuestra)

    suspend fun loadSamplesPlanLocal(idMuestra: String) =
        visitRepository.loadSamplesPlanLocal(idMuestra)

    suspend fun loadOfficial(idVisit: String) = visitRepository.loadOfficial(idVisit)

    suspend fun loadOfficialLocal(idVisit: String) = visitRepository.loadOfficialLocal(idVisit)

    suspend fun loadGroup(idVisit: String) = visitRepository.loadGroup(idVisit)

    suspend fun loadGroupLocal(idVisit: String) = visitRepository.loadGroupLocal(idVisit)

    suspend fun loadSubGroup(idVisit: String) = visitRepository.loadSubGroup(idVisit)

    suspend fun loadSubGroupLocal(idVisit: String) = visitRepository.loadSubGroupLocal(idVisit)

    suspend fun updateVisitLocal(visita: VisitEntity, isClosed: Boolean) =
        visitRepository.updateVisitLocal(visita, isClosed)

    suspend fun updateVisit(body: VisitEntity): Result<Unit> {
        return visitRepository.updateVisit(body)
    }

    suspend fun loadAntecedentLocalByPqr(idVisit: String) =
        visitRepository.loadAntecedentLocalByPqr(idVisit)

    suspend fun updateAntecedentLocal(antecedent: AntecedentEntity) =
        visitRepository.updateAntecedentLocal(antecedent)

    suspend fun updateAntecedent(body: List<AntecedentEntity>): Result<Unit> {
        return visitRepository.updateAntecedent(body)
    }

    suspend fun loadInteraction(idVisit: String) = visitRepository.loadInteraction(idVisit)

    suspend fun loadInteractionLocal(idVisit: String) =
        visitRepository.loadInteractionLocal(idVisit)

    suspend fun updateInteraction(body: InteractionEntity) = visitRepository.updateInteraction(body)

    suspend fun updateInteractionLocal(body: InteractionEntity) =
        visitRepository.updateInteractionLocal(body)

    suspend fun copyVisit(idVisit: String) = visitRepository.copyVisit(idVisit)

    suspend fun copyVisitInper(idVisit: Int) = visitRepository.copyVisitInper(idVisit)

    suspend fun loadSchedule(idTramite: Int) = visitRepository.loadSchedule(idTramite)

    suspend fun loadScheduleLocal(idTramite: Int) = visitRepository.loadScheduleLocal(idTramite)

    suspend fun syncLocation() = visitRepository.syncLocation()

    suspend fun saveLocation(latitude: String, longitude: String): Result<Unit> {
        val result = visitRepository.saveLocation(latitude, longitude)
        return when (result.exceptionOrNull()) {
            is WithoutConnectionException -> saveLocationLocal(latitude, longitude)
            else -> result
        }
    }

    suspend fun saveLocationLocal(latitude: String, longitude: String) =
        visitRepository.saveLocationLocal(latitude, longitude)

    suspend fun createAntecedent(body: AntecedentBodyEntity) = visitRepository.createAntecedent(body)
    suspend fun loadHolidays() = visitRepository.loadHolidays()
    suspend fun loadShiftType() = visitRepository.loadShiftType()
    suspend fun loadTypeHoursBilling() = visitRepository.loadTypeHoursBilling()
    suspend fun getIdTramite(idVisit: Int) = visitRepository.getIdTramite(idVisit)
    suspend fun getJordana(jornada: String) = visitRepository.getJordana(jornada)
    suspend fun existsEventDate(fecha: String) = visitRepository.existsEventDate(fecha)
    suspend fun saveScheduleTemp(idTramite: Int) = visitRepository.saveScheduleTemp(idTramite)
    suspend fun getQuantityHoursByCode(code: String) = visitRepository.getQuantityHoursByCode(code)
    suspend fun saveSchedule(list: List<ScheduleEntity>) = visitRepository.saveSchedule(list)
    suspend fun getTypeHoursBilling(idType: Int) = visitRepository.getTypeHoursBilling(idType)
    suspend fun deleteAllScheduleTemp() = visitRepository.deleteAllScheduleTemp()
    suspend fun updateSchedule() = visitRepository.updateSchedule()
}