package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.AntecedentModel
import com.soaint.domain.model.AntecedentBodyEntity
import com.soaint.domain.model.AntecedentEntity

data class AntecedentResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<AntecedentDto>,
    @SerializedName("statusCode") val statusCode: Int
)


data class AntecedentDto(
    @SerializedName("idAntecedente") val id: Int,
    @SerializedName("idTipo") val idTipo: Int?,
    @SerializedName("descTipo") val descTipo: String?,
    @SerializedName("referencia") val referencia: String?,
    @SerializedName("numeroAsociado") val numeroAsociado: String?,
    @SerializedName("codigoTipo") val codigoTipo: String?,
    @SerializedName("idDenuncia") val idDenuncia: Int?,
    @SerializedName("aplicaDenuncia") val aplicaDenuncia: Boolean?,
    @SerializedName("idTarea") val idTarea: Int?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
)

fun AntecedentDto.mapToDomain() = AntecedentEntity(
    id,
    idTipo,
    descTipo,
    referencia,
    numeroAsociado,
    codigoTipo,
    idDenuncia,
    aplicaDenuncia,
    idTarea,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica
)

fun AntecedentDto.mapToDb() = AntecedentModel(
    id,
    idTipo,
    descTipo,
    referencia,
    numeroAsociado,
    codigoTipo,
    idDenuncia,
    aplicaDenuncia,
    idTarea,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica
)

fun AntecedentEntity.mapToData() = AntecedentDto(
    id,
    idTipo,
    descTipo,
    referencia,
    numeroAsociado,
    codigoTipo,
    idDenuncia,
    aplicaDenuncia,
    idTarea,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica
)

data class AntecedentBodyDto(
    @SerializedName("idTipo") val idTipo: Int?,
    @SerializedName("referencia") val referencia: String?,
    @SerializedName("numeroAsociado") val numeroAsociado: String?,
    @SerializedName("idTarea") val idTarea: Int?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
)

fun AntecedentBodyEntity.mapToData() = AntecedentBodyDto(
    idTipo,
    referencia,
    numeroAsociado,
    idTarea,
    usuarioCrea,
)