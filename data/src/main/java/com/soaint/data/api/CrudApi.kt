package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface CrudApi {

    //Consulta facturas
    @POST("v1/listarSolicitudFactura")
    suspend fun getInvoice(
        @Body body: InfoTramitePapfBodyDto
    ): InvoicePapfResponseDto

    //Consulta transporte
    @POST("v1/listarEmpresaTransportadora")
    suspend fun getTransport(
        @Body body: InfoTramitePapfBodyDto
    ): TransportPapfResponseDto

    //Consulta responsableTecnico
    @POST("v1/consultar/responsable/tecnico")
    suspend fun getTechnical(
        @Body body: TechnicalBodyDto
    ): TechnicalResponseDto

    //Guardar responsableTecnico
    @POST("v1/guardar/confirmacion/responsable")
    suspend fun updateTechnical(
        @Body body: BodyTechnicalDto
    ): Unit



}