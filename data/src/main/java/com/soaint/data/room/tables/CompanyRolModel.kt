package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.data.model.VisitFormaDto
import com.soaint.domain.model.VisitCompanyRolEntity
import com.soaint.domain.model.VisitRepresentanteEntity

@Entity(tableName = "company_rol_table")
class CompanyRolModel(
    @ColumnInfo(name = "rol") val rol: String,
    @ColumnInfo(name = "empresaId") val empresaId: Int,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun CompanyRolModel.mapToDomain() = VisitCompanyRolEntity(
    rol
)