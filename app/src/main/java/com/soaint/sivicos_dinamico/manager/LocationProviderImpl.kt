package com.soaint.sivicos_dinamico.manager

import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.location.LocationManager.GPS_PROVIDER
import android.os.Looper
import androidx.core.app.ActivityCompat.checkSelfPermission
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest.Builder
import com.google.android.gms.maps.model.LatLng
import com.soaint.domain.provider.LocationProvider
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.isActive
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

private const val UPDATE_INTERVAL = 3600000L
private const val FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL // / 2

@SuppressLint("MissingPermission")
@Singleton
class LocationProviderImpl @Inject constructor(
    private val context: Context
) : LocationProvider {

    private val defaultLocation = Location(GPS_PROVIDER)

    private val fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)

    private var locationCallback: LocationCallback? = null

    private val locationRequest = LocationRequest().apply {
        interval = UPDATE_INTERVAL
        fastestInterval = FASTEST_UPDATE_INTERVAL
        priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    override suspend fun startLocationUpdates() = callbackFlow {
        if (isActive) {
            trySend(LatLng(defaultLocation.latitude, defaultLocation.longitude))
        }
        if (checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED) {
            locationCallback = object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult?) {
                    locationResult?.let {
                        trySend(LatLng(it.lastLocation.latitude, it.lastLocation.longitude))
                    }
                }
            }
            fusedLocationClient
                .requestLocationUpdates(
                    locationRequest,
                    locationCallback,
                    Looper.getMainLooper()
                )
        }
        awaitClose {
            trySend(LatLng(defaultLocation.latitude, defaultLocation.longitude))
        }
    }

    override fun stopLocationUpdates() {
        locationCallback?.let {
            fusedLocationClient.removeLocationUpdates(it)
        }
    }

    override suspend fun checkGpsStatus() = suspendCoroutine<Boolean> { cont ->
        val task = LocationServices.getSettingsClient(context)
            .checkLocationSettings(Builder().addLocationRequest(locationRequest).build())
        task.addOnCompleteListener {
            try {
                cont.resume(it.result != null)
            } catch (exception: Exception) {
                cont.resume(false)
            }
        }
    }

    override suspend fun getLocation(): LatLng? = suspendCoroutine<LatLng?> { continuation ->
        if (checkSelfPermission(context, ACCESS_COARSE_LOCATION) == PERMISSION_GRANTED) {
            fusedLocationClient.lastLocation.addOnSuccessListener { location ->
                continuation.resume(location?.let { LatLng(it.latitude, it.longitude) })
            }
        } else {
            continuation.resume(null)
        }
    }
}