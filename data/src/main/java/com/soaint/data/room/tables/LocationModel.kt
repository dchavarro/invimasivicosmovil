package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.LocationBodyEntity

@Entity(tableName = "location_table")
class LocationModel(
    @ColumnInfo(name = "latitud") val latitud: String,
    @ColumnInfo(name = "longitud") val longitud: String,
    @ColumnInfo(name = "idPersona") val idPersona: Int,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun LocationModel.mapToDomain() = LocationBodyEntity(
    latitud,
    longitud,
    idPersona,
    fechaCreacion,
    usuarioCrea,
)