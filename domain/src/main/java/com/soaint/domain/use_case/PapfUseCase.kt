package com.soaint.domain.use_case

import com.soaint.domain.model.*
import com.soaint.domain.repository.PapfRepository
import javax.inject.Inject

class PapfUseCase @Inject constructor(
    private val repository: PapfRepository
) {

    val infoTramit get() = repository.infoTramit

    suspend fun loadInfoTramit(idSolicitud: Int) = repository.loadInfoTramit(idSolicitud)

    suspend fun loadInfoTramitLocal(idSolicitud: Int) = repository.loadInfoTramitLocal(idSolicitud)

    suspend fun loadInvoice(idSolicitud: Int) = repository.loadInvoice(idSolicitud)

    suspend fun loadInvoiceLocal(idSolicitud: Int) = repository.loadInvoiceLocal(idSolicitud)

    suspend fun loadTransport(idSolicitud: Int) = repository.loadTransport(idSolicitud)

    suspend fun loadTransportLocal(idSolicitud: Int) = repository.loadTransportLocal(idSolicitud)

    suspend fun loadRegisterProduct(idSolicitud: Int) = repository.loadRegisterProduct(idSolicitud)

    suspend fun loadRegisterProductLocal(idSolicitud: Int) =
        repository.loadRegisterProductLocal(idSolicitud)

    suspend fun loadSanitaryCertificate(idSolicitud: Int) =
        repository.loadSanitaryCertificate(idSolicitud)

    suspend fun loadSanitaryCertificateLocal(idSolicitud: Int) =
        repository.loadSanitaryCertificateLocal(idSolicitud)

    suspend fun loadDocumentation(idSolicitud: Int) = repository.loadDocumentation(idSolicitud)

    suspend fun loadDocumentationLocal(idSolicitud: Int) =
        repository.loadDocumentationLocal(idSolicitud)

    suspend fun loadRegisterDate(idSolicitud: Int) =
        repository.loadRegisterDate(idSolicitud)

    suspend fun loadRegisterDateLocal(idSolicitud: Int) =
        repository.loadRegisterDateLocal(idSolicitud)

    suspend fun getCountPapfLocal() = repository.getCountPapfLocal()

    suspend fun saveRegisterDate(idSolicitud: Int) = repository.saveRegisterDate(idSolicitud)

    suspend fun insertRegisterDateLocal(body: RegisterDateBodyEntity?) =
        repository.insertRegisterDateLocal(body)

    suspend fun loadEmailNotification(idSolicitud: Int) =
        repository.loadEmailNotification(idSolicitud)

    suspend fun sendNotification(idSolicitud: Int) = repository.sendNotification(idSolicitud)

    suspend fun loadDinamicQuerys(codeQuery: String) = repository.loadDinamicQuerys(codeQuery)

    suspend fun loadDinamicQuerysLocal(idTipoTramite: Int? = null) =
        repository.loadDinamicQuerysLocal(idTipoTramite)

    suspend fun loadDinamicQuerysLocal(codeQuery: String) =
        repository.loadDinamicQuerysLocal(codeQuery)

    suspend fun loadDataReqInsSanLocal(idSolicitud: Int, detalleProducto: Int) =
        repository.loadDataReqInsSanLocal(idSolicitud, detalleProducto)

    suspend fun saveDataReqInsSan(idSolicitud: Int) = repository.saveDataReqInsSan(idSolicitud)

    suspend fun insertOrUpdateDataReqInsSanLocal(body: DataReqInsSanPapfEntity?) =
        repository.insertOrUpdateDataReqInsSanLocal(body)

    suspend fun loadDataReqActSampleLocal(idSolicitud: Int, idProducto: Int) =
        repository.loadDataReqActSampleLocal(idSolicitud, idProducto)

    suspend fun saveDataReqActSample(idSolicitud: Int) =
        repository.saveDataReqActSample(idSolicitud)

    suspend fun insertOrUpdateDataReqActSampleLocal(body: DataReqActSamplePapfEntity?) =
        repository.insertOrUpdateDataReqActSampleLocal(body)

    //
    suspend fun loadDataCloseIns(idSolicitud: Int) = repository.loadDataCloseIns(idSolicitud)

    suspend fun loadDataCloseInsLocal(idSolicitud: Int) =
        repository.loadDataCloseInsLocal(idSolicitud)

    suspend fun insertOrUpdateCloseInsFisLocal(body: ClasificationTramitPapfEntity?) =
        repository.insertOrUpdateCloseInsFisLocal(body)

    suspend fun saveCloseInspection(idSolicitud: Int) = repository.saveCloseInspection(idSolicitud)

    suspend fun loadObservationCloseIns(idSolicitud: Int) =
        repository.loadObservationCloseIns(idSolicitud)

    suspend fun insertOrUpdateObservationLocal(observation: CloseInspObservationPapfEntity?) =
        repository.insertOrUpdateObservationLocal(observation)

    suspend fun deleteObservationLocal(observation: CloseInspObservationPapfEntity?) =
        repository.deleteObservationLocal(observation)

    suspend fun loadDataEmitCis(idSolicitud: Int) = repository.loadDataEmitCis(idSolicitud)

    suspend fun loadDataEmitCisLocal(idSolicitud: Int) =
        repository.loadDataEmitCisLocal(idSolicitud)

    suspend fun loadObservationEmitCisLocal(idSolicitud: Int) =
        repository.loadObservationEmitCisLocal(idSolicitud)

    suspend fun insertOrUpdateObservationEmitCisLocal(observation: EmitCisObservationPapfEntity?) =
        repository.insertOrUpdateObservationEmitCisLocal(observation)

    suspend fun deleteObservationEmitCisLocal(observation: EmitCisObservationPapfEntity?) =
        repository.deleteObservationEmitCisLocal(observation)

    suspend fun loadInfoEmitir(idSolicitud: Int) = repository.loadInfoEmitir(idSolicitud)

    suspend fun loadInfoEmitirLocal(idSolicitud: Int) = repository.loadInfoEmitirLocal(idSolicitud)

    suspend fun loadInfoCheck(idSolicitud: Int) = repository.loadInfoCheck(idSolicitud)

    suspend fun loadInfoCheckLocal(idSolicitud: Int) = repository.loadInfoCheckLocal(idSolicitud)

    suspend fun searchPlantillaEmit(body: SearchPlantillaEmitEntity) =
        repository.searchPlantillaEmit(body)

    suspend fun saveEmit(idSolicitud: Int) = repository.saveEmit(idSolicitud)

    suspend fun insertOrUpdateEmitLocal(body: EmitCisClosePapfEntity?) =
        repository.insertOrUpdateEmitLocal(body)

    suspend fun loadDetailProductLocal(
        idSolicitud: Int,
        idProductoSolicitud: Int,
        idClasificacionProducto: Int
    ) =
        repository.loadDetailProductLocal(idSolicitud, idProductoSolicitud, idClasificacionProducto)

    suspend fun updateRequerimientoIF(idSolicitud: Int?, respuesta: Boolean?) = repository.updateRequerimientoIF(idSolicitud, respuesta)
    suspend fun selectProductInspSanitary(idSolicitud: Int) = repository.selectProductInspSanitary(idSolicitud)
    suspend fun selectProductActSample(idSolicitud: Int) = repository.selectProductActSample(idSolicitud)
    suspend fun hasReachedMaximumObservationsInspe(idSolicitud: Int) = repository.hasReachedMaximumObservationsInspe(idSolicitud)
    suspend fun hasReachedMaximumObservationsEmitirCis(idSolicitud: Int) = repository.hasReachedMaximumObservationsEmitirCis(idSolicitud)
    suspend fun loadConsecutivePapf(codigo: String?, idSolicitud: Int?) = repository.loadConsecutivePapf(codigo, idSolicitud)
    suspend fun saveActsRequest(data:SaveInfoActsPapfEntity) = repository.saveActsRequest(data)
    suspend fun selectConsecutivePapfById(idSolicitud: Int) = repository.selectConsecutivePapfById(idSolicitud)
}