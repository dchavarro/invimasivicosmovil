package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.DocumentBodyModel
import com.soaint.data.room.tables.DocumentModel
import com.soaint.data.room.tables.DocumentNameModel
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.DocumentNameEntity
import com.soaint.domain.model.MetadataEntity

data class DocumentResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<DocumentDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class DocumentDto(
    @SerializedName("extension") val extension: String?,
    @SerializedName("descripcionTipoDocumental") val descripcionTipoDocumental: String?,
    @SerializedName("urlSesuite") val urlSesuite: String?,
    @SerializedName("descripcionClasificacionDocumental") val descripcionClasificacionDocumental: String?,
    @SerializedName("detalle") val detalle: String?,
    @SerializedName("urlSharedPoint") val urlSharedPoint: String?,
    @SerializedName("descripcionDocumento") val descripcionDocumento: String?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("clasificacionDocumental") val clasificacionDocumental: Int?,
    @SerializedName("idDocumento") val idDocumento: Int?,
    @SerializedName("nombreDocumento") val nombreDocumento: String?,
    @SerializedName("consecutivoSesuite") val consecutivoSesuite: String?,
    @SerializedName("fechaDocumento") val fechaDocumento: String?,
    @SerializedName("nombreTipoDocumental") val nombreTipoDocumental: String?,
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int? = 0,
    @SerializedName("numeroFolios") val numeroFolios: String?,
    @SerializedName("idPlantilla") val idPlantilla: Int? = 0,
    @SerializedName("codigoPlantilla") val codigoPlantilla: String?,
    @SerializedName("indDocumentoFirmado") val indDocumentoFirmado: Boolean? = false,
)

fun DocumentDto.mapToDomain() = DocumentEntity(
    extension,
    descripcionTipoDocumental,
    urlSesuite,
    descripcionClasificacionDocumental,
    detalle,
    urlSharedPoint,
    descripcionDocumento,
    usuarioCrea,
    clasificacionDocumental,
    idDocumento,
    nombreDocumento,
    consecutivoSesuite,
    fechaDocumento,
    nombreTipoDocumental,
    idTipoDocumental,
    numeroFolios,
    idPlantilla,
    codigoPlantilla,
    indDocumentoFirmado,
    null
)

fun DocumentDto.mapToDb(idVisit: String) = DocumentModel(
    extension,
    descripcionTipoDocumental,
    urlSesuite,
    descripcionClasificacionDocumental,
    detalle,
    urlSharedPoint,
    descripcionDocumento,
    usuarioCrea,
    clasificacionDocumental,
    idDocumento,
    nombreDocumento,
    consecutivoSesuite,
    fechaDocumento,
    nombreTipoDocumental,
    idTipoDocumental,
    numeroFolios,
    idPlantilla,
    codigoPlantilla,
    indDocumentoFirmado,
    idVisit,
    null
)

data class DocumentNameResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: tipoDocumentalDTO,
    @SerializedName("statusCode") val statusCode: Int
)

data class tipoDocumentalDTO(
    @SerializedName("tipoDocumentalDTO") val tipoDocumentalDTO: List<DocumentNameDto>,
)

data class DocumentNameDto(
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int?,
    @SerializedName("nombreTipoDocumental") val nombreTipoDocumental: String?,
)

fun DocumentNameDto.mapToDomain() = DocumentNameEntity(
    idTipoDocumental,
    nombreTipoDocumental,
)

fun DocumentNameDto.mapToDb(idGrupo: Int) = DocumentNameModel(
    idTipoDocumental,
    nombreTipoDocumental,
    idGrupo
)

data class DocumentBodyDto(
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int,
    @SerializedName("idDocumento") val idDocumento: String? = null,
    @SerializedName("nombreDocumento") val nombreDocumento: String?,
    @SerializedName("descripcionDocumento") val descripcionDocumento: String?,
    @SerializedName("extension") val extension: String?,
    @SerializedName("archivoBase64") val archivoBase64: String?,
    @SerializedName("idSolicitud") val idSolicitud: String?,
    @SerializedName("idTramite") val idTramite: String?,
    @SerializedName("idExpediente") val idExpediente: String?,
    @SerializedName("numeroRadicado") val numeroRadicado: String? = null,
    @SerializedName("numeroActaSala") val numeroActaSala: String? = null,
    @SerializedName("idSala") val idSala: String?,
    @SerializedName("idVisita") val idVisita: String?,
    @SerializedName("numeroAgenda") val numeroAgenda: String? = null,
    @SerializedName("idEmpresa") val idEmpresa: String? = null,
    @SerializedName("numeroFolios") val numeroFolios: Int?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("idClasificacionDocumental") val idClasificacionDocumental: Int?,
    @SerializedName("metadata") val metadata: MetadataDto?,
)

fun DocumentBodyEntity.mapToData() = DocumentBodyDto(
    idTipoDocumental,
    idDocumento.orEmpty(),
    nombreDocumento,
    descripcionDocumento,
    extension,
    archivoBase64,
    idSolicitud,
    idTramite,
    idExpediente,
    numeroRadicado.orEmpty(),
    numeroActaSala.orEmpty(),
    idSala,
    idVisita,
    numeroAgenda.orEmpty(),
    idEmpresa.orEmpty(),
    numeroFolios,
    usuarioCrea,
    idClasificacionDocumental,
    metadata?.mapToData(),
)

fun DocumentBodyDto.mapToDb() = DocumentBodyModel(
    idTipoDocumental,
    idDocumento,
    nombreDocumento,
    descripcionDocumento,
    extension,
    archivoBase64,
    idSolicitud,
    idTramite,
    idExpediente,
    numeroRadicado,
    numeroActaSala,
    idSala,
    idVisita,
    numeroAgenda,
    idEmpresa,
    numeroFolios,
    usuarioCrea,
    idClasificacionDocumental
)

data class MetadataDto(
    @SerializedName("tipologiaDocumental") val tipologiaDocumental: String?,
    @SerializedName("tituloDocumento") val tituloDocumento: String?,
    @SerializedName("autor") val autor: String?,
    @SerializedName("autorFirmante") val autorFirmante: String?,
    @SerializedName("clasificacionAcceso") val clasificacionAcceso: String?,
    @SerializedName("folioGd") val folio_gd: String?,
    @SerializedName("formato") val formato: String?,
    @SerializedName("estadoDoc") val estadoDoc: String?,
    @SerializedName("macroProceso") val macroProceso: String?,
    @SerializedName("proceso") val proceso: String?,
    @SerializedName("documental") val documental: String?,
    @SerializedName("tecnologico") val tecnologico: String?,
    @SerializedName("nroVisita") val nroVisita: String?,
    @SerializedName("nombreEmpresa") val nombreEmpresa: String?,
    @SerializedName("nitEmpresa") val nitEmpresa: String?,
    @SerializedName("fechaCreacionDocumento") val fechaCreacionDocumento: String?,
)

fun MetadataEntity.mapToData() = MetadataDto(
    tipologiaDocumental,
    tituloDocumento,
    autor,
    autorFirmante,
    clasificacionAcceso,
    folio_gd,
    formato,
    estadoDoc,
    macroProceso,
    proceso,
    documental,
    tecnologico,
    nroVisita,
    nombreEmpresa,
    nitEmpresa,
    fechaCreacionDocumento
)