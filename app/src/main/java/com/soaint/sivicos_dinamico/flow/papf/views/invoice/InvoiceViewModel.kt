package com.soaint.sivicos_dinamico.flow.papf.views.invoice

import androidx.annotation.StringRes
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.InvoicePapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(val invoices: List<InvoicePapfEntity>) : OnViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class InvoiceViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _invoiceData = MutableLiveData<List<InvoicePapfEntity>>()
    val invoiceData = _invoiceData.asLiveData()

    private val _infoTramite = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramite = _infoTramite.asLiveData()

    fun init(infoTramit: InfoTramitePapfEntity) {
        infoTramit.idSolicitud?.let { loadInvoice(it) }
         _infoTramite.value = infoTramit
    }

    fun loadInvoice(requestId: Int) = execute {
        _event.value =
            OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _invoiceData.value =
            papfUseCase.loadInvoiceLocal(requestId).getOrNull().orEmpty()
        _event.value = if (_invoiceData.value != null) {
            invoiceData.value?.let { OnViewModelEvent.OnViewModelReady(it) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
