package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface TaskRepository {

    val tasks: StateFlow<List<TaskEntity>?>

    suspend fun loadTasks(): Result<List<TaskEntity>>

    suspend fun loadTasksLocal(): Result<List<TaskEntity>>

    suspend fun loadTasksLocalOnlyActive(): Result<List<TaskEntity>>

    suspend fun loadTaskLocal(idTask: Int): Result<TaskEntity>

    suspend fun updateTask(body: TaskBodyEntity): Result<Unit>

    suspend fun updateTaskLocal(task: TaskEntity?): Result<Unit>

    suspend fun createTask(body: TaskMssBodyEntity): Result<TaskEntity?>

    suspend fun createTaskCompany(body: TaskCompanyBodyEntity): Result<Unit>
}