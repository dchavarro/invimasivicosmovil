package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.personal

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseDialogFragment
import com.soaint.sivicos_dinamico.databinding.DialogPersonalBinding
import com.soaint.sivicos_dinamico.extensions.hideKeyboard
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.OnVisitViewModelEvent
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.GroupieAdapter

class DialogPersonal : BaseDialogFragment() {

    companion object {

        fun getInstance() = DialogPersonal().apply {
            arguments = bundleOf()
        }
    }

    private val viewModel by activityViewModels<VisitViewModel> { viewModelFactory }

    private val binding by fragmentBinding<DialogPersonalBinding>(R.layout.dialog_personal)

    private val adapter by lazy { GroupieAdapter() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.90).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window?.setLayout(width, height)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        return binding.root
    }

    private fun initView() {
        binding.editTextObservation.setText(viewModel.observation.value.orEmpty())
        viewModel.onObservationChange(viewModel.observation.value.orEmpty().toEditable())
        binding.editTextObservation.doAfterTextChanged {
            viewModel.onObservationChange(it)
        }
        binding.imageViewClose.setOnSingleClickListener { dismiss() }
        binding.buttonSend.setOnSingleClickListener {
            if (binding.editTextObservation.text.toString().isNotEmpty()) {
                viewModel.onObservation(binding.editTextObservation.text.toString())
                binding.textInputObservation.isErrorEnabled = false
                binding.textInputObservation.error = null
            } else {
                binding.textInputObservation.isErrorEnabled = true
                binding.textInputObservation.error = getString(R.string.campo_obligatorio)
            }
            requireActivity().hideKeyboard()
        }

    }

    private fun eventHandler(event: OnVisitViewModelEvent) {
        when (event) {
            is OnVisitViewModelEvent.onSnack -> showSnack(event.isSuccess, event.message)
            else -> {}
        }
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}