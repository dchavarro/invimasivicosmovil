package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import com.soaint.domain.model.EquipmentEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemEquipmentBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class EquipmentItemView(
    private val equipment: EquipmentEntity,
    private val onEdit: (EquipmentEntity) -> Unit,
    private val onDelete: (EquipmentEntity) -> Unit,
) : BindableItem<ItemEquipmentBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemEquipmentBinding,
        position: Int
    ) = with(viewBinding) {

        textViewZone.text = equipment.descripcionZonaVerificar
        textViewEquipment.text = equipment.equipo

        textViewContact.text = equipment.superficieContacto
        textViewOther.text = equipment.otrasSuperficies

        buttonDelete.isVisible = equipment.isLocal ?: false

        buttonEdit.setOnSingleClickListener {
            onEdit.invoke(equipment)
        }

        buttonDelete.setOnSingleClickListener {
            onDelete.invoke(equipment)
        }
    }

    override fun getLayout() = R.layout.item_equipment
}