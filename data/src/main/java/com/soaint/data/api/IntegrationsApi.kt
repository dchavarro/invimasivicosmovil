package com.soaint.data.api

import retrofit2.http.GET
import retrofit2.http.Query

interface IntegrationsApi {

    @GET("v1/visita/se-suite/generar-radicados")
    suspend fun generateRadicals(
        @Query("idVisita") idVisita: String,
        @Query("usuario") usuario: String,
    ): Unit
}