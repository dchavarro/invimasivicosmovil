package com.soaint.domain.use_case

import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.repository.NotificationRepository
import javax.inject.Inject

class NotificationUseCase @Inject constructor(
    private val notificationRepository: NotificationRepository
) {

    val notification get() = notificationRepository.notification

    suspend fun getEmails(idVisita: String) = notificationRepository.getEmails(idVisita)

    suspend fun getEmailsLocal(idVisita: String) = notificationRepository.getEmailsLocal(idVisita)

    suspend fun addEmail(idVisita: String, correo: String) = notificationRepository.addEmail(idVisita, correo)

    suspend fun saveEmails(idVisita: String, emails: List<String>) = notificationRepository.saveEmails(idVisita, emails)

    suspend fun sendNotification(idVisita: String, codigo: String) = notificationRepository.sendNotification(idVisita, codigo)
    suspend fun deleteEmail(id: Int?) = notificationRepository.deleteEmail(id)
}