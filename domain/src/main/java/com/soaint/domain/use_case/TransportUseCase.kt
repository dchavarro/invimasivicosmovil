package com.soaint.domain.use_case

import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.repository.TransportRepository
import javax.inject.Inject

class TransportUseCase @Inject constructor(
    private val transportRepository: TransportRepository
) {

    val transport get() = transportRepository.transport

    suspend fun loadTransport(idVisit: String) = transportRepository.loadTransport(idVisit)

    suspend fun loadTransportLocal(idVisit: String) = transportRepository.loadTransportLocal(idVisit)

    suspend fun loadTransportOfficialLocal(idVisita: String) = transportRepository.loadTransportOfficialLocal(idVisita)

    suspend fun saveTransport(body: TransportBodyEntity): Result<Unit> {
        return transportRepository.saveTransport(body)
        /*val result = transportRepository.saveTransport(body)
        return when (result.exceptionOrNull()) {
            is WithoutConnectionException -> saveTransportLocal(body)
            else -> result
        }*/
    }

    suspend fun saveTransportLocal(body: TransportBodyEntity)  = transportRepository.saveTransportLocal(body)

    suspend fun getTransportBodyLocal(idVisit: String) = transportRepository.getTransportBodyLocal(idVisit)

    suspend fun getTransportUpload() = transportRepository.getTransportUpload()

    suspend fun getAllTransport(idVisit: String) = transportRepository.getAllTransport(idVisit)

    suspend fun getAllTransportLocal(idVisit: String) = transportRepository.getAllTransportLocal(idVisit)

}