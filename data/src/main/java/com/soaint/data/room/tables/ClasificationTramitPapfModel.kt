package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ClasificationTramitPapfEntity
import com.soaint.domain.model.CloseInspObservationPapfEntity

@Entity(tableName = "clasification_tramit_papf_table")
class ClasificationTramitPapfModel(
    @ColumnInfo(name = "idTipoProductoPapf") val idTipoProductoPapf: Int? = 0,
    @ColumnInfo(name = "tipoProductoPapf") val tipoProductoPapf: String?,
    @ColumnInfo(name = "idActividad") val idActividad: Int? = 0,
    @ColumnInfo(name = "actividad") val actividad: String?,
    @ColumnInfo(name = "idTipoActividad") val idTipoActividad: Int? = 0,
    @ColumnInfo(name = "tipoActividad") val tipoActividad: String?,
    @ColumnInfo(name = "idIdiomaCis") val idIdiomaCis: Int? = 0,
    @ColumnInfo(name = "idiomaCis") val idiomaCis: String?,
    @ColumnInfo(name = "certificadoExportacion") val certificadoExportacion: Boolean?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idIdioma") val idIdioma: Int? = null,
    @ColumnInfo(name = "idioma") val idioma: String? = null,
    @ColumnInfo(name = "idFirmante") val idFirmante: Int? = null,
    @ColumnInfo(name = "firmante") val firmante: String? = null,
    @ColumnInfo(name = "idResultadoCIS") val idResultadoCIS: Int? = null,
    @ColumnInfo(name = "resultadoCIS") val resultadoCIS: String? = null,
    @ColumnInfo(name = "idUsoMPIG") val idUsoMPIG: Int? = null,
    @ColumnInfo(name = "usoMPIG") val usoMPIG: String? = null,
    @ColumnInfo(name = "justificacion") val justificacion: String? = null,
    @ColumnInfo(name = "respuestaRequerimientoIF") val respuestaRequerimientoIF: Boolean? = null,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun ClasificationTramitPapfModel.mapToDomain() = ClasificationTramitPapfEntity(
    idTipoProductoPapf,
    tipoProductoPapf,
    idActividad,
    actividad,
    idTipoActividad,
    tipoActividad,
    idIdiomaCis,
    idiomaCis,
    certificadoExportacion,
    idSolicitud.orEmpty(),
    idIdioma,
    idioma,
    idFirmante,
    firmante,
    idResultadoCIS,
    resultadoCIS,
    idUsoMPIG,
    usoMPIG,
    justificacion,
    respuestaRequerimientoIF,
)

fun ClasificationTramitPapfEntity.mapToModel() = ClasificationTramitPapfModel(
    idTipoProductoPapf,
    tipoProductoPapf,
    idActividad,
    actividad,
    idTipoActividad,
    tipoActividad,
    idIdiomaCis,
    idiomaCis,
    certificadoExportacion,
    idSolicitud.orEmpty(),
    idIdioma,
    idioma,
    idFirmante,
    firmante,
    idResultadoCIS,
    resultadoCIS,
    idUsoMPIG,
    usoMPIG,
    justificacion,
    respuestaRequerimientoIF,
)

@Entity(tableName = "close_insp_observation_papf_table")
class CloseInspObservationPapfModel(
    @ColumnInfo(name = "descripcion") val descripcion: String? = null,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int? = null,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun CloseInspObservationPapfEntity.mapToModel() = CloseInspObservationPapfModel(
    descripcion,
    idSolicitud,
    id,
)