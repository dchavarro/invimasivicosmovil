package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ShiftTypeEntity(
    val id: Int?,
    val jornada: String,
    val turno: String,
    val activo: Boolean,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val codigoJornada: String?,
    val horaInicio: String?,
    val horaFin: String?
) : Parcelable