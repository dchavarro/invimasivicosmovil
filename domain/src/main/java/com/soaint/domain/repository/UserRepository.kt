package com.soaint.domain.repository

import com.soaint.domain.model.UserEntity
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.UserRolesEntity
import kotlinx.coroutines.flow.StateFlow

interface UserRepository {

    val isLogged: StateFlow<Boolean>

    val user: StateFlow<UserEntity?>

    val userInformation: StateFlow<UserInformationEntity?>

    val userRoles: StateFlow<List<UserRolesEntity>?>

    val idMisional: Int

    val idDependencia: Int

    suspend fun getToken(userName: String, password: String): Result<UserEntity>

    suspend fun getLocalToken(userName: String, password: String): Result<UserEntity>

    suspend fun updateUserInformation(userName: String, ip: String): Result<UserInformationEntity>

    fun logout()
}