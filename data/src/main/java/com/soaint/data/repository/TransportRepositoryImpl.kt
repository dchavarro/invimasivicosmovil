package com.soaint.data.repository

import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.TransportDto
import com.soaint.data.model.mapToData
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.TransportDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity
import com.soaint.domain.repository.TransportRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class TransportRepositoryImpl @Inject constructor(
    private val visitApi: VisitApi,
    private val transportDao: TransportDao,
) : TransportRepository, BaseRepository() {

    private val _transport = MutableStateFlow<TransportEntity?>(null)
    override val transport get() = _transport.asStateFlow()

    //Carga de Transporte por visita
    override suspend fun loadTransport(idVisit: String): Result<TransportEntity> = execute {
        val transport = visitApi.getInfoTransport(idVisit).objectResponse
        deleteTransportDao(idVisit)
        saveTransportDao(transport, idVisit)
        Result.success(transport.mapToDomain())
    }

    //Carga de Transporte por visita DB local>
    override suspend fun loadTransportLocal(idVisit: String): Result<List<TransportEntity>> = Result.success(
        transportDao.selectTransport(idVisit).map { it.mapToDomain() }
    )

    //Carga de Transporte por visita DB local
    override suspend fun loadTransportOfficialLocal(idVisita: String): Result<List<OfficialEntity>> = Result.success(
        transportDao.selectTransportOfficial(idVisita).map { it.mapToDomain() }
    )

    //Guardar Transporte en DB local
    private suspend fun saveTransportDao(data: TransportDto, idVisit: String) {
        val model = data.mapToDb(idVisit)
        val officialModel = data.funcionarios?.map { it.mapToDb(model.idVisita.toString()) }.orEmpty()
        transportDao.insertTransport(model)
        transportDao.insertTransportOfficial(officialModel)
    }

    //Eliminar Transporte en DB local
    private suspend fun deleteTransportDao(idVisit: String) {
        transportDao.deleteTransportByVisit(idVisit)
        transportDao.deleteTransportOfficialByVisit(idVisit)
    }

    //Guardar Block with Value
    override suspend fun saveTransport(
        body: TransportBodyEntity
    ): Result<Unit> = execute {
        val result = visitApi.sendTransport(body.mapToData())
        deleteTransportDecomisoDao(body.idVisita)
        Result.success(result)
    }

    //Eliminar Transporte en DB local
    private suspend fun deleteTransportDecomisoDao(idVisit: String) {
        transportDao.deleteTransportDecomiso(idVisit)
    }

    //Guardar Block Local with Value
    override suspend fun saveTransportLocal(
        body: TransportBodyEntity
    ): Result<Unit> = execute {
        val result = transportDao.insertOrUpdateTransportDecomiso(body.mapToModel())
        Result.success(result)
    }

    //Carga de Transporte por visita DB local
    override suspend fun getTransportBodyLocal(idVisit: String): Result<List<TransportBodyEntity>> = Result.success(
        transportDao.selectTransportDecomiso(idVisit).map { it.mapToDomain() }
    )

    override suspend fun getTransportUpload(): Result<Int> = Result.success(
        transportDao.selectAllTransportDecomiso()
    )

    //Carga de Transporte por visita
    override suspend fun getAllTransport(idVisit: String): Result<List<TransportBodyEntity>> = execute {
        val transport = visitApi.getAllTransport(idVisit).objectResponse
        transportDao.deleteAllTransportDecomiso(idVisit)
        transport.map {
            saveTransportLocal(it.mapToDomain(false))
        }
        Result.success(transport.map { it.mapToDomain(false) })
    }

    //Carga de Transporte por visita DB local
    override suspend fun getAllTransportLocal(idVisit: String): Result<List<TransportBodyEntity>> = Result.success(
        transportDao.selectTransportDecomisoAll(idVisit).map { it.mapToDomain() }
    )
}