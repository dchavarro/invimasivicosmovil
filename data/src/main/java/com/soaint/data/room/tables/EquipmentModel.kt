package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.EquipmentEntity
import com.soaint.domain.model.EquipmentUpdateEntity
import com.soaint.domain.model.InteractionEntity
import com.soaint.sivicos_dinamico.utils.CODE_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU

@Entity(tableName = "equipment_table")
class EquipmentModel(
    @PrimaryKey var id: Int?,
    @ColumnInfo(name = "equipo") val equipo: String?,
    @ColumnInfo(name = "codigoZonaVerificar") val codigoZonaVerificar: String?,
    @ColumnInfo(name = "superficieContacto") val superficieContacto: String?,
    @ColumnInfo(name = "otrasSuperficies") val otrasSuperficies: String?,
    @ColumnInfo(name = "idZonaVerificar") val idZonaVerificar: Int?,
    @ColumnInfo(name = "descripcionZonaVerificar") val descripcionZonaVerificar: String?,
    @ColumnInfo(name = "idResultado") val idResultado: Int?,
    @ColumnInfo(name = "codigoResultado") val codigoResultado: String? = CODE_RESULT_EQUIPMENT_REEQ_NOCU,
    @ColumnInfo(name = "descripcionResultado") val descripcionResultado: String? = DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU,
    @ColumnInfo(name = "idSede") val idSede: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean? = true,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String? = getDateHourNow(),
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "fechaModifica") var fechaModifica: String?,
    @ColumnInfo(name = "observacion") val observacion: String?,
    @ColumnInfo(name = "isLocal") var isLocal: Boolean = false,
    @ColumnInfo(name = "isUpdate") var isUpdate: Boolean = false,
    )

fun EquipmentModel.mapToDomain() = EquipmentEntity(
    id,
    equipo,
    codigoZonaVerificar,
    superficieContacto,
    otrasSuperficies,
    idZonaVerificar,
    descripcionZonaVerificar,
    idResultado,
    codigoResultado,
    descripcionResultado,
    idSede,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    isLocal,
    isUpdate,
    observacion
)

fun EquipmentModel.updateFields(
    id: Int? = this.id,
    codigoZonaVerificar: String? = this.codigoZonaVerificar,
    idResultado: Int? = this.idResultado,
    codigoResultado: String? = this.codigoResultado,
    descripcionResultado: String? = this.descripcionResultado,
    observacion: String? = this.observacion,
    isUpdate: Boolean = this.isUpdate
) =
   EquipmentModel(
        id = id,
        equipo = equipo,
        codigoZonaVerificar = codigoZonaVerificar,
        superficieContacto =superficieContacto,
        otrasSuperficies = otrasSuperficies,
        idZonaVerificar = idZonaVerificar,
        descripcionZonaVerificar = descripcionZonaVerificar,
        idResultado = idResultado,
        codigoResultado = codigoResultado,
        descripcionResultado = descripcionResultado,
        idSede = idSede,
        activo = activo,
        usuarioCrea = usuarioCrea,
        fechaCreacion =fechaCreacion,
        usuarioModifica = usuarioModifica,
        fechaModifica = fechaModifica,
        isLocal = isLocal,
        isUpdate = isUpdate,
        observacion = observacion
    )
