package com.soaint.data.di

import javax.inject.Qualifier

@Qualifier
annotation class BaseMockPath

@Qualifier
annotation class BaseDummyPath

@Qualifier
annotation class BaseAuthPath

@Qualifier
annotation class BaseTransversalPath

@Qualifier
annotation class BaseParametricasPath

@Qualifier
annotation class BaseProxyPath

@Qualifier
annotation class BaseGeneralesPath

@Qualifier
annotation class BaseVisitPath

@Qualifier
annotation class BaseLoadPath

@Qualifier
annotation class BaseMasterPath

@Qualifier
annotation class BaseDocumentPath

@Qualifier
annotation class BaseGenericsPath

@Qualifier
annotation class BaseSanitaryPath

@Qualifier
annotation class BaseRegisterSanitaryPath

@Qualifier
annotation class BaseModeloIvcPath

@Qualifier
annotation class BaseCompanyPath

@Qualifier
annotation class BaseNotificationPath

@Qualifier
annotation class BaseAccessDataPath

@Qualifier
annotation class BaseProcedurePath

@Qualifier
annotation class BaseInperPath

@Qualifier
annotation class BaseMasterTransPath

@Qualifier
annotation class BasePlanningPath

@Qualifier
annotation class BaseIntegrationsPath

@Qualifier
annotation class BaseGeneralesPapfPath

@Qualifier
annotation class BasePapfPath

@Qualifier
annotation class BaseCrudPath

@Qualifier
annotation class BaseNotificacionEmailPath

@Qualifier
annotation class BaseFirmaPath

@Qualifier
annotation class LoggerInterceptors

@Qualifier
annotation class ErrorInterceptors