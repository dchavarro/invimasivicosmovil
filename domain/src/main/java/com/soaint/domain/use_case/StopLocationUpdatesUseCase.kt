package com.soaint.domain.use_case

import com.soaint.domain.repository.LocationRepository
import javax.inject.Inject

class StopLocationUpdatesUseCase @Inject constructor(
    private val locationRepository: LocationRepository
) {

    operator fun invoke() = locationRepository.stopLocationUpdates()
}