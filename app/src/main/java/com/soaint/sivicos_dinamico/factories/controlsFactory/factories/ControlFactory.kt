package com.soaint.sivicos_dinamico.factories.controlsFactory.factories

import com.soaint.sivicos_dinamico.factories.controlsFactory.controles.*
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.ControlsTypes

class ControlFactory {
    companion object {
        fun createControl(tipo: ControlsTypes): ControlInterface = when (tipo) {
            ControlsTypes.TEXTO_CORTO -> {
                TextoCorto()
            }
            ControlsTypes.TEXTO_EMAIL -> {
                TextoEmail()
            }
            ControlsTypes.TEXTO_LARGO -> {
                TextoLargo()
            }
            ControlsTypes.RADIO_GROUP -> {
                RadioGroup()
            }
            ControlsTypes.LISTA -> {
                Lista()
            }
            ControlsTypes.LISTA_TABLA -> {
                ListaTabla()
            }
            ControlsTypes.NUMERICO -> {
                Numerico()
            }
            ControlsTypes.FECHA -> {
                Fecha()
            }
            ControlsTypes.FECHA_HORA -> {
                FechaHora()
            }
            ControlsTypes.FIRMA -> {
                Firma()
            }
            ControlsTypes.CHECK_LIST -> {
                CheckList()
            }
            ControlsTypes.FOTO -> {
                Foto()
            }
            ControlsTypes.CHECK -> {
                Check()
            }
        }
    }
}