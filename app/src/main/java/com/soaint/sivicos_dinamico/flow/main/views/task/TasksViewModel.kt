package com.soaint.sivicos_dinamico.flow.main.views.task

import androidx.annotation.StringRes
import com.soaint.domain.model.TaskEntity
import com.soaint.domain.model.TaskPapfEntity
import com.soaint.domain.use_case.TaskPapfUseCase
import com.soaint.domain.use_case.TaskUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnTasksViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnTasksViewModelEvent()
    data class OnViewModelReady(val tasks: List<TaskEntity>) : OnTasksViewModelEvent()
    data class OnViewModelReadyPapf(val tasks: List<TaskPapfEntity>) : OnTasksViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnTasksViewModelEvent()
}

class TasksViewModel @Inject constructor(
    private val taskUseCase: TaskUseCase,
    private val taskPapfUseCase: TaskPapfUseCase,
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnTasksViewModelEvent>()
    val event = _event.asLiveData()

    private val _isPapf = SingleMutableLiveData<Boolean>()
    val isPapf = _isPapf.asLiveData()

    /**
     * Init
     */

    fun init() = execute {
        _isPapf.value = userUseCase.userIsPapf()
        if (userUseCase.userIsPapf()) loadTaskPapf() else loadTask()
    }

    fun loadTask(searchQuery: String? = null, showLabelPreloader: Boolean = true) = execute {
        if (showLabelPreloader) {
            _event.value =
                OnTasksViewModelEvent.OnChangeLabelPreloader(R.string.cargando_tareas)
        }
        val result = taskUseCase.loadTasksLocalOnlyActive()
        _event.value = if (result.isSuccess) {
            val filteredTasks = filterTasks(result.getOrNull(), searchQuery)
            OnTasksViewModelEvent.OnViewModelReady(filteredTasks ?: emptyList())
        } else {
            OnTasksViewModelEvent.OnLoadingError(result.exceptionOrNull())
        }
    }

    fun loadTaskPapf(searchQuery: String? = null, showLabelPreloader: Boolean = true) = execute {
        if (showLabelPreloader) {
            _event.value =
                OnTasksViewModelEvent.OnChangeLabelPreloader(R.string.cargando_trabajos)
        }
        val result = taskPapfUseCase.loadTasksLocal()
        _event.value = if (result.isSuccess) {
            val filteredTasks = filterPapfTasks(result.getOrNull(), searchQuery)
            OnTasksViewModelEvent.OnViewModelReadyPapf(filteredTasks ?: emptyList())
        } else {
            OnTasksViewModelEvent.OnLoadingError(result.exceptionOrNull())
        }
    }

    private fun filterTasks(tasks: List<TaskEntity>?, query: String?): List<TaskEntity>? {
        return tasks?.filter { it.codigoTarea?.contains(query.orEmpty(), true) == true }
    }

    private fun filterPapfTasks(
        tasks: List<TaskPapfEntity>?, query: String?
    ): List<TaskPapfEntity>? {
        return tasks?.filter { it.numeroRadicado?.contains(query.orEmpty(), true) == true }
    }
}
