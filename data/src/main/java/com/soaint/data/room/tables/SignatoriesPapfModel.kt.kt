package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.*


@Entity(tableName = "signatories_papf_table")
class SignatoriesPapfModel(
    @ColumnInfo(name = "idUsuario") val idUsuario: Int?,
    @ColumnInfo(name = "idFuncionario") val idFuncionario: Int?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun SignatoriesPapfModel.mapToDomain() = SignatoriesPapfEntity(
    idUsuario,
    idFuncionario,
    nombre
)
