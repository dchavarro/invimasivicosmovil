package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TaskPapfEntity (
    val idSolicitud: Int?,
    val numeroRadicado: String?,
    val idTipoInspeccion: String?,
    val descripcionTipoInspeccion: String?,
    val fecha: String?,
    val fechaPosibleInspeccion: String?,
    val papf: String?,
    val nombreLugarAlmacenamiento: String?,
    val nombreSitioInspeccion: String?,
    val lote: Int?,
    val idProgramacion: Int?,
    val descActividad: String?,
    val certificadoExportacion: Boolean?,
    val estado: String?,
    val fechaAsignacion: String?,
    val usuarioAsignado: String?,
    val rol: String?,
    val usuarioRed: String?,
) : Parcelable
