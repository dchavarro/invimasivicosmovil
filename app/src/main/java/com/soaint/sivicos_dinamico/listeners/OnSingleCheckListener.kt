package com.soaint.sivicos_dinamico.listeners

import android.os.SystemClock
import android.widget.CompoundButton

private const val MIN_CLICK_INTERVAL: Long = 1000

class OnSingleCheckListener(private val onSingleClick: (CompoundButton, Boolean) -> Unit) : CompoundButton.OnCheckedChangeListener {

    private var mLastClickTime: Long = 0

    override fun onCheckedChanged(compoundButton: CompoundButton, newState: Boolean) {
        val currentClickTime = SystemClock.uptimeMillis()
        val elapsedTime = currentClickTime - mLastClickTime
        mLastClickTime = currentClickTime
        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            compoundButton.isChecked = !newState
            return
        }
        onSingleClick(compoundButton, newState)
    }

}