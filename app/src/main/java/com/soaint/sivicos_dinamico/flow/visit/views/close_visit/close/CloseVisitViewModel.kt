package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.close

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.domain.use_case.TaskUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

sealed class onCloseVisitViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onCloseVisitViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onCloseVisitViewModelEvent()
    data class OnViewModelResultReady(val result: List<ManageMssEntity>) : onCloseVisitViewModelEvent()
    data class OnViewModelActionReady(val result: List<ManageMssEntity>) : onCloseVisitViewModelEvent()
    data class OnViewModelConceptReady(val result: List<ManageMssEntity>) : onCloseVisitViewModelEvent()
    data class OnViewModelStatusReady(val result: List<ManageMssEntity>) : onCloseVisitViewModelEvent()
    data class OnViewModelReadyVisit(val visit: VisitEntity?) : onCloseVisitViewModelEvent()
    data class OnViewModelSnackBar(@StringRes val message: Int, val isSuccess: Boolean): onCloseVisitViewModelEvent()

    data class OnValidateData(
        val actionId:Int?,
        val resultId:Int?,
        val conceptId:Int?,
        val statusId:Int?,
        val concept:String?,
        val percentage:String?,
        val observation:String?,
    ) : onCloseVisitViewModelEvent()

}

class CloseVisitViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val taskUseCase: TaskUseCase,
    private val userUseCase: UserUseCase,
    private val manageMssUseCase: ManageMssUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onCloseVisitViewModelEvent>()
    val event = _event.asLiveData()

    private val _visit = MutableLiveData<VisitEntity>()
    val visit = _visit.asLiveData()

    private val _user = MutableLiveData<UserInformationEntity>()
    val user = _user.asLiveData()

    private val _reprogramar = MutableLiveData<Boolean>()
    val reprogramar = _reprogramar.asLiveData()

    private val _openDialogPersonal = MutableLiveData<Boolean>(false)
    val openDialogPersonal = _openDialogPersonal.asLiveData()

    private val _visitGroup = MutableLiveData<List<VisitGroupEntity>>()
    val visitGroup = _visitGroup.asLiveData()

    private var resultVisit: List<ManageMssEntity> = emptyList()

    private var resultVisitSelected: ManageMssEntity? = null

    private var actionVisit: List<ManageMssEntity> = emptyList()

    //private var actionVisitSelected: ManageMssEntity? = null

    private var conceptVisit: List<ManageMssEntity> = emptyList()

    private var conceptVisitSelected: ManageMssEntity? = null

    private var statusCompany: List<ManageMssEntity> = emptyList()

    private var statusCompanySelected: ManageMssEntity? = null

    private val _actionVisitSelected = MutableLiveData<ManageMssEntity?>(null)
    val actionVisitSelected get() = _actionVisitSelected.asLiveData()

    private val _date = MutableLiveData(EMPTY_STRING)
    val date get() = _date.asLiveData()

    private val _percentage = MutableLiveData(EMPTY_STRING)
    val percentage get() = _percentage.asLiveData()

    private val _descConcept = MutableLiveData(EMPTY_STRING)
    val descConcept get() = _descConcept.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _isVisibleReprogramar = MutableLiveData<Boolean>(false)
    val isVisibleReprogramar = _isVisibleReprogramar.asLiveData()

    private val _isVisibleDateRequirimientos = MutableLiveData<Boolean>(false)
    val isVisibleDateRequirimientos = _isVisibleDateRequirimientos.asLiveData()

    val idTipoProducto get() = _visit.value?.idTipoProducto.orZero()


    fun init(visitData: List<VisitEntity>) {
        _user.value = userUseCase.getUserInformation()
        visitData.firstOrNull()?.let { onLoadVisit(it) }
    }

    private fun onLoadVisit(visit: VisitEntity) = execute {
        val result = visitUseCase.loadVisitLocal(visit.idTarea.orZero()).getOrNull().orEmpty()
        if (!result.isNullOrEmpty()) {
            _visit.value = result.firstOrNull()
            onloadGroup()
            onLoadResultVisit()
            onLoadStatusCompany()
            onLoadActionVisit()
            onLoadConceptVisit()
            _event.value = onCloseVisitViewModelEvent.OnViewModelReadyVisit(_visit.value)
        }
    }

    private suspend fun onLoadResultVisit() {
        val result = manageMssUseCase.loadTypeMssLocal(CDRESV, idTipoProducto)
        _event.value = if (result.isSuccess) {
            resultVisit = result.getOrNull().orEmpty()
            onCloseVisitViewModelEvent.OnViewModelResultReady(result.getOrNull().orEmpty())
        } else {
            onCloseVisitViewModelEvent.onLoadingError(null)
        }
    }

    fun setResultVisitSelected(id: Int?): ManageMssEntity? {
        if (id != null) {
            resultVisit.find { it.id == id }?.let {
                this.resultVisitSelected = it
                if(it.id == ID_RESULT_EJECUTADA) setActionVisitSelected(3) //Cierre Visita
            }
            return resultVisitSelected
        } else {
            resultVisitSelected = null
            return null
        }
    }

    fun setResultVisitSelected(itemSelected: String) {
        resultVisit.find { it.descripcion.equals(itemSelected, true) }?.let {
            this.resultVisitSelected = it
            if(it.id == ID_RESULT_EJECUTADA) setActionVisitSelected(3) //Cierre Visita
        }
    }

    private suspend fun onLoadStatusCompany() {
        val result = manageMssUseCase.loadTypeMssLocal(CDEE, idTipoProducto)
        _event.value = if (result.isSuccess) {
            statusCompany = result.getOrNull().orEmpty()
            onCloseVisitViewModelEvent.OnViewModelStatusReady(result.getOrNull().orEmpty())
        } else {
            onCloseVisitViewModelEvent.onLoadingError(null)
        }
    }

    fun setStatusCompanySelected(itemSelected: String) {
        statusCompany.find { it.descripcion.equals(itemSelected, true) }?.let {
            this.statusCompanySelected = it
        }
    }

    fun setStatusCompanySelected(id: Int?) : ManageMssEntity? {
        if (id != null) {
            statusCompany.find { it.id == id }?.let {
                this.statusCompanySelected = it
            }
            return statusCompanySelected
        } else {
            statusCompanySelected = null
            return null
        }
    }

    private suspend fun onLoadActionVisit() {
        val result = manageMssUseCase.loadTypeMssLocal(CDASS, idTipoProducto)
        _event.value = if (result.isSuccess) {
            actionVisit = result.getOrNull().orEmpty()
            onCloseVisitViewModelEvent.OnViewModelActionReady(result.getOrNull().orEmpty())
        } else {
            onCloseVisitViewModelEvent.onLoadingError(null)
        }
    }

    fun setActionVisitSelected(id: Int?) : ManageMssEntity? {
        _isVisibleReprogramar.value = false
        _visit.value?.requiereReprogramarVisita?.let { setReprogramar(it) }
        if (id != null) {
            actionVisit.find { it.id == id }?.let {
                when (it.codigo) {
                    CODE_ACTION_ACSE_SPVI -> _isVisibleReprogramar.value = true
                    CODE_ACTION_ACSE_CNVRC -> _isVisibleDateRequirimientos.value = true
                    else -> {
                        _isVisibleDateRequirimientos.value = false
                    }
                }
                _actionVisitSelected.value = it
            }
            return _actionVisitSelected.value
        } else {
            _isVisibleReprogramar.value = false
            _actionVisitSelected.value = null
            return _actionVisitSelected.value
        }
    }

    fun setActionVisitSelected(itemSelected: String) {
        _isVisibleReprogramar.value = false
        _isVisibleDateRequirimientos.value = false
        _openDialogPersonal.value = false
        _visit.value?.requiereReprogramarVisita?.let { setReprogramar(it) }
        actionVisit.find { it.descripcion.equals(itemSelected, true) }?.let {
            when (it.codigo) {
                CODE_ACTION_ACSE_SPVI -> _isVisibleReprogramar.value = true
                CODE_ACTION_ACSE_CNVRC -> {
                    if (_visit.value?.tramite?.indReprogramada == true) {
                        _event.value = onCloseVisitViewModelEvent.OnViewModelSnackBar(R.string.copy_error_indreprogrmada, false)
                    } else {
                        _isVisibleDateRequirimientos.value = true
                    }
                }
                CODE_ACTION_ACSE_STPAV -> _openDialogPersonal.value = true
                else -> {
                    _isVisibleReprogramar.value = false
                    _isVisibleDateRequirimientos.value = false
                    _openDialogPersonal.value = false
                }
            }
            _actionVisitSelected.value = it
        }
    }

    private suspend fun onLoadConceptVisit() {
        val result = manageMssUseCase.loadTypeMssLocal(CDCS, idTipoProducto)
        _event.value = if (result.isSuccess) {
            conceptVisit = result.getOrNull().orEmpty()
            onCloseVisitViewModelEvent.OnViewModelConceptReady(result.getOrNull().orEmpty())
        } else {
            onCloseVisitViewModelEvent.onLoadingError(null)
        }
    }

    fun setConceptVisitSelected(id: Int?) : ManageMssEntity? {
        if (id != null) {
            conceptVisit.find { it.id == id }?.let {
                this.conceptVisitSelected = it
            }
            return conceptVisitSelected
        } else {
            conceptVisitSelected = null
            return null
        }
    }

    fun setConceptVisitSelected(itemSelected: String) {
        conceptVisit.find { it.descripcion.equals(itemSelected, true) }?.let {
            this.conceptVisitSelected = it
        }
    }

    fun setReprogramar(reprogramar: Boolean) {
        _reprogramar.value = reprogramar
    }

    fun setDateChange(data: Editable?) {
        _date.value = data.toString()
    }

    fun setPercentageChange(data: Editable?) {
        _percentage.value = data.toString()
    }

    fun setDescConceptChange(data: Editable?) {
        _descConcept.value = data.toString()
    }

    fun setObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun checkData() {
        val resultId = resultVisitSelected?.id
        val actionId = _actionVisitSelected.value?.id
        val conceptId = conceptVisitSelected?.id
        val statusId = statusCompanySelected?.id
        val concept = _descConcept.value!!
        val percentage = _percentage.value!!
        val observation = _observation.value!!

        if (_visit.value?.codigoRazon == REASON_RAVI_INPER) {
            if (resultId == null || statusId == null || observation.isBlank() )
                _event.value = onCloseVisitViewModelEvent.OnValidateData(
                    actionId,
                    resultId,
                    conceptId,
                    statusId,
                    concept,
                    percentage,
                    observation
                )
            else
                updateVisit()
        } else {
            //TODO: Revisae que sucede cuando el la visita no tiene grupo asociado. osea _visitGroup = null o Zero
            _visitGroup.value?.mapNotNull {
                if (it.id == ID_GROUP_PLANTAS_ANIMAL) {
                    if (resultId == null || actionId == null || conceptId == null || statusId == null || concept.isBlank() || percentage.isBlank()|| observation.isBlank() )
                        _event.value = onCloseVisitViewModelEvent.OnValidateData(
                            actionId,
                            resultId,
                            conceptId,
                            statusId,
                            concept,
                            percentage,
                            observation
                        )
                    else
                        updateVisit()
                } else {
                    if (resultId == null || actionId == null || statusId == null || observation.isBlank() )
                        _event.value = onCloseVisitViewModelEvent.OnValidateData(
                            actionId,
                            resultId,
                            conceptId,
                            statusId,
                            concept,
                            percentage,
                            observation
                        )
                    else
                        updateVisit()
                }
            }
        }

    }

    fun updateVisit() = execute {
        val body = _visit.value?.copy(
            requiereReprogramarVisita = _reprogramar.value?: false,
            fechaVerificacionRequerimiento = _date.value.toString(),
            idResultado = resultVisitSelected?.id,
            observacionResultado = _observation.value.orEmpty(),
            calificacion = _percentage.value.orEmpty(),
            idConceptoSanitario = conceptVisitSelected?.id,
            descripcionConcepto = _descConcept.value.orEmpty(),
            idAccionSeguir = _actionVisitSelected.value?.id,
            idEstadoEmpresa = statusCompanySelected?.id,
        )
        val result = body?.let { visitUseCase.updateVisitLocal(it, false) }
        _event.value = if (result?.isSuccess == true) {
            onCloseVisitViewModelEvent.OnViewModelSnackBar(R.string.copy_update_success, true)
        } else {
            onCloseVisitViewModelEvent.onLoadingError(null)
        }
    }

    fun onloadGroup() = execute {
        val result = visitUseCase.loadGroupLocal(_visit.value?.id.orZero().toString())
        if (result.isSuccess) {
            _visitGroup.value = result.getOrNull()
        }
    }
}