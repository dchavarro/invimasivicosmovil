package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.CertificateEntity

@Entity(tableName = "interaction_certificate_table")
class CertificateModel(
    @ColumnInfo(name = "tipoProducto") val tipoProducto: String?= EMPTY_STRING,
    @ColumnInfo(name = "grupoProducto") val grupoProducto: String?= EMPTY_STRING,
    @ColumnInfo(name = "concepto") val concepto: String?= EMPTY_STRING,
    @ColumnInfo(name = "rol") val rol: String?= EMPTY_STRING,
    @ColumnInfo(name = "idtramite") val idtramite: Int?,
    @ColumnInfo(name = "idCertificado") val idCertificado: Int?,
    @ColumnInfo(name = "estado") val estado: String?,
    @ColumnInfo(name = "nitEmpresa") val nitEmpresa: String?= EMPTY_STRING,
    @ColumnInfo(name = "isUpdate") val isUpdate: Boolean = false,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun CertificateModel.mapToDomain() = CertificateEntity(
    tipoProducto,
    grupoProducto,
    concepto,
    rol,
    idtramite,
    idCertificado,
    estado
)

fun CertificateEntity.mapToModel() = CertificateModel(
    tipoProducto,
    grupoProducto,
    concepto,
    rol,
    idtramite,
    idCertificado,
    estado,
)