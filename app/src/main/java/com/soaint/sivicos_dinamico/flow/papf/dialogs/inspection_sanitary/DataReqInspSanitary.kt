package com.soaint.sivicos_dinamico.flow.papf.dialogs.inspection_sanitary

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import com.soaint.domain.model.DataReqInsSanPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseDialogFragment
import com.soaint.sivicos_dinamico.databinding.DialogPapfInspsanBinding
import com.soaint.sivicos_dinamico.extensions.*
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.GroupieAdapter

const val KEY_ID_PRODUCT_DETAIL = "KEY_ID_PRODUCT_DETAIL"
const val KEY_ID_REQUEST = "KEY_ID_REQUEST"

class DataReqInspSanitary : BaseDialogFragment() {

    companion object {

        fun getInstance(
            idDetalleProducto: Int,
            idSolicitud: Int
        ) = DataReqInspSanitary().apply {
            arguments = bundleOf(
                KEY_ID_PRODUCT_DETAIL to idDetalleProducto,
                KEY_ID_REQUEST to idSolicitud,
            )
        }
    }

    private val viewModel: DataReqViewModel by viewModels { viewModelFactory }

    private val binding by fragmentBinding<DialogPapfInspsanBinding>(R.layout.dialog_papf_inspsan)

    private val adapter by lazy { GroupieAdapter() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.95).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window?.setLayout(width, height)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.imageViewClose.setOnSingleClickListener { dismiss() }
        binding.buttonSave.setOnSingleClickListener {
            requireActivity().hideKeyboard()
            viewModel.checkData()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        val idDetalleProducto = arguments?.getInt(KEY_ID_PRODUCT_DETAIL) ?: 0
        val idSolicitud = arguments?.getInt(KEY_ID_REQUEST) ?: 0
        viewModel.init(idDetalleProducto, idSolicitud)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        when (event) {
            is OnViewModelEvent.onSnack -> showSnack(event.isSuccess, event.message)
            is OnViewModelEvent.onViewModelData -> onViewModelData(event.data)
            is OnViewModelEvent.onViewModelPacks -> onViewModelPacks(event.packs)
            is OnViewModelEvent.onViewModelConcepts -> onViewModelConcepts(event.concepts)
            is OnViewModelEvent.OnValidate -> validateErrorData(
                event.packId,
                event.conceptId,
                event.observation,
                event.condition,
            )
        }
    }

    private fun onViewModelData(result: DataReqInsSanPapfEntity?) {
        if (result != null) {
            result.descripcionEmpaque?.let {
                binding.spinnerStatePacking.selectValue(it)
                viewModel.setPack(it)
            }

            result.descripcionConcepto?.let {
                viewModel.setConcept(it)
                binding.spinnerConcept.selectValue(it)
            }

            binding.editTextObservation.setText(result.observaciones.orEmpty())
            viewModel.onObservationChange(result.observaciones.orEmpty().toEditable())

            binding.editTextCondition.setText(result.condicionesAlmacenamiento.orEmpty())
            viewModel.onConditionChange(result.condicionesAlmacenamiento.orEmpty().toEditable())
        }
    }

    private fun onViewModelPacks(result: List<DinamicQuerysPapfEntity>) {
        val array = result.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerStatePacking.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerStatePacking.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setPack(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelConcepts(result: List<DinamicQuerysPapfEntity>) {
        val array = result.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerConcept.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerConcept.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setConcept(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun validateErrorData(
        packId: Int?,
        conceptId: Int?,
        observation: String?,
        condition: String?,
    ) {
        binding.apply {
            validateSpinner(spinnerConcept, conceptId)
            validateSpinner(spinnerStatePacking, packId)
            validateField(textInputObservation, observation)
            validateField(textInputCondition, condition)
        }
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING,
        ) { }
        dialog?.dismiss()
    }
}