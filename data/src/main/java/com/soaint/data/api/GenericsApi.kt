package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface GenericsApi {

    @GET("v1/gestion-errores")
    suspend fun retry(
        @Query("idVisita") idVisita: String,
        @Query("idPlantilla") idPlantilla: String
    ): PdfResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getMss(
        @Body body: BodyGenericValuesDto
    ): MssResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getRol(
        @Body body: BodyGenericValuesDto
    ): RolResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getGroupPapf(
        @Body body: BodyGenericValuesDto
    ): GroupPapfResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getEmpaques(
        @Body body: BodyGenericValuesDto
    ): EmpaqueResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getConcept(
        @Body body: BodyGenericValuesDto
    ): ConceptCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getUnity(
        @Body body: BodyGenericValuesDto
    ): UnityCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getPresentation(
        @Body body: BodyGenericValuesDto
    ): PresentationCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getMpig(
        @Body body: BodyGenericValuesDto
    ): MpigCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getResultCis(
        @Body body: BodyGenericValuesDto
    ): ResultCisCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getIdiom(
        @Body body: BodyGenericValuesDto
    ): IdiomCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getFirmante(
        @Body body: BodyGenericValuesDto
    ): FirmatCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getObservationDefault(
        @Body body: BodyGenericValuesDto
    ): ObservationDefaultCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getRequerimientos(
        @Body body: BodyGenericValuesDto
    ): RequerimientosCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getInfoEmitir(
        @Body body: BodyGenericValuesDto
    ): InfoEmitirCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getTypeCertificate(
        @Body body: BodyGenericValuesDto
    ): CertificadosCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getDateInspection(
        @Body body: BodyGenericValuesDto
    ): RegisterDateCDResponseDto

    @POST("v1/genericos/consultas-dinamicas")
    suspend fun getTypeProduct(
        @Body body: BodyGenericValuesDto
    ): TypeProductResponseDto
}