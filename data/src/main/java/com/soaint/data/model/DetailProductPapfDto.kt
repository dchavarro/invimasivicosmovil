package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*

data class DetailProductPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: DetailProductObjectPapfDto?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class DetailProductObjectPapfDto(
    @SerializedName("InformacionProducto") val informacionProducto: List<DetailProductPapfDto>?,
    @SerializedName("Subpartida") val subpartida: List<SubPartidaPapfDto>?,
    @SerializedName("Expedidor") val expedidor: List<ExpedidorPapfDto>?,
    @SerializedName("Fabricante") val fabricante: List<FabricantePapfDto>?,
    @SerializedName("InformacionComplementaria") val informacionComplementaria: List<InfoComplementariaPapfDto>?,
    @SerializedName("Destinatario") val destinatario: List<DestinatarioPapfDto>?,
    @SerializedName("LugarDestino") val lugarDestino: List<DestinatarioPapfDto>?,
    @SerializedName("OperadorResponsable") val operadorResponsable: List<DestinatarioPapfDto>?,
    @SerializedName("PuertoControlFronterizo") val puertoControlFronterizo: List<DestinatarioPapfDto>?,
    @SerializedName("LicenciasImportacion") val licenciasImportacion: List<DestinatarioPapfDto>?,
)

fun DetailProductObjectPapfDto.mapToDomain() = DetailProductObjectPapfEntity(
    informacionProducto?.map { it.mapToDomain() }.orEmpty(),
    subpartida?.map { it.mapToDomain() }.orEmpty(),
    expedidor?.map { it.mapToDomain() }.orEmpty(),
    fabricante?.map { it.mapToDomain() }.orEmpty(),
    informacionComplementaria?.map { it.mapToDomain() }.orEmpty(),
    destinatario?.map { it.mapToDomain() }.orEmpty(),
    lugarDestino?.map { it.mapToDomain() }.orEmpty(),
    operadorResponsable?.map { it.mapToDomain() }.orEmpty(),
    puertoControlFronterizo?.map { it.mapToDomain() }.orEmpty(),
    licenciasImportacion?.map { it.mapToDomain() }.orEmpty(),
)

data class DetailProductPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("IdProductoSolicitud") val idProductoSolicitud: Int? = 0,
    @SerializedName("Expediente") val expediente: String?,
    @SerializedName("TieneRegistro") val tieneRegistro: Boolean?,
    @SerializedName("RegistroSanitario") val registroSanitario: String?,
    @SerializedName("FechaRegistro") val fechaRegistro: String?,
    @SerializedName("Estado") val estado: String?,
    @SerializedName("IdClasificacionProducto") val idClasificacionProducto: Int? = 0,
    @SerializedName("ClasificacionProducto") val clasificacionProducto: String?,
    @SerializedName("NombreProducto") val nombreProducto: String?,
    @SerializedName("GrupoProducto") val grupoProducto: String?,
    @SerializedName("CategoriaAlimento") val categoriaAlimento: String?,
    @SerializedName("SubCategoriaAlimento") val subCategoriaAlimento: String?,
)

fun DetailProductPapfDto.mapToDomain() = DetailProductPapfEntity(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    expediente.orEmpty(),
    tieneRegistro,
    registroSanitario.orEmpty(),
    fechaRegistro.orEmpty(),
    estado.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto.orEmpty(),
    nombreProducto.orEmpty(),
    grupoProducto.orEmpty(),
    categoriaAlimento.orEmpty(),
    subCategoriaAlimento.orEmpty(),
)

fun DetailProductPapfDto.mapToDb() = DetailProductPapfModel(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    expediente.orEmpty(),
    tieneRegistro,
    registroSanitario.orEmpty(),
    fechaRegistro.orEmpty(),
    estado.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto.orEmpty(),
    nombreProducto.orEmpty(),
    grupoProducto.orEmpty(),
    categoriaAlimento.orEmpty(),
    subCategoriaAlimento.orEmpty(),
)

data class SubPartidaPapfDto(
    @SerializedName("SubPartida") val subPartida: String?,
    @SerializedName("Descripcio") val descripcion: String?,
)

fun SubPartidaPapfDto.mapToDomain() = SubPartidaPapfEntity(
    subPartida.orEmpty(),
    descripcion.orEmpty(),
)

fun SubPartidaPapfDto.mapToDb(
    idSolicitud: Int?,
    idProductoSolicitud: Int?,
    idClasificacionProducto: Int?
) = SubPartidaPapfModel(
    subPartida.orEmpty(),
    descripcion.orEmpty(),
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
)

data class ExpedidorPapfDto(
    @SerializedName("IdExpedidor") val idExpedidor: Int? = 0,
    @SerializedName("RazonSocial") val razonSocial: String?,
    @SerializedName("Nombre") val nombre: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("Pais") val pais: String?,
    @SerializedName("Departamento") val departamento: String?,
    @SerializedName("Municipio") val municipio: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("CodigoISOExpedidor") val codigoISOExpedidor: String?,
    @SerializedName("CodigoPostalExpedidor") val codigoPostalExpedidor: String?,
)

fun ExpedidorPapfDto.mapToDomain() = ExpedidorPapfEntity(
    idExpedidor.orEmpty(),
    razonSocial.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOExpedidor.orEmpty(),
    codigoPostalExpedidor.orEmpty(),
)

fun ExpedidorPapfDto.mapToDb(
    idSolicitud: Int?,
    idProductoSolicitud: Int?,
    idClasificacionProducto: Int?
) = ExpedidorPapfModel(
    idExpedidor.orEmpty(),
    razonSocial.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOExpedidor.orEmpty(),
    codigoPostalExpedidor.orEmpty(),
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
)

data class FabricantePapfDto(
    @SerializedName("IdEmpresaFabricante") val idEmpresaFabricante: Int? = 0,
    @SerializedName("Fabricante") val fabricante: String?,
    @SerializedName("Nombre") val nombre: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("Pais") val pais: String?,
    @SerializedName("Departamento") val departamento: String?,
    @SerializedName("Municipio") val municipio: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("CodigoISOFabricante") val codigoISOFabricante: String?,
    @SerializedName("CodigoPostalFabricante") val codigoPostalFabricante: String?,
    @SerializedName("IdPlantaAutorizada") val idPlantaAutorizada: String?,
    @SerializedName("PlantaAutorizada") val plantaAutorizada: String?,
)

fun FabricantePapfDto.mapToDomain() = FabricantePapfEntity(
    idEmpresaFabricante.orEmpty(),
    fabricante.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOFabricante.orEmpty(),
    codigoPostalFabricante.orEmpty(),
    idPlantaAutorizada.orEmpty(),
    plantaAutorizada.orEmpty(),
)

fun FabricantePapfDto.mapToDb(
    idSolicitud: Int?,
    idProductoSolicitud: Int?,
    idClasificacionProducto: Int?
) = FabricantePapfModel(
    idEmpresaFabricante.orEmpty(),
    fabricante.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOFabricante.orEmpty(),
    codigoPostalFabricante.orEmpty(),
    idPlantaAutorizada.orEmpty(),
    plantaAutorizada.orEmpty(),
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
)

data class InfoComplementariaPapfDto(
    @SerializedName("RequiereInformacionComplementaria") val requiereInformacionComplementaria: Boolean?,
    @SerializedName("Especie") val especie: String?,
    @SerializedName("EstadoMateria") val estadoMateria: String?,
    @SerializedName("CodigoProductoSA") val codigoProductoSA: String?,
    @SerializedName("CertificadoComo") val certificadoComo: String?,
    @SerializedName("FechaFabricacion") val fechaFabricacion: String?,
    @SerializedName("ConsumidorFinal") val consumidorFinal: Boolean?,
    @SerializedName("NaturalezaMercancia") val naturalezaMercancia: String?,
)

fun InfoComplementariaPapfDto.mapToDomain() = InfoComplementariaPapfEntity(
    requiereInformacionComplementaria,
    especie.orEmpty(),
    estadoMateria.orEmpty(),
    codigoProductoSA.orEmpty(),
    certificadoComo.orEmpty(),
    fechaFabricacion.orEmpty(),
    consumidorFinal,
    naturalezaMercancia.orEmpty(),
)

fun InfoComplementariaPapfDto.mapToDb(
    idSolicitud: Int?,
    idProductoSolicitud: Int?,
    idClasificacionProducto: Int?
) = InfoComplementariaPapfModel(
    requiereInformacionComplementaria,
    especie.orEmpty(),
    estadoMateria.orEmpty(),
    codigoProductoSA.orEmpty(),
    certificadoComo.orEmpty(),
    fechaFabricacion.orEmpty(),
    consumidorFinal,
    naturalezaMercancia.orEmpty(),
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
)

data class DestinatarioPapfDto(
    @SerializedName("Pais") val pais: String?,
    @SerializedName("Ciudad") val ciudad: String?,
    @SerializedName("Nombre") val nombre: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("CodigoPostal") val codigoPostal: String?,
    @SerializedName("CodigoISO") val codigoISO: String?,
    @SerializedName("PlantaAutorizadaDestino") val plantaAutorizadaDestino: String?,
    @SerializedName("Autoridad") val autoridad: String?,
)

fun DestinatarioPapfDto.mapToDomain() = DestinatarioPapfEntity(
    pais.orEmpty(),
    ciudad.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    codigoPostal.orEmpty(),
    codigoISO.orEmpty(),
    plantaAutorizadaDestino.orEmpty(),
    autoridad.orEmpty(),
)

fun DestinatarioPapfDto.mapToDb(
    idSolicitud: Int?,
    idProductoSolicitud: Int?,
    idClasificacionProducto: Int?,
    typeList: String
) = DestinatarioPapfModel(
    pais.orEmpty(),
    ciudad.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    codigoPostal.orEmpty(),
    codigoISO,
    plantaAutorizadaDestino.orEmpty(),
    autoridad.orEmpty(),
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
    typeList.orEmpty(),
)
