package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.*
import com.soaint.domain.enum.ActValidate
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM015
import com.soaint.sivicos_dinamico.utils.IVC_VIG_FM118

@Dao
interface CloseDao {

    //Validacion MSS
    @Transaction
    suspend fun validationMss(idVisita: String, tipoMs: String): List<String> {
        var codigos: ArrayList<String> = arrayListOf()
        val items = selectAmsspxIdVisit(idVisita, tipoMs)
        val doc = selectDocument(idVisita)

        items?.onEach {
            val codigosPlantilla = it.codigoPlantillas?.split(",")
                ?.filter { !it.trim().isNullOrBlank() }.orEmpty().distinct()
            codigosPlantilla.onEach { codigo ->
                val document = doc?.find { it.codigoPlantilla == codigo }
                if (document == null) {
                    codigos.add(codigo)
                }
            }
        }
        return codigos.distinct()
    }

    suspend fun validationUpdateMssList(idVisita: String, tipoMs: String): List<String> {
        var codigos: ArrayList<String> = arrayListOf()
        val items = selectMspListxIdVisit(idVisita, tipoMs)
        val doc = selectDocument(idVisita)

        items?.onEach {
            val codigosPlantilla = it.codigoPlantillasActualizada?.split(",")
                ?.filter { !it.trim().isNullOrBlank() }.orEmpty().distinct()
            codigosPlantilla.onEach { codigo ->
                val document = doc?.find { it.codigoPlantilla == codigo }
                if (document == null) {
                    codigos.add(codigo)
                }
            }
        }
        return codigos.distinct()
    }

    @Query("SELECT mssp.* FROM amssp_check_table AS amssp INNER JOIN mss_type_table AS mssp ON mssp.codigo = amssp.codigo WHERE amssp.idVisita = :idVisita AND amssp.isSelected = :isSelected AND amssp.tipoMs = :tipoMs AND mssp.tipoMs = :tipoMs")
    suspend fun selectAmsspxIdVisit(
        idVisita: String,
        tipoMs: String,
        isSelected: Boolean = true
    ): List<TypeMssModel>?

    @Query("SELECT * FROM document_table WHERE idVisita = :idVisita ")
    suspend fun selectDocument(idVisita: String): List<DocumentModel>?

    @Query("SELECT COUNT(*)  as exist FROM document_table WHERE idVisita = :idVisita AND ((idPlantilla IS NOT NULL AND idPlantilla != 0) OR (codigoPlantilla IS NOT NULL AND codigoPlantilla != '' AND codigoPlantilla NOT LIKE '%COMISORIO%'))")
    suspend fun selectDocumentsForCondition(idVisita: String): Int?

    @Query("SELECT * FROM document_table WHERE idVisita = :idVisita AND indDocumentoFirmado = :indDocumentoFirmado")
    suspend fun selectDocumentFirmado(idVisita: String, indDocumentoFirmado: Boolean = true): List<DocumentModel>?

    @Query("SELECT msst.* FROM amssp_check_table AS amssp INNER JOIN mss_type_table AS msst ON msst.codigo = amssp.codigo INNER JOIN mss_list_table AS mssp ON mssp.codigoMedidaSanitaria = amssp.codigo WHERE amssp.idVisita = :idVisita AND amssp.isSelected = :isSelected AND mssp.isUpdate = :isSelected AND amssp.tipoMs = :tipoMs AND mssp.tipoMs = :tipoMs AND msst.tipoMs = :tipoMs")
    suspend fun selectMspListxIdVisit(
        idVisita: String,
        tipoMs: String,
        isSelected: Boolean = true
    ): List<TypeMssModel>?

    @Transaction
    suspend fun validationLegalAtiende(idVisita: String): Boolean {
        val visit = selectVisita(idVisita)
        val isValid = if (visit != null) {
            if (visit.legalAtiende == false) {
                val doc = selectDocument(idVisita)?.distinct()?.filterNotNull()
                val document = doc?.find { it.codigoPlantilla == IVC_VIG_FM118 }
                if (document == null) false else true
            } else {
                true
            }
        } else {
            false
        }
        return isValid
    }

    @Query("SELECT * FROM visit_table WHERE id = :idVisita")
    suspend fun selectVisita(idVisita: String): VisitModel?

    // Solicitud de personal - tiempo
    @Transaction
    suspend fun insertOrUpdateBodyObservation(observationModel: ObservationModel) {
        val items = selectBodyObservation(
            observationModel.idTipoProducto.toString(),
            observationModel.idVisita.toString()
        )
        if (items == null) insertBodyObservation(observationModel) else updateBodyObservation(
            observationModel.descripcion.toString(),
            observationModel.idTipoProducto.toString(),
            observationModel.idVisita.toString()
        )
    }

    @Query("SELECT * FROM observation_table WHERE idTipoProducto = :idTipoProducto AND idVisita = :idVisita")
    suspend fun selectBodyObservation(idTipoProducto: String, idVisita: String): ObservationModel?

    @Insert(entity = ObservationModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertBodyObservation(observationModel: ObservationModel)

    @Query("UPDATE observation_table SET descripcion = :descripcion WHERE idTipoProducto = :idTipoProducto AND idVisita = :idVisita ")
    suspend fun updateBodyObservation(descripcion: String, idTipoProducto: String, idVisita: String)

    @Query("SELECT * FROM observation_table")
    suspend fun selectAllBodyObservation(): List<ObservationModel>?

    @Query("DELETE FROM observation_table")
    suspend fun deleteAllObservation()

    suspend fun validationRaviInper(idVisita: String): List<String> {
        var codigos: ArrayList<String> = arrayListOf()
        val doc = selectDocument(idVisita)

        ActValidate.RAVI_INPER.act.onEach {
            val codigosPlantilla = it.split(",")
                .filter { !it.trim().isNullOrBlank() }.orEmpty().distinct()
            codigosPlantilla.onEach { codigo ->
                val document = doc?.find { it.codigoPlantilla == codigo }
                if (document == null) {
                    codigos.add(codigo)
                }
            }
        }
        return codigos.distinct()
    }

    @Transaction
    suspend fun validationAct015(idVisita: String): Boolean {
        val doc = selectDocument(idVisita)
        val document = doc?.find { it.codigoPlantilla == IVC_INS_FM015 }
        return if (document == null) false else true
    }

    @Transaction
    suspend fun validationActs(idVisita: Int, codigosPlantillas: List<String>): List<String> {
        var codigos: ArrayList<String> = arrayListOf()
        val doc = selectDocument(idVisita.orZero().toString())

        codigosPlantillas.onEach { codigo ->
            val document = doc?.find { it.codigoPlantilla == codigo }
            if (document == null) {
                codigos.add(codigo)
            }
        }
        return codigos.distinct()
    }

    @Transaction
    suspend fun validateAtLeastOneActs(idVisita: Int): Int{
        val exist = selectDocumentsForCondition(idVisita.orZero().toString())
        return exist.orZero()
    }


    @Query("SELECT * FROM block_body_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita AND isFinished = :isFinishedAct")
    suspend fun selectBodyBlock(
        idPlantilla: String,
        idVisita: String,
        isFinishedAct: Boolean = true
    ): BlockBodyModel?

    @Query("SELECT * FROM plantilla_table WHERE codigo = :codigo")
    suspend fun selectPlantillaByCodigo(codigo: String): List<PlantillaModel>?

    @Transaction
    suspend fun validationActsFinished(
        idVisita: Int,
        codigosPlantillas: List<String>
    ): List<String> {
/*        var codigos: ArrayList<String> = arrayListOf()
        codigosPlantillas.map {
            val plantilla = selectPlantillaByCodigo(it)?.firstOrNull()
            if (plantilla != null) {
                val doc =
                    selectBodyBlock(plantilla.id.orZero().toString(), idVisita.orZero().toString())
                if (doc == null) {
                    plantilla.codigo?.let { it1 -> codigos.add(it1) }
                }
            }
        }
        return codigos.distinct()*/

        var codigos: ArrayList<String> = arrayListOf()
        val doc = selectDocumentFirmado(idVisita.orZero().toString())

        codigosPlantillas.onEach { codigo ->
            val document = doc?.find { it.codigoPlantilla == codigo }
            if (document == null) {
                codigos.add(codigo)
            }
        }
        return codigos.distinct()
    }
}