package com.soaint.data.api

import com.soaint.data.model.BodyCertificateDto
import com.soaint.data.model.BodyTechnicalDto
import com.soaint.data.model.CertificateResponseDto
import com.soaint.data.model.TechnicalResponseDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface CompanyApi {

    @POST("v1/Empresa/listadoCertificadoVigentes")
    suspend fun getCertificate(
        @Body body: BodyCertificateDto
    ): CertificateResponseDto
}