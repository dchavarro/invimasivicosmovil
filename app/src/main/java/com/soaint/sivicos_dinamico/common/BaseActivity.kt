package com.soaint.sivicos_dinamico.common

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.ViewManager
import androidx.annotation.CallSuper
import androidx.annotation.IdRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.model.*
import com.soaint.sivicos_dinamico.utils.KotlinUtils
import com.soaint.sivicos_dinamico.utils.LoadingView
import dagger.android.support.DaggerAppCompatActivity
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnNeverAskAgain
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import javax.inject.Inject

@RuntimePermissions
abstract class BaseActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected val utils = KotlinUtils()

    protected var alertDialog: AlertDialog? = null

    private var loadingBar: LoadingView? = null

    private var paused = false

    /**
     * Actions
     */

    protected fun startActivity(activityClass: Class<*>, bundle: Bundle? = null) {
        val intent = Intent(this, activityClass)
        bundle?.let { intent.extras?.putAll(it) }
        startActivity(intent)
        finish()
    }

    protected fun startActivity(startActivityEvent: StartActivityEvent) {
        with(startActivityEvent) {
            val intent = Intent(this@BaseActivity, startActivityEvent.activity)
            bundle?.let { intent.putExtras(it) }
            intent.flags = flag
            if (code == 0) startActivity(intent)
            else startActivityForResult(intent, code)
        }
    }

    protected fun startClearStackActivity(
        currentActivity: Activity,
        newTopActivityClass: Class<out Activity?>?
    ) {
        val intent = Intent(currentActivity, newTopActivityClass)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        currentActivity.startActivity(intent)
    }

    protected fun finishActivity(finishResultEvent: FinishResultEvent) {
        with(finishResultEvent) {
            bundle?.let { setResult(resultCode, Intent().apply { putExtras(it) }) }
                ?: setResult(resultCode)
        }
        finish()
    }


    protected fun subscribeViewModel(viewModel: BaseViewModel, view: View) {
        viewModel.loader.observe(this, Observer(::showLoading))
        viewModel.navigationEvent.observe(this, Observer(::navigateFragment))
    }

    /**
     * Loading
     */

    private fun isLoading(): Boolean = loadingBar?.isShown == true

    private fun initLoading() {
        removeLoading()
        if (loadingBar == null) {
            loadingBar = LoadingView(this)
            this.addContentView(
                loadingBar,
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
            )
        }
    }

    protected open fun showLoading(showing: Boolean, @StringRes text: Int = R.string.cargando) {
        initLoading()
        loadingBar?.setState(showing, text)
    }

    private fun removeLoading() {
        if (loadingBar != null) {
            val viewParent = loadingBar?.parent
            if (viewParent != null && viewParent is ViewManager) {
                (viewParent as ViewManager).removeView(loadingBar)
            }
        }
        loadingBar = null
    }

    protected fun navigateFragment(navigation: NavigationEvents) {
        when (navigation) {
            is StartActivityEvent -> startActivity(navigation)
            is FinishResultEvent -> finishActivity(navigation)
            is OnBackPressedEvent -> onBackPressed(navigation)
            is AddFragmentEvent -> addFragment(navigation)
            is ReplaceFragmentEvent -> replaceFragment(navigation)
            else -> {}
        }
    }

    protected fun onBackPressed(onBackPressedEvent: OnBackPressedEvent) {
        setResult(onBackPressedEvent.resultCode)
        onBackPressed()
    }

    /**
     * Fragments
     */

    protected fun replaceFragment(
        event: ReplaceFragmentEvent
    ) = replaceFragment(event.fragment, event.bundle)

    protected fun replaceFragment(
        fragment: BaseFragment,
        bundle: Bundle? = null,
        @IdRes container: Int = R.id.fragment_container
    ) {
        bundle?.let { fragment.arguments = it }
        supportFragmentManager.beginTransaction()
            .replace(container, fragment, fragment.tag)
            .commit()
    }

    protected fun addFragment(
        event: AddFragmentEvent
    ) = addFragment(event.fragment, event.bundle, event.addToStack)

    protected fun addFragment(
        fragment: BaseFragment,
        bundle: Bundle? = null,
        addToStack: Boolean = true,
        @IdRes container: Int = R.id.fragment_container
    ) {
        bundle?.let { fragment.arguments = it }
        val fragmentTransition = supportFragmentManager.beginTransaction()
            .add(container, fragment, fragment.tag)
        if (addToStack) {
            fragmentTransition.addToBackStack(fragment.tag)
        }
        fragmentTransition.commit()
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

    /**
     * Permissions
     */

    protected fun requestLocationAndCameraPermission() {
        successLocationAndCameraPermissionWithPermissionCheck()
    }

    @NeedsPermission(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
    )
    open fun successLocationAndCameraPermission() {
    }

    @OnPermissionDenied(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        )
    open fun storageLocationAndCameraDenied() {
    }

    @OnNeverAskAgain(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.CAMERA,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE,
        )
    open fun locationAndCameraPermissionOnNeverAskAgain() {
    }

    /**
     * Dialog
     */

    fun showAlertDialog(alertDialogBuilder: AlertDialog.Builder) {
        if (!paused && !isFinishing) {
            alertDialog = alertDialogBuilder.create()
            alertDialog!!.show()
        }
    }

    fun dismissAlertDialog() {
        if (!paused && !isFinishing) {
            alertDialog?.dismiss()
        }
    }

    protected fun showDialog(
        title: String,
        message: String,
        titleBtnPositive: String = EMPTY_STRING,
        titleBtnNegative: String = EMPTY_STRING,
        action: () -> Unit,
    ) =
        AlertDialog.Builder(this)
            .setMessage(message)
            .setTitle(title)
            .setCancelable(false)
            .setPositiveButton(titleBtnPositive) { _, _ ->
                action.invoke()
            }
            .setNegativeButton(titleBtnNegative) { _, _ ->
                dismissAlertDialog()
            }

    /**
     * Overrides
     */

    override fun onPause() {
        paused = true
        super.onPause()
    }

    override fun onResume() {
        super.onResume()
        paused = false
    }

    override fun onBackPressed() {
        if (isLoading())
            showLoading(false)

        if (!isFinishing)
            super.onBackPressed()
    }

    @CallSuper
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        onRequestPermissionsResult(requestCode, grantResults)
    }
}