package com.soaint.sivicos_dinamico.flow.main

import android.os.Bundle
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityMainBinding
import com.soaint.sivicos_dinamico.extensions.goToAppSettings
import com.soaint.sivicos_dinamico.extensions.goToLocationSettings
import com.soaint.sivicos_dinamico.extensions.isEnabledLocation
import com.soaint.sivicos_dinamico.extensions.startLocationService
import com.soaint.sivicos_dinamico.properties.activityBinding

class MainActivity : BaseActivity() {

    private val binding by activityBinding<ActivityMainBinding>(R.layout.activity_main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(binding.toolbar)
    }

    override fun successLocationAndCameraPermission() {
        val isEnabledLocation = isEnabledLocation()
        if (isEnabledLocation) {
            startLocationService()
        } else {
            showAlertDialog(getEnableLocationDialog())
        }
    }

    override fun storageLocationAndCameraDenied() {
        showAlertDialog(getLocationPermissionDialog())
    }

    override fun locationAndCameraPermissionOnNeverAskAgain() {
        showAlertDialog(getLocationPermissionDialog())
    }

    private fun getEnableLocationDialog() = getLocationDialog(
        R.string.location_dialog_title,
        R.string.location_dialog_message,
        ::goToLocationSettings
    )

    private fun getLocationPermissionDialog() = getLocationDialog(
        R.string.location_dialog_title,
        R.string.location_dialog_description,
        ::goToAppSettings
    )

    private fun getLocationDialog(
        @StringRes title: Int,
        @StringRes message: Int,
        action: () -> Unit
    ) = AlertDialog.Builder(this)
        .setTitle(getString(title))
        .setMessage(getString(message))
        .setCancelable(false)
        .setPositiveButton(getString(R.string.si)) { _, _ -> action() }
        .setNegativeButton(getString(R.string.no)) { _, _ -> dismissAlertDialog() }

    override fun onResume() {
        super.onResume()
        requestLocationAndCameraPermission()
    }
}