package com.soaint.data.room

import androidx.room.Database
import androidx.room.RoomDatabase
import com.soaint.data.room.dao.*
import com.soaint.data.room.tables.*

@Database(
    entities = [
        Actas::class,
        GroupVisitModel::class,
        PlantillaModel::class,
        MisionalModel::class,
        TaskModel::class,
        VisitModel::class,
        CompanyModel::class,
        CompanyAddressModel::class,
        CompanyRolModel::class,
        VisitFormaModel::class,
        HistoricModel::class,
        AntecedentModel::class,
        VisitTramiteModel::class,
        SampleModel::class,
        BlockModel::class,
        BlockAtributePlantillaModel::class,
        BlockConfigAtributeModel::class,
        BlockListAtributeModel::class,
        RulesBussinessModel::class,
        HistoricMssModel::class,
        MssModel::class,
        ActasPdfModel::class,
        CredentialModel::class,
        UserModel::class,
        UserInformationModel::class,
        UserRolesModel::class,
        TransportModel::class,
        TransportOffcialModel::class,
        TransportBodyModel::class,
        DocumentModel::class,
        DocumentNameModel::class,
        DocumentBodyModel::class,
        MetadataDocumentBodyModel::class,
        NotificationModel::class,
        BlockBodyModel::class,
        BlockValueModel::class,
        VisitOfficialModel::class,
        CompanyRepresentanteModel::class,
        VisitGroupModel::class,
        VisitSubGroupModel::class,
        TypeMssModel::class,
        MsspListModel::class,
        AmsspCheckModel::class,
        ModeloIvcModel::class,
        IvcDaModel::class,
        TechnicalModel::class,
        InteractionModel::class,
        CertificateModel::class,
        RsModel::class,
        ObservationModel::class,
        ScheduleModel::class,
        EquipmentModel::class,
        LocationModel::class,
        TypeDaoModel::class,
        CountriesModel::class,
        DepartmentModel::class,
        TownModel::class,
        TaskPapfModel::class,
        InfoTramitPapfModel::class,
        SampleAnalysisModel::class,
        SamplePlanModel::class,
        InvoicePapfModel::class,
        TransportDocPapfModel::class,
        TransportComPapfModel::class,
        RegisterProductPapfModel::class,
        DestinoLotePapfModel::class,
        DocumentPapfModel::class,
        SanitaryCertificatePapfModel::class,
        DocumentationPapfModel::class,
        RegisterDateModel::class,
        InfoNotificationPapfModel::class,
        CompanyPersonModel::class,
        DataReqInsSanPapfModel::class,
        DataReqActSamplePapfModel::class,
        ClasificationTramitPapfModel::class,
        TypePapfModel::class,
        CloseInspObservationPapfModel::class,
        EmitCisClosePapfModel::class,
        EmitCisObservationPapfModel::class,
        EmitCisInfoPapfModel::class,
        EmitCisCheckPapfModel::class,
        DetailProductPapfModel::class,
        SubPartidaPapfModel::class,
        ExpedidorPapfModel::class,
        FabricantePapfModel::class,
        InfoComplementariaPapfModel::class,
        DestinatarioPapfModel::class,
        GroupDocumentModel::class,
        HolidaysModel::class,
        TypeHoursBillingModel::class,
        ShiftTypeModel::class,
        ScheduleModelTemp::class,
        SignatoriesPapfModel::class,
        ConsecutivePapfModel::class
    ],
    version = 113,
    exportSchema = false
)
abstract class SivicoDatabase : RoomDatabase() {

    abstract fun actasDao(): ActasDao

    abstract fun plantillasDao(): PlantillasDao

    abstract fun tasksDao(): TasksDao

    abstract fun visitDao(): VisitasDao

    abstract fun historicDao(): HistoricDao

    abstract fun historicMssDao(): HistoricMssDao

    abstract fun userDao(): UserDao

    abstract fun transportDao(): TransportDao

    abstract fun documentDao(): DocumentDao

    abstract fun notificationDao(): NotificationDao

    abstract fun blocksDao(): BlocksDao

    abstract fun manageMssDao(): ManageMssDao

    abstract fun modeloIvcDao(): ModeloIvcDao

    abstract fun technicalDao(): TechnicalDao

    abstract fun closeDao(): CloseDao

    abstract fun equipmentDao(): EquipmentDao

    abstract fun locationDao(): LocationDao

    abstract fun transversalDaoDao(): TransversalDaoDao

    abstract fun registerCompanyDao(): RegisterCompanyDao

    abstract fun tasksPapfDao(): TasksPapfDao

    abstract fun papfDao(): PapfDao
}