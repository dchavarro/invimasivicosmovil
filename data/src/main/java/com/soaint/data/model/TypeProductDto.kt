package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.model.*

data class TypeProductResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<TypeProductDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class TypeProductDto(
    @SerializedName("IdTipoProducto") val id: Int?,
    @SerializedName("Descripcion") val descripcion: String?
)

fun TypeProductDto.mapToDomain(tipo: String) = TypeProductEntity(
    id,
    descripcion.orEmpty(),
    tipo,
)

fun TypeProductDto.mapToDb(idTipoProducto: String, tipoMs: String) = TypeMssModel(
    id = id,
    codigo = null,
    descripcion = descripcion.orEmpty(),
    codigoPlantillas = null,
    codigoPlantillasActualizada = null,
    tipoMs = tipoMs,
    idTipoProducto = idTipoProducto
)