package com.soaint.sivicos_dinamico.flow.papf.item_view

import androidx.core.view.isVisible
import com.soaint.domain.model.CloseInspObservationPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.SubPartidaPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemObservationPapfBinding
import com.soaint.sivicos_dinamico.databinding.ItemSubpartidaPapfBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class SubpartidaPapfItemView(
    private val item: SubPartidaPapfEntity,
) : BindableItem<ItemSubpartidaPapfBinding>() {

    override fun bind(
        viewBinding: ItemSubpartidaPapfBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = item.subPartidaPapf
        textViewClassification.text = item.descripcion
    }

    override fun getLayout() = R.layout.item_subpartida_papf
}