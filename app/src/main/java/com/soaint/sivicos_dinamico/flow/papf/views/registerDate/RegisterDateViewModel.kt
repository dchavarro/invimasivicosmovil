package com.soaint.sivicos_dinamico.flow.papf.views.registerDate

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.RegisterDateBodyEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(val register: RegisterDateBodyEntity?) : OnViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
    data class OnViewModelSnackBar(@StringRes val message: Int, val isSuccess: Boolean) :
        OnViewModelEvent()

    data class OnValidateData(
        val date: String?,
        val observation: String?,
    ) : OnViewModelEvent()
}

class RegisterDateViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _documentsData = MutableLiveData<RegisterDateBodyEntity?>()
    val documentsData = _documentsData.asLiveData()

    private val _idSolicitud = MutableLiveData<Int>()
    val idSolicitud = _idSolicitud.asLiveData()

    private val _date = MutableLiveData(EMPTY_STRING)
    val date get() = _date.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _reprogramar = MutableLiveData<Boolean>()
    val reprogramar = _reprogramar.asLiveData()


    fun init(id: Int) {
        _idSolicitud.value = id
        loadRegisterDate(_idSolicitud.value!!)
    }

    fun loadRegisterDate(requestId: Int) = execute {
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _documentsData.value = papfUseCase.loadRegisterDateLocal(requestId).getOrNull()
        _event.value = OnViewModelEvent.OnViewModelReady(_documentsData.value)
    }

    fun onDateChange(data: Editable?) {
        _date.value = data.toString()
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun setReprogramar(reprogramar: Boolean) {
        _reprogramar.value = reprogramar
    }

    fun validateData() {
        val date = _date.value!!
        val observation = _observation.value!!

        if (date.isNullOrEmpty() || observation.isBlank())
            _event.value = OnViewModelEvent.OnValidateData(date, observation)
        else updateRegisterDate()

    }

    fun updateRegisterDate() = execute {
        val body = RegisterDateBodyEntity(
            fecha = _date.value,
            observaciones = _observation.value,
            idSolicitud = _idSolicitud.value.orZero()
        )
        val result = papfUseCase.insertRegisterDateLocal(body)
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.OnViewModelSnackBar(R.string.copy_update_success, true)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
