package com.soaint.domain.repository

interface FirmaRepository {

    suspend fun firmaCertificada(idSolicitud: Int, pdfFileBase64: String): Result<String?>

}