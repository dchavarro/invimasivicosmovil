package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.EmailsEntity
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.model.NotificationEntity

@Entity(tableName = "notification_email_table")
class NotificationModel(
    @ColumnInfo(name ="correo") val correo: String?,
    @ColumnInfo(name ="idVisita") val idVisita: String?,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun NotificationModel.mapToDomain() = NotificationEntity(
    correo
)

fun NotificationModel.mapToDomainEmails() = EmailsEntity(
    correo,
    id
)