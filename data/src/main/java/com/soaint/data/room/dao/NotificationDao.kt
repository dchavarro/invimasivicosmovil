package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.room.tables.*

@Dao
interface NotificationDao {

    @Query("SELECT correo, id FROM notification_email_table WHERE idVisita = :idVisita ")
    suspend fun selectEmail(idVisita: String): List<NotificationModel>

    @Query("SELECT * FROM notification_email_table WHERE correo = :correo AND idVisita = :idVisita ")
    suspend fun selectEmailAndVisita(idVisita: String, correo: String): List<NotificationModel>

    @Transaction
    suspend fun insertOrUpdateEmail(model: NotificationModel) {
        val items :List<NotificationModel> = selectEmailAndVisita(model.idVisita.toString(), model.correo.toString())
        if (items.isEmpty()) insertEmail(model) else updateDocument(model)
    }

    @Insert(entity = NotificationModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmail(model: NotificationModel)

    @Update
    suspend fun updateDocument(model: NotificationModel)

    @Query("DELETE FROM notification_email_table WHERE idVisita = :idVisita")
    suspend fun deleteEmailByVisita(idVisita: String)

    @Query("DELETE FROM notification_email_table WHERE id = :id")
    suspend fun deleteEmailById(id: Int?)
}