package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class InteractionEntity(
    val id: Int? = 0,
    var emitirCertificacion: Boolean?,
    var responsabilidadSanitaria: Boolean?,
    var tomaMuestras: Boolean?,
    var requiereRetiroProducto: Boolean?,
    var realizoSometimiento: Boolean?,
    var identificadorRecall: String?,
    var perdidaCertificacion: Boolean?,
    var productoTenenciaInvima: Boolean?,
    var productoComercializado: Boolean?,
    val idVisita: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
) : Parcelable

@Parcelize
data class CertificateEntity(
    val tipoProducto: String?,
    val grupoProducto: String?,
    val concepto: String?,
    val rol: String?,
    val idtramite: Int?,
    val idCertificado: Int?,
    val estado: String?,
) : Parcelable {
    var isSelected: Boolean = false
}

