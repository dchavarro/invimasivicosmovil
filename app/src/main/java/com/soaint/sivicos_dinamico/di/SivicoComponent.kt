package com.soaint.sivicos_dinamico.di

import android.app.Application
import com.soaint.sivicos_dinamico.SivicoApplication
import com.soaint.data.di.DataModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        DataModule::class
    ]
)
interface SivicoComponent : AndroidInjector<SivicoApplication> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): Builder

        fun build(): SivicoComponent
    }
}