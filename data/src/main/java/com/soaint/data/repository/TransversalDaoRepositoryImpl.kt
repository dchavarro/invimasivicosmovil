package com.soaint.data.repository

import com.soaint.data.api.MasterTransApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.TransversalDaoDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.CountriesEntity
import com.soaint.domain.model.DepartmentEntity
import com.soaint.domain.model.TownEntity
import com.soaint.domain.model.TypeDaoEntity
import com.soaint.domain.repository.TransversalDaoRepository
import javax.inject.Inject

class TransversalDaoRepositoryImpl @Inject constructor(
    private val masterTransApi: MasterTransApi,
    private val transversalDaoDao: TransversalDaoDao,
    ) : TransversalDaoRepository, BaseRepository() {

    //Carga tipos de documentos
    override suspend fun loadTypeDao(tipoDao: String): Result<List<TypeDaoEntity>?> = execute {
        val typeDoc = masterTransApi.getTypesDao(tipoDao).objectResponse
        transversalDaoDao.deleteAllTypeDao(tipoDao)
        typeDoc?.onEach {
            transversalDaoDao.insertTypeDao(it.mapToDb(tipoDao))
        }.orEmpty()
        Result.success(typeDoc?.map { it.mapToDomain(tipoDao) }.orEmpty())
    }

    //Carga tipos de documentos DB local
    override suspend fun loadTypeDaoLocal(tipoDao: String): Result<List<TypeDaoEntity>> = Result.success(
        transversalDaoDao.selectTypeDao(tipoDao).map { it.mapToDomain() }
    )

    //Carga Paises
    override suspend fun loadCountries(): Result<List<CountriesEntity>?> = execute {
        val typeDoc = masterTransApi.getCountries().objectResponse
        transversalDaoDao.deleteAllCountries()
        typeDoc?.onEach {
            transversalDaoDao.insertCountries(it.mapToDb())
        }.orEmpty()
        Result.success(typeDoc?.map { it.mapToDomain() }.orEmpty())
    }

    //Carga Paises DB local
    override suspend fun loadCountriesLocal(): Result<List<CountriesEntity>> = Result.success(
        transversalDaoDao.selectCountries().map { it.mapToDomain() }
    )

    //Carga Departamentos
    override suspend fun loadDepartment(): Result<List<DepartmentEntity>?> = execute {
        val typeDoc = masterTransApi.getDepartments().objectResponse
        transversalDaoDao.deleteAllDepartment()
        typeDoc?.onEach {
            transversalDaoDao.insertDepartment(it.mapToDb())
        }.orEmpty()
        Result.success(typeDoc?.map { it.mapToDomain() }.orEmpty())
    }

    //Carga Departamentos DB local
    override suspend fun loadDepartmentLocal(idPais: Int): Result<List<DepartmentEntity>> = Result.success(
        transversalDaoDao.selectDepartment(idPais).map { it.mapToDomain() }
    )

    //Carga Municipios
    override suspend fun loadTown(): Result<List<TownEntity>?> = execute {
        val typeDoc = masterTransApi.getTown().objectResponse
        transversalDaoDao.deleteAllTown()
        typeDoc?.onEach {
            transversalDaoDao.insertTown(it.mapToDb())
        }.orEmpty()
        Result.success(typeDoc?.map { it.mapToDomain() }.orEmpty())
    }

    //Carga Municipios DB local
    override suspend fun loadTownLocal(idDepartament: Int): Result<List<TownEntity>> = Result.success(
        transversalDaoDao.selectTown(idDepartament).map { it.mapToDomain() }
    )

}