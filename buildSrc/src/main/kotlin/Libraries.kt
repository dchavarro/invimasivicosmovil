import Libraries.Versions.daggerVersion

const val kotlinVersion = "1.7.10"
const val navigationVersion = "2.3.5"

const val googleServiceVersion = "4.3.10"
const val crashlyticsVersion = "2.4.1"

object Libraries {
    private object Versions {
        const val ktxVersion = "1.6.0"
        const val constraintLayoutVersion = "2.0.4"
        const val retrofitVersion = "2.9.0"
        const val okhttpVersion = "4.10.0"
        const val multidexVersion = "2.0.1"
        const val glideVersion = "4.10.0"
        const val lifecycleVersion = "2.2.0"
        const val androidMaterialVersion = "1.3.0-alpha03"
        const val dataBindingVersion = "3.5.0"
        const val coroutinesVersion = "1.5.1"
        const val daggerVersion = "2.36"
        const val activityVersion = "1.3.1"
        const val fragmentVersion = "1.3.6"
        const val groupieVersion = "2.9.0"
        const val circleImageViewVersion = "3.1.0"
        const val junitVersion = "4.13"
        const val mockitoVersion = "2.19.0"
        const val mockitoKotlinVersion = "2.2.0"
        const val powerMockVersion = "1.6.5"
        const val powerMockModuleApiVersion = "2.0.0"
        const val junitRulesVersion = "1.2.0"
        const val junitExtVersion = "1.1.1"
        const val testVersion = "1.2.0"
        const val espressoVersion = "3.2.0"
        const val crashlyticsVersion = "17.3.1"
        const val analyticsVersion = "19.0.0"
        const val databaseFirebaseVersion = "19.7.0"
        const val messagingFirebaseVersion = "21.0.1"
        const val dynamicVersion = "20.1.0"
        const val inappmessagingVersion = "20.0.0"
        const val datastoreCore = "1.0.0"
        const val datastorePreferences = "1.0.0"
    }

    // Testing
    val testingLibraries = arrayOf(
        "junit:junit:${Versions.junitVersion}",
        "org.mockito:mockito-core:${Versions.mockitoVersion}",
        "org.mockito:mockito-inline:${Versions.mockitoVersion}",
        "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlinVersion}"
    )
    val androidTestLibraries = arrayOf(
        "androidx.test:core:${Versions.testVersion}",
        "androidx.test:runner:${Versions.testVersion}",
        "androidx.test:rules:${Versions.junitRulesVersion}",
        "androidx.test.ext:junit:${Versions.junitExtVersion}",
        "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    )

    // PowerMock
    val powerMockLibraries = arrayOf(
        "org.powermock:powermock:${Versions.powerMockVersion}",
        "org.powermock:powermock-module-junit4:${Versions.powerMockModuleApiVersion}",
        "org.powermock:powermock-api-mockito2:${Versions.powerMockModuleApiVersion}",
        "org.powermock:powermock-module-junit4:${Versions.powerMockModuleApiVersion}"
    )

    // DataBinding
    const val dataBinding = "com.android.databinding:compiler:${Versions.dataBindingVersion}"

    // Kotlin
    val kotlinLibraries = arrayOf(
        "org.jetbrains.kotlin:kotlin-stdlib-jdk8:${kotlinVersion}",
        "org.jetbrains.kotlin:kotlin-stdlib:${kotlinVersion}",
        "org.jetbrains.kotlinx:kotlinx-coroutines-android:${Versions.coroutinesVersion}"
    )

    // Ktx
    val kotlinKtx = arrayOf(
        "androidx.core:core-ktx:${Versions.ktxVersion}",
        "androidx.activity:activity-ktx:${Versions.activityVersion}",
        "androidx.fragment:fragment-ktx:${Versions.fragmentVersion}",
        "androidx.paging:paging-runtime-ktx:3.1.0-alpha03",
        "androidx.swiperefreshlayout:swiperefreshlayout:1.1.0"
    )

    // Retrofit
    val retrofitLibraries = arrayOf(
        "com.squareup.okhttp3:okhttp:${Versions.okhttpVersion}",
        "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpVersion}",
        "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}",
        "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    )

    // Multidex
    val multidexLibrary = "androidx.multidex:multidex:${Versions.multidexVersion}"

    // Glide
    val glideLibraries = arrayOf(
        "com.github.bumptech.glide:glide:${Versions.glideVersion}",
        "com.github.bumptech.glide:okhttp-integration:${Versions.glideVersion}"
    )
    val glideAnnotationProcessor = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"

    // Lifecycle
    val lifecycleLibraries = arrayOf(
        "androidx.lifecycle:lifecycle-runtime:${Versions.lifecycleVersion}",
        "androidx.lifecycle:lifecycle-extensions:${Versions.lifecycleVersion}",
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycleVersion}",
        "androidx.lifecycle:lifecycle-runtime-ktx:${Versions.lifecycleVersion}",
        "androidx.lifecycle:lifecycle-common-java8:${Versions.lifecycleVersion}"
    )

    // Permissions
    val permissionsLibrary = "org.permissionsdispatcher:permissionsdispatcher:4.8.0"
    val permissionsKaptLibrary = "org.permissionsdispatcher:permissionsdispatcher-processor:4.8.0"

    // AndroidX
    val androidXLibraries = arrayOf(
        "com.google.android.material:material:${Versions.androidMaterialVersion}",
        "androidx.cardview:cardview:1.0.0",
        "androidx.legacy:legacy-support-v4:1.0.0",
        "androidx.appcompat:appcompat:1.1.0",
        "androidx.recyclerview:recyclerview:1.1.0",
        "androidx.vectordrawable:vectordrawable:1.1.0"
    )

    // Preference
    val preferenceLibrary = "androidx.preference:preference:1.1.1"
    val secretPreferenceLibrary = "androidx.security:security-crypto:1.0.0"

    // Datastore
    val datastoreLibraries = arrayOf(
        "androidx.datastore:datastore-core:${Versions.datastoreCore}",
        "androidx.datastore:datastore-preferences:${Versions.datastorePreferences}"
    )

    // Navigation
    val navigationLibraries = arrayOf(
        "androidx.navigation:navigation-fragment-ktx:$navigationVersion",
        "androidx.navigation:navigation-ui-ktx:$navigationVersion"
    )

    // Dagger
    val daggerLibrary = arrayOf(
        "com.google.dagger:dagger:$daggerVersion",
        "com.google.dagger:dagger-android:$daggerVersion",
        "com.google.dagger:dagger-android-support:$daggerVersion"
    )
    val daggerKaptLibrary = arrayOf(
        "com.google.dagger:dagger-compiler:$daggerVersion",
        "com.google.dagger:dagger-android-processor:$daggerVersion"
    )

    //Chuck
    const val chuckDebugLibrary = "com.github.chuckerteam.chucker:library:3.5.2"
    const val chuckReleaseLibrary = "com.github.chuckerteam.chucker:library-no-op:3.5.2"

    //Groupie
    val groupieLibrary = arrayOf(
        "com.xwray:groupie:${Versions.groupieVersion}",
        "com.xwray:groupie-databinding:${Versions.groupieVersion}"
    )

    //CircleImageView
    val circleImageViewLibrary = "de.hdodenhof:circleimageview:${Versions.circleImageViewVersion}"

    //Firebase
    val firebaseLibrary = arrayOf(
        "com.google.firebase:firebase-crashlytics-ktx:${Versions.crashlyticsVersion}",
        "com.google.firebase:firebase-analytics-ktx:${Versions.analyticsVersion}",
        "com.google.firebase:firebase-database-ktx:${Versions.databaseFirebaseVersion}",
        "com.google.firebase:firebase-messaging-ktx:${Versions.messagingFirebaseVersion}",
        "com.google.firebase:firebase-dynamic-links-ktx:${Versions.dynamicVersion}",
        "com.google.firebase:firebase-inappmessaging-display-ktx:${Versions.inappmessagingVersion}"
    )
    val googleMaps = arrayOf(
        "com.google.android.gms:play-services-location:17.0.0",
        "com.google.android.gms:play-services-maps:18.0.0"
    )

    // Room components
    val roomLibrary = arrayOf(
        "androidx.room:room-ktx:2.4.3",
        "androidx.room:room-runtime:2.4.3"
    )
    val roomLibraryKapt = "androidx.room:room-compiler:2.4.3"

    // iText
    val itextpdfLibrary = "com.itextpdf:itextg:5.5.10"

    // Cookie
    val cookieLibrary = "org.aviran.cookiebar2:cookiebar2:1.1.4"

    // MultiSelectSpinner
    val multiSelectSpinnerLibrary = "com.github.pratikbutani:MultiSelectSpinner:751cb832d7"

    // toolTipLibrary
    val toolTipLibrary = "com.github.douglasjunior:android-simple-tooltip:1.1.0"

    // Searchable Spinner
    val searchSpinnerLibrary = "com.github.chivorns:smartmaterialspinner:1.5.0"
}