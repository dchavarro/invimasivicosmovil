package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.DocumentationPapfModel
import com.soaint.data.room.tables.SanitaryCertificatePapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.DocumentationPapfEntity

data class DocumentationPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<DocumentationPapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class DocumentationPapfDto(
    @SerializedName("idDocumento") val idDocumento: Int? = 0,
    @SerializedName("idSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("idTipoDocumento") val idTipoDocumento: Int? = 0,
    @SerializedName("tipoDocumento") val tipoDocumento: String?,
    @SerializedName("numero") val numero: String?,
    @SerializedName("tipoAdicional") val tipoAdicional: String?,
    @SerializedName("fechaExpedicion") val fechaExpedicion: String?,
    @SerializedName("numeroPlanta") val numeroPlanta: String?,
    @SerializedName("contenedor") val contenedor: String?,
    @SerializedName("pais") val pais: String?,
    @SerializedName("archivoSharePoint") val archivoSharePoint: String?,
    @SerializedName("archivoSesuite") val archivoSesuite: String?,
    @SerializedName("IdTipo") val idTipo: Int? = 0,
    @SerializedName("TipoCertificado") val tipoCertificado: String?,
    @SerializedName("NombreDocumento") val nombreDocumento: String?,
    @SerializedName("Folios") val folios: String?,
    @SerializedName("Tipo") val tipo: String?,
    @SerializedName("Clasificacion") val clasificacion: String?,
)

fun DocumentationPapfDto.mapToDomain() = DocumentationPapfEntity(
    idDocumento.orEmpty(),
    idSolicitud.orEmpty(),
    idTipoDocumento.orEmpty(),
    tipoDocumento.orEmpty(),
    numero.orEmpty(),
    tipoAdicional.orEmpty(),
    fechaExpedicion.orEmpty(),
    numeroPlanta.orEmpty(),
    contenedor.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
    folios.orEmpty(),
    tipo.orEmpty(),
    clasificacion.orEmpty(),
)

fun DocumentationPapfDto.mapToDb() = DocumentationPapfModel(
    idDocumento.orEmpty(),
    idSolicitud.orEmpty(),
    idTipoDocumento.orEmpty(),
    tipoDocumento.orEmpty(),
    numero.orEmpty(),
    tipoAdicional.orEmpty(),
    fechaExpedicion.orEmpty(),
    numeroPlanta.orEmpty(),
    contenedor.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
    folios.orEmpty(),
    tipo.orEmpty(),
    clasificacion.orEmpty(),
)