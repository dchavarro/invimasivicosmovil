package com.soaint.sivicos_dinamico.model

import android.app.Activity.RESULT_OK
import android.os.Bundle
import androidx.annotation.IdRes
import androidx.navigation.NavDirections
import com.soaint.sivicos_dinamico.common.BaseFragment

sealed class NavigationEvents

data class StartActivityEvent(
    val activity: Class<*>,
    val bundle: Bundle? = null,
    val code: Int = 0,
    val flag: Int = 0
) : NavigationEvents()

data class FinishResultEvent(
    val resultCode: Int = RESULT_OK,
    val bundle: Bundle? = null
) : NavigationEvents()

data class OnBackPressedEvent(
    val resultCode: Int = RESULT_OK
) : NavigationEvents()

data class AddFragmentEvent(
    val fragment: BaseFragment,
    val bundle: Bundle? = null,
    val addToStack: Boolean = true
) : NavigationEvents()

data class ReplaceFragmentEvent(
    val fragment: BaseFragment,
    val bundle: Bundle? = null
) : NavigationEvents()

data class NavigationToDirectionEvent(
    val navDirections: NavDirections
) : NavigationEvents()

data class NavigationToActionIdEvent(
    @IdRes val actionId: Int,
    val arguments: Bundle? = null
) : NavigationEvents()

object NavigationUpEvent : NavigationEvents()

object PopBackStackEvent : NavigationEvents()