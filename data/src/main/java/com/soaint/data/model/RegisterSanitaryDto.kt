package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.InfoProductEntity
import com.soaint.domain.model.RegisterSanitaryEntity

data class RegisterSanitaryResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: RegisterSanitaryDto?,
    @SerializedName("statusCode") val statusCode: Int
)

data class RegisterSanitaryDto(
    @SerializedName("infoGeneralProducto") val infoGeneralProducto: InfoProductDto?,
)

data class InfoProductDto(
    @SerializedName("nombreProducto") val nombreProducto: String? = EMPTY_STRING,
)

fun RegisterSanitaryDto.mapToDomain() = RegisterSanitaryEntity(
    infoGeneralProducto?.mapToDomain()

)fun InfoProductDto.mapToDomain() = InfoProductEntity(
    nombreProducto.orEmpty()
)