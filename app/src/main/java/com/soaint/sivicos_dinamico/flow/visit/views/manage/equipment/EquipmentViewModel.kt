package com.soaint.sivicos_dinamico.flow.visit.views.equipment

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.EquipmentEntity
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CDZVE
import javax.inject.Inject

sealed class OnEquipmentViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnEquipmentViewModelEvent()
    data class OnViewModelZoneReady(val zone: List<ManageMssEntity>) : OnEquipmentViewModelEvent()
    data class OnEquipmentReady(val equipment: List<EquipmentEntity>) : OnEquipmentViewModelEvent()
    object OnLoadingError : OnEquipmentViewModelEvent()
    data class OnValidateData(
        val equipment: String,
        val contact: String,
        val other: String,
        val zoneId: Int?,
    ) : OnEquipmentViewModelEvent()

    data class OnViewModelSuccess(@StringRes val message: Int) : OnEquipmentViewModelEvent()
}

class EquipmentViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
    private val manageUseCase: ManageUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val equipmentUseCase: EquipmentUseCase,
    private val visitUseCase: VisitUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnEquipmentViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<VisitEntity?>()
    val visitData = _visitData.asLiveData()

    private val _equipment = MutableLiveData(EMPTY_STRING)
    val equipment get() = _equipment.asLiveData()

    private val _contact = MutableLiveData(EMPTY_STRING)
    val contact get() = _contact.asLiveData()

    private val _other = MutableLiveData(EMPTY_STRING)
    val other get() = _other.asLiveData()

    private var zone: List<ManageMssEntity> = emptyList()

    private var zoneSelected: ManageMssEntity? = null

    val idTipoProducto get() = _visitData.value?.idTipoProducto.orZero()

    /**
     * INIT
     * */

    fun init(visitData: VisitEntity?) = execute {
        _visitData.value = visitData
        onLoadZone()
        onLoadEquipment()
    }

    fun onEquipmentChange(data: Editable?) {
        _equipment.value = data.toString()
    }

    fun onContactChange(data: Editable?) {
        _contact.value = data.toString()
    }

    fun onOtherChange(data: Editable?) {
        _other.value = data.toString()
    }


    /**
     * lOAD service
     * */

    private fun onLoadZone() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDZVE, idTipoProducto)
        _event.value = if (result.isSuccess) {
            zone = result.getOrNull().orEmpty()
            OnEquipmentViewModelEvent.OnViewModelZoneReady(result.getOrNull().orEmpty())
        } else {
            OnEquipmentViewModelEvent.OnLoadingError
        }
    }


    private fun onLoadEquipment() = execute {
        val idSede = _visitData.value?.empresa?.idSede.orZero()
        val result = equipmentUseCase.loadEquipmentLocal(idSede)
        _event.value = if (result.isSuccess) {
            OnEquipmentViewModelEvent.OnEquipmentReady(result.getOrNull().orEmpty())
        } else {
            OnEquipmentViewModelEvent.OnLoadingError
        }
    }

    fun setZoneSelected(groupSelected: String) {
        zone.find { it.descripcion.equals(groupSelected, true) }?.let {
            zoneSelected = it
        }
    }

    /**
     * Validate
     * */

    fun checkData(body: EquipmentEntity?) {
        val equipment = _equipment.value!!
        val contact = _contact.value!!
        val other = _other.value!!
        val zoneId = zoneSelected?.id
        if (equipment.isBlank() || contact.isBlank() || other.isBlank() || zoneId == null || zoneId == 0)
            _event.value =
                OnEquipmentViewModelEvent.OnValidateData(equipment, contact, other, zoneId)
        else {
            if (body == null) addEquipment() else updateEquipment(
                body.copy(
                    equipo = equipment,
                    codigoZonaVerificar = zoneSelected?.codigo,
                    superficieContacto = contact,
                    otrasSuperficies = other,
                    idZonaVerificar = zoneSelected?.id,
                    descripcionZonaVerificar = zoneSelected?.descripcion,
                )
            )
        }
    }

    fun addEquipment() = execute {
        val body = EquipmentEntity(
            id = 0,
            equipo = _equipment.value,
            codigoZonaVerificar = zoneSelected?.codigo,
            superficieContacto = _contact.value!!,
            otrasSuperficies = _other.value!!,
            idZonaVerificar = zoneSelected?.id,
            descripcionZonaVerificar = zoneSelected?.descripcion,
            idSede = _visitData.value?.empresa?.idSede,
        )
        val result = equipmentUseCase.addEquipmentLocal(body)
        _event.value = if (result.isSuccess) {
            OnEquipmentViewModelEvent.OnViewModelSuccess(R.string.copy_add_success)
        } else {
            OnEquipmentViewModelEvent.OnLoadingError
        }
        onLoadEquipment()
        zoneSelected = null
    }

    fun updateEquipment(equipment: EquipmentEntity?) = execute {
        val result = equipment?.let { equipmentUseCase.updateEquipmentLocal(it) }
        if (result != null) {
            _event.value = if (result.isSuccess) {
                OnEquipmentViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
            } else {
                OnEquipmentViewModelEvent.OnLoadingError
            }
        }
        onLoadEquipment()
        zoneSelected = null
    }

    fun deleteEquipment(equipment: EquipmentEntity?) = execute {
        val result = equipment?.let { equipmentUseCase.deleteEquipmentLocal(it) }
        if (result != null) {
            _event.value = if (result.isSuccess) {
                OnEquipmentViewModelEvent.OnViewModelSuccess(R.string.copy_delete_success)
            } else {
                OnEquipmentViewModelEvent.OnLoadingError
            }
        }
        onLoadEquipment()
    }
}