package com.soaint.data.repository


import com.soaint.data.api.InperApi
import com.soaint.data.api.MasterApi
import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.CloseDao
import com.soaint.data.room.dao.ManageMssDao
import com.soaint.data.room.dao.RegisterCompanyDao
import com.soaint.data.room.dao.VisitasDao
import com.soaint.data.room.tables.*
import com.soaint.domain.common.getDateHourNow
import com.soaint.domain.common.getDateHourWithOutFormat
import com.soaint.domain.common.getIp
import com.soaint.domain.model.*
import com.soaint.domain.repository.UserRepository
import com.soaint.domain.repository.VisitRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class VisitsRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val visitApi: VisitApi,
    private val inperApi: InperApi,
    private val visitasDao: VisitasDao,
    private val closeDao: CloseDao,
    private val registerCompanyDao: RegisterCompanyDao,
    private val manageMssDao: ManageMssDao,
    private val masterApi: MasterApi,
) : VisitRepository, BaseRepository() {

    private val _visitas = MutableStateFlow<List<VisitEntity>?>(null)
    override val visitas get() = _visitas.asStateFlow()

    private val _groupsVisit = MutableStateFlow<List<VisitGroupEntity>?>(null)
    override val groupsVisit get() = _groupsVisit.asStateFlow()

    //Carga de Visitas
    override suspend fun loadVisitas(idTask: String): Result<List<VisitEntity>> = execute {
        closeDao.deleteAllObservation()
        val visit = visitApi.getVisits(idTask).objectResponse.distinct()
        visit.map { deleteVisitDao(it.mapToDomain()) }
        saveVisitDao(visit)
        Result.success(visit.map { it.mapToDomain() })
    }

    //Carga de Visitas DB Local
    override suspend fun loadVisitLocal(idTask: Int): Result<List<VisitEntity>> {
        val visit = visitasDao.selectVisitas(idTask)
        val result = visit.map {
            val visitId = it.id
            val empresa = visitasDao.selectEmpresa(visitId)
            val empresaId = empresa?.idEmpresa ?: 0
            val address = visitasDao.selectAddressEmpresa(empresaId)
            val representante = visitasDao.selectRepresentante(empresaId, visitId)
            val personasVinculadas = registerCompanyDao.selectCompanyPerson(visitId)
            val rol = visitasDao.selectRolEmpresa(empresaId)
            val forma = visitasDao.selectVisitForma(visitId)
            val tramite = visitasDao.selectTramite(visitId)
            it.mapToDomain(
                empresa?.mapToDomain(
                    address?.mapToDomain(),
                    rol.map { it.mapToDomain() },
                    representante.map { it.mapToDomain() },
                    personasVinculadas.map { it.mapToDomain() },
                ),
                forma?.mapToDomain(visitId),
                tramite?.mapToDomain(visitId)
            )
        }
        return Result.success(result)
    }

    //Carga de Visitas DB Local
    override suspend fun loadVisitLocalById(idVisit: Int): Result<VisitEntity?> {
        val visit = visitasDao.selectVisita(idVisit.orZero().toString())
            val visitId = visit?.id.orZero()
            val empresa = visitasDao.selectEmpresa(visitId)
            val empresaId = empresa?.idEmpresa ?: 0
            val address = visitasDao.selectAddressEmpresa(empresaId)
            val representante = visitasDao.selectRepresentante(empresaId, idVisit)
            val personasVinculadas = registerCompanyDao.selectCompanyPerson(visitId)
            val rol = visitasDao.selectRolEmpresa(empresaId)
            val forma = visitasDao.selectVisitForma(visitId)
            val tramite = visitasDao.selectTramite(visitId)
        val result = visit?.mapToDomain(
                empresa?.mapToDomain(
                    address?.mapToDomain(),
                    rol.map { it.mapToDomain() },
                    representante.map { it.mapToDomain() },
                    personasVinculadas.map { it.mapToDomain() },
                ),
                forma?.mapToDomain(visitId),
                tramite?.mapToDomain(visitId)
            )
        return Result.success(result)
    }

    //Guardar Visitas en DB local
    private suspend fun saveVisitDao(data: List<VisitDto>) {
        data.onEach { visita ->
            val model = visita.mapToDb(false)
            val forma = visita.formaRealizarVisita?.mapToDb(visita.id)
            val tramite = visita.tramite?.mapToDb(visita.id)
            val company = visita.empresa?.mapToDb(visita.id, false)
            val companyAddress =
                visita.empresa?.direccion?.mapToDb(visita.empresa.idEmpresa.orZero())
            val companyPerson = visita.empresa?.personasVinculadas?.orEmpty()
            val companyRepresentante = visita.empresa?.representanteLegal.orEmpty()
            val companyRol = visita.empresa?.rolSucursal.orEmpty()
            visitasDao.insertVisitas(model)
            forma?.let { visitasDao.insertVisitForma(it) }
            tramite?.let { visitasDao.insertTramite(it) }
            company?.let { visitasDao.insertCompany(it) }
            companyAddress?.let { visitasDao.insertCompanyAddress(it) }
            companyRol.map {
                visitasDao.insertCompanyRol(
                    CompanyRolModel(
                        it.rol.orEmpty(),
                        visita.empresa?.idEmpresa.orZero()
                    )
                )
            }
            companyRepresentante.map {
                visitasDao.insertCompanyRepresentante(
                    CompanyRepresentanteModel(
                        it.nombre.orEmpty(),
                        it.correoElectronico.orEmpty(),
                        it.telefono.orEmpty(),
                        it.celular.orEmpty(),
                        visita.empresa?.idEmpresa.orZero(),
                        visita.id
                    )
                )
            }
            companyPerson?.mapNotNull {
                registerCompanyDao.insertCompanyPerson(
                    CompanyPersonModel(
                        codigoTipoDocumentoPersona = it.codigoTipoDocumentoPersona,
                        tipoDocumento = it.tipoDocumento,
                        it.numeroDocumentoPersona,
                        it.primerNombre,
                        it.segundoNombre,
                        it.primerApellido,
                        it.segundoApellido,
                        it.idRolPersona,
                        it.descripcionRolPersona,
                        it.correoElectronicoP,
                        visita.id.orZero(),
                    )
                )
            }
        }
    }

    //Eliminar Visitas en DB local
    private suspend fun deleteVisitDao(visit: VisitEntity) {
        visit.empresa?.idEmpresa?.let {
            visitasDao.deleteRepresentanteByCompany(it, visit.id)
            visitasDao.deleteAddressByCompany(it)
            visitasDao.deleteRolByCompany(it)
        }
        visitasDao.deleteCompanyByVisita(visit.id)
        visitasDao.deleteVisitaById(visit.id)
    }

/////////////

    //Carga de antecedentes por visita
    override suspend fun loadAntecedent(idVisit: String): Result<List<AntecedentEntity>> = execute {
        val antecedent = visitApi.getAntecedent(idVisit).objectResponse
        // deleteAntecedentDao()
        saveAntecedentDao(antecedent)
        Result.success(antecedent.map { it.mapToDomain() })
    }

    //Carga de antecedentes por visita DB local
    override suspend fun loadAntecedentLocal(idVisit: String): Result<List<AntecedentEntity>> =
        Result.success(
            visitasDao.selectAntecedent(idVisit).map { it.mapToDomain() }
        )
    override suspend fun loadAntecedentLocalForTask(idTask: Int?): Result<List<AntecedentEntity>> =
        Result.success(
            visitasDao.selectAntecedentForTask(idTask).map { it.mapToDomain() }
        )

    //Guardar antecedentes en DB local
    private suspend fun saveAntecedentDao(data: List<AntecedentDto>) {
        data.onEach { antecedent ->
            val model = antecedent.mapToDb()
            visitasDao.insertAntecedent(model)
        }
    }

    //Eliminar antecedentes en DB local
    private suspend fun deleteAntecedentDao() {
        visitasDao.deleteAllAntecedent()
    }

    /////////////


    //Carga de muestra por visita
    override suspend fun loadSamples(idVisit: String): Result<List<SampleEntity>> = execute {
        val sample = visitApi.getSamples(idVisit).objectResponse
        // deleteAntecedentDao()
        sample.mapNotNull {
            val analysisRequired = visitApi.getAnalysisRequired(it.id.toString()).objectResponse
            visitasDao.deleteSampleAnalysisByIdSample(it.id.toString())
            saveAnalysisRequiredDao(analysisRequired)
            val samplingPlan = visitApi.getSamplingPlan(it.id.toString()).objectResponse
            visitasDao.deleteSamplePlanByIdSample(it.id.toString())
            saveSamplingPlanDao(samplingPlan)
        }
        saveSampleDao(sample)
        Result.success(sample.map { it.mapToDomain() })
    }

    //Guardar muestra en DB local
    private suspend fun saveSampleDao(data: List<SampleDto>) {
        data.onEach { visitasDao.insertSample(it.mapToDb()) }
    }

    //Guardar analisis requerido en DB local
    private suspend fun saveAnalysisRequiredDao(data: List<SampleAnalysisDto>) {
        data.onEach { visitasDao.insertSampleAnalysis(it.mapToDb()) }
    }

    //Guardar analisis requerido en DB local
    private suspend fun saveSamplingPlanDao(data: List<SamplePlanDto>) {
        data.onEach { visitasDao.insertSamplePlan(it.mapToDb()) }
    }

    //Carga de muestra por visita DB local
    override suspend fun loadSamplesLocal(idVisit: String): Result<List<SampleEntity>> =
        Result.success(
            visitasDao.selectSample(idVisit).map { it.mapToDomain() }
        )

    //Carga de Analisis requerido por muestra DB local
    override suspend fun loadSamplesAnalysisLocal(idMuestra: String): Result<List<SampleAnalysisEntity>> =
        Result.success(
            visitasDao.selectSampleAnalysis(idMuestra).map { it.mapToDomain() }
        )

    //Carga de Analisis requerido por muestra DB local
    override suspend fun loadSamplesPlanLocal(idMuestra: String): Result<List<SamplePlanEntity>> =
        Result.success(
            visitasDao.selectSamplePlan(idMuestra).map { it.mapToDomain() }
        )

    //Eliminar muestra en DB local
    private suspend fun deleteSampleDao() {
        visitasDao.deleteAllSample()
    }

    //Carga de funcionarios
    override suspend fun loadOfficial(idVisit: String): Result<List<VisitOfficialEntity>> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val officials = visitApi.getOfficialVisit(
                BodyGenericVisitDto(
                    getIp(),
                    usuario,
                    idVisit.toInt()
                )
            ).objectResponse
            visitasDao.deleteVisitOfficialByVisit(idVisit)
            saveOfficialDao(officials, idVisit)
            Result.success(officials.map { it.mapToDomain(idVisit.toInt()) })
        }

    //Guardar funcionarios en DB local
    private suspend fun saveOfficialDao(data: List<VisitOfficialDto>, idVisit: String) {
        data.onEach { official ->
            val model = official.mapToDb(idVisit.toInt())
            visitasDao.insertOfficial(model)
        }
    }

    //Carga de funcionarios por visita DB local
    override suspend fun loadOfficialLocal(idVisit: String): Result<List<VisitOfficialEntity>> =
        Result.success(
            visitasDao.selectOfficial(idVisit).map { it.mapToDomain(idVisit.toInt()) }
        )


    //Carga de grupos
    override suspend fun loadGroup(idVisit: String): Result<List<VisitGroupEntity>> = execute {
        val grupos = visitApi.getGroup(idVisit).objectResponse
        visitasDao.deleteVisitGroupByIdVisit(idVisit)
        saveGroupDao(grupos)
        Result.success(grupos.map { it.mapToDomain() })
    }

    //Carga de grupos
    override suspend fun loadGroupLocal(idVisit: String): Result<List<VisitGroupEntity>> = execute {
        val group = visitasDao.selectVisitGroup(idVisit.toInt()).map { it.mapToDomain() }
        _groupsVisit.value = group
        Result.success(group)
    }

    //Guardar grupos en DB local
    private suspend fun saveGroupDao(data: List<VisitGroupDto>) {
        data.onEach { grupos ->
            val model = grupos.mapToDb()
            visitasDao.insertVisitGroup(model)
        }
    }

    //Carga de grupos
    override suspend fun loadSubGroup(idVisit: String): Result<List<VisitGroupEntity>> = execute {
        val subgrupos = visitApi.getSubGroup(idVisit).objectResponse
        visitasDao.deleteVisitSubGroupByIdVisit(idVisit)
        saveSubGroupDao(subgrupos)
        Result.success(subgrupos.map { it.mapToDomain() })
    }

    //Carga de grupos
    override suspend fun loadSubGroupLocal(idVisit: String): Result<List<VisitGroupEntity>> =
        Result.success(
            visitasDao.selectVisitSubGroup(idVisit.toInt()).map { it.mapToDomain() }
        )

    //Guardar grupos en DB local
    private suspend fun saveSubGroupDao(data: List<VisitSubGroupDto>) {
        data.onEach { subgrupos ->
            val model = subgrupos.mapToDb()
            visitasDao.insertVisitSubGroup(model)
        }
    }

    //Update Visita en DB local
    override suspend fun updateVisitLocal(visita: VisitEntity, isClosed: Boolean): Result<Unit> =
        execute {
            val result = visita.mapToData().mapToDb(isClosed).let {
                visitasDao.updateVisit(it)
                visitasDao.updateCompany(it.idEstadoEmpresa, it.id)
            }
            Result.success(result)
        }

    //Update Visita
    override suspend fun updateVisit(
        body: VisitEntity
    ): Result<Unit> = execute {
        val result = visitApi.updateVisit(body.mapToData())
        Result.success(result)
    }

    //Carga de antecedentes por visita DB local
    override suspend fun loadAntecedentLocalByPqr(idVisit: String): Result<List<AntecedentEntity>> =
        Result.success(
            visitasDao.selectAntecedentByPqr(idVisit).map { it.mapToDomain() }
        )

    //Update Antecedente local
    override suspend fun updateAntecedentLocal(body: AntecedentEntity?): Result<Unit> = execute {
        val result = body?.mapToData()?.mapToDb()?.let { visitasDao.updateAntecedent(it) }
        Result.success(result)
    } as Result<Unit>

    //Update Antecedente
    override suspend fun updateAntecedent(body: List<AntecedentEntity>): Result<Unit> = execute {
        var listValue: ArrayList<AntecedentDto> = arrayListOf()
        body.mapNotNull { listValue.add(it.mapToData()) }
        val result = if (!listValue.isNullOrEmpty()) {
            visitApi.updateAntecedent(listValue)
        } else {
            Unit
        }
        Result.success(result)
    }

    //Carga de interacciones por visita
    override suspend fun loadInteraction(idVisit: String): Result<InteractionEntity> = execute {
        val antecedent = visitApi.getInteraction(idVisit).objectResponse
        deleteInteractionDao(idVisit)
        saveInteractionDao(antecedent)
        Result.success(antecedent.mapToDomain())
    }

    //Guardar interacciones en DB local
    private suspend fun saveInteractionDao(data: InteractionDto) {
        val model = data.mapToDb()
        if (model.idVisita != null) {
            visitasDao.insertInteraction(model)
        }
    }

    //Eliminar interacciones en DB local
    private suspend fun deleteInteractionDao(idVisit: String) {
        visitasDao.deleteAllInteraction(idVisit)
    }

    //Carga de interacciones por visita DB local
    override suspend fun loadInteractionLocal(idVisit: String): Result<InteractionEntity?> =
        Result.success(
            visitasDao.selectInteraction(idVisit)?.mapToDomain()
        )

    //Update interacciones
    override suspend fun updateInteraction(
        body: InteractionEntity
    ): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val result = visitApi.updateInteraction(
            BodyGenericInteractionDto(
                getIp(),
                userName,
                body.mapToData()
            )
        )
        Result.success(result)
    }

    //Update interacciones local
    override suspend fun updateInteractionLocal(body: InteractionEntity?): Result<Unit> = execute {
        val result = body?.mapToData()?.mapToDb()?.let { visitasDao.updateInteraction(it) }
        Result.success(result)
    } as Result<Unit>

    //Copia Visita
    override suspend fun copyVisit(idVisit: String): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val result = visitApi.copyVisit(BodyGenericVisitDto(getIp(), userName, idVisit.toInt()))
        Result.success(result)
    }

    //Copia Visita Inper
    override suspend fun copyVisitInper(idVisit: Int): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val result = inperApi.copyVisit(BodyGenericVisitDto(getIp(), userName, idVisit.toInt()))
        Result.success(result)
    }

    //Carga de horas por visita
    override suspend fun loadSchedule(idTramite: Int): Result<List<ScheduleEntity>?> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val schedule = inperApi.getSchedule(BodyGenericScheduleDto(getIp(), userName, idTramite)).objectResponse
        deleteScheduleDao(idTramite)
        saveScheduleDao(schedule)
        Result.success(schedule?.map { it.mapToDomain() })
    }

    //Guardar horas en DB local
    private suspend fun saveScheduleDao(data: List<ScheduleDto>?) {
        data?.onEach { schedule ->
            val model = schedule.mapToDb()
            visitasDao.insertSchedule(model)
        }
    }

    //Eliminar horas en DB local
    private suspend fun deleteScheduleDao(idTramite: Int) {
        visitasDao.deleteScheduleByTramit(idTramite)
    }

    //Carga de horas por visita DB local
    override suspend fun loadScheduleLocal(idTramite: Int): Result<List<ScheduleTempEntity>> =
        Result.success(
            visitasDao.getQuantityHoursByTramite(idTramite)?.map { it.mapToDomain() }
                ?: emptyList()
        )

    //Sync Location
    override suspend fun syncLocation(): Result<Unit> = execute {
        val item = visitasDao.selectLocation().orEmpty()
        val result = if (!item.isNullOrEmpty()) {
            item.mapNotNull {
                visitApi.location(
                    LocationBodyDto(
                        it.latitud,
                        it.longitud,
                        it.idPersona,
                        it.fechaCreacion,
                        it.usuarioCrea
                    )
                )
                visitasDao.deleteLocationById(it.id)
            }
        } else {
            Unit
        }
        Result.success(result)
    } as Result<Unit>

    //Location
    override suspend fun saveLocation(latitude: String, longitude: String): Result<Unit> = execute {
        val idPersona = userRepository.userInformation.value?.idPersona.orZero()
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val result = visitApi.location(
            LocationBodyDto(
                latitude,
                longitude,
                idPersona,
                getDateHourWithOutFormat(),
                userName
            )
        )
        Result.success(result)
    }

    //Location BD local
    override suspend fun saveLocationLocal(latitude: String, longitude: String): Result<Unit> =
        execute {
            val idPersona = userRepository.userInformation.value?.idPersona.orZero()
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val result = visitasDao.insertLocation(
                LocationBodyDto(
                    latitude,
                    longitude,
                    idPersona,
                    getDateHourWithOutFormat(),
                    userName
                ).mapToDb()
            )
            Result.success(result)
        }

    //Crear antecedente
    override suspend fun createAntecedent(body: AntecedentBodyEntity): Result<Unit> = execute {
        val result = visitApi.createAntecedent(body.mapToData())
        Result.success(result)
    }

    override suspend fun loadHolidays(): Result<List<HolidaysEntity>> = execute {
        val holiday = masterApi.getHolidays().objectResponse
        saveHolidaysDao(holiday)
        Result.success(holiday.map { it.mapToDomain() })
    }
    private suspend fun saveHolidaysDao(data: List<HolidayDto>) {
        data.onEach { holiday ->
            val model = holiday.mapToDb()
            visitasDao.insertHoliday(model)
        }
    }
  override suspend fun loadShiftType(): Result<List<ShiftTypeEntity>> = execute {
        val shiftType = masterApi.getShiftType().objectResponse
        saveShiftTypeDao(shiftType)
        Result.success(shiftType.map { it.mapToDomain() })
    }

    private suspend fun saveShiftTypeDao(data: List<ShiftTypeDto>) {
        data.onEach { shiftType ->
            val model = shiftType.mapToDb()
            visitasDao.insertShiftType(model)
        }
    }

    override suspend fun loadTypeHoursBilling(): Result<List<TypeHoursBillingEntity>> = execute {
        val typeHoursBilling = masterApi.getTypeHoursBilling().objectResponse
        saveTypeHoursBillingDao(typeHoursBilling)
        Result.success(typeHoursBilling.map { it.mapToDomain() })
    }

    private suspend fun saveTypeHoursBillingDao(data: List<TypeHoursBillingDto>) {
        data.onEach { typeHoursBilling ->
            val model = typeHoursBilling.mapToDb()
            visitasDao.insertTypeHoursBilling(model)
        }
    }
    override suspend fun getIdTramite(idVisit: Int): Int?  {
        val result = visitasDao.selectTramite(idVisit)?.idTramite
        return result
    }
    override suspend fun getJordana(jornada: String): String?{
        return visitasDao.selectJornada(jornada)?.horaInicio
    }
    override suspend fun existsEventDate(fecha: String): Boolean  {
       return visitasDao.existsEventDate(fecha)

    }

    override suspend fun saveScheduleTemp(idTramite: Int){
        val result = visitasDao.getQuantityHoursByTramite(idTramite)
        result.onEach { data ->
            visitasDao.insertScheduleTemp(data)
        }
    }
    override suspend fun getQuantityHoursByCode(code: String): Double?{
        val result = visitasDao.getQuantityHoursByCode(code)?.cantidadHoras
        return result
    }
    override suspend fun saveSchedule(list: List<ScheduleEntity>){
        val horaFacturacionTramites = list.map { value ->
            val cantidad = value.cantidadHoras

            ScheduleModel(
                idTipoHoraFacturacion = value.idTipoHoraFacturacion,
                idTramite = value.idTramite,
                tipoHora = value.tipoHora,
                cantidadHoras = cantidad,
                codigo = value.codigo
            )
        }
        val result = visitasDao.insertScheduleList(horaFacturacionTramites)
        return result
    }

    override suspend fun getTypeHoursBilling(idType: Int): TypeHoursBillingEntity {
        val result = visitasDao.getTypeHoursBilling(idType)
        return result.mapToDomain()
    }

    override suspend fun deleteAllScheduleTemp() {
      visitasDao.deleteAllScheduleTemp()
    }

    override suspend fun updateSchedule(): Result<List<Unit>> = execute {
        val query = visitasDao.selectScheduleForUpdate()
        val result = query?.groupBy { it.idTramite }?.map { (idTramite, scheduleModels) ->
            val tipoHoraList = scheduleModels.map { scheduleModel ->
               ScheduleUploadDto(
                    idTipoHora = scheduleModel.idTipoHoraFacturacion,
                    cantidadHoras = scheduleModel.cantidadHoras?.toInt()
                )
            }
            val scheduleUploadDto = BodyScheduleUploaDto(
                tipoHoraList,
                idTramite
            )
            inperApi.updateSchedule(scheduleUploadDto)
             deleteScheduleDao(idTramite.toString().toInt())
        }.orEmpty()
        Result.success(result)
    }
}