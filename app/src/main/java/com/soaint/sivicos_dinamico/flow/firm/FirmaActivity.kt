package com.soaint.sivicos_dinamico.flow.firm

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.utils.FirmaView

class FirmaActivity : BaseActivity() {
    private var firmaView: FirmaView? = null
    private var contFirma: CardView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_firma)

        contFirma = findViewById(R.id.contFirma)
        firmaView = FirmaView(applicationContext)
        contFirma!!.addView(firmaView)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_firma, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_salir -> finish()
            R.id.action_borrar -> firmaView!!.clearSignature()
            R.id.action_aceptar -> {
                val ruta = firmaView!!.signature

                val _result = Intent()
                _result.putExtra("bmp", ruta)
                setResult(RESULT_OK, _result)
                finish()
            }
        }
        return true
    }
}