package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.ScheduleEntity
import com.soaint.domain.model.ScheduleTempEntity

@Entity(tableName = "schedule_table")
class ScheduleModel(
    @ColumnInfo(name = "IdTramite") val idTramite: Int?,
    @ColumnInfo(name = "IdTipoHoraFacturacion") val idTipoHoraFacturacion: Int?,
    @ColumnInfo(name = "Codigo") val codigo: String?,
    @ColumnInfo(name = "tipoHora") val tipoHora:String?,
    @ColumnInfo(name = "cantidadHoras") val cantidadHoras: Double?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    )
@Entity(tableName = "schedule_temp_table")
class ScheduleModelTemp(
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "cantidadHoras") val cantidadHoras: Double?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun ScheduleModel.mapToDomain() = ScheduleEntity(
    idTramite,
    idTipoHoraFacturacion,
    codigo,
    tipoHora,
    cantidadHoras,
)fun ScheduleModelTemp.mapToDomain() = ScheduleTempEntity(
    codigo,
    cantidadHoras,
)