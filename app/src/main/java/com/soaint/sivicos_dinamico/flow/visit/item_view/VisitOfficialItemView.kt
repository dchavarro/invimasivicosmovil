package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.VisitOfficialEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemVisitOfficialBinding
import com.xwray.groupie.databinding.BindableItem

class VisitOfficialItemView(
    private val official: VisitOfficialEntity,
) : BindableItem<ItemVisitOfficialBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemVisitOfficialBinding,
        position: Int
    ) = with(viewBinding) {
        textViewUser.text = official.usuario?.uppercase()
        textViewName.text = official.nombreCompleto?.uppercase()
        textViewDocument.text = official.tipoDocumento + " - " + official.numeroDocumento
        textViewJob.text = official.cargo
    }

    override fun getLayout() = R.layout.item_visit_official
}