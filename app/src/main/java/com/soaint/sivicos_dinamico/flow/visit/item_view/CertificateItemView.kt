package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.CertificateEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemCertificateBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class CertificateItemView(
    private val item: CertificateEntity,
    private val onSelected: (CertificateEntity, Boolean) -> Unit,
) : BindableItem<ItemCertificateBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemCertificateBinding,
        position: Int
    ) = with(viewBinding) {
        val context = root.context

        textViewName.text = item.tipoProducto
        textViewTypeDoc.text = item.grupoProducto

        textViewProfesion.text = item.rol
        textViewCargo.text = item.concepto

        checkBox.isChecked = !item.estado.isNullOrEmpty()

        root.setOnSingleClickListener {
            checkBox.isChecked = !checkBox.isChecked
            onSelected.invoke(item, checkBox.isChecked)
        }
    }

    override fun getLayout() = R.layout.item_certificate
}