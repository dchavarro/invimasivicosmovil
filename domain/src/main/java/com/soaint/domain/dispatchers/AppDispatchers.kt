package com.soaint.domain.dispatchers

import kotlinx.coroutines.CoroutineDispatcher

interface AppDispatchers {

    fun mainDispatcher(): CoroutineDispatcher

    fun networkDispatcher(): CoroutineDispatcher
}