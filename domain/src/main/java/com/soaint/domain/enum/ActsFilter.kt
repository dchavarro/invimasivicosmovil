package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.FILTER_MSS
import com.soaint.sivicos_dinamico.utils.FILTER_PAPF



enum class ActsFilter(val filter: Array<String>) {
    ACTSFILTERPAPF(
        arrayOf(
            FILTER_PAPF
        )
    ),
    ACTSFILTERMSS(
    arrayOf(
        FILTER_MSS
    )
    )
}
