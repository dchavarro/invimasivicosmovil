package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.interaction

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.orZero
import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.InteractionEntity
import com.soaint.domain.model.RsEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCloseInteractionBinding
import com.soaint.sivicos_dinamico.extensions.hideKeyboard
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.CertificateItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssSearchItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.RsItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateRadioGroup
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class InteractionFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCloseInteractionBinding>(R.layout.fragment_close_interaction)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: InteractionViewModel by viewModels { viewModelFactory }

    private val adapterList by lazy { GroupieAdapter() }

    private val adapterSearch by lazy { GroupieAdapter() }

    private val adapterRs by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_close_interaction))

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterList

        binding.rvSearch.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvSearch.adapter = adapterSearch

        binding.rvRs.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvRs.adapter = adapterRs

        binding.includeEmitirCertificacion.yes.setOnSingleClickListener { viewModel.setEmitirCertificacion(true) }
        binding.includeEmitirCertificacion.no.setOnSingleClickListener { viewModel.setEmitirCertificacion(false) }

        binding.includeResponsabilidadSanitaria.yes.setOnSingleClickListener { viewModel.setResponsabilidadSanitaria(true) }
        binding.includeResponsabilidadSanitaria.no.setOnSingleClickListener { viewModel.setResponsabilidadSanitaria(false) }

        binding.includeTomaMuestras.yes.setOnSingleClickListener { viewModel.setTomaMuestras(true) }
        binding.includeTomaMuestras.no.setOnSingleClickListener { viewModel.setTomaMuestras(false) }

        binding.includeRetiroProducto.yes.setOnSingleClickListener { viewModel.setRetiroProducto(true, true) }
        binding.includeRetiroProducto.no.setOnSingleClickListener { viewModel.setRetiroProducto(false, true) }

        binding.includeSometimiento.yes.setOnSingleClickListener { viewModel.setSometimiento(true, true) }
        binding.includeSometimiento.no.setOnSingleClickListener { viewModel.setSometimiento(false, true) }

        binding.includePerdida.yes.setOnSingleClickListener { viewModel.setPerdida(true) }
        binding.includePerdida.no.setOnSingleClickListener { viewModel.setPerdida(false) }

        binding.includeTenencia.yes.setOnSingleClickListener { viewModel.setTenencia(true) }
        binding.includeTenencia.no.setOnSingleClickListener { viewModel.setTenencia(false) }

        binding.includeComercializazdo.yes.setOnSingleClickListener { viewModel.setComercializado(true, true) }
        binding.includeComercializazdo.no.setOnSingleClickListener { viewModel.setComercializado(false, true) }

        binding.buttonSave.setOnSingleClickListener { viewModel.checkData(); requireActivity().hideKeyboard() }

        binding.buttonAdd.setOnSingleClickListener { viewModel.onInsert() }

        binding.buttonSearch.setOnSingleClickListener {
            if (binding.editTextSearch.text.toString().isNotEmpty()) {
                viewModel.onSearch(binding.editTextSearch.text.toString())
                binding.textInputLayoutSearch.isErrorEnabled = false
                binding.textInputLayoutSearch.error = null
            } else {
                binding.textInputLayoutSearch.isErrorEnabled = true
                binding.textInputLayoutSearch.error = getString(R.string.campo_obligatorio)
            }
            requireActivity().hideKeyboard()
        }

        binding.textInputLayoutSearch.setEndIconOnClickListener {
            binding.editTextSearch.text?.clear()
            binding.cardView.isVisible = false
        }

    }

    private fun observerViewModelEvents() {
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
    }

    private fun eventHandler(event: onInteractionViewModelEvent) {
        showLoading(false)
        when (event) {
            is onInteractionViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onInteractionViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onInteractionViewModelEvent.onSnackLoadingError -> onSnackLoadingError(event.message)
            is onInteractionViewModelEvent.OnViewModelReady -> OnViewModelReady(event.interaction)
            is onInteractionViewModelEvent.onViewModelReadyProduct -> onViewModelReadyProduct(event.interactionProduct)
            is onInteractionViewModelEvent.OnViewModelSuccess -> onSuccess(event.message, event.finished)
            is onInteractionViewModelEvent.onViewModelReadySearch -> onViewModelReadySearch(event.search)
            is onInteractionViewModelEvent.onViewModelInsertRs -> onViewModelInsertRs()
            is onInteractionViewModelEvent.onViewModelReadyRs -> onViewModelReadyRs(event.rs)
            is onInteractionViewModelEvent.onValidateEvent -> {
                val type = event.validateData
                when (type) {
                    is ValidateInteraction.OnValidateEmitirCertificado -> binding.apply {
                        includeEmitirCertificacion.root.isVisible = type.isVisible
                        includeEmitirCertificacion.checked = type.isChecked
                        if (type.enableValidation) validateRadioGroup(includeEmitirCertificacion.radiogroup, type.emitirCertificado)
                    }
                    is ValidateInteraction.OnValidateResponsabilidadSanitaria -> binding.apply {
                        includeResponsabilidadSanitaria.root.isVisible = type.isVisible
                        includeResponsabilidadSanitaria.checked = type.isVisible
                        if (type.enableValidation) validateRadioGroup(includeResponsabilidadSanitaria.radiogroup, type.responsabilidadSanitaria)
                    }
                    is ValidateInteraction.OnValidateTomaMuestras -> binding.apply {
                        includeTomaMuestras.root.isVisible = type.isVisible
                        type.tomaMuestra?.let { includeTomaMuestras.checked = it }
                        if (type.enableValidation) validateRadioGroup(includeTomaMuestras.radiogroup, type.tomaMuestra)
                    }
                    is ValidateInteraction.OnValidateRequiereRetiro -> binding.apply {
                        includeRetiroProducto.root.isVisible = type.isVisible
                        includeRetiroProducto.checked = type.isChecked ?: type.requiereRetiro
                        if (type.enableValidation) validateRadioGroup(includeRetiroProducto.radiogroup, type.requiereRetiro)
                    }
                    is ValidateInteraction.OnValidateSometimiento -> binding.apply {
                        includeSometimiento.root.isVisible = type.isVisible
                        includeSometimiento.checked = type.sometimiento
                        if (type.enableValidation) validateRadioGroup(includeSometimiento.radiogroup, type.sometimiento)
                    }
                    is ValidateInteraction.OnValidateReCall -> binding.apply {
                        textInputRecall.isVisible = type.isVisible
                        editTextRecall.text = type.recall?.toEditable()
                    }
                    is ValidateInteraction.OnValidatePerdidaCertificacion -> binding.apply {
                        includePerdida.root.isVisible = type.isVisible
                        includePerdida.checked = type.isChecked ?: type.perdidaCertificacion
                        rvList.isVisible = (type.isChecked ?: type.perdidaCertificacion) == false
                        if (type.enableValidation) validateRadioGroup(includePerdida.radiogroup, type.perdidaCertificacion)
                    }
                    is ValidateInteraction.OnValidateProductoTenenciaInvima -> binding.apply {
                        includeTenencia.root.isVisible = type.isVisible
                        includeTenencia.checked = type.isChecked ?: type.productoTenenciaInvima
                        if (type.enableValidation) validateRadioGroup(includeTenencia.radiogroup, type.productoTenenciaInvima)
                    }
                    is ValidateInteraction.OnValidateProductoComercializado -> binding.apply {
                        includeComercializazdo.root.isVisible = type.isVisible
                        type.productoComercializado?.let { includeComercializazdo.checked = it }
                        if (type.enableValidation) validateRadioGroup(includeComercializazdo.radiogroup, type.productoComercializado)
                    }
                    is ValidateInteraction.OnValidateSearch -> binding.apply {
                        textInputLayoutSearch.isVisible = type.isVisible
                        textViewTitleSearch.isVisible = type.isVisible
                        buttonSearch.isVisible = type.isVisible
                    }
                }
            }
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun OnViewModelReady(interaction: InteractionEntity?) {
        if (interaction != null) {
            viewModel.setEmitirCertificacion(interaction.emitirCertificacion)
            viewModel.setResponsabilidadSanitaria(interaction.responsabilidadSanitaria)
            viewModel.setTomaMuestras(interaction.tomaMuestras)
            viewModel.setRetiroProducto(interaction.requiereRetiroProducto)
            viewModel.setSometimiento(interaction.realizoSometimiento)
            viewModel.setPerdida(interaction.perdidaCertificacion)
            viewModel.setTenencia(interaction.productoTenenciaInvima)
            viewModel.setComercializado(interaction.productoComercializado)
            viewModel.setReCall(interaction.identificadorRecall?.toEditable())
        }
    }

    private fun onViewModelReadyProduct(msspList: List<CertificateEntity>?) {
        adapterList.clear()
        val items = mutableListOf<Group>()

        if (!msspList.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_close_interaction_title)))
            items.addAll(msspList.map { CertificateItemView(it, ::onItemSelected) })
        }

        val isVisible = msspList.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
        adapterList.updateAsync(items)
    }

    private fun onItemSelected(item: CertificateEntity, isSelected: Boolean) {
        viewModel.setItemSelected(item.idCertificado.orZero(), isSelected)
    }

    private fun onViewModelReadySearch(nombreProducto: String) {
        val isVisible = nombreProducto.isNullOrEmpty()
        if (isVisible) {
            onSnackLoadingError(R.string.copy_close_visit_error_search)
        } else {
            adapterSearch.clear()
            val items = mutableListOf<Group>()
            items.addAll(
                listOf(
                    MssSearchItemView(
                        nombreProducto,
                        binding.editTextSearch.text.toString(),
                        ::onSearchSelected
                    )
                )
            )
            binding.cardView.isVisible = !isVisible
            adapterSearch.updateAsync(items)
        }
    }

    private fun onSearchSelected(nombreProducto: String, registroSanitario: String) {
        viewModel.setSearchSelected(nombreProducto, registroSanitario)
    }

    private fun onViewModelReadyRs(rs: List<RsEntity>?) {
        adapterRs.clear()
        val items = mutableListOf<Group>()

        if (!rs.isNullOrEmpty()) {
            items.addAll(rs.map { RsItemView(it, ::onRsSelected) })
        }

        val isVisible = rs.isNullOrEmpty()
        binding.rvRs.isVisible = !isVisible
        adapterRs.updateAsync(items)
    }

    private fun onRsSelected(item: RsEntity) {}

    private fun onViewModelInsertRs() {
        onSuccess(R.string.copy_update_success)
        binding.editTextSearch.text?.clear()
        binding.cardView.isVisible = false
    }

    private fun onSuccess(@StringRes message: Int, finished: Boolean = false) {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
        if (finished) requireActivity().onBackPressed()
    }

    private fun onSnackLoadingError(@StringRes message: Int) {
        requireActivity().showSnackBar(
            type = AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}