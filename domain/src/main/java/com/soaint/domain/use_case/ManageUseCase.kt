package com.soaint.domain.use_case

import com.soaint.domain.repository.ManageRepository
import javax.inject.Inject

class ManageUseCase @Inject constructor(
    private val manageRepository: ManageRepository,
) {

    suspend fun loadGroupPapf() = manageRepository.loadGroupPapf()

    suspend fun loadGroup(idTipoProducto: Int) = manageRepository.loadGroup(idTipoProducto)

    suspend fun loadGroupLocal(idTipoProducto: Int?) = manageRepository.loadGroupLocal(idTipoProducto)

    suspend fun loadGroupDocument() = manageRepository.loadGroupDocument()

    suspend fun loadGroupDocumentLocal() = manageRepository.loadGroupDocumentLocal()

    suspend fun loadRulesBussiness(idGroup: Int, idTipoProducto: Int) = manageRepository.loadRulesBussiness(idGroup, idTipoProducto)

    suspend fun loadRulesBussinessLocal(idGroup: Int, idTipoProductoPapf: Int?) = manageRepository.loadRulesBussinessLocal(idGroup, idTipoProductoPapf)

    suspend fun loadRulesBussinessLocal(idPlantilla: String) = manageRepository.loadRulesBussinessLocal(idPlantilla)

    suspend fun loadRulesBussinessPapf(idTipoProductoPapf: Int, filter: String) = manageRepository.loadRulesBussinessPapf(idTipoProductoPapf, filter)

}