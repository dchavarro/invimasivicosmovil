package com.soaint.sivicos_dinamico.flow.visit.views.register_company

import android.app.Activity
import android.os.Bundle
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentRegisterCompanyBinding
import com.soaint.sivicos_dinamico.extensions.selectValue
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.MssItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.PersonItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateEmailField
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class RegisterCompanyFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentRegisterCompanyBinding>(R.layout.fragment_register_company)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: RegisterCompanyViewModel by activityViewModels { viewModelFactory }

    private val adapterCheck by lazy { GroupieAdapter() }

    private val adapterPerson by lazy { GroupieAdapter() }

    private var textWatcher: TextWatcher? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_register_company))

        adapterCheck.spanCount = 2
        binding.rvCheck.layoutManager =
            GridLayoutManager(context, adapterCheck.spanCount).apply {
                spanSizeLookup = adapterCheck.spanSizeLookup
            }
        binding.rvCheck.adapter = adapterCheck

        binding.rvList.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rvList.adapter = adapterPerson

        binding.textInputAddress.setOnSingleClickListener {
            showDialogAddress()
        }
        binding.editTextAddress.setOnSingleClickListener {
            showDialogAddress()
        }
        binding.buttonAdd.setOnSingleClickListener {
            viewModel.checkPersonData()
        }
        binding.buttonSave.setOnSingleClickListener {
            viewModel.checkCompanyData()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.address.observe(viewLifecycleOwner, ::setAddress)
    }

    private fun setAddress(a: String?) {
        if (textWatcher != null) {
            binding.textInputAddress.editText?.removeTextChangedListener(textWatcher)
        }
        binding.textInputAddress.editText?.setText(a.orEmpty())
        textWatcher = binding.textInputAddress.editText?.doAfterTextChanged {
            viewModel.onAddressChange(it)
        }
    }

    private fun eventHandler(event: OnRegisterCompanyViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnRegisterCompanyViewModelEvent.OnChangeLabelPreloader -> showLoading(
                true,
                event.label
            )
            is OnRegisterCompanyViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnRegisterCompanyViewModelEvent.onSnack -> {
                showSnack(
                    event.isSuccess,
                    event.message
                ); if (event.isSuccess) requireActivity().onBackPressed(); mainViewModel.refreshVisit()
            }
            is OnRegisterCompanyViewModelEvent.onViewModelReadyTypeDoc -> onViewModelReadyTypeDoc(
                event.typeDoc
            )
            is OnRegisterCompanyViewModelEvent.onViewModelReadyCountries -> onViewModelReadyCountries(
                event.countries,
                event.countrySelected,
                event.department,
                event.departmentSelected,
                event.town,
                event.townSelected,
            )
            is OnRegisterCompanyViewModelEvent.onViewModelReadyTypeProduct -> onViewModelReadyTypeProduct(
                event.typeProduct
            )
            is OnRegisterCompanyViewModelEvent.onViewModelReadyRol -> onViewModelReadyRol(event.rol)
            is OnRegisterCompanyViewModelEvent.onViewModelReadyPerson -> onViewModelReadyPerson(
                event.person
            )
            is OnRegisterCompanyViewModelEvent.OnValidatePersonData ->
                validateErrorData(
                    event.namePerson,
                    event.idTipoDocumentoPerson,
                    event.numeroDocumentoPerson,
                    event.rolId,
                    event.email
                )
            is OnRegisterCompanyViewModelEvent.OnValidateCompanyData ->
                validateCompanyData(
                    event.idTipoDocumento,
                    event.numeroDocumento,
                    event.departamentId,
                    event.townId,
                    event.address
                )
            is OnRegisterCompanyViewModelEvent.onViewModelReadyCompany -> onViewModelReadyCompany(
                event.company
            )
            is OnRegisterCompanyViewModelEvent.onViewModelReadyDepartment -> onReadyDepartment(event.department)

            is OnRegisterCompanyViewModelEvent.onViewModelReadyTown -> onReadyTown(event.town)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReadyCompany(company: RegisterCompanyBodyEntity?) {
        if (company != null) {

            val getTypeDoc =
                viewModel.setTypeDocSelected(company.idTipoDocumento)?.descripcion.orEmpty()
            binding.spinnerTypeDoc.selectValue(getTypeDoc)
            viewModel.setTypeDocSelected(getTypeDoc)

            binding.editTextDoc.setText(company.numeroDocumento)
            viewModel.onDocChange(company.numeroDocumento.orEmpty().toEditable())

            binding.editTextDigit.setText(company.digitoVerificacion.orZero().toString())
            viewModel.onDigitChange(company.digitoVerificacion.orZero().toString().toEditable())

            binding.editTextCompany.setText(company.razonSocial)
            viewModel.onCompanyNameChange(company.razonSocial.orEmpty().toEditable())

            binding.editTextSigla.setText(company.nombreComercial)
            viewModel.onComercialNameChange(company.nombreComercial.orEmpty().toEditable())

            binding.editTextWeb.setText(company.paginaWeb)
            viewModel.onPageWebChange(company.paginaWeb.orEmpty().toEditable())

            binding.editTextEmail.setText(company.correoElectronico)
            viewModel.onEmailChange(company.correoElectronico.orEmpty().toEditable())

            binding.editTextTelephone.setText(company.telefono)
            viewModel.onTelephoneChange(company.telefono.orEmpty().toEditable())

            binding.editTextCellphone.setText(company.celular)
            viewModel.onCellphoneChange(company.celular.orEmpty().toEditable())

            binding.editTextAddress.setText(company.direccion)
            viewModel.onAddressChange(company.direccion.orEmpty().toEditable())

        }
    }

    private fun onViewModelReadyTypeDoc(typeDoc: List<TypeDaoEntity>) {
        val array = typeDoc.toMutableList()
        array.add(0, TypeDaoEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerTypeDoc.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerTypeDoc.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setTypeDocSelected(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        binding.spinnerPersonTypeDoc.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerPersonTypeDoc.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setTypeDocPersonSelected(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyCountries(
        countries: List<CountriesEntity>?,
        countrySelected: CountriesEntity?,
        departments: List<DepartmentEntity>?,
        departmentSelected: DepartmentEntity?,
        towns: List<TownEntity>?,
        townSelected: TownEntity?,
    ) {
        countries?.let { onReadyCountry(it) }
        countrySelected?.let {
            binding.spinnerCountry.selectValue(it.nombre.orEmpty())
        }

        departments?.let { onReadyDepartment(it) }
        departmentSelected?.let {
            binding.spinnerDepartment.selectValue(it.nombre.orEmpty())
        }

        towns?.let { onReadyTown(it) }
        townSelected?.let {
            binding.spinnerTown.selectValue(it.nombre.orEmpty())
        }

        binding.spinnerCountry.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>,
                    view: View,
                    position: Int,
                    id: Long
                ) {
                    val oldCountrySelected = viewModel.countrySelected?.copy()
                    val newCountrySelected = viewModel.setCountrySelected(position)
                    if (oldCountrySelected != newCountrySelected) {
                        viewModel.onGetDepartment()
                        viewModel.onGetTown()
                    }
                }

                override fun onNothingSelected(parent: AdapterView<*>) {}
            }

        binding.spinnerDepartment.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    val oldDepartmentSelected = viewModel.departmentSelected?.copy()
                    val newDepartmentSelected = viewModel.setDepartmentSelected(position - 1)
                    if (oldDepartmentSelected != newDepartmentSelected) {
                        viewModel.onGetTown()
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }

        binding.spinnerTown.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                if (position != 0) {
                    val oldTownSelected = viewModel.townSelected?.copy()
                    val newTownSelected = viewModel.setTownSelected(position - 1)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onReadyCountry(countries: List<CountriesEntity>) {
        val array = countries.toMutableList()
        val items = array.mapNotNull { it.nombre }
        binding.spinnerCountry.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
    }

    private fun onReadyDepartment(department: List<DepartmentEntity>) {
        val array = department.toMutableList()
        array.add(0, DepartmentEntity(nombre = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.nombre }
        binding.spinnerDepartment.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
    }

    private fun onReadyTown(town: List<TownEntity>) {
        val array = town.toMutableList()
        array.add(0, TownEntity(nombre = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.nombre }
        binding.spinnerTown.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
    }

    private fun onViewModelReadyTypeProduct(typeProduct: List<ManageMssEntity>) {
        adapterCheck.clear()
        val items = mutableListOf<Group>()
        if (!typeProduct.isNullOrEmpty()) {
            items.addAll(typeProduct.map { MssItemView(it, ::onTypeProductSelected) })
        }

        val isVisible = typeProduct.isNullOrEmpty()
        binding.rvCheck.isVisible = !isVisible
        adapterCheck.updateAsync(items)
    }

    private fun onTypeProductSelected(mssp: ManageMssEntity, isSelected: Boolean) {
        viewModel.setTypeProductSelected(mssp.id.orZero(), isSelected)
    }

    private fun onViewModelReadyRol(rol: List<ManageMssEntity>) {
        val array = rol.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerPersonRol.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerPersonRol.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setRolSelected(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyPerson(person: List<VisitCompanyPersonEntity>) {
        adapterPerson.clear()
        val items = mutableListOf<Group>()
        if (!person.isNullOrEmpty()) {
            items.addAll(person.map { PersonItemView(it, ::onDelete) })
        }

        val isVisible = person.isNullOrEmpty()
        binding.rvList.isVisible = !isVisible
        adapterPerson.updateAsync(items)
    }

    private fun onDelete(person: VisitCompanyPersonEntity) {
        viewModel.onDeletePerson(person)
    }

    private fun validateErrorData(
        namePerson: String?,
        idTipoDocumentoPerson: Int?,
        numeroDocumentoPerson: String?,
        rolId: Int?,
        email: String?
    ) {
        validateField(binding.textInputPersonName, namePerson)
        validateSpinner(binding.spinnerPersonTypeDoc, idTipoDocumentoPerson)
        validateField(binding.textInputPersonDoc, numeroDocumentoPerson)
        validateSpinner(binding.spinnerPersonRol, rolId)
        validateEmailField(binding.textInputPersonEmail, email)
    }

    private fun validateCompanyData(
        idTipoDocumento: Int?,
        numeroDocumento: String?,
        departamentId: Int?,
        townId: Int?,
        address: String?
    ) {
        validateSpinner(binding.spinnerTypeDoc, idTipoDocumento)
        validateField(binding.textInputDoc, numeroDocumento)
        validateSpinner(binding.spinnerDepartment, departamentId)
        validateSpinner(binding.spinnerTown, townId)
        validateField(binding.textInputAddress, address)
    }

    private fun cleanPerson() {
        binding.editTextPersonName.text?.clear()
        binding.editTextPersonDoc.text?.clear()
        binding.editTextPersonEmail.text?.clear()
        binding.textInputPersonEmail.error = null
        viewModel.onGetRol()
        viewModel.onGetTypeDoc()
    }
}