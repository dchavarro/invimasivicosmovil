package com.soaint.domain.use_case

import com.soaint.domain.repository.PlantillasRepository
import javax.inject.Inject

class PlantillasUseCase @Inject constructor(
    private val plantillasRepository: PlantillasRepository
) {

    val plantillas get() = plantillasRepository.plantillas

    suspend fun loadPlantillas(idTipoProducto: Int) = plantillasRepository.loadPlantillas(idTipoProducto)

    suspend fun loadPlantillasLocal(idPlantilla: String) =
        plantillasRepository.loadPlantillasLocal(idPlantilla)

    suspend fun loadAllPlantillasLocal() =
        plantillasRepository.loadPlantillasLocal()

    suspend fun loadBlocks(idPlantilla: String) = plantillasRepository.loadBlocks(idPlantilla)

    suspend fun loadBlocksLocal(idPlantilla: String) =
        plantillasRepository.loadBlockLocal(idPlantilla)

    suspend fun saveBlockBody(idPlantilla: String, idVisita: String, body: String, id: Int) =
        plantillasRepository.saveBlockBody(idPlantilla, idVisita, body, id)

    suspend fun getBlockBody(idPlantilla: String, idVisita: String) =
        plantillasRepository.getBlockBody(idPlantilla, idVisita)
    suspend fun selectBodyBlockUnfinished(idPlantilla: String, idVisita: String) =
        plantillasRepository.selectBodyBlockUnfinished(idPlantilla, idVisita)
    suspend fun isFinishedAct(idPlantilla: String, idVisita: String) =
        plantillasRepository.isFinishedAct(idPlantilla, idVisita)

    suspend fun saveFinishedActa(idPlantilla: String, idVisita: String, id: Int) =
        plantillasRepository.saveFinishedActa(idPlantilla, idVisita, id)

    suspend fun getAllBlockBody() = plantillasRepository.getAllBlockBody()

    suspend fun deleteBlockBody(idPlantilla: String, idVisita: String, id: Int) =
        plantillasRepository.deleteBlockBody(idPlantilla, idVisita, id)

    suspend fun isEditableActa(idPlantilla: String, idVisita: String, id: Int) =
        plantillasRepository.isEditableActa(idPlantilla, idVisita, id)

}