package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.denunciation

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.AntecedentEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentDenunciationBinding
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.flow.visit.item_view.DenunciationItemView
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.close.onCloseVisitViewModelEvent
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ShowAlert
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class DenunciationFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentDenunciationBinding>(R.layout.fragment_denunciation)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: DenunciationViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.copy_close_denuncia))

        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter

    }

    private fun observerViewModelEvents() {
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
    }

    private fun eventHandler(event: onDenunciationViewModelEvent) {
        showLoading(false)
        when (event) {
            is onDenunciationViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onDenunciationViewModelEvent.onLoadingError -> onTokenError(event.error)
            is onDenunciationViewModelEvent.OnViewModelReady -> OnViewModelReady(event.denunciation)
            is onDenunciationViewModelEvent.OnViewModelSuccess -> onSuccess(event.message)
            else -> {}
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun OnViewModelReady(antecedent: List<AntecedentEntity>?) {
        adapter.clear()
        val items = mutableListOf<Group>()
        val isVisible = antecedent.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        if (!antecedent.isNullOrEmpty()) {
            items.addAll(antecedent.map { DenunciationItemView(it, ::onRadioSelected, ::showDialog, ::setApply) })
        }

        adapter.updateAsync(items)
    }

    private fun setApply(apply: Int): String {
        return viewModel.setApplySelectedByID(apply)
    }

    private fun onRadioSelected(isSelected: Boolean, antecedent: AntecedentEntity) {
        viewModel.setIsSelected(isSelected, antecedent)
    }

    private fun showDialog(antecedent: AntecedentEntity) {
        val array = viewModel.apply.value?.toMutableList()
        val items = array?.mapNotNull { it.descripcion }?.toTypedArray()
        showListDialog(R.string.copy_option, items as Array<String>) { position ->
            val selected = items[position]
            viewModel.setApplySelected(selected, antecedent)
        }
    }

    private fun onSuccess(@StringRes message: Int) {
        requireActivity().showSnackBar(
            type = AlertTypes.SUCCESS,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}