package com.soaint.domain.use_case

import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.repository.DocumentRepository
import javax.inject.Inject

class DocumentUseCase @Inject constructor(
    private val documentRepository: DocumentRepository
) {

    val document get() = documentRepository.document

    fun getDocuments() = documentRepository.documents.value

    suspend fun loadDocument(idVisit: String): Result<List<DocumentEntity>> =
        documentRepository.loadDocument(idVisit)

    suspend fun loadDocumentLocal(idVisit: String) = documentRepository.loadDocumentLocal(idVisit)

    suspend fun loadDocumentInView(idVisit: String): Result<List<DocumentEntity>> {
        val result = loadDocument(idVisit)
        return when (result.exceptionOrNull()) {
            is Exception -> loadDocumentLocal(idVisit)
            else -> result
        }
    }

    suspend fun loadDocumentName(idGrupo: Int) = documentRepository.loadDocumentName(idGrupo)

    suspend fun loadDocumentNameLocal(idGrupo: Int) =
        documentRepository.loadDocumentNameLocal(idGrupo)

    suspend fun addDocument(body: DocumentBodyEntity): Result<Unit> {
        return documentRepository.addDocument(body)
/*        val result = documentRepository.addDocument(body)
        return when (result.exceptionOrNull()) {
            is WithoutConnectionException -> addDocumentLocal(body)
            else -> result
        }*/
    }

    suspend fun addDocumentLocal(body: DocumentBodyEntity) =
        documentRepository.addDocumentLocal(body)

    suspend fun getDocumentBodyLocalByVisit(idVisit: String) =
        documentRepository.getDocumentBodyLocalByVisit(idVisit)

    suspend fun getDocumentBodyLocalById(idDocumentBody: String) =
        documentRepository.getDocumentBodyLocalById(idDocumentBody)

    suspend fun getDocumentActLocal(idPlantilla: Int, idVisita: Int) =
        documentRepository.getDocumentActLocal(idPlantilla,idVisita )
    suspend fun deleteDocument(id: Int, url: String?, idVisita: String, idPlantilla: String?) =
        documentRepository.deleteDocument(id, url, idVisita, idPlantilla)

    suspend fun addPLantilla2DocumentLocal(body: DocumentEntity, idVisita: String) =
        documentRepository.addPLantilla2DocumentLocal(body, idVisita)

    suspend fun getDocumentUpload() = documentRepository.getDocumentUpload()

    suspend fun viewDocument(idDocument: Int): Result<String> {
        return documentRepository.viewDocument(idDocument)
    }

}