package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.CertificateModel
import com.soaint.data.room.tables.TechnicalModel
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.TechnicalEntity

@Dao
interface TechnicalDao {

    @Query("SELECT * FROM technical_table WHERE idVisita = :idVisita ")
    suspend fun selectTechnical(idVisita: Int): List<TechnicalModel>?

    @Query("SELECT * FROM technical_table WHERE idPersona = :idPersona")
    suspend fun selectTechnicalById(idPersona: Int): TechnicalModel?

    @Query("SELECT * FROM technical_table WHERE confirmado = :confirmado")
    suspend fun selectTechnicalByIsConfirmed(confirmado: Boolean = true): List<TechnicalModel>?

    @Insert(entity = TechnicalModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTechnical(model: TechnicalModel)

    @Update(entity = TechnicalModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTechnicalModel(model: TechnicalModel)

    @Query("DELETE FROM technical_table WHERE idVisita = :idVisita")
    suspend fun deleteTechnicalByVisita(idVisita: Int)

    @Transaction
    suspend fun updateTechnical(model: TechnicalEntity) {
        updateFalseTechnical(model.idVisita.orZero())
        val item = selectTechnicalById(model.idPersona.orZero())
        if (item != null) {
            item.confirmado = model.mapToModel().confirmado
            updateTechnicalModel(item)
        }
    }

    @Query("UPDATE technical_table SET confirmado = :confirmado WHERE idVisita = :idVisita")
    suspend fun updateFalseTechnical(idVisita: Int, confirmado: Boolean = false)


    //Product interaction
    @Query("SELECT * FROM interaction_certificate_table WHERE isUpdate = :isUpdate")
    suspend fun selectAllCertificate(isUpdate: Boolean = true): List<CertificateModel>?

    @Query("SELECT * FROM interaction_certificate_table WHERE nitEmpresa = :nitEmpresa ")
    suspend fun selectCertificateByNit(nitEmpresa: String): List<CertificateModel>?

    @Insert(entity = CertificateModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCertificate(model: CertificateModel)

    @Transaction
    suspend fun updateCertificate(it: CertificateEntity) {
        val item = selectCertificateById(it.idCertificado.orZero())
        if (item != null) updateCertificateById(item.idCertificado.orZero(), item.estado.orEmpty())
    }

    @Query("SELECT * FROM interaction_certificate_table WHERE idCertificado = :idCertificado")
    suspend fun selectCertificateById(idCertificado: Int): CertificateModel?

    @Query("UPDATE interaction_certificate_table SET estado = :estado, isUpdate = :isUpdate WHERE idCertificado = :idCertificado")
    suspend fun updateCertificateById(idCertificado: Int, estado: String, isUpdate: Boolean = true)

}