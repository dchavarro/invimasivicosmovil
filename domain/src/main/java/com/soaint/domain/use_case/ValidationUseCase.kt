package com.soaint.domain.use_case

import com.soaint.domain.model.BodyObservationEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.ValidationRepository
import javax.inject.Inject

class ValidationUseCase @Inject constructor(
    private val validationRepository: ValidationRepository
) {

    suspend fun validationMss(idVisita: String, tipoMs: String) = validationRepository.validationMss(idVisita, tipoMs)

    suspend fun validationUpdateMssList(idVisita: String, tipoMs: String) = validationRepository.validationUpdateMssList(idVisita, tipoMs)

    suspend fun validationLegalAtiende(idVisita: String) = validationRepository.validationLegalAtiende(idVisita)

    suspend fun addObservation(
        idVisita: String,
        observation: String,
        idTipoProducto: Int
    ) = validationRepository.addObservation(idVisita, observation, idTipoProducto)

    suspend fun getObservationLocal(
        idVisita: String,
        idTipoProducto: Int
    ) = validationRepository.getObservationLocal(idVisita, idTipoProducto)

    suspend fun updateObservation(body: BodyObservationEntity) = validationRepository.updateObservation(body)

    suspend fun updateProcessExt(visita: VisitEntity) = validationRepository.updateProcessExt(visita)

    suspend fun validationRaviInper(idVisita: String) = validationRepository.validationRaviInper(idVisita)

    suspend fun validationAct015(idVisita: String) = validationRepository.validationAct015(idVisita)

    suspend fun generateRadicals(idVisita: String) = validationRepository.generateRadicals(idVisita)

    suspend fun validationActs(id: Int, codigosPlantillas: List<String>) = validationRepository.validationActs(id, codigosPlantillas)
    suspend fun validateAtLeastOneActs(idVisita: Int ) = validationRepository.validateAtLeastOneActs(idVisita)

    suspend fun validationActsFinished(id: Int, codigosPlantillas: List<String>) = validationRepository.validationActsFinished(id, codigosPlantillas)

}