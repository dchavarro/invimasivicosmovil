package com.soaint.sivicos_dinamico.flow.papf.item_view

import androidx.core.view.isVisible
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.common.formatDate
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemVisitInfoBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.utils.REASON_RAVI_ACOM
import com.soaint.sivicos_dinamico.utils.REASON_RAVI_INPER
import com.xwray.groupie.databinding.BindableItem

class InfoTramitItemView(
    private val visit: VisitEntity,
    private val onRegisterCompany: () -> Unit
) : BindableItem<ItemVisitInfoBinding>() {

    override fun bind(
        viewBinding: ItemVisitInfoBinding,
        position: Int
    ) = with(viewBinding) {
        textViewNumber.text = visit.numeroVisita
        textViewDateEnd.text = visit.fechaRealizo.formatDate()
        textViewSocial.text = visit.empresa?.razonSocial
        textViewNumberDoc.text = visit.empresa?.numDocumento
        textViewDistance.text = visit.descDistancia
        textViewLocation.text = visit.empresa?.direccion?.descDepartamento
        textViewPhone.text = visit.empresa?.telefono
        textViewEmail.text = visit.empresa?.correo
        textViewAddress.text = visit.empresa?.direccion?.descripcion
        textViewRol.text = visit.empresa?.rolSucursal?.map { it.rol }.orEmpty().joinToString (",")
        textViewSucursal.text = visit.empresa?.codigoSucursal
        textViewExpedient.text = EMPTY_STRING //TODO no llega data

        textViewDateStart.text = visit.fechaInicioEjecucion.formatDate()
        textViewTypeProduct.text = visit.descTipoProducto
        textViewTypeDoc.text = visit.empresa?.tipoDocumento
        textViewRepresent.text = visit.empresa?.representanteLegal?.map { it.nombre }.orEmpty().joinToString (",")
        textViewCountry.text = visit.empresa?.direccion?.descPais
        textViewCity.text = visit.empresa?.direccion?.descMunicipio
        textViewCellphone.text = visit.empresa?.celular
        textViewCode.text = visit.empresa?.codigoSucursal

        textViewCode.isVisible = visit.codigoRazon != REASON_RAVI_INPER
        textViewTitleCode.isVisible = visit.codigoRazon != REASON_RAVI_INPER
        buttonRegisterCompany.isVisible = visit.codigoRazon == REASON_RAVI_ACOM

        buttonRegisterCompany.setOnSingleClickListener {
            onRegisterCompany.invoke()
        }
    }

    override fun getLayout() = R.layout.item_visit_info
}