package com.soaint.sivicos_dinamico.flow.papf.item_view

import androidx.core.view.isVisible
import com.soaint.data.common.ID_TYPE_ACT
import com.soaint.data.common.ID_TYPE_HACCP
import com.soaint.data.common.ID_TYPE_PROCCESS
import com.soaint.domain.common.formatDate
import com.soaint.domain.model.SanitaryCertificatePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemCertificatePapfBinding
import com.xwray.groupie.databinding.BindableItem

class CertificatePapfItemView(
    private val item: SanitaryCertificatePapfEntity,
) : BindableItem<ItemCertificatePapfBinding>() {

    override fun bind(
        viewBinding: ItemCertificatePapfBinding, position: Int
    ) = with(viewBinding) {

        val context = root.context
        textViewTitleId.text = context.getString(R.string.copy_certificate_id)
        textViewId.text = item.idCertificado

        textViewTitleDate.text = context.getString(R.string.copy_certificate_date_expirate)
        textViewDate.text = item.fechaExpedicion?.formatDate()

        textViewTitleDestiny.isVisible = true
        textViewDestiny.isVisible = true
        textViewDestiny.text = item.pais

        textViewFile.text = item.nombreDocumento

        when (item.idTipo) {
            ID_TYPE_PROCCESS -> {
                textViewTitleDestiny.isVisible = false
                textViewDestiny.isVisible = false
            }
            ID_TYPE_HACCP -> {
                textViewTitleDate.text = context.getString(R.string.copy_certificate_date_vig)
                textViewTitleDestiny.isVisible = false
                textViewDestiny.isVisible = false
            }
            ID_TYPE_ACT -> {
                textViewTitleId.text = context.getString(R.string.copy_number_visit)
                textViewTitleDate.text = context.getString(R.string.copy_certificate_date_visit)
                textViewTitleDestiny.isVisible = false
                textViewDestiny.isVisible = false
            }
        }
    }

    override fun getLayout() = R.layout.item_certificate_papf
}