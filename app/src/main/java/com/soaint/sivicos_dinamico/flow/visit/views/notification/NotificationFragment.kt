package com.soaint.sivicos_dinamico.flow.visit.views.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.data.common.SPACE_STRING
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.EmailsEntity
import com.soaint.domain.model.NotificationEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentNotificationBinding
import com.soaint.sivicos_dinamico.databinding.ItemNotificationBinding
import com.soaint.sivicos_dinamico.extensions.hideKeyboard
import com.soaint.sivicos_dinamico.extensions.isValidEmail
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.validateEmail
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding

class NotificationFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentNotificationBinding>(R.layout.fragment_notification)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: NotificationViewModel by viewModels { viewModelFactory }

    private val dataEmails = arrayListOf<String?>()

    private var position: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.notificar_visita))

        binding.buttonAdd.setOnSingleClickListener {
            binding.recyclerView.addItemView(null, position)
            position++
        }
        binding.buttonSave.setOnSingleClickListener {
            if (dataEmails.size > 0) {
                showAlertDialog(
                    showDialog(
                        getString(R.string.adv),
                        getString(R.string.copy_notification_message),
                        titleBtnPositive = getString(R.string.si),
                        titleBtnNegative = getString(R.string.no)
                    )
                    { saveEmailsLocal() }
                )
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(NotificationFragmentDirections.toInfo())
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnNotificationViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnNotificationViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnNotificationViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnNotificationViewModelEvent.OnViewModelReady -> onViewModelReady(event.emails)
            is OnNotificationViewModelEvent.OnViewModelReadyAddEmail -> OnViewModelReadyAddEmail()
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun OnViewModelReadyAddEmail() {
        viewModel.reload()
    }

    private fun onViewModelReady(emails: List<EmailsEntity>) {
        binding.recyclerView.removeAllViews()
        emails.map {
            binding.recyclerView.addItemView(it, position)
            position++
        }
    }

    private fun saveEmailsLocal() {
        val email = dataEmails.filterNotNull().orEmpty().toString()
            .replace(SPACE_STRING, EMPTY_STRING)
            .replace("[",EMPTY_STRING)
            .replace("]",EMPTY_STRING)
            .split(",")

        var error: Int = 0
        for (i in dataEmails.indices) {
            if (!email[i].isValidEmail() || email[i].isEmpty()) {
                showAlertDialog(
                    showDialog(
                        getString(R.string.error),
                        getString(R.string.mensaje_email_no_valido),
                        titleBtnPositive = getString(R.string.cerrar))
                    { dismissAlertDialog() }
                )
                error++
            }
        }
        if (error.equals(0)) {
            requireActivity().hideKeyboard()
            viewModel.saveEmail(dataEmails.filterNotNull() )
        }
    }

    private fun LinearLayout.addItemView(
        data: EmailsEntity?,
        position: Int,
    ) {
        val binding = DataBindingUtil.inflate<ItemNotificationBinding>(
            LayoutInflater.from(context),
            R.layout.item_notification,
            null,
            true
        )
        dataEmails.add(data?.correo?: "")
        binding.editText.setText(data?.correo)
        //binding.imageViewDelete.isVisible = data.isNullOrEmpty()
        binding.editText.isEnabled = data?.correo.isNullOrEmpty()

        binding.editText.doAfterTextChanged {
            if (context.validateEmail(it.toString(), binding.textInputLayout)) {
                dataEmails.set(position, it.toString())
            } else {
                dataEmails.set(position, EMPTY_STRING)
            }
        }

        binding.imageViewDelete.setOnClickListener {
            showAlertDialog(
                showDialog(
                    getString(R.string.confirmar),
                    getString(R.string.confirmar_borrar_item),
                    titleBtnPositive = getString(R.string.si),
                    titleBtnNegative = getString(R.string.no))
                {
                    dataEmails.set(position, EMPTY_STRING)
                    removeView(binding.root)
                    dataEmails.removeAt(position)
                    viewModel.deleteEmail(data?.id)
                    dismissAlertDialog()
                }
            )
        }
        addView(binding.root)
    }
}