package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.domain.model.*

@Entity(tableName = "actas_table")
class Actas(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    @ColumnInfo(name = "id_acta") val id_acta: Int,
    @ColumnInfo(name = "id_misional") val id_misional: Int,
    @ColumnInfo(name = "nombre") val nombre: String
)

@Entity(tableName = "group_visit_table")
class GroupVisitModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int?,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun GroupVisitModel.mapToDomain() = GroupEntity(
    id, codigo, descripcion, idTipoProducto
)

@Entity(tableName = "group_document_table")
class GroupDocumentModel(
    @PrimaryKey val idGrupo: Int,
    @ColumnInfo(name = "nombreDependencia") val nombreDependencia: String?,
)

fun GroupDocumentModel.mapToDomain() = GroupDocumentEntity(idGrupo, nombreDependencia)

@Entity(tableName = "rules_bussiness_act_table")
class RulesBussinessModel(
    @ColumnInfo(name = "idPlantilla") val id: Int?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "codigoPlantilla") val codigoPlantilla: String?,
    @ColumnInfo(name = "idTipoDocumental") val idTipoDocumental: Int?,
    @ColumnInfo(name = "idGroup") val idGroup: Int?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int? = null,
    @ColumnInfo(name = "idTipoProductoPapf") val idTipoProductoPapf: Int? = null,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun RulesBussinessModel.mapToDomain() = RulesBussinessEntity (
    id, nombre, codigoPlantilla, idTipoDocumental, idGroup, idTipoProducto
)

@Entity(tableName = "acta_pdf_table")
class ActasPdfModel(
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
    @ColumnInfo(name = "pdf") val pdf: String,
    @ColumnInfo(name = "idPlantilla") val idPlantilla: String,
    @ColumnInfo(name = "idVisita") val idVisita: String?,
)