package com.soaint.data.api

import com.soaint.data.model.FirmaBodyDto
import com.soaint.data.model.FirmaResponseDto
import retrofit2.http.Body
import retrofit2.http.POST

interface FirmaApi {

    //SERVICIO FIRMA ROTULADA
    @POST("v1/FirmaRotulada/firmaCertificada")
    suspend fun firmaCertificada(
        @Body body: FirmaBodyDto
    ): FirmaResponseDto

}