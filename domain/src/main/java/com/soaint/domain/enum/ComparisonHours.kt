package com.soaint.domain.enum

enum class ComparisonResult {
    MENOR, IGUAL, MAYOR, ERROR
}
