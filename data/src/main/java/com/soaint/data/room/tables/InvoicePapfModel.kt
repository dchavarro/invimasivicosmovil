package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.InvoicePapfEntity
import com.soaint.domain.model.TaskPapfEntity

@Entity(tableName = "invoice_papf_table")
class InvoicePapfModel(
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "numeroFactura") val numeroFactura: String?,
    @ColumnInfo(name = "fechaExpedicion") val fechaExpedicion: String?,
    @ColumnInfo(name = "idDocumento") val idDocumento: Int?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "nombreEmpresa") val nombreEmpresa: String?,
    @ColumnInfo(name = "idPais") val idPais: Int?,
    @ColumnInfo(name = "pais") val pais: String?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun InvoicePapfModel.mapToDomain() = InvoicePapfEntity(
    idSolicitud,
    numeroFactura.orEmpty(),
    fechaExpedicion.orEmpty(),
    idDocumento.orEmpty(),
    numeroDocumento.orEmpty(),
    nombreEmpresa.orEmpty(),
    idPais.orEmpty(),
    pais.orEmpty(),
)