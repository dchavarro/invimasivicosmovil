package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.data.model.VisitFormaDto
import com.soaint.domain.model.VisitDireccionEntity
import com.soaint.domain.model.VisitFormaEntity

@Entity(tableName = "visit_forma_table",
    foreignKeys = [
        ForeignKey(
            entity = VisitModel::class,
            parentColumns = ["id"],
            childColumns = ["idVisita"],
            onDelete = ForeignKey.CASCADE
        )
    ])
class VisitFormaModel(
    @ColumnInfo(name = "id") val id: Int,
    @ColumnInfo(name = "codigo") val codigo: String,
    @ColumnInfo(name = "descripcion") val descripcion: String,
    @ColumnInfo(name = "idVisita") val idVisita: Int,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)


fun VisitFormaModel.mapToDomain(idVisita: Int) = VisitFormaEntity(
    id,
    codigo,
    descripcion,
    idVisita
)