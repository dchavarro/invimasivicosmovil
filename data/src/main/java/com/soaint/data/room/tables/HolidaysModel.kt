package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.utils.getDateHourNow
import com.soaint.domain.model.HolidaysEntity


@Entity(tableName = "holidays_table")
class HolidaysModel(
    @PrimaryKey var idCalendario: Int?,
    @ColumnInfo(name = "nombre") val nombre: String,
    @ColumnInfo(name = "fechaEvento") val fechaEvento: String,
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String? = getDateHourNow(),
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?
)
fun HolidaysModel.mapToDomain() = HolidaysEntity(
    idCalendario,
    nombre,
    fechaEvento,
    activo,
    fechaCreacion,
    usuarioCrea,
    fechaModifica,
    usuarioModifica
)
