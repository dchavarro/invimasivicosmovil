package com.soaint.domain.use_case

import com.soaint.domain.model.*
import com.soaint.domain.repository.TaskRepository
import javax.inject.Inject

class TaskUseCase @Inject constructor(
    private val taskRepository: TaskRepository
) {

    val tasks get() = taskRepository.tasks

    suspend fun loadTask() = taskRepository.loadTasks()

    suspend fun loadTasksLocal() = taskRepository.loadTasksLocal()

    suspend fun loadTasksLocalOnlyActive() = taskRepository.loadTasksLocalOnlyActive()

    suspend fun loadTaskLocal(idTask: Int)= taskRepository.loadTaskLocal(idTask)

    suspend fun updateTask(body: TaskBodyEntity): Result<Unit> { return taskRepository.updateTask(body) }

    suspend fun updateTaskLocal(task: TaskEntity?)= taskRepository.updateTaskLocal(task)

    suspend fun createTask(body: TaskMssBodyEntity): Result<TaskEntity?> = taskRepository.createTask(body)

    suspend fun createTaskCompany(body: TaskCompanyBodyEntity) = taskRepository.createTaskCompany(body)

}