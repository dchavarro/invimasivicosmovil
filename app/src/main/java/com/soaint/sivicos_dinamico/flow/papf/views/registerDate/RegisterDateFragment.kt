package com.soaint.sivicos_dinamico.flow.papf.views.registerDate

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.domain.common.formatDate12Hour
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.RegisterDateBodyEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentRegisterDatePapfBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.xwray.groupie.GroupieAdapter
import java.text.DecimalFormat
import java.util.*

class RegisterDateFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentRegisterDatePapfBinding>(R.layout.fragment_register_date_papf)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: RegisterDateViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.register_date_inspection_papf))

        binding.editTextDate.setOnSingleClickListener { setDate() }
        binding.textInputDate.setOnSingleClickListener { setDate() }
        binding.includeReprogramar.yes.setOnSingleClickListener { viewModel.setReprogramar(true) }
        binding.includeReprogramar.no.setOnSingleClickListener { viewModel.setReprogramar(false) }
        binding.buttonSend.setOnSingleClickListener { viewModel.validateData() }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(RegisterDateFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        viewModel.reprogramar.observe(viewLifecycleOwner, ::enableView)
        mainViewModel.idRequest.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelSnackBar -> showSnackBar(event.isSuccess, event.message)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.register)
            is OnViewModelEvent.OnValidateData -> validateErrorData(event.date, event.observation)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(showDialog(
            getString(R.string.error),
            getString(R.string.copy_generic_error),
            titleBtnPositive = getString(R.string.cerrar)
        ) { dismissAlertDialog() })
    }

    private fun onViewModelReady(register: RegisterDateBodyEntity?) {
        binding.includeReprogramar.checked = register?.fecha.isNullOrEmpty()
        binding.editTextDate.text = register?.fecha?.orEmpty()?.formatDate12Hour()?.toEditable()
        binding.editTextObservation.text = register?.observaciones?.orEmpty()?.toEditable()
        enableView(register == null)
    }

    private fun enableView(enable: Boolean) {
        binding.includeReprogramar.root.isVisible = !enable
        binding.textInputDate.isEnabled = enable
        binding.editTextDate.isEnabled = enable
        binding.editTextObservation.isEnabled = enable
        binding.buttonSend.isEnabled = enable
    }

    private fun setDate() {
        val c: Calendar = Calendar.getInstance()
        val yearCalendar = c.get(Calendar.YEAR)
        val dayMonth = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val timePickerDialog =
            DatePickerDialog(requireContext(), { _, yearResponse, month, day_month ->
                val monthSelected = DecimalFormat("00").format(month + 1).toString()
                val daySelected = DecimalFormat("00").format(day_month).toString()
                setHour("$yearResponse-$monthSelected-$daySelected")
            }, yearCalendar, month, dayMonth)
        timePickerDialog.setCancelable(false)
        timePickerDialog.datePicker.minDate = c.timeInMillis
        timePickerDialog.show()
    }

    fun setHour(fechaString: String) {
        val c: Calendar = Calendar.getInstance()
        val mHour = c.get(Calendar.HOUR_OF_DAY)
        val mMinute = c.get(Calendar.MINUTE)
        val timePickerDialog = TimePickerDialog(
            requireContext(), { _, hourOfDay, minute ->
                val hourSelected = DecimalFormat("00").format(hourOfDay).toString()
                val minuteSelected = DecimalFormat("00").format(minute).toString()
                val dateString = "$fechaString" + "T" + "$hourSelected:$minuteSelected:00"
                viewModel.onDateChange(dateString.toString().toEditable())
                binding.textInputDate.editText?.text = dateString.formatDate12Hour().toEditable()
            }, mHour, mMinute, true
        )
        timePickerDialog.setCancelable(false)
        timePickerDialog.show()
    }

    private fun validateErrorData(date: String?, observation: String?) {
        binding.apply {
            validateField(textInputDate, date)
            validateField(textInputObservation, observation)
        }
    }

    fun showSnackBar(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
        if (isSuccess) viewModel.setReprogramar(false)
    }
}