import com.android.build.gradle.internal.cxx.configure.gradleLocalProperties

plugins {
    id(Pluggins.androidApplication)
    kotlin(Pluggins.kotlinAndroid)
    id(Pluggins.kotlinParcelizeExtensions)
    kotlin(Pluggins.kotlinKapt)
    id(Pluggins.androidNavigation)
    id(Pluggins.crashlytics)
    id(Pluggins.googleService)
}

android {
    compileSdk = Application.compileSdk
    buildToolsVersion = Application.buildToolsVersion

    defaultConfig {
        applicationId = Application.appId
        minSdk = Application.minSdk
        targetSdk = Application.targetSdk
        versionCode = Application.versionCode
        versionName = Application.versionName
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {
            extra["enableCrashlytics"] = false
            buildConfigField("Boolean", "enableCrashlytics", "false")
            buildConfigField("String", "URL_MOCK", gradleLocalProperties(rootDir).getProperty("URL_MOCK_DEV"))
            buildConfigField("String", "URL_DUMMY", gradleLocalProperties(rootDir).getProperty("URL_DUMMY_DEV"))
            buildConfigField("String", "URL_AUTH", gradleLocalProperties(rootDir).getProperty("URL_AUTH_DEV"))
            buildConfigField("String", "URL_TRANSVERSAL", gradleLocalProperties(rootDir).getProperty("URL_TRANSVERSAL_DEV"))
            buildConfigField("String", "URL_PARAMETRICS", gradleLocalProperties(rootDir).getProperty("URL_PARAMETRICS_DEV"))
            buildConfigField("String", "URL_PROXY", gradleLocalProperties(rootDir).getProperty("URL_PROXY_DEV"))
            buildConfigField("String", "URL_GENERALES", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_DEV"))
            buildConfigField("String", "URL_VISIT", gradleLocalProperties(rootDir).getProperty("URL_VISIT_DEV"))
            buildConfigField("String", "URL_LOAD", gradleLocalProperties(rootDir).getProperty("URL_LOAD_DEV"))
            buildConfigField("String", "URL_MASTER", gradleLocalProperties(rootDir).getProperty("URL_MASTER_DEV"))
            buildConfigField("String", "URL_DOCUMENTS", gradleLocalProperties(rootDir).getProperty("URL_DOCUMENTS_DEV"))
            buildConfigField("String", "URL_GENERICS", gradleLocalProperties(rootDir).getProperty("URL_GENERICS_DEV"))
            buildConfigField("String", "URL_MEASURE_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_MEASURE_SANITARY_DEV"))
            buildConfigField("String", "URL_REGISTER_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_REGISTER_SANITARY_DEV"))
            buildConfigField("String", "URL_MODELO_IVC", gradleLocalProperties(rootDir).getProperty("URL_MODELO_IVC_DEV"))
            buildConfigField("String", "URL_COMPANY", gradleLocalProperties(rootDir).getProperty("URL_COMPANY_DEV"))
            buildConfigField("String", "URL_NOTIFICATION", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_DEV"))
            buildConfigField("String", "URL_ACCESS_DATA", gradleLocalProperties(rootDir).getProperty("URL_ACCESS_DATA_DEV"))
            buildConfigField("String", "URL_PROCEDURE", gradleLocalProperties(rootDir).getProperty("URL_PROCEDURE_DEV"))
            buildConfigField("String", "URL_INPER", gradleLocalProperties(rootDir).getProperty("URL_INPER_DEV"))
            buildConfigField("String", "URL_MASTER_TRANS", gradleLocalProperties(rootDir).getProperty("URL_MASTER_TRANS_DEV"))
            buildConfigField("String", "URL_PLANNING", gradleLocalProperties(rootDir).getProperty("URL_PLANNING_DEV"))
            buildConfigField("String", "URL_INTEGRATIONS", gradleLocalProperties(rootDir).getProperty("URL_INTEGRATIONS_DEV"))
            buildConfigField("String", "URL_GENERALES_PAPF", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_PAPF_DEV"))
            buildConfigField("String", "URL_PAPF", gradleLocalProperties(rootDir).getProperty("URL_PAPF_DEV"))
            buildConfigField("String", "URL_CRUD", gradleLocalProperties(rootDir).getProperty("URL_CRUD_DEV"))
            buildConfigField("String", "URL_NOTIFICATION_EMAIL", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_EMAIL_DEV"))
            buildConfigField("String", "URL_FIRMA", gradleLocalProperties(rootDir).getProperty("URL_FIRMA_DEV"))
            versionNameSuffix = " Dev-" + Application.QAVersion
            isDebuggable = true
            isMinifyEnabled = false
            addManifestPlaceholders(mapOf("appName" to "@string/app_name_dev_soaint"))
        }

        create("qa") {
            initWith(buildTypes.getByName("debug"))
            signingConfig = signingConfigs.getByName("debug")
            extra["enableCrashlytics"] = true
            buildConfigField("Boolean", "enableCrashlytics", "true")
            buildConfigField("String", "URL_MOCK", gradleLocalProperties(rootDir).getProperty("URL_MOCK_QA"))
            buildConfigField("String", "URL_DUMMY", gradleLocalProperties(rootDir).getProperty("URL_DUMMY_QA"))
            buildConfigField("String", "URL_AUTH", gradleLocalProperties(rootDir).getProperty("URL_AUTH_QA"))
            buildConfigField("String", "URL_TRANSVERSAL", gradleLocalProperties(rootDir).getProperty("URL_TRANSVERSAL_QA"))
            buildConfigField("String", "URL_PARAMETRICS", gradleLocalProperties(rootDir).getProperty("URL_PARAMETRICS_QA"))
            buildConfigField("String", "URL_PROXY", gradleLocalProperties(rootDir).getProperty("URL_PROXY_QA"))
            buildConfigField("String", "URL_GENERALES", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_QA"))
            buildConfigField("String", "URL_VISIT", gradleLocalProperties(rootDir).getProperty("URL_VISIT_QA"))
            buildConfigField("String", "URL_LOAD", gradleLocalProperties(rootDir).getProperty("URL_LOAD_QA"))
            buildConfigField("String", "URL_MASTER", gradleLocalProperties(rootDir).getProperty("URL_MASTER_QA"))
            buildConfigField("String", "URL_DOCUMENTS", gradleLocalProperties(rootDir).getProperty("URL_DOCUMENTS_QA"))
            buildConfigField("String", "URL_GENERICS", gradleLocalProperties(rootDir).getProperty("URL_GENERICS_QA"))
            buildConfigField("String", "URL_MEASURE_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_MEASURE_SANITARY_QA"))
            buildConfigField("String", "URL_REGISTER_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_REGISTER_SANITARY_QA"))
            buildConfigField("String", "URL_MODELO_IVC", gradleLocalProperties(rootDir).getProperty("URL_MODELO_IVC_QA"))
            buildConfigField("String", "URL_COMPANY", gradleLocalProperties(rootDir).getProperty("URL_COMPANY_QA"))
            buildConfigField("String", "URL_NOTIFICATION", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_QA"))
            buildConfigField("String", "URL_ACCESS_DATA", gradleLocalProperties(rootDir).getProperty("URL_ACCESS_DATA_QA"))
            buildConfigField("String", "URL_PROCEDURE", gradleLocalProperties(rootDir).getProperty("URL_PROCEDURE_QA"))
            buildConfigField("String", "URL_INPER", gradleLocalProperties(rootDir).getProperty("URL_INPER_QA"))
            buildConfigField("String", "URL_MASTER_TRANS", gradleLocalProperties(rootDir).getProperty("URL_MASTER_TRANS_QA"))
            buildConfigField("String", "URL_PLANNING", gradleLocalProperties(rootDir).getProperty("URL_PLANNING_QA"))
            buildConfigField("String", "URL_INTEGRATIONS", gradleLocalProperties(rootDir).getProperty("URL_INTEGRATIONS_QA"))
            buildConfigField("String", "URL_GENERALES_PAPF", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_PAPF_QA"))
            buildConfigField("String", "URL_PAPF", gradleLocalProperties(rootDir).getProperty("URL_PAPF_QA"))
            buildConfigField("String", "URL_CRUD", gradleLocalProperties(rootDir).getProperty("URL_CRUD_QA"))
            buildConfigField("String", "URL_NOTIFICATION_EMAIL", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_EMAIL_QA"))
            buildConfigField("String", "URL_FIRMA", gradleLocalProperties(rootDir).getProperty("URL_FIRMA_QA"))

            isDebuggable = true
            versionNameSuffix = " Qa-Soaint-" + Application.QAVersion
            addManifestPlaceholders(mapOf("appName" to "@string/app_name_qa_soaint"))
        }

        create("qaInvima") {
            initWith(buildTypes.getByName("debug"))
            signingConfig = signingConfigs.getByName("debug")
            extra["enableCrashlytics"] = true
            buildConfigField("Boolean", "enableCrashlytics", "true")
            buildConfigField("String", "URL_MOCK", gradleLocalProperties(rootDir).getProperty("URL_MOCK_QA_INVIMA"))
            buildConfigField("String", "URL_DUMMY", gradleLocalProperties(rootDir).getProperty("URL_DUMMY_QA_INVIMA"))
            buildConfigField("String", "URL_AUTH", gradleLocalProperties(rootDir).getProperty("URL_AUTH_QA_INVIMA"))
            buildConfigField("String", "URL_TRANSVERSAL", gradleLocalProperties(rootDir).getProperty("URL_TRANSVERSAL_QA_INVIMA"))
            buildConfigField("String", "URL_PARAMETRICS", gradleLocalProperties(rootDir).getProperty("URL_PARAMETRICS_QA_INVIMA"))
            buildConfigField("String", "URL_PROXY", gradleLocalProperties(rootDir).getProperty("URL_PROXY_QA_INVIMA"))
            buildConfigField("String", "URL_GENERALES", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_QA_INVIMA"))
            buildConfigField("String", "URL_VISIT", gradleLocalProperties(rootDir).getProperty("URL_VISIT_QA_INVIMA"))
            buildConfigField("String", "URL_LOAD", gradleLocalProperties(rootDir).getProperty("URL_LOAD_QA_INVIMA"))
            buildConfigField("String", "URL_MASTER", gradleLocalProperties(rootDir).getProperty("URL_MASTER_QA_INVIMA"))
            buildConfigField("String", "URL_DOCUMENTS", gradleLocalProperties(rootDir).getProperty("URL_DOCUMENTS_QA_INVIMA"))
            buildConfigField("String", "URL_GENERICS", gradleLocalProperties(rootDir).getProperty("URL_GENERICS_QA_INVIMA"))
            buildConfigField("String", "URL_MEASURE_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_MEASURE_SANITARY_QA_INVIMA"))
            buildConfigField("String", "URL_REGISTER_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_REGISTER_SANITARY_QA_INVIMA"))
            buildConfigField("String", "URL_MODELO_IVC", gradleLocalProperties(rootDir).getProperty("URL_MODELO_IVC_QA_INVIMA"))
            buildConfigField("String", "URL_COMPANY", gradleLocalProperties(rootDir).getProperty("URL_COMPANY_QA_INVIMA"))
            buildConfigField("String", "URL_NOTIFICATION", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_QA_INVIMA"))
            buildConfigField("String", "URL_ACCESS_DATA", gradleLocalProperties(rootDir).getProperty("URL_ACCESS_DATA_QA_INVIMA"))
            buildConfigField("String", "URL_PROCEDURE", gradleLocalProperties(rootDir).getProperty("URL_PROCEDURE_QA_INVIMA"))
            buildConfigField("String", "URL_INPER", gradleLocalProperties(rootDir).getProperty("URL_INPER_QA_INVIMA"))
            buildConfigField("String", "URL_MASTER_TRANS", gradleLocalProperties(rootDir).getProperty("URL_MASTER_TRANS_QA_INVIMA"))
            buildConfigField("String", "URL_PLANNING", gradleLocalProperties(rootDir).getProperty("URL_PLANNING_QA_INVIMA"))
            buildConfigField("String", "URL_INTEGRATIONS", gradleLocalProperties(rootDir).getProperty("URL_INTEGRATIONS_QA_INVIMA"))
            buildConfigField("String", "URL_GENERALES_PAPF", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_PAPF_QA_INVIMA"))
            buildConfigField("String", "URL_PAPF", gradleLocalProperties(rootDir).getProperty("URL_PAPF_QA_INVIMA"))
            buildConfigField("String", "URL_CRUD", gradleLocalProperties(rootDir).getProperty("URL_CRUD_QA_INVIMA"))
            buildConfigField("String", "URL_NOTIFICATION_EMAIL", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_EMAIL_QA_INVIMA"))
            buildConfigField("String", "URL_FIRMA", gradleLocalProperties(rootDir).getProperty("URL_FIRMA_QA_INVIMA"))


            isDebuggable = true
            versionNameSuffix = " Qa-Invima-" + Application.QAInvimaVersion
            addManifestPlaceholders(mapOf("appName" to "@string/app_name_qa_invima"))
        }

        getByName("release") {
            extra["enableCrashlytics"] = true
            buildConfigField("Boolean", "enableCrashlytics", "true")
            buildConfigField("String", "URL_MOCK", gradleLocalProperties(rootDir).getProperty("URL_MOCK_PROD"))
            buildConfigField("String", "URL_DUMMY", gradleLocalProperties(rootDir).getProperty("URL_DUMMY_PROD"))
            buildConfigField("String", "URL_AUTH", gradleLocalProperties(rootDir).getProperty("URL_AUTH_PROD"))
            buildConfigField("String", "URL_TRANSVERSAL", gradleLocalProperties(rootDir).getProperty("URL_TRANSVERSAL_PROD"))
            buildConfigField("String", "URL_PARAMETRICS", gradleLocalProperties(rootDir).getProperty("URL_PARAMETRICS_PROD"))
            buildConfigField("String", "URL_PROXY", gradleLocalProperties(rootDir).getProperty("URL_PROXY_PROD"))
            buildConfigField("String", "URL_GENERALES", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_PROD"))
            buildConfigField("String", "URL_VISIT", gradleLocalProperties(rootDir).getProperty("URL_VISIT_PROD"))
            buildConfigField("String", "URL_LOAD", gradleLocalProperties(rootDir).getProperty("URL_LOAD_PROD"))
            buildConfigField("String", "URL_MASTER", gradleLocalProperties(rootDir).getProperty("URL_MASTER_PROD"))
            buildConfigField("String", "URL_DOCUMENTS", gradleLocalProperties(rootDir).getProperty("URL_DOCUMENTS_PROD"))
            buildConfigField("String", "URL_GENERICS", gradleLocalProperties(rootDir).getProperty("URL_GENERICS_PROD"))
            buildConfigField("String", "URL_MEASURE_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_MEASURE_SANITARY_PROD"))
            buildConfigField("String", "URL_REGISTER_SANITARY", gradleLocalProperties(rootDir).getProperty("URL_REGISTER_SANITARY_PROD"))
            buildConfigField("String", "URL_MODELO_IVC", gradleLocalProperties(rootDir).getProperty("URL_MODELO_IVC_PROD"))
            buildConfigField("String", "URL_COMPANY", gradleLocalProperties(rootDir).getProperty("URL_COMPANY_PROD"))
            buildConfigField("String", "URL_NOTIFICATION", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_PROD"))
            buildConfigField("String", "URL_ACCESS_DATA", gradleLocalProperties(rootDir).getProperty("URL_ACCESS_DATA_PROD"))
            buildConfigField("String", "URL_PROCEDURE", gradleLocalProperties(rootDir).getProperty("URL_PROCEDURE_PROD"))
            buildConfigField("String", "URL_INPER", gradleLocalProperties(rootDir).getProperty("URL_INPER_PROD"))
            buildConfigField("String", "URL_MASTER_TRANS", gradleLocalProperties(rootDir).getProperty("URL_MASTER_TRANS_PROD"))
            buildConfigField("String", "URL_PLANNING", gradleLocalProperties(rootDir).getProperty("URL_PLANNING_PROD"))
            buildConfigField("String", "URL_INTEGRATIONS", gradleLocalProperties(rootDir).getProperty("URL_INTEGRATIONS_PROD"))
            buildConfigField("String", "URL_GENERALES_PAPF", gradleLocalProperties(rootDir).getProperty("URL_GENERALES_PAPF_PROD"))
            buildConfigField("String", "URL_PAPF", gradleLocalProperties(rootDir).getProperty("URL_PAPF_PROD"))
            buildConfigField("String", "URL_CRUD", gradleLocalProperties(rootDir).getProperty("URL_CRUD_PROD"))
            buildConfigField("String", "URL_NOTIFICATION_EMAIL", gradleLocalProperties(rootDir).getProperty("URL_NOTIFICATION_EMAIL_PROD"))
            buildConfigField("String", "URL_FIRMA", gradleLocalProperties(rootDir).getProperty("URL_FIRMA_PROD"))

            isMinifyEnabled = false
            addManifestPlaceholders(mapOf("appName" to "@string/app_name"))
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    buildFeatures {
        dataBinding = true
        viewBinding = true
    }

    packagingOptions {
        exclude("META-INF/rxjava.properties")
    }

    lintOptions {
        isAbortOnError = false
    }

    dexOptions {
        javaMaxHeapSize = "4g"
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_1_8.toString()
    }
}

dependencies {
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))

    //Domain
    implementation(project(":domain"))

    //Data
    implementation(project(":data"))
    implementation("androidx.constraintlayout:constraintlayout:2.1.1")

    //DataBinding
    kapt(Libraries.dataBinding)

    //Kotlin
    Libraries.kotlinLibraries.forEach { implementation(it) }

    //Kotlin KTX
    Libraries.kotlinKtx.forEach { implementation(it) }

    //Retrofit
    Libraries.retrofitLibraries.forEach { implementation(it) }

    //Multidex
    implementation(Libraries.multidexLibrary)

    //Glide
    Libraries.glideLibraries.forEach { implementation(it) }
    annotationProcessor(Libraries.glideAnnotationProcessor)

    //Lifecycle
    Libraries.lifecycleLibraries.forEach { implementation(it) }

    //Permissions
    implementation(Libraries.permissionsLibrary)
    kapt(Libraries.permissionsKaptLibrary)

    //AndroidX
    Libraries.androidXLibraries.forEach { implementation(it) }

    //Navigation
    Libraries.navigationLibraries.forEach { implementation(it) }

    //Preferences
    implementation(Libraries.preferenceLibrary)
    implementation(Libraries.secretPreferenceLibrary)

    //Hilt
    Libraries.daggerLibrary.forEach { implementation(it) }
    Libraries.daggerKaptLibrary.forEach { kapt(it) }

    //Groupie
    Libraries.groupieLibrary.forEach { implementation(it) }

    //CircleImageView
    implementation(Libraries.circleImageViewLibrary)

    //Firebase
    Libraries.firebaseLibrary.forEach { implementation(it) }

    //Google
    Libraries.googleMaps.forEach { implementation(it) }

    //Room
    Libraries.roomLibrary.forEach { implementation(it) }
    kapt(Libraries.roomLibraryKapt)

    // iText
    implementation(Libraries.itextpdfLibrary)

    //Retrofit
    Libraries.retrofitLibraries.forEach { implementation(it) }

    // cookieLibrary
    implementation(Libraries.cookieLibrary)

    // MultiSelectSpinner
    implementation(Libraries.multiSelectSpinnerLibrary)

    // toolTipLibrary
    implementation(Libraries.toolTipLibrary)

    // Searchable Spinner
    implementation(Libraries.searchSpinnerLibrary)
}