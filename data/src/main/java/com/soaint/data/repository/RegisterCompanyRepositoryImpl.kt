package com.soaint.data.repository

import com.soaint.data.api.PlanningApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.ManageMssDao
import com.soaint.data.room.dao.RegisterCompanyDao
import com.soaint.data.room.dao.VisitasDao
import com.soaint.data.room.tables.CompanyAddressModel
import com.soaint.data.room.tables.CompanyModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.common.getDateHourNow
import com.soaint.domain.common.getIp
import com.soaint.domain.model.RegisterCompanyBodyEntity
import com.soaint.domain.model.VisitCompanyPersonEntity
import com.soaint.domain.repository.RegisterCompanyRepository
import com.soaint.domain.repository.UserRepository
import com.soaint.sivicos_dinamico.utils.CD_TYPE_PRODUCT
import com.soaint.sivicos_dinamico.utils.CTIPPROD
import javax.inject.Inject

class RegisterCompanyRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val planningApi: PlanningApi,
    private val registerCompanyDao: RegisterCompanyDao,
    private val manageMssDao: ManageMssDao,
    private val visitasDao: VisitasDao,
) : RegisterCompanyRepository, BaseRepository() {

    // carga personas vinculadas local
    override suspend fun loadPersonLocal(idVisita: Int): Result<List<VisitCompanyPersonEntity>> =
        execute {
            val result = registerCompanyDao.selectCompanyPerson(idVisita).map { it.mapToDomain() }
            Result.success(result)
        }

    // insert Person vinculadas local
    override suspend fun insertPersonLocal(
        body: VisitCompanyPersonEntity,
        idVisita: Int,
    ): Result<Unit> = execute {
        val result = registerCompanyDao.insertCompanyPerson(body.mapToModel(idVisita))
        Result.success(result)
    }

    //Eliminar Person vinculadas  en DB local
    override suspend fun deletePersonLocal(numeroDocumento: String, idVisita: Int): Result<Unit> =
        execute {
            val result = registerCompanyDao.deletePersonById(numeroDocumento, idVisita)
            Result.success(result)
        }

    // carga personas vinculadas local
    override suspend fun loadCompanyLocal(idVisita: Int): Result<RegisterCompanyBodyEntity?> =
        execute {
            val company = visitasDao.selectEmpresa(idVisita)
            val result = if (company != null) {
                val address = visitasDao.selectAddressEmpresa(company.idEmpresa.orZero())
                RegisterCompanyBodyEntity(
                    idTipoDocumento = company.idTipoDocumento,
                    tipoDocumento = company.tipoDocumento,
                    numeroDocumento = company.numDocumento,
                    digitoVerificacion = company.digitoVerificacion,
                    razonSocial = company.razonSocial,
                    nombreComercial = company.sigla,
                    paginaWeb = company.paginaweb,
                    idPais = address?.idPais,
                    pais = address?.descPais,
                    idDepartamento = address?.idDepartamento,
                    departamento = address?.descDepartamento,
                    idMunicipio = address?.idMunicipio,
                    municipio = address?.descMunicipio,
                    correoElectronico = company.correo,
                    direccion = address?.descripcion,
                    telefono = company.telefono,
                    celular = company.celular,
                    idSede = company.idSede,
                )
            } else {
                null
            }
            Result.success(result)
        }

    // insert Person vinculadas local
    override suspend fun insertOrUpdateCompanyLocal(
        body: RegisterCompanyBodyEntity,
        idVisita: Int
    ): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        // INSERT COMPANY MODEL
        val company = CompanyModel(
            activo = true,
            codigoSucursal = null,
            empresaAsociada = null,
            fechaCreacion = getDateHourNow(),
            fechaModifica = getDateHourNow(),
            idEmpresa = null,
            idEstadoEmpresa = null,
            idSede = body.idSede,
            idTipoDocumento = body.idTipoDocumento,
            idVisita = idVisita,
            numDocumento = body.numeroDocumento,
            razonSocial = body.razonSocial,
            tipoDocumento = body.tipoDocumento,
            usuarioCrea = userName,
            usuarioModifica = userName,
            telefono = body.telefono,
            celular = body.celular,
            correo = body.correoElectronico,
            paginaweb = body.paginaWeb,
            digitoVerificacion = body.digitoVerificacion,
            sigla = body.nombreComercial,
            isLocal = true
        )

        // INSERT COMPANY ADDRESS MODEL
        val address = CompanyAddressModel(
            descDepartamento = body.departamento.orEmpty(),
            descMunicipio = body.municipio.orEmpty(),
            descPais = body.pais.orEmpty(),
            descripcion = body.direccion.orEmpty(),
            idDepartamento = body.idDepartamento.orZero(),
            idMunicipio = body.idMunicipio.orZero(),
            idPais = body.idPais.orZero(),
        )
        val result = visitasDao.insertOrUpdateCompany(company, address)
        Result.success(result)
    }

    // subir company register no inscrita
    override suspend fun saveCompany(idVisita: Int): Result<Unit> = execute {
        val empresa = visitasDao.selectEmpresa(idVisita)
        val empresaId = empresa?.idEmpresa ?: 0
        val address = visitasDao.selectAddressEmpresa(empresaId)
        val userName = userRepository.userInformation.value?.usuario.orEmpty()

        val result = if (empresa != null) {
            val company = RegisterCompanyDto(
                codigoTipoDocumento = empresa.idTipoDocumento.orZero(),
                numeroDocumento = empresa.numDocumento.orEmpty(),
                digitoVerificacion = empresa.digitoVerificacion,
                razonSocial = empresa.razonSocial,
                nombreComercial = empresa.sigla,
                paginaWeb = empresa.paginaweb,
                pais = address?.idPais.orZero(),
                departamento = address?.idDepartamento.orZero(),
                municipio = address?.idMunicipio.orZero(),
                correoElectronico = empresa.correo,
                direccion = address?.descripcion.orEmpty(),
                telefono = empresa.telefono,
                celular = empresa.celular,
                tipo = if (empresa.isLocal == true) "0" else "1",
            )
            val sede = if (empresa.idSede != null) {
                RegisterCompanySedeDto(
                    idSede = empresa.idSede,
                    telefonoSede = empresa.telefono,
                    celularSede = empresa.celular,
                    correoElectronicoSede = empresa.correo,
                    paisSede = address?.idPais.orZero(),
                    departamentoSede = address?.idDepartamento.orZero(),
                    municipioSede = address?.idMunicipio.orZero(),
                    direccionSede = address?.descripcion.orEmpty(),
                )
            } else null
            val person = registerCompanyDao.selectCompanyPerson(idVisita)
                .map { it.mapToDomain().mapToData() }
            var listIdTipoProduct: ArrayList<TipoProductoDto> = arrayListOf()
            val query = manageMssDao.selectAmsspXvisit(CD_TYPE_PRODUCT, idVisita)
            query?.mapNotNull {
                val i = manageMssDao.selectEmsspxCodigo(it.codigo.orEmpty(), CTIPPROD)?.id
                val list = TipoProductoDto(i.orZero().toString())
                listIdTipoProduct.add(list)
            }
            planningApi.saveEmpresa(
                RegisterCompanyBodyDto(
                    getIp(),
                    userName,
                    company,
                    sede,
                    listIdTipoProduct,
                    person
                )
            )
            deleteCompanyDao(idVisita)
        } else {
            Unit
        }
        Result.success(result)
    }

    //Eliminar company register con relaciones en DB local
    private suspend fun deleteCompanyDao(idVisita: Int) {
        registerCompanyDao.deletePersonByVisit(idVisita)
        //manageMssDao.deleteAmsspCheckByTipo(CTIPPROD)
    }

}