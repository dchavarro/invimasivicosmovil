package com.soaint.data.api

import retrofit2.http.GET
import retrofit2.http.Query

interface NotificationApi {

    @GET("v1/visita/enviar-notificaciones")
    suspend fun sendNotification(
        @Query("idVisita") idVisita: String,
        @Query("codigoNotificacion") codigoNotificacion: String
    ): Unit
}