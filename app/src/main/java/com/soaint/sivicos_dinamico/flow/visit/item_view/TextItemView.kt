package com.soaint.sivicos_dinamico.flow.visit.item_view

import androidx.annotation.ColorRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTextBinding
import com.soaint.sivicos_dinamico.databinding.ItemTitleBinding
import com.xwray.groupie.databinding.BindableItem

class TextItemView(
    private val title: String,
    private @ColorRes val color: Int = R.color.colorGray
) : BindableItem<ItemTextBinding>() {

    override fun bind(
        viewBinding: ItemTextBinding,
        position: Int
    ) = with(viewBinding) {
        val context = root.context
        textView.text = title
        textView.setTextColor(ContextCompat.getColor(context, color))
    }

    override fun getLayout() = R.layout.item_text
}