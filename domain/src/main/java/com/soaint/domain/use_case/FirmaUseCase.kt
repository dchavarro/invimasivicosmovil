package com.soaint.domain.use_case

import com.soaint.domain.repository.FirmaRepository
import javax.inject.Inject

class FirmaUseCase @Inject constructor(
    private val repository: FirmaRepository
) {

    suspend fun firmaCertificada(idSolicitud: Int, pdfFileBase64: String) =
        repository.firmaCertificada(idSolicitud, pdfFileBase64)

}