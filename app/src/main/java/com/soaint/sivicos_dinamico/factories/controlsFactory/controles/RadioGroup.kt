package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.RadioGroup
import androidx.cardview.widget.CardView
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface

class RadioGroup: ControlInterface {

    lateinit var rg: RadioGroup
    lateinit var view: View
    lateinit var cardView: CardView
    lateinit var classContext: Context
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context

        val inflater: LayoutInflater = LayoutInflater.from(context)
        view = inflater.inflate(R.layout.view_radio_group, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        lblText.text = control.descripcion
        rg = view.findViewById<RadioGroup>(R.id.radioGroup)
        cardView = view.findViewById<CardView>(R.id.cardView)
        var rbChecked: RadioButton? = null
        for (label in control.configuracion?.lista.orEmpty()) {
            val rb = RadioButton(context)
            val lp = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            rb.layoutParams = lp
            rb.text = label.nombre

            if(label.nombre == valorAlmacenado) {
                rbChecked = rb
            } else if(label.seleccionado == true && valorAlmacenado.isEmpty()) {
                rbChecked = rb
            }

            rg.addView(rb)
        }

        if (rbChecked != null) {
            rbChecked.isChecked = true
        }
        rg.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)

        return view
    }

    override fun getData(): String {
        val rbChecked = view.findViewById<RadioButton>(rg.checkedRadioButtonId)
        return rbChecked?.text?.toString().orEmpty()
    }

    override fun showError(mensaje: String?) {
        cardView.setBackgroundResource(R.drawable.border_red)
    }

    override fun clearError() {
        cardView.setBackgroundResource(R.drawable.border_white)
    }
}