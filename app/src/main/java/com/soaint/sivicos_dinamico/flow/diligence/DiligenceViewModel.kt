package com.soaint.sivicos_dinamico.flow.diligence

import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.ID_TYPE_PRODUCT_PAPF
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM030
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM076
import javax.inject.Inject

sealed class OnManageViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnManageViewModelEvent()
    object OnLoadingError : OnManageViewModelEvent()
    data class OnViewModelGroupReady(val group: List<GroupEntity>) : OnManageViewModelEvent()

    data class OnViewModelPlantillaReady(val plantilla: List<RulesBussinessEntity>) :
        OnManageViewModelEvent()

    data class OnViewModelBlockReady(
        val idVisita: Int?,
        val plantilla: List<PlantillaEntity>,
        val block: List<BlockEntity>,
        val idTipoDocumental: Int
    ) : OnManageViewModelEvent()

    data class onViewModelReadyAct076(val isValid: List<String>) : OnManageViewModelEvent()
    object OnValidateDiligenceActaReady : OnManageViewModelEvent()

}

class DiligenceViewModel @Inject constructor(
    private val manageUseCase: ManageUseCase,
    private val plantillasUseCase: PlantillasUseCase,
    private val userUseCase: UserUseCase,
    private val visitUseCase: VisitUseCase,
    private val validationUseCase: ValidationUseCase,
    private val syncDataUseCase: SyncDataUseCase,
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnManageViewModelEvent>()
    val event = _event.asLiveData()

    private val _plantillaId = MutableLiveData(EMPTY_STRING)
    val plantillaId = _plantillaId.asLiveData()

    private val _idTipoDocumental = MutableLiveData(EMPTY_STRING)
    val idTipoDocumental = _idTipoDocumental.asLiveData()

    private var groupVisit: List<GroupEntity> = emptyList()

    private var plantilla: List<RulesBussinessEntity> = emptyList()

    private var block: List<BlockEntity> = emptyList()

    private var groupVisitSelected: GroupEntity? = null

    private var actSelected: RulesBussinessEntity? = null

    private val _visitData = MutableLiveData<VisitEntity>()
    val visitData = _visitData.asLiveData()

    private val _isVisibleEquipment = MutableLiveData<Boolean>(false)
    val isVisibleEquipment = _isVisibleEquipment.asLiveData()

    private val _count = MutableLiveData<Int>()
    val count = _count.asLiveData()

    private val _idRequest = MutableLiveData<Int>()
    val idRequest = _idRequest.asLiveData()

    val idTipoProducto get() = _visitData.value?.idTipoProducto

    fun init(id: Int) = execute {
        getVisit(id)
    }

    private suspend fun getVisit(id: Int) {
        _visitData.value = visitUseCase.loadVisitLocalById(id).getOrNull()
        _idRequest.value = _visitData.value?.id ?: id
        onLoadGroup()
    }

    private suspend fun onLoadGroup() {
        val result = manageUseCase.loadGroupLocal(idTipoProducto)
        _event.value = if (result.isSuccess) {
            groupVisit = result.getOrNull().orEmpty()
            countQuerys()
            OnManageViewModelEvent.OnViewModelGroupReady(result.getOrNull().orEmpty())
        } else {
            OnManageViewModelEvent.OnLoadingError
        }
    }

    fun setGroupSelected(groupSelected: String) {
        groupVisit.find { it.descripcion.equals(groupSelected, true) }?.let {
            groupVisitSelected = it
            onLoadRulesBussiness()
        }
    }

    private fun onLoadRulesBussiness() = execute {
        val idTipoProductoPapf = if (_idRequest.value != null) {
            papfUseCase.loadInfoTramitLocal(_idRequest.value!!).getOrNull().orEmpty().firstOrNull()?.idTipoProducto
        } else null


        val result = manageUseCase.loadRulesBussinessLocal(groupVisitSelected?.id!!, idTipoProductoPapf)
        _event.value = if (result.isSuccess) {
            plantilla = result.getOrNull().orEmpty()
            OnManageViewModelEvent.OnViewModelPlantillaReady(result.getOrNull().orEmpty())
        } else {
            OnManageViewModelEvent.OnLoadingError
        }
    }

    fun setRulesBussinessSelected(actSelected: String) {
        plantilla.find { it.nombre.equals(actSelected, true) }?.let {
            this.actSelected = it
            _isVisibleEquipment.value = it.codigoPlantilla == IVC_INS_FM030 && !userUseCase.userIsPapf()
            _idTipoDocumental.value = it.idTipoDocumental.toString()
            _plantillaId.value = it.id.toString()
        }
    }

    fun onLoadBlock() = execute {
        _event.value =
            OnManageViewModelEvent.OnChangeLabelPreloader(R.string.cargando_acta)
        val result = plantillasUseCase.loadBlocksLocal(_plantillaId.value.orEmpty())
        _event.value = if (result.isSuccess) {
            /*val sortedBlocks = result.getOrNull()?.map {
                it.copy(atributosPlantilla = it.atributosPlantilla.orEmpty().sortedWith(compareBy({ it.numeroOrden ?: Int.MAX_VALUE }, { it.id })))
            }*/

            val sortedBlocks = result.getOrNull()?.map {
                val filteredAttributes = it.atributosPlantilla?.filter { it.configuracion?.ocultar != true }
                it.copy(atributosPlantilla = filteredAttributes.orEmpty().sortedWith(compareBy({ it.numeroOrden ?: Int.MAX_VALUE }, { it.id }))
                )
            }
            block = result.getOrNull().orEmpty()
            val id = _idRequest.value
            val resultPlantilla =
                plantillasUseCase.loadPlantillasLocal(_plantillaId.value.orEmpty())
            val plantilla = resultPlantilla.getOrNull().orEmpty()
            OnManageViewModelEvent.OnViewModelBlockReady(
                id,
                plantilla,
                sortedBlocks.orEmpty(),
                _idTipoDocumental.value.intOrString()
            )
        } else {
            OnManageViewModelEvent.OnLoadingError
        }
    }

    fun validFM076() = execute {
        if (!userUseCase.userIsPapf()) {
            _event.value = if (actSelected?.codigoPlantilla == IVC_INS_FM076) {
                val result =
                    validationUseCase.validationRaviInper(_visitData.value?.id.toString())
                if (result.isSuccess) {
                    val data = result.getOrNull().orEmpty()
                    OnManageViewModelEvent.onViewModelReadyAct076(data.distinct())
                } else {
                    OnManageViewModelEvent.OnLoadingError
                }
            } else {
                OnManageViewModelEvent.onViewModelReadyAct076(emptyList())
            }
        } else {
            onLoadBlock()
        }
    }

    fun validateDilgenceActa() = execute {
        val isFinishActa = plantillasUseCase.isFinishedAct(
            plantillaId.value.toString(), _idRequest.value.toString()
        ) == true
        if (groupVisitSelected?.id == 44 && isFinishActa) {
           _event.value = OnManageViewModelEvent.OnValidateDiligenceActaReady
        } else {
            validFM076()
        }
    }

    fun countQuerys() = execute {
        _count.value =
            if (userUseCase.userIsPapf()) syncDataUseCase.syncPapfPending() else syncDataUseCase.syncPending()
    }

    fun clean() {
        groupVisitSelected = null
        actSelected = null
        _visitData.value?.id?.let { init(it) }
    }
}