package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity

@Entity(tableName = "historic_mss_table")
class HistoricMssModel(
    @ColumnInfo(name = "numeroExpediente") val numeroExpediente: String?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "empresaAsociada") val empresaAsociada: String?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "municipio") val municipio: String?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun HistoricMssModel.mapToDomain() = HistoricMssEntity(
    numeroExpediente.orEmpty(),
    razonSocial.orEmpty(),
    empresaAsociada.orEmpty(),
    direccion.orEmpty(),
    municipio.orEmpty(),
    emptyList(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty()
)

@Entity(tableName = "mss_table")
class MssModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "numeroMSS") val numeroMSS: String?,
    @ColumnInfo(name = "numeroVisita") val numeroVisita: String?,
    @ColumnInfo(name = "razonVisita") val razonVisita: String?,
    @ColumnInfo(name = "aplicado") val aplicado: String?,
    @ColumnInfo(name = "idTipoMedidaSanita") val idTipoMedidaSanita: Int?,
    @ColumnInfo(name = "tipoMedidaSanita") val tipoMedidaSanita: String?,
    @ColumnInfo(name = "producto") val producto: String?,
    @ColumnInfo(name = "idEstadoMedidaSanita") val idEstadoMedidaSanita: Int?,
    @ColumnInfo(name = "estadoMSS") val estadoMSS: String?,
    @ColumnInfo(name = "fechaCierreVisita") val fechaCierreVisita: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "tipoProducto") val tipoProducto: String?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun MssModel.mapToDomain() = MssEntity(
    id,
    numeroMSS,
    numeroVisita,
    razonVisita,
    aplicado,
    idTipoMedidaSanita,
    tipoMedidaSanita,
    producto,
    idEstadoMedidaSanita,
    estadoMSS,
    fechaCierreVisita,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    tipoProducto.orEmpty()
)