package com.soaint.domain.repository

import com.soaint.domain.model.BlockBodyValueEntity
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.PlantillaEntity
import kotlinx.coroutines.flow.StateFlow

interface PlantillasRepository {

    val plantillas: StateFlow<List<PlantillaEntity>?>

    suspend fun loadPlantillas(idTipoProducto: Int): Result<List<PlantillaEntity>>

    suspend fun loadPlantillasLocal(idPlantilla: String): Result<List<PlantillaEntity>>

    suspend fun loadPlantillasLocal(): Result<List<PlantillaEntity>>

    suspend fun loadBlocks(idPlantilla: String): Result<List<BlockEntity>>

    suspend fun loadBlockLocal(idPlantilla: String): Result<List<BlockEntity>>

    suspend fun saveBlockBody(idPlantilla: String, idVisita: String, body: String, id: Int)

    suspend fun getBlockBody(idPlantilla: String, idVisita: String): String?
    suspend fun selectBodyBlockUnfinished(idPlantilla: String, idVisita: String): Result<BlockBodyValueEntity?>
    suspend fun isFinishedAct(idPlantilla: String, idVisita: String): Boolean?

    suspend fun saveFinishedActa(idPlantilla: String, idVisita: String, id: Int)
    suspend fun isEditableActa(idPlantilla: String, idVisita: String, id: Int): Boolean?

    suspend fun getAllBlockBody(): List<BlockBodyValueEntity>

    suspend fun deleteBlockBody(idPlantilla: String, idVisita: String, id: Int)
}