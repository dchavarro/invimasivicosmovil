package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.*

// Producto
@Entity(tableName = "type_papf_table")
class TypePapfModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "codeQuery") val codeQuery: String?,
    @ColumnInfo(name = "idTipoTramite") val idTipoTramite: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun TypePapfModel.mapToDomain() = DinamicQuerysPapfEntity(
    id,
    descripcion.orEmpty(),
    codeQuery.orEmpty(),
    idTipoTramite.orEmpty(),
)
