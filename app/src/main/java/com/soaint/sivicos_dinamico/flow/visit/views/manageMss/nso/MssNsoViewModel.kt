package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.nso

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class onMssNsoViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onMssNsoViewModelEvent()
    data class onViewModelReadyAmssp(val amssp: List<ManageMssEntity>): onMssNsoViewModelEvent()
    data class onViewModelReadyEmssp(val emssp: List<ManageMssEntity>): onMssNsoViewModelEvent()
    data class onViewModelReadyMsspList(val msspList: List<ManageMsspEntity>): onMssNsoViewModelEvent()
    object onViewModelUpdateMsspList: onMssNsoViewModelEvent()
    object onViewModelInsertMsspList: onMssNsoViewModelEvent()
    data class onViewModelReadySearch(val search: String): onMssNsoViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onMssNsoViewModelEvent()
    data class onSnackLoadingError(@StringRes val message: Int) : onMssNsoViewModelEvent()

}

class MssNsoViewModel @Inject constructor(
    private val manageMssUseCase: ManageMssUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onMssNsoViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private var amssp: List<ManageMssEntity> = emptyList()

    private var emssp: List<ManageMssEntity> = emptyList()

    private var emsspSelected: ManageMssEntity? = null

    private var msspList: List<ManageMsspEntity> = emptyList()

    private var nombreProducto: String? = EMPTY_STRING

    private var registroSanitario: String? = EMPTY_STRING

    private var lote: String? = EMPTY_STRING

    val idTipoProducto get() = _visitData.value?.firstOrNull()?.idTipoProducto.orZero()


    fun init(visitData: List<VisitEntity>) = execute {
        _visitData.value = visitData
        onGetEmssp()
        onGetMsspList()
    }

    fun onGetAmssp() = execute {
        _event.value =
            onMssNsoViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        val amssp_ = manageMssUseCase.loadTypeMssLocal(MSSP, idTipoProducto)
        val resultAmssp = amssp_.getOrNull()
            ?.filter { it.codigo == ID_CODE_MSS_SUSPENSION || it.codigo == ID_CODE_MSS_CANCELACION
                    || it.codigo == ID_CODE_MSS_RETIRO
            }
        _event.value = if (!resultAmssp.isNullOrEmpty()) {
            amssp = resultAmssp.orEmpty()
            onMssNsoViewModelEvent.onViewModelReadyAmssp(resultAmssp)
        } else {
            onMssNsoViewModelEvent.onLoadingError(null)
        }
    }

    fun setAmsspSelected(code: String, isSelected: Boolean) {
        amssp.find { it.codigo == code }?.let {
            it.isSelected = isSelected
            //onUpdateAmssp(it)
        }
    }

    fun onGetEmssp() = execute {
        val resultMssp = manageMssUseCase.loadTypeMssLocal(EMSSP, idTipoProducto).getOrNull()
        _event.value = if (!resultMssp.isNullOrEmpty()) {
            emssp = resultMssp
            onMssNsoViewModelEvent.onViewModelReadyEmssp(resultMssp)
        } else {
            onMssNsoViewModelEvent.onLoadingError(null)
        }
    }

    fun setEmsspSelected(groupSelected: String) {
        emssp.find { it.descripcion.equals(groupSelected, true) }?.let {
            emsspSelected = it
        }
    }

    fun onGetMsspList() = execute {
        val msspList_ = _visitData.value?.filter { it.empresa != null }?.mapNotNull { manageMssUseCase.loadListMssLocal(it.empresa?.razonSocial.orEmpty(), MSSP) }
        val resultMsspList = msspList_?.filter { it.isSuccess }?.mapNotNull { it.getOrNull() }?.flatten()
        msspList = resultMsspList?.filter { it.codigoMedidaSanitaria == ID_CODE_MSS_SUSPENSION
                || it.codigoMedidaSanitaria == ID_CODE_MSS_CANCELACION
                || it.codigoMedidaSanitaria == ID_CODE_MSS_RETIRO
        }.orEmpty()
        _event.value = onMssNsoViewModelEvent.onViewModelReadyMsspList(msspList)
    }

    fun setMsspSelected(id: Int, isSelected: Boolean) {
        msspList.find { it.id == id }?.let {
            it.isSelected = isSelected
        }
    }

    fun onUpdate() = execute {
        msspList.map {
            if(it.isSelected) {
                val msspList = manageMssUseCase.updateMsspListLocal(
                    it.id.orZero(),
                    if (emsspSelected?.codigo.isNullOrEmpty()) it.codigoMedidaSanitaria.orEmpty()
                    else emsspSelected?.codigo.orEmpty(),
                    if (emsspSelected?.tipoMs.isNullOrEmpty()) it.tipoMs.orEmpty()
                    else emsspSelected?.tipoMs.orEmpty(),
                    it.lote.orEmpty()
                    )
                if (msspList.isSuccess) {
                    it.isSelected = false
                    _event.value = onMssNsoViewModelEvent.onViewModelUpdateMsspList
                } else {
                    onMssNsoViewModelEvent.onLoadingError(null)
                }
            }
        }
        onGetEmssp()
        onGetMsspList()
    }

    fun setLote(id: Int, lote: String, isSelected: Boolean) {
        msspList.find { it.id == id }?.let {
            it.isSelected = isSelected
            it.lote = lote
        }
    }

    fun onSearch(search: String) = execute {
        val result = manageMssUseCase.searchRegisterSanitary(search)
        _event.value = when (result.exceptionOrNull()) {
            is WithoutConnectionException -> onMssNsoViewModelEvent.onSnackLoadingError(R.string.no_internet)
            is Exception -> onMssNsoViewModelEvent.onLoadingError(result.exceptionOrNull())
            else -> onMssNsoViewModelEvent.onViewModelReadySearch(result.getOrNull().orEmpty())
        }
    }

    fun onInsert() = execute {
        if (!nombreProducto.isNullOrEmpty()) {
            amssp.filter { it.isSelected }.onEach {
                val msspList = manageMssUseCase.insertMsseListLocal(
                    _visitData.value?.firstOrNull()!!,
                    it.codigo.orEmpty(),
                    MSSP,
                    EMPTY_STRING,
                    nombreProducto.orEmpty(),
                    registroSanitario.orEmpty()
                )
                if (msspList.isSuccess) {
                    _event.value = onMssNsoViewModelEvent.onViewModelInsertMsspList
                } else {
                    onMssNsoViewModelEvent.onLoadingError(null)
                }
            }
            onGetEmssp()
            onGetMsspList()
        } else {
            _event.value = onMssNsoViewModelEvent.onSnackLoadingError(R.string.copy_option)
        }

    }

    fun setSearchSelected(nombreProducto: String, registroSanitario: String) {
        this.nombreProducto = nombreProducto
        this.registroSanitario = registroSanitario
    }
}