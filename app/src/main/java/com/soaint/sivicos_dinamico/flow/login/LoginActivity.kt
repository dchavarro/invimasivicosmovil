package com.soaint.sivicos_dinamico.flow.login

import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.autofill.AutofillManager
import android.widget.EditText
import androidx.activity.viewModels
import com.google.android.material.textfield.TextInputLayout
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.FragmentLoginBinding
import com.soaint.sivicos_dinamico.extensions.*
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.properties.activityBinding

class LoginActivity : BaseActivity() {

    private val binding: FragmentLoginBinding by activityBinding(R.layout.fragment_login)

    private val viewModel: LoginViewModel by viewModels { viewModelFactory }

    override fun onCreate(
        savedInstanceState: Bundle?
    ) {
        super.onCreate(savedInstanceState)
        initView()
        observerViewModelEvents()
    }

    private fun initView() = with(binding) {
        textViewVersion.text = getString(R.string.copy_version, BuildConfig.VERSION_NAME)
        txtPswd.setOnKeyListener { _, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_ENTER && event.action == KeyEvent.ACTION_UP) {
                doLogin()
                true
            } else false
        }
        btnIngresar.setOnSingleClickListener { doLogin() }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(this, ::eventHandler)
    }

    private fun eventHandler(event: OnLoginViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnLoginViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnLoginViewModelEvent.OnTokenError -> onTokenError(event.error)
            is OnLoginViewModelEvent.OnViewModelReady -> onViewModelReady()
        }
    }

    private fun doLogin() = with(binding) {
        val userName = txtUser.text.toString().trim()
        val userPass = txtPswd.text.toString().trim()
        if (validateUser(txtUser, txtUser, textInputLayoutUser) && validatePassword(txtPswd)) {
            viewModel.onDoLogin(userName, userPass)
        }
        hideKeyboard()
    }

    private fun initSavePass() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            getSystemService(AutofillManager::class.java)?.commit()
        }
    }

    private fun onTokenError(error: Throwable?) {
        showSnackBar(
            type = AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = error?.message ?: getString(R.string.copy_generic_error),
            msj = EMPTY_STRING
        ) {}
    }

    private fun onViewModelReady() {
        initSavePass()
        startActivity(MainActivity::class.java)
    }

    private fun validatePassword(inputPassword: EditText): Boolean {
        var inputLayoutPassword: TextInputLayout? = null
        if (inputPassword === binding.txtPswd) {
            inputLayoutPassword = binding.textInputLayoutPswd
        }
        if (inputPassword.text.toString().trim { it <= ' ' }.isEmpty()) {
            inputLayoutPassword?.error = getString(R.string.mensaje_pswd_no_valido)
            inputPassword.requestFocusView()
            return false
        } else {
            inputLayoutPassword?.isErrorEnabled = false
        }
        return true
    }
}