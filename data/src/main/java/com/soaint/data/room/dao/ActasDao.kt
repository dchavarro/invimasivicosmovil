package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.*
import com.soaint.domain.model.RulesBussinessEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface ActasDao {

    @Query("SELECT * FROM actas_table ORDER BY nombre ASC")
    fun getAlphabetizedActas(): Flow<List<Actas>>

    @Query("SELECT * FROM actas_table WHERE id_misional = :id_misional ORDER BY nombre ASC")
    fun getActasByMisional(id_misional: Int): Flow<List<Actas>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(acta: Actas)

    @Query("DELETE FROM actas_table")
    suspend fun deleteAll()

    // GRUPO DE VISITA
    @Query("SELECT * FROM group_visit_table WHERE idTipoProducto = :idTipoProducto")
    suspend fun selectGroupVisit(idTipoProducto: Int): List<GroupVisitModel>

    @Query("SELECT * FROM group_visit_table")
    suspend fun selectAllGroupVisit(): List<GroupVisitModel>

    @Insert(entity = GroupVisitModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGroupVisit(idVisit: GroupVisitModel)

    @Query("DELETE FROM group_visit_table WHERE idTipoProducto = :idTipoProducto")
    suspend fun deleteGroupVisitByIdTipoProducto(idTipoProducto: Int)


    // SUBGRUPO DE VISITA
    @Query("SELECT * FROM group_document_table")
    suspend fun selectGroupDocument(): List<GroupDocumentModel>

    @Insert(entity = GroupDocumentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGroupDocument(idVisit: GroupDocumentModel)

    @Query("DELETE FROM group_document_table")
    suspend fun deleteAllGroupDocument()


    // ACTA REGLA DE NEGOCIO DE VISITA
    @Query("SELECT * FROM rules_bussiness_act_table WHERE idGroup = :idGroup")
    suspend fun selectActaVisit(idGroup: Int): List<RulesBussinessModel>

    @Query("SELECT * FROM rules_bussiness_act_table WHERE idGroup = :idGroup AND idTipoProductoPapf = :idTipoProductoPapf")
    suspend fun selectActaVisit(idGroup: Int, idTipoProductoPapf: Int): List<RulesBussinessModel>

    // ACTA REGLA DE NEGOCIO DE VISITA
    @Query("SELECT * FROM rules_bussiness_act_table WHERE idPlantilla = :idPlantilla")
    suspend fun selectActaVisitByPlantilla(idPlantilla: String): RulesBussinessModel?

    @Insert(entity = RulesBussinessModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertActaVisit(idVisit: RulesBussinessModel)

    @Query("DELETE FROM rules_bussiness_act_table WHERE idGroup = :idGroup")
    suspend fun deleteActaVisitByIdGroup(idGroup: Int)

    @Query("DELETE FROM rules_bussiness_act_table WHERE idTipoProducto = :idTipoProducto")
    suspend fun deleteActaVisitByIdTipoProducto(idTipoProducto: Int)

    @Query("DELETE FROM rules_bussiness_act_table WHERE idTipoProductoPapf = :idTipoProductoPapf")
    suspend fun deleteAllActaXidTipoPapf(idTipoProductoPapf: Int)


    //PDF
    @Query("SELECT * FROM acta_pdf_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita")
    suspend fun selectPdfActa(idPlantilla: Int, idVisita: Int): ActasPdfModel

    @Insert(entity = ActasPdfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPdf(model: ActasPdfModel)
}