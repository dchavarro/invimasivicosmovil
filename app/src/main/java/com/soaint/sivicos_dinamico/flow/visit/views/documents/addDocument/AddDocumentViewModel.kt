package com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.ID_DOCUMENTARY_CLASSIFICATION_EJECU
import com.soaint.data.common.orZero
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.domain.model.*
import com.soaint.domain.use_case.DocumentUseCase
import com.soaint.domain.use_case.ManageUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnAddDocumentViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnAddDocumentViewModelEvent()
    data class OnViewModelGroupReady(val group: List<GroupDocumentEntity>) : OnAddDocumentViewModelEvent()

    data class OnViewModelDocumentNameReady(val documentName: List<DocumentNameEntity>) :
        OnAddDocumentViewModelEvent()

    object OnLoadingError : OnAddDocumentViewModelEvent()
    data class OnValidateData(
        val folio: String,
        val path: String,
        val name: String,
        val extension: String,
    ) : OnAddDocumentViewModelEvent()

    data class OnViewModelSuccess(val idVisit: String) : OnAddDocumentViewModelEvent()
}

class AddDocumentViewModel @Inject constructor(
    private val userUseCase: UserUseCase,
    private val manageUseCase: ManageUseCase,
    private val documentUseCase: DocumentUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnAddDocumentViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<VisitEntity?>()
    val visitData = _visitData.asLiveData()

    private val _folio = MutableLiveData(EMPTY_STRING)
    val folio get() = _folio.asLiveData()

    private var group: List<GroupDocumentEntity> = emptyList()

    private var documentName: List<DocumentNameEntity> = emptyList()

    private var groupSelected: GroupDocumentEntity? = null

    private var documentNameSelected: DocumentNameEntity? = null

    val idTipoProducto get() = _visitData.value?.idTipoProducto.orZero()


    /**
     * INIT
     * */

    fun init(visitData: VisitEntity?) = execute {
        _visitData.value = visitData
        onLoadGroup()
    }

    fun onFolioChange(data: Editable?) {
        _folio.value = data.toString()
    }

    /**
     * lOAD service
     * */

    private fun onLoadGroup() = execute {
        val result = manageUseCase.loadGroupDocumentLocal()
        _event.value = if (result.isSuccess) {
            group = result.getOrNull().orEmpty()
            OnAddDocumentViewModelEvent.OnViewModelGroupReady(result.getOrNull().orEmpty())
        } else {
            OnAddDocumentViewModelEvent.OnLoadingError
        }
    }

    fun setGroupSelected(groupSelectedview: String) {
        group.find { it.nombreDependencia.equals(groupSelectedview, true) }?.let {
            groupSelected = it
            onLoadDocumentName()
        }
    }

    private fun onLoadDocumentName() = execute {
        val result = documentUseCase.loadDocumentNameLocal(groupSelected?.idGrupo!!)
        _event.value = if (result.isSuccess) {
            documentName = result.getOrNull().orEmpty()
            OnAddDocumentViewModelEvent.OnViewModelDocumentNameReady(result.getOrNull().orEmpty())
        } else {
            OnAddDocumentViewModelEvent.OnLoadingError
        }
    }

    fun setDocumentNameSelected(actSelected: String) {
        documentName.find { it.nombreTipoDocumental.equals(actSelected, true) }?.let {
            this.documentNameSelected = it
        }
    }

    /**
     * Validate
     * */

    fun checkData(path: String, name: String, ext: String) {
        val folio = _folio.value!!
        if (folio.isBlank() || path.isBlank() || name.isBlank() || ext.isBlank())
            _event.value = OnAddDocumentViewModelEvent.OnValidateData(folio, path, name, ext)
        else {
            addDocument(path, name, ext)
        }
    }

    fun addDocument(path: String, name: String, ext: String) = execute {
        _event.value =
            OnAddDocumentViewModelEvent.OnChangeLabelPreloader(R.string.copy_save_document)
        val body = DocumentBodyEntity(
            idTipoDocumental = documentNameSelected?.idTipoDocumental!!,
            nombreDocumento = name,
            descripcionDocumento = documentNameSelected?.nombreTipoDocumental!!,
            extension = ext,
            archivoBase64 = path,
            metadata = MetadataEntity(
                tipologiaDocumental = documentNameSelected?.nombreTipoDocumental!!,
                tituloDocumento = name,
                autor = userUseCase.fullname(),
                autorFirmante = userUseCase.fullname(),
                folio_gd = _folio.value?.toString(),
                formato = ext,
                nroVisita = _visitData.value?.id.toString(),
                nombreEmpresa = _visitData.value?.empresa?.empresaAsociada.toString(),
                nitEmpresa = _visitData.value?.empresa?.numDocumento.toString(),
                fechaCreacionDocumento = getDateHourWithOutFormat()
            ),
            idVisita = _visitData.value?.id.toString(),
            numeroFolios = _folio.value?.toInt(),
            usuarioCrea = userUseCase.getUserInformation()?.usuario.toString(),
            idClasificacionDocumental = ID_DOCUMENTARY_CLASSIFICATION_EJECU,
        )
        val result = documentUseCase.addDocumentLocal(body)
        _event.value = if (result.isSuccess) {
            OnAddDocumentViewModelEvent.OnViewModelSuccess(
                _visitData.value?.id?.toString().orEmpty()
            )
        } else {
            OnAddDocumentViewModelEvent.OnLoadingError
        }
    }
}