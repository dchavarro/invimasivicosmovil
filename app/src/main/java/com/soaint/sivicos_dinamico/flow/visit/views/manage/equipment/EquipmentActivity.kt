package com.soaint.sivicos_dinamico.flow.visit.views.equipment

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.EquipmentEntity
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityEquipmentBinding
import com.soaint.sivicos_dinamico.extensions.selectValue
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.flow.diligence.KEY_ID
import com.soaint.sivicos_dinamico.flow.visit.item_view.EquipmentItemView
import com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument.KEY_VISIT
import com.soaint.sivicos_dinamico.flow.visit.views.equipment.OnEquipmentViewModelEvent.*
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

const val KEY_VISIT = "KEY_VISIT"

class EquipmentActivity : BaseActivity() {

    companion object {
        fun getStartActivityEvent(
            visit: VisitEntity,
        ) = StartActivityEvent(
            EquipmentActivity::class.java,
            Bundle().apply { putParcelable(KEY_VISIT, visit) },
        )
    }

    private val binding by activityBinding<ActivityEquipmentBinding>(R.layout.activity_equipment)

    private val viewModel by viewModels<EquipmentViewModel> { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    private var equipment: EquipmentEntity? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.root
        initView()
        observerViewModelEvents()
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = getString(R.string.equipment)

        binding.rv.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter

        binding.buttonAdd.setOnSingleClickListener { viewModel.checkData(null) }
        binding.buttonSave.setOnSingleClickListener { viewModel.checkData(equipment) }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(this, ::eventHandler)
        val visit: VisitEntity? = intent.getParcelableExtra(KEY_VISIT)
        viewModel.init(visit)
    }

    private fun eventHandler(event: OnEquipmentViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnLoadingError -> onError()
            is OnViewModelZoneReady -> onZoneReady(event.zone)
            is OnEquipmentReady -> OnEquipmentReady(event.equipment)
            is OnValidateData -> validateErrorData(
                event.equipment,
                event.contact,
                event.other,
                event.zoneId
            )
            is OnViewModelSuccess -> {
                showSnack(true, event.message)
                clean()
            }
            else -> {}
        }
    }

    private fun validateErrorData(equipment: String, contact: String, other: String, zoneId: Int?) {
        validateField(binding.textInputEquipment, equipment)
        validateField(binding.textInputContact, contact)
        validateField(binding.textInputOther, other)
        validateSpinner(binding.spinnerZone, zoneId)
        if (equipment.isBlank() || contact.isBlank() || other.isBlank() || zoneId == null || zoneId == 0) onError()
    }

    private fun onError(message: String? = getString(R.string.copy_generic_error)) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                message.orEmpty(),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onSuccess() {
        finish()
    }

    private fun onZoneReady(group: List<ManageMssEntity>) {
        val array = group.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerZone.adapter =
            ArrayAdapter(this, android.R.layout.simple_list_item_1, items)
        binding.spinnerZone.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setZoneSelected(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReady() {}

    private fun OnEquipmentReady(equipment: List<EquipmentEntity>) {
        adapter.clear()
        val items = mutableListOf<Group>()

        if (!equipment.isNullOrEmpty()) {
            items.addAll(equipment.sortedByDescending { it.fechaCreacion }
                .map { EquipmentItemView(it, ::onEdit, ::onDelete) })
        }

        adapter.addAll(items)
        val isVisible = equipment.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
    }

    private fun onEdit(equipment: EquipmentEntity) {
        this.equipment = equipment
        binding.nestedScrollView.fullScroll(View.FOCUS_UP)
        binding.editTextEquipment.text = equipment.equipo.orEmpty().toEditable()
        binding.editTextContact.text = equipment.superficieContacto.orEmpty().toEditable()
        binding.editTextOther.text = equipment.otrasSuperficies.orEmpty().toEditable()
        binding.spinnerZone.selectValue(equipment.descripcionZonaVerificar)
        viewModel.setZoneSelected(equipment.descripcionZonaVerificar.orEmpty())
        binding.buttonSave.isVisible = true
    }

    private fun onDelete(equipment: EquipmentEntity) {
        showAlertDialog(
            showDialog(
                getString(R.string.adv),
                getString(R.string.copy_equipment_delete_message),
                titleBtnPositive = getString(R.string.confirmar),
                titleBtnNegative = getString(R.string.cerrar)
            )
            { viewModel.deleteEquipment(equipment) }
        )
    }

    private fun clean() {
        binding.editTextEquipment.text?.clear()
        binding.editTextContact.text?.clear()
        binding.editTextOther.text?.clear()
        binding.spinnerZone.selectValue(getString(R.string.copy_option))
        binding.buttonSave.isVisible = false
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}