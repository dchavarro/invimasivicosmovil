package com.soaint.sivicos_dinamico.flow.papf.views.transport

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.TransportObjectPapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(val transportDoc: TransportObjectPapfEntity?) :
        OnViewModelEvent()

    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class TransportPapfViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _invoiceData = MutableLiveData<TransportObjectPapfEntity?>()
    val invoiceData = _invoiceData.asLiveData()


    fun init(id: Int) {
        loadTransport(id)
    }

    fun loadTransport(requestId: Int) = execute {
        _event.value =
            OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando_transport)
        _invoiceData.value =
            papfUseCase.loadTransportLocal(requestId).getOrNull()
        _event.value = if (_invoiceData.value != null) {
            invoiceData.value?.let { OnViewModelEvent.OnViewModelReady(it) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
