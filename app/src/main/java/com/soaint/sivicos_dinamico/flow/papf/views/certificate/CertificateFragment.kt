package com.soaint.sivicos_dinamico.flow.papf.views.certificate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.*
import com.soaint.domain.model.SanitaryCertificatePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCertificatePapfBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.item_view.CertificatePapfItemView
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class CertificateFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCertificatePapfBinding>(R.layout.fragment_certificate_papf)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: CertificateViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.certification_conditional_papf))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(CertificateFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.idRequest.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.certificate)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReady(certificates: List<SanitaryCertificatePapfEntity>) {
        adapter.clear()
        val items = mutableListOf<Group>()
        if (!certificates.isNullOrEmpty()) {

            if (certificates.any { it.idTipo == ID_TYPE_VETERINARY }) {
                items.add(TitleItemView(getString(R.string.copy_certificate_veterinaty)))
                items.addAll(certificates.filter { it.idTipo == ID_TYPE_VETERINARY }
                    .map { CertificatePapfItemView(it) })
            }
            if (certificates.any { it.idTipo == ID_TYPE_SANITARY }) {
                items.add(TitleItemView(getString(R.string.copy_certificate_sanitary)))
                items.addAll(certificates.filter { it.idTipo == ID_TYPE_SANITARY }
                    .map { CertificatePapfItemView(it) })
            }
            if (certificates.any { it.idTipo == ID_TYPE_PROCCESS }) {
                items.add(TitleItemView(getString(R.string.copy_certificate_process)))
                items.addAll(certificates.filter { it.idTipo == ID_TYPE_PROCCESS }
                    .map { CertificatePapfItemView(it) })
            }
            if (certificates.any { it.idTipo == ID_TYPE_HACCP }) {
                items.add(TitleItemView(getString(R.string.copy_certificate_haccp)))
                items.addAll(certificates.filter { it.idTipo == ID_TYPE_HACCP }
                    .map { CertificatePapfItemView(it) })
            }
            if (certificates.any { it.idTipo == ID_TYPE_ACT }) {
                items.add(TitleItemView(getString(R.string.copy_certificate_act_control)))
                items.addAll(certificates.filter { it.idTipo == ID_TYPE_ACT }
                    .map { CertificatePapfItemView(it) })
            }
        }

        val isVisible = certificates.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }

    private fun openRegisterCompany() {
        //findNavController().navigate(VisitInfoFragmentDirections.informacionVisitaToRegistroEmpresa())
    }
}