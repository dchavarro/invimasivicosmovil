package com.soaint.domain.repository

import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow

interface LocationRepository {

    suspend fun startLocationUpdates(): Flow<LatLng>

    fun stopLocationUpdates()

    suspend fun checkGpsStatus(): Boolean

    suspend fun getLocation(): LatLng?
}
