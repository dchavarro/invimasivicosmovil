package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.*

interface SanitaryApi {

    @GET("v1/visita/medidas-sanitarias/historico")
    suspend fun getHistoricoMss(
        @Query("razonSocial") razonSocial: String,
        @Query("pagination") pagination: Boolean = false,
    ): HistoricMssResponseDto

    @POST("v1/visita/medidas-sanitariassp")
    suspend fun getMssp(
        @Body body: MssBodyDto
    ): MsspResponseDto

    @PUT("v1/visita/medidas-sanitarias")
    suspend fun updateMssp(
        @Body body: List<UpdateMssBodyDto>
    ): Unit

    @POST("v1/visita/medidas-sanitarias")
    suspend fun insertMsse(
        @Body body: List<InsertMssBodyDto>
    ): Unit

    @GET("v1/visita/medidas-sanitarias/aplicadas")
    suspend fun getMssApplied(
        @Query("idVisita") idVisita: String,
    ): MssAppliedResponseDto

    @POST("v1/visita/medidas-sanitarias/aplicadas")
    suspend fun insertMssApplied(
        @Body body: MssAppliedBodyDto
    ): Unit

}