package com.soaint.sivicos_dinamico.common

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.soaint.domain.dispatchers.AppDispatchers
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.model.NavigationEvents
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {

    @Inject
    lateinit var appDispatchers: AppDispatchers

    open fun onSaveInstanceState() = Bundle()

    open fun onRestoreInstanceState(savedInstanceState: Bundle) {}

    /**
     * Excute
     */

    protected val _loader = MutableLiveData<Boolean>()
    val loader get() = _loader.asLiveData()

    protected val _navigationEvent = SingleMutableLiveData<NavigationEvents>()
    val navigationEvent get() = _navigationEvent.asLiveData()

    /**
     * Loading
     */

    protected fun hideLoading() {
        _loader.value = false
    }

    protected fun showLoading() {
        _loader.value = true
    }

    protected fun execute(
        dispatcher: CoroutineDispatcher = appDispatchers.mainDispatcher(),
        action: suspend () -> Unit
    ) = viewModelScope.launch(dispatcher) { action() }

    protected fun observe(
        dispatcher: CoroutineDispatcher = appDispatchers.mainDispatcher(),
        observer: suspend () -> Unit
    ) = viewModelScope.launch(dispatcher) { observer.invoke() }
}