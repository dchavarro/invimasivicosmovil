package com.soaint.data.model


import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.model.*

data class BlockResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<BlockDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class BlockDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("codigo") val codigo: String?,
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("idPlantilla") val idPlantilla: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("atributosPlantilla") val atributosPlantilla: List<BlockAtributePlantillaDto>?,
)

data class BlockAtributePlantillaDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("codigo") val codigo: String?,
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("obligatorio") val obligatorio: Boolean?,
    @SerializedName("configuracion") val configuracion: BlockConfigAtributeDto?,
    @SerializedName("idTipoAtributo") val idTipoAtributo: Int?,
    @SerializedName("descTipoAtributo") val descTipoAtributo: String?,
    @SerializedName("idBloque") val idBloque: Int?,
    @SerializedName("numeroOrden") val numeroOrden: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("valor") val valor: Any?,
)

data class BlockConfigAtributeDto(
    @SerializedName("lista") val lista: List<BlockListAtributeDto>?,
    @SerializedName("longitud") val longitud: String?,
    @SerializedName("rangoFinal") val rangoFinal: String?,
    @SerializedName("rangoInicial") val rangoInicial: String?,
    @SerializedName("requerido") val requerido: Boolean?,
    @SerializedName("ocultar") val ocultar: Boolean?,
    @SerializedName("seleccionado") val seleccionado: Boolean?,
    @SerializedName("bloquearConInformacion") val bloquearConInformacion: Boolean?,
    @SerializedName("mensajeAyuda") val mensajeAyuda: String?
)

data class BlockListAtributeDto(
    @SerializedName("id") val id: String?,
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("seleccionado") val seleccionado: Boolean?
)

// map Entity
fun BlockDto.mapToDomain() = BlockEntity(
    id,
    codigo,
    nombre,
    descripcion,
    idPlantilla,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica,
    atributosPlantilla.orEmpty().map { it.mapToDomain() },
)

fun BlockAtributePlantillaDto.mapToDomain() = BlockAtributePlantillaEntity(
    id,
    codigo,
    nombre,
    descripcion,
    obligatorio,
    configuracion?.mapToDomain(),
    idTipoAtributo,
    descTipoAtributo,
    idBloque,
    numeroOrden,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    if (valor is String) valor else null,
    if (valor is ArrayList<*>) valor as? ArrayList<String> else null,
    if (valor is ArrayList<*>) valor as? ArrayList<ArrayList<String>> else null,
)

fun BlockConfigAtributeDto.mapToDomain() = BlockConfigAtributeEntity(
    lista?.map { it.mapToDomain() },
    longitud,
    rangoFinal,
    rangoInicial,
    requerido,
    ocultar,
    seleccionado,
    bloquearConInformacion,
    mensajeAyuda
)

fun BlockConfigAtributeEntity.mapToData() = BlockConfigAtributeDto(
    lista?.map { it.mapToData() },
    longitud,
    rangoFinal,
    rangoInicial,
    requerido,
    ocultar,
    seleccionado,
    bloquearConInformacion,
    mensajeAyuda
)

fun BlockListAtributeDto.mapToDomain() = BlockListAtributeEntity(
    id,
    nombre.orEmpty(),
    seleccionado
)

fun BlockListAtributeEntity.mapToData() = BlockListAtributeDto(
    id,
    nombre,
    seleccionado
)

// mapToDB
fun BlockDto.mapToDb() = BlockModel(
    id,
    codigo.orEmpty(),
    nombre.orEmpty(),
    descripcion.orEmpty(),
    idPlantilla,
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
)

fun BlockAtributePlantillaDto.mapToDb() = BlockAtributePlantillaModel(
    id,
    codigo,
    nombre,
    descripcion,
    obligatorio,
    idTipoAtributo,
    descTipoAtributo,
    idBloque,
    numeroOrden,
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    usuarioModifica.orEmpty(),
    fechaModifica.orEmpty()
)

fun BlockConfigAtributeDto.mapToDb(idAtribute: Int?) = BlockConfigAtributeModel(
    idAtribute,
    longitud,
    rangoFinal,
    rangoInicial,
    requerido,
    ocultar,
    seleccionado,
    bloquearConInformacion,
    mensajeAyuda
)

fun BlockListAtributeDto.mapToDb(idConfig: Int?) = BlockListAtributeModel(
    id,
    nombre,
    seleccionado,
    idConfig
)
