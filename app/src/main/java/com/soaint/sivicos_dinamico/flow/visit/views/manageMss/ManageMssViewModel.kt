package com.soaint.sivicos_dinamico.flow.visit.views.manageMss

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class onManageMssViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onManageMssViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onManageMssViewModelEvent()
}

class ManageMssViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onManageMssViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _user = MutableLiveData<UserInformationEntity>()
    val user = _user.asLiveData()

    private val _aplicaMedidaSanitaria = MutableLiveData<Boolean>()
    val aplicaMedidaSanitaria = _aplicaMedidaSanitaria.asLiveData()

    private val _legalAtiende = MutableLiveData<Boolean>()
    val legalAtiende = _legalAtiende.asLiveData()

    fun init(visitData: List<VisitEntity>) = execute {
        _user.value = userUseCase.getUserInformation()
        _visitData.value = visitUseCase.loadVisitLocal(visitData.firstOrNull()?.idTarea.orEmpty()).getOrNull().orEmpty()
        _aplicaMedidaSanitaria.value = _visitData.value?.firstOrNull()?.aplicaMedidaSanitaria
        _legalAtiende.value = _visitData.value?.firstOrNull()?.legalAtiende
    }

    fun setAplicaMedidaSanitaria(aplicaMedidaSanitaria: Boolean) {
        _aplicaMedidaSanitaria.value = aplicaMedidaSanitaria
        updateVisit()
    }

    fun setLegalAtiende(legalAtiende: Boolean) {
        _legalAtiende.value = legalAtiende
        updateVisit()
    }

    fun updateVisit() = execute {
        val body = _visitData.value?.firstOrNull()?.copy(
            aplicaMedidaSanitaria = _aplicaMedidaSanitaria.value,
            legalAtiende = _legalAtiende.value
        )
        body?.let { visitUseCase.updateVisitLocal(it, false) }
        _visitData.value = visitUseCase.loadVisitLocal(_visitData.value?.firstOrNull()?.idTarea.orEmpty()).getOrNull().orEmpty()
    }
}