package com.soaint.domain.repository

import com.soaint.domain.model.BodyObservationEntity
import com.soaint.domain.model.VisitEntity

interface ValidationRepository {

    suspend fun validationMss(idVisita: String, tipoMs: String): Result<List<String>>

    suspend fun validationUpdateMssList(idVisita: String, tipoMs: String): Result<List<String>>

    suspend fun validationLegalAtiende(idVisita: String): Result<Boolean>

    suspend fun addObservation(
        idVisita: String,
        observation: String,
        idTipoProducto: Int
    ): Result<Unit>

    suspend fun getObservationLocal(
        idVisita: String,
        idTipoProducto: Int
    ): Result<BodyObservationEntity>

    suspend fun updateObservation(body: BodyObservationEntity?): Result<Unit?>

    suspend fun updateProcessExt(visita: VisitEntity): Result<Unit>

    suspend fun validationRaviInper(idVisita: String): Result<List<String>>

    suspend fun validationAct015(idVisita: String): Result<Boolean>

    suspend fun generateRadicals(idVisita: String): Result<Unit>

    suspend fun validationActs(id: Int, codigosPlantillas: List<String>): Result<List<String>>
    suspend fun validateAtLeastOneActs(idVisita: Int): Int

    suspend fun validationActsFinished(
        id: Int,
        codigosPlantillas: List<String>
    ): Result<List<String>>

}