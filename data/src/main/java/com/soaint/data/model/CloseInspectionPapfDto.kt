package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.ClasificationTramitPapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ClasificationTramitPapfEntity
import com.soaint.domain.model.CloseInspectionPapfEntity
import com.soaint.domain.model.DataReqActSamplePapfEntity

data class CloseInspectionPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<CloseInspectionPapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class CloseInspectionPapfDto(
    @SerializedName("ClasificacionTramite") val clasificacionTramite: ClasificationTramitPapfDto? = null,

    )

data class ClasificationTramitPapfDto(
    @SerializedName("IdTipoProductoPapf") val idTipoProductoPapf: Int? = 0,
    @SerializedName("TipoProductoPapf") val tipoProductoPapf: String?,
    @SerializedName("IdActividad") val idActividad: Int? = 0,
    @SerializedName("Actividad") val actividad: String?,
    @SerializedName("IdTipoActividad") val idTipoActividad: Int? = 0,
    @SerializedName("TipoActividad") val tipoActividad: String?,
    @SerializedName("IdIdiomaCis") val idIdiomaCis: Int? = 0,
    @SerializedName("IdiomaCis") val idiomaCis: String?,
    @SerializedName("CertificadoExportacion") val certificadoExportacion: Boolean?,
)

fun CloseInspectionPapfDto.mapToDomain() = CloseInspectionPapfEntity(
    clasificacionTramite?.mapToDomain()
)

fun ClasificationTramitPapfDto.mapToDomain() = ClasificationTramitPapfEntity(
    idTipoProductoPapf,
    tipoProductoPapf,
    idActividad,
    actividad,
    idTipoActividad,
    tipoActividad,
    idIdiomaCis,
    idiomaCis,
    certificadoExportacion,
)

fun ClasificationTramitPapfDto.mapToDb(idSolicitud: Int?) = ClasificationTramitPapfModel(
    idTipoProductoPapf,
    tipoProductoPapf,
    idActividad,
    actividad,
    idTipoActividad,
    tipoActividad,
    idIdiomaCis,
    idiomaCis,
    certificadoExportacion,
    idSolicitud.orZero(),
)

fun ClasificationTramitPapfEntity.mapToData() = ClasificationTramitPapfDto(
    idTipoProductoPapf,
    tipoProductoPapf,
    idActividad,
    actividad,
    idTipoActividad,
    tipoActividad,
    idIdiomaCis,
    idiomaCis,
    certificadoExportacion,
)