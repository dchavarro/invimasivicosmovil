package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.*

@Dao
interface HistoricDao {

    @Query("SELECT * FROM historic_table WHERE razonSocial = :razonSocial ORDER BY fechaCreacion DESC LIMIT 2")
    suspend fun selectHistoric(razonSocial: String): List<HistoricModel>

    @Insert(entity = HistoricModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHistoric(idVisit: HistoricModel)

    @Query("DELETE FROM historic_table WHERE razonSocial = :razonSocial")
    suspend fun deleteHistoricByRazon(razonSocial: String)
}