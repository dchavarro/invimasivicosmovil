package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.EquipmentModel
import com.soaint.domain.model.EquipmentEntity

data class EquipmentResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<EquipmentDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class EquipmentDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("equipo") val equipo: String?,
    @SerializedName("codigoZonaVerificar") val codigoZonaVerificar: String?,
    @SerializedName("superficieContacto") val superficieContacto: String?,
    @SerializedName("otrasSuperficies") val otrasSuperficies: String?,
    @SerializedName("idZonaVerificar") val idZonaVerificar: Int?,
    @SerializedName("descripcionZonaVerificar") val descripcionZonaVerificar: String?,
    @SerializedName("idResultado") val idResultado: Int?,
    @SerializedName("codigoResultado") val codigoResultado: String?,
    @SerializedName("descripcionResultado") val descripcionResultado: String?,
    @SerializedName("idSede") val idSede: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("observaciones") val observacion: String?,
)

fun EquipmentDto.mapToDomain() = EquipmentEntity(
    id,
    equipo,
    codigoZonaVerificar,
    superficieContacto,
    otrasSuperficies,
    idZonaVerificar,
    descripcionZonaVerificar,
    idResultado,
    codigoResultado,
    descripcionResultado,
    idSede,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica
)

fun EquipmentDto.mapToDb() = EquipmentModel(
    id,
    equipo,
    codigoZonaVerificar,
    superficieContacto,
    otrasSuperficies,
    idZonaVerificar,
    descripcionZonaVerificar,
    idResultado,
    codigoResultado,
    descripcionResultado,
    idSede,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    observacion
)

fun EquipmentEntity.mapToData() = EquipmentDto(
    id,
    equipo,
    codigoZonaVerificar,
    superficieContacto,
    otrasSuperficies,
    idZonaVerificar,
    descripcionZonaVerificar,
    idResultado,
    codigoResultado,
    descripcionResultado,
    idSede,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    observacion
)