package com.soaint.sivicos_dinamico.flow.web

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.os.bundleOf
import com.soaint.data.common.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityWebviewBinding
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding

const val URL = "URL"
const val TITLE = "TITLE"

class WebViewActivity : BaseActivity() {

    companion object {
        fun getStartActivityEvent(
            url: String = EMPTY_STRING,
            title: String = EMPTY_STRING,
        ) = StartActivityEvent(
            WebViewActivity::class.java,
            bundleOf(
                URL to url,
                TITLE to title
            )
        )
    }

    private val binding by activityBinding<ActivityWebviewBinding>(R.layout.activity_webview)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(binding.toolbar)
        initView()
    }

    @SuppressLint("SetJavaScriptEnabled")
    fun initView() {
        showLoading(true, R.string.cargando)

        var url = intent.getStringExtra(URL)
        val title = intent.getStringExtra(TITLE)
        binding.apply {
            supportActionBar?.title = title
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setDisplayShowHomeEnabled(true)
            //toolbar.setOnSingleClickListener { finish() }
            webview.settings.javaScriptEnabled = true
            webview.settings.useWideViewPort = true
            webview.settings.loadWithOverviewMode = true
            webview.settings.setSupportZoom(true)
            webview.settings.builtInZoomControls = true
            webview.settings.displayZoomControls = false
            webview.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, url: String): Boolean {
                    view?.loadUrl(url)
                    return true
                }
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    showLoading(false)
                }
            }

/*            webview.setDownloadListener(DownloadListener { url, userAgent, contentDisposition, mimetype, contentLength ->
                val i = Intent(Intent.ACTION_VIEW)
                i.data = Uri.parse(url)
                startActivity(i)
            })*/

            if (url != null) {
/*                if(url!!.contains(".pdf")){
                    url = "https://drive.google.com/viewerng/viewer?embedded=true&url=" + url
                }*/
                webview.loadUrl(url.replace(" ", "%20"))
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (binding.webview.canGoBack()) {
            binding.webview.goBack()
        }
        super.onBackPressed()
    }

}