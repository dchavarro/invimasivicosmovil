package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.*

@Dao
interface TransversalDaoDao {

    //tipos de  docuemntos
    @Query("SELECT * FROM type_dao_table WHERE tipoDao = :tipoDao")
    suspend fun selectTypeDao(tipoDao: String): List<TypeDaoModel>

    @Insert(entity = TypeDaoModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTypeDao(model: TypeDaoModel)

    @Query("DELETE FROM type_dao_table WHERE tipoDao = :tipoDao")
    suspend fun deleteAllTypeDao(tipoDao: String)

    //paises
    @Query("SELECT * FROM countries_table")
    suspend fun selectCountries(): List<CountriesModel>

    @Insert(entity = CountriesModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCountries(model: CountriesModel)

    @Query("DELETE FROM countries_table")
    suspend fun deleteAllCountries()

    //Departamentos
    @Query("SELECT * FROM department_table WHERE idPais = :idPais")
    suspend fun selectDepartment(idPais: Int): List<DepartmentModel>

    @Insert(entity = DepartmentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDepartment(model: DepartmentModel)

    @Query("DELETE FROM department_table")
    suspend fun deleteAllDepartment()


    //Municipios
    @Query("SELECT * FROM town_table WHERE idDepartamento = :idDepartament")
    suspend fun selectTown(idDepartament: Int): List<TownModel>

    @Insert(entity = TownModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTown(model: TownModel)

    @Query("DELETE FROM Town_table")
    suspend fun deleteAllTown()
}




