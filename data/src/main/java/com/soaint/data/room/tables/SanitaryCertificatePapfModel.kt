package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*

@Entity(tableName = "sanitary_certificate_papf_table")
class SanitaryCertificatePapfModel(
    @ColumnInfo(name = "idCertificado") val idCertificado: String?,
    @ColumnInfo(name = "fechaExpedicion") val fechaExpedicion: String?,
    @ColumnInfo(name = "pais") val pais: String?,
    @ColumnInfo(name = "archivoSharePoint") val archivoSharePoint: String?,
    @ColumnInfo(name = "archivoSesuite") val archivoSesuite: String?,
    @ColumnInfo(name = "idDocumento") val idDocumento: Int? = 0,
    @ColumnInfo(name = "IdTipo") val idTipo: Int? = 0,
    @ColumnInfo(name = "TipoCertificado") val tipoCertificado: String?,
    @ColumnInfo(name = "NombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun SanitaryCertificatePapfModel.mapToDomain() = SanitaryCertificatePapfEntity(
    idCertificado.orEmpty(),
    fechaExpedicion.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idDocumento.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
)