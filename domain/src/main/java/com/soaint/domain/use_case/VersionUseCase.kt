package com.soaint.domain.use_case

import com.soaint.domain.repository.VersionRepository
import javax.inject.Inject

class VersionUseCase @Inject constructor(
    private val versionRepository: VersionRepository
) {

    suspend fun validate(versionCode: String) =
        versionRepository.validateVersionCode(versionCode)
}