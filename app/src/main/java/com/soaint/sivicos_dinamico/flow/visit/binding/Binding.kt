package com.soaint.sivicos_dinamico.flow.visit.binding

import android.view.View
import android.widget.EditText
import android.widget.RadioButton
import androidx.core.view.isVisible
import androidx.databinding.BindingAdapter
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.model.VisitGroupEntity
import com.soaint.domain.model.VisitTramiteEntity
import com.soaint.sivicos_dinamico.utils.*

@BindingAdapter("isVisible")
fun setText(view: View, isVisible: Boolean?) {
    view.isVisible = isVisible ?: false
}

@BindingAdapter("text")
fun setText(editText: EditText, text: String?) {
    editText.setText(text.orEmpty())
}

@BindingAdapter("isCosmeticos")
fun setIsCosmeticos(view: View, text: Int?) {
    view.isVisible = text.orEmpty().toInt() == ID_MISIONAL_COSMETICOS
}

@BindingAdapter("isMedicamentos")
fun setIsMedicamentos(view: View, text: Int?) {
    view.isVisible = text.orEmpty().toInt() == ID_MISIONAL_MEDICAMENTOS
}

@BindingAdapter("isAlimentos")
fun setIsAlimentos(view: View, text: Int?) {
    view.isVisible = text.orEmpty().toInt() == ID_MISIONAL_ALIMENTOS
}

@BindingAdapter("isNotAlimentos")
fun setIsNotAlimentos(view: View, text: Int?) {
    view.isVisible = !(text.orEmpty().toInt() == ID_MISIONAL_ALIMENTOS)
}

@BindingAdapter("isDispositivos")
fun setIsDispositivos(view: View, text: Int?) {
    view.isVisible = text.orEmpty().toInt() == ID_MISIONAL_DISPOSITIVOS
}

@BindingAdapter("isSanitarias")
fun setIsSanitarias(view: View, text: String?) {
    view.isVisible = text.orEmpty().toInt() == ID_MISIONAL_SANITARIAS
}

@BindingAdapter("dateHour")
fun setFormartDateHour(editText: EditText, date: String?) {
    if (date != null) {
        editText.setText(date.formatDateHour())
    }
}

@BindingAdapter("tramit", "group")
fun setIsNotAlimentos(view: View, tramit: VisitTramiteEntity?, group: List<VisitGroupEntity>?) {
    view.isVisible = if (tramit == null) {
        if (!group.isNullOrEmpty()) {
            group.any { it.id == ID_GROUP_PLANTAS_ANIMAL }
        } else {
            false
        }
    } else {
        true
    }
}

@BindingAdapter("visit", "group")
fun setConceptVisible(view: View, visit: VisitEntity?, group: List<VisitGroupEntity>?) {
    // si tiene tramite ó si tiene grupo ID_GROUP_PLANTAS_ANIMAL ó RAVI_AYCVS ó RAVI_AYCVC
    view.isVisible = if (visit?.tramite != null || group?.any { it.id == ID_GROUP_PLANTAS_ANIMAL } == true ||
        visit?.codigoRazon ==  REASON_RAVI_AYCVS || visit?.codigoRazon ==  REASON_RAVI_AYCVC) {
        true
    } else false
}

@BindingAdapter("groupVisit", "isAlimento")
fun setIsVisiblePercentage(view: View, group: List<VisitGroupEntity>?, isAlimento: Int?) {
    view.isVisible = if (!group.isNullOrEmpty()) {
        group.any { it.id == ID_GROUP_PLANTAS_ANIMAL && isAlimento == ID_MISIONAL_ALIMENTOS }
    } else {
        false
    }
}

@BindingAdapter("isDispositivos", "requiereRetiro")
fun setIsVisibleSometimiento(
    view: View,
    isDispositivos: Int?,
    requiereRetiro: Boolean?,
) {
    view.isVisible = isDispositivos == ID_MISIONAL_DISPOSITIVOS && requiereRetiro == true
}

@BindingAdapter("isDispositivos", "sometimiento")
fun setIsVisibleIdRecall(
    view: View,
    isDispositivos: Int?,
    sometimiento: Boolean?,
) {
    view.isVisible = isDispositivos == ID_MISIONAL_DISPOSITIVOS && sometimiento == true
}

@BindingAdapter("reason")
fun setIsVisible(view: View, reason: String?) {
    view.isVisible =
        reason.orEmpty() == REASON_RAVI_AYCVC || reason.orEmpty() == REASON_RAVI_AYCVVR ||
                reason.orEmpty() == REASON_RAVI_AYCVVRMSS || reason.orEmpty() != REASON_RAVI_INPER
}

@BindingAdapter("isRaviInper", "isCosmeticos")
fun setIsVisibleRaviInper(view: View, reason: String?, isCosmeticos: Int?) {
    view.isVisible =
        (reason.orEmpty() != REASON_RAVI_INPER && isCosmeticos == ID_MISIONAL_COSMETICOS)
}

@BindingAdapter("isRaviInperCode")
fun setIsVisibleRaviInperCode(view: View, reason: String?) {
    view.isVisible = reason.orEmpty() != REASON_RAVI_INPER
}

@BindingAdapter("isRaviInper", "isDispositivos")
fun setIsDispositivosRaviInper(view: View, reason: String?, isDispositivos: Int?) {
    view.isVisible =
        (reason.orEmpty() != REASON_RAVI_INPER && isDispositivos == ID_MISIONAL_DISPOSITIVOS)
}

@BindingAdapter("isRaviInper", "isNotAlimentos")
fun setIsNotAlimentosRaviInper(view: View, reason: String?, isNotAlimentos: Int?) {
    view.isVisible =
        (reason.orEmpty() != REASON_RAVI_INPER && isNotAlimentos != ID_MISIONAL_ALIMENTOS)
}

@BindingAdapter("textEndSplit")
fun setTextEndSplit(editText: EditText, text: String?) {
    if (!text.isNullOrBlank()) {
        editText.setText(text.split("-").get(1))
    }
}

@BindingAdapter("codigoRural")
fun setIsRural(view: View, codigoRural: String?) {
    view.isVisible = codigoRural.orEmpty() != CODE_RURAL
}

@BindingAdapter("isRuralEnable")
fun setIsRuralEnable(view: View, codigoRural: String?) {
    view.isEnabled = codigoRural.orEmpty() == CODE_RURAL
}

@BindingAdapter("checked")
fun setChecked(view: RadioButton, checked: Boolean?) {
    view.isChecked = if (checked != null) checked else false
}

@BindingAdapter("checked_positive")
fun setCheckedPositive(view: RadioButton, checked: Boolean?) {
    view.isChecked = if (checked != null) checked else false
}

@BindingAdapter("checked_negative")
fun setCheckedNegative(view: RadioButton, checked: Boolean?) {
    view.isChecked = if (checked != null) !checked else false
}


@BindingAdapter("isExportation")
fun setIsExportation(view: View, isExportation: Int?) {
    view.isVisible = isExportation == ID_TYPE_EXPORTER
}

@BindingAdapter("isImportation")
fun setIsImportation(view: View, isImportation: Int?) {
    view.isVisible = isImportation == ID_TYPE_IMPORTER
}