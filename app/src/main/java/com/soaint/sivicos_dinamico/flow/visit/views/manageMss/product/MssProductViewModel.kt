package com.soaint.sivicos_dinamico.flow.visit.views.manageMss.product

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class onMsspViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onMsspViewModelEvent()
    data class onViewModelReadyAmssp(val amssp: List<ManageMssEntity>) : onMsspViewModelEvent()
    object onViewModelReadyAmsspCheck : onMsspViewModelEvent()
    data class onViewModelReadyEmssp(val emssp: List<ManageMssEntity>) : onMsspViewModelEvent()
    data class onViewModelReadyMsspList(val msspList: List<ManageMsspEntity>) :
        onMsspViewModelEvent()

    object onViewModelUpdateMsspList : onMsspViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onMsspViewModelEvent()
}

class MssProductViewModel @Inject constructor(
    private val manageMssUseCase: ManageMssUseCase,
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onMsspViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private var amssp: List<ManageMssEntity> = emptyList()

    private var emssp: List<ManageMssEntity> = emptyList()

    private var emsspSelected: ManageMssEntity? = null

    private var msspList: List<ManageMsspEntity> = emptyList()

    val idMisional: Int? get() = userUseCase.getUserMisional()

    val idTipoproducto get() = _visitData.value?.firstOrNull()?.idTipoProducto.orZero()


    fun init(visitData: List<VisitEntity>) = execute {
        _visitData.value = visitData
        _event.value = onMsspViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        onGetAmssp()
        onGetEmssp()
        onGetMsspList()
    }

    fun onGetAmssp() = execute {
        val resultAmssp = manageMssUseCase.loadTypeMssLocal(MSSP, idTipoproducto).getOrNull()
        if (!resultAmssp.isNullOrEmpty()) {
            amssp = resultAmssp
            onGetAmsspSelect()
        }
    }

    fun onGetAmsspSelect() = execute {
        val amssp_selected = manageMssUseCase.loadMssAppliedLocal(
            _visitData.value?.firstOrNull()?.id?.toString().orEmpty()
        )
        val result = amssp_selected.getOrNull()
        result?.mapNotNull { setAmsspSelected(it.codigo.orEmpty(), it.isSelected ?: false, false) }
        _event.value = onMsspViewModelEvent.onViewModelReadyAmssp(amssp)
    }

    fun setAmsspSelected(code: String, isSelected: Boolean, update: Boolean = true) {
        amssp.find { it.codigo == code }?.let {
            it.isSelected = isSelected
            if (update) onUpdateAmssp(it)
        }
    }

    fun onUpdateAmssp(it: ManageMssEntity) = execute {
        val amsspCheck =
            manageMssUseCase.updateAmsspLocal(it, _visitData.value?.firstOrNull()?.id.toString())
        if (amsspCheck.isSuccess) {
            _event.value = onMsspViewModelEvent.onViewModelReadyAmsspCheck
        } else {
            onMsspViewModelEvent.onLoadingError(null)
        }
    }

    fun onGetEmssp() = execute {
        val mssp = manageMssUseCase.loadTypeMssLocal(EMSSP, idTipoproducto)
        val resultMssp = mssp.getOrNull()
        _event.value = if (!resultMssp.isNullOrEmpty()) {
            emssp = mssp.getOrNull().orEmpty()
            onMsspViewModelEvent.onViewModelReadyEmssp(resultMssp)
        } else {
            onMsspViewModelEvent.onLoadingError(null)
        }
    }

    fun setEmsspSelected(groupSelected: String) {
        emssp.find { it.descripcion.equals(groupSelected, true) }?.let {
            emsspSelected = it
        }
    }

    fun onGetMsspList() = execute {
        val resultMsspList = _visitData.value?.filter { it.empresa != null }?.mapNotNull {
            manageMssUseCase.loadListMssLocal(it.empresa?.razonSocial.orEmpty(), MSSP)
        }
        msspList = resultMsspList?.filter { it.isSuccess }?.mapNotNull { it.getOrNull() }?.flatten().orEmpty()
        _event.value = onMsspViewModelEvent.onViewModelReadyMsspList(msspList)
    }

    fun setMsspSelected(id: Int, isSelected: Boolean) {
        msspList.find { it.id == id }?.let {
            it.isSelected = isSelected
        }
    }

    fun onUpdate() = execute {
        msspList.map {
            if (it.isSelected) {
                val msspList = manageMssUseCase.updateMsspListLocal(
                    it.id.orZero(),
                    emsspSelected?.codigo.orEmpty(),
                    emsspSelected?.tipoMs.orEmpty(),
                    it.lote.orEmpty()
                )
                if (msspList.isSuccess) {
                    it.isSelected = false
                    _event.value = onMsspViewModelEvent.onViewModelUpdateMsspList
                } else {
                    onMsspViewModelEvent.onLoadingError(null)
                }
            }
        }
        onGetEmssp()
        onGetMsspList()
    }
}