package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface GeneralesPapfApi {

    //SERVICIO CONSULTA TAREAS
    @POST("v1/trabajoAsignado")
    suspend fun getTasks(
        @Body body: TaskPapfBodyDto
    ): TaskPapfResponseDto

    //SERVICIO REGISTRAR PRODUCT
    @POST("v1/registrarProductos")
    suspend fun getProducts(
        @Body body: InfoTramitePapfBodyDto
    ): RegisterProductPapfResponseDto

    //SERVICIO DETALLE PRODUCT
    @POST("v1/buscarProducto")
    suspend fun getDetailProducts(
        @Body body: InfoTramitePapfBodyDto
    ): DetailProductPapfResponseDto

    //CERTIFICADOS SANITARIOS
    @POST("v1/listarCertificadoSanitario")
    suspend fun getSanitaryCertificate(
        @Body body: InfoTramitePapfBodyDto
    ): SanitaryCertificatePapfResponseDto

    //SERVICIO DOCUMENTATION
    @POST("v1/listarDocumento")
    suspend fun getDocumentation(
        @Body body: InfoTramitePapfBodyDto
    ): DocumentationPapfResponseDto

    //SERVICIO INFORMACION LEGAL
    @POST("v1/informacionLegal")
    suspend fun getInfoLegal(
        @Body body: InfoTramitePapfBodyDto
    ): InfoLegalPapfResponseDto

    //GUARDAR ACTAS INSPECCCION SANITARIA
    @POST("v1/guardarActaInspecc")
    suspend fun saveActaInspecc(
        @Body body: DataReqInsSanPapfBodyDto
    ): Unit

    //GUARDAR ACTAS TOMA MUESTRA
    @POST("v1/guardarActaTomaMuestras")
    suspend fun saveActaSample(
        @Body body: DataReqActSamplePapfBodyDto
    ): Unit

    //GUARDAR CIERRE
    @POST("v1/guardarCierre")
    suspend fun saveCloseInsp(
        @Body body: CloseInspPapfBodyDto
    ): Unit

    //ACTUALIZAR ESTADO
    @POST("v1/actualizarEstadoSolicitud")
    suspend fun udpateStatusRequest(
        @Body body: UpdateStatusPapfBodyDto
    ): Unit

    //SERVICIO DATOS BASICO CIERRE INSPECCION
    @POST("v1/datosBasicosSolicitud")
    suspend fun getDataBasic(
        @Body body: InfoTramitePapfBodyDto
    ): CloseInspectionPapfResponseDto

    //CONSULTAR ACTAS DILIGENCIAS
    @POST("v1/informacion/documento/acta")
    suspend fun getDocsActs(
        @Body body: DocsActsPapfBodyDto
    ): DocsActsPapfResponseDto

    //SERVICIO DATOS INFORMACION EMITIR CIS
    @POST("v1/informacionEmitirCis")
    suspend fun getDataEmitCis(
        @Body body: InfoTramitePapfBodyDto
    ): EmitCisPapfResponseDto

    //SERVICIO LISTA CHEQUEO
    @POST("v1/consultar/lista/chequeo")
    suspend fun getCheckList(
        @Body body: SolicitudPapfBodyDto
    ): CheckListCDResponseDto

    //SERVICIO BUSCAR PLANTILLA
    @POST("v1/buscarPlantillaEmision")
    suspend fun searchPlantillaEmit(
        @Body body: SearchPlantillaEmitPapfBodyDto
    ): SearchPlantillaEmitPapfResponseDto

    //SERVICIO GENERAR CONSECUTIVO
    @POST("v1/generarConsecutivo")
    suspend fun generateConsecutive(
        @Body body: GenerateConsecutivePapfBodyDto
    ): GenerateConsecutivePapfResponseDto

    @POST("v1/notificacion/inspeccion/fisica")
    suspend fun saveDateInspection(
        @Body body: RegisterDateBodyDto
    ): Unit

    @POST("v1/informacion/actas")
    suspend fun getInfoActa(
        @Body body: DataInfoActaPapfBodyDto
    ): InfoActaPapfResponseDto

    @POST("v1/asignarFuncionarioTramite")
    suspend fun assignFunctionary(
        @Body body: AssignFunctionaryBodyDto
    ): CloseAssignFunctionaryResponseDto

    @POST("v1/guardarActaSolicitud")
    suspend fun saveActsRequest(
        @Body body: SaveInfoActsPapfBodyDto
    ): SaveInfoActsPapfResponseDto
}