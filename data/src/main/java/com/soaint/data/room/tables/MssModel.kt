package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.common.orZero
import com.soaint.domain.model.*

// Producto
@Entity(tableName = "mss_type_table")
class TypeMssModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "codigoPlantillas") val codigoPlantillas: String?,
    @ColumnInfo(name = "codigoPlantillasActualizada") val codigoPlantillasActualizada: String?,
    @ColumnInfo(name = "tipoMs") val tipoMs: String?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: String?,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun TypeMssModel.mapToDomain() = ManageMssEntity(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
    codigoPlantillas,
    codigoPlantillasActualizada,
    tipoMs.orEmpty(),
    idTipoProducto.orEmpty(),
)

@Entity(tableName = "mss_list_table")
class MsspListModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "numeroVisita") val numeroVisita: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "numeroMss") val numeroMss: String?,
    @ColumnInfo(name = "nombreProducto") val nombreProducto: String?,
    @ColumnInfo(name = "tipoMedidaSanitaria") val tipoMedidaSanitaria: String?,
    @ColumnInfo(name = "estado") val estado: String?,
    @ColumnInfo(name = "idEstado") val idEstado: Int?,
    @ColumnInfo(name = "fechaAplicacion") val fechaAplicacion: String?,
    @ColumnInfo(name = "notificacionSanitariaObligatoria") val notificacionSanitariaObligatoria: String?,
    @ColumnInfo(name = "lote") val lote: String?,
    @ColumnInfo(name = "codigoMedidaSanitaria") val codigoMedidaSanitaria: String?,
    @ColumnInfo(name = "tiposActividades") val tiposActividades: String?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "tipoMs") val tipoMs: String?,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean = false,
    @ColumnInfo(name = "isUpdate") val isUpdate: Boolean = false,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun MsspListModel.mapToDomain() = ManageMsspEntity(
    id,
    numeroVisita.orEmpty(),
    idVisita.orZero(),
    numeroMss.orEmpty(),
    nombreProducto.orEmpty(),
    tipoMedidaSanitaria.orEmpty(),
    estado.orEmpty(),
    idEstado.orZero(),
    fechaAplicacion.orEmpty(),
    notificacionSanitariaObligatoria.orEmpty(),
    lote.orEmpty(),
    codigoMedidaSanitaria.orEmpty(),
    tiposActividades.orEmpty(),
    razonSocial,
    tipoMs,
)

@Entity(tableName = "amssp_check_table")
class AmsspCheckModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "isSelected") val isSelected: Boolean?,
    @ColumnInfo(name = "tipoMs") val tipoMs: String?,
    @ColumnInfo(name = "createdAt") val createdAt: String? = null,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun AmsspCheckModel.mapToDomain() = ManageAmssEntity(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
    idVisita.orZero(),
    isSelected,
    tipoMs.orEmpty(),
    createdAt.orEmpty(),
)


@Entity(tableName = "rs_table")
class RsModel(
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombreProducto") val nombreProducto: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean = false,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun RsModel.mapToDomain() = RsEntity(
    codigo.orEmpty(),
    nombreProducto.orEmpty(),
    idVisita.orZero(),
)
