package com.soaint.sivicos_dinamico.factories.controlsFactory.types

enum class AlertTypes {
    ERROR,
    SUCCESS,
    HELP,
    INFORMATION,
    SYNC
}