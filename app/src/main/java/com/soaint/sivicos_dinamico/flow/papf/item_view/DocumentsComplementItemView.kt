package com.soaint.sivicos_dinamico.flow.papf.item_view

import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.DocumentationPapfEntity
import com.soaint.domain.model.InvoicePapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemDocComplementPapfBinding
import com.soaint.sivicos_dinamico.databinding.ItemInvoiceBinding
import com.xwray.groupie.databinding.BindableItem

class DocumentsComplementItemView(
    private val item: DocumentationPapfEntity,
) : BindableItem<ItemDocComplementPapfBinding>() {

    override fun bind(
        viewBinding: ItemDocComplementPapfBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = item.tipoAdicional
        textViewDetail.text = item.nombreDocumento
        textViewFolio.text = item.folios
        textViewDate.text = item.fechaExpedicion?.formatDateHour()
        textViewClassification.text = item.clasificacion
    }

    override fun getLayout() = R.layout.item_doc_complement_papf
}