package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.BodyObservationEntity

@Entity(tableName = "observation_table")
class ObservationModel(
    @ColumnInfo(name ="descripcion") val descripcion: String,
    @ColumnInfo(name ="idTipoProducto") val idTipoProducto: Int,
    @ColumnInfo(name ="idVisita") val idVisita: Int,
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
)

fun ObservationModel.mapToDomain() = BodyObservationEntity(
    descripcion,
    idTipoProducto,
    idVisita
)