package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.orZero
import com.soaint.domain.model.*

data class RegisterCompanyBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("empresa") val empresa: RegisterCompanyDto,
    @SerializedName("Sede") val sede: RegisterCompanySedeDto?,
    @SerializedName("TipoProducto") val tipoProducto: List<TipoProductoDto>,
    @SerializedName("PersonasVinculadas") val personasVinculadas: List<VisitCompanyPersonDto>
) {
    constructor(ip: String, user: String, company: RegisterCompanyDto, sede: RegisterCompanySedeDto?,
                tipoProducto: List<TipoProductoDto>, PersonasVinculadas: List<VisitCompanyPersonDto>) : this(
        AuditoriaCapitalDto(ip, user),
        company,
        sede,
        tipoProducto,
        PersonasVinculadas
    )
}

data class RegisterCompanyDto(
    @SerializedName("CodigoTipoDocumento") val codigoTipoDocumento: Int,
    @SerializedName("NumeroDocumento") val numeroDocumento: String,
    @SerializedName("DigitoVerificacion") val digitoVerificacion: Int?,
    @SerializedName("RazonSocial") val razonSocial: String?,
    @SerializedName("NombreComercial") val nombreComercial: String?,
    @SerializedName("PaginaWeb") val paginaWeb: String?,
    @SerializedName("Pais") val pais: Int,
    @SerializedName("Departamento") val departamento: Int,
    @SerializedName("Municipio") val municipio: Int,
    @SerializedName("CorreoElectronico") val correoElectronico: String?,
    @SerializedName("Direccion") val direccion: String,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("Celular") val celular: String?,
    @SerializedName("Tipo") val tipo: String?
)

fun RegisterCompanyBodyEntity.mapToData(tipo: String?) = RegisterCompanyDto(
    idTipoDocumento.orZero(),
    numeroDocumento.orEmpty(),
    digitoVerificacion.orZero(),
    razonSocial,
    nombreComercial,
    paginaWeb,
    idPais.orZero(),
    idDepartamento.orZero(),
    idMunicipio.orZero(),
    correoElectronico,
    direccion.orEmpty(),
    telefono,
    celular,
    tipo
)

data class RegisterCompanySedeDto(
    @SerializedName("idSede") val idSede: Int?,
    @SerializedName("telefonoSede") val telefonoSede: String?,
    @SerializedName("celularSede") val celularSede: String?,
    @SerializedName("correoElectronicoSede") val correoElectronicoSede: String?,
    @SerializedName("paisSede") val paisSede: Int?,
    @SerializedName("departamentoSede") val departamentoSede: Int?,
    @SerializedName("municipioSede") val municipioSede: Int?,
    @SerializedName("direccionSede") val direccionSede: String?,
)

fun RegisterCompanySedeBodyEntity.mapToData() = RegisterCompanySedeDto(
    idSede,
    telefonoSede,
    celularSede,
    correoElectronicoSede,
    paisSede,
    departamentoSede,
    municipioSede,
    direccionSede,
)

data class TipoProductoDto(
    @SerializedName("IdTipoProducto") val idTipoProducto: String,
)

