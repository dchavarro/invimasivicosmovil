package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemMsspListBinding
import com.soaint.sivicos_dinamico.utils.*
import com.xwray.groupie.databinding.BindableItem

class MsspListItemView(
    private val msspList: ManageMsspEntity,
    private val onSelected: (ManageMsspEntity, Boolean) -> Unit,
    private val isNso: Boolean = false,
    private val onChangeLote: (Int, String, Boolean) -> Unit,
) : BindableItem<ItemMsspListBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemMsspListBinding,
        position: Int
    ) = with(viewBinding) {

        textViewTitleNso.isVisible = isNso
        textViewNso.isVisible = isNso
        editTextLote.isVisible = isNso
        textViewTitleLote.isVisible = isNso

        when(msspList.tipoMs) {
            MSSP -> {
                textViewTitleNameProduct.text = root.context.getString(R.string.copy_sample_name_product)
                textViewTitleMssProduct.text = root.context.getString(R.string.copy_manage_mss_product_title)
                textViewNameProduct.text = msspList.nombreProducto
            }
            MSSE -> {
                textViewTitleNameProduct.text = root.context.getString(R.string.copy_manage_mss_title_type_activity)
                textViewTitleMssProduct.text = root.context.getString(R.string.copy_manage_mss_product_title_companny)
                textViewNameProduct.text = msspList.tiposActividades
            }
        }
        textViewNumberVisit.text = msspList.numeroVisita
        textViewNumberMss.text = msspList.numeroMss

        textViewMssProduct.text = msspList.tipoMedidaSanitaria
        textViewStatus.text = msspList.estado
        textViewDate.text = msspList.fechaAplicacion?.formatDate()

        textViewNso.text = msspList.notificacionSanitariaObligatoria
        editTextLote.setText(msspList.lote)

        editTextLote.doAfterTextChanged {
            onChangeLote.invoke(msspList.id.orEmpty(), it.toString(), checkBox.isChecked)
        }

        if(msspList.codigoMedidaSanitaria == ID_CODE_MSS_SUSPENSION || msspList.codigoMedidaSanitaria == ID_CODE_MSS_CANCELACION) {
            editTextLote.isEnabled = false
        }

        checkBox.isChecked = false

        root.setOnClickListener {
            checkBox.isChecked = !checkBox.isChecked
            onSelected.invoke(msspList, checkBox.isChecked)
        }
    }

    override fun getLayout() = R.layout.item_mssp_list
}