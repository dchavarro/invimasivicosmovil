package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.data.common.orZero
import com.soaint.domain.model.AntecedentEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemDenunciationBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class DenunciationItemView(
    private val antecedent: AntecedentEntity,
    private val onRadioSelected: (Boolean, AntecedentEntity) -> Unit,
    private val OnViewModelApplyReady: (AntecedentEntity) -> Unit,
    private val apply: (Int) -> String,
) : BindableItem<ItemDenunciationBinding>() {

    override fun bind(
        viewBinding: ItemDenunciationBinding,
        position: Int
    ) = with(viewBinding) {
        val context = root.context
        textViewTypeAntecedent.text = antecedent.numeroAsociado
        includeVerific.checked = if (antecedent.aplicaDenuncia == true) true else false
        var text = if (apply.invoke(antecedent.idDenuncia.orZero()).isNullOrEmpty())
            context.getString(R.string.copy_option) else apply.invoke(antecedent.idDenuncia.orZero())
        selectApply.text = text
        includeVerific.yes.setOnSingleClickListener { onRadioSelected.invoke(true, antecedent) }
        includeVerific.no.setOnSingleClickListener { onRadioSelected.invoke(false, antecedent) }
        selectApply.setOnSingleClickListener { OnViewModelApplyReady.invoke(antecedent) }
    }

    override fun getLayout() = R.layout.item_denunciation
}