package com.soaint.domain.use_case

import com.soaint.domain.model.PersonCompanyBodyEntity
import com.soaint.domain.model.RegisterCompanyBodyEntity
import com.soaint.domain.model.VisitCompanyPersonEntity
import com.soaint.domain.model.VisitEmpresaEntity
import com.soaint.domain.repository.RegisterCompanyRepository
import javax.inject.Inject

class RegisterCompanyUseCase @Inject constructor(
    private val repository: RegisterCompanyRepository
) {

    suspend fun loadPersonLocal(idVisita: Int)= repository.loadPersonLocal(idVisita)

    suspend fun insertPersonLocal(body: VisitCompanyPersonEntity, idVisita: Int) = repository.insertPersonLocal(body, idVisita)

    suspend fun deletePersonLocal(numeroDocumento: String, idVisita: Int)= repository.deletePersonLocal(numeroDocumento, idVisita)

    suspend fun loadCompanyLocal(idVisita: Int)= repository.loadCompanyLocal(idVisita)

    suspend fun insertOrUpdateCompanyLocal(body: RegisterCompanyBodyEntity, idVisita: Int) = repository.insertOrUpdateCompanyLocal(body, idVisita)

    suspend fun saveCompany(idVisita: Int)= repository.saveCompany(idVisita)

}