package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.content.DialogInterface
import android.text.Editable
import android.text.InputType
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.soaint.data.common.intOrString
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemViewListaBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface
import com.soaint.sivicos_dinamico.utils.IVC_VIG_FM002
import com.soaint.sivicos_dinamico.utils.ShowAlert
import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip
import kotlinx.coroutines.*

class ListaTabla : ControlInterface, CoroutineScope by MainScope() {

    lateinit var btnMore: ImageView
    lateinit var recyclerView: LinearLayout
    lateinit var classContext: Context
    private var position: Int = 0
    private val itemBinding = ArrayDeque<ItemViewListaBinding>()
    var array: List<String> = emptyList()
    var arrayTextInput = mutableMapOf<Int, ArrayList<TextInputLayout>>()
    var isVisible: Boolean = false

    private val map = mutableMapOf<Int, ArrayList<String>>()

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {
        classContext = context
        position = 0

        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_lista_tabla, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)
        val enable =
            isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            if (isVisible) showToolTip(imageViewHelp, control.configuracion?.mensajeAyuda.orEmpty())
        }

        lblText.text = control.descripcion
        btnMore = view.findViewById<ImageView>(R.id.btn_lista_tabla)
        recyclerView = view.findViewById<LinearLayout>(R.id.recyclerView)

        array = control.configuracion?.lista?.map { it.nombre.orEmpty() }.orEmpty()

        val newData =
            GsonBuilder().create().fromJson<ArrayList<ArrayList<String>>>(valorAlmacenado, object :
                TypeToken<ArrayList<ArrayList<String>>>() {}.type)

        if (newData != null) {
            newData.removeAt(0)
            val dataItems = newData
            dataItems.onEach { data ->
                if (data.size == array.size) {
                    recyclerView.addItemView(array, position, data, enable = enable)
                    position = position + 1
                }
            }
        }

        btnMore.setOnSingleClickListener {
            recyclerView.addItemView(
                array,
                position,
                controlNombre = control.nombre,
                action = action
            )
            position = position + 1
        }

        btnMore.isEnabled = enable
        recyclerView.isEnabled = enable

        return view
    }

    private fun LinearLayout.addItemView(
        hint: List<String>,
        position: Int,
        data: List<String>? = null,
        controlNombre: String? = null,
        enable: Boolean = true,
        action: (suspend () -> Any?)? = null
    ) {
        val binding = DataBindingUtil.inflate<ItemViewListaBinding>(
            LayoutInflater.from(context),
            R.layout.item_view_lista,
            this,
            true
        )

        map[position] = arrayListOf()
        arrayTextInput[position] = arrayListOf()

        hint.onEachIndexed { index, text ->
            val content = data?.get(index).orEmpty().trim()
            map[position]?.add(content)
            var job: Job? = null
            var changeEnable = enable
            if (controlNombre.equals("medidasSanitarias")){
                if ("Numero medida sanitaria" in text){
                    changeEnable = false
                }
            }
            val editText = binding.linearLayout.addEditText(text, content, index, changeEnable) {
                val text = it.toString()
                map[position]?.set(index, text)
                if (controlNombre == "requerimientos") {
                    job?.cancel()
                    val textInputType = arrayTextInput[position]?.get(index)
                    val textInputDescription = arrayTextInput[position]?.get(index + 1)
                    textInputType?.editText?.inputType =
                        InputType.TYPE_CLASS_NUMBER or InputType.TYPE_CLASS_PHONE
                    textInputDescription?.isEnabled = false
                    job = launch {
                        delay(500)
                        val result = action?.invoke() as? List<DinamicQuerysPapfEntity>?
                        val dataDescripcion =
                            result?.find { it.id == text.intOrString() }?.descripcion.orEmpty()
                        try {
                            if (dataDescripcion.isNullOrEmpty()) {
                                textInputType?.isErrorEnabled = true
                                textInputType?.error = "El tipo de requerimiento no existe"
                            } else {
                                textInputType?.isErrorEnabled = false
                                textInputType?.error = null
                            }
                            textInputDescription?.editText?.text = dataDescripcion.toEditable()
                            map[position]?.set(index + 1, dataDescripcion)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                }
            }
            arrayTextInput[position]?.add(editText)
        }

        binding.btnDelete.setOnClickListener {
            ShowAlert(context,
                context.getString(R.string.confirmar),
                context.getString(R.string.confirmar_borrar_item),
                context.getString(R.string.si),
                context.getString(R.string.no),
                object : ShowAlert.OnClickAlert {
                    override fun OnClickBotonIzq(dialog: DialogInterface) {
                        map[position]?.clear()
                        itemBinding.remove(binding)
                        recyclerView.removeView(binding.root)
                        dialog.dismiss()
                    }

                    override fun OnClickBotonDer(dialog: DialogInterface) {
                        dialog.dismiss()
                    }
                })
        }
        binding.btnDelete.isVisible = data.isNullOrEmpty()
        itemBinding.add(binding)
    }

    private fun LinearLayoutCompat.addEditText(
        data: String,
        content: String,
        index: Int,
        enable: Boolean,
        action: (text: Editable?) -> Unit
    ): TextInputLayout {
        val layoutParams = LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )

        val etInputLayout = TextInputLayout(
            context,
            null,
            R.style.Widget_MaterialComponents_TextInputLayout_OutlinedBox
        )
        etInputLayout.hint = data
        etInputLayout.isEnabled = enable
        etInputLayout.isExpandedHintEnabled = false
        etInputLayout.boxBackgroundMode = TextInputLayout.BOX_BACKGROUND_OUTLINE
        layoutParams.setMargins(0, 0, 0, 10)
        etInputLayout.layoutParams = layoutParams
        etInputLayout.setBoxCornerRadii(5f, 5f, 5f, 5f)

        val editText = TextInputEditText(etInputLayout.context)
        editText.layoutParams = layoutParams
        editText.tag = index.toString()
        editText.isEnabled = enable
        editText.setText(if (content != "null") content else EMPTY_STRING)
        editText.setPadding(20, 20, 20, 20)
        editText.doAfterTextChanged(action)
        etInputLayout.addView(editText)
        addView(etInputLayout)
        return etInputLayout
    }

    fun showToolTip(view: View, text: String) {
        SimpleTooltip.Builder(classContext)
            .anchorView(view)
            .text(text)
            .gravity(Gravity.START)
            .animated(true)
            .transparentOverlay(false)
            .build()
            .show()
    }

    override fun getData(): String {
        val data = mutableListOf<List<String>>()
        data.add(array)
        data.addAll(map.values.filter { it.isNotEmpty() })
        return Gson().toJson(data)
    }

    override fun showError(mensaje: String?) {
        btnMore.backgroundTintList = classContext.resources.getColorStateList(R.color.colorRed)
    }

    override fun clearError() {
        btnMore.backgroundTintList = classContext.resources.getColorStateList(R.color.colorPrimary)
    }
}