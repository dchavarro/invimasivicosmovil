package com.soaint.domain.common

import android.content.ActivityNotFoundException
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.OpenableColumns
import android.util.Base64
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.core.content.FileProvider
import com.soaint.domain.enum.ComparisonResult
import com.soaint.domain.model.EMPTY_STRING
import java.io.*
import java.net.Inet4Address
import java.net.NetworkInterface
import java.text.SimpleDateFormat
import java.util.*

inline fun <T1 : Any, T2 : Any, R : Any> safeLet(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}

inline fun <T1 : Any, T2 : Any, T3 : Any, R : Any> safeLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    block: (T1, T2, T3) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null) block(p1, p2, p3) else null
}

inline fun <T1 : Any, T2 : Any, T3 : Any, T4 : Any, R : Any> safeLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    p4: T4?,
    block: (T1, T2, T3, T4) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null && p4 != null) block(p1, p2, p3, p4) else null
}

inline fun <T1 : Any, T2 : Any, T3 : Any, T4 : Any, T5 : Any, R : Any> safeLet(
    p1: T1?,
    p2: T2?,
    p3: T3?,
    p4: T4?,
    p5: T5?,
    block: (T1, T2, T3, T4, T5) -> R?
): R? {
    return if (p1 != null && p2 != null && p3 != null && p4 != null && p5 != null) block(
        p1,
        p2,
        p3,
        p4,
        p5
    ) else null
}

// OBTENER IMAGE DESDE BASE64
fun getBase64ToBitMap(encodedString: String?) = try {
    val encodeByte = Base64.decode(encodedString, Base64.DEFAULT)
    BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.size)
} catch (e: Exception) {
    null
}

fun getFileFromBase64AndSave(
    context: Context,
    base64: String,
    filename: String,
    extension: String
): String? {
    var filePath: String? = null
    var newFileName = filename.replace("[^a-zA-Z0-9.-]".toRegex(), "_")
    try {
        val pdfAsBytes = Base64.decode(base64, 0)
        val os = FileOutputStream(getReportPath(context, newFileName, extension), false)
        os.write(pdfAsBytes)
        os.flush()
        os.close()
        filePath = getReportPath(context, newFileName, extension)
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return filePath
}

fun getReportPath(context: Context, filename: String, extension: String): String {
    val folder = File(context.getExternalFilesDir(null)!!.path)
    if (!folder.exists()) folder.mkdirs()
    return (folder.absolutePath + "/" + filename + "." + extension)
}

// OBTENER BASE64 DESDE PATH
fun getBase64FromPath(path: String?): String? {
    var base64: String? = EMPTY_STRING
    if (!path.isNullOrEmpty()) {
        try {
            val file = File(path)
            val buffer = ByteArray(file.length().toInt() + 100)
            val length = FileInputStream(file).read(buffer)
            base64 = Base64.encodeToString(buffer, 0, length, Base64.DEFAULT)
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }
        return base64.orEmpty()
            .replace(" ", "")
            .replace("\n", "")
            .replace("\r", "")
    } else {
        return base64
    }
}

// OBTENER PATH DE UN ARCHIVO
fun getFilePathFromUri(context: Context, uri: Uri): String =
    if (uri.path?.contains("file://") == true) uri.path!!
    else getFileFromContentUri(context, uri).path


private fun getFileFromContentUri(context: Context, contentUri: Uri): File {
    val fileName = getFileName(context, contentUri)
    val extension = getFileExtension(context, contentUri)
    val folder = File(context.getExternalFilesDir(null)!!.path)

    if (!folder.exists()) folder.mkdirs()
    val rutaTemp = folder.path + File.separator + fileName + "." + extension
    val tempFile = File(rutaTemp)

    var oStream: FileOutputStream? = null
    var inputStream: InputStream? = null
    try {
        oStream = FileOutputStream(tempFile)
        inputStream = context.contentResolver.openInputStream(contentUri)
        inputStream?.let { copy(inputStream, oStream) }
        oStream.flush()
    } catch (e: Exception) {
        e.printStackTrace()
    } finally {
        // Close streams
        inputStream?.close()
        oStream?.close()
    }
    return tempFile
}

// CREATE COPIA DE ARCHIVO
@Throws(IOException::class)
private fun copy(source: InputStream, target: OutputStream) {
    val buf = ByteArray(8192)
    var length: Int
    while (source.read(buf).also { length = it } > 0) {
        target.write(buf, 0, length)
    }
}

// Obtener Extension de un archivo
fun getFileExtension(context: Context, uri: Uri): String? =
    if (uri.scheme == ContentResolver.SCHEME_CONTENT)
        MimeTypeMap.getSingleton().getExtensionFromMimeType(context.contentResolver.getType(uri))
    else uri.path?.let { MimeTypeMap.getFileExtensionFromUrl(Uri.fromFile(File(it)).toString()) }

// Obtener nombre de un archivo
fun getFileName(context: Context, uri: Uri?): String? {
    val fileCursor: Cursor? = context.getContentResolver()
        .query(uri!!, arrayOf(OpenableColumns.DISPLAY_NAME), null, null, null)
    var fileName: String? = null
    if (fileCursor != null && fileCursor.moveToFirst()) {
        val cIndex: Int = fileCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME)
        if (cIndex != -1) {
            fileName = fileCursor.getString(cIndex).split(".")[0]
        }
    }
    return fileName
}

// Obtener peso de un archivo
fun getFileSize(context: Context, uri: Uri?): Int {
    val fileCursor: Cursor? = context.getContentResolver()
        .query(uri!!, arrayOf(OpenableColumns.SIZE), null, null, null)
    var fileSize: Int = 0
    if (fileCursor != null && fileCursor.moveToFirst()) {
        val cIndex: Int = fileCursor.getColumnIndex(OpenableColumns.SIZE)
        if (cIndex != -1) {
            fileSize = fileCursor.getInt(cIndex) / 1000
        }
    }
    return fileSize
}

// Valida si es menor a 5MB
fun isFileLessThan5MB(size: Int): Boolean {
    val maxFileSize = 5 * 1024 * 1024
    return size >= maxFileSize
}

// OBTENER FECHA Y HORA ACTUAL
fun getDateHourNow(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString().formatDateHour()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString().formatDateHour()
}

// OBTENER FECHA
fun getDateNow(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString().formatDate()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString().formatDate()
}

// OBTENER FECHA Y HORA ACTUAL sin formatear
fun getDateHourWithOutFormat(): String {
    val sdf = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.getDefault())
    val currentDate = sdf.format(Date())
    return currentDate.toString()
    //return LocalDateTime.ofInstant(Instant.now(), ZoneOffset.systemDefault()).toString()
}

// ABRIR UN ARCHIVO DESDE EL PATH
fun openFile(context: Context, path: String, applicationId: String) {
    val file = File(path)
    val uri =
        FileProvider.getUriForFile(
            context,
            applicationId + ".provider",
            file
        )
    try {
        val intent = Intent(Intent.ACTION_VIEW)
        if (uri.toString().contains(".doc") || uri.toString().contains(".docx")) {
            // Word document
            intent.setDataAndType(uri, "application/msword")
        } else if (uri.toString().contains(".pdf")) {
            // PDF file
            intent.setDataAndType(uri, "application/pdf")
        } else if (uri.toString().contains(".jpg") || uri.toString()
                .contains(".jpeg") || uri.toString().contains(".png")
        ) {
            // JPG file
            intent.setDataAndType(uri, "image/jpeg")
        } else if (uri.toString().contains(".ppt") || uri.toString().contains(".pptx")) {
            // Powerpoint file
            intent.setDataAndType(uri, "application/vnd.ms-powerpoint")
        } else if (uri.toString().contains(".xls") || uri.toString().contains(".xlsx")) {
            // Excel file
            intent.setDataAndType(uri, "application/vnd.ms-excel")
        } else if (uri.toString().contains(".zip")) {
            // ZIP file
            intent.setDataAndType(uri, "application/zip")
        } else if (uri.toString().contains(".rar")) {
            // RAR file
            intent.setDataAndType(uri, "application/x-rar-compressed")
        } else if (uri.toString().contains(".rtf")) {
            // RTF file
            intent.setDataAndType(uri, "application/rtf")
        } else if (uri.toString().contains(".wav") || uri.toString().contains(".mp3")) {
            // WAV audio file
            intent.setDataAndType(uri, "audio/x-wav")
        } else if (uri.toString().contains(".gif")) {
            // GIF file
            intent.setDataAndType(uri, "image/gif")
        } else if (uri.toString().contains(".txt")) {
            // Text file
            intent.setDataAndType(uri, "text/plain")
        } else if (uri.toString().contains(".3gp") || uri.toString().contains(".mpg") ||
            uri.toString().contains(".mpeg") || uri.toString().contains(".mpe") ||
            uri.toString().contains(".mp4") || uri.toString().contains(".avi")
        ) {
            // Video files
            intent.setDataAndType(uri, "video/*")
        } else {
            intent.setDataAndType(uri, "*/*")
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        context.startActivity(intent)
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(context, "No application found which can open the file", Toast.LENGTH_SHORT)
            .show()
    }
}

// Eliminar un Archivo
fun deleteFileByPath(path: String) {
    val file = File(path.orEmpty())
    if (file.exists()) {
        file.delete()
    }
}

// OBTENER IP LOCAL
fun getIp(): String = try {
    NetworkInterface.getNetworkInterfaces()?.toList()?.mapNotNull { networkInterface ->
        networkInterface.inetAddresses?.toList()
            ?.find { !it.isLoopbackAddress && it is Inet4Address }
            ?.hostAddress
    }?.firstOrNull().orEmpty()
} catch (e: Exception) {
    EMPTY_STRING
}
fun converterDate(fecha: String): String {
    val formatoEntrada = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
    val formatoSalida = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault())

    val date = formatoEntrada.parse(fecha)
    return formatoSalida.format(date)
}
fun converterHourStringToDate(hour: String): Date? {
    val formatoHora = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
    return formatoHora.parse(hour)
}

fun hourFormatter(hour: String): String {
    val formatoEntrada = SimpleDateFormat("HH:mm", Locale.getDefault())
    val formatoSalida = SimpleDateFormat("HH:mm:ss", Locale.getDefault())

    val hora = formatoEntrada.parse(hour)
    return formatoSalida.format(hora!!)
}

fun isWeekendDay(fecha: String): Boolean {
    val formatoFecha = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.getDefault())
    val fechaDate = formatoFecha.parse(fecha)

    val formatoDiaSemana = SimpleDateFormat("EEEE", Locale("es", "CO"))
    val diaSemana = formatoDiaSemana.format(fechaDate)

    return diaSemana in listOf("sábado", "domingo")
}


fun comparisonHours(hora1: String, hora2: String): ComparisonResult {
    val dateHora1 = converterHourStringToDate(hora1)
    val dateHora2 = converterHourStringToDate(hora2)

    return when {
        dateHora1 != null && dateHora2 != null -> {
            when (dateHora1.compareTo(dateHora2)) {
                -1 -> ComparisonResult.MENOR
                0 -> ComparisonResult.IGUAL
                1 -> ComparisonResult.MAYOR
                else -> ComparisonResult.ERROR
            }
        }
        else -> ComparisonResult.ERROR
    }
}

fun calculateHourDifference(time1: String, time2: String): Int {
    val date1 = converterHourStringToDate(time1)
    val date2 = converterHourStringToDate(time2)
    return (((date2?.time ?: 0) - (date1?.time ?: 0)) / (1000 * 60)).toInt()
}

fun isRangeGreaterThan24Hours(date1: String, time1: String, date2: String, time2: String): Boolean {
    val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss",Locale.getDefault())
    val dateTime1 = format.parse(date1.substringBefore("T") + "T" + time1)
    val dateTime2 = format.parse(date2.substringBefore("T") + "T" + time2)


    val differenceMillis = dateTime2.time - dateTime1.time

    val hoursInMillis = 24 * 60 * 60 * 1000 // 24 hours in milliseconds

    return differenceMillis > hoursInMillis

}