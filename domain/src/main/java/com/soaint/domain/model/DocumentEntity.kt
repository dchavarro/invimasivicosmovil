package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

val PUBLIC_ACCESS_CLASSIFICATION = "publicable"
val STATE_DOC_EJE = "ejecución"
val MACROPROCESS = "Inspección Vigilancia y Control Sanitario"
val PROCESS = "Proceso de inspección, Proceso de vigilancia, Proceso Control Sanitario"
val MISSIONARY_DOCUMENTARY = "Misional"
val SIVICOS = "SIVICOS"

@Parcelize
data class DocumentEntity(
    val extension: String? = EMPTY_STRING,
    val descripcionTipoDocumental: String? = EMPTY_STRING,
    val urlSesuite: String? = EMPTY_STRING,
    val descripcionClasificacionDocumental: String? = EMPTY_STRING,
    val detalle: String? = EMPTY_STRING,
    val urlSharedPoint: String? = EMPTY_STRING,
    val descripcionDocumento: String? = EMPTY_STRING,
    val usuarioCrea: String? = EMPTY_STRING,
    val clasificacionDocumental: Int? = 0,
    val idDocumento: Int? = 0,
    val nombreDocumento: String? = EMPTY_STRING,
    val consecutivoSesuite: String? = EMPTY_STRING,
    val fechaDocumento: String? = EMPTY_STRING,
    val nombreTipoDocumental: String? = EMPTY_STRING,
    val idTipoDocumental: Int? = 0,
    val numeroFolios: String? = EMPTY_STRING,
    val idPlantilla: Int? = 0,
    val codigoPlantilla: String? = EMPTY_STRING,
    val indDocumentoFirmado: Boolean? = false,
    val idDocumentBody: Int? = null,
    val id: Int? = null,
) : Parcelable

@Parcelize
data class DocumentNameEntity(
    val idTipoDocumental: Int? = 0,
    val nombreTipoDocumental: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class DocumentBodyEntity(
    val idTipoDocumental: Int = 0,
    val idDocumento: String? = null,
    val nombreDocumento: String? = EMPTY_STRING,
    val descripcionDocumento: String? = EMPTY_STRING,
    val extension: String? = EMPTY_STRING,
    val archivoBase64: String? = EMPTY_STRING,
    val idSolicitud: String? = EMPTY_STRING,
    val idTramite: String? = EMPTY_STRING,
    val idExpediente: String? = EMPTY_STRING,
    val numeroRadicado: String? = null,
    val numeroActaSala: String? = null,
    val idSala: String? = EMPTY_STRING,
    val idVisita: String? = null,
    val numeroAgenda: String? = null,
    val idEmpresa: String? = null,
    val numeroFolios: Int? = 0,
    val usuarioCrea: String? = null,
    val idClasificacionDocumental: Int? = 0,
    val metadata: MetadataEntity? = null,
) : Parcelable

@Parcelize
data class MetadataEntity(
    val tipologiaDocumental: String? = EMPTY_STRING,
    val tituloDocumento: String? = EMPTY_STRING,
    val autor: String? = EMPTY_STRING,
    val autorFirmante: String? = EMPTY_STRING,
    val clasificacionAcceso: String? = PUBLIC_ACCESS_CLASSIFICATION,
    val folio_gd: String? = EMPTY_STRING,
    val formato: String? = EMPTY_STRING,
    val estadoDoc: String? = STATE_DOC_EJE,
    val macroProceso: String? = MACROPROCESS,
    val proceso: String? = PROCESS,
    val documental: String? = MISSIONARY_DOCUMENTARY,
    val tecnologico: String? = SIVICOS,
    val nroVisita: String? = EMPTY_STRING,
    val nombreEmpresa: String? = EMPTY_STRING,
    val nitEmpresa: String? = EMPTY_STRING,
    val fechaCreacionDocumento: String? = EMPTY_STRING,
    val idDocumentBody: Int? = 0,
) : Parcelable

