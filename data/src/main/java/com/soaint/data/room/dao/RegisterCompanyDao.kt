package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.CompanyPersonModel

@Dao
interface RegisterCompanyDao {

    //Gestion tablas personas vinculadas
    @Query("SELECT * FROM company_person_table WHERE idVisita = :idVisita ORDER BY `index` DESC")
    suspend fun selectCompanyPerson(idVisita: Int): List<CompanyPersonModel>

    @Insert(entity = CompanyPersonModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCompanyPerson(model: CompanyPersonModel)

    @Query("DELETE FROM company_person_table WHERE numeroDocumentoPersona = :numeroDocumento AND idVisita = :idVisita")
    suspend fun deletePersonById(numeroDocumento: String, idVisita: Int)

    @Query("DELETE FROM company_person_table WHERE idVisita = :idVisita")
    suspend fun deletePersonByVisit(idVisita: Int)
}