package com.soaint.domain.repository

import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.BlockSaveBodyEntity
import com.soaint.domain.model.BlockValueEntity
import com.soaint.domain.model.VisitEntity

interface ActRepository {

    suspend fun saveBlock(
        idPlantilla: String,
        idVisita: String,
        body: BlockSaveBodyEntity
    ): Result<BlockSaveBodyEntity>

    suspend fun getPdf(
        idPlantilla: String,
        visita: VisitEntity,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String>

    suspend fun getBlockSync(
        idPlantilla: String,
        idVisita: String,
    ): Result<BlockSaveBodyEntity>

    suspend fun retry(
        idVisita: String,
        idPlantilla: String,
    ): Result<String>

    suspend fun getBlockValue(
        idPlantilla: String,
        idVisita: String,
    ): Result<List<BlockEntity>>

    suspend fun updateBlockValue(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String,
        value: Any?,
        idTipoAtributo: Int?,
    )

    suspend fun getBlockValueLocal(
        idPlantilla: String,
        idVisita: String,
        idBlock: String,
        idBlockAtribute: String,
    ): Result<BlockValueEntity?>

    // PAPF
    suspend fun getPdfPapf(
        idPlantilla: String,
        idSolicitud: Int,
        indVistaPrevia: Boolean,
        body: Map<String, Any?>,
    ): Result<String>

    suspend fun getBlockValuePapf(
        idPlantilla: String,
        idSolicitud: String,
    ): Result<List<BlockEntity>>

    suspend fun getNameAtribute(
        idBlock: String,
        idBlockAtribute: String
    ): Result<BlockAtributePlantillaEntity>
}