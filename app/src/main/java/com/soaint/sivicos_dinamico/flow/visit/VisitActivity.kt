package com.soaint.sivicos_dinamico.flow.visit

import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.databinding.ActivityVisitBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.stopLocationService
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceActivity
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.splash.SplashActivity
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import com.soaint.sivicos_dinamico.properties.activityBinding
import com.soaint.sivicos_dinamico.utils.REQUEST_LOAD_TASK

const val KEY_ID = "KEY_ID"

class VisitActivity : BaseActivity(), HideShowIconInterface {

    companion object {
        fun getStartActivityEvent(
            id: Int,
            code: Int = REQUEST_LOAD_TASK
        ) = StartActivityEvent(
            VisitActivity::class.java,
            Bundle().apply {
                putInt(KEY_ID, id)
            },
            code = code
        )
    }

    private val viewModel by viewModels<VisitViewModel> { viewModelFactory }

    private val binding by activityBinding<ActivityVisitBinding>(R.layout.activity_visit)

    val actionBarDrawerToggle by lazy {
        object : ActionBarDrawerToggle(
            this,
            binding.drawerLayout,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        ) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setSupportActionBar(binding.toolbar)
        initView()
        observerViewModelEvents()
        binding.root
    }

    private fun initView() {
        binding.drawerLayout.addDrawerListener(actionBarDrawerToggle)
        actionBarDrawerToggle.drawerArrowDrawable.color = resources.getColor(R.color.white)
        actionBarDrawerToggle.syncState()
        binding.fragmentMenu.buttonInfo.setOnSingleClickListener { openInfo() }
        binding.fragmentMenu.buttonHistoric.setOnSingleClickListener { openHistoric() }
        binding.fragmentMenu.buttonHistoricMss.setOnSingleClickListener { openHistoricMss() }
        binding.fragmentMenu.buttonActa.setOnSingleClickListener { openAct() }
        binding.fragmentMenu.buttonDocuments.setOnSingleClickListener { openDocument() }
        binding.fragmentMenu.buttonNotification.setOnSingleClickListener { openNotification() }
        binding.fragmentMenu.buttonTransport.setOnSingleClickListener { openTransport() }
        binding.fragmentMenu.buttonMss.setOnSingleClickListener { openMss() }
        binding.fragmentMenu.buttonClose.setOnSingleClickListener { openClose() }
        binding.fragmentMenu.buttonSync.setOnSingleClickListener { sync() }
        binding.fragmentMenu.buttonLogout.setOnSingleClickListener { logout() }
        binding.fragmentMenu.textViewVersion.text =
            getString(R.string.copy_version, BuildConfig.VERSION_NAME)
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(viewModel, binding.root)
        viewModel.event.observe(this, ::eventHandler)
        viewModel.isVisibleTransport.observe(this, ::menuVisible)
        viewModel.init(intent.getIntExtra(KEY_ID, 0))
    }

    private fun eventHandler(event: OnVisitViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnVisitViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnVisitViewModelEvent.OnLoadingError -> onTokenError()
            is OnVisitViewModelEvent.OnViewModelReady -> onViewModelReady(event.visits)
            else -> {}
        }
    }

    private fun menuVisible(menu: Boolean) {
        binding.fragmentMenu.buttonTransport.isVisible = !menu
    }

    private fun onTokenError() {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { finish() }
        )
    }

    private fun onViewModelReady(visit: List<VisitEntity>) {}

    private fun openMenu() {
        viewModel.onGetAmsspSelect()
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }

    private fun closeMenu() {
        binding.drawerLayout.closeDrawers()
    }

    private fun openInfo() {
        findNavController(binding.navHost.id).navigate(R.id.informacionVisita)
        closeMenu()
    }

    private fun openHistoric() {
        findNavController(binding.navHost.id).navigate(R.id.historicoVisita)
        closeMenu()
    }

    private fun openHistoricMss() {
        findNavController(binding.navHost.id).navigate(R.id.historicoMss)
        closeMenu()
    }

    private fun openAct() {
        startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
        closeMenu()
    }

    private fun openDocument() {
        findNavController(binding.navHost.id).navigate(R.id.documentosVisita)
        closeMenu()
    }

    private fun openNotification() {
        findNavController(binding.navHost.id).navigate(R.id.notificarVisita)
        closeMenu()
    }

    private fun openTransport() {
        findNavController(binding.navHost.id).navigate(R.id.transporteVisita)
        closeMenu()
    }

    private fun openMss() {
        findNavController(binding.navHost.id).navigate(R.id.gestionarMss)
        closeMenu()
    }

    private fun openClose() {
        findNavController(binding.navHost.id).navigate(R.id.cierre)
        closeMenu()
    }

    private fun logout() {
        showAlertDialog(
            showDialog(
                getString(R.string.cerrar_sesion),
                getString(R.string.copy_logout),
                getString(R.string.si),
                getString(R.string.no),
            ) {
                viewModel.logout()
                stopLocationService()
                startClearStackActivity(this@VisitActivity, SplashActivity::class.java)
                finish()
            }
        )
    }

    private fun sync() {
        startClearStackActivity(this@VisitActivity, MainActivity::class.java)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        invalidateOptionsMenu()
        return when (item.getItemId()) {
            android.R.id.home -> {
                if (actionBarDrawerToggle.isDrawerIndicatorEnabled()) {
                    openMenu()
                } else {
                    onBackPressed()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
                getSupportFragmentManager().popBackStackImmediate();
            } else {
                super.onBackPressed();
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun showHamburgerIcon() {
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBarDrawerToggle.setDrawerIndicatorEnabled(true)
        supportActionBar!!.title = getString(R.string.app_name)
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    override fun showBackIcon(title: String?) {
        actionBarDrawerToggle.setDrawerIndicatorEnabled(false)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.title =
            if (title.isNullOrEmpty()) getString(R.string.app_name) else title
        binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }
}