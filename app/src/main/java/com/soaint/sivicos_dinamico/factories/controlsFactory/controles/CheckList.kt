package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.core.view.allViews
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface

class CheckList : ControlInterface {

    lateinit var contenedor: LinearLayout
    lateinit var cardView: CardView
    lateinit var classContext: Context
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        var inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_check_list, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        lblText.text = control.descripcion
        contenedor = view.findViewById<LinearLayout>(R.id.contenedorCheckList)
        cardView = view.findViewById<CardView>(R.id.cardView)

        val string = valorAlmacenado.replace("[", "").replace("]", "")
        val arrayAlmacenado: List<String> = listOf(*string.split(",").toTypedArray()).toList()

        for (opcion in control.configuracion?.lista.orEmpty()) {
            val check = CheckBox(context)
            val lp = RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
            )
            check.layoutParams = lp
            check.text = opcion.nombre

            if (arrayAlmacenado.contains(opcion.nombre) || opcion.seleccionado == true ) {
                check.isChecked = true
            }
            contenedor.addView(check)
        }
        cardView.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)
        return view
    }

    override fun getData(): String {
        val array: ArrayList<String> = arrayListOf()
        for (check in contenedor.allViews) {
            if (check is CheckBox && check.isChecked) {
                array.add(check.text.toString())
            }
        }
        return array.toString()
    }

    override fun showError(mensaje: String?) {
        cardView.setBackgroundResource(R.drawable.border_red)
    }

    override fun clearError() {
        cardView.setBackgroundResource(R.drawable.border_white)
    }
}