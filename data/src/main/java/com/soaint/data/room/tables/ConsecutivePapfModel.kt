package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "consecutive_papf_table")
class ConsecutivePapfModel(
    @ColumnInfo(name = "consecutivo") val consecutivo: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)
