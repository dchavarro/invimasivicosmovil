package com.soaint.sivicos_dinamico.utils;

import static com.soaint.data.common.ConstantsKt.PREFIX_FIRM;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;

import com.soaint.sivicos_dinamico.R;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Objects;

/**
 * Created by CESAR on 31/05/2016.
 */
public class FirmaView extends View {
    // set the stroke width
    private static final float STROKE_WIDTH = 5f;
    private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;

    private Paint paint = new Paint();
    private Path path = new Path();
    private Canvas canvas = new Canvas();
    private Context context;

    private float lastTouchX;
    private float lastTouchY;
    private final RectF dirtyRect = new RectF();
    private String ruta = "";

    public FirmaView(Context _context) {
        super(_context);
        context = _context;
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeJoin(Paint.Join.ROUND);
        paint.setStrokeWidth(STROKE_WIDTH);

        // set the bg color as white
        this.setBackgroundColor(ContextCompat.getColor(context, R.color.colorWhite));
        // width and height should cover the screen
        this.setLayoutParams(new ViewGroup.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 500));
    }

    /**
     * Get signature
     *
     * @return
     */
    public String getSignature() {
        Bitmap signatureBitmap = null;

        // set the signature bitmap
        signatureBitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.RGB_565);

        // important for saving signature
        final Canvas canvas = new Canvas(signatureBitmap);
        this.draw(canvas);

        //GUARDO LA IMAGEN ESCALADA EN EL ARCHIVO INICIAL
        ruta = Objects.requireNonNull(new KotlinUtils().getOutputMediaFile(context, PREFIX_FIRM)).ruta;

        try {
            FileOutputStream fos = null;
            fos = new FileOutputStream(ruta);
            signatureBitmap.compress(Bitmap.CompressFormat.JPEG, 70, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ruta;
    }

    /**
     * clear signature canvas
     */
    public void clearSignature() {
        path.reset();
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        this.invalidate();
    }

    // all touch events during the drawing
    @Override
    protected void onDraw(Canvas canvas) {
        this.canvas = canvas;
        canvas.drawPath(this.path, this.paint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        float eventX = event.getX();
        float eventY = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                path.moveTo(eventX, eventY);

                lastTouchX = eventX;
                lastTouchY = eventY;
                return true;

            case MotionEvent.ACTION_MOVE:

            case MotionEvent.ACTION_UP:

                resetDirtyRect(eventX, eventY);
                int historySize = event.getHistorySize();
                for (int i = 0; i < historySize; i++) {
                    float historicalX = event.getHistoricalX(i);
                    float historicalY = event.getHistoricalY(i);

                    expandDirtyRect(historicalX, historicalY);
                    path.lineTo(historicalX, historicalY);
                }
                path.lineTo(eventX, eventY);
                break;

            default:

                return false;
        }

        invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
                (int) (dirtyRect.top - HALF_STROKE_WIDTH),
                (int) (dirtyRect.right + HALF_STROKE_WIDTH),
                (int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

        lastTouchX = eventX;
        lastTouchY = eventY;

        return true;
    }

    private void expandDirtyRect(float historicalX, float historicalY) {
        if (historicalX < dirtyRect.left) {
            dirtyRect.left = historicalX;
        } else if (historicalX > dirtyRect.right) {
            dirtyRect.right = historicalX;
        }

        if (historicalY < dirtyRect.top) {
            dirtyRect.top = historicalY;
        } else if (historicalY > dirtyRect.bottom) {
            dirtyRect.bottom = historicalY;
        }
    }

    private void resetDirtyRect(float eventX, float eventY) {
        dirtyRect.left = Math.min(lastTouchX, eventX);
        dirtyRect.right = Math.max(lastTouchX, eventX);
        dirtyRect.top = Math.min(lastTouchY, eventY);
        dirtyRect.bottom = Math.max(lastTouchY, eventY);
    }
}
