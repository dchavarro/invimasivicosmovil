package com.soaint.data.repository

import com.soaint.data.api.InperApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.EquipmentDto
import com.soaint.data.model.mapToData
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.EquipmentDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.updateFields
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.domain.model.EquipmentEntity
import com.soaint.domain.model.EquipmentUpdateEntity
import com.soaint.domain.repository.EquipmentRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class EquipmentRepositoryImpl @Inject constructor(
    private val inperApi: InperApi,
    private val equipmentDao: EquipmentDao,
    private val userRepository: UserRepository,
) : EquipmentRepository, BaseRepository() {

    private val _equipment = MutableStateFlow<EquipmentEntity?>(null)
    override val equipment get() = _equipment.asStateFlow()

    //Carga de equipos por sede
    override suspend fun loadEquipment(idSede: Int): Result<List<EquipmentEntity>> = execute {
        equipmentDao.deleteEquipmentBySede(idSede)
        val equipment = inperApi.getEquipos(idSede).objectResponse
        saveDao(equipment)
        Result.success(equipment.mapNotNull { it.mapToDomain() })
    }

    //Carga de equipos por sede DB local
    override suspend fun loadEquipmentLocal(idSede: Int): Result<List<EquipmentEntity>> =
        Result.success(
            equipmentDao.selectEquipment(idSede).map { it.mapToDomain() }
        )

    //Guardar equipos en DB local
    private suspend fun saveDao(data: List<EquipmentDto>) {
        data.onEach { equipment ->
            val model = equipment.mapToDb()
            equipmentDao.insertEquipment(model)
        }.orEmpty()
    }

    // Guardar Servicio equipos
    override suspend fun addEquipment(): Result<List<Unit>> = execute {
        val query = equipmentDao.selectEquipmentByLocal()
        val result = query?.mapNotNull {
            it.id = null
            inperApi.insertEquipos(it.mapToDomain().mapToData())
        }.orEmpty()
        Result.success(result)
    }

    //Guardar equipos Local
    override suspend fun addEquipmentLocal(body: EquipmentEntity): Result<Unit> {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val newBody = body.copy(
            usuarioCrea = userName, fechaCreacion = getDateHourWithOutFormat(),
            usuarioModifica = userName, fechaModifica = getDateHourWithOutFormat()
        )
        val result = equipmentDao.insertOrUpdateEquipment(newBody.mapToData().mapToDb())
        return Result.success(result)
    }

    //Update equipos en DB local
    override suspend fun updateEquipmentLocal(body: EquipmentEntity): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val newBody =
            body.copy(usuarioModifica = userName, fechaModifica = getDateHourWithOutFormat())
        val result = equipmentDao.insertOrUpdateEquipment(newBody.mapToData().mapToDb())
        Result.success(result)
    }

    override suspend fun updateEquipment(): Result<List<Unit>> = execute {
        val query = equipmentDao.selectEquipmentUpdate()
        val result = query?.mapNotNull {
            inperApi.updateEquipos(listOf(it.mapToDomain().mapToData()))
        }.orEmpty()
        Result.success(result)
    }

    //Update equipos en DB local
    override suspend fun deleteEquipmentLocal(body: EquipmentEntity): Result<Unit> = execute {
        val result = equipmentDao.deleteEquipmentById(body.id.orZero())
        Result.success(result)
    }

    override suspend fun selectEquipmentUnfinished()
            : Result<List<EquipmentEntity>> = Result.success(
        equipmentDao.selectEquipmentUnfinished()
            .map { it.mapToDomain() }
    )

    override suspend fun updateEquipmentBatch(body: List<EquipmentUpdateEntity>): Result<Unit> =
        execute {
            try {
                for (equipmentUpdate in body) {
                    equipmentDao.selectEquipmentById(equipmentUpdate.id ?: 0)?.let { equipment ->
                        val updatedModel = equipment.updateFields(
                            id = equipmentUpdate.id,
                            idResultado = equipmentUpdate.idResultado,
                            codigoResultado = equipmentUpdate.codigoResultado,
                            descripcionResultado = equipmentUpdate.descripcionResultado,
                            observacion = equipmentUpdate.observacion,
                            isUpdate = equipmentUpdate.isLocal?.let { !it } ?: equipmentUpdate.isUpdate!!
                        )
                        equipmentDao.updateEquipment(updatedModel)
                    } ?: throw Exception("Equipo no encontrado")
                }
                Result.success(Unit)
            } catch (e: Exception) {
                Result.failure(e)
            }
        }
}