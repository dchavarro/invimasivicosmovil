package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.InfoTramitPapfModel
import com.soaint.data.room.tables.InvoicePapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.InfoTramitePapfSolicitudEntity
import com.soaint.domain.model.InvoicePapfEntity

data class InvoicePapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<InvoicePapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class InvoicePapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int?,
    @SerializedName("numeroFactura") val numeroFactura: String?,
    @SerializedName("fechaExpedicion") val fechaExpedicion: String?,
    @SerializedName("IdDocumento") val idDocumento: Int?,
    @SerializedName("numeroDocumento") val numeroDocumento: String?,
    @SerializedName("nombreEmpresa") val nombreEmpresa: String?,
    @SerializedName("IdPais") val idPais: Int?,
    @SerializedName("pais") val pais: String?,
)

fun InvoicePapfDto.mapToDomain() = InvoicePapfEntity(
    idSolicitud,
    numeroFactura,
    fechaExpedicion,
    idDocumento,
    numeroDocumento,
    nombreEmpresa,
    idPais,
    pais,
)

fun InvoicePapfDto.mapToDb() = InvoicePapfModel(
    idSolicitud,
    numeroFactura,
    fechaExpedicion,
    idDocumento,
    numeroDocumento,
    nombreEmpresa,
    idPais,
    pais,
)