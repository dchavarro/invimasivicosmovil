package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.TaskPapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.TaskPapfEntity

data class TaskPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosPapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class TaskPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: List<TaskPapfDto>?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class TaskPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("NumeroRadicado") val numeroRadicado: String?,
    @SerializedName("IdTipoInspeccion") val idTipoInspeccion: String?,
    @SerializedName("DescripcionTipoInspeccion") val descripcionTipoInspeccion: String?,
    @SerializedName("Fecha") val fecha: String?,
    @SerializedName("FechaPosibleInspeccion") val fechaPosibleInspeccion: String?,
    @SerializedName("PAPF") val papf: String?,
    @SerializedName("NombreLugarAlmacenamiento") val nombreLugarAlmacenamiento: String?,
    @SerializedName("NombreSitioInspeccion") val nombreSitioInspeccion: String?,
    @SerializedName("Lote") val lote: Int?,
    @SerializedName("IdProgramacion") val idProgramacion: Int?,
    @SerializedName("DescActividad") val descActividad: String?,
    @SerializedName("CertificadoExportacion") val certificadoExportacion: Boolean?,
    @SerializedName("Estado") val estado: String?,
    @SerializedName("FechaAsignacion") val fechaAsignacion: String?,
    @SerializedName("UsuarioAsignado") val usuarioAsignado: String?,
    @SerializedName("Rol") val rol: String?,
    @SerializedName("UsuarioRed") val usuarioRed: String?,
)

fun TaskPapfDto.mapToDomain() = TaskPapfEntity (
    idSolicitud,
    numeroRadicado,
    idTipoInspeccion,
    descripcionTipoInspeccion,
    fecha,
    fechaPosibleInspeccion,
    papf,
    nombreLugarAlmacenamiento,
    nombreSitioInspeccion,
    lote,
    idProgramacion,
    descActividad,
    certificadoExportacion,
    estado,
    fechaAsignacion,
    usuarioAsignado,
    rol,
    usuarioRed,
)

fun TaskPapfDto.mapToDb() = TaskPapfModel(
    idSolicitud.orEmpty(),
    numeroRadicado,
    idTipoInspeccion,
    descripcionTipoInspeccion,
    fecha,
    fechaPosibleInspeccion,
    papf,
    nombreLugarAlmacenamiento,
    nombreSitioInspeccion,
    lote,
    idProgramacion,
    descActividad,
    certificadoExportacion,
    estado,
    fechaAsignacion,
    usuarioAsignado,
    rol,
    usuarioRed,
)

fun TaskPapfEntity.mapToData() = TaskPapfDto(
    idSolicitud,
    numeroRadicado,
    idTipoInspeccion,
    descripcionTipoInspeccion,
    fecha,
    fechaPosibleInspeccion,
    papf,
    nombreLugarAlmacenamiento,
    nombreSitioInspeccion,
    lote,
    idProgramacion,
    descActividad,
    certificadoExportacion,
    estado,
    fechaAsignacion,
    usuarioAsignado,
    rol,
    usuarioRed,
)