package com.soaint.sivicos_dinamico.flow.papf.views.documentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.DocumentationPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentDocumentationPapfBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.item_view.DocumentsComplementItemView
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.item_view.TextItemView
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class DocumentationFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentDocumentationPapfBinding>(R.layout.fragment_documentation_papf)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: DocumentationViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.documents_papf))
        binding.rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(DocumentationFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.idRequest.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.documents)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(showDialog(
            getString(R.string.error),
            getString(R.string.copy_generic_error),
            titleBtnPositive = getString(R.string.cerrar)
        ) { dismissAlertDialog() })
    }

    private fun onViewModelReady(documents: List<DocumentationPapfEntity>) {
        adapter.clear()
        val items = mutableListOf<Group>()
        if (!documents.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_documentation_complement)))
            items.add(
                TextItemView(
                    getString(R.string.copy_documentation_complement_subtitle), R.color.colorGreen
                )
            )
            items.addAll(documents.map { DocumentsComplementItemView(it) })
        }

        val isVisible = documents.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }
}