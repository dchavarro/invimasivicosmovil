package com.soaint.sivicos_dinamico.flow.main.item_view

import com.soaint.domain.common.formatDate
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.TaskPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTaskPapfBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class TaskPapfItemView(
    private val tarea: TaskPapfEntity,
    private val onTareaSelected: (TaskPapfEntity) -> Unit
) : BindableItem<ItemTaskPapfBinding>() {

    override fun bind(
        viewBinding: ItemTaskPapfBinding,
        position: Int
    ) = with(viewBinding) {
        val context = root.context
        txtRegisteredNumber.text = tarea.numeroRadicado
        txtInspectionType.text = tarea.descripcionTipoInspeccion
        txtDate.text = tarea.fecha?.formatDateHour()
        txtInspectionDate.text = tarea.fechaPosibleInspeccion?.formatDate()
        txtPapf.text = tarea.papf
        txtPlace.text = tarea.nombreLugarAlmacenamiento
        txtSite.text = tarea.nombreSitioInspeccion
        txtLote.text = tarea.lote.orEmpty().toString()
        txtTypeActivty.text = tarea.descActividad
        txtExport.text = if (tarea.certificadoExportacion == true) context.getString(R.string.si)
        else context.getString(R.string.no)
        txtStatus.text = tarea.estado

        root.setOnSingleClickListener {
            onTareaSelected.invoke(tarea)
        }
    }

    override fun getLayout() = R.layout.item_task_papf
}