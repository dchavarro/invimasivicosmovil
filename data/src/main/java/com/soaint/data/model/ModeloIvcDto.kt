package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.IvcDaModel
import com.soaint.data.room.tables.ModeloIvcModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.IvcDaEntity
import com.soaint.domain.model.ModeloIvcEntity

data class ModeloIvcResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ModeloIvcDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class ModeloIvcDto(
    @SerializedName("id") val id: Int,
    @SerializedName("idTipoRequerimiento") val idTipoRequerimiento: Int?,
    @SerializedName("descripciontipoRequerimiento") val descripciontipoRequerimiento: String? = EMPTY_STRING,
    @SerializedName("numeroRequerimiento") val numeroRequerimiento: Int?,
    @SerializedName("idCriticidad") val idCriticidad: Int?,
    @SerializedName("descripcionCriticidad") val descripcionCriticidad: String? = EMPTY_STRING,
)

fun ModeloIvcDto.mapToDomain() = ModeloIvcEntity(
    id,
    idTipoRequerimiento.orZero(),
    descripciontipoRequerimiento.orEmpty(),
    numeroRequerimiento.orZero(),
    idCriticidad,
    descripcionCriticidad.orEmpty(),
)

fun ModeloIvcDto.mapToDb(idVisit: Int, isLocal: Boolean) = ModeloIvcModel(
    id,
    idTipoRequerimiento.orZero(),
    descripciontipoRequerimiento.orEmpty(),
    numeroRequerimiento.orZero(),
    idCriticidad,
    descripcionCriticidad.orEmpty(),
    idVisit,
    isLocal = isLocal
)

data class BodyModeloIvcDto(
    @SerializedName("idTipoRequerimiento") val idTipoRequerimiento: Int,
    @SerializedName("numeroRequerimiento") val numeroRequerimiento: Int,
    @SerializedName("idCriticidad") val idCriticidad: Int? = null,
    @SerializedName("idVisita") val idVisita: Int,
)

fun ModeloIvcEntity.mapToData(idVisit: Int) = BodyModeloIvcDto(
    idTipoRequerimiento.orZero(),
    numeroRequerimiento.orZero(),
    idCriticidad,
    idVisit
)


data class IvcDaResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<IvcDaDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class IvcDaDto(
    @SerializedName("id") val id: Int,
    @SerializedName("totalRequerimientos") val totalRequerimientos: Int?,
    @SerializedName("numeralesDeficiencias") val numeralesDeficiencias: String? = null,
    @SerializedName("idDistribucion") val idDistribucion: Int?= null,
    @SerializedName("descripcionDistribucion") val descripcionDistribucion: String? = null,
    @SerializedName("volumenBeneficio") val volumenBeneficio: Int? = null,
    @SerializedName("veterinarioPlanta") val veterinarioPlanta: Boolean? = null,
    @SerializedName("idTipoActividad") val idTipoActividad: String? = null,
    @SerializedName("descripcionTipoActividad") val descripcionTipoActividad: String? = null,
    @SerializedName("idProductosProcesados") val idProductosProcesados: String? = null,
    @SerializedName("descripcionProductosProcesados") val descripcionProductosProcesados: String? = null,
    @SerializedName("idPruebasLaboratorio") val idPruebasLaboratorio: String? = null,
    @SerializedName("descripcionPruebasLaboratorio") val descripcionPruebasLaboratorio: String? = null,
)

fun IvcDaDto.mapToDomain(isLocal: Boolean) = IvcDaEntity(
    id,
    totalRequerimientos.orZero(),
    numeralesDeficiencias.orEmpty(),
    idDistribucion.orZero(),
    descripcionDistribucion.orEmpty(),
    volumenBeneficio.orZero(),
    veterinarioPlanta,
    idTipoActividad.orEmpty(),
    descripcionTipoActividad.orEmpty(),
    idProductosProcesados.orEmpty(),
    descripcionProductosProcesados.orEmpty(),
    idPruebasLaboratorio.orEmpty(),
    descripcionPruebasLaboratorio.orEmpty(),
    isLocal
)

fun IvcDaDto.mapToDb(idVisit: Int, isLocal: Boolean) = IvcDaModel(
    id,
    totalRequerimientos,
    numeralesDeficiencias,
    idDistribucion,
    descripcionDistribucion,
    volumenBeneficio,
    veterinarioPlanta,
    idTipoActividad,
    descripcionTipoActividad,
    idProductosProcesados,
    descripcionProductosProcesados,
    idPruebasLaboratorio,
    descripcionPruebasLaboratorio,
    idVisit,
    isLocal
)

data class BodyIvcDaDto(
    @SerializedName("totalRequerimientosModelo") val totalRequerimientos: Int? = null,
    @SerializedName("numeralesDeficienciasModelo") val numeralesDeficiencias: String? = null,
    @SerializedName("idDistribucionAutorizadaModelo") val idDistribucion: Int?= null,
    @SerializedName("volumenBeneficioModelo") val volumenBeneficio: Int? = null,
    @SerializedName("veterinarioPlantaModelo") val veterinarioPlanta: Boolean? = null,
    @SerializedName("idTipoActividadCadenaModelo") val idTipoActividad: List<Int>? = null,
    @SerializedName("idProductosProcesados") val idProductosProcesados: List<Int>? = null,
    @SerializedName("idPruebasLaboratorio") val idPruebasLaboratorio: List<Int>? = null,
    @SerializedName("idVisita") val idVisita: Int? = null,
)

data class BodyGenericIvcDaDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: BodyIvcDaDto,
) {
    constructor(ip: String, user: String, parametros: BodyIvcDaDto) : this(
        AuditoriaDto(ip, user), parametros
    )
}