package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.CountriesEntity
import com.soaint.domain.model.DepartmentEntity
import com.soaint.domain.model.TownEntity
import com.soaint.domain.model.TypeDaoEntity

@Entity(tableName = "type_dao_table")
class TypeDaoModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "tipoDao") val tipoDao: String?,
    @PrimaryKey(autoGenerate = true) val i: Int? = null,
)

fun TypeDaoModel.mapToDomain() = TypeDaoEntity(
    id, codigo, descripcion, tipoDao
)

@Entity(tableName = "countries_table")
class CountriesModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idZonaGeografica") val idZonaGeografica: Int?,
)

fun CountriesModel.mapToDomain() = CountriesEntity(
    id, codigo, nombre, idZonaGeografica
)

@Entity(tableName = "department_table")
class DepartmentModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idPais") val idPais: Int?,
)

fun DepartmentModel.mapToDomain() = DepartmentEntity(
    id, codigo, nombre, idPais
)

@Entity(tableName = "town_table")
class TownModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idDepartamento") val idDepartamento: Int?,
)

fun TownModel.mapToDomain() = TownEntity(
    id, codigo, nombre, idDepartamento
)