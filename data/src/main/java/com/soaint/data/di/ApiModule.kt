package com.soaint.data.di

import com.soaint.data.api.*
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun providerMockApi(
        @Named(MOCK_RETROFIT) retrofit: Retrofit
    ): MockApi = retrofit.create(MockApi::class.java)

    @Provides
    @Singleton
    fun providerDummyApi(
        @Named(DUMMY_RETROFIT) retrofit: Retrofit
    ): DummyApi = retrofit.create(DummyApi::class.java)

    @Provides
    @Singleton
    fun providerAuthApi(
        @Named(AUTH_RETROFIT) retrofit: Retrofit
    ): AuthApi = retrofit.create(AuthApi::class.java)

    @Provides
    @Singleton
    fun providerTransversalApi(
        @Named(TRANSVERSAL_RETROFIT) retrofit: Retrofit
    ): TransversalApi = retrofit.create(TransversalApi::class.java)

    @Provides
    @Singleton
    fun providerParametricApi(
        @Named(PARAMETRICAS_RETROFIT) retrofit: Retrofit
    ): ParametricApi = retrofit.create(ParametricApi::class.java)

    @Provides
    @Singleton
    fun providerProxyApi(
        @Named(PROXY_RETROFIT) retrofit: Retrofit
    ): ProxyApi = retrofit.create(ProxyApi::class.java)

    @Provides
    @Singleton
    fun providerGeneralesApi(
        @Named(GENERALES_RETROFIT) retrofit: Retrofit
    ): GeneralesApi = retrofit.create(GeneralesApi::class.java)

    @Provides
    @Singleton
    fun providerVisitApi(
        @Named(VISIT_RETROFIT) retrofit: Retrofit
    ): VisitApi = retrofit.create(VisitApi::class.java)

    @Provides
    @Singleton
    fun providerLoadApi(
        @Named(LOAD_RETROFIT) retrofit: Retrofit
    ): LoadApi = retrofit.create(LoadApi::class.java)

    @Provides
    @Singleton
    fun providerMasterApi(
        @Named(MASTER_RETROFIT) retrofit: Retrofit
    ): MasterApi = retrofit.create(MasterApi::class.java)

    @Provides
    @Singleton
    fun providerDocumentApi(
        @Named(DOCUMENT_RETROFIT) retrofit: Retrofit
    ): DocumentApi = retrofit.create(DocumentApi::class.java)

    @Provides
    @Singleton
    fun providerGenericsApi(
        @Named(GENERICS_RETROFIT) retrofit: Retrofit
    ): GenericsApi = retrofit.create(GenericsApi::class.java)

    @Provides
    @Singleton
    fun providerSanitaryApi(
        @Named(SANITARY_RETROFIT) retrofit: Retrofit
    ): SanitaryApi = retrofit.create(SanitaryApi::class.java)

    @Provides
    @Singleton
    fun provideRegisterSanitaryRetrofit(
        @Named(REGISTER_SANITARY_RETROFIT) retrofit: Retrofit
    ): RegisterSanitaryApi = retrofit.create(RegisterSanitaryApi::class.java)

    @Provides
    @Singleton
    fun provideModeloIvcRetrofit(
        @Named(MODELO_IVC_RETROFIT) retrofit: Retrofit
    ): ModeloIvcApi = retrofit.create(ModeloIvcApi::class.java)

    @Provides
    @Singleton
    fun providerCompanyApi(
        @Named(COMPANY_RETROFIT) retrofit: Retrofit
    ): CompanyApi = retrofit.create(CompanyApi::class.java)

    @Provides
    @Singleton
    fun providerNotificationApi(
        @Named(NOTIFICATION_RETROFIT) retrofit: Retrofit
    ): NotificationApi = retrofit.create(NotificationApi::class.java)

    @Provides
    @Singleton
    fun providerAccessDataApi(
        @Named(ACCESS_DATA_RETROFIT) retrofit: Retrofit
    ): AccessDataApi = retrofit.create(AccessDataApi::class.java)

    @Provides
    @Singleton
    fun providerProcedureApi(
        @Named(PROCEDURE_RETROFIT) retrofit: Retrofit
    ): ProcedureApi = retrofit.create(ProcedureApi::class.java)

    @Provides
    @Singleton
    fun providerInperApi(
        @Named(INPER_RETROFIT) retrofit: Retrofit
    ): InperApi = retrofit.create(InperApi::class.java)

    @Provides
    @Singleton
    fun providerMasterTransApi(
        @Named(MASTER_TRANS_RETROFIT) retrofit: Retrofit
    ): MasterTransApi = retrofit.create(MasterTransApi::class.java)

    @Provides
    @Singleton
    fun providerPlanningApi(
        @Named(PLANNING_RETROFIT) retrofit: Retrofit
    ): PlanningApi = retrofit.create(PlanningApi::class.java)

    @Provides
    @Singleton
    fun providerIntegrationsApi(
        @Named(INTEGRATIONS_RETROFIT) retrofit: Retrofit
    ): IntegrationsApi = retrofit.create(IntegrationsApi::class.java)

    @Provides
    @Singleton
    fun providerGeneralesPapfApi(
        @Named(GENERALES_PAPF_RETROFIT) retrofit: Retrofit
    ): GeneralesPapfApi = retrofit.create(GeneralesPapfApi::class.java)

    @Provides
    @Singleton
    fun providerPapfApi(
        @Named(PAPF_RETROFIT) retrofit: Retrofit
    ): PapfApi = retrofit.create(PapfApi::class.java)

    @Provides
    @Singleton
    fun providerCrudApi(
        @Named(CRUD_RETROFIT) retrofit: Retrofit
    ): CrudApi = retrofit.create(CrudApi::class.java)

    @Provides
    @Singleton
    fun providerNotificationEmailApi(
        @Named(NOTIFICATION_EMAIL_RETROFIT) retrofit: Retrofit
    ): NotificationEmailApi = retrofit.create(NotificationEmailApi::class.java)

    @Provides
    @Singleton
    fun providerFirmaApi(
        @Named(FIRMA_RETROFIT) retrofit: Retrofit
    ): FirmaApi = retrofit.create(FirmaApi::class.java)
}