package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class NotificationEntity(
    val correo: String?
) : Parcelable


@Parcelize
data class NotificationBodyEntity(
    val idVisita: Int,
    val correos: List<String>,
    val usuario: String,
) : Parcelable

@Parcelize
data class EmailsEntity(
    val correo: String?,
    val id: Int?
) : Parcelable