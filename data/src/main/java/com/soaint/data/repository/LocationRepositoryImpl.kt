package com.soaint.data.repository

import com.soaint.domain.provider.LocationProvider
import com.soaint.domain.repository.LocationRepository
import javax.inject.Inject

class LocationRepositoryImpl @Inject constructor(
    private val locationProvider: LocationProvider
) : LocationRepository {

    override suspend fun startLocationUpdates() = locationProvider.startLocationUpdates()

    override fun stopLocationUpdates() = locationProvider.stopLocationUpdates()

    override suspend fun checkGpsStatus() = locationProvider.checkGpsStatus()

    override suspend fun getLocation() = locationProvider.getLocation()
}