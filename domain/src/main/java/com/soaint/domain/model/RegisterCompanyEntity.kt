package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RegisterCompanyBodyEntity(
    val idTipoDocumento: Int?,
    val tipoDocumento: String?,
    val numeroDocumento: String?,
    val digitoVerificacion: Int?,
    val razonSocial: String?,
    val nombreComercial: String?,
    val paginaWeb: String?,
    val idPais: Int?,
    val pais: String?,
    val idDepartamento: Int?,
    val departamento: String?,
    val idMunicipio: Int?,
    val municipio: String?,
    val correoElectronico: String?,
    val direccion: String?,
    val telefono: String?,
    val celular: String?,
    val idSede: Int?,
) : Parcelable

@Parcelize
data class RegisterCompanySedeBodyEntity(
    val idSede: Int?,
    val telefonoSede: String?,
    val celularSede: String?,
    val correoElectronicoSede: String?,
    val paisSede: Int?,
    val departamentoSede: Int?,
    val municipioSede: Int?,
    val direccionSede: String?,
) : Parcelable

@Parcelize
data class PersonCompanyBodyEntity(
    val name: String,
    val idTipoDocumento: Int?,
    val tipoDocumento: String?,
    val numeroDocumento: String?,
    val idRol: Int?,
    val rol: String?,
    val email: String?,
) : Parcelable