package com.soaint.domain.model

import android.os.Parcelable
import com.soaint.domain.common.TiposControl
import kotlinx.parcelize.Parcelize

@Parcelize
data class BlockEntity(
    val id: Int?,
    val codigo: String?,
    val nombre: String?,
    val descripcion: String?,
    val idPlantilla: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val fechaModifica: String?,
    val atributosPlantilla: List<BlockAtributePlantillaEntity>?,
) : Parcelable

@Parcelize
data class BlockAtributePlantillaEntity(
    val id: Int?,
    val codigo: String?,
    val nombre: String?,
    val descripcion: String?,
    val obligatorio: Boolean?,
    val configuracion: BlockConfigAtributeEntity?,
    val idTipoAtributo: Int?,
    val descTipoAtributo: String?,
    val idBloque: Int?,
    val numeroOrden: Int?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val usuarioModifica: String?,
    val fechaModifica: String?,
    val valueString: String?,
    val valueArray: List<String>?,
    val valueArrayArray: List<List<String>>?,
) : Parcelable {
    val valor get() = valueString ?: valueArray ?: valueArrayArray

    val isCheckRadioButton: Boolean get() = idTipoAtributo == TiposControl.RADIO_GROUP && nombre?.startsWith("checkG") == true
}

@Parcelize
data class BlockConfigAtributeEntity(
    var lista: List<BlockListAtributeEntity>?,
    val longitud: String?,
    val rangoFinal: String?,
    val rangoInicial: String?,
    val requerido: Boolean?,
    val ocultar: Boolean?,
    val seleccionado: Boolean?,
    val bloquearConInformacion: Boolean?,
    val mensajeAyuda: String?,
) : Parcelable

@Parcelize
data class BlockListAtributeEntity(
    val id: String?,
    val nombre: String?,
    val seleccionado: Boolean?
) : Parcelable

@Parcelize
data class BlockBodyValueEntity(
    val idVisita: String,
    val idPlantilla: String,
    val body: String,
    val id: Int
) : Parcelable

@Parcelize
data class BlockValueEntity(
    val idVisita: String?,
    val idPlantilla: String?,
    val idBloque: String?,
    val idBloqueAtributo: String?,
    val valueString: String?,
    val valueArray: List<String>?,
    val valueArrayArray: List<List<String>>?,
) : Parcelable {
    val valor get() = valueString ?: valueArray ?: valueArrayArray
}