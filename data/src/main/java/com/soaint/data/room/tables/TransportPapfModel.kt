package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.TransportComPapfEntity
import com.soaint.domain.model.TransportDocPapfEntity

@Entity(tableName = "transport_doc_papf_table")
class TransportDocPapfModel(
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "idDocumento") val idDocumento: Int?,
    @ColumnInfo(name = "nombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name = "identificacionTipoTransporte") val identificacionTipoTransporte: String?,
    @ColumnInfo(name = "contenedor") val contenedor: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun TransportDocPapfModel.mapToDomain() = TransportDocPapfEntity(
    idSolicitud,
    tipoDocumento.orEmpty(),
    numeroDocumento.orEmpty(),
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty(),
    identificacionTipoTransporte.orEmpty(),
    contenedor.orEmpty(),
)

@Entity(tableName = "transport_com_papf_table")
class TransportComPapfModel(
    @ColumnInfo(name = "nombreEmpresa") val nombreEmpresa: String?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun TransportComPapfModel.mapToDomain() = TransportComPapfEntity(
    nombreEmpresa.orEmpty(),
    numeroDocumento.orEmpty(),
)