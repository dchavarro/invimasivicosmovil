package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.data.model.VisitEmpresaDto

@Entity(tableName = "address_table",
    foreignKeys = [
        ForeignKey(
            entity = CompanyModel::class,
            parentColumns = ["id"],
            childColumns = ["company_id"],
            onDelete = ForeignKey.CASCADE
        )
    ])
class AddressModel(
    @PrimaryKey val id: String,
    @ColumnInfo(name = "company_id") val company_id: String,
    @ColumnInfo(name = "descripcion") val descripcion: String,
    @ColumnInfo(name = "idPais") val idPais: String?,
    @ColumnInfo(name = "descPais") val descPais: String?,
    @ColumnInfo(name = "idDepartamento") val idDepartamento: String?,
    @ColumnInfo(name = "descDepartamento") val descDepartamento: String?,
    @ColumnInfo(name = "idMunicipio") val idMunicipio: String?,
    @ColumnInfo(name = "descMunicipio") val descMunicipio: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
)