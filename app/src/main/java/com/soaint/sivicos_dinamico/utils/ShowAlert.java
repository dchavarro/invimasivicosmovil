package com.soaint.sivicos_dinamico.utils;

import android.app.Activity;
import androidx.appcompat.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

public class ShowAlert
{
    public static AlertDialog.Builder alertDialogBuilder;

    public ShowAlert(Context context, String titulo, String mensaje, String tituloBotonIzq, String tituloBotonDer, final OnClickAlert onClickAlert)
    {
        alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle(titulo);
        alertDialogBuilder.setMessage(mensaje).setCancelable(false);

        if(tituloBotonIzq != null) {
            alertDialogBuilder.setPositiveButton(tituloBotonIzq, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //dialog.cancel();
                    onClickAlert.OnClickBotonIzq(dialog);
                }
            });
        }

        if(tituloBotonDer != null) {
            alertDialogBuilder.setNegativeButton(tituloBotonDer, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    //dialog.cancel();
                    onClickAlert.OnClickBotonDer(dialog);
                }
            });
        }

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        Activity activity = (Activity)context;
        // show it
        if(!activity.isFinishing()) {
            // show it
            alertDialog.show();
        }
    }

    public interface OnClickAlert
    {
        public void OnClickBotonIzq(DialogInterface dialog);
        public void OnClickBotonDer(DialogInterface dialog);
    }
}