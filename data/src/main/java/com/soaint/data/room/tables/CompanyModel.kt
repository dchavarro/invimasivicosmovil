package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.domain.model.VisitDireccionEntity
import com.soaint.domain.model.VisitEmpresaEntity
import com.soaint.domain.model.VisitEntity

@Entity(tableName = "company_table")
class  CompanyModel(
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "codigoSucursal") val codigoSucursal: String?,
    @ColumnInfo(name = "empresaAsociada") val empresaAsociada: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "idEmpresa") var idEmpresa: Int?,
    @ColumnInfo(name = "idEstadoEmpresa") val idEstadoEmpresa: Int?,
    @ColumnInfo(name = "idSede") val idSede: Int?,
    @ColumnInfo(name = "idTipoDocumento") val idTipoDocumento: Int?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "numDocumento") val numDocumento: String?,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "celular") val celular: String?,
    @ColumnInfo(name = "correo") val correo: String?,
    @ColumnInfo(name = "paginaweb") val paginaweb: String?,
    @ColumnInfo(name = "digitoVerificacion") val digitoVerificacion: Int?,
    @ColumnInfo(name = "sigla") val sigla: String?,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean?,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
    )

fun CompanyAddressModel.mapToDomain() = VisitDireccionEntity(
    descDepartamento, descMunicipio, descPais, descripcion, idDepartamento, idMunicipio, idPais
)