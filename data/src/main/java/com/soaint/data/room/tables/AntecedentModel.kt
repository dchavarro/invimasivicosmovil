package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.AntecedentEntity

@Entity(tableName = "antecedent_table")
class AntecedentModel(
    @PrimaryKey var id: Int,
    @ColumnInfo(name = "idTipo") val idTipo: Int?,
    @ColumnInfo(name = "descTipo") val descTipo: String?,
    @ColumnInfo(name = "referencia") val referencia: String?,
    @ColumnInfo(name = "numeroAsociado") val numeroAsociado: String?,
    @ColumnInfo(name = "codigoTipo") val codigoTipo: String?,
    @ColumnInfo(name = "idDenuncia") val idDenuncia: Int?,
    @ColumnInfo(name = "aplicaDenuncia") val aplicaDenuncia: Boolean?,
    @ColumnInfo(name = "idTarea") val idTarea: Int?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
)

fun AntecedentModel.mapToDomain() = AntecedentEntity(
    id,
    idTipo,
    descTipo,
    referencia,
    numeroAsociado,
    codigoTipo,
    idDenuncia,
    aplicaDenuncia,
    idTarea,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica
)