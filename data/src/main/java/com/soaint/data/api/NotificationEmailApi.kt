package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.Body
import retrofit2.http.POST

interface NotificationEmailApi {

    //Consulta facturas
    @POST("v1/enviarNotificacionCorreo/enviar")
    suspend fun sendNotification(
        @Body body: NotificationPapfBodyDto
    ): Unit



}