package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.InteractionEntity

@Entity(tableName = "interaction_table")
class InteractionModel(
    @PrimaryKey var id: Int?,
    @ColumnInfo(name = "emitirCertificacion") val emitirCertificacion: Boolean?,
    @ColumnInfo(name = "responsabilidadSanitaria") val responsabilidadSanitaria: Boolean?,
    @ColumnInfo(name = "tomaMuestras") val tomaMuestras: Boolean?,
    @ColumnInfo(name = "requiereRetiroProducto") val requiereRetiroProducto: Boolean?,
    @ColumnInfo(name = "realizoSometimiento") val realizoSometimiento: Boolean?,
    @ColumnInfo(name = "identificadorRecall") val identificadorRecall: String?,
    @ColumnInfo(name = "perdidaCertificacion") val perdidaCertificacion: Boolean?,
    @ColumnInfo(name = "productoTenenciaInvima") val productoTenenciaInvima: Boolean?,
    @ColumnInfo(name = "productoComercializado") val productoComercializado: Boolean?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
)

fun InteractionModel.mapToDomain() = InteractionEntity(
    id,
    emitirCertificacion,
    responsabilidadSanitaria,
    tomaMuestras,
    requiereRetiroProducto,
    realizoSometimiento,
    identificadorRecall,
    perdidaCertificacion,
    productoTenenciaInvima,
    productoComercializado,
    idVisita,
    activo,
    usuarioCrea,
)

fun InteractionEntity.mapToModel() = InteractionModel(
    id,
    emitirCertificacion,
    responsabilidadSanitaria,
    tomaMuestras,
    requiereRetiroProducto,
    realizoSometimiento,
    identificadorRecall,
    perdidaCertificacion,
    productoTenenciaInvima,
    productoComercializado,
    idVisita,
    activo,
    usuarioCrea,
)