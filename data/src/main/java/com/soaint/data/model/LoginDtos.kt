package com.soaint.data.model

import com.google.gson.annotations.SerializedName

data class LoginDto(
    @SerializedName("message") var message: String,
    @SerializedName("objectResponse") var user: UserDto,
    @SerializedName("statusCode") var statusCode: Int
)

data class LoginBodyDto(
    @SerializedName("request") var request: LoginRequestDto,
)

data class LoginRequestDto(
    @SerializedName("userName") var userName: String,
    @SerializedName("password") var password: String,
)