package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.CODE_EJECU
import com.soaint.data.room.tables.TaskModel

@Dao
interface TasksDao {

    @Query("SELECT * FROM task_table WHERE idTipoProducto = :idTipoProducto AND usuario = :usuario")
    suspend fun selectTask(idTipoProducto: String, usuario: String): List<TaskModel>

    @Query("SELECT * FROM task_table WHERE usuario = :usuario")
    suspend fun selectTaskByUser(usuario: String): List<TaskModel>

    @Query("SELECT * FROM task_table WHERE activo = '1' AND usuario = :usuario")
    suspend fun selectTaskOnlyActive(usuario: String): List<TaskModel>

    @Insert(entity = TaskModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTask(task: TaskModel)

    @Query("DELETE FROM task_table")
    suspend fun deleteAllTask()

    @Update(entity = TaskModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTask(visita: TaskModel)

    @Query("SELECT * FROM task_table WHERE idTarea = :idTask")
    suspend fun selectTaskById(idTask: Int): TaskModel?
}




