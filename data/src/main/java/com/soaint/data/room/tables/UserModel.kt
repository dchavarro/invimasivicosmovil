package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.common.orZero
import com.soaint.domain.model.UserEntity
import com.soaint.domain.model.UserInformationEntity
import com.soaint.domain.model.UserRolesEntity

@Entity(tableName = "credential_table")
class CredentialModel(
    @ColumnInfo(name = "userName") val userName: String,
    @ColumnInfo(name = "password") val password: String,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

@Entity(tableName = "user_table")
class UserModel(
    @ColumnInfo(name = "sub") val sub: String,
    @ColumnInfo(name = "email_verified") val emailVerified: Boolean,
    @ColumnInfo(name = "id_token") val id_token: String,
    @ColumnInfo(name = "preferred_username") val preferredUsername: String,
    @ColumnInfo(name = "given_name") val givenName: String,
    @ColumnInfo(name = "token_type") val tokenType: String,
    @ColumnInfo(name = "access_token") val accessToken: String,
    @ColumnInfo(name = "refresh_token") val refreshToken: String,
    @ColumnInfo(name = "refresh_expires_in") val refreshExpiresIn: Int,
    @ColumnInfo(name = "not-before-policy") val notBeforePolicy: Int,
    @ColumnInfo(name = "scope") val scope: String,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "session_state") val sessionState: String,
    @ColumnInfo(name = "family_name") val familyName: String,
    @ColumnInfo(name = "expires_in") val expiresIn: Int,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "userName") val userName: String,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

@Entity(tableName = "user_information_table")
class UserInformationModel(
    @ColumnInfo(name = "ciudad") val ciudad: String,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String,
    @ColumnInfo(name = "departamento") val departamento: String,
    @ColumnInfo(name = "direccion") val direccion: String,
    @ColumnInfo(name = "direccionMisional") val direccionMisional: String,
    @ColumnInfo(name = "idMisional") val idMisional: Int,
    @ColumnInfo(name = "numeroIdentidad") val numeroIdentidad: String,
    @ColumnInfo(name = "primerApellido") val primerApellido: String,
    @ColumnInfo(name = "primerNombre") val primerNombre: String,
    @ColumnInfo(name = "segundoApellido") val segundoApellido: String,
    @ColumnInfo(name = "segundoNombre") val segundoNombre: String,
    @ColumnInfo(name = "tipoIdentidad") val tipoIdentidad: String,
    @ColumnInfo(name = "userName") val usuario: String,
    @ColumnInfo(name = "idPersona") val idPersona: Int,
    @ColumnInfo(name = "idUsuario") val idUsuario: Int,
    @ColumnInfo(name = "grupoDependencia") val grupoDependencia: String,
    @ColumnInfo(name = "idDependencia") val idDependencia: Int,
    @ColumnInfo(name = "idFuncionario") val idFuncionario: Int,
    @ColumnInfo(name = "cargo") val cargo: String,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

@Entity(tableName = "user_roles_table")
class UserRolesModel(
    @ColumnInfo(name = "idRol") val idRol: Int,
    @ColumnInfo(name = "codigo") val codigo: String,
    @ColumnInfo(name = "rol") val rol: String,
    @ColumnInfo(name = "idUsuario") val idUsuario: Int,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun UserModel.mapToDomain() = UserEntity(
    sub,
    emailVerified,
    id_token,
    preferredUsername,
    givenName,
    tokenType,
    accessToken,
    refreshToken,
    refreshExpiresIn,
    notBeforePolicy,
    scope,
    name,
    sessionState,
    familyName,
    expiresIn,
    email
)

fun UserEntity.mapToModel(userName: String) = UserModel(
    sub,
    emailVerified,
    idToken,
    preferredUsername,
    givenName,
    tokenType,
    accessToken,
    refreshToken,
    refreshExpiresIn,
    notBeforePolicy,
    scope,
    name,
    sessionState,
    familyName,
    expiresIn,
    email,
    userName
)

fun UserInformationModel.mapToDomain() = UserInformationEntity(
    ciudad.orEmpty(),
    correoElectronico.orEmpty(),
    departamento.orEmpty(),
    direccion.orEmpty(),
    direccionMisional.orEmpty(),
    idMisional.orZero(),
    numeroIdentidad.orEmpty(),
    primerApellido.orEmpty(),
    primerNombre.orEmpty(),
    segundoApellido.orEmpty(),
    segundoNombre.orEmpty(),
    tipoIdentidad.orEmpty(),
    usuario.orEmpty(),
    idPersona.orZero(),
    idUsuario.orZero(),
    grupoDependencia.orEmpty(),
    idDependencia.orZero(),
    idFuncionario.orZero(),
    cargo.orEmpty()
)

fun UserInformationEntity.mapToModel() = UserInformationModel(
    ciudad.orEmpty(),
    correoElectronico.orEmpty(),
    departamento.orEmpty(),
    direccion.orEmpty(),
    direccionMisional.orEmpty(),
    idMisional.orZero(),
    numeroIdentidad.orEmpty(),
    primerApellido.orEmpty(),
    primerNombre.orEmpty(),
    segundoApellido.orEmpty(),
    segundoNombre.orEmpty(),
    tipoIdentidad.orEmpty(),
    usuario.orEmpty(),
    idPersona.orZero(),
    idUsuario.orZero(),
    grupoDependencia.orEmpty(),
    idDependencia.orZero(),
    idFuncionario.orZero(),
    cargo.orEmpty()
)

fun UserRolesModel.mapToDomain() = UserRolesEntity(
    idRol.orZero(),
    codigo.orEmpty(),
    rol.orEmpty(),
)

fun UserRolesEntity.mapToModel(idUsuario: Int) = UserRolesModel(
    idRol.orZero(),
    codigo.orEmpty(),
    rol.orEmpty(),
    idUsuario.orZero()
)