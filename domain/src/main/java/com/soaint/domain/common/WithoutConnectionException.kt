package com.soaint.domain.common

class WithoutConnectionException(message: String = "Sin conexión a internet") : Throwable(message)

class BadCredentialException(message: String = "Las credenciales no coinciden") : Throwable(message)