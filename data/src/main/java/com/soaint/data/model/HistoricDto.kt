package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.HistoricModel
import com.soaint.data.room.tables.VisitModel
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.HistoricEntity

data class HistoricResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<HistoricDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class HistoricDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("numero") val numero: String? = EMPTY_STRING,
    @SerializedName("codigoSucursal") val codigoSucursal: String? = EMPTY_STRING,
    @SerializedName("razonSocial") val razonSocial: String? = EMPTY_STRING,
    @SerializedName("fechaRegistro") val fechaRegistro: String? = EMPTY_STRING,
    @SerializedName("descDireccion") val descDireccion: String? = EMPTY_STRING,
    @SerializedName("fechaRealizo") val fechaRealizo: String? = EMPTY_STRING,
    @SerializedName("clasificacion") val clasificacion: String? = EMPTY_STRING,
    @SerializedName("descRespRealizar") val descRespRealizar: String? = EMPTY_STRING,
    @SerializedName("descFuenteInfo") val descFuenteInfo: String? = EMPTY_STRING,
    @SerializedName("descRazon") val descRazon: String? = EMPTY_STRING,
    @SerializedName("resultado") val resultado: String? = EMPTY_STRING,
    @SerializedName("observacionResultado") val observacionResultado: String? = EMPTY_STRING,
    @SerializedName("estado") val estado: String? = EMPTY_STRING,
    @SerializedName("inspector") val inspector: String? = EMPTY_STRING,
    @SerializedName("idVisita") val idVisita: String? = EMPTY_STRING,
    @SerializedName("departamento") val departamento: String? = EMPTY_STRING,
    @SerializedName("municipio") val municipio: String? = EMPTY_STRING,
    @SerializedName("medidasSanitarias") val medidasSanitarias: String? = EMPTY_STRING,
    @SerializedName("conceptoSanitario") val conceptoSanitario: String? = EMPTY_STRING,
    @SerializedName("empresaAsociada") val empresaAsociada: String? = EMPTY_STRING,
    @SerializedName("nombreEmpresaAsociada") val nombreEmpresaAsociada: String? = EMPTY_STRING,
    @SerializedName("tipoTramite") val tipoTramite: String? = EMPTY_STRING,
    @SerializedName("subTipoTramite") val subTipoTramite: String? = EMPTY_STRING,
    @SerializedName("activo") val activo: Boolean? = false,
    @SerializedName("usuarioCrea") val usuarioCrea: String? = EMPTY_STRING,
    @SerializedName("fechaCreacion") val fechaCreacion: String? = EMPTY_STRING,
    @SerializedName("usuarioModifica") val usuarioModifica: String? = EMPTY_STRING,
    @SerializedName("idEstadoHistVisita") val idEstadoHistVisita: Int?
)

fun HistoricDto.mapToDomain() = HistoricEntity(
    id,
    numero.orEmpty(),
    codigoSucursal.orEmpty(),
    razonSocial.orEmpty(),
    fechaRegistro.orEmpty(),
    descDireccion.orEmpty(),
    fechaRealizo.orEmpty(),
    clasificacion.orEmpty(),
    descRespRealizar.orEmpty(),
    descFuenteInfo.orEmpty(),
    descRazon.orEmpty(),
    resultado.orEmpty(),
    observacionResultado.orEmpty(),
    estado.orEmpty(),
    inspector.orEmpty(),
    idVisita.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    medidasSanitarias.orEmpty(),
    conceptoSanitario.orEmpty(),
    empresaAsociada.orEmpty(),
    nombreEmpresaAsociada.orEmpty(),
    tipoTramite.orEmpty(),
    subTipoTramite.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    usuarioModifica.orEmpty(),
    idEstadoHistVisita
)

fun HistoricDto.mapToDb() = HistoricModel(
    id,
    numero.orEmpty(),
    codigoSucursal.orEmpty(),
    razonSocial.orEmpty(),
    fechaRegistro.orEmpty(),
    descDireccion.orEmpty(),
    fechaRealizo.orEmpty(),
    clasificacion.orEmpty(),
    descRespRealizar.orEmpty(),
    descFuenteInfo.orEmpty(),
    descRazon.orEmpty(),
    resultado.orEmpty(),
    observacionResultado.orEmpty(),
    estado.orEmpty(),
    inspector.orEmpty(),
    idVisita.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    medidasSanitarias.orEmpty(),
    conceptoSanitario.orEmpty(),
    empresaAsociada.orEmpty(),
    nombreEmpresaAsociada.orEmpty(),
    tipoTramite.orEmpty(),
    subTipoTramite.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    usuarioModifica.orEmpty(),
    idEstadoHistVisita
)