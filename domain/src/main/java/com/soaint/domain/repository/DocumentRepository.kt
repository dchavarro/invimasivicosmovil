package com.soaint.domain.repository

import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.DocumentNameEntity
import com.soaint.domain.model.GroupEntity
import kotlinx.coroutines.flow.StateFlow

interface DocumentRepository {

    val document: StateFlow<DocumentEntity?>

    val documents: StateFlow<List<DocumentEntity>?>

    suspend fun loadDocument(idVisit: String): Result<List<DocumentEntity>>

    suspend fun loadDocumentLocal(idVisit: String): Result<List<DocumentEntity>>

    suspend fun loadDocumentName(idGrupo: Int): Result<List<DocumentNameEntity>>

    suspend fun loadDocumentNameLocal(idGrupo: Int): Result<List<DocumentNameEntity>>

    suspend fun addDocument(body: DocumentBodyEntity): Result<Unit>

    suspend fun addDocumentLocal(body: DocumentBodyEntity): Result<Unit>

    suspend fun getDocumentBodyLocalByVisit(idVisit: String): Result<List<DocumentBodyEntity>>

    suspend fun getDocumentBodyLocalById(idDocumentBody: String): Result<List<DocumentBodyEntity>>

    suspend fun getDocumentActLocal(idPlantilla: Int, idVisita: Int): Result<String?>

    suspend fun deleteDocument(id: Int, url: String?, idVisita: String, idPlantilla: String?): Result<Unit>

    suspend fun addPLantilla2DocumentLocal(body: DocumentEntity, idVisita: String): Result<Unit>

    suspend fun getDocumentUpload(): Result<Int>

    suspend fun viewDocument(idDocument: Int): Result<String>
}