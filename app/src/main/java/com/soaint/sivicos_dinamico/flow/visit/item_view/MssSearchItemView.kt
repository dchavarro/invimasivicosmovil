package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.ManageMsspEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemMsspListBinding
import com.soaint.sivicos_dinamico.databinding.ItemMsspSearchBinding
import com.xwray.groupie.databinding.BindableItem

class MssSearchItemView(
    private val nombreProducto: String,
    private val registroSaniario: String,
    private val onSelected: (String, String) -> Unit,
    ) : BindableItem<ItemMsspSearchBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemMsspSearchBinding,
        position: Int
    ) = with(viewBinding) {

        textViewNso.text = registroSaniario
        textViewNameProduct.text = nombreProducto
        checkBox.isChecked = false

        root.setOnClickListener {
            checkBox.isChecked = !checkBox.isChecked
            onSelected.invoke(nombreProducto, registroSaniario)
        }
    }

    override fun getLayout() = R.layout.item_mssp_search
}