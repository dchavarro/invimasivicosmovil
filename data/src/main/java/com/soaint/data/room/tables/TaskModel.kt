package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.domain.model.TaskEntity

@Entity(tableName = "task_table")
class TaskModel(
    @PrimaryKey val idTarea: Int,
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "codigoTarea") val codigoTarea: String?,
    @ColumnInfo(name = "descEstado") val descEstado: String?,
    @ColumnInfo(name = "descripcionActividad") val descripcionActividad: String?,
    @ColumnInfo(name = "descripcionOrigen") val descripcionOrigen: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "fechaTarea") val fechaTarea: String?,
    @ColumnInfo(name = "idActividadIVC") val idActividadIVC: Int,
    @ColumnInfo(name = "idEstadoTarea") val idEstadoTarea: Int,
    @ColumnInfo(name = "idOrigenTarea") val idOrigenTarea: Int,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "codigoEstado") val codigoEstado: String?,
    @ColumnInfo(name = "codigoOrigen") val codigoOrigen: String?,
    @ColumnInfo(name = "codigoActividad") val codigoActividad: String?,
    @ColumnInfo(name = "usuario") val usuario: String?,
)

fun TaskModel.mapToDomain() = TaskEntity(
    activo,
    codigoTarea,
    descEstado,
    descripcionActividad,
    descripcionOrigen,
    fechaCreacion,
    fechaModifica,
    fechaTarea,
    idActividadIVC,
    idEstadoTarea,
    idOrigenTarea,
    idTarea,
    idTipoProducto,
    usuarioCrea,
    usuarioModifica,
    codigoEstado,
    codigoOrigen,
    codigoActividad
)