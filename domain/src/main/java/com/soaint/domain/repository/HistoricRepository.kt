package com.soaint.domain.repository

import com.soaint.domain.model.HistoricEntity
import kotlinx.coroutines.flow.StateFlow

interface HistoricRepository {

    val historico: StateFlow<List<HistoricEntity>?>

    suspend fun loadHistoric(razonSocial: String): Result<List<HistoricEntity>>

    suspend fun loadHistoricLocal(razonSocial: String): Result<List<HistoricEntity>>
}