package com.soaint.sivicos_dinamico.flow.visit.item_view

import androidx.core.view.isVisible
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.DocumentEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemDocumentBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class DocumentItemView(
    private val document: DocumentEntity,
    private val onSelected: (DocumentEntity) -> Unit,
    private val onView: (DocumentEntity) -> Unit,
    private val onDelete: (DocumentEntity) -> Unit,
    private val onEdit: (idPlantilla: String) -> Unit,
) : BindableItem<ItemDocumentBinding>() {

    override fun bind(
        viewBinding: ItemDocumentBinding,
        position: Int
    ) = with(viewBinding) {
        textViewName.text = document.nombreDocumento
        textViewType.text = document.nombreTipoDocumental
        textViewDetail.text = document.detalle
        textViewQuantity.text = document.numeroFolios
        textViewDate.text = document.fechaDocumento?.formatDateHour()
        textViewUser.text = document.usuarioCrea
        textViewClassification.text = document.descripcionClasificacionDocumental.takeIf { it != "EJECU" } ?: "Ejecucion"

        //VER
        // si no esta firmado y tiene idplantilla
        // !(dataService.indDocumentoFirmado == false && dataService.idPlantilla)

        //EDITAR
        // si no esta firmado y tiene idplantilla
        // dataService.indDocumentoFirmado == false && dataService.idPlantilla

        //ELIMINAR
         //en ejecucion no se puede eliminar
         // si tiene clasificacion documental


        buttonEdit.isVisible = document.idPlantilla != null && document.indDocumentoFirmado == false
        buttonDelete.isVisible =
            (document.idDocumento == 0 || document.idDocumento == null) && document.indDocumentoFirmado == false
        buttonView.isVisible = !(document.idPlantilla != null && document.indDocumentoFirmado == false)
            //document.idPlantilla != null && document.indDocumentoFirmado == true && (document.idDocumento != 0 || document.idDocumento != null)

        buttonEdit.setOnSingleClickListener { onEdit.invoke(document.idPlantilla.toString()) }
        buttonDelete.setOnSingleClickListener { onDelete.invoke(document) }
        buttonView.setOnSingleClickListener { onView.invoke(document) }

        root.setOnSingleClickListener { }
    }

    override fun getLayout() = R.layout.item_document
}