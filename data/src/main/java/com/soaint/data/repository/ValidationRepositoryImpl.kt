package com.soaint.data.repository

import com.soaint.data.api.AccessDataApi
import com.soaint.data.api.GeneralesApi
import com.soaint.data.api.IntegrationsApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.ProcessExtBodyDto
import com.soaint.data.model.mapToData
import com.soaint.data.room.dao.CloseDao
import com.soaint.data.room.tables.ObservationModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.BodyObservationEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.UserRepository
import com.soaint.domain.repository.ValidationRepository
import javax.inject.Inject

class ValidationRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val generalesApi: GeneralesApi,
    private val accessDataApi: AccessDataApi,
    private val integrationsApi: IntegrationsApi,
    private val closeDao: CloseDao
) : ValidationRepository, BaseRepository() {

    //Validations
    override suspend fun validationMss(idVisita: String, tipoMs: String): Result<List<String>> =
        execute {
            val validation = closeDao.validationMss(idVisita, tipoMs)
            Result.success(validation)
        }

    override suspend fun validationUpdateMssList(
        idVisita: String,
        tipoMs: String
    ): Result<List<String>> = execute {
        val validation = closeDao.validationUpdateMssList(idVisita, tipoMs)
        Result.success(validation)
    }

    override suspend fun validationLegalAtiende(idVisita: String): Result<Boolean> = execute {
        val validation = closeDao.validationLegalAtiende(idVisita)
        Result.success(validation)
    }

    override suspend fun addObservation(
        idVisita: String,
        observation: String,
        idTipoProducto: Int
    ): Result<Unit> = execute {
        val result = closeDao.insertOrUpdateBodyObservation(
            ObservationModel(
                observation,
                idTipoProducto,
                idVisita.toInt(),
            )
        )
        Result.success(result)
    }

    //Carga de observacion  DB local
    override suspend fun getObservationLocal(
        idVisita: String,
        idTipoProducto: Int
    ): Result<BodyObservationEntity> {
        val result =
            closeDao.selectBodyObservation(idTipoProducto.toString(), idVisita)?.mapToDomain()
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    //Carga de observacion
    override suspend fun updateObservation(body: BodyObservationEntity?): Result<Unit?> = execute {
        val result = body?.mapToData()?.let { generalesApi.insertObservation(it) }
        Result.success(result)
    }

    //Carga de actualizar proceso externo
    override suspend fun updateProcessExt(visita: VisitEntity): Result<Unit> = execute {
        val user = userRepository.userInformation.value
        val result = accessDataApi.updateProcessExt(
            ProcessExtBodyDto(
                visita.tramite?.idTramite,
                user?.idPersona,
                "VISITA",
                user?.usuario
            )
        )
        Result.success(result)
    }


    override suspend fun validationRaviInper(idVisita: String): Result<List<String>> = execute {
        val validation = closeDao.validationRaviInper(idVisita)
        Result.success(validation)
    }

    override suspend fun validationAct015(idVisita: String): Result<Boolean> = execute {
        val validation = closeDao.validationAct015(idVisita)
        Result.success(validation)
    }

    override suspend fun generateRadicals(idVisita: String): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        Result.success(integrationsApi.generateRadicals(idVisita, userName))
    }

    override suspend fun validationActs(
        idVisita: Int,
        codigosPlantillas: List<String>
    ): Result<List<String>> = execute {
        val validation = closeDao.validationActs(idVisita, codigosPlantillas)
        Result.success(validation)
    }

    override suspend fun validateAtLeastOneActs(
        idVisita: Int
    ): Int {
        val exist = closeDao.validateAtLeastOneActs(idVisita)
       return exist
    }

    override suspend fun validationActsFinished(
        idVisita: Int,
        codigosPlantillas: List<String>
    ): Result<List<String>> = execute {
        val validation = closeDao.validationActsFinished(idVisita, codigosPlantillas)
        Result.success(validation)
    }
}