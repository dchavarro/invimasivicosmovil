package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.LocationModel
import com.soaint.data.room.tables.TypeMssModel
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.RolEntity

data class GroupPapfResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<GroupPapfDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class GroupPapfDto(
    @SerializedName("IdDependencia") val id: Int,
    @SerializedName("NombreDependencia") val nombreDependencia: String?
)

fun GroupPapfDto.mapToDomain(idDependencia: String, tipo: String) = DinamicQuerysPapfEntity(
    id,
    nombreDependencia.orEmpty(),
    tipo,
    idDependencia
)

fun GroupPapfDto.mapToDb(idDependencia: String, tipo:String) = TypeMssModel(
    id = id,
    null,
    descripcion = nombreDependencia,
    null,
    null,
    tipoMs = tipo,
    idTipoProducto = idDependencia
)
