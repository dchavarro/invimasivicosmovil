package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.ivc

import android.text.Editable
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.google.gson.Gson
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed interface ValidateType {

    val isVisible: Boolean
    val isValid: Boolean

    data class OnValidateCritic(
        val critic: Int?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateDeficit(
        val deficit: String?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidatePba(
        val pba: Int?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateMedic(
        val medic: Boolean?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateVolumen(
        val volumen: String?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateBank(
        val bank: List<Int>?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateProduct(
        val product: List<Int>?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType

    data class OnValidateLab(
        val lab:  List<Int>?, override val isVisible: Boolean, override val isValid: Boolean
    ) : ValidateType
}

sealed class onCloseIvoViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onCloseIvoViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onCloseIvoViewModelEvent()
    data class OnViewModelTypeReqReady(val result: List<ManageMssEntity>) :
        onCloseIvoViewModelEvent()

    data class OnViewModelCriticReady(val result: List<ManageMssEntity>) :
        onCloseIvoViewModelEvent()

    data class OnViewModelPbaReady(val result: List<ManageMssEntity>) : onCloseIvoViewModelEvent()
    data class OnViewModelBankReady(val result: List<ManageMssEntity>) : onCloseIvoViewModelEvent()
    data class OnViewModelProductReady(val result: List<ManageMssEntity>) :
        onCloseIvoViewModelEvent()

    data class OnViewModelLabReady(val result: List<ManageMssEntity>) : onCloseIvoViewModelEvent()
    data class OnViewModelIvcReady(val ivc: List<ModeloIvcEntity>) : onCloseIvoViewModelEvent()
    data class OnViewModelIvcDaReady(val ivcDa: IvcDaEntity?) : onCloseIvoViewModelEvent()
    data class OnViewModelSuccess(@StringRes val message: Int) : onCloseIvoViewModelEvent()

    data class onValidateTypeEvent(val validateData: ValidateType) : onCloseIvoViewModelEvent()

    data class OnValidateAdd(
        val typeId: Int,
        val number: Int,
    ) : onCloseIvoViewModelEvent()

}

class CloseIvoViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val modeloIvcUseCase: ModeloIvcUseCase,
    private val documentUseCase: DocumentUseCase,
    private val validationUseCase: ValidationUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onCloseIvoViewModelEvent>()
    val event = _event.asLiveData()

    private val _ivcDaData = MutableLiveData<IvcDaEntity>()
    val ivcDaData = _ivcDaData.asLiveData()

    private val _visit = MutableLiveData<VisitEntity>()
    val visit = _visit.asLiveData()

    private val _user = MutableLiveData<UserInformationEntity>()
    val user = _user.asLiveData()

    private val _isMedic = MutableLiveData<Boolean>()
    val isMedic = _isMedic.asLiveData()

    private var typeReq: List<ManageMssEntity> = emptyList()

    private var typeReqSelected: ManageMssEntity? = null

    private var critic: List<ManageMssEntity> = emptyList()

    private var criticSelected: ManageMssEntity? = null

    private var numberSelected: String? = null

    private var ivcList: List<ModeloIvcEntity> = emptyList()

    private var pba: List<ManageMssEntity> = emptyList()

    private var pbaSelected: ManageMssEntity? = null

    private var bank: List<ManageMssEntity> = emptyList()

    var selectedBankList: List<Int> = emptyList()

    private var product: List<ManageMssEntity> = emptyList()

    var  selectedProductList: List<Int> = emptyList()

    private var lab: List<ManageMssEntity> = emptyList()

    var selectedLabList:List<Int> = emptyList()

    private val _deficit = MutableLiveData(EMPTY_STRING)
    val deficit get() = _deficit.asLiveData()

    private val _volumen = MutableLiveData(EMPTY_STRING)
    val volumen get() = _volumen.asLiveData()

    private val _descConcept = MutableLiveData(EMPTY_STRING)
    val descConcept get() = _descConcept.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _total = MutableLiveData(0)
    val total get() = _total.asLiveData()

    val idMisional: Int? get() = userUseCase.getUserMisional()

    val idTipoproducto get() = _visit.value?.idTipoProducto.orZero()

    val gson = Gson()

    private var documents: List<DocumentEntity>? = emptyList()

    fun init(visitData: List<VisitEntity>) = execute {
        _event.value = onCloseIvoViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _user.value = userUseCase.getUserInformation()
        _visit.value =
            visitUseCase.loadVisitLocal(visitData.firstOrNull()?.idTarea.orZero()).getOrNull()
                ?.firstOrNull()
        val visitGroup = visitUseCase.loadGroupLocal(_visit.value?.id.orZero().toString())
        documents = documentUseCase.loadDocumentLocal(visitData.firstOrNull()?.id.toString()).getOrNull()
        onLoadIvc()
        onLoadTypeReq()
        onLoadCritic()
        onLoadPba()
        onLoadIvcDa()
    }

    private fun onLoadIvcDa() = execute {
        val result = modeloIvcUseCase.getReqDaLocal(_visit.value?.id.orZero().toString())
        _event.value = if (result.isSuccess) {
            _ivcDaData.value = result.getOrNull()
            onLoadBank()
            onLoadProduct()
            onLoadLab()
            onCloseIvoViewModelEvent.OnViewModelIvcDaReady(result.getOrNull())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
        visibility()
    }

    private fun onLoadIvc() = execute {
        val result = modeloIvcUseCase.getRequerimientosLocal(_visit.value?.id.orZero())
        _event.value = if (result.isSuccess) {
            ivcList = result.getOrNull().orEmpty()
            _total.value = result.getOrNull()?.count() ?: 0
            onCloseIvoViewModelEvent.OnViewModelIvcReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    private fun onLoadTypeReq() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDTR, idTipoproducto)
        _event.value = if (result.isSuccess) {
            typeReq = result.getOrNull().orEmpty()
            onCloseIvoViewModelEvent.OnViewModelTypeReqReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setTypeReqSelected(typeReqSelected: String) {
        this.typeReqSelected = typeReq.find { it.descripcion.equals(typeReqSelected, true) }
    }

    private fun onLoadCritic() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDC, idTipoproducto)
        _event.value = if (result.isSuccess) {
            critic = result.getOrNull().orEmpty()
            onCloseIvoViewModelEvent.OnViewModelCriticReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setCriticSelected(criticSelected: String) {
        this.criticSelected = critic.find { it.descripcion.equals(criticSelected, true) }
    }

    fun setNumberSelected(numberSelected: String) {
        this.numberSelected = numberSelected

    }

    fun onDelete(id: Int) {
        ivcList.find { it.id.orZero() == id }?.let {
            it.isDeleted = true
            onDeleteIvc(it)
        }
    }

    fun onDeleteIvc(it: ModeloIvcEntity) = execute {
        val result = modeloIvcUseCase.deleteRequerimientoLocal(
            it, _visit.value?.id.orZero()
        )
        if (result.isFailure) {
            _event.value = onCloseIvoViewModelEvent.onLoadingError(null)
        }
        onLoadIvc()
    }

    private fun onLoadPba() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDDPBA, idTipoproducto)
        _event.value = if (result.isSuccess) {
            pba = result.getOrNull().orEmpty()
            onCloseIvoViewModelEvent.OnViewModelPbaReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setPbaSelected(pbaSelected: String) {
        this.pbaSelected = pba.find { it.descripcion.equals(pbaSelected, true) }
    }

    fun setMedic(data: Boolean?) {
        _isMedic.value = data
    }

    private fun onLoadBank() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDTAG, idTipoproducto)
        _event.value = if (result.isSuccess) {
            bank = result.getOrNull().orEmpty()
            val selectedIds: List<Int> = gson.fromJson(ivcDaData.value?.idTipoActividad ?: "[]", Array<Int>::class.java).toList()
            setBankSelected(selectedIds)
            onCloseIvoViewModelEvent.OnViewModelBankReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setBankSelected(bankSelected: List<Int>) {
        this.selectedBankList = ArrayList(bank.filter { bankSelected.contains(it.id) }.map { it.id ?: 0 })
    }

    private fun onLoadProduct() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDPP, idTipoproducto)
        _event.value = if (result.isSuccess) {
            product = result.getOrNull().orEmpty()
            val selectedIds: List<Int> = gson.fromJson(ivcDaData.value?.idProductosProcesados ?: "[]", Array<Int>::class.java).toList()
            setProductSelected(selectedIds)
            onCloseIvoViewModelEvent.OnViewModelProductReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setProductSelected(productSelected: List<Int>) {
        this.selectedProductList = ArrayList(product.filter { productSelected.contains(it.id) }.map { it.id ?: 0 })
    }

    private fun onLoadLab() = execute {
        val result = manageMssUseCase.loadTypeMssLocal(CDPL, idTipoproducto)
        _event.value = if (result.isSuccess) {
            lab = result.getOrNull().orEmpty()
            val selectedIds: List<Int> = gson.fromJson(ivcDaData.value?.idPruebasLaboratorio ?: "[]", Array<Int>::class.java).toList()
            setLabSelected(selectedIds)
            onCloseIvoViewModelEvent.OnViewModelLabReady(result.getOrNull().orEmpty())
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
    }

    fun setLabSelected(labSelected: List<Int>) {
        this.selectedLabList =  ArrayList(lab.filter { labSelected.contains(it.id) }.map { it.id ?: 0 })
    }

    fun setDeficitChange(data: Editable?) {
        _deficit.value = data.toString()
    }

    fun setVolumenChange(data: Editable?) {
        _volumen.value = data.toString()
    }

    fun setDescConceptChange(data: Editable?) {
        _descConcept.value = data.toString()
    }

    fun setObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun checkAdd() {
        val typeId = typeReqSelected?.id.orZero()
        val number = numberSelected?.orEmpty()?.toInt().orZero()

        if (typeId.equals(0) || number.equals(0)) _event.value =
            onCloseIvoViewModelEvent.OnValidateAdd(typeId, number)
        else {
            addIvc()
        }
    }

    fun addIvc() = execute {
        val body = ModeloIvcEntity(
            idTipoRequerimiento = typeReqSelected?.id.orZero(),
            descripciontipoRequerimiento = typeReqSelected?.descripcion.orEmpty(),
            numeroRequerimiento = numberSelected.orEmpty().toInt(),
            idCriticidad = criticSelected?.id,
            descripcionCriticidad = criticSelected?.descripcion.orEmpty(),
            isLocal = true
        )
        val result =
            modeloIvcUseCase.addRequerimientoLocal(body, _visit.value?.id.orZero().toString())
        _event.value = if (result.isSuccess) {
            onCloseIvoViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
        onLoadIvc()
        onLoadTypeReq()
        numberSelected = null
    }

    fun visibility(
        disableValidation: Boolean = false
    ): Boolean {
        val deficit = _deficit.value!!
        val pba = pbaSelected?.id
        val volumen = _volumen.value
        val medic = _isMedic.value
        val bank = selectedBankList
        val product = selectedProductList
        val lab = selectedLabList
        val critic = criticSelected?.id

        //Criticidad
        val isCriticidadVisible =
            (idMisional == ID_MISIONAL_MEDICAMENTOS) && visitUseCase.getGroupsVisit()
                ?.any { it.id == 36 } == true
        val isCriticidadValid = !disableValidation || (critic != null && critic != 0)
        val typeCritic = ValidateType.OnValidateCritic(
            critic, isVisible = isCriticidadVisible, isCriticidadValid
        )
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeCritic)

        //deficiencias
        val isDeficitVisible =
            (idMisional == ID_MISIONAL_ALIMENTOS) && documents
                ?.any { it.codigoPlantilla == IVC_INS_FM027 } == true
        val isDeficitValid = !disableValidation || deficit.isNotBlank()
        val typeDeficit =
            ValidateType.OnValidateDeficit(deficit, isVisible = isDeficitVisible, isDeficitValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeDeficit)


        //distribucion
        val isPbaVisible = _visit.value?.idGrupoTrabajo != 22 && visitUseCase.getGroupsVisit()
            ?.any { it.id == ID_GROUP_PLANTAS_ANIMAL } == true
        val isPbaValid = !disableValidation || (pba != null && pba != 0)
        val typePba = ValidateType.OnValidatePba(pba, isVisible = isPbaVisible, isPbaValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typePba)


        //medico veterinario
        val isMedicVisible = _visit.value?.idGrupoTrabajo != 22 && visitUseCase.getGroupsVisit()
            ?.any { it.id == ID_GROUP_PLANTAS_ANIMAL } == true
        val isMedicValid = !disableValidation || medic != null
        val typeMedic =
            ValidateType.OnValidateMedic(medic, isVisible = isMedicVisible, isMedicValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeMedic)

        //volumen
        val isVolumenVisible =
            _visit.value?.idGrupoTrabajo != 22 && documents?.any {
                it.codigoPlantilla == IVC_INS_FM105 || it.codigoPlantilla == IVC_INS_FM106 || it.codigoPlantilla == IVC_INS_FM147 || it.codigoPlantilla == IVC_INS_FM108 || it.codigoPlantilla == IVC_INS_FM109 || it.codigoPlantilla == IVC_INS_FM089
            } == true

        val isVolumenValid = !disableValidation || volumen?.isNotBlank() == true
        val typeVolumen =
            ValidateType.OnValidateVolumen(volumen, isVisible = isVolumenVisible, isVolumenValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeVolumen)

        //Banco
        val isBankVisible =
            _visit.value?.idGrupoTrabajo != 22 && idMisional == ID_MISIONAL_DISPOSITIVOS && _visit.value?.codigoRazon != REASON_RAVI_INPER && visitUseCase.getGroupsVisit()
                ?.any { it.id == 24 || it.id == 26 } == true
        val isBankValid = !disableValidation || (bank.isNotEmpty() && bank.all { it != 0 })
        val typeBank = ValidateType.OnValidateBank(bank, isVisible = isBankVisible, isBankValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeBank)


        //Productos
        val isProductVisible =
            _visit.value?.idTipoProducto == ID_MISIONAL_DISPOSITIVOS && visitUseCase.getGroupsVisit()
                ?.any { it.id == 24 || it.id == 26 } == true
        val isProductValid = !disableValidation ||  (product.isNotEmpty() && product.all { it != 0 })
        val typeProduct =
            ValidateType.OnValidateProduct(product, isVisible = isProductVisible, isProductValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeProduct)

        //laboratorio
        val isLabVisible =
            _visit.value?.idTipoProducto == ID_MISIONAL_DISPOSITIVOS && visitUseCase.getGroupsVisit()
                ?.any { it.id == 26 } == true
        val isLabValid = !disableValidation || (lab.isNotEmpty() && lab.all { it != 0 })
        val typeLab = ValidateType.OnValidateLab(lab, isVisible = isLabVisible, isLabValid)
        _event.value = onCloseIvoViewModelEvent.onValidateTypeEvent(typeLab)

        return (!isCriticidadVisible || isCriticidadValid) && (!isDeficitVisible || isDeficitValid) &&
                (!isPbaVisible || isPbaValid) && (!isMedicVisible || isMedicValid) && (!isVolumenVisible || isVolumenValid) &&
                (!isBankVisible || isBankValid) && (!isProductVisible || isProductValid) && (!isLabVisible || isLabValid)
    }

    fun checkData() {
        val isValid = visibility(true)
        if (isValid) {
            updateDA()
        }
    }

    fun updateDA() = execute {
        val body = IvcDaEntity(
            totalRequerimientos = _total.value,
            numeralesDeficiencias = _deficit.value,
            idDistribucion = pbaSelected?.id,
            descripcionDistribucion = pbaSelected?.descripcion,
            volumenBeneficio = _volumen.value?.toIntOrNull(),
            veterinarioPlanta = _isMedic.value,
            idTipoActividad = selectedBankList.toString(),
            descripcionTipoActividad = null,
            idProductosProcesados = selectedProductList.toString(),
            descripcionProductosProcesados = null,
            idPruebasLaboratorio = selectedLabList.toString(),
            descripcionPruebasLaboratorio = null,
            isLocal = true
        )

       val result = modeloIvcUseCase.addReqDALocal(body, _visit.value?.id.orZero().toString())
        _event.value = if (result.isSuccess) {
            onCloseIvoViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
        } else {
            onCloseIvoViewModelEvent.onLoadingError(null)
        }
        _visit.value =
            visitUseCase.loadVisitLocal(_visit.value?.idTarea.orZero()).getOrNull()?.firstOrNull()
    }
}