package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.room.tables.CertificateModel
import com.soaint.data.room.tables.InteractionModel
import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.InteractionEntity

data class InteractionResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: InteractionDto,
    @SerializedName("statusCode") val statusCode: Int
)

data class InteractionDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("emitirCertificacion") val emitirCertificacion: Boolean?,
    @SerializedName("responsabilidadSanitaria") val responsabilidadSanitaria: Boolean?,
    @SerializedName("tomaMuestras") val tomaMuestras: Boolean?,
    @SerializedName("requiereRetiroProducto") val requiereRetiroProducto: Boolean?,
    @SerializedName("realizoSometimiento") val realizoSometimiento: Boolean?,
    @SerializedName("identificadorRecall") val identificadorRecall: String?,
    @SerializedName("perdidaCertificacion") val perdidaCertificacion: Boolean?,
    @SerializedName("productoTenenciaInvima") val productoTenenciaInvima: Boolean?,
    @SerializedName("productoComercializado") val productoComercializado: Boolean?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
)

fun InteractionDto.mapToDomain() = InteractionEntity(
    id,
    emitirCertificacion,
    responsabilidadSanitaria,
    tomaMuestras,
    requiereRetiroProducto,
    realizoSometimiento,
    identificadorRecall,
    perdidaCertificacion,
    productoTenenciaInvima,
    productoComercializado,
    idVisita,
    activo,
    usuarioCrea,
)

fun InteractionDto.mapToDb() = InteractionModel(
    id,
    emitirCertificacion,
    responsabilidadSanitaria,
    tomaMuestras,
    requiereRetiroProducto,
    realizoSometimiento,
    identificadorRecall,
    perdidaCertificacion,
    productoTenenciaInvima,
    productoComercializado,
    idVisita,
    activo,
    usuarioCrea,
)

fun InteractionEntity.mapToData() = InteractionDto(
    id,
    emitirCertificacion,
    responsabilidadSanitaria,
    tomaMuestras,
    requiereRetiroProducto,
    realizoSometimiento,
    identificadorRecall,
    perdidaCertificacion,
    productoTenenciaInvima,
    productoComercializado,
    idVisita,
    activo,
    usuarioCrea,
)


data class BodyGenericInteractionDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: InteractionDto
) {
    constructor(ip: String, user: String, interaction: InteractionDto) : this(
        AuditoriaDto(ip, user),
        interaction
    )
}

data class BodyCertificateDto(
    @SerializedName("objAuditoria") val objAuditoria: AuditoriaDto,
    @SerializedName("objOperacion") val objOperacion: NitDto
) {
    constructor(ip: String, user: String, nit: String) : this(
        AuditoriaDto(ip, user),
        NitDto(nit)
    )
}

data class NitDto(
    @SerializedName("NIT") val NIT: String? = EMPTY_STRING
)

data class CertificateResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<CertificateDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class CertificateDto(
    @SerializedName("tipoProducto") val tipoProducto: String?,
    @SerializedName("grupoProducto") val grupoProducto: String?,
    @SerializedName("concepto") val concepto: String?,
    @SerializedName("rol") val rol: String?,
    @SerializedName("Idtramite") val idtramite: Int?,
    @SerializedName("IdCertificado") val idCertificado: Int?,
    @SerializedName("estado") val estado: String?,
)

fun CertificateDto.mapToDomain() = CertificateEntity(
    tipoProducto,
    grupoProducto,
    concepto,
    rol,
    idtramite,
    idCertificado,
    estado
)

fun CertificateEntity.mapToData() = CertificateDto(
    tipoProducto,
    grupoProducto,
    concepto,
    rol,
    idtramite,
    idCertificado,
    estado
)

fun CertificateDto.mapToDb(nitEmpresa: String) = CertificateModel(
    tipoProducto,
    grupoProducto,
    concepto,
    rol,
    idtramite,
    idCertificado,
    estado,
    nitEmpresa
)

data class BodyUpdateCertificateDto(
    @SerializedName("objAuditoria") val objAuditoria: AuditoriaDto,
    @SerializedName("objOperacion") val objOperacion: BodyUpdateCertificateOperationDto
) {
    constructor(ip: String, user: String, obj: BodyUpdateCertificateOperationDto) : this(
        AuditoriaDto(ip, user),
        obj
    )
}

data class BodyUpdateCertificateOperationDto(
    @SerializedName("nit") val nit: String?,
    @SerializedName("idTramite") val idTramite: Int?,
    @SerializedName("idCertificado") val idCertificado: Int?,
    @SerializedName("estadoCertificado") val estado: String?,
)