package com.soaint.domain.use_case

import com.soaint.domain.model.ManageAmssEntity
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.ManageMssRepository
import javax.inject.Inject

class ManageMssUseCase @Inject constructor(
    private val manageMssRepository: ManageMssRepository
) {

    val manageMss get() = manageMssRepository.manageMss

    suspend fun loadTypeMss(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int,
    ) = manageMssRepository.loadTypeMss(tipoMedidaSanitaria, idTipoProducto)

    suspend fun loadTypeMssLocal(
        tipoMedidaSanitaria: String,
        idTipoProducto: Int
    ) = manageMssRepository.loadTypeMssLocal(tipoMedidaSanitaria, idTipoProducto)

    suspend fun loadRol(
        tipo: String,
        idTipoProducto: Int
    ) = manageMssRepository.loadRol(tipo, idTipoProducto)

    suspend fun countMssLocal(idVisita: String) = manageMssRepository.countMssLocal(idVisita)

    suspend fun loadListMss(razonSocial: String, tipoMedidaSanitaria: String) =
        manageMssRepository.loadListMss(razonSocial, tipoMedidaSanitaria)

    suspend fun loadListMssLocal(razonSocial: String, tipoMedidaSanitaria: String) =
        manageMssRepository.loadListMssLocal(razonSocial, tipoMedidaSanitaria)

    suspend fun updateMsspList() = manageMssRepository.updateMsspList()

    suspend fun insertMsseList(idVisit: Int) = manageMssRepository.insertMsseList(idVisit)

    suspend fun updateMsspListLocal(id: Int, codigo: String, tipoMs: String, lote: String) =
        manageMssRepository.updateMsspListLocal(id, codigo, tipoMs, lote)

    suspend fun insertMsseListLocal(
        visit: VisitEntity,
        codigo: String,
        tipoMs: String,
        typeActivity: String,
        nombreProducto: String,
        registroSanitario: String
    ) = manageMssRepository.insertMsseListLocal(
        visit,
        codigo,
        tipoMs,
        typeActivity,
        nombreProducto,
        registroSanitario
    )

    suspend fun updateAmsspLocal(it: ManageMssEntity, idVisita: String) =
        manageMssRepository.updateAmsspLocal(it, idVisita)

    suspend fun insertOrUpdateTypeProductLocal(it: ManageMssEntity, idVisita: String) =
        manageMssRepository.insertOrUpdateTypeProductLocal(it, idVisita)

    suspend fun searchRegisterSanitary(search: String) =
        manageMssRepository.searchRegisterSanitary(search)

    suspend fun loadMssApplied(idVisita: String) = manageMssRepository.loadMssApplied(idVisita)

    suspend fun insertMssApplied(idVisita: String) = manageMssRepository.insertMssApplied(idVisita)

    suspend fun loadMssAppliedLocal(idVisita: String) =
        manageMssRepository.loadMssAppliedLocal(idVisita)

    suspend fun getmssUpload() = manageMssRepository.getmssUpload()

    suspend fun insertRsLocal(idVisit: Int, nombreProducto: String, registroSanitario: String) =
        manageMssRepository.insertRsLocal(idVisit, nombreProducto, registroSanitario)

    suspend fun loadRsLocal(idVisit: Int) = manageMssRepository.loadRsLocal(idVisit)

    suspend fun loadMssLocalByIdVisit(idVisit: Int) =
        manageMssRepository.loadMssLocalByIdVisit(idVisit)

    suspend fun loadDateMssApplied(idVisita: Int, mss: ManageAmssEntity) =
        manageMssRepository.loadDateMssApplied(idVisita, mss)

    suspend fun loadTypeProduct(tipo: String) = manageMssRepository.loadTypeProduct(tipo)

}