package com.soaint.data.api

import com.soaint.data.common.CODE_EJECU
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.model.*
import com.soaint.domain.model.TaskEntity
import retrofit2.http.*

interface GeneralesApi {

    //SERVICIO CONSULTA TAREAS
    @GET("v1/tarea")
    suspend fun getTasks(
        @Query("usuario") usuario: String,
        @Query("codigoActividad") codigoActividad: String = CODE_EJECU,
        @Query("indNoRequierePagina") indNoRequierePagina: Boolean = true,
    ): TaskResponseDto

    //actualizar TAREAS
    @PUT("v1/tarea")
    suspend fun updateTask(
        @Body body: TaskBodyDto
    ): Unit

    //Crear TAREAS
    @POST("v1/tarea")
    suspend fun createTask(
        @Body body: TaskMssBodyDto
    ): TaskOneResponseDto

    //SERVICIO CREACION OBSERVACION - SOLICITUD PERSONAL
    @POST("v1/observacion")
    suspend fun insertObservation(
        @Body body: BodyObservationDto
    ): Unit

    //SERVICIO RELACIONAR EMPRESA CON TAREA
    @POST("v1/tarea/sucursal-empresa")
    suspend fun createTaskCompany(
        @Body body: List<TaskCompanyBodyDto>
    ): Unit
}