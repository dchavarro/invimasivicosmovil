package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.NotificationModel
import com.soaint.domain.model.NotificationBodyEntity
import com.soaint.domain.model.NotificationEntity

data class NotificationResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<NotificationDto>,
    @SerializedName("statusCode") val statusCode: Int
)

data class NotificationDto(
    @SerializedName("correo") val correo: String?,
)


fun NotificationDto.mapToDomain() = NotificationEntity(
    correo
)

fun NotificationDto.mapToDb(idVisit: String) = NotificationModel(
    correo,
    idVisit
)

data class NotificationBodyDto(
    @SerializedName("idVisita") val idVisita: Int,
    @SerializedName("correos") val correos: List<String>,
    @SerializedName("usuario") val usuario: String
)

fun NotificationBodyEntity.mapToData() = NotificationBodyDto(
    idVisita,
    correos.map { it },
    usuario,
)
