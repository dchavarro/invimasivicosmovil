package com.soaint.sivicos_dinamico.flow.papf.views.documentation

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.DocumentationPapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(val documents: List<DocumentationPapfEntity>) : OnViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class DocumentationViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _documentsData = MutableLiveData<List<DocumentationPapfEntity>>()
    val documentsData = _documentsData.asLiveData()


    fun init(id: Int) {
        loadInvoice(id)
    }

    fun loadInvoice(requestId: Int) = execute {
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _documentsData.value = papfUseCase.loadDocumentationLocal(requestId).getOrNull().orEmpty()
        _event.value = if (_documentsData.value != null) {
            documentsData.value?.let { OnViewModelEvent.OnViewModelReady(it) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
