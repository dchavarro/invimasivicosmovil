package com.soaint.data.api

import com.soaint.data.model.RegisterSanitaryResponseDto
import retrofit2.http.GET
import retrofit2.http.Path

interface RegisterSanitaryApi {

    @GET("v1/registroSanitario/consultarRegistroSanitarioTipoProducto/{registroSanitario}")
    suspend fun searchRegisterSanitary(
        @Path("registroSanitario") registroSanitario: String,
    ): RegisterSanitaryResponseDto

}