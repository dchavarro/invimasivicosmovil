package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.soaint.domain.common.getBase64FromPath
import com.soaint.domain.common.getBase64ToBitMap
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface

interface OnFirmaEvent {
    fun onFirmaRequested(txt: TextView, img: ImageView)
    fun onOpenDialogRequested(txt: TextView, img: ImageView)
}

class Firma : ControlInterface {
    var listener: OnFirmaEvent? = null
    lateinit var txtRutaFirma: TextView
    lateinit var btn: Button
    lateinit var classContext: Context
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_firma, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imgFirma = view.findViewById<ImageView>(R.id.imgFirma)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        lblText.text = control.descripcion
        txtRutaFirma = view.findViewById<TextView>(R.id.txtRutaFirma)
        txtRutaFirma.text = valorAlmacenado
        btn = view.findViewById<Button>(R.id.btnFirmar)
        btn.text = context.getString(R.string.firmar)
        if (valorAlmacenado.isNotEmpty()) {
            val text = getBase64FromPath(valorAlmacenado)
            val bitmap = getBase64ToBitMap(text)
            //?:*/ BitmapFactory.decodeFile(valorAlmacenado)
            val newBitmap: Bitmap? = bitmap?.let { Bitmap.createScaledBitmap(it, 512, 512, false) }
            imgFirma!!.setImageBitmap(newBitmap)
        }

        btn.setOnSingleClickListener {
            if (listener != null) {
                listener!!.onOpenDialogRequested(txtRutaFirma, imgFirma)
            }
        }
        btn.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)
        return view
    }

    override fun getData(): String {
        return txtRutaFirma.text.toString()
    }

    override fun showError(mensaje: String?) {
        btn.setBackgroundColor(classContext.getColor(R.color.colorRed))
    }

    override fun clearError() {
        btn.setBackgroundColor(classContext.getColor(R.color.colorPrimary))
    }
}