package com.soaint.sivicos_dinamico.di

import com.soaint.sivicos_dinamico.services.LocationService
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServiceBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun bindLocationService(): LocationService
}