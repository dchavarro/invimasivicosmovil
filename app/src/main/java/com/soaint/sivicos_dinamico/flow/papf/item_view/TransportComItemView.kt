package com.soaint.sivicos_dinamico.flow.papf.item_view

import com.soaint.domain.model.TransportComPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTransportComPapfBinding
import com.xwray.groupie.databinding.BindableItem

class TransportComItemView(
    private val com: TransportComPapfEntity,
) : BindableItem<ItemTransportComPapfBinding>() {

    override fun bind(
        viewBinding: ItemTransportComPapfBinding,
        position: Int
    ) = with(viewBinding) {
        textViewDoc.text = com.numeroDocumento
        textViewName.text = com.nombreEmpresa
    }

    override fun getLayout() = R.layout.item_transport_com_papf
}