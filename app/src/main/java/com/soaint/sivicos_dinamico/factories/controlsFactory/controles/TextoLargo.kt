package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.view.isVisible
import com.google.android.material.textfield.TextInputLayout
import com.soaint.data.common.intOrString
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface

class TextoLargo : ControlInterface {
    var editText: EditText? = null
    lateinit var til: TextInputLayout
    var isVisible: Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_texto_largo, null)
        val lblHint = view.findViewById<TextView>(R.id.lblTitulo)
        val editTextHint = view.findViewById<TextView>(R.id.editText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)

        val maxLength: Int =
            if (control.configuracion?.longitud.isNullOrEmpty() || control.configuracion?.longitud == "0") {
                1000
            } else {
                control.configuracion?.longitud.intOrString()
            }
        til = view.findViewById<TextInputLayout>(R.id.textInputLayout)
        til.editText!!.filters = arrayOf<InputFilter>(InputFilter.LengthFilter(maxLength))
        til.counterMaxLength = maxLength
        lblHint.text = control.descripcion
        editTextHint.hint = control.descripcion
        til.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            til.helperText = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        if (valorAlmacenado.isNotEmpty()) {
            til.editText!!.setText(valorAlmacenado)
        }

        editText = til.editText

        return view
    }

    override fun getData(): String {
        return editText!!.text.toString()
    }

    override fun showError(mensaje: String?) {
        til.isErrorEnabled = true
        til.error = mensaje
    }

    override fun clearError() {
        til.error = null
    }
}