package com.soaint.data.api

import com.soaint.data.model.RegisterCompanyBodyDto
import retrofit2.http.Body
import retrofit2.http.POST

interface PlanningApi {

    @POST("/v1/empresa/saveEmpresaNoInscrita")
    suspend fun saveEmpresa(
        @Body body: RegisterCompanyBodyDto
    ): Unit

}