package com.soaint.data.api

import com.soaint.data.model.UserInformationBodyDto
import com.soaint.data.model.UserInformationDto
import retrofit2.http.Body
import retrofit2.http.POST

interface TransversalApi {

    @POST("v1/usuario/informacion")
    suspend fun getUserInformation(
        @Body body: UserInformationBodyDto
    ): UserInformationDto
}