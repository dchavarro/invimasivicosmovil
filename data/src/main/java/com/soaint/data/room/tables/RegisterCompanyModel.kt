package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.VisitCompanyPersonEntity

@Entity(
    tableName = "company_person_table",
    foreignKeys = [
        ForeignKey(
            entity = VisitModel::class,
            parentColumns = ["id"],
            childColumns = ["idVisita"],
            onDelete = ForeignKey.CASCADE
        )
    ]
)
class CompanyPersonModel(
    @ColumnInfo(name = "codigoTipoDocumentoPersona") val codigoTipoDocumentoPersona: Int?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "numeroDocumentoPersona") val numeroDocumentoPersona: String?,
    @ColumnInfo(name = "primerNombre") val primerNombre: String?,
    @ColumnInfo(name = "segundoNombre") val segundoNombre: String?,
    @ColumnInfo(name = "primerApellido") val primerApellido: String?,
    @ColumnInfo(name = "segundoApellido") val segundoApellido: String?,
    @ColumnInfo(name = "idRolPersona") val idRolPersona: Int?,
    @ColumnInfo(name = "descripcionRolPersona") val descripcionRolPersona: String?,
    @ColumnInfo(name = "correoElectronicoP") val correoElectronicoP: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun CompanyPersonModel.mapToDomain() = VisitCompanyPersonEntity(
    codigoTipoDocumentoPersona.orEmpty(),
    tipoDocumento.orEmpty(),
    numeroDocumentoPersona.orEmpty(),
    primerNombre.orEmpty(),
    segundoNombre.orEmpty(),
    primerApellido.orEmpty(),
    segundoApellido.orEmpty(),
    idRolPersona.orEmpty(),
    descripcionRolPersona.orEmpty(),
    correoElectronicoP.orEmpty(),
)

fun VisitCompanyPersonEntity.mapToModel(idVisita: Int) = CompanyPersonModel(
    codigoTipoDocumentoPersona.orEmpty(),
    tipoDocumento.orEmpty(),
    numeroDocumentoPersona.orEmpty(),
    primerNombre.orEmpty(),
    segundoNombre.orEmpty(),
    primerApellido.orEmpty(),
    segundoApellido.orEmpty(),
    idRolPersona.orEmpty(),
    descripcionRolPersona.orEmpty(),
    correoElectronicoP.orEmpty(),
    idVisita,
)