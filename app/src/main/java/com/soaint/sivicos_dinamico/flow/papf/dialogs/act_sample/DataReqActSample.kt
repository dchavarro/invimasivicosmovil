package com.soaint.sivicos_dinamico.flow.papf.dialogs.act_sample

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.viewModels
import com.soaint.data.common.orZero
import com.soaint.domain.model.DataReqActSamplePapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseDialogFragment
import com.soaint.sivicos_dinamico.databinding.DialogPapfActsampleBinding
import com.soaint.sivicos_dinamico.extensions.*
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.GroupieAdapter

const val KEY_ID_PRODUCT = "KEY_ID_PRODUCT"
const val KEY_ID_REQUEST = "KEY_ID_REQUEST"

class DataReqActSample : BaseDialogFragment() {

    companion object {

        fun getInstance(
            idProducto: Int,
            idSolicitud: Int
        ) = DataReqActSample().apply {
            arguments = bundleOf(
                KEY_ID_PRODUCT to idProducto,
                KEY_ID_REQUEST to idSolicitud,
            )
        }
    }

    private val viewModel: DataReqActSampleViewModel by viewModels { viewModelFactory }

    private val binding by fragmentBinding<DialogPapfActsampleBinding>(R.layout.dialog_papf_actsample)

    private val adapter by lazy { GroupieAdapter() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.95).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window?.setLayout(width, height)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.imageViewClose.setOnSingleClickListener { dismiss() }
        binding.buttonSave.setOnSingleClickListener {
            requireActivity().hideKeyboard()
            viewModel.checkData()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        val idProducto = arguments?.getInt(KEY_ID_PRODUCT) ?: 0
        val idSolicitud = arguments?.getInt(KEY_ID_REQUEST) ?: 0
        viewModel.init(idProducto, idSolicitud)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        when (event) {
            is OnViewModelEvent.onSnack -> showSnack(event.isSuccess, event.message)
            is OnViewModelEvent.onViewModelData -> onViewModelData(event.data)
            is OnViewModelEvent.onViewModelUnity -> onViewModelUnity(event.unitys)
            is OnViewModelEvent.onViewModelPresentation -> onViewModelPresentation(event.presentations)
            is OnViewModelEvent.OnValidate -> validateErrorData(
                event.unityId,
                event.presentationId,
                event.unityLote,
                event.neto,
            )
        }
    }

    private fun onViewModelData(result: DataReqActSamplePapfEntity?) {
        if (result != null) {
            result.descripcionUnidades?.let {
                binding.spinnerUnity.selectValue(it)
                viewModel.setNroUnity(it)
            }

            result.descripcionPresentacion?.let {
                viewModel.setPresentation(it)
                binding.spinnerPresentation.selectValue(it)
            }

            binding.editTextUnityLote.setText(result.unidadesMedida.orZero().toString())
            viewModel.onUnityLoteChange(result.unidadesMedida.orZero().toString().toEditable())

            binding.editTextNeto.setText(result.contenidoNeto.orZero().toString())
            viewModel.onNetoChange(result.contenidoNeto.orZero().toString().toEditable())
        }
    }

    private fun onViewModelUnity(result: List<DinamicQuerysPapfEntity>) {
        val array = result.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerUnity.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerUnity.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setNroUnity(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelPresentation(result: List<DinamicQuerysPapfEntity>) {
        val array = result.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerPresentation.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerPresentation.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setPresentation(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun validateErrorData(
        unityId: Int?,
        presentationId: Int?,
        unityLote: String?,
        neto: String?,
    ) {
        binding.apply {
            validateSpinner(spinnerUnity, unityId)
            validateSpinner(spinnerPresentation, presentationId)
            validateField(textInputUnityLote, unityLote)
            validateField(textInputNeto, neto)
        }
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING,
        ) { }
        dialog?.dismiss()
    }
}