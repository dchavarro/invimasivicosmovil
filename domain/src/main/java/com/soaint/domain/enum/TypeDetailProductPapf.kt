package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.*

enum class TypeDetailProductPapf(val id: Array<String>) {
    typeArray(
        arrayOf(
            DESTINATARIO,
            LUGAR_DESTINO,
            OPERADOR_RESPONSABLE,
            PUERTO_CONTROL_FRONTERIZO,
            LICENCIA_IMPORTACION,
        )
    )
}