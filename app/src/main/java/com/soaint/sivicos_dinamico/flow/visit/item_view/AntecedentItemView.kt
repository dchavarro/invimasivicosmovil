package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.domain.model.AntecedentEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemAntecedentBinding
import com.xwray.groupie.databinding.BindableItem

class AntecedentItemView(
    private val antecedent: AntecedentEntity,
) : BindableItem<ItemAntecedentBinding>() {

    override fun bind(
        viewBinding: ItemAntecedentBinding,
        position: Int
    ) = with(viewBinding) {
        textViewTypeAntecedent.text = antecedent.descTipo
        textViewReferenceAntecedent.text = antecedent.referencia
        textViewNumber.text = antecedent.numeroAsociado
    }

    override fun getLayout() = R.layout.item_antecedent
}