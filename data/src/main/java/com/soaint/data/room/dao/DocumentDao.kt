package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.*
import com.soaint.domain.common.getDateHourWithOutFormat

@Dao
interface DocumentDao {

    //Documento
    @Query("SELECT * FROM document_table WHERE idVisita = :idVisita ")
    suspend fun selectDocument(idVisita: String): List<DocumentModel>

    @Query("SELECT * FROM document_table WHERE idPlantilla = :idPlantilla AND idVisita = :idVisita ORDER BY fechaDocumento DESC")
    suspend fun selectDocumentxPlantilla(idPlantilla: String, idVisita: String): List<DocumentModel>

    @Insert(entity = DocumentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDocument(documentModel: DocumentModel)

    @Query("DELETE FROM document_table WHERE idVisita = :idVisita")
    suspend fun deleteDocumentByIdVisita(idVisita: String)

    @Query("DELETE FROM document_table WHERE id = :id")
    suspend fun deleteDocument(id: Int)

    @Query("DELETE FROM document_table")
    suspend fun deleteAllDocument()

    @Query("DELETE FROM document_table WHERE idVisita = :idVisita")
    suspend fun deleteDocumentxVisit(idVisita: Int)

    @Update
    suspend fun updateDocument(documentModel: DocumentModel)

    @Transaction
    suspend fun insertOrUpdateDocument(documentModel: DocumentModel) {
        val items :List<DocumentModel> = selectDocumentxPlantilla(documentModel.idPlantilla.toString(), documentModel.idVisita.toString())
        if (items.isEmpty()) {
            val visita = selectVisita(documentModel.idVisita.orEmpty())
            if (visita != null) {
                val dateNow = getDateHourWithOutFormat()
                updateDateVisit(dateNow, visita.id.orZero())
            }
            insertDocument(documentModel)
        } else {
            documentModel.id = items.first().id
            updateDocument(documentModel)
        }
    }

    @Query("SELECT * FROM visit_table WHERE id = :idVisita")
    suspend fun selectVisita(idVisita: String): VisitModel?

    @Update(entity = VisitModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateVisit(visita: VisitModel)

    @Query("UPDATE visit_table SET fechaInicioEjecucion = :fechaInicioEjecucion WHERE id = :idVisita")
    suspend fun updateDateVisit(fechaInicioEjecucion: String, idVisita: Int)

    //Documento Name
    @Query("SELECT * FROM document_name_table WHERE idGrupo = :idGrupo")
    suspend fun selectDocumentName(idGrupo: Int): List<DocumentNameModel>

    @Insert(entity = DocumentNameModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDocumentName(documentNameModel: DocumentNameModel)

    @Query("DELETE FROM document_name_table WHERE idGrupo = :idGrupo")
    suspend fun deleteDocumentName(idGrupo: Int)

    //Document DATA
    @Query("SELECT * FROM document_body_table WHERE idVisita = :idVisita ORDER BY fechaCreacion")
    suspend fun selectAddDocument(idVisita: String): List<DocumentBodyModel>

    @Query("SELECT * FROM document_body_table WHERE id = :id ORDER BY fechaCreacion")
    suspend fun selectAddDocumentById(id: String): List<DocumentBodyModel>

    @Insert(entity = DocumentBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAddDocument(documentBodyModel: DocumentBodyModel) : Long

    @Query("DELETE FROM document_body_table WHERE id = :id")
    suspend fun deleteAddDocument(id: Int)

    @Query("SELECT COUNT(id) FROM document_body_table")
    suspend fun selectAllDocument(): Int

    //Document METADATA
    @Query("SELECT * FROM document_metadata_body_table WHERE idDocumentBody = :idDocumentBody ORDER BY fechaCreacionDocumento")
    suspend fun selectAddDocumentMetadata(idDocumentBody: Int): List<MetadataDocumentBodyModel>

    @Insert(entity = MetadataDocumentBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAddDocumentMetadata(metadataDocumentBodyModel: MetadataDocumentBodyModel?)

    @Query("DELETE FROM document_metadata_body_table WHERE idDocumentBody = :id")
    suspend fun deleteAddDocumentMetadata(id: Int)

/*    @Query("DELETE FROM document_metadata_body_table WHERE id IN (SELECT id FROM document_metadata_body_table WHERE idDocumentBody = :idDocumentBody ORDER BY fechaCreacionDocumento LIMIT 1)")
    suspend fun deleteAddDocumentMetadata(idDocumentBody: String)*/
}
