package com.soaint.data.repository

import com.soaint.data.api.*
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.DocumentDao
import com.soaint.data.room.dao.ManageMssDao
import com.soaint.data.room.dao.PapfDao
import com.soaint.data.room.dao.TasksPapfDao
import com.soaint.data.room.tables.CloseInspObservationPapfModel
import com.soaint.data.room.tables.EmitCisObservationPapfModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.domain.common.formatDate12Hour
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.common.getDateHourNow
import com.soaint.domain.common.getIp
import com.soaint.domain.common.orEmpty
import com.soaint.domain.enum.TypeDetailProductPapf
import com.soaint.domain.model.*
import com.soaint.domain.repository.PapfRepository
import com.soaint.domain.repository.UserRepository
import com.soaint.sivicos_dinamico.utils.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import retrofit2.HttpException
import javax.inject.Inject

class PapfRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val papfApi: PapfApi,
    private val crudApi: CrudApi,
    private val generalesPapfApi: GeneralesPapfApi,
    private val notificationEmailApi: NotificationEmailApi,
    private val genericsApi: GenericsApi,
    private val papfDao: PapfDao,
    private val tasksPapfDao: TasksPapfDao,
    private val manageMssDao: ManageMssDao,
    private val documentDao: DocumentDao,
    private val documentApi: DocumentApi,
) : PapfRepository, BaseRepository() {

    private val _infoTramit = MutableStateFlow<List<InfoTramitePapfEntity>?>(null)
    override val infoTramit get() = _infoTramit.asStateFlow()

    override val idTipoTramite get() = _infoTramit.value?.firstOrNull()?.idTipoTramite.orEmpty()

    override val idTipoProducto get() = _infoTramit.value?.firstOrNull()?.idTipoProducto.orEmpty()

    //Carga de Tareas
    override suspend fun loadInfoTramit(idSolicitud: Int): Result<List<InfoTramitePapfEntity>> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = papfApi.getInfoTramite(body).data.solicitud.orEmpty()
            _infoTramit.value = result.firstOrNull()?.let { listOf(it.mapToDomain()) }
            papfDao.deleteInfoTramitById(idSolicitud)
            result.onEach { papfDao.insertInfoTramit(it.mapToDb()) }
            Result.success(result.map { it.mapToDomain() })
        }


    //Carga de Tareas DB Local
    override suspend fun loadInfoTramitLocal(idSolicitud: Int): Result<List<InfoTramitePapfEntity>> =
        execute {
            val result = papfDao.selectInfoTramit(idSolicitud)
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga de facturas comerciales
    override suspend fun loadInvoice(idSolicitud: Int): Result<List<InvoicePapfEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val body = InfoTramitePapfBodyDto(
            getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
        )
        val result = crudApi.getInvoice(body).data.orEmpty()
        papfDao.deleteInvoiceById(idSolicitud)
        result.onEach { papfDao.insertInvoice(it.mapToDb()) }
        Result.success(result.map { it.mapToDomain() })
    }

    //Carga de facturas comerciales DB Local
    override suspend fun loadInvoiceLocal(idSolicitud: Int): Result<List<InvoicePapfEntity>> =
        execute {
            val result = papfDao.selectInvoice(idSolicitud)
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga de Transporte
    override suspend fun loadTransport(idSolicitud: Int): Result<TransportObjectPapfEntity?> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = crudApi.getTransport(body).data
            papfDao.deleteTransportComById(idSolicitud)
            papfDao.deleteTransportDocById(idSolicitud)
            result?.empresa?.onEach { papfDao.insertTransportCom(it.mapToDb(idSolicitud)) }
            result?.documentos?.onEach { papfDao.insertTransportDoc(it.mapToDb()) }
            Result.success(result?.mapToDomain())
        }

    //Carga de Transporte DB Local
    override suspend fun loadTransportLocal(idSolicitud: Int): Result<TransportObjectPapfEntity?> =
        execute {
            val document = papfDao.selectTransportDoc(idSolicitud).orEmpty()
            val company = papfDao.selectTransportCom(idSolicitud).orEmpty()
            Result.success(
                TransportObjectPapfEntity(company.map { it.mapToDomain() },
                    document.map { it.mapToDomain() })
            )
        }

    //Carga de registar producto
    override suspend fun loadRegisterProduct(idSolicitud: Int): Result<RegisterProductObjectPapfEntity?> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = generalesPapfApi.getProducts(body).data
            papfDao.deleteRegisterProductById(idSolicitud)
            papfDao.deleteRegisterProductDestinoById(idSolicitud)
            papfDao.deleteRegisterProductDocumentById(idSolicitud)
            result?.registroProductos?.onEach {
                papfDao.insertRegisterProduct(it.mapToDb())

                //consulta detail producto
                val bodyDetail = InfoTramitePapfBodyDto(
                    getIp(), usuario, ParametrosInfoTramitePapfDto(
                        idSolicitud.orEmpty().toString(),
                        it.idClasificacionProducto.orZero().toString(),
                        it.idProductoSolicitud.orEmpty().toString(),
                    )
                )
                loadInfoProduct(bodyDetail)

                //consulta info acta
                val bodyInfoActa = DataInfoActaPapfBodyDto(
                    getIp(), usuario, ParametrosDataInfoActaPapfDto(
                        it.idDetalleProducto.orEmpty(),
                    )
                )
                loadInfoActa(it.idSolicitud.orZero(), bodyInfoActa)
            }
            result?.destinoLoteSolicitud?.onEach {
                papfDao.insertRegisterProductDestino(
                    it.mapToDb(
                        idSolicitud
                    )
                )
            }
            result?.documentos?.onEach {
                papfDao.insertRegisterProductDocument(
                    it.mapToDb(
                        idSolicitud
                    )
                )
            }
            Result.success(result?.mapToDomain())
        }

    suspend fun loadInfoProduct(body: InfoTramitePapfBodyDto) {
        val idSolicitud = body.parametros.idSolicitud.intOrString()
        val idProducto = body.parametros.idProducto.intOrString()

        val result = generalesPapfApi.getDetailProducts(body).data
        deleteInfoProduct(idSolicitud, idProducto)

        result?.informacionProducto?.onEach {
            papfDao.insertDetailProduct(it.mapToDb())
            result.subpartida?.onEach { subPartida ->
                papfDao.insertSubPartidaProduct(
                    subPartida.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto
                    )
                )
            }
            result.expedidor?.onEach { expedidor ->
                papfDao.insertExpedidorProduct(
                    expedidor.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto
                    )
                )
            }
            result.fabricante?.onEach { fabricante ->
                papfDao.insertFabricanteProduct(
                    fabricante.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto
                    )
                )
            }
            result.informacionComplementaria?.onEach { info ->
                papfDao.insertInfoCompleProduct(
                    info.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto
                    )
                )
            }
            result.destinatario?.onEach { destinatario ->
                papfDao.insertDestinityProduct(
                    destinatario.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto,
                        DESTINATARIO
                    )
                )
            }
            result.lugarDestino?.onEach { lugarDestino ->
                papfDao.insertDestinityProduct(
                    lugarDestino.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto,
                        LUGAR_DESTINO
                    )
                )
            }
            result.operadorResponsable?.onEach { operadorResponsable ->
                papfDao.insertDestinityProduct(
                    operadorResponsable.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto,
                        OPERADOR_RESPONSABLE
                    )
                )
            }
            result.puertoControlFronterizo?.onEach { puertoControlFronterizo ->
                papfDao.insertDestinityProduct(
                    puertoControlFronterizo.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto,
                        PUERTO_CONTROL_FRONTERIZO
                    )
                )
            }
            result.licenciasImportacion?.onEach { licenciasImportacion ->
                papfDao.insertDestinityProduct(
                    licenciasImportacion.mapToDb(
                        it.idSolicitud,
                        it.idProductoSolicitud,
                        it.idClasificacionProducto,
                        LICENCIA_IMPORTACION
                    )
                )
            }
        }
    }

    suspend fun deleteInfoProduct(idSolicitud: Int, idProducto: Int) {
        papfDao.deleteDetailProductById(idSolicitud, idProducto)
        papfDao.deleteSubPartidaProductById(idSolicitud, idProducto)
        papfDao.deleteExpedidorProductById(idSolicitud, idProducto)
        papfDao.deleteFabricanteProductById(idSolicitud, idProducto)
        papfDao.deleteInfoCompleProductById(idSolicitud, idProducto)
        TypeDetailProductPapf.typeArray.id.onEach {
            papfDao.deleteDestinityProductById(idSolicitud, idProducto, it.toString())
        }
    }

    //Carga de registar producto DB Local
    override suspend fun loadRegisterProductLocal(idSolicitud: Int): Result<RegisterProductObjectPapfEntity?> =
        execute {
            val product = papfDao.selectRegisterProduct(idSolicitud).orEmpty()
            val destino = papfDao.selectRegisterProductDestino(idSolicitud).orEmpty()
            val document = papfDao.selectRegisterProductDocument(idSolicitud).orEmpty()

            Result.success(
                RegisterProductObjectPapfEntity(product.map { it.mapToDomain() },
                    destino.map { it.mapToDomain() },
                    document.map { it.mapToDomain() })
            )
        }

    //Carga de Certificado sanitario
    override suspend fun loadSanitaryCertificate(idSolicitud: Int): Result<List<SanitaryCertificatePapfEntity>> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = generalesPapfApi.getSanitaryCertificate(body).data.orEmpty()
            papfDao.deleteSanitaryCertificateById(idSolicitud)
            result.onEach { papfDao.insertSanitaryCertificate(it.mapToDb(idSolicitud)) }
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga de Certificado sanitario DB Local
    override suspend fun loadSanitaryCertificateLocal(idSolicitud: Int): Result<List<SanitaryCertificatePapfEntity>> =
        execute {
            val result = papfDao.selectSanitaryCertificate(idSolicitud)
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga Documentacion
    override suspend fun loadDocumentation(idSolicitud: Int): Result<List<DocumentationPapfEntity>> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = generalesPapfApi.getDocumentation(body).data.orEmpty()
            papfDao.deleteDocumentationPapfById(idSolicitud)
            result.onEach { papfDao.insertDocumentationPapf(it.mapToDb()) }
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga de Documentacion DB Local
    override suspend fun loadDocumentationLocal(idSolicitud: Int): Result<List<DocumentationPapfEntity>> =
        execute {
            val result = papfDao.selectDocumentationPapf(idSolicitud).orEmpty()
            Result.success(result.map { it.mapToDomain() })
        }

    //Carga Documentacion
    override suspend fun loadRegisterDate(idSolicitud: Int): Result<List<RegisterDateBodyEntity>?> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val result =
                genericsApi.getDateInspection(
                    BodyGenericValuesDto(
                        getIp(),
                        usuario,
                        CD_DATE_INSPECTION,
                        idSolicitudPapf = idSolicitud
                    )
                ).objectResponse
            papfDao.deleteRegisterDateById(idSolicitud)
            result?.onEach { papfDao.insertRegisterDate(it.mapToDb(false)) }
            Result.success(result?.map { it.mapToDomain() })
        }

    //Carga de Documentacion DB Local
    override suspend fun loadRegisterDateLocal(idSolicitud: Int): Result<RegisterDateBodyEntity?> =
        execute {
            val result = papfDao.selectRegisterDate(idSolicitud)
            Result.success(result?.mapToDomain())
        }

    override suspend fun getCountPapfLocal(): Result<Int?> = Result.success(
        //papfDao.selectCountRegisterDate() + papfDao.selectClasificationTramit()
        0
    )

    //Insert register date local
    override suspend fun saveRegisterDate(idSolicitud: Int): Result<Unit> = execute {
        val item = papfDao.selectRegisterDateLocal(idSolicitud)
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val result = if (item != null) {
            val body = RegisterDateBodyDto(
                getIp(), usuario, RegisterDateDto(
                    idSolicitud = item.idSolicitud.orZero().toString(),
                    fechaProgramada = item.fecha.orEmpty().formatDateHour(),
                    observaciones = item.observaciones.orEmpty(),
                )
            )
            generalesPapfApi.saveDateInspection(body)
        } else Unit
        Result.success(result)
    }

    //Insert register date local
    override suspend fun insertRegisterDateLocal(body: RegisterDateBodyEntity?): Result<Unit> =
        execute {
            val result =
                body?.mapToData()?.mapToDb(true)?.let { papfDao.insertRegisterDate(it) }
            Result.success(result)
        } as Result<Unit>

    //Carga Emails Notification
    override suspend fun loadEmailNotification(idSolicitud: Int): Result<List<InfoNotificationPapfEntity>> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            papfDao.deleteNotificationPapfById(idSolicitud)
            val result = generalesPapfApi.getInfoLegal(body).data?.informacionNotificacion.orEmpty()
            result.onEach { papfDao.insertNotificationPapf(it.mapToDb(idSolicitud)) }
            Result.success(result.map { it.mapToDomain() })
        }

    override suspend fun sendNotification(idSolicitud: Int): Result<Unit> = execute {
        val registerDate = papfDao.selectRegisterDateLocal(idSolicitud)
        val result = if (registerDate != null) {
            val infoTramite = papfDao.selectInfoTramit(idSolicitud).firstOrNull()
            val emails = papfDao.selectNotificationPapf(idSolicitud).orEmpty()
                .mapNotNull { it.correoElectronico + DOMAIN_ACUSE_RECIBIDO }

            val adjuntos = AdjuntosDto(
                nombre = "Inspecciónfísica.pdf", base64 = ""
            )
            val etiquetas = EtiquetasDto(
                operacioneSanitarias = "Operaciones sanitarias",
                papf = infoTramite?.nombreDependencia.orEmpty(),
                radicado = infoTramite?.radicado.orEmpty(),
                fechaInspeccion = registerDate.fecha.orEmpty().formatDate12Hour(),
                observaciones = registerDate.observaciones.orEmpty()
            )
            val body = NotificationPapfBodyDto(
                plantilla = "fechaInspeccionFisica.html",
                destinatarios = emails,
                copias = emptyList(),
                asunto = "(R ${infoTramite?.radicado.orEmpty()}) Fechas de Inspección fisica " + infoTramite?.radicado.orEmpty(),
                adjuntos = listOf(adjuntos),
                etiquetas = etiquetas,
            )

            notificationEmailApi.sendNotification(body)
            papfDao.deleteRegisterDateById(idSolicitud)
        } else Unit

        Result.success(result)
    }

    //Carga de Roles
    override suspend fun loadDinamicQuerys(codeQuery: String): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        papfDao.deleteAllTypePapfByCode(codeQuery)

        val body = BodyGenericValuesDto(getIp(), userName, codeQuery)
        val bodyImport =
            BodyGenericValuesDto(getIp(), userName, codeQuery, idTipoTramitePapf = ID_TYPE_IMPORTER)
        val bodyExport =
            BodyGenericValuesDto(getIp(), userName, codeQuery, idTipoTramitePapf = ID_TYPE_EXPORTER)

        when (codeQuery) {
            CD_EMPAQ -> {
                genericsApi.getEmpaques(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_CONCEPT -> {
                genericsApi.getConcept(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_UNITY -> {
                genericsApi.getUnity(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_PRESENTATION -> {
                genericsApi.getPresentation(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_MPIG -> {
                genericsApi.getMpig(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_RESULT_CIS -> {
                genericsApi.getResultCis(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_IDIOM -> {
                genericsApi.getIdiom(bodyExport).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(ID_TYPE_EXPORTER.toString(), codeQuery))
                }.orEmpty()
                genericsApi.getIdiom(bodyImport).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(ID_TYPE_IMPORTER.toString(), codeQuery))
                }.orEmpty()
            }
            CD_FIRMATE -> {
                genericsApi.getFirmante(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                    papfDao.insertSignatoriesPapf(it.mapSignatoriesToDb())
                }.orEmpty()
            }
            CD_OBSERVATION_PAPF -> {
                genericsApi.getObservationDefault(bodyExport).objectResponse?.onEach {
                    papfDao.insertTypePapf(
                        it.mapToDb(
                            ID_MISIONAL_PAPF_BEBIDAS.toString(),
                            codeQuery
                        )
                    )
                }.orEmpty()
                genericsApi.getObservationDefault(bodyImport).objectResponse?.onEach {
                    papfDao.insertTypePapf(
                        it.mapToDb(
                            ID_MISIONAL_PAPF_ALIMENTOS.toString(),
                            codeQuery
                        )
                    )
                }.orEmpty()
            }
            CD_REQUERIMIENTOS -> {
                genericsApi.getRequerimientos(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            CD_TYPE_CERTIFICATE -> {
                genericsApi.getTypeCertificate(body).objectResponse?.onEach {
                    papfDao.insertTypePapf(it.mapToDb(codeQuery))
                }.orEmpty()
            }
            YES_NO_MSS, YES_NO_REEMBARQUE, YES_NO_CERTIFICATE_EXPORTA -> {
                papfDao.insertTypePapf(YesNoDto(1, "SI").mapToDb(codeQuery))
                papfDao.insertTypePapf(YesNoDto(2, "NO").mapToDb(codeQuery))
            }
            else -> {
                Unit
            }
        }
        Result.success(Unit)
    }

    //Carga de Consultas dianamicas DB Local
    override suspend fun loadDinamicQuerysLocal(
        idTipoTramite: Int?
    ): Result<List<DinamicQuerysPapfEntity>?> {
        val result = if (idTipoTramite != null) {
            papfDao.selectTypePapf(idTipoTramite.toString())?.map { it.mapToDomain() }
        } else {
            papfDao.selectTypePapfByCode()?.map { it.mapToDomain() }
        }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    //Carga de Consultas dianamicas DB Local
    override suspend fun loadDinamicQuerysLocal(
        codeQuery: String
    ): Result<List<DinamicQuerysPapfEntity>?> {
        val result = papfDao.selectTypePapfByCode(codeQuery)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    //Carga DATA REQUERIDA INSPECCION SANITARIA DB Local
    override suspend fun loadDataReqInsSanLocal(
        idSolicitud: Int,
        detalleProducto: Int
    ): Result<DataReqInsSanPapfEntity?> = execute {
        val result = papfDao.selectProductInspSanitary(idSolicitud, detalleProducto)?.firstOrNull()
        Result.success(result?.mapToDomain())
    }

    //Carga DATA REQUERIDA INSPECCION SANITARIA
    override suspend fun saveDataReqInsSan(idSolicitud: Int): Result<Unit> = execute {
        val items = papfDao.selectProductInspSanitary(idSolicitud).orEmpty()
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()

        val result = if (!items.isNullOrEmpty()) {
            items.mapNotNull {
                val body = DataReqInsSanPapfBodyDto(
                    getIp(), usuario, ParametrosDataReqInsSanPapfDto(
                        it.condicionesAlmacenamiento.orEmpty(),
                        it.observaciones.orEmpty(),
                        it.idEstadoEmpaque.orEmpty(),
                        it.idConcepto.orEmpty(),
                        it.detalleProducto.orEmpty(),
                    )
                )
                generalesPapfApi.saveActaInspecc(body)
            }
        } else {
            Unit
        }
        Result.success(result)
    } as Result<Unit>

    //Insert register DATA REQUERIDA INSPECCION SANITARIA
    override suspend fun insertOrUpdateDataReqInsSanLocal(body: DataReqInsSanPapfEntity?): Result<Unit> =
        execute {
            val result =
                body?.mapToData()?.mapToDb()?.let { papfDao.insertOrUpdateProductInspSanitary(it) }
            Result.success(result)
        } as Result<Unit>

    //Carga DATA REQUERIDA ACTA TOMA DE MUESTRA DB Local
    override suspend fun loadDataReqActSampleLocal(
        idSolicitud: Int, idProducto: Int
    ): Result<DataReqActSamplePapfEntity?> = execute {
        val result = papfDao.selectProductActSample(idSolicitud, idProducto)?.firstOrNull()
        Result.success(result?.mapToDomain())
    }

    //Carga DATA REQUERIDA INSPECCION SANITARIA
    override suspend fun saveDataReqActSample(idSolicitud: Int): Result<Unit> = execute {
        val items = papfDao.selectProductActSample(idSolicitud).orEmpty()
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()

        val result = if (!items.isNullOrEmpty()) {
            items.mapNotNull {
                val body = DataReqActSamplePapfBodyDto(
                    getIp(), usuario, ParametrosDataReqActSamplePapfDto(
                        it.nroUnidades.orEmpty(),
                        it.unidadesMedida.orEmpty(),
                        it.presentacion.orEmpty(),
                        it.contenidoNeto.orEmpty(),
                        it.idProducto.orEmpty(),
                    )
                )
                generalesPapfApi.saveActaSample(body)
            }
        } else {
            Unit
        }
        Result.success(result)
    } as Result<Unit>

    //Insert register DATA REQUERIDA INSPECCION SANITARIA
    override suspend fun insertOrUpdateDataReqActSampleLocal(body: DataReqActSamplePapfEntity?): Result<Unit> =
        execute {
            val result =
                body?.mapToData()?.mapToDb()?.let { papfDao.insertOrUpdateProductActSample(it) }
            Result.success(result)
        } as Result<Unit>


    //Carga DATA Basica cierre de inspeccion DB Local
    override suspend fun loadDataCloseIns(idSolicitud: Int): Result<ClasificationTramitPapfEntity?> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result =
                generalesPapfApi.getDataBasic(body).data?.firstOrNull()?.clasificacionTramite
            papfDao.deleteClasificationTramitById(idSolicitud)
            result?.mapToDb(idSolicitud)?.let { papfDao.insertClasificationTramit(it) }
            Result.success(result?.mapToDomain())
        }

    //Carga de facturas comerciales DB Local
    override suspend fun loadDataCloseInsLocal(idSolicitud: Int): Result<ClasificationTramitPapfEntity?> =
        execute {
            val result = papfDao.selectAllClasificationTramit(idSolicitud)
            Result.success(result?.mapToDomain())
        }

    //Insert cierre de inspeccion fisica
    override suspend fun insertOrUpdateCloseInsFisLocal(body: ClasificationTramitPapfEntity?): Result<Unit> =
        execute {
            val result =
                body?.mapToModel()?.let { papfDao.insertOrUpdateCloseInsFisLocal(it) }
            Result.success(result)
        } as Result<Unit>

    //Carga Cierre Inspeccion
    override suspend fun saveCloseInspection(idSolicitud: Int): Result<Unit> {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        // cierre solicitud
        val data = papfDao.selectClasificationTramit(idSolicitud)
        val idFuncionario = papfDao.getIdUsuarioFirmante(data?.idFirmante)?.idFuncionario
        val task =  tasksPapfDao.selectTaskById(idSolicitud)

        if (data != null) {
            val observation =
                papfDao.selectCloseInsObservationPapfBySolicitud(idSolicitud)?.mapNotNull {
                    ParametrosDescripcionCloseInsPapfDto(it.descripcion)
                }

            val body = CloseInspPapfBodyDto(
                getIp(), usuario, ParametrosCloseInsPapfDto(
                    idSolicitud = idSolicitud.orEmpty().toString(),
                    respuestaRequerimientoIF = data.respuestaRequerimientoIF,
                    justificacion = data.justificacion,
                    idUsoMPIG = data.idUsoMPIG,
                    idResultadoCIS = data.idResultadoCIS,
                    idFirmante = data.idFirmante,
                    idIdioma = data.idIdioma,
                    idActividad = data.idActividad,
                    idTipoProducto = data.idTipoProductoPapf,
                    observaciones = observation
                )
            )
            generalesPapfApi.saveCloseInsp(body)

            // Actualizacion estado
            val bodyUpdateStatus = UpdateStatusPapfBodyDto(
                getIp(), usuario, ParametrosUpdateStatusPapfDto(
                    estado = if (data.respuestaRequerimientoIF == true) 7 else 10,
                    solicitud = SolicitudPapfDto(idSolicitud.orZero()),
                )
            )
            if (task != null) {
                val assignFunctionary = AssignFunctionaryBodyDto(
                    getIp(), usuario, ParametrosAssignFunctionaryDto(
                        idFuncionario = idFuncionario,
                        fecha = getDateHourNow(),
                        Solicitud = arrayOf(SolicitudAssignDto(
                            idSolicitud = idSolicitud.orEmpty(),
                            idProgramacion = task.idProgramacion
                        ))
                    )
                )
                generalesPapfApi.assignFunctionary(assignFunctionary)
            }

            generalesPapfApi.udpateStatusRequest(bodyUpdateStatus)

            // Notificacion Correo resultado de la solicitud
            sendNotificationClose(idSolicitud)
            papfDao.deleteCloseInsObservationPapfBySolicitud(idSolicitud)
        }
        return Result.success(Unit)
    }

    //Carga de Documentacion DB Local
    override suspend fun loadObservationCloseIns(idSolicitud: Int): Result<List<CloseInspObservationPapfEntity>?> =
        execute {
            var observations: ArrayList<CloseInspObservationPapfEntity> = arrayListOf()
            papfDao.selectCloseInsObservationPapfBySolicitud(idSolicitud)?.mapNotNull {
                observations.add(
                    CloseInspObservationPapfEntity(
                        it.descripcion.orEmpty(),
                        it.idSolicitud.orEmpty(),
                        it.id.orEmpty()
                    )
                )
            }
            Result.success(observations)
        }

    //Insert observacion inspeccion fisica
    override suspend fun insertOrUpdateObservationLocal(
        observation: CloseInspObservationPapfEntity?,
    ): Result<Unit> =
        execute {
            val result = papfDao.insertOrUpdateObservationLocal(
                CloseInspObservationPapfModel(
                    descripcion = observation?.descripcion,
                    idSolicitud = observation?.idSolicitud,
                    id = observation?.id
                )
            )
            Result.success(result)
        }

    override suspend fun deleteObservationLocal(
        observation: CloseInspObservationPapfEntity?,
    ): Result<Unit> = execute {
        val result =
            papfDao.deleteCloseInsObservationPapf(observation?.idSolicitud, observation?.id)
        Result.success(result)
    }

    private suspend fun sendNotificationClose(idSolicitud: Int): Result<Unit> {
        val userName = userRepository.userInformation.value?.usuario.orEmpty()
        val docs = documentDao.selectDocument(idSolicitud.orZero().toString())

        val adjuntos = docs.mapNotNull { doc ->
            val bodyDocs = DocsActsPapfBodyDto(
                getIp(), userName, ParametrosDocsActsPapfDto(
                    idSolicitud.orEmpty().toString(),
                    doc.idTipoDocumental.orZero().toString()
                )
            )
            val base64: List<String>? = try {
                generalesPapfApi.getDocsActs(bodyDocs).objectResponse?.mapNotNull { dataDoc ->
                    try {
                        val base64 = documentApi.viewDocument(dataDoc.idDocumento.orZero()).objectResponse.orEmpty()
                        base64
                    } catch (e: HttpException) {
                        null // Retorna null en caso de error para evitar que se bloquee la aplicación
                    }
                }
            } catch (e: HttpException) {
                null // Retorna null en caso de error para evitar que se bloquee la aplicación
            }


            base64?.mapNotNull {
                AdjuntosDto(
                    nombre = doc.codigoPlantilla + doc.extension,
                    base64 = it
                )
            }
        }.flatten()

        val infoTramite = papfDao.selectInfoTramit(idSolicitud).firstOrNull()
        val emails = papfDao.selectNotificationPapf(idSolicitud).orEmpty()
            .mapNotNull { it.correoElectronico + DOMAIN_ACUSE_RECIBIDO }

        val etiquetas = EtiquetasDto(
            operacioneSanitarias = "Operaciones sanitarias",
            papf = infoTramite?.nombreDependencia.orEmpty(),
            linkTramite = "https://invima.gov.co/"
        )
        val body = NotificationPapfBodyDto(
            plantilla = "resultadoInspeccionFisica.html",
            destinatarios = emails,
            copias = emptyList(),
            asunto = "(R ${infoTramite?.radicado.orEmpty()}) Resultado inspección fisica " + infoTramite?.radicado.orEmpty(),
            adjuntos = adjuntos,
            etiquetas = etiquetas,
        )

        val result = notificationEmailApi.sendNotification(body)
        papfDao.deleteRegisterDateById(idSolicitud)
        documentDao.deleteDocumentByIdVisita(idSolicitud.orZero().toString())

        return Result.success(result)
    }


    //Carga DATA Basica emitir cis
    override suspend fun loadDataEmitCis(idSolicitud: Int): Result<EmitCisPapfEntity?> =
        execute {
            val usuario = userRepository.userInformation.value?.usuario.orEmpty()
            val body = InfoTramitePapfBodyDto(
                getIp(), usuario, ParametrosInfoTramitePapfDto(idSolicitud.orEmpty().toString())
            )
            val result = generalesPapfApi.getDataEmitCis(body).data
            papfDao.deleteEmitCisById(idSolicitud)
            papfDao.deleteObservationEmitCisById(idSolicitud)

            result?.cierre?.firstOrNull()?.let { papfDao.insertEmitCisPapf(it.mapToDb()) }
            result?.observaciones?.mapNotNull {
                papfDao.insertObservationEmitCisPapf(it.mapToDb(idSolicitud, true))
            }
            Result.success(result?.mapToDomain())
        }

    //Carga de emitir cis DB Local
    override suspend fun loadDataEmitCisLocal(idSolicitud: Int): Result<EmitCisClosePapfEntity?> =
        execute {
            val emit = papfDao.selectAllEmitCis(idSolicitud)
            Result.success(emit?.mapToDomain())
        }

    override suspend fun loadObservationEmitCisLocal(idSolicitud: Int): Result<List<EmitCisObservationPapfEntity>?> =
        execute {
            val observation = papfDao.selectAllObservationEmitCis(idSolicitud).orEmpty()
            Result.success(observation.map { it.mapToDomain() })
        }

    //Insert observacion inspeccion fisica
    override suspend fun insertOrUpdateObservationEmitCisLocal(
        observation: EmitCisObservationPapfEntity?,
    ): Result<Unit> =
        execute {
            val result = papfDao.insertOrUpdateObservationEmitCisLocal(
                EmitCisObservationPapfModel(
                    descripcion = observation?.descripcion,
                    idSolicitud = observation?.idSolicitud,
                    id = observation?.id
                )
            )
            Result.success(result)
        }

    override suspend fun deleteObservationEmitCisLocal(
        observation: EmitCisObservationPapfEntity?,
    ): Result<Unit> = execute {
        val result = papfDao.deleteObservationEmitCisById(observation?.idSolicitud, observation?.id)
        Result.success(result)
    }

    //Carga DATA Info emitir acta
    override suspend fun loadInfoEmitir(idSolicitud: Int): Result<InfoEmitirPapfEntity?> =
        execute {
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val body = BodyGenericValuesDto(
                getIp(),
                userName,
                CD_INFO_ACTAS_EMITIR,
                idSolicitudPapf = idSolicitud
            )
            val result = genericsApi.getInfoEmitir(body).objectResponse?.firstOrNull()
            result?.mapToDb(idSolicitud)?.let {
                papfDao.deleteAllInfoEmitir(idSolicitud)
                papfDao.insertInfoEmitir(it)
            }
            Result.success(result?.mapToDomain())
        }

    //Carga DATA Info emitir acta DB Local
    override suspend fun loadInfoEmitirLocal(idSolicitud: Int): Result<InfoEmitirPapfEntity?> =
        execute {
            val result = papfDao.selectInfoEmitir(idSolicitud)
            Result.success(result?.mapToDomain())
        }


    //Carga DATA Info emitir acta
    override suspend fun loadInfoCheck(idSolicitud: Int): Result<CheckListEntity?> =
        execute {
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val body = SolicitudPapfBodyDto(getIp(), userName, idSolicitud)
            val result = generalesPapfApi.getCheckList(body).objectResponse?.firstOrNull()
            result?.mapToDb()?.let {
                papfDao.deleteAllChecklist(idSolicitud)
                papfDao.insertChecklist(it)
            }
            Result.success(result?.mapToDomain())
        }

    //Carga DATA Info emitir acta DB Local
    override suspend fun loadInfoCheckLocal(idSolicitud: Int): Result<CheckListEntity?> =
        execute {
            val result = papfDao.selectChecklist(idSolicitud)
            Result.success(result?.mapToDomain())
        }

    //Carga DATA Info emitir acta
    override suspend fun searchPlantillaEmit(body: SearchPlantillaEmitEntity): Result<List<SearchPlantillaEmitPapfEntity>?> =
        execute {
            val userName = userRepository.userInformation.value?.usuario.orEmpty()
            val newBody = SearchPlantillaEmitPapfBodyDto(getIp(), userName, body.mapToData())
            val result = generalesPapfApi.searchPlantillaEmit(newBody).objectResponse
            Result.success(result?.map { it.mapToDomain() })
        }

    //Carga Guardar emitirCis
    override suspend fun saveEmit(idSolicitud: Int): Result<Unit> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        // cierre solicitud
        val data = papfDao.selectAllEmitCis(idSolicitud)
        val observation =
            papfDao.selectAllObservationEmitCis(idSolicitud)?.mapNotNull {
                ParametrosDescripcionCloseInsPapfDto(it.descripcion)
            }
        val lab = papfDao.selectInfoEmitir(idSolicitud)?.nombreLaboratorio
        if (data != null) {
            val body = CloseInspPapfBodyDto(
                getIp(), usuario, ParametrosCloseInsPapfDto(
                    idSolicitud = idSolicitud.orEmpty().toString(),
                    idUsoMPIG = data.idUsoMPIG,
                    idResultadoCIS = data.idResultadoCIS,
                    idFirmante = data.idFirmante,
                    idIdioma = data.idIdioma,
                    idTipoCertificado = data.idTipoCertificado,
                    nombreLaboratorio = lab,
                    reembarque = data.descripcionReembarque,
                    medidaSanitaria = data.descripcionMSS,
                    observaciones = observation
                )
            )
            generalesPapfApi.saveCloseInsp(body)
            papfDao.deleteObservationEmitCisById(idSolicitud)
        }
        Result.success(Unit)
    }

    //Insert cierre de inspeccion fisica
    override suspend fun insertOrUpdateEmitLocal(body: EmitCisClosePapfEntity?): Result<Unit> =
        execute {
            val result =
                body?.let { papfDao.insertOrUpdateEmitLocal(it.mapToModel()) }
            Result.success(result)
        } as Result<Unit>

    //Carga de detalle de producto DB Local
    override suspend fun loadDetailProductLocal(
        idSolicitud: Int,
        idProductoSolicitud: Int,
        idClasificacionProducto: Int
    ): Result<DetailProductObjectPapfEntity?> =
        execute {
            val detail = papfDao.selectDetailProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto
            )
            val subpartida = papfDao.selectSubPartidaProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto
            )
            val expedidor = papfDao.selectExpedidorProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto
            )
            val fabricante = papfDao.selectFabricanteProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto
            )
            val informacionComplementaria = papfDao.selectInfoCompleProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto
            )
            val destinatario = papfDao.selectDestinityProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto,
                DESTINATARIO
            )
            val lugarDestino = papfDao.selectDestinityProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto,
                LUGAR_DESTINO
            )
            val operadorResponsable = papfDao.selectDestinityProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto,
                OPERADOR_RESPONSABLE
            )
            val puertoControlFronterizo = papfDao.selectDestinityProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto,
                PUERTO_CONTROL_FRONTERIZO
            )
            val licenciasImportacion = papfDao.selectDestinityProduct(
                idSolicitud,
                idProductoSolicitud,
                idClasificacionProducto,
                LICENCIA_IMPORTACION
            )
            val body = DetailProductObjectPapfEntity(
                detail?.map { it.mapToDomain() },
                subpartida?.map { it.mapToDomain() },
                expedidor?.map { it.mapToDomain() },
                fabricante?.map { it.mapToDomain() },
                informacionComplementaria?.map { it.mapToDomain() },
                destinatario?.map { it.mapToDomain() },
                lugarDestino?.map { it.mapToDomain() },
                operadorResponsable?.map { it.mapToDomain() },
                puertoControlFronterizo?.map { it.mapToDomain() },
                licenciasImportacion?.map { it.mapToDomain() },
            )
            Result.success(body)
        }

    //Carga Documentacion
    suspend fun loadInfoActa(idSolicitud: Int, body: DataInfoActaPapfBodyDto) {
        val idDetalleProducto = body.parametros.idDetalleProducto

        val result = generalesPapfApi.getInfoActa(body).data
        papfDao.deleteProductInspSanitary(idSolicitud, idDetalleProducto)
        papfDao.deleteProductActSample(idSolicitud, idDetalleProducto)
        if (result?.actaInspeccion == null) {
            insertOrUpdateDataReqInsSanLocal(
                DataReqInsSanPapfEntity(
                    idConcepto = 1,
                    descripcionConcepto = "Aprobado",
                    detalleProducto = idDetalleProducto,
                    idSolicitud = idSolicitud
                )
            )
        } else {
            result.actaInspeccion.onEach {
                insertOrUpdateDataReqInsSanLocal(
                    DataReqInsSanPapfEntity(
                        idEstadoEmpaque = it.idEstadoEmp,
                        descripcionEmpaque = it.descripcionEstadoEmpaque,
                        idConcepto = it.idConcepto,
                        descripcionConcepto = it.descripcionConcepto,
                        condicionesAlmacenamiento = it.condActaInspeccion,
                        observaciones = it.obsActaInspeccion,
                        detalleProducto = idDetalleProducto,
                        idSolicitud = it.idSolicitud,
                    )
                )
            }
        }
        result?.actaTomaMuestra?.onEach {
            insertOrUpdateDataReqActSampleLocal(
                DataReqActSamplePapfEntity(
                    nroUnidades = it.unidadesMuestraLote,
                    descripcionUnidades = it.descripcionUnidades,
                    unidadesMedida = it.unidadesMedida,
                    presentacion = it.idPresentacion,
                    descripcionPresentacion = it.descripcionPresentacion,
                    contenidoNeto = it.contenidoNeto,
                    idProducto = it.idProductoSolicitud,
                    idSolicitud = it.idSolicitud
                )
            )
        }
    }

    override suspend fun updateRequerimientoIF(idSolicitud: Int?, respuesta: Boolean?){
        papfDao.updateRespuestaRequerimientoIF(idSolicitud,respuesta)
    }
    override suspend fun selectProductInspSanitary(idSolicitud: Int): Result<List<DataReqInsSanPapfEntity>?> = execute {
        val result = papfDao.selectProductInspSanitary(idSolicitud)?.map { it.mapToDomain() }
        Result.success(result)
    }

    override suspend fun selectProductActSample(idSolicitud: Int): Result<List<DataReqActSamplePapfEntity>?> = execute {
        val result = papfDao.selectProductActSample(idSolicitud)?.map { it.mapToDomain() }
        Result.success(result)
    }

    override suspend fun hasReachedMaximumObservationsInspe(idSolicitud: Int): Boolean {
        return papfDao.hasReachedMaximumObservationsInspe(idSolicitud) >= 5
    }

    override suspend fun hasReachedMaximumObservationsEmitirCis(idSolicitud: Int): Boolean {
        return papfDao.hasReachedMaximumObservationsEmitirCis(idSolicitud) >= 10
    }

    override suspend fun loadConsecutivePapf(codigo: String?, idSolicitud: Int?): Result<ConsecutivePapfEntity?> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val body = GenerateConsecutivePapfBodyDto(
            getIp(), usuario, GenerateConsecutivePapfDto(codigo.orEmpty(), "0")
        )
        val result = generalesPapfApi.generateConsecutive(body).objectResponse
        result?.let { papfDao.deleteConsecutivePapfById(idSolicitud)}
        result?.let { papfDao.insertConsecutivePapf(result.mapToDb(idSolicitud)) }
        Result.success(result?.mapToDomain())
    }

    override suspend fun saveActsRequest(data: SaveInfoActsPapfEntity): Result<Unit> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val body = SaveInfoActsPapfBodyDto(
            getIp(), usuario, data.mapToData()
        )
        generalesPapfApi.saveActsRequest(body)
        Result.success(Unit)
    }

    override suspend fun selectConsecutivePapfById(idSolicitud: Int): Result<String> = execute {
      val result= papfDao.selectConsecutivePapfById(idSolicitud)?.consecutivo
        Result.success(result.toString())
    }

}