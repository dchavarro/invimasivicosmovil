package com.soaint.domain.provider

import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow

interface LocationProvider {

    suspend fun startLocationUpdates(): Flow<LatLng>

    fun stopLocationUpdates()

    suspend fun checkGpsStatus(): Boolean

    suspend fun getLocation(): LatLng?
}