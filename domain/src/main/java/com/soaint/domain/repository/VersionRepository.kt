package com.soaint.domain.repository

interface VersionRepository {

    suspend fun validateVersionCode(versionCode: String): Result<Boolean>
}