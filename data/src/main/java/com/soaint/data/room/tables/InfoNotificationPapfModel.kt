package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.InfoNotificationPapfEntity

@Entity(tableName = "info_notification_papf_table")
class InfoNotificationPapfModel(
    @ColumnInfo(name = "tipoNotificacion") val tipoNotificacion: String?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun InfoNotificationPapfModel.mapToDomain() = InfoNotificationPapfEntity(
    tipoNotificacion.orEmpty(),
    direccion.orEmpty(),
    correoElectronico.orEmpty(),
)