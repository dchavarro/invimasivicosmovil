package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.RegisterDateModel
import com.soaint.domain.model.RegisterDateBodyEntity

data class RegisterDateCDResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<RegisterDateCDDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class RegisterDateCDDto(
    @SerializedName("FechaProgramada") val fechaProgramada: String?,
    @SerializedName("Observaciones") val observaciones: String?,
    @SerializedName("IdSolicitud") val idSolicitud: Int,
)

fun RegisterDateCDDto.mapToDomain() = RegisterDateBodyEntity(
    fecha = fechaProgramada,
    observaciones = observaciones,
    idSolicitud = idSolicitud
)

fun RegisterDateCDDto.mapToDb(isLocal: Boolean) = RegisterDateModel(
    fecha = fechaProgramada,
    observaciones = observaciones,
    isLocal = isLocal,
    idSolicitud = idSolicitud
)

fun RegisterDateBodyEntity.mapToData() = RegisterDateCDDto(
    fecha,
    observaciones,
    idSolicitud
)

data class RegisterDateBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: RegisterDateDto,
) {
    constructor(ip: String, user: String, parametros: RegisterDateDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class RegisterDateDto(
    @SerializedName("idSolicitud") val idSolicitud: String,
    @SerializedName("fechaProgramada") val fechaProgramada: String,
    @SerializedName("observaciones") val observaciones: String,
)