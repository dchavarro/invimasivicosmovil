package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.SearchPlantillaEmitEntity
import com.soaint.domain.model.SearchPlantillaEmitPapfEntity

data class SearchPlantillaEmitPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: SearchPlantillaEmitDto,
) {
    constructor(ip: String, user: String, parametros: SearchPlantillaEmitDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class SearchPlantillaEmitDto(
    @SerializedName("IdActividad") val idActividad: Int? = 0,
    @SerializedName("IdTipoCertificado") val idTipoCertificado: Int? = 0,
    @SerializedName("IdResultadoCis") val idResultadoCis: Int? = 0,
    @SerializedName("Reembarque") val reembarque: Int? = 0,
    @SerializedName("MSS") val mss: Int? = 0,
    @SerializedName("IdIdioma") val idIdioma: Int? = 0,
)

fun SearchPlantillaEmitEntity.mapToData() = SearchPlantillaEmitDto(
    idActividad,
    idTipoCertificado,
    idResultadoCis,
    reembarque,
    mss,
    idIdioma,
)


data class SearchPlantillaEmitPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val objectResponse: List<SearchPlantillaEmitPapfDto>? = null,
    @SerializedName("statusCode") val statusCode: Int,
)

data class SearchPlantillaEmitPapfDto(
    @SerializedName("CodigoPlantilla") val codigoPlantilla: String? = null,
    @SerializedName("Estado") val estado: Int? = null,
)

fun SearchPlantillaEmitPapfDto.mapToDomain() = SearchPlantillaEmitPapfEntity(
    codigoPlantilla,
    estado
)