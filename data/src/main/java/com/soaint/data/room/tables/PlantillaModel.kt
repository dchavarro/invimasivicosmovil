package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.data.model.mapToDomain
import com.soaint.domain.model.*

@Entity(tableName = "plantilla_table")
class PlantillaModel(
    @PrimaryKey val id: Int?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "activo") val activo: Boolean?,
    @ColumnInfo(name = "codigo") val codigo: String?,
    @ColumnInfo(name = "descEstado") val descEstado: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "idEstado") val idEstado: Int?,
    @ColumnInfo(name = "plantillaOriginal") val plantillaOriginal: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "version") val version: Int?,
    @ColumnInfo(name = "idTipoDocumental") val idTipoDocumental: Int?,
)

fun PlantillaModel.mapToDomain(misionales: List<MisionalEntity>?) = PlantillaEntity(
    activo,
    codigo,
    descEstado,
    descripcion,
    fechaCreacion,
    fechaModifica,
    id,
    idEstado,
    misionales,
    nombre,
    plantillaOriginal,
    usuarioCrea.orEmpty(),
    usuarioModifica,
    version,
    idTipoDocumental
)