package com.soaint.sivicos_dinamico.flow.visit.item_view

import com.soaint.domain.model.PersonCompanyBodyEntity
import com.soaint.domain.model.VisitCompanyPersonEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemPersonBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class PersonItemView(
    private val person: VisitCompanyPersonEntity,
    private val onDelete: (VisitCompanyPersonEntity) -> Unit
) : BindableItem<ItemPersonBinding>() {

    override fun bind(
        viewBinding: ItemPersonBinding,
        position: Int
    ) = with(viewBinding) {
        textViewTypeReq.text =
            "${person.primerNombre} ${person.segundoNombre} ${person.primerApellido} ${person.segundoApellido}"
        textViewCritic.text = person.tipoDocumento
        textViewNumber.text = person.numeroDocumentoPersona
        textViewRol.text = person.descripcionRolPersona
        textViewEmail.text = person.correoElectronicoP

        buttonDelete.setOnSingleClickListener {
            onDelete.invoke(person)
        }
    }

    override fun getLayout() = R.layout.item_person
}