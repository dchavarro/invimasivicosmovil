package com.soaint.data.di

import android.content.Context
import android.os.Bundle
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okio.Buffer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.net.HttpURLConnection
import java.util.concurrent.TimeUnit.SECONDS
import javax.inject.Named
import javax.inject.Singleton

private const val CONTENT_TYPE = "Content-Type"
private const val APPLICATION_JSON = "application/json"

const val MOCK_RETROFIT = "MOCK_RETROFIT"
const val DUMMY_RETROFIT = "DUMMY_RETROFIT"
const val TRANSVERSAL_RETROFIT = "TRANSVERSAL_RETROFIT"
const val PARAMETRICAS_RETROFIT = "PARAMETRICAS_RETROFIT"
const val PROXY_RETROFIT = "PROXY_RETROFIT"
const val GENERALES_RETROFIT = "GENERALES_RETROFIT"
const val AUTH_RETROFIT = "AUTH_RETROFIT"
const val VISIT_RETROFIT = "VISIT_RETROFIT"
const val LOAD_RETROFIT = "LOAD_RETROFIT"
const val MASTER_RETROFIT = "MASTER_RETROFIT"
const val DOCUMENT_RETROFIT = "DOCUMENT_RETROFIT"
const val GENERICS_RETROFIT = "GENERICS_RETROFIT"
const val SANITARY_RETROFIT = "SANITARY_RETROFIT"
const val REGISTER_SANITARY_RETROFIT = "REGISTER_SANITARY_RETROFIT"
const val MODELO_IVC_RETROFIT = "MODELO_IVC_RETROFIT"
const val COMPANY_RETROFIT = "COMPANY_RETROFIT"
const val NOTIFICATION_RETROFIT = "NOTIFICATION_RETROFIT"
const val ACCESS_DATA_RETROFIT = "ACCESS_DATA_RETROFIT"
const val PROCEDURE_RETROFIT = "PROCEDURE_RETROFIT"
const val INPER_RETROFIT = "INPER_RETROFIT"
const val MASTER_TRANS_RETROFIT = "MASTER_TRANS_RETROFIT"
const val PLANNING_RETROFIT = "PLANNING_RETROFIT"
const val INTEGRATIONS_RETROFIT = "INTEGRATIONS_RETROFIT"
const val GENERALES_PAPF_RETROFIT = "GENERALES_PAPF_RETROFIT"
const val PAPF_RETROFIT = "PAPF_RETROFIT"
const val CRUD_RETROFIT = "CRUD_RETROFIT"
const val NOTIFICATION_EMAIL_RETROFIT = "NOTIFICATION_EMAIL_RETROFIT"
const val FIRMA_RETROFIT = "FIRMA_RETROFIT"

// Error API
const val EVENT_A_ERROR_API = "EVENT_A_ERROR_API"
const val KEY_METHOD = "KEY_METHOD"
const val KEY_URL = "KEY_URL"
const val KEY_HEADER = "KEY_HEADER"
const val KEY_BODY = "KEY_BODY"
const val KEY_RESPONSE = "KEY_RESPONSE"

@Module
class AppNetworkModule {

    @Provides
    @Singleton
    @Named(MOCK_RETROFIT)
    fun provideMockfit(
        gson: Gson,
        @BaseMockPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(DUMMY_RETROFIT)
    fun provideDummyfit(
        gson: Gson,
        @BaseDummyPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(AUTH_RETROFIT)
    fun provideRetrofit(
        gson: Gson,
        @BaseAuthPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(TRANSVERSAL_RETROFIT)
    fun provideInformationRetrofit(
        gson: Gson,
        @BaseTransversalPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(PARAMETRICAS_RETROFIT)
    fun provideParametricasRetrofit(
        gson: Gson,
        @BaseParametricasPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(PROXY_RETROFIT)
    fun provideProxyRetrofit(
        gson: Gson,
        @BaseProxyPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(GENERALES_RETROFIT)
    fun provideGeneralesRetrofit(
        gson: Gson,
        @BaseGeneralesPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(VISIT_RETROFIT)
    fun provideVisitRetrofit(
        gson: Gson,
        @BaseVisitPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(LOAD_RETROFIT)
    fun provideLoadRetrofit(
        gson: Gson,
        @BaseLoadPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(MASTER_RETROFIT)
    fun provideMasterRetrofit(
        gson: Gson,
        @BaseMasterPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(DOCUMENT_RETROFIT)
    fun provideDocumentRetrofit(
        gson: Gson,
        @BaseDocumentPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(GENERICS_RETROFIT)
    fun provideGenericsRetrofit(
        gson: Gson,
        @BaseGenericsPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(SANITARY_RETROFIT)
    fun provideSanitaryRetrofit(
        gson: Gson,
        @BaseSanitaryPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(REGISTER_SANITARY_RETROFIT)
    fun provideRegisterSanitaryRetrofit(
        gson: Gson,
        @BaseRegisterSanitaryPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(MODELO_IVC_RETROFIT)
    fun provideModeloIvcRetrofit(
        gson: Gson,
        @BaseModeloIvcPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(COMPANY_RETROFIT)
    fun provideCompanyRetrofit(
        gson: Gson,
        @BaseCompanyPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(NOTIFICATION_RETROFIT)
    fun provideNotificationRetrofit(
        gson: Gson,
        @BaseNotificationPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(ACCESS_DATA_RETROFIT)
    fun provideAccessDataRetrofit(
        gson: Gson,
        @BaseAccessDataPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(PROCEDURE_RETROFIT)
    fun provideProcedureRetrofit(
        gson: Gson,
        @BaseProcedurePath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(INPER_RETROFIT)
    fun provideInperRetrofit(
        gson: Gson,
        @BaseInperPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(MASTER_TRANS_RETROFIT)
    fun provideMasterTransRetrofit(
        gson: Gson,
        @BaseMasterTransPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(PLANNING_RETROFIT)
    fun providePlanningRetrofit(
        gson: Gson,
        @BasePlanningPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(INTEGRATIONS_RETROFIT)
    fun provideIntegrationsRetrofit(
        gson: Gson,
        @BaseIntegrationsPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(GENERALES_PAPF_RETROFIT)
    fun provideGeneralesPapfRetrofit(
        gson: Gson,
        @BaseGeneralesPapfPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(PAPF_RETROFIT)
    fun providePapfRetrofit(
        gson: Gson,
        @BasePapfPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(CRUD_RETROFIT)
    fun provideCrudRetrofit(
        gson: Gson,
        @BaseCrudPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(NOTIFICATION_EMAIL_RETROFIT)
    fun provideNotificationEmailRetrofit(
        gson: Gson,
        @BaseNotificacionEmailPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @Singleton
    @Named(FIRMA_RETROFIT)
    fun provideFirmaRetrofit(
        gson: Gson,
        @BaseFirmaPath urlBase: String,
        @LoggerInterceptors loggerInterceptors: Interceptor?,
        @ErrorInterceptors errorInterceptors: Interceptor?,
    ): Retrofit =
        getRetrofit(gson, urlBase, loggerInterceptors, errorInterceptor = errorInterceptors)

    @Provides
    @LoggerInterceptors
    fun provideLoggerInterceptor(
        context: Context
    ): Interceptor = ChuckerInterceptor.Builder(context)
        .collector(ChuckerCollector(context))
        .redactHeaders(emptySet())
        .alwaysReadResponseBody(false)
        .build()

    @Provides
    @ErrorInterceptors
    fun provideErrorInterceptor(
        firebaseAnalytics: FirebaseAnalytics,
    ): Interceptor = Interceptor { chain ->
        val request = chain.request()
        val response = chain.proceed(request)
        val code = response.code
        if (code != HttpURLConnection.HTTP_OK && code != HttpURLConnection.HTTP_NO_CONTENT) {
            val headerRequest = request.headers.toString()
            val bodyRequest = request.body?.let {
                val buffer = Buffer()
                it.writeTo(buffer)
                buffer.readUtf8()
            }
            val bodyResponse = response.peekBody(Long.MAX_VALUE).string()
            val data = Bundle().apply {
                putString(KEY_METHOD, request.method)
                putString(KEY_URL, request.url.toString())
                putString(KEY_HEADER, headerRequest)
                putString(KEY_BODY, bodyRequest.orEmpty())
                putString(KEY_RESPONSE, bodyResponse.orEmpty())
            }
            firebaseAnalytics.logEvent(EVENT_A_ERROR_API, data)
        }
        response
    }

    private fun getRetrofit(
        gson: Gson,
        @BaseMockPath urlBase: String,
        loggerInterceptors: Interceptor? = null,
        headerInterceptor: Interceptor? = null,
        errorInterceptor: Interceptor? = null
    ): Retrofit = Retrofit.Builder().baseUrl(urlBase)
        .client(getSimpleHttpClientBuilder(loggerInterceptors, headerInterceptor, errorInterceptor))
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    private fun getSimpleHttpClientBuilder(
        loggerInterceptors: Interceptor? = null,
        headerInterceptor: Interceptor? = null,
        errorInterceptor: Interceptor? = null
    ): OkHttpClient {
        val TIME_OUT = 120L
        val logging = HttpLoggingInterceptor().apply { level = BODY }
        val clientBuilder = OkHttpClient.Builder()
            .connectTimeout(TIME_OUT, SECONDS)
            .readTimeout(TIME_OUT, SECONDS)
            .writeTimeout(TIME_OUT, SECONDS)
            .addInterceptor(logging)
        loggerInterceptors?.let { clientBuilder.addInterceptor(it) }
        headerInterceptor?.let { clientBuilder.addInterceptor(it) }
        errorInterceptor?.let { clientBuilder.addInterceptor(it) }
        return clientBuilder.build()
    }
}