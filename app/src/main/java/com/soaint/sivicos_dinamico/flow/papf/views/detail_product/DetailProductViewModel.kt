package com.soaint.sivicos_dinamico.flow.papf.views.detail_product

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.DetailProductObjectPapfEntity
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(
        val detailProduct: DetailProductObjectPapfEntity,
        val infoTramit: InfoTramitePapfEntity?
    ) : OnViewModelEvent()

    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class DetailProductViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _infoTramit = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramit = _infoTramit.asLiveData()

    private val _detailProduct = MutableLiveData<DetailProductObjectPapfEntity>()
    val detailProduct = _detailProduct.asLiveData()


    fun init(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int) {
        loadDetailProduct(idSolicitud, idProductoSolicitud, idClasificacionProducto)
    }

    fun loadDetailProduct(
        idSolicitud: Int,
        idProductoSolicitud: Int,
        idClasificacionProducto: Int
    ) = execute {
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _detailProduct.value = papfUseCase.loadDetailProductLocal(
            idSolicitud,
            idProductoSolicitud,
            idClasificacionProducto
        ).getOrNull()
        _event.value = if (_detailProduct.value != null) {
            OnViewModelEvent.OnViewModelReady(_detailProduct.value!!, _infoTramit.value)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }

    fun loadInfoTramit(infoTramit: InfoTramitePapfEntity) {
        _infoTramit.value = infoTramit
    }

}
