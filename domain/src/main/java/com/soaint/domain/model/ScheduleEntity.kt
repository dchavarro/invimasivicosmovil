package com.soaint.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ScheduleEntity(
    val idTramite: Int?,
    val idTipoHoraFacturacion: Int?,
    val codigo: String?,
    val tipoHora: String?,
    val cantidadHoras: Double?,
) : Parcelable

@Parcelize
data class ScheduleTempEntity(
    val codigo: String?,
    val cantidadHoras: Double?,
) : Parcelable