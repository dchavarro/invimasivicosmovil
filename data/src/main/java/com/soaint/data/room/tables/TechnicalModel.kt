package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.TechnicalEntity

@Entity(tableName = "technical_table")
class TechnicalModel(
    @ColumnInfo(name = "idPersona") var idPersona: Int?,
    @ColumnInfo(name = "idRolPersona") val idRolPersona: Int?,
    @ColumnInfo(name = "idEmpresaAsociada") val idEmpresaAsociada: Int?,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "nombreCompleto") val nombreCompleto: String?,
    @ColumnInfo(name = "numeroDocumento") val numeroDocumento: String?,
    @ColumnInfo(name = "rolPersona") val rolPersona: String?,
    @ColumnInfo(name = "celular") val celular: String?,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "codigoTipoDocumento") val codigoTipoDocumento: String?,
    @ColumnInfo(name = "numeroVisita") val numeroVisita: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "confirmado") var confirmado: Boolean?,
    @ColumnInfo(name = "profesion") var profesion: String?,
    @ColumnInfo(name = "seleccionado") var seleccionado: Boolean? = false,
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
)

fun TechnicalModel.mapToDomain() = TechnicalEntity(
    idPersona,
    idRolPersona,
    idEmpresaAsociada,
    correoElectronico,
    telefono,
    nombreCompleto,
    numeroDocumento,
    rolPersona,
    celular,
    tipoDocumento,
    codigoTipoDocumento,
    numeroVisita,
    idVisita,
    confirmado,
    profesion,
    seleccionado
)

fun TechnicalEntity.mapToModel() = TechnicalModel(
    idPersona,
    idRolPersona,
    idEmpresaAsociada,
    correoElectronico,
    telefono,
    nombreCompleto,
    numeroDocumento,
    rolPersona,
    celular,
    tipoDocumento,
    codigoTipoDocumento,
    numeroVisita,
    idVisita,
    confirmado,
    profesion,
    seleccionado
)