package com.soaint.domain.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize


@Parcelize
data class InfoActaPapfResponseObjectPapFEntity(
    val actaInspeccion: List<ActaInspeccionPapfEntity>?,
    val actaTomaMuestra: List<ActaTomaMuestraPapfEntity>?,
) : Parcelable

@Parcelize
data class ActaInspeccionPapfEntity(
    val idSolicitud: Int?,
    val idProductoSolicitud: Int?,
    val condActaInspeccion: String?,
    val obsActaInspeccion: String?,
    val idEstadoEmp: Int?,
    val descripcionEstadoEmpaque: String?,
    val idConcepto: Int?,
    val descripcionConcepto: String?
) : Parcelable


@Parcelize
data class ActaTomaMuestraPapfEntity(
    val idSolicitud: Int?,
    val idProductoSolicitud: Int?,
    val unidadesMuestraLote: String?,
    val unidadesMedida: String?,
    val idPresentacion: Int?,
    val contenidoNeto: String?
) : Parcelable