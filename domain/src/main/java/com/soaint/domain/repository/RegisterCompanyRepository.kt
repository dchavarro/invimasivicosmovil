package com.soaint.domain.repository

import com.soaint.domain.model.*

interface RegisterCompanyRepository {

    suspend fun loadPersonLocal(idVisita: Int): Result<List<VisitCompanyPersonEntity>>

    suspend fun insertPersonLocal(body: VisitCompanyPersonEntity, idVisita: Int): Result<Unit>

    suspend fun deletePersonLocal(numeroDocumento: String, idVisita: Int): Result<Unit>

    suspend fun loadCompanyLocal(idVisita: Int): Result<RegisterCompanyBodyEntity?>

    suspend fun insertOrUpdateCompanyLocal(body: RegisterCompanyBodyEntity, idVisita: Int): Result<Unit>

    suspend fun saveCompany(idVisita: Int): Result<Unit>

}