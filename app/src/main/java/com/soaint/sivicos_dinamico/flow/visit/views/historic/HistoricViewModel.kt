package com.soaint.sivicos_dinamico.flow.visit.views.historic

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.HistoricEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.HistoricUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.model.NavigationToDirectionEvent
import javax.inject.Inject


sealed class OnHistoricViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnHistoricViewModelEvent()
    data class OnViewModelReady(val historic: List<HistoricEntity>): OnHistoricViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnHistoricViewModelEvent()
}

class HistoricViewModel @Inject constructor(
    private val historicUseCase: HistoricUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnHistoricViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    fun init(visitData: List<VisitEntity>) = execute {
        _event.value =
            OnHistoricViewModelEvent.OnChangeLabelPreloader(R.string.cargando_historic)
        val historic = visitData.filter { it.empresa != null }.mapNotNull { historicUseCase.loadHistoricoLocal(it.empresa?.razonSocial.orEmpty()) }
        val resultHistoric = historic.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        _event.value = OnHistoricViewModelEvent.OnViewModelReady(resultHistoric)
    }

    fun goToDocument(idVisita: String) = execute {
        val navigation = HistoricFragmentDirections.historicoToDocumento(idVisita = idVisita)
        _navigationEvent.value = NavigationToDirectionEvent(navigation)
    }
}