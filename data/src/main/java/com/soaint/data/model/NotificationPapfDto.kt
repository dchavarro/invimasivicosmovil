package com.soaint.data.model

import com.google.gson.annotations.SerializedName

data class NotificationPapfBodyDto(
    @SerializedName("plantilla") val plantilla: String,
    @SerializedName("destinatarios") val destinatarios: List<String>?,
    @SerializedName("copias") val copias: List<String>?,
    @SerializedName("asunto") val asunto: String,
    @SerializedName("adjuntos") val adjuntos: List<AdjuntosDto>,
    @SerializedName("etiquetas") val etiquetas: EtiquetasDto,
)

data class AdjuntosDto(
    @SerializedName("nombre") val nombre: String,
    @SerializedName("base64") val base64: String,
)

data class EtiquetasDto(
    @SerializedName("\${operacioneSanitarias}") val operacioneSanitarias: String? = null,
    @SerializedName("\${papf}") val papf: String? = null,
    @SerializedName("\${Radicado}") val radicado: String? = null,
    @SerializedName("\${fecha_inspeccion}") val fechaInspeccion: String? = null,
    @SerializedName("\${observaciones}") val observaciones: String? = null,
    @SerializedName("\${linkTramite}") val linkTramite: String? = null
)

