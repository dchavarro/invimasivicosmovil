package com.soaint.domain.repository

import com.soaint.domain.model.GroupDocumentEntity
import com.soaint.domain.model.GroupEntity
import com.soaint.domain.model.RulesBussinessEntity

interface ManageRepository {

    suspend fun loadGroupPapf(): Result<List<GroupEntity>>

    suspend fun loadGroup(idTipoProducto: Int): Result<List<GroupEntity>>

    suspend fun loadGroupLocal(idTipoProducto: Int?): Result<List<GroupEntity>>

    suspend fun loadGroupDocument(): Result<List<GroupDocumentEntity>>

    suspend fun loadGroupDocumentLocal(): Result<List<GroupDocumentEntity>>

    suspend fun loadRulesBussiness(idGroup: Int, idTipoProducto: Int): Result<List<RulesBussinessEntity>>

    suspend fun loadRulesBussinessLocal(idGroup: Int, idTipoProductoPapf: Int?): Result<List<RulesBussinessEntity>>

    suspend fun loadRulesBussinessLocal(idPlantilla: String): Result<RulesBussinessEntity?>

    suspend fun loadRulesBussinessPapf(idTipoProductoPapf: Int, filter:String): Result<List<RulesBussinessEntity>?>

}