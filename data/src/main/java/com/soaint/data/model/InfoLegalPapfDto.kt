package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.InfoNotificationPapfModel
import com.soaint.domain.model.InfoLegalPapfEntity
import com.soaint.domain.model.InfoNotificationPapfEntity

data class InfoLegalPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: InfoLegalPapfDto?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class InfoLegalPapfDto(
    @SerializedName("informacionNotificacion") val informacionNotificacion: List<InfoNotificationPapfDto>?,
)

fun InfoLegalPapfDto.mapToDomain() = InfoLegalPapfEntity(
    informacionNotificacion?.map { it.mapToDomain() }.orEmpty(),
)

data class InfoNotificationPapfDto(
    @SerializedName("TipoNotificacion") val tipoNotificacion: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("CorreoElectronico") val correoElectronico: String?,
)

fun InfoNotificationPapfDto.mapToDomain() = InfoNotificationPapfEntity(
    tipoNotificacion.orEmpty(),
    direccion.orEmpty(),
    correoElectronico.orEmpty(),
)

fun InfoNotificationPapfDto.mapToDb(idSolicitud: Int) = InfoNotificationPapfModel(
    tipoNotificacion.orEmpty(),
    direccion.orEmpty(),
    correoElectronico.orEmpty(),
    idSolicitud
)