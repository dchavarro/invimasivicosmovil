package com.soaint.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.soaint.data.room.tables.LocationModel

@Dao
interface LocationDao {

    @Query("SELECT * FROM location_table WHERE idPersona = :idPersona")
    suspend fun selectLocationByUser(idPersona: Int): List<LocationModel>

    @Insert(entity = LocationModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocation(location: LocationModel)

    @Query("DELETE FROM location_table")
    suspend fun deleteAllLocation()

    @Query("DELETE FROM location_table WHERE idPersona = :idPersona")
    suspend fun deleteLocationByUser(idPersona: Int)
}




