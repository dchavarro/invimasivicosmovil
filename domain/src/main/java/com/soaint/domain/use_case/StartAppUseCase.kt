package com.soaint.domain.use_case

import com.soaint.domain.repository.PreferencesRepository
import javax.inject.Inject

private val USER_NAME_KEY = "USER_NAME_KEY"

sealed class StartAppResult {
    object UpgradeAppResult : StartAppResult()
    object OpenHomeResult : StartAppResult()
    object OpenLoginResult : StartAppResult()
    data class ErrorResult(val error: Throwable?) : StartAppResult()
}

class StartAppUseCase @Inject constructor(
    private val validateVersionCode: VersionUseCase,
    private val userUseCase: UserUseCase,
    private val preferencesRepository: PreferencesRepository,
) {

    suspend fun start(versionCode: String): StartAppResult {
        val result = validateVersionCode.validate(versionCode)
        return if (result.isSuccess) {
            validateVersionCode(result.getOrDefault(false))
        } else {
            validateVersionCode(false)
        }
    }

    private fun validateVersionCode(needUpdateApp: Boolean) =
        if (needUpdateApp) StartAppResult.UpgradeAppResult else updateProfile()

    private fun updateProfile() =
        if (userUseCase.getUser() == null) StartAppResult.OpenLoginResult else StartAppResult.OpenHomeResult
}