package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.domain.model.*

data class DataInfoActaPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosDataInfoActaPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosDataInfoActaPapfDto) : this(
        AuditoriaCapitalDto(ip, user), parametros
    )
}

data class ParametrosDataInfoActaPapfDto(
    @SerializedName("idDetalleProducto") val idDetalleProducto: Int,
)

data class InfoActaPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: InfoActaPapfResponseObjectPapfDto?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class InfoActaPapfResponseObjectPapfDto(
    @SerializedName("ActaInspeccion") val actaInspeccion: List<ActaInspeccionPapfDto>?,
    @SerializedName("ActaTomaMuestra") val actaTomaMuestra: List<ActaTomaMuestraPapfDto>?,
)

data class ActaInspeccionPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int?,
    @SerializedName("IdProductoSolicitud") val idProductoSolicitud: Int?,
    @SerializedName("CondActaInspeccion") val condActaInspeccion: String?,
    @SerializedName("ObsActaInspeccion") val obsActaInspeccion: String?,
    @SerializedName("IdEstadoEmp") val idEstadoEmp: Int?,
    @SerializedName("DescripcionEstadoEmpaque") val descripcionEstadoEmpaque: String?,
    @SerializedName("IdConcepto") val idConcepto: Int?,
    @SerializedName("DescripcionConcepto") val descripcionConcepto: String?
)

data class ActaTomaMuestraPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int?,
    @SerializedName("IdProductoSolicitud") val idProductoSolicitud: Int?,
    @SerializedName("unidadesMuestraLote") val unidadesMuestraLote: Int?,
    @SerializedName("descripcionUnidades") val descripcionUnidades: String?,
    @SerializedName("unidadesMedida") val unidadesMedida: Int?,
    @SerializedName("IdPresentacion") val idPresentacion: Int?,
    @SerializedName("DescripcionPresentacion") val descripcionPresentacion: String?,
    @SerializedName("contenidoNeto") val contenidoNeto: Int?,
)
