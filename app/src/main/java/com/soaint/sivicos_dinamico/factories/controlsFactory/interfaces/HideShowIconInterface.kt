package com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces

import com.soaint.domain.model.EMPTY_STRING

interface HideShowIconInterface {
    fun showHamburgerIcon()
    fun showBackIcon(title: String? = EMPTY_STRING)
}