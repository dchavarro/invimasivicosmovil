package com.soaint.data.api

import com.soaint.data.model.*
import retrofit2.http.*

interface InperApi {

    @POST("v1/visita/inspeccion-permanente/consultarHorasFacturadas")
    suspend fun getSchedule(
        @Body body: BodyGenericScheduleDto
    ): ScheduleResponseDto

    @POST("v1/visita/inspeccion-permanente/guardarHorasEjecutadas")
    suspend fun updateSchedule(
        @Body body: BodyScheduleUploaDto
    ): Unit

    @GET("v1/visita/inspeccion-permanente/equipo")
    suspend fun getEquipos(
        @Query("idSede") idSede: Int,
        @Query("codigoResultado") codigoResultado: String = "REEQ_NOCU",
    ): EquipmentResponseDto

    @PUT("v1/visita/inspeccion-permanente/equipo")
    suspend fun updateEquipos(
        @Body body: List<EquipmentDto>
    ): Unit

    @POST("v1/visita/inspeccion-permanente/equipo")
    suspend fun insertEquipos(
        @Body body: EquipmentDto
    ): Unit

    @POST("v1/visita/inspeccion-permanente/copia")
    suspend fun copyVisit(
        @Body body: BodyGenericVisitDto
    ): Unit

}