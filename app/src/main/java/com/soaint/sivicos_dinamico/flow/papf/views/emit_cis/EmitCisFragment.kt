package com.soaint.sivicos_dinamico.flow.papf.views.emit_cis

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.model.EmitCisClosePapfEntity
import com.soaint.domain.model.EmitCisObservationPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentEmitCisBinding
import com.soaint.sivicos_dinamico.extensions.selectValue
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.item_view.ObservationEmitPapfItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.*
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class EmitCisFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentEmitCisBinding>(R.layout.fragment_emit_cis)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: EmitCisViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.emit_cis))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(EmitCisFragmentDirections.toInfo())
        }

        binding.buttonSend.setOnSingleClickListener { viewModel.checkData() }
        binding.buttonAdd.setOnSingleClickListener { viewModel.validateObservation() }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.infoTramit.observe(viewLifecycleOwner, viewModel::init)
        mainViewModel.isTaskCis.observe(viewLifecycleOwner, viewModel::taskCis)
        viewModel.query.observe(viewLifecycleOwner, ::onViewModelSpinnerReady)
        viewModel.mssSelected.observe(viewLifecycleOwner, ::setMss)
        viewModel.reembarqueSelected.observe(viewLifecycleOwner, ::setReembarque)
    }

    private fun setMss(action: DinamicQuerysPapfEntity?) {
        if (action != null) {
            binding.spinnerMss.selectValue(action.descripcion)
        }
    }

    private fun setReembarque(action: DinamicQuerysPapfEntity?) {
        if (action != null) {
            binding.spinnerReembarque.selectValue(action.descripcion)
        }
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelSuccess -> {
                showSnack(true, event.message)
                binding.editTextObservation.text = EMPTY_STRING.toEditable()
            }
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.emitData)
            is OnViewModelEvent.OnViewModelObservation -> onViewModelObservation(event.observacition)
            is OnViewModelEvent.validateMaximiumObservations -> validateMaximiumObservations(event.message)
            is OnViewModelEvent.OnValidateCertificate -> validateSpinner(
                binding.spinnerCountry,
                event.certificateId
            )
            is OnViewModelEvent.OnValidateTypeCertificate -> validateSpinner(
                binding.spinnerCertificate,
                event.typeCertificateId
            )
            is OnViewModelEvent.OnValidateDataImport -> validateSpinner(
                binding.spinnerMpig,
                event.mpigId
            )
            is OnViewModelEvent.OnValidateResult -> validateSpinner(
                binding.spinnerResult,
                event.resultId
            )
            is OnViewModelEvent.OnValidateIdioma -> validateSpinner(
                binding.spinnerIdiom,
                event.idiomaId
            )
            is OnViewModelEvent.OnValidateReemabarqueMss -> validateErrorSpinner(
                event.mssId,
                event.reembarqueId
            )
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelSpinnerReady(query: List<DinamicQuerysPapfEntity>?) {
        if (!query.isNullOrEmpty()) {
            val idTipoTramite = mainViewModel.infoTramit.value?.idTipoTramite.orZero().toString()
            loadSpinner(query, YES_NO_CERTIFICATE_EXPORTA, binding.spinnerCountry)
            loadSpinner(query, CD_TYPE_CERTIFICATE, binding.spinnerCertificate)
            loadSpinner(query, CD_MPIG, binding.spinnerMpig)
            loadSpinner(query, CD_RESULT_CIS, binding.spinnerResult)
            loadSpinner(query, CD_FIRMATE, binding.spinnerFirmat)
            loadSpinner(
                query.filter { it.idTipoTramite == idTipoTramite },
                CD_IDIOM,
                binding.spinnerIdiom
            )
            loadSpinner(query, YES_NO_REEMBARQUE, binding.spinnerReembarque)
            loadSpinner(query, YES_NO_MSS, binding.spinnerMss)
        }
    }

    private fun loadSpinner(
        codeQuery: List<DinamicQuerysPapfEntity>,
        query: String,
        spinner: Spinner
    ) {
        val array = codeQuery.filter { it.codeQuery == query }.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        spinner.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                viewModel.setItemSelected(itemSelected, query)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReady(emitData: EmitCisClosePapfEntity?) {

        binding.spinnerFirmat.isEnabled = emitData?.idFirmante == null

        if (emitData != null) {

            emitData.descripcionCertificadoExporta?.let {
                viewModel.setItemSelected(it, YES_NO_CERTIFICATE_EXPORTA)
                binding.spinnerCountry.selectValue(it)
            }
            emitData.descripcionTipoCertificado?.let {
                viewModel.setItemSelected(it, CD_TYPE_CERTIFICATE)
                binding.spinnerCertificate.selectValue(it)
            }
            emitData.descripcionUsoMPIG?.let {
                viewModel.setItemSelected(it, CD_MPIG)
                binding.spinnerMpig.selectValue(it)
            }
            emitData.descripcionResultadoCis?.let {
                viewModel.setItemSelected(it, CD_RESULT_CIS)
                binding.spinnerResult.selectValue(it)
            }
            emitData.firmante?.let {
                viewModel.setItemSelected(it, CD_FIRMATE)
                binding.spinnerFirmat.selectValue(it)
            }
            emitData.descripcionIdioma?.let {
                viewModel.setItemSelected(it, CD_IDIOM)
                binding.spinnerIdiom.selectValue(it)
            }
            emitData.descripcionReembarque?.let {
                viewModel.setItemSelected(it, YES_NO_REEMBARQUE)
                binding.spinnerReembarque.selectValue(it)
            }
            emitData.descripcionMSS?.let {
                viewModel.setItemSelected(it, YES_NO_MSS)
                binding.spinnerMss.selectValue(it)
            }
        }
    }

    private fun onViewModelObservation(observation: List<EmitCisObservationPapfEntity>?) {
        adapter.clear()
        val items = mutableListOf<Group>()

        if (!observation.isNullOrEmpty()) {
            items.addAll(observation.map { ObservationEmitPapfItemView(it, ::onEdit, ::onDelete) })
        }

        val isVisible = observation.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        adapter.updateAsync(items)
        binding.editTextObservation.text = EMPTY_STRING.toEditable()
        binding.buttonAdd.text = getString(R.string.copy_add)
    }

    private fun onDelete(item: EmitCisObservationPapfEntity) {
        viewModel.deleteObservation(item)
    }

    private fun onEdit(item: EmitCisObservationPapfEntity) {
        viewModel.setObservation(item)
        binding.editTextObservation.text = item.descripcion.orEmpty().toEditable()
        binding.buttonAdd.text = getString(R.string.copy_notification_save)
    }

    private fun validateErrorSpinner(
        mssId: Int?,
        reembarqueId: Int?,
    ) {
        validateSpinner(binding.spinnerMss, mssId)
        validateSpinner(binding.spinnerReembarque, reembarqueId)
    }
    private fun validateMaximiumObservations(@StringRes message: Int){
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(message),
                titleBtnPositive = getString(R.string.cerrar)
            )
            {}
        )
        binding.editTextObservation.text = EMPTY_STRING.toEditable()
    }
}