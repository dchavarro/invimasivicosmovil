package com.soaint.data.repository

import com.soaint.data.api.DocumentApi
import com.soaint.data.api.MasterApi
import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.CODE_DOCUMENTARY_CLASSIFICATION_EJECU
import com.soaint.data.model.*
import com.soaint.data.room.dao.ActasDao
import com.soaint.data.room.dao.BlocksDao
import com.soaint.data.room.dao.DocumentDao
import com.soaint.data.room.tables.DocumentModel
import com.soaint.data.room.tables.mapToDomain
import com.soaint.data.room.tables.mapToModel
import com.soaint.data.utils.getBase64FromPath
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.DocumentNameEntity
import com.soaint.domain.model.GroupEntity
import com.soaint.domain.repository.DocumentRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class DocumentRepositoryImpl @Inject constructor(
    private val visitApi: VisitApi,
    private val masterApi: MasterApi,
    private val documentApi: DocumentApi,
    private val documentDao: DocumentDao,
    private val blocksDao: BlocksDao,
    private val actasDao: ActasDao,
) : DocumentRepository, BaseRepository() {

    private val _document = MutableStateFlow<DocumentEntity?>(null)
    override val document get() = _document.asStateFlow()

    private val _documents = MutableStateFlow<List<DocumentEntity>?>(null)
    override val documents get() = _documents.asStateFlow()

    //Carga de Documentos Asociados por visita
    override suspend fun loadDocument(idVisit: String): Result<List<DocumentEntity>> = execute {
        val document = visitApi.getDocument(idVisit).objectResponse
        //deleteDocumentDao()
        saveDocumentDao(document, idVisit)
        Result.success(document.map { it.mapToDomain() })
    }

    //Carga de Documentos Asociados por visita DB local
    override suspend fun loadDocumentLocal(idVisit: String): Result<List<DocumentEntity>> = execute {
        val documents = documentDao.selectDocument(idVisit).map { it.mapToDomain() }
        _documents.value = documents
        Result.success(documents)
    }

    //Guardar Documentos Asociados en DB local
    private suspend fun saveDocumentDao(data: List<DocumentDto>, idVisit: String) {
        documentDao.deleteDocumentxVisit(idVisit.toInt())
        data.onEach { document ->
            val model = document.mapToDb(idVisit)
            documentDao.insertDocument(model)
        }.orEmpty()
    }

    //Eliminar Documentos Asociados en DB local
    private suspend fun deleteDocumentDao() {
        documentDao.deleteAllDocument()
    }

    //Carga de Documentos Asociados por visita
    override suspend fun loadDocumentName(idGrupo: Int): Result<List<DocumentNameEntity>> =
        execute {
            val document = visitApi.getNameDocument(idGrupo).objectResponse.tipoDocumentalDTO
            deleteDocumentNameDao(idGrupo)
            saveDocumentNameDao(document, idGrupo)
            Result.success(document.map { it.mapToDomain() })
        }

    //Carga de Documentos Asociados por visita DB local
    override suspend fun loadDocumentNameLocal(idGrupo: Int): Result<List<DocumentNameEntity>> =
        Result.success(
            documentDao.selectDocumentName(idGrupo).map { it.mapToDomain() }
        )

    //Guardar Documentos Asociados en DB local
    private suspend fun saveDocumentNameDao(data: List<DocumentNameDto>, idGrupo: Int) {
        data.onEach { documentName ->
            val model = documentName.mapToDb(idGrupo)
            documentDao.insertDocumentName(model)
        }
    }

    //Eliminar Documentos Asociados en DB local
    private suspend fun deleteDocumentNameDao(idGrupo: Int) {
        documentDao.deleteDocumentName(idGrupo)
    }

    // Servicio Documento with Value
    override suspend fun addDocument(
        body: DocumentBodyEntity
    ): Result<Unit> = execute {
        val newBody = body.copy(
            archivoBase64 = getBase64FromPath(body.archivoBase64),
            nombreDocumento = "${body.nombreDocumento}.${body.extension}"
        )
        val result = documentApi.addDocument(newBody.mapToData())
        deleteNewDocumentDao(body.metadata?.idDocumentBody)
        Result.success(result)
    }

    //Guardar Documento Local with Value
    override suspend fun addDocumentLocal(body: DocumentBodyEntity): Result<Unit> {
        val result = documentDao.insertAddDocument(body.mapToModel()).let { id ->
            documentDao.insertAddDocumentMetadata(body.metadata?.mapToModel(id.toString().toInt()))
            documentDao.insertDocument(
                DocumentModel(
                    extension = body.extension,
                    descripcionTipoDocumental = body.descripcionDocumento,
                    urlSesuite = null,
                    descripcionClasificacionDocumental = CODE_DOCUMENTARY_CLASSIFICATION_EJECU,
                    detalle = "${body.nombreDocumento}.${body.extension}",
                    urlSharedPoint = null,
                    descripcionDocumento = body.descripcionDocumento,
                    usuarioCrea = body.usuarioCrea,
                    clasificacionDocumental = body.idClasificacionDocumental,
                    idDocumento = body.idDocumento?.toInt(),
                    nombreDocumento = body.nombreDocumento,
                    consecutivoSesuite = null,
                    fechaDocumento = body.metadata?.fechaCreacionDocumento,
                    nombreTipoDocumental = body.descripcionDocumento,
                    idTipoDocumental = body.idTipoDocumental,
                    numeroFolios = body.numeroFolios.toString(),
                    idPlantilla = null,
                    codigoPlantilla = null,
                    indDocumentoFirmado = false,
                    idVisita = body.idVisita,
                    idDocumentBody = id.toInt()
                )
            )
        }

        return Result.success(result)
    }

    //Eliminar Documentos Asociados en DB local
    private suspend fun deleteNewDocumentDao(id: Int?) {
        id?.let {
            documentDao.deleteAddDocumentMetadata(it)
            documentDao.deleteAddDocument(it)
        }
    }

    //Carga de Transporte por visita DB local
    override suspend fun getDocumentBodyLocalByVisit(idVisit: String): Result<List<DocumentBodyEntity>> {
        val result = documentDao.selectAddDocument(idVisit)
            .map { document ->
                val id = document.id
                val metadata = id.let {
                    documentDao.selectAddDocumentMetadata(it)
                        .map { it.mapToDomain(it.idDocumentBody) }
                }
                document.mapToDomain(metadata.firstOrNull())
            }
        return Result.success(result)
    }

    //Carga de Transporte por id DB local
    override suspend fun getDocumentBodyLocalById(idDocumentBody: String): Result<List<DocumentBodyEntity>> {
        val result = documentDao.selectAddDocumentById(idDocumentBody)
            .map { document ->
                val id = document.id
                val metadata = id.let {
                    documentDao.selectAddDocumentMetadata(it)
                        .map { it.mapToDomain(it.idDocumentBody) }
                }
                document.mapToDomain(metadata.firstOrNull())
            }
        return Result.success(result)
    }

    override suspend fun getDocumentActLocal(idPlantilla: Int, idVisita: Int): Result<String> {
        val result = actasDao.selectPdfActa(idPlantilla, idVisita)
        return if (result != null && result.pdf != null) {
            Result.success(result.pdf)
        } else {
            Result.failure(Throwable())
        }
    }

    override suspend fun deleteDocument(
        id: Int,
        url: String?,
        idVisita: String,
        idPlantilla: String?
    ): Result<Unit> = Result.success(
        if (url.isNullOrBlank()) {
            documentDao.deleteDocument(id)
            blocksDao.deleteBodyBlock(idPlantilla.orEmpty(), idVisita, 0)
            deleteNewDocumentDao(id)
        } else {
            Unit
        } //TODO implentar servicio de eliminar docuemnto
    )

    override suspend fun addPLantilla2DocumentLocal(
        body: DocumentEntity,
        idVisita: String
    ): Result<Unit> = Result.success(
        documentDao.insertOrUpdateDocument(
            DocumentModel(
                extension = body.extension,
                descripcionTipoDocumental = body.descripcionTipoDocumental,
                urlSesuite = body.urlSesuite,
                descripcionClasificacionDocumental = body.descripcionClasificacionDocumental,
                detalle = body.detalle,
                urlSharedPoint = body.urlSharedPoint,
                descripcionDocumento = body.descripcionDocumento,
                usuarioCrea = body.usuarioCrea,
                clasificacionDocumental = body.clasificacionDocumental,
                idDocumento = body.idDocumento?.toInt(),
                nombreDocumento = body.nombreDocumento,
                consecutivoSesuite = body.consecutivoSesuite,
                fechaDocumento = body.fechaDocumento,
                nombreTipoDocumental = body.nombreTipoDocumental,
                idTipoDocumental = body.idTipoDocumental,
                numeroFolios = body.numeroFolios.toString(),
                idPlantilla = body.idPlantilla,
                codigoPlantilla = body.codigoPlantilla,
                indDocumentoFirmado = body.indDocumentoFirmado,
                idVisita = idVisita,
                idDocumentBody = null
            )
        )
    )

    override suspend fun getDocumentUpload(): Result<Int> = Result.success(
        documentDao.selectAllDocument()
    )

    // Servicio ver documuento
    override suspend fun viewDocument(
        idDocument: Int
    ): Result<String> = execute {
        val result = documentApi.viewDocument(idDocument).objectResponse.orEmpty()
        Result.success(result)
    }
}