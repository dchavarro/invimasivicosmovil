package com.soaint.sivicos_dinamico.flow.papf.views.emit_cis

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.model.*
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.domain.use_case.TaskPapfUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.ValidationUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(
        val emitData: EmitCisClosePapfEntity?,
    ) : OnViewModelEvent()

    data class OnViewModelObservation(
        val observacition: List<EmitCisObservationPapfEntity>?
    ) : OnViewModelEvent()

    data class OnViewModelSuccess(@StringRes val message: Int) : OnViewModelEvent()
    data class validateMaximiumObservations(@StringRes val message: Int) : OnViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()

    data class OnValidateTypeCertificate(
        val typeCertificateId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateCertificate(
        val certificateId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateDataImport(
        val mpigId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateResult(
        val resultId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateIdioma(
        val idiomaId: Int?,
    ) : OnViewModelEvent()

    data class OnValidateReemabarqueMss(
        val reembarqueId: Int?,
        val mssId: Int?,
    ) : OnViewModelEvent()
}

class EmitCisViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
    private val userUseCase: UserUseCase,
    private val validationUseCase: ValidationUseCase,
    private val taskPapfUseCase: TaskPapfUseCase,
) : BaseViewModel() {

    private val _query = MutableLiveData<List<DinamicQuerysPapfEntity>?>(null)
    val query = _query.asLiveData()

    private var mpigSelected: DinamicQuerysPapfEntity? = null

    private var resultCisSelected: DinamicQuerysPapfEntity? = null

    private var firmatSelected: DinamicQuerysPapfEntity? = null

    private var idiomSelected: DinamicQuerysPapfEntity? = null

    private val _typeCertificateSelected = MutableLiveData<DinamicQuerysPapfEntity?>(null)
    val typeCertificateSelected get() = _typeCertificateSelected.asLiveData()

    private val _reembarqueSelected = MutableLiveData<DinamicQuerysPapfEntity?>(null)
    val reembarqueSelected get() = _reembarqueSelected.asLiveData()

    private val _mssSelected = MutableLiveData<DinamicQuerysPapfEntity?>(null)
    val mssSelected get() = _mssSelected.asLiveData()

    private val _certificateExportaSelected = MutableLiveData<DinamicQuerysPapfEntity?>(null)
    val certificateExportaSelected get() = _certificateExportaSelected.asLiveData()


    private val _isVisibleCertificate = MutableLiveData<Boolean>(false)
    val isVisibleCertificate = _isVisibleCertificate.asLiveData()

    private val _isVisibleReembarque = MutableLiveData<Boolean>(false)
    val isVisibleReembarque = _isVisibleReembarque.asLiveData()

    private val _isVisibleLab = MutableLiveData<Boolean>(false)
    val isVisibleLab = _isVisibleLab.asLiveData()

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _emitInfoData = MutableLiveData<InfoEmitirPapfEntity?>(null)
    val emitInfoData = _emitInfoData.asLiveData()

    private val _checkListData = MutableLiveData<CheckListEntity?>(null)
    val checkListData = _checkListData.asLiveData()

    private val _data = MutableLiveData<EmitCisClosePapfEntity>()
    val data = _data.asLiveData()

    private val _infoTramit = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramit = _infoTramit.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _itemObservation = MutableLiveData<EmitCisObservationPapfEntity?>()
    val itemObservation = _itemObservation.asLiveData()

    private val _isTaskCis = MutableLiveData<Boolean>()
    val isTaskCis = _isTaskCis.asLiveData()


    fun init(infoTramit: InfoTramitePapfEntity) = execute {
        _infoTramit.value = infoTramit
        _event.value = OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        loadDataEmit()
        getObservation()
    }

    fun taskCis(status: Boolean) {
        _isTaskCis.value = status
    }

    private suspend fun loadDataEmit() {
        _query.value = papfUseCase.loadDinamicQuerysLocal().getOrNull().orEmpty()
        _emitInfoData.value =
            _infoTramit.value?.idSolicitud?.let { papfUseCase.loadInfoEmitirLocal(it).getOrNull() }
        _checkListData.value =
            _infoTramit.value?.idSolicitud?.let { papfUseCase.loadInfoCheckLocal(it).getOrNull() }
        _data.value =
            _infoTramit.value?.idSolicitud?.let { papfUseCase.loadDataEmitCisLocal(it).getOrNull() }
        if (_data.value != null) _event.value = OnViewModelEvent.OnViewModelReady(_data.value)
        validateAct()
    }

    private suspend fun validateAct() {
        val result = _infoTramit.value?.idSolicitud?.let {
            validationUseCase.validationActs(it, listOf(IVC_INS_FM012))
        }
        val data = result?.getOrNull()?.any { it == IVC_INS_FM012}
        _isVisibleLab.value = data == false && _isTaskCis.value != true
    }

    /*
    Set Items
     */

    fun setItemSelected(itemSelected: String, codeQuery: String) {
        val query = _query.value
        when (codeQuery) {
            CD_MPIG -> this.mpigSelected = query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }
            CD_RESULT_CIS -> query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }?.let {
                this.resultCisSelected = it
                _isVisibleReembarque.value = it.id == 2 || it.id == 3
                _reembarqueSelected.value = null
                _mssSelected.value = null
            }
            CD_FIRMATE -> this.firmatSelected = query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }
            CD_IDIOM -> this.idiomSelected = query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery && _infoTramit.value?.idTipoTramite.toString() == it.idTipoTramite
            }
            YES_NO_CERTIFICATE_EXPORTA -> query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }?.let {
                _certificateExportaSelected.value = it
                _isVisibleCertificate.value = it.id == 1
                _typeCertificateSelected.value = null
            }
            CD_TYPE_CERTIFICATE -> query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }?.let {
                _typeCertificateSelected.value = it
            }
            YES_NO_REEMBARQUE -> query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }?.let {
                _reembarqueSelected.value = it
                if (it.id == 1) {
                    setMssSelected(2)
                } else if (it.id == 2) {
                    setMssSelected(1)
                } else setMssSelected(null)
            }
            YES_NO_MSS -> query?.find {
                it.descripcion.equals(
                    itemSelected, true
                ) && it.codeQuery == codeQuery
            }?.let {
                _mssSelected.value = it
                if (it.id == 1) {
                    setReembarqueSelected(2)
                } else if (it.id == 2) {
                    setReembarqueSelected(1)
                } else setReembarqueSelected(null)
            }
        }
    }

    fun setMssSelected(id: Int?): DinamicQuerysPapfEntity? {
        if (id != null) {
            _query.value?.find { it.id == id && it.codeQuery == YES_NO_MSS }?.let {
                _mssSelected.value = it
            }
            return _mssSelected.value
        } else {
            _mssSelected.value = null
            return null
        }
    }

    fun setReembarqueSelected(id: Int?): DinamicQuerysPapfEntity? {
        if (id != null) {
            _query.value?.find { it.id == id && it.codeQuery == YES_NO_REEMBARQUE }?.let {
                _reembarqueSelected.value = it
            }
            return _reembarqueSelected.value
        } else {
            _reembarqueSelected.value = null
            return null
        }
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun setObservation(itemObservation: EmitCisObservationPapfEntity) {
        _itemObservation.value = itemObservation
    }
    fun validateObservation(saveCloseIns: Boolean = false) = execute {
        val idSolicitud = _infoTramit.value?.idSolicitud.orZero()
        val isMaximum = papfUseCase.hasReachedMaximumObservationsEmitirCis(idSolicitud)
        if (isMaximum){
            val res = if (saveCloseIns) R.string.limit_observations_save_emitir_cis else R.string.limit_observations
            if (!_observation.value.isNullOrBlank()) _event.value = OnViewModelEvent.validateMaximiumObservations(res)
            _observation.value = null
        }else{
            addObservation()
        }
    }
    fun getObservation() = execute {
        var observationsDefault: ArrayDeque<EmitCisObservationPapfEntity> = ArrayDeque()
        val result =
            papfUseCase.loadObservationEmitCisLocal(_infoTramit.value?.idSolicitud.orZero())
        _event.value = if (result.isSuccess) {
            _query.value?.filter {
                it.codeQuery == CD_OBSERVATION_PAPF && it.idTipoTramite == _infoTramit.value?.idTipoTramite.toString()
                        && it.idTipoTramite != ID_TYPE_EXPORTER.toString()
            }?.mapNotNull {
                observationsDefault.add(
                    EmitCisObservationPapfEntity(
                        it.descripcion.orEmpty(), isDefault = true
                    )
                )
            }
            val observations =
                (observationsDefault + result.getOrNull().orEmpty()).distinctBy { it.descripcion }
            OnViewModelEvent.OnViewModelObservation(observations)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }

    fun addObservation() = execute {
        if (!_observation.value.isNullOrBlank()) {
            val result = papfUseCase.insertOrUpdateObservationEmitCisLocal(
                EmitCisObservationPapfEntity(
                    _observation.value,
                    _infoTramit.value?.idSolicitud,
                    _itemObservation.value?.id,
                )
            )
            _event.value = if (result.isSuccess) {
                _itemObservation.value = null
                _observation.value = null
                getObservation()
                OnViewModelEvent.OnViewModelSuccess(R.string.copy_add_success)
            } else {
                OnViewModelEvent.OnLoadingError(null)
            }
        }
    }

    fun deleteObservation(itemObservation: EmitCisObservationPapfEntity) = execute {
        val result = papfUseCase.deleteObservationEmitCisLocal(itemObservation)
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.OnViewModelSuccess(R.string.copy_delete_success)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
        getObservation()
    }

    fun checkData() {
        val certificateId = _certificateExportaSelected.value?.id
        val typeCertificateId = _typeCertificateSelected.value?.id
        val mpigId = mpigSelected?.id
        val resultId = resultCisSelected?.id
        val idiomaId = idiomSelected?.id
        val reembarqueId = _reembarqueSelected.value?.id
        val mssId = _mssSelected.value?.id

        // Validar campos obligatorios
        if (_infoTramit.value?.idTipoTramite == ID_TYPE_EXPORTER && (certificateId == null || certificateId == 0)) {
            _event.value = OnViewModelEvent.OnValidateCertificate(certificateId)
        } else if (certificateId == 1 && (typeCertificateId == null || typeCertificateId == 0)) {
            _event.value = OnViewModelEvent.OnValidateTypeCertificate(typeCertificateId)
        } else if (_infoTramit.value?.idTipoTramite == ID_TYPE_IMPORTER && (mpigId == null || mpigId == 0)) {
            _event.value = OnViewModelEvent.OnValidateDataImport(mpigId)
        } else if (resultId == null || resultId == 0) {
            _event.value = OnViewModelEvent.OnValidateResult(resultId)
        } else if (idiomaId == null || idiomaId == 0) {
            _event.value = OnViewModelEvent.OnValidateIdioma(idiomaId)
        } else if ((resultId == 2 || resultId == 3) && (reembarqueId == null || reembarqueId == 0 || mssId == null || mssId == 0)) {
            _event.value = OnViewModelEvent.OnValidateReemabarqueMss(reembarqueId, mssId)
        } else {
            insertOrUpdate()
        }
    }

    fun insertOrUpdate() = execute {
        val body = _data.value?.copy(
            idSolicitud = _infoTramit.value?.idSolicitud,
            idUsoMPIG = mpigSelected?.id,
            descripcionUsoMPIG = mpigSelected?.descripcion,
            idResultadoCIS = resultCisSelected?.id,
            descripcionResultadoCis = resultCisSelected?.descripcion,
            idFirmante = firmatSelected?.id,
            firmante = firmatSelected?.descripcion,
            idIdioma = idiomSelected?.id,
            descripcionIdioma = idiomSelected?.descripcion,
            idCertificadoExporta = _certificateExportaSelected.value?.id,
            descripcionCertificadoExporta = _certificateExportaSelected.value?.descripcion,
            idTipoCertificado = _typeCertificateSelected.value?.id,
            descripcionTipoCertificado = _typeCertificateSelected.value?.descripcion,
            idReembarque = _reembarqueSelected.value?.id,
            descripcionReembarque = _reembarqueSelected.value?.descripcion,
            idMSS = _mssSelected.value?.id,
            descripcionMSS = _mssSelected.value?.descripcion,
        )
        val result = papfUseCase.insertOrUpdateEmitLocal(body)
        _event.value = if (result.isSuccess) {
            validateObservation(true)
            loadDataEmit()
            OnViewModelEvent.OnViewModelSuccess(R.string.copy_add_success)
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
