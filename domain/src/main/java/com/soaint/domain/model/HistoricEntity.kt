package com.soaint.domain.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HistoricEntity(
    val id: Int?,
    val numero: String?,
    val codigoSucursal: String?,
    val razonSocial: String?,
    val fechaRegistro: String?,
    val descDireccion: String?,
    val fechaRealizo: String?,
    val clasificacion: String?,
    val descRespRealizar: String?,
    val descFuenteInfo: String?,
    val descRazon: String?,
    val resultado: String?,
    val observacionResultado: String?,
    val estado: String?,
    val inspector: String?,
    val idVisita: String?,
    val departamento: String?,
    val municipio: String?,
    val medidasSanitarias: String?,
    val conceptoSanitario: String?,
    val empresaAsociada: String?,
    val nombreEmpresaAsociada: String?,
    val tipoTramite: String?,
    val subTipoTramite: String?,
    val activo: Boolean?,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val usuarioModifica: String?,
    val idEstadoHistVisita: Int?
) : Parcelable