package com.soaint.sivicos_dinamico.flow.visit.item_view

import androidx.core.view.isVisible
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.model.VisitGroupEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemReasonBinding
import com.soaint.sivicos_dinamico.utils.REASON_RAVI_INPER
import com.xwray.groupie.databinding.BindableItem

class  ReasonItemView(
    private val visit: VisitEntity,
    private val group: List<VisitGroupEntity>,
    private val subgroup: List<VisitGroupEntity>,
    private var isVisible: Boolean = false
) : BindableItem<ItemReasonBinding>() {

    override fun bind(
        viewBinding: ItemReasonBinding,
        position: Int
    ) = with(viewBinding) {
        relativeLayoutLeft.isVisible = false
        relativeLayoutRight.isVisible = false

        isVisible(visit.codigoRazon.orEmpty(), viewBinding)

        textViewReason.text = visit.descRazon
        textViewInfo.text = visit.descFuenteInformacion
        textViewModality.text = visit.modalidadEjecutar
        textViewClassification.text = visit.descClasificacion
        textViewPriority.text = visit.descPrioridad
        textViewFraude.text = visit.posibleMotivo
        textViewTramit.text = visit.tramite?.descTipoTramite
        textViewSubtramit.text = visit.tramite?.descSubTipoTramite
        textViewGroup.text = group.map { "- " + it.nombre }.joinToString("\n")
        textViewSubgroup.text = subgroup.map { "- " + it.nombre }.joinToString("\n")

        textViewTitleGroup.isVisible = !group.isNullOrEmpty()
        textViewTitleSubgroup.isVisible = !group.isNullOrEmpty()
        textViewGroup.isVisible = !group.isNullOrEmpty()
        textViewSubgroup.isVisible = !group.isNullOrEmpty()

        constraintLayoutPrincipal.setOnClickListener {
            isVisible = !isVisible
            relativeLayoutLeft.isVisible = isVisible
            relativeLayoutRight.isVisible = isVisible
        }
    }

    override fun getLayout() = R.layout.item_reason

    fun isVisible(codigoRazon: String, viewBinding: ItemReasonBinding) = with(viewBinding) {
        val visible = codigoRazon != REASON_RAVI_INPER
        textViewModality.isVisible = visible
        textViewTitleModality.isVisible = visible
        textViewClassification.isVisible = visible
        textViewTitleClassification.isVisible = visible
        textViewPriority.isVisible = visible
        textViewTitlePriority.isVisible = visible
        textViewFraude.isVisible = visible
        textViewTitleFraude.isVisible = visible
        textViewSubtramit.isVisible = visible
        textViewTitleSubtramit.isVisible = visible
    }
}