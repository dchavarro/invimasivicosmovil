package com.soaint.sivicos_dinamico.flow.papf.dialogs.act_sample

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.intOrString
import com.soaint.data.common.orZero
import com.soaint.data.dispatchers.AppDispatchersImpl
import com.soaint.domain.model.DataReqActSamplePapfEntity
import com.soaint.domain.model.DataReqInsSanPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.CD_PRESENTATION
import com.soaint.sivicos_dinamico.utils.CD_UNITY
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) : OnViewModelEvent()
    data class onViewModelUnity(val unitys: List<DinamicQuerysPapfEntity>) : OnViewModelEvent()
    data class onViewModelPresentation(val presentations: List<DinamicQuerysPapfEntity>) :
        OnViewModelEvent()
    data class onViewModelData(val data: DataReqActSamplePapfEntity?) : OnViewModelEvent()

    data class OnValidate(
        val unityId: Int?,
        val presentationId: Int?,
        val unityLote: String?,
        val neto: String?,
    ) : OnViewModelEvent()
}

class DataReqActSampleViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _unity = MutableLiveData<List<DinamicQuerysPapfEntity>>()
    val unity = _unity.asLiveData()

    private val _unitySelected = MutableLiveData<DinamicQuerysPapfEntity>(null)
    val unitySelected = _unitySelected.asLiveData()

    private val _presentation = MutableLiveData<List<DinamicQuerysPapfEntity>>()
    val presentation = _presentation.asLiveData()

    private val _presentationSelected = MutableLiveData<DinamicQuerysPapfEntity>(null)
    val presentationSelected = _presentationSelected.asLiveData()

    private val _unityLote = MutableLiveData(EMPTY_STRING)
    val unityLote get() = _unityLote.asLiveData()

    private val _neto = MutableLiveData(EMPTY_STRING)
    val neto get() = _neto.asLiveData()

    private val _idProduct = MutableLiveData<Int>()
    val idProduct = _idProduct.asLiveData()

    private val _idRequest = MutableLiveData<Int>()
    val idRequest = _idRequest.asLiveData()


    fun init(idProducto: Int, idSolicitud: Int) = execute {
        _idProduct.value = idProducto
        _idRequest.value = idSolicitud
        getUnity()
        getPresentation()
        getData()
    }

    suspend fun getData()  {
        val result = papfUseCase.loadDataReqActSampleLocal(
            _idRequest.value.orZero(),
            _idProduct.value.orZero()
        )
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.onViewModelData(result.getOrNull())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }

    suspend fun getUnity()  {
        val result = papfUseCase.loadDinamicQuerysLocal(CD_UNITY)
        _event.value = if (result.isSuccess) {
            _unity.value = result.getOrNull().orEmpty()
            OnViewModelEvent.onViewModelUnity(result.getOrNull().orEmpty())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }

    suspend fun getPresentation()  {
        val result = papfUseCase.loadDinamicQuerysLocal(CD_PRESENTATION)
        _event.value = if (result.isSuccess) {
            _presentation.value = result.getOrNull().orEmpty()
            OnViewModelEvent.onViewModelPresentation(result.getOrNull().orEmpty())
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }


    fun onUnityLoteChange(data: Editable?) {
        _unityLote.value = data.toString()
    }

    fun onNetoChange(data: Editable?) {
        _neto.value = data.toString()
    }


    fun setNroUnity(itemSelected: String) {
        _unity.value?.find { it.descripcion.equals(itemSelected, true) }?.let {
            _unitySelected.value = it
        }
    }

    fun setPresentation(itemSelected: String) {
        _presentation.value?.find { it.descripcion.equals(itemSelected, true) }?.let {
            _presentationSelected.value = it
        }
    }

    fun checkData() {
        val unityId = _unitySelected.value?.id
        val presentationId = _presentationSelected.value?.id
        val unityLote = _unityLote.value!!
        val neto = _neto.value!!

        if (unityId == null || unityId == 0 || presentationId == null || presentationId == 0 || neto.isNullOrBlank() || unityLote.isNullOrBlank())
            _event.value = OnViewModelEvent.OnValidate(unityId, presentationId, unityLote, neto)
        else onSaveLocal()

    }

    fun onSaveLocal() = execute {
        val result = papfUseCase.insertOrUpdateDataReqActSampleLocal(
            DataReqActSamplePapfEntity(
                _unitySelected.value?.id,
                _unitySelected.value?.descripcion,
                _unityLote.value.intOrString(),
                _presentationSelected.value?.id,
                _presentationSelected.value?.descripcion,
                _neto.value.intOrString(),
                _idProduct.value!!,
                _idRequest.value!!,
            )
        )
        _event.value = if (result.isSuccess) {
            OnViewModelEvent.onSnack(true, R.string.copy_update_success)
        } else {
            OnViewModelEvent.onSnack(false, R.string.error)
        }
    }
}