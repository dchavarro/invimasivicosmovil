package com.soaint.domain.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class ManageMssEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val descripcion: String? = EMPTY_STRING,
    val codigoPlantillas: String? = null,
    val codigoPlantillasActualizada: String? = null,
    val tipoMs: String? = EMPTY_STRING,
    val idTipoProducto: String? = EMPTY_STRING,
) : Parcelable {
    var isSelected: Boolean = false
}

@Parcelize
data class ManageMsspEntity(
    val id: Int? = 0,
    val numeroVisita: String? = EMPTY_STRING,
    val idVisita: Int? = 0,
    val numeroMss: String? = EMPTY_STRING,
    val nombreProducto: String? = EMPTY_STRING,
    val tipoMedidaSanitaria: String? = EMPTY_STRING,
    val estado: String? = EMPTY_STRING,
    val idEstado: Int? = 0,
    val fechaAplicacion: String? = EMPTY_STRING,
    val notificacionSanitariaObligatoria: String? = EMPTY_STRING,
    var lote: String? = EMPTY_STRING,
    val codigoMedidaSanitaria: String? = EMPTY_STRING,
    val tiposActividades: String? = EMPTY_STRING,
    val razonSocial: String? = EMPTY_STRING,
    val tipoMs: String? = EMPTY_STRING,
) : Parcelable {
    var isSelected: Boolean = false
}

@Parcelize
data class MssAppliedEntity(
    val idMedidaSanitaria: Int,
    val codigoMedidaSanitaria: String,
    val descripcionMedida: String,
    val valorAplicado: Boolean,
    val idVisita: Int,
) : Parcelable

@Parcelize
data class LstValoresMssAppliedEntity(
    val id: Int,
    val valor: Boolean,
) : Parcelable

@Parcelize
data class ManageAmssEntity(
    val id: Int? = 0,
    val codigo: String? = EMPTY_STRING,
    val descripcion: String? = EMPTY_STRING,
    val idVisita: Int? = 0,
    val isSelected: Boolean? = false,
    val tipoMs: String? = EMPTY_STRING,
    val createdAt: String? = EMPTY_STRING,
) : Parcelable

@Parcelize
data class RsEntity(
    val codigo: String? = EMPTY_STRING,
    val nombreProducto: String? = EMPTY_STRING,
    val idVisita: Int? = 0,
) : Parcelable