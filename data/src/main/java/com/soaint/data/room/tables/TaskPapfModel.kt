package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.TaskPapfEntity

@Entity(tableName = "task_papf_table")
class TaskPapfModel(
    @PrimaryKey val idSolicitud: Int,
    @ColumnInfo(name = "numeroRadicado") val numeroRadicado: String?,
    @ColumnInfo(name = "idTipoInspeccion") val idTipoInspeccion: String?,
    @ColumnInfo(name = "descripcionTipoInspeccion") val descripcionTipoInspeccion: String?,
    @ColumnInfo(name = "fecha") val fecha: String?,
    @ColumnInfo(name = "fechaPosibleInspeccion") val fechaPosibleInspeccion: String?,
    @ColumnInfo(name = "papf") val papf: String?,
    @ColumnInfo(name = "nombreLugarAlmacenamiento") val nombreLugarAlmacenamiento: String?,
    @ColumnInfo(name = "nombreSitioInspeccion") val nombreSitioInspeccion: String?,
    @ColumnInfo(name = "lote") val lote: Int?,
    @ColumnInfo(name = "idProgramacion") val idProgramacion: Int?,
    @ColumnInfo(name = "descActividad") val descActividad: String?,
    @ColumnInfo(name = "certificadoExportacion") val certificadoExportacion: Boolean?,
    @ColumnInfo(name = "estado") val estado: String?,
    @ColumnInfo(name = "fechaAsignacion") val fechaAsignacion: String?,
    @ColumnInfo(name = "usuarioAsignado") val usuarioAsignado: String?,
    @ColumnInfo(name = "rol") val rol: String?,
    @ColumnInfo(name = "usuarioRed") val usuarioRed: String?,
)

fun TaskPapfModel.mapToDomain() = TaskPapfEntity(
    idSolicitud,
    numeroRadicado,
    idTipoInspeccion,
    descripcionTipoInspeccion,
    fecha,
    fechaPosibleInspeccion,
    papf,
    nombreLugarAlmacenamiento,
    nombreSitioInspeccion,
    lote,
    idProgramacion,
    descActividad,
    certificadoExportacion,
    estado,
    fechaAsignacion,
    usuarioAsignado,
    rol,
    usuarioRed,
)