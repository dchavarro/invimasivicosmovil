package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.close

import android.app.DatePickerDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.InputFilter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.formatDateHour
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCloseVisitBinding
import com.soaint.sivicos_dinamico.extensions.*
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ShowAlert
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import java.text.DecimalFormat
import java.util.*

class CloseVisitFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCloseVisitBinding>(R.layout.fragment_close_visit)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: CloseVisitViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.cierre_visita))
        binding.includeReprogramar.yes.setOnSingleClickListener { viewModel.setReprogramar(true) }
        binding.includeReprogramar.no.setOnSingleClickListener { viewModel.setReprogramar(false) }
        binding.editTextDate.setOnSingleClickListener { setDate() }
        binding.textInputDate.setOnSingleClickListener { setDate() }
        binding.buttonSend.setOnSingleClickListener { viewModel.checkData() }
        binding.editTextPercentage.setFilters(arrayOf<InputFilter>(InputFilterMinMax(0, 100)))
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
        viewModel.openDialogPersonal.observe(viewLifecycleOwner, ::openDialog)
        viewModel.actionVisitSelected.observe(viewLifecycleOwner, ::setAction)
    }

    private fun setAction(action: ManageMssEntity?) {
        if (action != null) {
            binding.spinnerAction.selectValue(action.descripcion)
        }
    }

    private fun eventHandler(event: onCloseVisitViewModelEvent) {
        showLoading(false)
        when (event) {
            is onCloseVisitViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is onCloseVisitViewModelEvent.onLoadingError -> onError()
            is onCloseVisitViewModelEvent.OnViewModelResultReady -> OnViewModelResultReady(event.result)
            is onCloseVisitViewModelEvent.OnViewModelStatusReady -> OnViewModelStatusReady(event.result)
            is onCloseVisitViewModelEvent.OnViewModelActionReady -> OnViewModelActionReady(event.result)
            is onCloseVisitViewModelEvent.OnViewModelConceptReady -> OnViewModelConceptReady(event.result)
            is onCloseVisitViewModelEvent.OnViewModelReadyVisit -> OnViewModelReadyVisit(event.visit)
            is onCloseVisitViewModelEvent.OnViewModelSnackBar -> showSnackBar(event.isSuccess, event.message)
            is onCloseVisitViewModelEvent.OnValidateData -> validateErrorData(
                event.actionId,
                event.resultId,
                event.conceptId,
                event.statusId,
                event.concept,
                event.percentage,
                event.observation,
            )

        }
    }

    private fun onError() {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar))
            { dismissAlertDialog() }
        )
    }

    private fun OnViewModelReadyVisit(visit: VisitEntity?) {
        if (visit != null) {
            val MAX_CHARACTERS = 5000 // Cambia esto al límite deseado
            val filters = arrayOf<InputFilter>(InputFilter.LengthFilter(MAX_CHARACTERS))

            val getResult = viewModel.setResultVisitSelected(visit.idResultado)?.descripcion.orEmpty()
            binding.spinnerResult.selectValue(getResult)
            viewModel.setResultVisitSelected(getResult)

            val getAction = viewModel.setActionVisitSelected(visit.idAccionSeguir)?.descripcion.orEmpty()
            binding.spinnerAction.selectValue(getAction)
            viewModel.setActionVisitSelected(getAction)

            viewModel.setReprogramar(visit.requiereReprogramarVisita?: false)

            viewModel.setDateChange(visit.fechaVerificacionRequerimiento.orEmpty().toEditable())
            binding.editTextDate.setText(visit.fechaVerificacionRequerimiento.orEmpty().formatDateHour().toEditable())

            val getConcept = viewModel.setConceptVisitSelected(visit.idConceptoSanitario)?.descripcion.orEmpty()
            binding.spinnerConcept.selectValue(getConcept)
            viewModel.setConceptVisitSelected(getConcept)

            val getStatus = viewModel.setStatusCompanySelected(visit.idEstadoEmpresa)?.descripcion.orEmpty()
            binding.spinnerStatus.selectValue(getStatus)
            viewModel.setStatusCompanySelected(getConcept)

            binding.editTextPercentage.setText(visit.calificacion.orEmpty())
            viewModel.setPercentageChange(visit.calificacion.orEmpty().toEditable())

            binding.editTextConcept.setText(visit.descripcionConcepto.orEmpty())
            binding.editTextConcept.filters = filters
            viewModel.setDescConceptChange(visit.descripcionConcepto.orEmpty().toEditable())

            binding.editTextObservation.setText(visit.observacionResultado.orEmpty())
            binding.editTextObservation.filters = filters
            viewModel.setObservationChange(visit.observacionResultado.orEmpty().toEditable())

        }
    }

    private fun OnViewModelStatusReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerStatus.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerStatus.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setStatusCompanySelected(itemSelected)
                } else {
                    viewModel.setStatusCompanySelected(null)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelResultReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerResult.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerResult.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setResultVisitSelected(itemSelected)
                } else {
                    viewModel.setResultVisitSelected(null)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelActionReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerAction.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerAction.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setActionVisitSelected(itemSelected)
                } else {
                    viewModel.setActionVisitSelected(null)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelConceptReady(result: List<ManageMssEntity>) {
        val array = result.toMutableList()
        array.add(0, ManageMssEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerConcept.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerConcept.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setConceptVisitSelected(itemSelected)
                } else {
                    viewModel.setConceptVisitSelected(null)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    fun openDialog(isVisible: Boolean) {
        if (isVisible) showDialogPersonal()
    }

    private fun setDate() {
        val c: Calendar = Calendar.getInstance()
        val yearCalendar = c.get(Calendar.YEAR)
        val dayMonth = c.get(Calendar.DAY_OF_MONTH)
        val month = c.get(Calendar.MONTH)
        val timePickerDialog = DatePickerDialog(requireContext(), { _, yearResponse, month, day_month ->
            val monthSelected = DecimalFormat("00").format(month + 1).toString()
            val daySelected = DecimalFormat("00").format(day_month).toString()
            val dateString = "$yearResponse-$monthSelected-$daySelected"+"T00:00:00"
            viewModel.setDateChange(dateString.toEditable())
            binding.textInputDate.editText?.text = dateString.formatDate().toEditable()
        }, yearCalendar, month, dayMonth)
        timePickerDialog.setCancelable(false)
        timePickerDialog.show()
    }

    private fun validateErrorData(
        actionId:Int?,
        resultId:Int?,
        conceptId:Int?,
        statusId:Int?,
        concept:String?,
        percentage:String?,
        observation:String?,
    ) {
        binding.apply {
            validateSpinner(spinnerAction, actionId)
            validateSpinner(spinnerResult, resultId)
            validateSpinner(spinnerConcept, conceptId)
            validateSpinner(spinnerStatus, statusId)
            validateField(textInputConcept, concept)
            validateField(textInputPercentage, percentage)
            validateField(textInputObservation, observation)
        }
    }

    fun showSnackBar(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
        if (isSuccess) findNavController().popBackStack(); mainViewModel.refreshVisit()
    }
}