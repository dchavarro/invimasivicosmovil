package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TaskEntity (
    val activo: Boolean,
    val codigoTarea: String?,
    val descEstado: String?,
    val descripcionActividad: String?,
    val descripcionOrigen: String?,
    val fechaCreacion: String?,
    val fechaModifica: String?,
    val fechaTarea: String?,
    val idActividadIVC: Int,
    val idEstadoTarea: Int,
    val idOrigenTarea: Int,
    val idTarea: Int,
    val idTipoProducto: Int,
    val usuarioCrea: String?,
    val usuarioModifica: String?,
    val codigoEstado: String?,
    val codigoOrigen: String?,
    val codigoActividad: String?
) : Parcelable

@Parcelize
data class TaskBodyEntity (
    val idTarea: Int,
    val activo: Boolean,
    val codigoActividad: String?,
    val codigoEstado: String?,
    val codigoOrigen: String?,
    val usuarioModifica: String?,
) : Parcelable

@Parcelize
data class TaskMssBodyEntity (
    val idActividadIVC: String,
    val idOrigenTarea: String,
    val idEstadoTarea: String,
    val idTipoProducto: Int,
    val usuarioCrea: String,
) : Parcelable

@Parcelize
data class TaskCompanyBodyEntity (
    val idSedeEmpreTarea: Int,
    val idTarea: Int,
    val usuarioCrea: String,
) : Parcelable
