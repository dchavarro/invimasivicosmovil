package com.soaint.data.repository

import com.soaint.data.api.VisitApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.HistoricDto
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.HistoricDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.HistoricEntity
import com.soaint.domain.repository.HistoricRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class HistoricRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val visitApi: VisitApi,
    private val historicDao: HistoricDao,
) : HistoricRepository, BaseRepository() {

    private val _historico = MutableStateFlow<List<HistoricEntity>?>(null)
    override val historico get() = _historico.asStateFlow()

    //Carga de Historico por razonSocial
    override suspend fun loadHistoric(razonSocial: String): Result<List<HistoricEntity>> = execute {
        val historic = visitApi.getHistorico(razonSocial).objectResponse
        deleteHistoricDao(razonSocial)
        saveHistoricDao(historic?.orEmpty())
        Result.success(historic?.map { it.mapToDomain() }.orEmpty())
    }

    //Carga de Historico por razonSocial DB local
    override suspend fun loadHistoricLocal(razonSocial: String): Result<List<HistoricEntity>> =
        Result.success(historicDao.selectHistoric(razonSocial).map { it.mapToDomain() })

    //Guardar Historico en DB local
    private suspend fun saveHistoricDao(data: List<HistoricDto>?) {
        data?.onEach { historic ->
            val model = historic.mapToDb()
            historicDao.insertHistoric(model)
        }.orEmpty()
    }

    //Eliminar Historico en DB local
    private suspend fun deleteHistoricDao(razonSocial: String) {
        historicDao.deleteHistoricByRazon(razonSocial)
    }


}