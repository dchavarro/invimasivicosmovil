package com.soaint.data.repository

import com.soaint.data.api.CompanyApi
import com.soaint.data.api.CrudApi
import com.soaint.data.api.ProcedureApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.STATUS_PERDIDA_CERTIFICACION
import com.soaint.data.common.orZero
import com.soaint.data.model.*
import com.soaint.data.room.dao.TechnicalDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.common.getIp
import com.soaint.domain.model.CertificateEntity
import com.soaint.domain.model.TechnicalEntity
import com.soaint.domain.repository.TechnicalRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class TechnicalRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val companyApi: CompanyApi,
    private val procedureApi: ProcedureApi,
    private val crudApi: CrudApi,
    private val technicalDao: TechnicalDao,
) : TechnicalRepository, BaseRepository() {

    private val _technical = MutableStateFlow<List<TechnicalEntity>?>(null)
    override val technical get() = _technical.asStateFlow()

    //Carga de tecnico por visita
    override suspend fun getTechnicals(idVisita: Int): Result<List<TechnicalEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val body = TechnicalBodyDto(getIp(), usuario, idVisita)
        val technicals = crudApi.getTechnical(body).objectResponse
        //deleteDao(idVisita)
        saveDao(technicals.orEmpty())
        Result.success(technicals?.map { it.mapToDomain() }.orEmpty())
    }

    //Carga de tecnico por visita DB local
    override suspend fun getTechnicalsLocal(idVisita: Int): Result<List<TechnicalEntity>> {
        val result = technicalDao.selectTechnical(idVisita)?.map { it.mapToDomain() }.orEmpty()
        return result.let { Result.success(it) }
    }

    //Guardar tecnico en DB local
    private suspend fun saveDao(data: List<TechnicalDto>?) {
        data?.onEach { technical ->
            val model = technical.mapToDb()
            technicalDao.insertTechnical(model)
        }.orEmpty()
    }

    //Eliminar tecnico por visita en DB local
    private suspend fun deleteDao(idVisita: Int) {
        technicalDao.deleteTechnicalByVisita(idVisita)
    }

    //Update tecnico en DB local
    override suspend fun updateTechnicalLocal(technical: TechnicalEntity): Result<Unit> = execute {
        val result = technicalDao.updateTechnical(technical)
        Result.success(result)
    }

    //Update tecnico
    override suspend fun updateTechnical(): Result<Unit> = execute {
        val userName = userRepository.userInformation.value?.usuario.toString()
        val item = technicalDao.selectTechnicalByIsConfirmed().orEmpty()
        val result = if (!item.isNullOrEmpty()) {
            item.mapNotNull {
                crudApi.updateTechnical(
                    BodyTechnicalDto(
                        getIp(),
                        userName,
                        it.mapToDomain().mapToData()
                    )
                )
            }
        } else Unit
        Result.success(result)
    } as Result<Unit>

    //Carga de product Interacciones por nit
    override suspend fun getCertificate(nitEmpresa: String): Result<List<CertificateEntity>> =
        execute {
            val userName = userRepository.userInformation.value?.usuario.toString()
            val product = companyApi.getCertificate(
                BodyCertificateDto(
                    getIp(),
                    userName,
                    nitEmpresa
                )
            ).objectResponse
            //deleteDao(nitEmpresa)
            saveProductInteractionDao(product.orEmpty(), nitEmpresa)
            Result.success(product?.map { it.mapToDomain() }.orEmpty())
        }

    //Carga de interacciones por visita DB local
    override suspend fun getCertificateLocal(nitEmpresa: String): Result<List<CertificateEntity>> {
        val result = technicalDao.selectCertificateByNit(nitEmpresa)?.map { it.mapToDomain() }
        return result?.let { Result.success(it) } ?: Result.failure(Throwable())
    }

    //Guardar product Interacciones en DB local
    private suspend fun saveProductInteractionDao(data: List<CertificateDto>?, nitEmpresa: String) {
        data?.onEach { product ->
            val model = product.mapToDb(nitEmpresa)
            technicalDao.insertCertificate(model)
        }.orEmpty()
    }

    //Update product Interacciones en DB local
    override suspend fun updateCertificateLocal(itemProduct: CertificateEntity): Result<Unit> =
        execute {
            val item =
                if (itemProduct.isSelected) itemProduct.copy(estado = STATUS_PERDIDA_CERTIFICACION)
                else itemProduct.copy(estado = null)
            val result = technicalDao.updateCertificate(item)
            Result.success(result)
        }

    override suspend fun updateCertificate(): Result<List<Unit>> = execute {
        val userName = userRepository.userInformation.value?.usuario.toString()
        val query = technicalDao.selectAllCertificate()
        val result = query?.mapNotNull {
            procedureApi.updateCertificate(
                BodyUpdateCertificateDto(
                    getIp(), userName,
                    BodyUpdateCertificateOperationDto(
                        nit = it.nitEmpresa,
                        idTramite = it.idtramite,
                        idCertificado = it.idCertificado,
                        estado = it.estado
                    )
                )
            )
        }.orEmpty()
        Result.success(result)
    }
}