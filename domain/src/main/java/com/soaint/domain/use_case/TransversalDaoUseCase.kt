package com.soaint.domain.use_case

import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.model.DocumentBodyEntity
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.ManageMssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.repository.ManageMssRepository
import com.soaint.domain.repository.TransversalDaoRepository
import javax.inject.Inject

class TransversalDaoUseCase @Inject constructor(
    private val repository: TransversalDaoRepository
) {

    suspend fun loadTypeDao(tipoDao: String) = repository.loadTypeDao(tipoDao)

    suspend fun loadTypeDaoLocal(tipoDao: String) = repository.loadTypeDaoLocal(tipoDao)

    suspend fun loadCountries() = repository.loadCountries()

    suspend fun loadCountriesLocal() = repository.loadCountriesLocal()

    suspend fun loadDepartment() = repository.loadDepartment()

    suspend fun loadDepartmentLocal(idPais: Int) = repository.loadDepartmentLocal(idPais)

    suspend fun loadTown() = repository.loadTown()

    suspend fun loadTownLocal(idDepartament: Int) = repository.loadTownLocal(idDepartament)

}