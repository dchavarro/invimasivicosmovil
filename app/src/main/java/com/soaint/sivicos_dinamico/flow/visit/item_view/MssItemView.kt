package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.annotation.SuppressLint
import com.soaint.domain.model.ManageMssEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemMssBinding
import com.xwray.groupie.databinding.BindableItem

class MssItemView(
    private val mssp: ManageMssEntity,
    private val onSelected: (ManageMssEntity, Boolean) -> Unit
) : BindableItem<ItemMssBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemMssBinding,
        position: Int
    ) = with(viewBinding) {

        checkBox.text = mssp.descripcion
        checkBox.isChecked = mssp.isSelected

        root.setOnClickListener {
            checkBox.isChecked = !checkBox.isChecked
            onSelected.invoke(mssp, checkBox.isChecked)
        }
    }

    override fun getLayout() = R.layout.item_mss
}