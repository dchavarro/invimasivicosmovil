package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity

@Entity(tableName = "transport_table")
class TransportModel(
    @ColumnInfo(name = "direccionMisional") val direccionMisional: String?,
    @ColumnInfo(name = "numeroOficioComisorio") val numeroOficioComisorio: String?,
    @ColumnInfo(name = "nombreEmpresa") val nombreEmpresa: String?,
    @ColumnInfo(name = "nitEmpresa") val nitEmpresa: String?,
    @ColumnInfo(name = "direccionEmpresa") val direccionEmpresa: String?,
    @ColumnInfo(name = "idVisita") val idVisita: String?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun TransportModel.mapToDomain() = TransportEntity(
    direccionMisional.orEmpty(),
    numeroOficioComisorio.orEmpty(),
    nombreEmpresa.orEmpty(),
    nitEmpresa.orEmpty(),
    direccionEmpresa.orEmpty(),
    emptyList(),
)

@Entity(tableName = "transport_official_table")
class TransportOffcialModel(
    @ColumnInfo(name = "nombreApellidos") val nombreApellidos: String?,
    @ColumnInfo(name = "celular") val celular: String?,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String?,
    @ColumnInfo(name = "idVisita") val idVisita: String?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun TransportOffcialModel.mapToDomain() = OfficialEntity(
    nombreApellidos,
    celular,
    correoElectronico
)

@Entity(tableName = "transport_body_table")
class TransportBodyModel(
    @ColumnInfo(name = "idTipoTransporte") val idTipoTransporte: Int,
    @ColumnInfo(name = "tipoTransporte") val tipoTransporte: String,
    @ColumnInfo(name = "motivoAppMediSanitaria") val motivoAppMediSanitaria: String,
    @ColumnInfo(name = "dimensionesAproxMercancia") val dimensionesAproxMercancia: String,
    @ColumnInfo(name = "numPiezasAproximadamente") val numPiezasAproximadamente: String,
    @ColumnInfo(name = "volumenAproximado") val volumenAproximado: String,
    @ColumnInfo(name = "pesoAproximado") val pesoAproximado: String,
    @ColumnInfo(name = "tipoMercancia") val tipoMercancia: String,
    @ColumnInfo(name = "numAuxNecesaCargue") val numAuxNecesaCargue: String,
    @ColumnInfo(name = "valorAproxDecomiso") val valorAproxDecomiso: String,
    @ColumnInfo(name = "fechaRecogida") val fechaRecogida: String,
    @ColumnInfo(name = "correoElectronico") val correoElectronico: String,
    @ColumnInfo(name = "idVisita") val idVisita: String,
    @ColumnInfo(name = "activo") val activo: Boolean = true,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String,
    @ColumnInfo(name = "isLocal") val isLocal: Boolean = false,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)

fun TransportBodyModel.mapToDomain() = TransportBodyEntity(
    idTipoTransporte,
    tipoTransporte,
    motivoAppMediSanitaria,
    dimensionesAproxMercancia,
    numPiezasAproximadamente,
    volumenAproximado,
    pesoAproximado,
    tipoMercancia,
    numAuxNecesaCargue,
    valorAproxDecomiso,
    fechaRecogida,
    correoElectronico,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica,
    isLocal
)

fun TransportBodyEntity.mapToModel() = TransportBodyModel(
    idTipoTransporte,
    tipoTransporte,
    motivoAppMediSanitaria,
    dimensionesAproxMercancia,
    numPiezasAproximadamente,
    volumenAproximado,
    pesoAproximado,
    tipoMercancia,
    numAuxNecesaCargue,
    valorAproxDecomiso,
    fechaRecogida,
    correoElectronico,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica,
    isLocal
)