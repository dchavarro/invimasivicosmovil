package com.soaint.data.api

import com.soaint.data.model.GroupDocumentResponseDto
import com.soaint.data.model.GroupResponseDto
import com.soaint.data.model.HolidayResponseDto
import com.soaint.data.model.ShiftTypeDto
import com.soaint.data.model.ShiftTypeResponseDto
import com.soaint.data.model.TypeHoursBillingDto
import com.soaint.data.model.TypeHoursBillingResponseDto
import retrofit2.http.GET
import retrofit2.http.Query

interface MasterApi {

    @GET("v1/grupos")
    suspend fun getGroup(
        @Query("idTipoProducto") idTipoProducto: String,
    ): GroupResponseDto

    @GET("v1/gruposST")
    suspend fun getGroupDocument(): GroupDocumentResponseDto

    @GET("v1/getFestivos")
    suspend fun getHolidays(): HolidayResponseDto

    @GET("v1/getJornadas")
    suspend fun getShiftType(): ShiftTypeResponseDto

    @GET("v1/getTipoFacturacionHora")
    suspend fun getTypeHoursBilling(): TypeHoursBillingResponseDto
}