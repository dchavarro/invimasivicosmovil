package com.soaint.sivicos_dinamico.flow.papf.views.products

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.RegisterProductObjectPapfEntity
import com.soaint.domain.use_case.PapfUseCase
import com.soaint.domain.use_case.TaskPapfUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.STATUS_PAPF_CIS
import javax.inject.Inject

sealed class OnViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnViewModelEvent()
    data class OnViewModelReady(
        val products: RegisterProductObjectPapfEntity,
        val infoTramit: InfoTramitePapfEntity?
    ) : OnViewModelEvent()

    data class OnLoadingError(val error: Throwable?) : OnViewModelEvent()
}

class RegisterProductsViewModel @Inject constructor(
    private val papfUseCase: PapfUseCase,
    private val userUseCase: UserUseCase,
    private val taskPapfUseCase: TaskPapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnViewModelEvent>()
    val event = _event.asLiveData()

    private val _productsData = MutableLiveData<RegisterProductObjectPapfEntity>()
    val productsData = _productsData.asLiveData()

    private val _infoTramit = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramit = _infoTramit.asLiveData()

    private val _isTaskCis = MutableLiveData<Boolean>()
    val isTaskCis = _isTaskCis.asLiveData()

    fun init(infoTramit: InfoTramitePapfEntity) {
        _infoTramit.value = infoTramit
        infoTramit.idSolicitud?.let { loadRegisterProducts(it) }
    }

    fun taskCis(status: Boolean) {
        _isTaskCis.value = status
    }

    fun loadRegisterProducts(requestId: Int) = execute {
        _event.value =
            OnViewModelEvent.OnChangeLabelPreloader(R.string.cargando)
        _productsData.value =
            papfUseCase.loadRegisterProductLocal(requestId).getOrNull()
        _event.value = if (_productsData.value != null) {
            productsData.value?.let { OnViewModelEvent.OnViewModelReady(it, _infoTramit.value) }
        } else {
            OnViewModelEvent.OnLoadingError(null)
        }
    }
}
