package com.soaint.sivicos_dinamico.flow.visit.views.address

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.StringRes
import androidx.core.os.bundleOf
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.TypeDaoEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseDialogFragment
import com.soaint.sivicos_dinamico.databinding.DialogAddressBinding
import com.soaint.sivicos_dinamico.extensions.hideKeyboard
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.visit.views.register_company.RegisterCompanyViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.validateField
import com.soaint.sivicos_dinamico.utils.validateSpinner
import com.xwray.groupie.GroupieAdapter

const val KEY_TITLE = "KEY_TITLE"

class DialogAddress : BaseDialogFragment() {

    companion object {

        fun getInstance() = DialogAddress().apply {
            arguments = bundleOf()
        }
    }

    private val viewModel: AddressViewModel by viewModels { viewModelFactory }

    private val registerCompanyViewModel by activityViewModels<RegisterCompanyViewModel> { viewModelFactory }

    private val binding by fragmentBinding<DialogAddressBinding>(R.layout.dialog_address)

    private val adapter by lazy { GroupieAdapter() }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val width = (resources.displayMetrics.widthPixels * 0.95).toInt()
        val height = ViewGroup.LayoutParams.WRAP_CONTENT
        dialog!!.window?.setLayout(width, height)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog?.setCancelable(false)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        binding.tvTitle.text = getString(R.string.copy_address)
        binding.imageViewClose.setOnSingleClickListener { dismiss() }
        binding.buttonSave.setOnSingleClickListener {
            requireActivity().hideKeyboard()
            viewModel.checkAddress()
        }
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        viewModel.initAddress()
    }

    private fun eventHandler(event: OnAddressViewModelEvent) {
        when (event) {
            is OnAddressViewModelEvent.onSnack -> showSnack(event.isSuccess, event.message)
            is OnAddressViewModelEvent.onViewModelReadyTypeAddress -> onViewModelReadyTypeDoc(event.typeAddress)
            is OnAddressViewModelEvent.onViewModelReadyTypeVia -> onViewModelReadyTypeVia(event.typeVia)
            is OnAddressViewModelEvent.onViewModelReadyTypeInmueble -> onViewModelReadyTypeInmueble(event.typeInmueble)
            is OnAddressViewModelEvent.onViewModelReadyComplement -> onViewModelReadyComplement(event.complement)
            is OnAddressViewModelEvent.OnValidateAddressRural -> validateField(binding.textInputDetail, event.detail)
            is OnAddressViewModelEvent.OnValidateAddress -> validateErrorData(
                event.typeId,
                event.viaId,
                event.neighborhood,
            )
        }
    }

    private fun onViewModelReadyTypeDoc(typeDoc: List<TypeDaoEntity>) {
        val array = typeDoc.toMutableList()
        array.add(0, TypeDaoEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerTypeAddress.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerTypeAddress.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setTypeAddress(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyTypeVia(typeDoc: List<TypeDaoEntity>) {
        val array = typeDoc.toMutableList()
        array.add(0, TypeDaoEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerAddress.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerAddress.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setTypeVia(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyTypeInmueble(typeDoc: List<TypeDaoEntity>) {
        val array = typeDoc.toMutableList()
        array.add(0, TypeDaoEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerTypeInmueble.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerTypeInmueble.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setInmueble(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun onViewModelReadyComplement(typeDoc: List<TypeDaoEntity>) {
        val array = typeDoc.toMutableList()
        array.add(0, TypeDaoEntity(descripcion = getString(R.string.seleccione)))
        val items = array.mapNotNull { it.descripcion }

        binding.spinnerTypeComplement.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        binding.spinnerTypeComplement.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                if (position != 0) {
                    viewModel.setInmueble(itemSelected)
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun validateErrorData(
        typeId:Int?,
        viaId:Int?,
        neighborhood:String?,
    ) {
        binding.apply {
            validateSpinner(spinnerTypeAddress, typeId)
            validateSpinner(spinnerAddress, viaId)
            validateField(textInputNeighborhood, neighborhood)
        }
    }

    fun showSnack(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING,
        ) { }
        registerCompanyViewModel.onAddressChange(viewModel.address.value.orEmpty().toEditable())
        dialog?.dismiss()
    }
}