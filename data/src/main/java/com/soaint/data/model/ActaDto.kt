package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.room.tables.GroupDocumentModel
import com.soaint.data.room.tables.GroupVisitModel
import com.soaint.data.room.tables.RulesBussinessModel
import com.soaint.domain.model.*

data class GroupResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<GroupDto>?,
    @SerializedName("statusCode") val statusCode: Int?
)

data class GroupDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("descripcion") val descripcion: String? = EMPTY_STRING,
    @SerializedName("idTipoProducto") val idTipoProducto: Int? = 0,
)

fun GroupDto.mapToDomain() = GroupEntity(
    id, codigo, descripcion, idTipoProducto
)

fun GroupDto.mapToDb(idTipoProducto: Int) = GroupVisitModel(
    id, codigo.orEmpty(), descripcion.orEmpty(), idTipoProducto
)

data class GroupDocumentResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<GroupDocumentDto>?,
    @SerializedName("statusCode") val statusCode: Int?
)

data class GroupDocumentDto(
    @SerializedName("nombreDependencia") val nombreDependencia: String? = EMPTY_STRING,
    @SerializedName("idGrupo") val idGrupo: Int,
)

fun GroupDocumentDto.mapToDomain() = GroupDocumentEntity(idGrupo, nombreDependencia)

fun GroupDocumentDto.mapToDb() = GroupDocumentModel(idGrupo, nombreDependencia.orEmpty())

data class ResultRulesBussinessResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<RulesBussinessDto>?,
    @SerializedName("statusCode") val statusCode: Int?
)

data class RulesBussinessDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("nombre") val nombre: String? = EMPTY_STRING,
    @SerializedName("codigoPlantilla") val codigoPlantilla: String? = EMPTY_STRING,
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int? = 0,
    @SerializedName("idGrupo") val idGrupo: Int? = 0,
    @SerializedName("idDependencia") val idTipoProducto: Int? = 0,
)

fun RulesBussinessDto.mapToDomain() = RulesBussinessEntity(
    id, nombre, codigoPlantilla, idTipoDocumental, idGrupo, idTipoProducto
)

fun RulesBussinessDto.mapToDb(idTipoProductoPapf: Int? = null) = RulesBussinessModel(
    id, nombre, codigoPlantilla, idTipoDocumental, idGrupo, idTipoProducto, idTipoProductoPapf
)

data class ResultSaveBlockResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: BlockSaveBodyDto?,
    @SerializedName("statusCode") val statusCode: Int?,
    @SerializedName("totalRegistros") val totalRegistros: String?
)

// BODY GUARDAR SINCRONIZACION
data class BlockSaveBodyDto(
    @SerializedName("documento") val documento: BlockDocumentSaveDto,
    @SerializedName("bloques") val bloques: List<BlockSaveDto>
)

fun BlockSaveBodyEntity.mapToData() = BlockSaveBodyDto(
    documento.mapToData(),
    bloques.map { it.mapToData() }
)

fun BlockSaveBodyDto.mapToDomain() = BlockSaveBodyEntity(
    documento.mapToDomain(),
    bloques.map { it.mapToDomain() }
)

fun BlockDocumentSaveDto.mapToDomain() = BlockDocumentSaveEntity(
    nombre,
    titulo.orEmpty(),
    usuario.orEmpty(),
    fecha.orEmpty(),
    idTipoDocumental,
    detalle,
    cantidadFolios,
    codigoClasificacion,
    codigoEstado,
)

data class BlockDocumentSaveDto(
    @SerializedName("nombre") val nombre: String,
    @SerializedName("titulo") val titulo: String,
    @SerializedName("usuario") val usuario: String,
    @SerializedName("fecha") val fecha: String,
    @SerializedName("idTipoDocumental") val idTipoDocumental: Int,
    @SerializedName("detalle") val detalle: String,
    @SerializedName("cantidadFolios") val cantidadFolios: Int,
    @SerializedName("codigoClasificacion") val codigoClasificacion: String,
    @SerializedName("codigoEstado") val codigoEstado: String
)

fun BlockDocumentSaveEntity.mapToData() = BlockDocumentSaveDto(
    nombre, titulo, usuario, fecha, idTipoDocumental, detalle, cantidadFolios, codigoClasificacion, codigoEstado
)

data class BlockSaveDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("codigo") val codigo: String?,
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("obligatorio") val obligatorio: Boolean?,
    @SerializedName("configuracion") val configuracion: BlockConfigAtributeDto?,
    @SerializedName("idTipoAtributo") val idTipoAtributo: Int?,
    @SerializedName("descTipoAtributo") val descTipoAtributo: String?,
    @SerializedName("idBloque") val idBloque: Int?,
    @SerializedName("activo") val activo: Boolean?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("valor") val valor: Any?,
)

fun BlockSaveDto.mapToDomain() = BlockSaveEntity(
    id,
    codigo,
    nombre,
    descripcion,
    obligatorio,
    configuracion?.mapToDomain(),
    idTipoAtributo,
    descTipoAtributo,
    idBloque,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    valor
)

fun BlockSaveEntity.mapToData() = BlockSaveDto(
    id,
    codigo,
    nombre,
    descripcion,
    obligatorio,
    configuracion?.mapToData(),
    idTipoAtributo,
    descTipoAtributo,
    idBloque,
    activo,
    usuarioCrea,
    fechaCreacion,
    usuarioModifica,
    fechaModifica,
    valor
)

data class BlockBodyDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("atributosPlantilla") val atributosPlantilla: List<BlockDataBodyDto>?,
)

data class BlockDataBodyDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("valor") val valor: String?,
)

fun BlockBodyEntity.mapToData() = BlockBodyDto(
    nombre, atributosPlantilla?.map { it.mapToData() }
)

fun BlockDataBodyEntity.mapToData() = BlockDataBodyDto(
    nombre, valor
)

data class PdfResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: String?,
    @SerializedName("statusCode") val statusCode: Int?
)

data class ResultGetBlockValueResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: List<BlockDto>?,
    @SerializedName("statusCode") val statusCode: Int?,
)