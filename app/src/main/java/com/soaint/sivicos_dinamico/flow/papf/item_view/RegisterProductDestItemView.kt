package com.soaint.sivicos_dinamico.flow.papf.item_view

import android.annotation.SuppressLint
import com.soaint.domain.common.formatDate
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.DestinoLotePapfEntity
import com.soaint.domain.model.DocumentEntity
import com.soaint.domain.model.RegisterProductPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemRegisterProductAddBinding
import com.soaint.sivicos_dinamico.databinding.ItemRegisterProductDestBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class RegisterProductDestItemView(
    private val item: DestinoLotePapfEntity,
) : BindableItem<ItemRegisterProductDestBinding>() {

    @SuppressLint("SetTextI18n")
    override fun bind(
        viewBinding: ItemRegisterProductDestBinding,
        position: Int
    ) = with(viewBinding) {

        textViewNumberDoc.text = item.numeroDocumento.orEmpty()
        textViewName.text = item.razonSocial.orEmpty()
        textViewSucursal.text = item.sucursal.orEmpty()
        textViewDepartment.text = item.departamento.orEmpty()
        textViewCity.text = item.municipio.orEmpty()
        textViewAddress.text = item.direccion.orEmpty()

        textViewPhone.text = item.telefono.orEmpty()
        textViewContact.text = item.contacto.orEmpty()
        textViewDestinyProduct.text = item.descripcionDestinoProducto.orEmpty()
        textViewLote.text = item.lote.orEmpty()
        textViewQuantity.text = item.cantidad.orEmpty().toString()
        textViewMpig.text = item.descripcionUsoMPIG.orEmpty()

    }

    override fun getLayout() = R.layout.item_register_product_dest
}