package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserEntity(
    var sub: String,
    var emailVerified: Boolean,
    var idToken: String,
    var preferredUsername: String,
    var givenName: String,
    var tokenType: String,
    var accessToken: String,
    var refreshToken: String,
    var refreshExpiresIn: Int,
    var notBeforePolicy: Int,
    var scope: String,
    var name: String,
    var sessionState: String,
    var familyName: String,
    var expiresIn: Int,
    var email: String
): Parcelable

@Parcelize
data class UserInformationEntity(
    val ciudad: String,
    val correoElectronico: String,
    val departamento: String,
    val direccion: String,
    val direccionMisional: String,
    val idMisional: Int,
    val numeroIdentidad: String,
    val primerApellido: String,
    val primerNombre: String,
    val segundoApellido: String,
    val segundoNombre: String,
    val tipoIdentidad: String,
    val usuario: String,
    val idPersona: Int,
    val idUsuario: Int,
    val grupoDependencia: String,
    val idDependencia: Int,
    val idFuncionario: Int,
    var cargo: String
): Parcelable

@Parcelize
data class UserRolesEntity(
    val idRol: Int,
    val codigo: String,
    val rol: String,
): Parcelable