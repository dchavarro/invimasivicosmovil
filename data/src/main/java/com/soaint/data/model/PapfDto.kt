package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.ConsecutivePapfModel
import com.soaint.data.room.tables.InfoTramitPapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.ConsecutivePapfEntity
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.InfoTramitePapfSolicitudEntity
import com.soaint.domain.model.TransportObjectPapfEntity

data class InfoTramitePapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosInfoTramitePapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosInfoTramitePapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosInfoTramitePapfDto(
    @SerializedName("idSolicitud") val idSolicitud: String,
    @SerializedName("idClasificacion") val idClasificacion: String? = null,
    @SerializedName("idProducto") val idProducto: String? = null,
)

data class InfoTramitePapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: InfoTramitePapfSolicitudDto,
    @SerializedName("statusCode") val statusCode: Int,
)

data class InfoTramitePapfSolicitudDto(
    @SerializedName("Solicitud") val solicitud: List<InfoTramitePapfDto>?,
)

fun InfoTramitePapfSolicitudDto.mapToDomain() = InfoTramitePapfSolicitudEntity(
    solicitud?.map { it.mapToDomain() }.orEmpty(),
)

data class InfoTramitePapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("NumeroSolicitud") val numeroSolicitud: Int? = 0,
    @SerializedName("Radicado") val radicado: String?,
    @SerializedName("IdCategoriaTramite") val idCategoriaTramite: Int? = 0,
    @SerializedName("DescripcionCategoria") val descripcionCategoria: String?,
    @SerializedName("IdTipoTramite") val idTipoTramite: Int? = 0,
    @SerializedName("TipoTramite") val tipoTramite: String?,
    @SerializedName("TipoImportacion") val tipoImportacion: String?,
    @SerializedName("IdTipoProducto") val idTipoProducto: Int? = 0,
    @SerializedName("TipoProducto") val tipoProducto: String?,
    @SerializedName("Importador") val importador: String?,
    @SerializedName("IdOficinaINVIMA") val idOficinaInvima: String?,
    @SerializedName("NombreDependencia") val nombreDependencia: String?,
    @SerializedName("Estado") val estado: Int? = 0,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("NumeroDocumento") val numeroDocumento: String?,
    @SerializedName("TipoDocumento") val tipoDocumento: String?,
    @SerializedName("DescripcionTipoInspeccion") val descripcionTipoInspeccion: String?,
    @SerializedName("IdSolicitudTramite") val idSolicitudTramite: Int? = 0,
    @SerializedName("CodigoPapf") val codigoPapf: String?,
)

fun InfoTramitePapfDto.mapToDomain() = InfoTramitePapfEntity(
    idSolicitud,
    numeroSolicitud,
    radicado,
    idCategoriaTramite,
    descripcionCategoria,
    idTipoTramite,
    tipoTramite,
    tipoImportacion,
    idTipoProducto,
    tipoProducto,
    importador,
    idOficinaInvima,
    nombreDependencia,
    estado,
    direccion,
    telefono,
    numeroDocumento,
    tipoDocumento,
    descripcionTipoInspeccion,
    idSolicitudTramite,
    codigoPapf,
)

fun InfoTramitePapfDto.mapToDb() = InfoTramitPapfModel(
    idSolicitud.orEmpty(),
    numeroSolicitud,
    radicado,
    idCategoriaTramite,
    descripcionCategoria,
    idTipoTramite,
    tipoTramite,
    tipoImportacion,
    idTipoProducto,
    tipoProducto,
    importador,
    idOficinaInvima,
    nombreDependencia,
    estado,
    direccion,
    telefono,
    numeroDocumento,
    tipoDocumento,
    descripcionTipoInspeccion,
    idSolicitudTramite,
    codigoPapf,
)

fun InfoTramitePapfEntity.mapToData() = InfoTramitePapfDto(
    idSolicitud,
    numeroSolicitud,
    radicado,
    idCategoriaTramite,
    descripcionCategoria,
    idTipoTramite,
    tipoTramite,
    tipoImportacion,
    idTipoProducto,
    tipoProducto,
    importador,
    idOficinaInvima,
    nombreDependencia,
    estado,
    direccion,
    telefono,
    numeroDocumento,
    tipoDocumento,
    descripcionTipoInspeccion,
    idSolicitudTramite,
    codigoPapf,
)

data class CloseInspPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosCloseInsPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosCloseInsPapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosCloseInsPapfDto(
    @SerializedName("idSolicitud") val idSolicitud: String? = null,
    @SerializedName("respuestaRequerimientoIF") val respuestaRequerimientoIF: Boolean? = null,
    @SerializedName("justificacion") val justificacion: String? = null,
    @SerializedName("idUsoMPIG") val idUsoMPIG: Int? = null,
    @SerializedName("idResultadoCIS") val idResultadoCIS: Int? = null,
    @SerializedName("idFirmante") val idFirmante: Int? = null,
    @SerializedName("idIdioma") val idIdioma: Int? = null,
    @SerializedName("idActividad") val idActividad: Int? = null,
    @SerializedName("idTipoProducto") val idTipoProducto: Int? = null,
    @SerializedName("idTipoCertificado") val idTipoCertificado: Int? = null,
    @SerializedName("nombreLaboratorio") val nombreLaboratorio: String? = null,
    @SerializedName("reembarque") val reembarque: String? = null,
    @SerializedName("medidaSanitaria") val medidaSanitaria: String? = null,
    @SerializedName("observaciones") val observaciones: List<ParametrosDescripcionCloseInsPapfDto>?,
)

data class ParametrosDescripcionCloseInsPapfDto(
    @SerializedName("descripcion") val descripcionObservaciones: String?,
)

data class UpdateStatusPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosUpdateStatusPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosUpdateStatusPapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosUpdateStatusPapfDto(
    @SerializedName("estado") val estado: Int?,
    @SerializedName("solicitud") val solicitud: SolicitudPapfDto,
)

data class SolicitudPapfDto(
    @SerializedName("idSolicitud") val idSolicitud: Int,
)

data class DocsActsPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: ParametrosDocsActsPapfDto,
) {
    constructor(ip: String, user: String, parametros: ParametrosDocsActsPapfDto) : this(
        AuditoriaCapitalDto(ip, user),
        parametros
    )
}

data class ParametrosDocsActsPapfDto(
    @SerializedName("idSolicitudPapf") val idSolicitudPapf: String?,
    @SerializedName("idTipoDocumental") val idTipoDocumental: String?,
)

data class DocsActsPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val objectResponse: List<DocsActsPapfDto>? = null,
    @SerializedName("statusCode") val statusCode: Int,
)

data class DocsActsPapfDto(
    @SerializedName("IdDocumentoSincronizacion") val idDocumentoSincronizacion: Int? = 0,
    @SerializedName("IdTipoDocumental") val idTipoDocumental: Int? = 0,
    @SerializedName("IdPlantilla") val idPlantilla: Int? = 0,
    @SerializedName("IdDocumento") val idDocumento: Int? = 0,
    @SerializedName("IdSolicitudPapf") val idSolicitudPapf: Int? = 0,
)

data class SolicitudPapfBodyDto(
    @SerializedName("Auditoria") val cabecera: AuditoriaCapitalDto,
    @SerializedName("Parametros") val parametros: SolicitudPapfDto,
) {
    constructor(ip: String, user: String, idSolicitud: Int) : this(
        AuditoriaCapitalDto(ip, user),
        SolicitudPapfDto(idSolicitud)
    )
}

data class GenerateConsecutivePapfBodyDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("Parametros") val entrada: GenerateConsecutivePapfDto,
) {
    constructor(ip: String, user: String, parametros: GenerateConsecutivePapfDto) : this(
        AuditoriaDto(ip, user),
        parametros
    )
}

data class GenerateConsecutivePapfDto(
    @SerializedName("codigo") val codigo: String,
    @SerializedName("preview") val preview: String? = null,
)

data class GenerateConsecutivePapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val objectResponse: ConsecutivePapfDto? = null,
    @SerializedName("statusCode") val statusCode: Int,
)

data class ConsecutivePapfDto(
    @SerializedName("consecutivo") val consecutivo: String? = null,
)

fun ConsecutivePapfDto.mapToDomain() = ConsecutivePapfEntity (
    consecutivo
)

fun ConsecutivePapfDto.mapToDb(idSolicitud: Int?) = ConsecutivePapfModel (
    consecutivo,
    idSolicitud = idSolicitud
)