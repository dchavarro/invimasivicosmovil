package com.soaint.data.model


import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.TransportBodyModel
import com.soaint.data.room.tables.TransportModel
import com.soaint.data.room.tables.TransportOffcialModel
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity

data class TransportResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: TransportDto,
    @SerializedName("statusCode") val statusCode: Int
)

data class TransportDto(
    @SerializedName("direccionMisional") val direccionMisional: String?,
    @SerializedName("numeroOficioComisorio") val numeroOficioComisorio: String?,
    @SerializedName("nombreEmpresa") val nombreEmpresa: String?,
    @SerializedName("nitEmpresa") val nitEmpresa: String?,
    @SerializedName("direccionEmpresa") val direccionEmpresa: String?,
    @SerializedName("funcionarios") val funcionarios: List<OfficialDto>?,
)

data class OfficialDto(
    @SerializedName("nombreCompleto") val nombreApellidos: String?,
    @SerializedName("celular") val celular: String?,
    @SerializedName("correo") val correoElectronico: String?
)

fun TransportDto.mapToDomain() = TransportEntity(
    direccionMisional,
    numeroOficioComisorio,
    nombreEmpresa,
    nitEmpresa,
    direccionEmpresa,
    funcionarios.orEmpty().map { it.mapToDomain() },
)

fun OfficialDto.mapToDomain() = OfficialEntity(
    nombreApellidos,
    celular,
    correoElectronico
)


fun TransportDto.mapToDb(idVisit: String) = TransportModel(
    direccionMisional,
    numeroOficioComisorio,
    nombreEmpresa,
    nitEmpresa,
    direccionEmpresa,
    idVisit
)

fun OfficialDto.mapToDb(idVisita: String) = TransportOffcialModel(
    nombreApellidos,
    celular,
    correoElectronico,
    idVisita
)

data class TransportBodyDto(
    @SerializedName("idTipoTransporte") val idTipoTransporte: Int? = 9,
    @SerializedName("tipoTransporte") val tipoTransporte: String? = EMPTY_STRING,
    @SerializedName("motivoAppMediSanitaria") val motivoAppMediSanitaria: String? = EMPTY_STRING,
    @SerializedName("dimensionesAproxMercancia") val dimensionesAproxMercancia: String? = EMPTY_STRING,
    @SerializedName("numPiezasAproximadamente") val numPiezasAproximadamente: String? = EMPTY_STRING,
    @SerializedName("volumenAproximado") val volumenAproximado: String? = EMPTY_STRING,
    @SerializedName("pesoAproximado") val pesoAproximado: String? = EMPTY_STRING,
    @SerializedName("tipoMercancia") val tipoMercancia: String? = EMPTY_STRING,
    @SerializedName("numAuxNecesaCargue") val numAuxNecesaCargue: String? = EMPTY_STRING,
    @SerializedName("valorAproxDecomiso") val valorAproxDecomiso: String? = EMPTY_STRING,
    @SerializedName("fechaRecogida") val fechaRecogida: String? = EMPTY_STRING,
    @SerializedName("correoElectronico") val correoElectronico: String? = EMPTY_STRING,
    @SerializedName("idVisita") val idVisita: String? = EMPTY_STRING,
    @SerializedName("activo") val activo: Boolean = true,
    @SerializedName("usuarioCrea") val usuarioCrea: String? = EMPTY_STRING,
    @SerializedName("fechaCreacion") val fechaCreacion: String? = EMPTY_STRING,
    @SerializedName("fechaModifica") val fechaModifica: String? = EMPTY_STRING,
)

fun TransportBodyEntity.mapToData() = TransportBodyDto(
    idTipoTransporte,
    tipoTransporte,
    motivoAppMediSanitaria,
    dimensionesAproxMercancia,
    numPiezasAproximadamente,
    volumenAproximado,
    pesoAproximado,
    tipoMercancia,
    numAuxNecesaCargue,
    valorAproxDecomiso,
    fechaRecogida,
    correoElectronico,
    idVisita,
    activo,
    usuarioCrea,
    fechaCreacion,
    fechaModifica
)

fun TransportBodyDto.mapToDb() = TransportBodyModel(
    idTipoTransporte.orZero(),
    tipoTransporte.orEmpty(),
    motivoAppMediSanitaria.orEmpty(),
    dimensionesAproxMercancia.orEmpty(),
    numPiezasAproximadamente.orEmpty(),
    volumenAproximado.orEmpty(),
    pesoAproximado.orEmpty(),
    tipoMercancia.orEmpty(),
    numAuxNecesaCargue.orEmpty(),
    valorAproxDecomiso.orEmpty(),
    fechaRecogida.orEmpty(),
    correoElectronico.orEmpty(),
    idVisita.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
)

fun TransportBodyDto.mapToDomain(isLocal: Boolean) = TransportBodyEntity(
    idTipoTransporte.orZero(),
    tipoTransporte.orEmpty(),
    motivoAppMediSanitaria.orEmpty(),
    dimensionesAproxMercancia.orEmpty(),
    numPiezasAproximadamente.orEmpty(),
    volumenAproximado.orEmpty(),
    pesoAproximado.orEmpty(),
    tipoMercancia.orEmpty(),
    numAuxNecesaCargue.orEmpty(),
    valorAproxDecomiso.orEmpty(),
    fechaRecogida.orEmpty(),
    correoElectronico.orEmpty(),
    idVisita.orEmpty(),
    activo,
    usuarioCrea.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    isLocal
)

data class TransportBodyResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<TransportBodyDto>,
    @SerializedName("statusCode") val statusCode: Int
)
