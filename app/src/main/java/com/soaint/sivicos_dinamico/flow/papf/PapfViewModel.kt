package com.soaint.sivicos_dinamico.flow.papf

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.flow.papf.views.close_insp.OnViewModelEvent
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class OnPapfViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnPapfViewModelEvent()
    data class OnViewModelReady(val infoTramit: InfoTramitePapfEntity) : OnPapfViewModelEvent()
    data class OnLoadingError(val error: String) : OnPapfViewModelEvent()
    data class onViewModelReadyCheck(val isValid: List<String>) : OnPapfViewModelEvent()
    data class onViewModelReadyLegal(val isValid: Boolean?) : OnPapfViewModelEvent()
    object onViewModelReadyClose : OnPapfViewModelEvent()
    object OnErrorAct015 : OnPapfViewModelEvent()
    object onViewResponseRequestIF : OnPapfViewModelEvent()
    data class OnViewModelValidateActs(val acts: List<String>) : OnPapfViewModelEvent()
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) : OnPapfViewModelEvent()
}

class PapfViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
    private val syncDataUseCase: SyncDataUseCase,
    private val validationUseCase: ValidationUseCase,
    private val taskUseCase: TaskUseCase,
    private val taskPapfUseCase: TaskPapfUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnPapfViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _idRequest = MutableLiveData<Int>()
    val idRequest = _idRequest.asLiveData()

    private val _infoTramit = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramit = _infoTramit.asLiveData()

    private val _isTaskCis = MutableLiveData(false)
    val isTaskCis = _isTaskCis.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    fun init(id: Int) = execute {
        _idRequest.value = id
        getTask(id)
    }

    suspend fun getTask(idSolicitud: Int) {
        val task = taskPapfUseCase.loadTaskLocal(idSolicitud)
        _isTaskCis.value = task.getOrNull()?.estado == STATUS_PAPF_CIS
    }
    fun getInfoTramit(infoTramit: InfoTramitePapfEntity) {
        _infoTramit.value = infoTramit
    }

    fun stopLocation() {
        userUseCase.logout()
    }

    fun logout() {
        userUseCase.logout()
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

   fun validateResponseRequestIF () = execute{
        val clasificationTramit =
            papfUseCase.loadDataCloseInsLocal(_infoTramit.value?.idSolicitud.orZero()).getOrNull()
        if (clasificationTramit?.respuestaRequerimientoIF == true) {
            _event.value = OnPapfViewModelEvent.onViewResponseRequestIF
        }else{
            validateActs()
        }
    }
    fun validateActs() = execute {
        val codigoPlantilla = when (_infoTramit.value?.idTipoProducto) {
            ID_MISIONAL_PAPF_ALIMENTOS -> IVC_INS_FM055
            ID_MISIONAL_PAPF_BEBIDAS -> IVC_INS_FM156
            else -> EMPTY_STRING
        }
        val result =
            _infoTramit.value?.idSolicitud?.let {
                validationUseCase.validationActsFinished(
                    it,
                    listOf(codigoPlantilla)
                )
            }
        _event.value =
            OnPapfViewModelEvent.OnViewModelValidateActs(result?.getOrNull().orEmpty().distinct())
    }

/*    fun onObservation(observation: String) = execute {
        val result = validationUseCase.addObservation(_visitData.value?.firstOrNull()?.id.toString(), observation)
        _event.value = when (result.exceptionOrNull()) {
            is WithoutConnectionException -> OnVisitViewModelEvent.onSnack(false, R.string.no_internet)
            is Exception -> OnVisitViewModelEvent.onSnack(false, R.string.error)
            else -> OnVisitViewModelEvent.onSnack(true, R.string.copy_update_success)
        }
    }

    fun validateMssp() = execute {
        val result = validationUseCase.validationMss(_visitData.value?.firstOrNull()?.id.toString(), MSSP)
        if (result.isSuccess) {
            validateMsse(result.getOrNull().orEmpty())
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validateMsse(list: List<String>) = execute {
        val result = validationUseCase.validationMss(_visitData.value?.firstOrNull()?.id.toString(), MSSE)
        if (result.isSuccess) {
            val data = result.map {
                    it.onEach { d ->
                        list.plus(d)
                    }
                list
            }.getOrNull().orEmpty()

            if (_visitData.value?.firstOrNull()?.aplicaMedidaSanitaria == true) {
                validationUpdateMssList(data.distinct())
            } else {
                _event.value = OnVisitViewModelEvent.onViewModelReadyCheck(emptyList())
            }

        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validationUpdateMssList(list: List<String>) = execute {
        val result = validationUseCase.validationUpdateMssList(_visitData.value?.firstOrNull()?.id.toString(), MSSP)
        if (result.isSuccess) {
            val data = result.map {
                it.onEach { d ->
                    list.plus(d)
                }
                list
            }.getOrNull().orEmpty()
            _event.value = OnVisitViewModelEvent.onViewModelReadyCheck(data.distinct())
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validation() = execute {
        if (_visit.value?.codigoRazon == REASON_RAVI_INPER) validateActO15()
        else validationLegalAtiende()
    }

    fun validationLegalAtiende() = execute {
        val result = validationUseCase.validationLegalAtiende(_visitData.value?.firstOrNull()?.id.toString())
        _event.value =  if (result.isSuccess) OnVisitViewModelEvent.onViewModelReadyLegal(result.getOrNull())
        else OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
    }

    fun validateActO15() = execute {
        val result = validationUseCase.validationAct015(_visitData.value?.firstOrNull()?.id.toString())
        if (result.isSuccess) {
            if (result.getOrDefault(false) == true) {
                validationLegalAtiende()
            } else {
                _event.value = OnVisitViewModelEvent.OnErrorAct015
            }
        } else {
            _event.value = OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validateCloseVisit() = execute {
        if (visit.value?.aplicaMedidaSanitaria == true) {
            if (manageMssUseCase.countMssLocal(visit.value?.empresa?.razonSocial.orEmpty()).getOrNull().orEmpty() > 0) {
                validateFinish()
            } else {
                _event.value = OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_mss)
            }
        } else {
            validateFinish()
        }
    }

    fun loadGroup() = execute  {
        _group.value = visitUseCase.loadGroupLocal(visit.value?.id.toString()).getOrNull()
    }


    fun validateConcept() :Boolean {
        loadGroup()
        return if (visit.value?.tramite == null)
            if (!_group.value.isNullOrEmpty()) _group.value!!.any { it.id == ID_GROUP_PLANTAS_ANIMAL }
            else false
        else true
    }

    fun validateFinish() {
        val visit = _visitData.value?.firstOrNull()
        // TODO aqui van campos obligaorios para cerrar visita
        // si aplica campo idConceptoSanitario
         if (visit?.idAccionSeguir == null) {
             _event.value = OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_action)
        } else if (visit.idResultado == null) {
             _event.value = OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_result)
        } else if (visit.requiereReprogramarVisita == null) {
             _event.value = OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_reprograma)
         } else if (validateConcept() == true && visit.idConceptoSanitario == null) {
             _event.value = OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_concept)
         } else {
             updateTask()
         }
    }

    fun updateTask() = execute {
        val result = taskUseCase.loadTaskLocal(_visitData.value?.firstOrNull()?.idTarea.orEmpty())
        val body = result.getOrNull()?.copy(
            activo = false,
            usuarioModifica = userUseCase.getUserName()
        )
        if (result.isSuccess) {
            val update = taskUseCase.updateTaskLocal(body)
            if (update.isSuccess) updateVisit()
        } else {
            _event.value = OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun updateVisit() = execute {
        val body = _visitData.value?.firstOrNull()?.copy(
            fechaRealizo = getDateHourWithOutFormat(),
            fechaModifica = getDateHourWithOutFormat(),
        )
        val result = visitUseCase.updateVisitLocal(body, true)
        _event.value = if (result.isSuccess) {
            OnVisitViewModelEvent.onViewModelReadyClose
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun refreshVisit() = execute {
        val result =
            _visit.value?.idTarea?.let { visitUseCase.loadVisitLocal(it).getOrNull().orEmpty() }
        if (!result.isNullOrEmpty()) {
            _visitData.value = result
            _visit.value = result.firstOrNull()
        }
    }*/
}