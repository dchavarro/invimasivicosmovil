package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.CountriesModel
import com.soaint.data.room.tables.DepartmentModel
import com.soaint.data.room.tables.TownModel
import com.soaint.data.room.tables.TypeDaoModel
import com.soaint.domain.model.*

data class TypeDaoResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<TypeDaoDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class TypeDaoDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("descripcion") val descripcion: String? = EMPTY_STRING,
)

fun TypeDaoDto.mapToDomain(tipoDao: String) = TypeDaoEntity(
    id, codigo, descripcion, tipoDao
)

fun TypeDaoDto.mapToDb(tipoDao: String) = TypeDaoModel(
    id, codigo.orEmpty(), descripcion.orEmpty(), tipoDao
)


data class CountriesResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<CountriesDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class CountriesDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("nombre") val nombre: String? = EMPTY_STRING,
    @SerializedName("idZonaGeografica") val idZonaGeografica: Int? = 0,
)

fun CountriesDto.mapToDomain() = CountriesEntity(
    id, codigo, nombre, idZonaGeografica
)

fun CountriesDto.mapToDb() = CountriesModel(
    id, codigo.orEmpty(), nombre.orEmpty(), idZonaGeografica
)


data class DepartmentResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<DepartmentDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class DepartmentDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("nombre") val nombre: String? = EMPTY_STRING,
    @SerializedName("idPais") val idPais: Int? = 0,
)

fun DepartmentDto.mapToDomain() = DepartmentEntity(
    id, codigo, nombre, idPais
)

fun DepartmentDto.mapToDb() = DepartmentModel(
    id, codigo.orEmpty(), nombre.orEmpty(), idPais
)


data class TownResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<TownDto>?,
    @SerializedName("statusCode") val statusCode: Int
)

data class TownDto(
    @SerializedName("id") val id: Int? = 0,
    @SerializedName("codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("nombre") val nombre: String? = EMPTY_STRING,
    @SerializedName("idDepartamento") val idDepartamento: Int? = 0,
)

fun TownDto.mapToDomain() = TownEntity(
    id, codigo, nombre, idDepartamento
)

fun TownDto.mapToDb() = TownModel(
    id, codigo.orEmpty(), nombre.orEmpty(), idDepartamento
)