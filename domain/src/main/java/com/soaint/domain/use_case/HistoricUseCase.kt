package com.soaint.domain.use_case

import com.soaint.domain.repository.HistoricRepository
import javax.inject.Inject

class HistoricUseCase @Inject constructor(
    private val historicRepository: HistoricRepository
) {

    val historico get() = historicRepository.historico

    suspend fun loadHistorico(razonSocial: String) = historicRepository.loadHistoric(razonSocial)

    suspend fun loadHistoricoLocal(razonSocial: String) = historicRepository.loadHistoricLocal(razonSocial)
}