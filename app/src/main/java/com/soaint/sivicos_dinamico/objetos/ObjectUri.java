package com.soaint.sivicos_dinamico.objetos;

import android.net.Uri;

public class ObjectUri extends Object
{
    Uri uri;
    String ruta;

    public Uri getUri()
    {
        return uri;
    }

    public void setUri(Uri uri)
    {
        this.uri = uri;
    }

    public String getRuta()
    {
        return ruta;
    }

    public void setRuta(String ruta)
    {
        this.ruta = ruta;
    }
}
