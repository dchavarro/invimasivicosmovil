package com.soaint.sivicos_dinamico.manager

import android.content.Context
import com.soaint.domain.common.getBase64FromPath
import com.soaint.domain.common.getFileFromBase64AndSave
import com.soaint.domain.provider.FileProvider
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileProviderImpl @Inject constructor(
    private val context: Context
) : FileProvider {

    override suspend fun saveFile(fileName: String?, content: String): String? {
        val path = getFileFromBase64AndSave(context, content, fileName.orEmpty(), "txt")
        return path
    }

    override suspend fun getFile(fileName: String?): String? {
        val content = getBase64FromPath(fileName)
        return content
    }
}