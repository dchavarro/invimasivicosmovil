package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.UserEntity

data class UserDto(
    @SerializedName("sub") var sub: String?,
    @SerializedName("email_verified") var emailVerified: Boolean?,
    @SerializedName("id_token") var id_token: String?,
    @SerializedName("preferred_username") var preferredUsername: String?,
    @SerializedName("given_name") var givenName: String?,
    @SerializedName("token_type") var tokenType: String?,
    @SerializedName("access_token") var accessToken: String?,
    @SerializedName("refresh_token") var refreshToken: String?,
    @SerializedName("refresh_expires_in") var refreshExpiresIn: Int?,
    @SerializedName("not-before-policy") var notBeforePolicy: Int?,
    @SerializedName("scope") var scope: String?,
    @SerializedName("name") var name: String?,
    @SerializedName("session_state") var sessionState: String?,
    @SerializedName("family_name") var familyName: String?,
    @SerializedName("expires_in") var expiresIn: Int?,
    @SerializedName("email") var email: String?
)

fun UserDto.mapToDomain() = UserEntity(
    sub.orEmpty(),
    emailVerified ?: true,
    id_token.orEmpty(),
    preferredUsername.orEmpty(),
    givenName.orEmpty(),
    tokenType.orEmpty(),
    accessToken.orEmpty(),
    refreshToken.orEmpty(),
    refreshExpiresIn.orEmpty(),
    notBeforePolicy.orEmpty(),
    scope.orEmpty(),
    name.orEmpty(),
    sessionState.orEmpty(),
    familyName.orEmpty(),
    expiresIn.orEmpty(),
    email.orEmpty()
)