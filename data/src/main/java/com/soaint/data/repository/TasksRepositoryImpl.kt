package com.soaint.data.repository

import com.soaint.data.api.GeneralesApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.common.orZero
import com.soaint.data.model.TaskDto
import com.soaint.data.model.mapToData
import com.soaint.data.model.mapToDb
import com.soaint.data.model.mapToDomain
import com.soaint.data.room.dao.TasksDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.model.*
import com.soaint.domain.repository.TaskRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class TasksRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val tasksApi: GeneralesApi,
    private val tasksDao: TasksDao
) : TaskRepository, BaseRepository() {

    private val _tasks = MutableStateFlow<List<TaskEntity>?>(null)
    override val tasks get() = _tasks.asStateFlow()

    //Carga de Tareas
    override suspend fun loadTasks(): Result<List<TaskEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val tasks = tasksApi.getTasks(usuario).data
        deleteDao()
        saveDao(tasks, usuario)
        Result.success(tasks.map { it.mapToDomain() })
    }

    //Carga de Tareas DB Local
    override suspend fun loadTasksLocal(): Result<List<TaskEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val tasks = tasksDao.selectTaskByUser(usuario)
        Result.success(tasks.map { it.mapToDomain() })
    }

    //Carga de Tareas DB Local
    override suspend fun loadTasksLocalOnlyActive(): Result<List<TaskEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val tasks = tasksDao.selectTaskOnlyActive(usuario)
        Result.success(tasks.map { it.mapToDomain() })
    }

    //Carga de Tareas DB Local
    override suspend fun loadTaskLocal(idTask: Int): Result<TaskEntity> = execute {
        val task = tasksDao.selectTaskById(idTask)
        task?.let { Result.success(it.mapToDomain()) }?: Result.failure(Throwable())
    }

    //Guardar Tareas en DB local
    private suspend fun saveDao(data: List<TaskDto>, usuario: String) {
        data.onEach { task ->
            val model = task.mapToDb(usuario)
            tasksDao.insertTask(model)
        }
    }

    //Eliminar Tareas en DB local
    private suspend fun deleteDao() {
        tasksDao.deleteAllTask()
    }

    //Update Tarea
    override suspend fun updateTask(task: TaskBodyEntity): Result<Unit> = execute {
        val result = tasksApi.updateTask(task.mapToData())
        Result.success(result)
    }

    //Update Tarea en DB local
    override suspend fun updateTaskLocal(task: TaskEntity?): Result<Unit> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val result = task?.mapToData()?.mapToDb(usuario)?.let { tasksDao.updateTask(it) }
        Result.success(result)
    } as Result<Unit>

    //Update Tarea
    override suspend fun createTask(task: TaskMssBodyEntity): Result<TaskEntity?> = execute {
        val result = tasksApi.createTask(task.mapToData()).data.mapToDomain()
        Result.success(result)
    }

    //Crear antecedente
    override suspend fun createTaskCompany(body: TaskCompanyBodyEntity): Result<Unit> = execute {
        val result = tasksApi.createTaskCompany(listOf(body.mapToData()))
        Result.success(result)
    }
}