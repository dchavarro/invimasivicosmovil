package com.soaint.data.api

import com.soaint.data.model.PdfResponseDto
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ProxyApi {

    @POST("v1/plantilla/reemplazaEtiquetas")
    suspend fun getPdf(
        @Query("idPlantilla") idPlantilla: String,
        @Query("idVisita") idVisita: String,
        @Query("indVistaPrevia") indVistaPrevia: Boolean,
        @Query("usuarioCrea") usuarioCrea: String,
        @Query("idTramite") idTramite: Int?,
        @Body body: Map<String, @JvmSuppressWildcards Any?>,
    ): PdfResponseDto

    @POST("v1/plantilla/reemplazaEtiquetas")
    suspend fun getPdfPapf(
        @Query("idPlantilla") idPlantilla: String,
        @Query("idSolicitudPafp") idSolicitudPafp: Int,
        @Query("indVistaPrevia") indVistaPrevia: Boolean,
        @Query("usuarioCrea") usuarioCrea: String,
        @Body body: Map<String, @JvmSuppressWildcards Any?>,
    ): PdfResponseDto
}