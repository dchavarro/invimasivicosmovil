package com.soaint.sivicos_dinamico.common

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import com.soaint.sivicos_dinamico.model.StartActivityEvent
import dagger.android.support.DaggerDialogFragment
import javax.inject.Inject

abstract class BaseDialogFragment : DaggerDialogFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory


    protected fun startActivity(startActivityEvent: StartActivityEvent) {
        val intent = Intent(context, startActivityEvent.activity).apply {
            startActivityEvent.bundle?.let { putExtras(it) }
            flags = startActivityEvent.flag
        }
        startActivityForResult(intent, startActivityEvent.code)
    }

    /**
     * Loading
     */



}