package com.soaint.sivicos_dinamico.factories.controlsFactory.controles

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import com.soaint.domain.model.BlockAtributePlantillaEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.ControlInterface


class Lista: ControlInterface {

    var valorSeleccionado: String = ""
    lateinit var btn: Button
    lateinit var classContext: Context
    var isVisible:Boolean = false

    override fun createControl(
        context: Context,
        control: BlockAtributePlantillaEntity,
        valorAlmacenado: String,
        isEditable: () -> Boolean,
        action: (suspend () -> Any?)?
    ): View {

        classContext = context
        valorSeleccionado = valorAlmacenado

        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.view_lista, null)
        val lblText = view.findViewById<TextView>(R.id.lblText)
        val imageViewHelp = view.findViewById<ImageView>(R.id.imageView_help)
        val textViewHelp = view.findViewById<TextView>(R.id.textView_help)

        imageViewHelp.isVisible = !control.configuracion?.mensajeAyuda.isNullOrEmpty()
        imageViewHelp.setOnClickListener {
            isVisible = !isVisible
            textViewHelp.isVisible = isVisible
            textViewHelp.text = if (isVisible) control.configuracion?.mensajeAyuda else null
        }

        lblText.text = control.descripcion
        btn = view.findViewById<Button>(R.id.btnLista)
        val array: ArrayList<String> = arrayListOf()

        for (configuracion in control.configuracion?.lista.orEmpty()) {
            array.add(configuracion.nombre.orEmpty())
            if (configuracion.seleccionado == true) {
                valorSeleccionado = configuracion.nombre.orEmpty()
            }
        }

        if (valorSeleccionado.isEmpty()) {
            btn.text = context.getString(R.string.seleccione)
        } else {
            btn.text = valorSeleccionado
        }

        btn.setOnSingleClickListener {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(R.string.seleccione)
                    .setItems(array.toTypedArray(),
                            DialogInterface.OnClickListener { dialog, which ->
                                btn.text = array[which].toString()
                                valorSeleccionado = array[which].toString()
                            })

            val alertDialog: AlertDialog = builder.create()
            alertDialog.show();
        }

        btn.isEnabled = isEditable() && !(valorAlmacenado.isNotEmpty() && control.configuracion?.bloquearConInformacion == true)
        return view
    }

    override fun getData(): String {
        return valorSeleccionado
    }

    override fun showError(mensaje: String?) {
        btn.setBackgroundColor(classContext.getColor(R.color.colorRed))
    }

    override fun clearError() {
        btn.setBackgroundColor(classContext.getColor(R.color.colorPrimary))
    }
}