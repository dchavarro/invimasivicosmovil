package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*

@Entity(tableName = "detail_product_papf_table")
class DetailProductPapfModel(
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int? = 0,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int? = 0,
    @ColumnInfo(name = "expediente") val expediente: String?,
    @ColumnInfo(name = "tieneRegistro") val tieneRegistro: Boolean?,
    @ColumnInfo(name = "registroSanitario") val registroSanitario: String?,
    @ColumnInfo(name = "fechaRegistro") val fechaRegistro: String?,
    @ColumnInfo(name = "estado") val estado: String?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int? = 0,
    @ColumnInfo(name = "clasificacionProducto") val clasificacionProducto: String?,
    @ColumnInfo(name = "nombreProducto") val nombreProducto: String?,
    @ColumnInfo(name = "grupoProducto") val grupoProducto: String?,
    @ColumnInfo(name = "categoriaAlimento") val categoriaAlimento: String?,
    @ColumnInfo(name = "subCategoriaAlimento") val subCategoriaAlimento: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DetailProductPapfModel.mapToDomain() = DetailProductPapfEntity(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    expediente.orEmpty(),
    tieneRegistro,
    registroSanitario.orEmpty(),
    fechaRegistro.orEmpty(),
    estado.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto.orEmpty(),
    nombreProducto.orEmpty(),
    grupoProducto.orEmpty(),
    categoriaAlimento.orEmpty(),
    subCategoriaAlimento.orEmpty(),
)

@Entity(tableName = "subpartida_papf_table")
class SubPartidaPapfModel(
    @ColumnInfo(name = "subPartida") val subPartida: String?,
    @ColumnInfo(name = "descripcion") val descripcion: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun SubPartidaPapfModel.mapToDomain() = SubPartidaPapfEntity(
    subPartida.orEmpty(),
    descripcion.orEmpty(),
)

@Entity(tableName = "expedidor_papf_table")
class ExpedidorPapfModel(
    @ColumnInfo(name = "idExpedidor") val idExpedidor: Int? = 0,
    @ColumnInfo(name = "razonSocial") val razonSocial: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "pais") val pais: String?,
    @ColumnInfo(name = "departamento") val departamento: String?,
    @ColumnInfo(name = "municipio") val municipio: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "codigoISOExpedidor") val codigoISOExpedidor: String?,
    @ColumnInfo(name = "codigoPostalExpedidor") val codigoPostalExpedidor: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun ExpedidorPapfModel.mapToDomain() = ExpedidorPapfEntity(
    idExpedidor.orEmpty(),
    razonSocial.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOExpedidor.orEmpty(),
    codigoPostalExpedidor.orEmpty()
)

@Entity(tableName = "fabricante_papf_table")
class FabricantePapfModel(
    @ColumnInfo(name = "idEmpresaFabricante") val idEmpresaFabricante: Int? = 0,
    @ColumnInfo(name = "fabricante") val fabricante: String?,
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "direccion") val direccion: String?,
    @ColumnInfo(name = "pais") val pais: String?,
    @ColumnInfo(name = "departamento") val departamento: String?,
    @ColumnInfo(name = "municipio") val municipio: String?,
    @ColumnInfo(name = "telefono") val telefono: String?,
    @ColumnInfo(name = "codigoISOFabricante") val codigoISOFabricante: String?,
    @ColumnInfo(name = "codigoPostalFabricante") val codigoPostalFabricante: String?,
    @ColumnInfo(name = "idPlantaAutorizada") val idPlantaAutorizada: String?,
    @ColumnInfo(name = "plantaAutorizada") val plantaAutorizada: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun FabricantePapfModel.mapToDomain() = FabricantePapfEntity(
    idEmpresaFabricante.orEmpty(),
    fabricante.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    pais.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    telefono.orEmpty(),
    codigoISOFabricante.orEmpty(),
    codigoPostalFabricante.orEmpty(),
    idPlantaAutorizada.orEmpty(),
    plantaAutorizada.orEmpty(),
)


@Entity(tableName = "info_complementaria_papf_table")
class InfoComplementariaPapfModel(
    @ColumnInfo(name = "requiereInformacionComplementaria") val requiereInformacionComplementaria: Boolean?,
    @ColumnInfo(name = "especie") val especie: String?,
    @ColumnInfo(name = "estadoMateria") val estadoMateria: String?,
    @ColumnInfo(name = "codigoProductoSA") val codigoProductoSA: String?,
    @ColumnInfo(name = "certificadoComo") val certificadoComo: String?,
    @ColumnInfo(name = "fechaFabricacion") val fechaFabricacion: String?,
    @ColumnInfo(name = "consumidorFinal") val consumidorFinal: Boolean?,
    @ColumnInfo(name = "naturalezaMercancia") val naturalezaMercancia: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun InfoComplementariaPapfModel.mapToDomain() = InfoComplementariaPapfEntity(
    requiereInformacionComplementaria,
    especie.orEmpty(),
    estadoMateria.orEmpty(),
    codigoProductoSA.orEmpty(),
    certificadoComo.orEmpty(),
    fechaFabricacion.orEmpty(),
    consumidorFinal,
    naturalezaMercancia.orEmpty(),
)

@Entity(tableName = "destinatario_papf_table")
class DestinatarioPapfModel(
    @ColumnInfo(name = "Pais") val pais: String?,
    @ColumnInfo(name = "Ciudad") val ciudad: String?,
    @ColumnInfo(name = "Nombre") val nombre: String?,
    @ColumnInfo(name = "Direccion") val direccion: String?,
    @ColumnInfo(name = "Telefono") val telefono: String?,
    @ColumnInfo(name = "CodigoPostal") val codigoPostal: String?,
    @ColumnInfo(name = "CodigoISO") val codigoISO: String?,
    @ColumnInfo(name = "PlantaAutorizadaDestino") val plantaAutorizadaDestino: String?,
    @ColumnInfo(name = "Autoridad") val autoridad: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idProductoSolicitud") val idProductoSolicitud: Int?,
    @ColumnInfo(name = "idClasificacionProducto") val idClasificacionProducto: Int?,
    @ColumnInfo(name = "typeList") val typeList: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DestinatarioPapfModel.mapToDomain() = DestinatarioPapfEntity(
    pais.orEmpty(),
    ciudad.orEmpty(),
    nombre.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    codigoPostal.orEmpty(),
    codigoISO,
    plantaAutorizadaDestino.orEmpty(),
    autoridad.orEmpty(),
    typeList.orEmpty(),
)