package com.soaint.data.repository

import android.content.SharedPreferences
import com.soaint.data.di.SECRET_SHARED_PREFERENCES
import com.soaint.domain.repository.SecretPreferencesRepository
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class SecretPreferencesRepositoryImpl @Inject constructor(
    @Named(SECRET_SHARED_PREFERENCES) private val sharedPreferences: SharedPreferences?,
) : SecretPreferencesRepository {

    override fun setString(preferenceKey: String, value: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString(preferenceKey, value)
        editor?.commit()
    }

    override fun getString(preferenceKey: String, defaultValue: String): String {
        return sharedPreferences?.getString(preferenceKey, defaultValue) ?: defaultValue
    }
}