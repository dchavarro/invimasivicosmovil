package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.*

@Dao
interface PapfDao {

    //INFO TRAMITE
    @Query("SELECT * FROM info_tramit_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectInfoTramit(idSolicitud: Int): List<InfoTramitPapfModel>

    @Insert(entity = InfoTramitPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInfoTramit(task: InfoTramitPapfModel)

    @Query("DELETE FROM info_tramit_papf_table")
    suspend fun deleteAllInfoTramit()

    @Query("DELETE FROM info_tramit_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteInfoTramitById(idSolicitud: Int)

    //fACTURAS COMERCIALES
    @Query("SELECT * FROM invoice_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectInvoice(idSolicitud: Int): List<InvoicePapfModel>

    @Insert(entity = InvoicePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInvoice(task: InvoicePapfModel)

    @Query("DELETE FROM invoice_papf_table")
    suspend fun deleteAllInvoice()

    @Query("DELETE FROM invoice_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteInvoiceById(idSolicitud: Int)

    //DOC TRANSPORTE
    @Query("SELECT * FROM transport_doc_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectTransportDoc(idSolicitud: Int): List<TransportDocPapfModel>?

    @Insert(entity = TransportDocPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransportDoc(task: TransportDocPapfModel)

    @Query("DELETE FROM transport_doc_papf_table")
    suspend fun deleteAllTransportDoc()

    @Query("DELETE FROM transport_doc_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteTransportDocById(idSolicitud: Int)

    //COM TRANSPORTE
    @Query("SELECT * FROM transport_com_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectTransportCom(idSolicitud: Int): List<TransportComPapfModel>?

    @Insert(entity = TransportComPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransportCom(task: TransportComPapfModel)

    @Query("DELETE FROM transport_com_papf_table")
    suspend fun deleteAllTransportCom()

    @Query("DELETE FROM transport_com_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteTransportComById(idSolicitud: Int)

    //registrar productos
    @Query("SELECT * FROM register_product_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectRegisterProduct(idSolicitud: Int): List<RegisterProductPapfModel>?

    @Insert(entity = RegisterProductPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRegisterProduct(task: RegisterProductPapfModel)

    @Query("DELETE FROM register_product_papf_table")
    suspend fun deleteAllRegisterProduct()

    @Query("DELETE FROM register_product_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteRegisterProductById(idSolicitud: Int)

    //destino lote
    @Query("SELECT * FROM register_product_destino_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectRegisterProductDestino(idSolicitud: Int): List<DestinoLotePapfModel>?

    @Insert(entity = DestinoLotePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRegisterProductDestino(task: DestinoLotePapfModel)

    @Query("DELETE FROM register_product_destino_papf_table")
    suspend fun deleteAllRegisterProductDestino()

    @Query("DELETE FROM register_product_destino_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteRegisterProductDestinoById(idSolicitud: Int)

    //documentos
    @Query("SELECT * FROM register_product_document_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectRegisterProductDocument(idSolicitud: Int): List<DocumentPapfModel>?

    @Insert(entity = DocumentPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRegisterProductDocument(task: DocumentPapfModel)

    @Query("DELETE FROM register_product_document_papf_table")
    suspend fun deleteAllRegisterProductDocument()

    @Query("DELETE FROM register_product_document_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteRegisterProductDocumentById(idSolicitud: Int)


    //CERTIFICADO SANITARIO
    @Query("SELECT * FROM sanitary_certificate_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectSanitaryCertificate(idSolicitud: Int): List<SanitaryCertificatePapfModel>

    @Insert(entity = SanitaryCertificatePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSanitaryCertificate(task: SanitaryCertificatePapfModel)

    @Query("DELETE FROM sanitary_certificate_papf_table")
    suspend fun deleteAllSanitaryCertificate()

    @Query("DELETE FROM sanitary_certificate_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteSanitaryCertificateById(idSolicitud: Int)

    //DOCUMENTACION
    @Query("SELECT * FROM documentation_papf_table WHERE idSolicitud = :idSolicitud ORDER BY fechaExpedicion DESC")
    suspend fun selectDocumentationPapf(idSolicitud: Int?): List<DocumentationPapfModel>?

    @Insert(entity = DocumentationPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDocumentationPapf(task: DocumentationPapfModel)

    @Query("DELETE FROM documentation_papf_table")
    suspend fun deleteAllDocumentationPapf()

    @Query("DELETE FROM documentation_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteDocumentationPapfById(idSolicitud: Int)

    //REGISTRO FECHA
    @Query("SELECT * FROM register_date_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectRegisterDate(idSolicitud: Int): RegisterDateModel?

    @Query("SELECT * FROM register_date_papf_table WHERE idSolicitud = :idSolicitud AND isLocal = :isLocal")
    suspend fun selectRegisterDateLocal(idSolicitud: Int, isLocal: Boolean = true): RegisterDateModel?

    @Transaction
    suspend fun insertOrUpdateRegisterDate(model: RegisterDateModel) {
        val items: RegisterDateModel? = model.idSolicitud?.let { selectRegisterDate(it) }
        if (items == null) insertRegisterDate(model) else updateRegisterDate(model)
    }

    @Insert(entity = RegisterDateModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertRegisterDate(model: RegisterDateModel)

    @Update
    suspend fun updateRegisterDate(model: RegisterDateModel)

    @Query("SELECT COUNT(`idSolicitud`) FROM register_date_papf_table")
    suspend fun selectCountRegisterDate(): Int

    @Query("DELETE FROM register_date_papf_table")
    suspend fun deleteAllRegisterDate()

    @Query("DELETE FROM register_date_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteRegisterDateById(idSolicitud: Int)

    //Notification
    @Query("SELECT * FROM info_notification_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectNotificationPapf(idSolicitud: Int?): List<InfoNotificationPapfModel>?

    @Transaction
    suspend fun insertOrUpdateNotificationPapf(model: InfoNotificationPapfModel) {
        val items: List<InfoNotificationPapfModel>? =
            model.idSolicitud?.let { selectNotificationPapf(it) }
        if (items.isNullOrEmpty()) insertNotificationPapf(model) else updateNotificationPapf(model)
    }

    @Insert(entity = InfoNotificationPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertNotificationPapf(task: InfoNotificationPapfModel)

    @Update
    suspend fun updateNotificationPapf(model: InfoNotificationPapfModel)

    @Query("DELETE FROM info_notification_papf_table")
    suspend fun deleteAllNotificationPapf()

    @Query("DELETE FROM info_notification_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteNotificationPapfById(idSolicitud: Int)

    //DATA REQUERIDA INSPECCION SANITARIA
    @Query("SELECT * FROM data_req_insp_sanitary_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectProductInspSanitary(
        idSolicitud: Int,
    ): List<DataReqInsSanPapfModel>?

    @Query("SELECT * FROM data_req_insp_sanitary_papf_table WHERE idSolicitud = :idSolicitud AND detalleProducto = :detalleProducto")
    suspend fun selectProductInspSanitary(
        idSolicitud: Int,
        detalleProducto: Int
    ): List<DataReqInsSanPapfModel>?

    @Transaction
    suspend fun insertOrUpdateProductInspSanitary(model: DataReqInsSanPapfModel) {
        val items: List<DataReqInsSanPapfModel>? =
            selectProductInspSanitary(model.idSolicitud ?: 0, model.detalleProducto ?: 0)
        if (items.isNullOrEmpty()) insertProductInspSanitary(model)
        else updateProductInspSanitary(
            model.idEstadoEmpaque,
            model.descripcionEmpaque,
            model.idConcepto,
            model.descripcionConcepto,
            model.condicionesAlmacenamiento,
            model.observaciones,
            model.detalleProducto,
            model.idSolicitud,
        )
    }

    @Insert(entity = DataReqInsSanPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProductInspSanitary(model: DataReqInsSanPapfModel)

    @Query("UPDATE data_req_insp_sanitary_papf_table SET idEstadoEmpaque = :idEstadoEmpaque, descripcionEmpaque = :descripcionEmpaque, idConcepto = :idConcepto, descripcionConcepto = :descripcionConcepto, condicionesAlmacenamiento = :condicionesAlmacenamiento, observaciones = :observaciones WHERE idSolicitud = :idSolicitud AND detalleProducto = :detalleProducto")
    suspend fun updateProductInspSanitary(
        idEstadoEmpaque: Int?,
        descripcionEmpaque: String?,
        idConcepto: Int?,
        descripcionConcepto: String?,
        condicionesAlmacenamiento: String?,
        observaciones: String?,
        detalleProducto: Int?,
        idSolicitud: Int?
    )

    @Query("DELETE FROM data_req_insp_sanitary_papf_table WHERE idSolicitud = :idSolicitud AND detalleProducto = :detalleProducto")
    suspend fun deleteProductInspSanitary(idSolicitud: Int, detalleProducto: Int)


    //DATA REQUERIDA ACTA TOMA DE MUESTRA
    @Query("SELECT * FROM data_req_act_sample_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectProductActSample(
        idSolicitud: Int,
    ): List<DataReqActSamplePapfModel>?

    @Query("SELECT * FROM data_req_act_sample_papf_table WHERE idSolicitud = :idSolicitud AND producto = :idProducto")
    suspend fun selectProductActSample(
        idSolicitud: Int,
        idProducto: Int
    ): List<DataReqActSamplePapfModel>?

    @Transaction
    suspend fun insertOrUpdateProductActSample(model: DataReqActSamplePapfModel) {
        val items: List<DataReqActSamplePapfModel>? =
            selectProductActSample(model.idSolicitud ?: 0, model.idProducto ?: 0)
        if (items.isNullOrEmpty()) insertProductActSample(model)
        else updateProductActSample(
            model.nroUnidades,
            model.descripcionUnidades,
            model.unidadesMedida,
            model.presentacion,
            model.descripcionPresentacion,
            model.contenidoNeto,
            model.idProducto,
            model.idSolicitud,
        )
    }

    @Insert(entity = DataReqActSamplePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertProductActSample(model: DataReqActSamplePapfModel)

    @Query("UPDATE data_req_act_sample_papf_table SET nroUnidades = :nroUnidades, descripcionUnidades = :descripcionUnidades, unidadesMedida = :unidadesMedida, presentacion = :presentacion, descripcionPresentacion = :descripcionPresentacion, contenidoNeto = :contenidoNeto WHERE idSolicitud = :idSolicitud AND producto = :producto")
    suspend fun updateProductActSample(
        nroUnidades: Int?,
        descripcionUnidades: String?,
        unidadesMedida: Int?,
        presentacion: Int?,
        descripcionPresentacion: String?,
        contenidoNeto: Int?,
        producto: Int?,
        idSolicitud: Int?
    )

    @Query("DELETE FROM data_req_act_sample_papf_table WHERE idSolicitud = :idSolicitud AND producto = :idProducto")
    suspend fun deleteProductActSample(idSolicitud: Int, idProducto: Int)


    // DATOS BASICOS CIERRE INSPECCION
    @Query("SELECT * FROM clasification_tramit_papf_table WHERE idSolicitud = :idSolicitud ")
    suspend fun selectAllClasificationTramit(idSolicitud: Int): ClasificationTramitPapfModel?

    @Query("SELECT * FROM clasification_tramit_papf_table WHERE idSolicitud = :idSolicitud AND idFirmante IS NOT NULL ")
    suspend fun selectClasificationTramit(idSolicitud: Int): ClasificationTramitPapfModel?

    @Query("UPDATE clasification_tramit_papf_table SET respuestaRequerimientoIF = :respuesta WHERE idSolicitud = :idSolicitud")
    suspend fun updateRespuestaRequerimientoIF(idSolicitud: Int?, respuesta: Boolean?)

    @Insert(entity = ClasificationTramitPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertClasificationTramit(task: ClasificationTramitPapfModel)

    @Query("DELETE FROM clasification_tramit_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteClasificationTramitById(idSolicitud: Int)

    //CONSULTAS DINAMICAS SELECTORES
    @Query("SELECT * FROM type_papf_table")
    suspend fun selectTypePapfByCode(): List<TypePapfModel>?

    @Query("SELECT * FROM type_papf_table WHERE codeQuery = :codeQuery")
    suspend fun selectTypePapfByCode(codeQuery: String): List<TypePapfModel>?

    @Query("SELECT * FROM type_papf_table WHERE idTipoTramite = :idTipoTramite")
    suspend fun selectTypePapf(
        idTipoTramite: String?,
    ): List<TypePapfModel>?

    @Insert(entity = TypePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTypePapf(model: TypePapfModel)

    @Query("DELETE FROM type_papf_table WHERE idTipoTramite = :idTipoTramite AND codeQuery = :codeQuery")
    suspend fun deleteAllTypePapf(idTipoTramite: String, codeQuery: String)

    @Query("DELETE FROM type_papf_table WHERE codeQuery = :codeQuery")
    suspend fun deleteAllTypePapfByCode(codeQuery: String)

    // CIERRE INPECCION FISICA
    @Transaction
    suspend fun insertOrUpdateCloseInsFisLocal(model: ClasificationTramitPapfModel) {
        val item: ClasificationTramitPapfModel? = selectCloseInsFisPapf(model.idSolicitud.orZero())
        if (item == null)
            insertCloseInsFisPapf(model)
        else
            updateCloseInsFisPapf(
                model.idIdioma,
                model.idioma,
                model.idFirmante,
                model.firmante,
                model.idResultadoCIS,
                model.resultadoCIS,
                model.idUsoMPIG,
                model.usoMPIG,
                model.justificacion,
                model.respuestaRequerimientoIF,
                model.idSolicitud,
                item.index,
            )
    }

    @Query("SELECT * FROM clasification_tramit_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectCloseInsFisPapf(idSolicitud: Int): ClasificationTramitPapfModel?

    @Insert(entity = ClasificationTramitPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCloseInsFisPapf(model: ClasificationTramitPapfModel)

    @Query("UPDATE clasification_tramit_papf_table SET idIdioma = :idIdioma, idioma = :idioma, idFirmante = :idFirmante, firmante = :firmante, idResultadoCIS = :idResultadoCIS, resultadoCIS = :resultadoCIS, idUsoMPIG = :idUsoMPIG, usoMPIG = :usoMPIG, justificacion = :justificacion, respuestaRequerimientoIF = :respuestaRequerimientoIF  WHERE idSolicitud = :idSolicitud AND `index` = :index")
    suspend fun updateCloseInsFisPapf(
        idIdioma: Int?,
        idioma: String?,
        idFirmante: Int?,
        firmante: String?,
        idResultadoCIS: Int?,
        resultadoCIS: String?,
        idUsoMPIG: Int?,
        usoMPIG: String?,
        justificacion: String?,
        respuestaRequerimientoIF: Boolean?,
        idSolicitud: Int?,
        index: Int?,
    )

    // CIERRE INPECCION FISICA agregar documento
    @Transaction
    suspend fun insertOrUpdateObservationLocal(model: CloseInspObservationPapfModel) {
        val item: CloseInspObservationPapfModel? = selectCloseInsObservationPapf(model.id.orZero())
        if (item == null)
            insertCloseInsObservationPapf(model)
        else
            updateCloseInsObservationPapf(
                model.descripcion,
                model.idSolicitud,
                model.id,
            )
    }

    @Query("SELECT * FROM close_insp_observation_papf_table WHERE id = :id")
    suspend fun selectCloseInsObservationPapf(id: Int): CloseInspObservationPapfModel?

    @Query("SELECT * FROM close_insp_observation_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectCloseInsObservationPapfBySolicitud(idSolicitud: Int): List<CloseInspObservationPapfModel>?

    @Insert(entity = CloseInspObservationPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCloseInsObservationPapf(model: CloseInspObservationPapfModel)

    @Query("UPDATE close_insp_observation_papf_table SET descripcion = :descripcion WHERE idSolicitud = :idSolicitud AND id = :id")
    suspend fun updateCloseInsObservationPapf(
        descripcion: String?,
        idSolicitud: Int?,
        id: Int?,
    )

    @Query("DELETE FROM close_insp_observation_papf_table WHERE idSolicitud = :idSolicitud AND id = :id")
    suspend fun deleteCloseInsObservationPapf(idSolicitud: Int?, id: Int?)

    @Query("DELETE FROM close_insp_observation_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteCloseInsObservationPapfBySolicitud(idSolicitud: Int?)

    @Transaction
    suspend fun hasReachedMaximumObservationsInspe(idSolicitud: Int): Int {
        val data = selectCloseInsObservationPapfBySolicitud(idSolicitud)
        return data?.size.orZero()
    }
    @Transaction
    suspend fun hasReachedMaximumObservationsEmitirCis(idSolicitud: Int): Int {
        val data = selectAllObservationEmitCis(idSolicitud)
        return data?.size.orZero()
    }

    @Query("SELECT COUNT(`index`) FROM clasification_tramit_papf_table")
    suspend fun selectClasificationTramit(): Int

    // DATOS INFORMACION EMITIR CIS
    @Query("SELECT * FROM emit_cis_close_papf_table WHERE idSolicitud = :idSolicitud ")
    suspend fun selectAllEmitCis(idSolicitud: Int): EmitCisClosePapfModel?

    @Query("SELECT * FROM emit_cis_close_papf_table WHERE idSolicitud = :idSolicitud ")
    suspend fun update(idSolicitud: Int): EmitCisClosePapfModel?

    @Transaction
    suspend fun insertOrUpdateEmitLocal(model: EmitCisClosePapfModel) {
        val item: EmitCisClosePapfModel? = selectAllEmitCis(model.idSolicitud.orZero())
        if (item == null)
            insertEmitCisPapf(model)
        else
            updateEmitCisPapf(
                model.idUsoMPIG,
                model.descripcionUsoMPIG,
                model.idResultadoCIS,
                model.descripcionResultadoCis,
                model.idFirmante,
                model.firmante,
                model.idIdioma,
                model.descripcionIdioma,
                model.idCertificadoExporta,
                model.descripcionCertificadoExporta,
                model.idTipoCertificado,
                model.descripcionTipoCertificado,
                model.idReembarque,
                model.descripcionReembarque,
                model.idMSS,
                model.descripcionMSS,
                model.idSolicitud,
                item.index,
            )
    }

    @Insert(entity = EmitCisClosePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertEmitCisPapf(model: EmitCisClosePapfModel)

    @Query(
        "UPDATE emit_cis_close_papf_table SET idUsoMPIG = :idUsoMPIG, descripcionUsoMPIG = :descripcionUsoMPIG, " +
                "idResultadoCIS = :idResultadoCIS, descripcionResultadoCis = :descripcionResultadoCis, idFirmante = :idFirmante, " +
                "firmante = :firmante, idIdioma = :idIdioma, descripcionIdioma = :descripcionIdioma, " +
                "idCertificadoExporta = :idCertificadoExporta, descripcionCertificadoExporta = :descripcionCertificadoExporta,  " +
                "idTipoCertificado = :idTipoCertificado, descripcionTipoCertificado = :descripcionTipoCertificado,  " +
                "idReembarque = :idReembarque, descripcionReembarque = :descripcionReembarque,  " +
                "idMSS = :idMSS, descripcionMSS = :descripcionMSS  " +
                "WHERE idSolicitud = :idSolicitud AND `index` = :index"
    )
    suspend fun updateEmitCisPapf(
        idUsoMPIG: Int?,
        descripcionUsoMPIG: String?,
        idResultadoCIS: Int?,
        descripcionResultadoCis: String?,
        idFirmante: Int?,
        firmante: String?,
        idIdioma: Int?,
        descripcionIdioma: String?,
        idCertificadoExporta: Int?,
        descripcionCertificadoExporta: String?,
        idTipoCertificado: Int?,
        descripcionTipoCertificado: String?,
        idReembarque: Int?,
        descripcionReembarque: String?,
        idMSS: Int?,
        descripcionMSS: String?,
        idSolicitud: Int?,
        index: Int?,
    )

    @Query("DELETE FROM emit_cis_close_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteEmitCisById(idSolicitud: Int)

    // EMIT CIS OBSERVATION
    @Query("SELECT * FROM emit_cis_observation_papf_table WHERE idSolicitud = :idSolicitud ")
    suspend fun selectAllObservationEmitCis(idSolicitud: Int): List<EmitCisObservationPapfModel>?

    @Transaction
    suspend fun insertOrUpdateObservationEmitCisLocal(model: EmitCisObservationPapfModel) {
        val item: List<EmitCisObservationPapfModel>? =
            selectAllObservationEmitCis(model.id.orZero())
        if (item.isNullOrEmpty())
            insertObservationEmitCisPapf(model)
        else
            updateCloseInsObservationPapf(
                model.descripcion,
                model.idSolicitud,
                model.id,
            )
    }

    @Insert(entity = EmitCisObservationPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertObservationEmitCisPapf(model: EmitCisObservationPapfModel)

    @Query("DELETE FROM emit_cis_observation_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteObservationEmitCisById(idSolicitud: Int)

    @Query("DELETE FROM emit_cis_observation_papf_table WHERE idSolicitud = :idSolicitud AND id = :id")
    suspend fun deleteObservationEmitCisById(idSolicitud: Int?, id: Int?)

    //Info emitir
    @Query("SELECT * FROM emit_cis_info_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectInfoEmitir(idSolicitud: Int): EmitCisInfoPapfModel?

    @Insert(entity = EmitCisInfoPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInfoEmitir(model: EmitCisInfoPapfModel)

    @Query("DELETE FROM emit_cis_info_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteAllInfoEmitir(idSolicitud: Int)

    //CHECK LIST
    @Query("SELECT * FROM emit_cis_check_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun selectChecklist(idSolicitud: Int): EmitCisCheckPapfModel?

    @Insert(entity = EmitCisCheckPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertChecklist(model: EmitCisCheckPapfModel)

    @Query("DELETE FROM emit_cis_check_papf_table WHERE idSolicitud = :idSolicitud")
    suspend fun deleteAllChecklist(idSolicitud: Int)

    //Detalle Producto
    @Query("SELECT * FROM detail_product_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto")
    suspend fun selectDetailProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int): List<DetailProductPapfModel>?

    @Insert(entity = DetailProductPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDetailProduct(task: DetailProductPapfModel)

    @Query("DELETE FROM detail_product_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud")
    suspend fun deleteDetailProductById(idSolicitud: Int, idProductoSolicitud: Int)

    //subpartida
    @Query("SELECT * FROM subpartida_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto")
    suspend fun selectSubPartidaProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int): List<SubPartidaPapfModel>?

    @Insert(entity = SubPartidaPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSubPartidaProduct(task: SubPartidaPapfModel)

    @Query("DELETE FROM subpartida_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud")
    suspend fun deleteSubPartidaProductById(idSolicitud: Int, idProductoSolicitud: Int)

    //Expedidor
    @Query("SELECT * FROM expedidor_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto")
    suspend fun selectExpedidorProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int): List<ExpedidorPapfModel>?

    @Insert(entity = ExpedidorPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertExpedidorProduct(task: ExpedidorPapfModel)

    @Query("DELETE FROM expedidor_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud")
    suspend fun deleteExpedidorProductById(idSolicitud: Int, idProductoSolicitud: Int)

    //Fabricante
    @Query("SELECT * FROM fabricante_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto")
    suspend fun selectFabricanteProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int): List<FabricantePapfModel>?

    @Insert(entity = FabricantePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFabricanteProduct(task: FabricantePapfModel)

    @Query("DELETE FROM fabricante_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud")
    suspend fun deleteFabricanteProductById(idSolicitud: Int, idProductoSolicitud: Int)

    //InformacionComplementaria
    @Query("SELECT * FROM info_complementaria_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto")
    suspend fun selectInfoCompleProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int): List<InfoComplementariaPapfModel>?

    @Insert(entity = InfoComplementariaPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInfoCompleProduct(task: InfoComplementariaPapfModel)

    @Query("DELETE FROM info_complementaria_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud")
    suspend fun deleteInfoCompleProductById(idSolicitud: Int, idProductoSolicitud: Int)

    //Destinatario, LugarDestino, OperadorResponsable, PuertoControlFronterizo, LicenciasImportacion
    @Query("SELECT * FROM destinatario_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND idClasificacionProducto = :idClasificacionProducto AND typeList = :typeList")
    suspend fun selectDestinityProduct(idSolicitud: Int, idProductoSolicitud: Int, idClasificacionProducto: Int, typeList: String): List<DestinatarioPapfModel>?

    @Insert(entity = DestinatarioPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDestinityProduct(task: DestinatarioPapfModel)

    @Query("DELETE FROM destinatario_papf_table WHERE idSolicitud = :idSolicitud AND idProductoSolicitud = :idProductoSolicitud AND typeList = :typeList")
    suspend fun deleteDestinityProductById(idSolicitud: Int, idProductoSolicitud: Int, typeList: String)

    @Insert(entity = SignatoriesPapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSignatoriesPapf(model: SignatoriesPapfModel)

    @Query("SELECT * FROM signatories_papf_table WHERE idUsuario = :idFirmante")
    suspend fun getIdUsuarioFirmante(idFirmante: Int?): SignatoriesPapfModel?

    @Insert(entity = ConsecutivePapfModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertConsecutivePapf(task: ConsecutivePapfModel)

    @Query("DELETE from consecutive_papf_table where idSolicitud = :idSolicitud")
    suspend fun deleteConsecutivePapfById(idSolicitud: Int?)

    @Query("DELETE from consecutive_papf_table")
    suspend fun deleteAllConsecutivePapf()
    @Query("SELECT * from consecutive_papf_table where idSolicitud = :idSolicitud")
    suspend fun selectConsecutivePapfById(idSolicitud: Int):ConsecutivePapfModel?
}