package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.soaint.data.model.VisitEmpresaDto
import com.soaint.data.model.VisitFormaDto
import com.soaint.domain.model.VisitDireccionEntity
import com.soaint.domain.model.VisitFormaEntity
import com.soaint.domain.model.VisitTramiteEntity

@Entity(tableName = "visit_tramite_table",
    foreignKeys = [
        ForeignKey(
            entity = VisitModel::class,
            parentColumns = ["id"],
            childColumns = ["idVisita"],
            onDelete = ForeignKey.CASCADE
        )
    ])
class VisitTramiteModel(
    @ColumnInfo(name = "id") val id: Int?,
    @ColumnInfo(name = "idTipo") val idTipo: Int?,
    @ColumnInfo(name = "descTipoTramite") val descTipoTramite: String?,
    @ColumnInfo(name = "idSubTipo") val idSubTipo: Int?,
    @ColumnInfo(name = "descSubTipoTramite") val descSubTipoTramite: String?,
    @ColumnInfo(name = "fechaRadicacion") val fechaRadicacion: String?,
    @ColumnInfo(name = "numeroRadicacion") val numeroRadicacion: String?,
    @ColumnInfo(name = "idTramite") val idTramite: Int?,
    @ColumnInfo(name = "indNotificarResultado") val indNotificarResultado: Boolean?,
    @ColumnInfo(name = "indReprogramada") val indReprogramada: Boolean?,
    @ColumnInfo(name = "expediente") val expediente: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int,
    @PrimaryKey(autoGenerate = true) var index: Int = 0,
)


fun VisitTramiteModel.mapToDomain(idVisita: Int) = VisitTramiteEntity(
    id, idTipo, descTipoTramite, idSubTipo, descSubTipoTramite, fechaRadicacion, numeroRadicacion,
    idTramite, indNotificarResultado, indReprogramada, expediente, idVisita
)