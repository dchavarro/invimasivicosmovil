package com.soaint.domain.model

import android.os.Parcelable
import com.soaint.sivicos_dinamico.utils.CODE_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU
import com.soaint.sivicos_dinamico.utils.ID_RESULT_EQUIPMENT_REEQ_NOCU
import kotlinx.parcelize.Parcelize

@Parcelize
data class EquipmentEntity(
    val id: Int?,
    val equipo: String?,
    val codigoZonaVerificar: String?,
    val superficieContacto: String?,
    val otrasSuperficies: String?,
    val idZonaVerificar: Int?,
    val descripcionZonaVerificar: String?,
    val idResultado: Int? = ID_RESULT_EQUIPMENT_REEQ_NOCU,
    val codigoResultado: String? = CODE_RESULT_EQUIPMENT_REEQ_NOCU,
    val descripcionResultado: String? = DESCRIP_RESULT_EQUIPMENT_REEQ_NOCU,
    val idSede: Int?,
    val activo: Boolean? = true,
    val usuarioCrea: String? = EMPTY_STRING,
    val fechaCreacion: String? = EMPTY_STRING,
    val usuarioModifica: String? = EMPTY_STRING,
    val fechaModifica: String? = EMPTY_STRING,
    val isLocal: Boolean? = false,
    val isUpdate: Boolean? = false,
    val observacion: String? = EMPTY_STRING,
) : Parcelable

data class EquipmentUpdateEntity(
    val id: Int?,
    val codigoZonaVerificar: String?,
    val idResultado: Int?,
    val codigoResultado: String?,
    val descripcionResultado: String?,
    val observacion: String?,
    val isLocal: Boolean?,
    val isUpdate: Boolean?
)