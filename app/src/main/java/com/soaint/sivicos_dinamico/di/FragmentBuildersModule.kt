package com.soaint.sivicos_dinamico.di

import com.soaint.sivicos_dinamico.common.ControlBaseFragment
import com.soaint.sivicos_dinamico.flow.main.views.sync.SyncFragment
import com.soaint.sivicos_dinamico.flow.main.views.task.TasksFragment
import com.soaint.sivicos_dinamico.flow.papf.dialogs.act_sample.DataReqActSample
import com.soaint.sivicos_dinamico.flow.papf.dialogs.inspection_sanitary.DataReqInspSanitary
import com.soaint.sivicos_dinamico.flow.papf.views.certificate.CertificateFragment
import com.soaint.sivicos_dinamico.flow.papf.views.close_insp.CloseInspectionFragment
import com.soaint.sivicos_dinamico.flow.papf.views.detail_product.DetailProductFragment
import com.soaint.sivicos_dinamico.flow.papf.views.documentation.DocumentationFragment
import com.soaint.sivicos_dinamico.flow.papf.views.emit_cis.EmitCisFragment
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.papf.views.invoice.InvoiceFragment
import com.soaint.sivicos_dinamico.flow.papf.views.products.RegisterProductsFragment
import com.soaint.sivicos_dinamico.flow.papf.views.registerDate.RegisterDateFragment
import com.soaint.sivicos_dinamico.flow.papf.views.transport.TransportPapfFragment
import com.soaint.sivicos_dinamico.flow.visit.views.address.DialogAddress
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.CloseFragment
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.close.CloseVisitFragment
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.denunciation.DenunciationFragment
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.interaction.InteractionFragment
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.ivc.CloseIvcFragment
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.personal.DialogPersonal
import com.soaint.sivicos_dinamico.flow.visit.views.close_visit.technical.TechnicalFragment
import com.soaint.sivicos_dinamico.flow.visit.views.documents.DocumentFragment
import com.soaint.sivicos_dinamico.flow.visit.views.historic.HistoricFragment
import com.soaint.sivicos_dinamico.flow.visit.views.historicMSS.HistoricMssFragment
import com.soaint.sivicos_dinamico.flow.visit.views.info_visit.VisitInfoFragment
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.ManageMssFragment
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.company.MssCompanyFragment
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.nso.MssNsoFragment
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.product.MssProductFragment
import com.soaint.sivicos_dinamico.flow.visit.views.notification.NotificationFragment
import com.soaint.sivicos_dinamico.flow.visit.views.register_company.RegisterCompanyFragment
import com.soaint.sivicos_dinamico.flow.visit.views.transport.TransportFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun bindControlBaseFragment(): ControlBaseFragment

    @ContributesAndroidInjector
    internal abstract fun bindSincronizacionFragment(): SyncFragment

    @ContributesAndroidInjector
    internal abstract fun bindTasksFragment(): TasksFragment

    @ContributesAndroidInjector
    internal abstract fun bindVisitInfoFragment(): VisitInfoFragment

    @ContributesAndroidInjector
    internal abstract fun bindHistoricFragment(): HistoricFragment

    @ContributesAndroidInjector
    internal abstract fun bindHistoricMssFragment(): HistoricMssFragment

    @ContributesAndroidInjector
    internal abstract fun bindDocumentFragment(): DocumentFragment

    @ContributesAndroidInjector
    internal abstract fun bindTransportFragment(): TransportFragment

    @ContributesAndroidInjector
    internal abstract fun bindNotificationFragment(): NotificationFragment

    @ContributesAndroidInjector
    internal abstract fun bindManageMssFragment(): ManageMssFragment

    @ContributesAndroidInjector
    internal abstract fun bindMssProductFragment(): MssProductFragment

    @ContributesAndroidInjector
    internal abstract fun bindMssCompanyFragment(): MssCompanyFragment

    @ContributesAndroidInjector
    internal abstract fun bindMssNsoFragment(): MssNsoFragment

    @ContributesAndroidInjector
    internal abstract fun bindCloseFragment(): CloseFragment

    @ContributesAndroidInjector
    internal abstract fun bindCloseVisitFragment(): CloseVisitFragment

    @ContributesAndroidInjector
    internal abstract fun bindCloseIvoFragment(): CloseIvcFragment

    @ContributesAndroidInjector
    internal abstract fun bindDenunciationFragment(): DenunciationFragment

    @ContributesAndroidInjector
    internal abstract fun bindTechnicalFragment(): TechnicalFragment

    @ContributesAndroidInjector
    internal abstract fun bindInteractionFragment(): InteractionFragment

    @ContributesAndroidInjector
    internal abstract fun bindDialogPersonal(): DialogPersonal

    @ContributesAndroidInjector
    internal abstract fun bindRegisterCompanyFragment(): RegisterCompanyFragment

    @ContributesAndroidInjector
    internal abstract fun bindDialogAddress(): DialogAddress

    @ContributesAndroidInjector
    internal abstract fun bindInfoTramitFragment(): InfoTramitFragment

    @ContributesAndroidInjector
    internal abstract fun bindInvoiceFragment(): InvoiceFragment

    @ContributesAndroidInjector
    internal abstract fun bindTransportPapfFragment(): TransportPapfFragment

    @ContributesAndroidInjector
    internal abstract fun bindRegisterProductsFragment(): RegisterProductsFragment

    @ContributesAndroidInjector
    internal abstract fun bindCertificateFragment(): CertificateFragment

    @ContributesAndroidInjector
    internal abstract fun bindDocumentationFragment(): DocumentationFragment

    @ContributesAndroidInjector
    internal abstract fun bindRegisterDateFragment(): RegisterDateFragment

    @ContributesAndroidInjector
    internal abstract fun bindDataReqInspSanitary(): DataReqInspSanitary

    @ContributesAndroidInjector
    internal abstract fun bindDataReqActSample(): DataReqActSample

    @ContributesAndroidInjector
    internal abstract fun bindCloseInspectionFragment(): CloseInspectionFragment

    @ContributesAndroidInjector
    internal abstract fun bindEmitCisFragment(): EmitCisFragment

    @ContributesAndroidInjector
    internal abstract fun bindDetailProductFragment(): DetailProductFragment
}