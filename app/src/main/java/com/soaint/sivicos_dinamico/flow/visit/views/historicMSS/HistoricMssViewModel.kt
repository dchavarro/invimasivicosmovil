package com.soaint.sivicos_dinamico.flow.visit.views.historicMSS

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.model.HistoricMssEntity
import com.soaint.domain.model.MssEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.HistoricMssUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnHistoricMssViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnHistoricMssViewModelEvent()
    data class OnViewModelReady(
        val historicMss: List<HistoricMssEntity>?,
        val mss: List<List<MssEntity>>?,
        ): OnHistoricMssViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnHistoricMssViewModelEvent()
}

class HistoricMssViewModel @Inject constructor(
    private val historicMssUseCase: HistoricMssUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnHistoricMssViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    fun init(visitData: List<VisitEntity>) = execute {
        _event.value =
            OnHistoricMssViewModelEvent.OnChangeLabelPreloader(R.string.cargando_historic)
        val historic = visitData.filter { it.empresa != null }.mapNotNull { historicMssUseCase.loadHistoricoMssLocal(it.empresa?.razonSocial.orEmpty()) }
        val mss = visitData.filter { it.empresa != null }.mapNotNull { historicMssUseCase.loadMssLocal(it.empresa?.razonSocial.orEmpty()) }
        val resultHistoric = historic.filter { it.isSuccess }.mapNotNull { it.getOrNull() }
        val resultmss = mss.filter { it.isSuccess }.mapNotNull { it.getOrNull() }
        _event.value = OnHistoricMssViewModelEvent.OnViewModelReady(resultHistoric, resultmss)
    }
}