package com.soaint.sivicos_dinamico.flow.act

import android.content.res.ColorStateList
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import androidx.activity.viewModels
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.gson.Gson
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.data.store.PreferencesManager
import com.soaint.domain.common.TiposControl
import com.soaint.domain.common.getFileFromBase64AndSave
import com.soaint.domain.common.openFile
import com.soaint.domain.common.safeLet
import com.soaint.domain.model.BlockBodyValueEntity
import com.soaint.domain.model.BlockEntity
import com.soaint.domain.model.PlantillaEntity
import com.soaint.sivicos_dinamico.BuildConfig
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseActivity
import com.soaint.sivicos_dinamico.common.ControlBaseFragment
import com.soaint.sivicos_dinamico.databinding.ActivityActBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.setProgressCompat
import com.soaint.sivicos_dinamico.properties.activityBinding
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM012
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM015
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM030
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM055
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM156
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.json.JSONObject
import kotlin.math.roundToInt


class ActActivity : BaseActivity() {

    private val binding by activityBinding<ActivityActBinding>(R.layout.activity_act)

    private val viewModel by viewModels<ActViewModel> { viewModelFactory }

    var numPages: Int = 0
    var blocks: List<BlockEntity> = emptyList()
    var plantilla: List<PlantillaEntity> = emptyList()
    var map = mutableMapOf<String, String>()
    var idVisita: Int? = 0
    var idTipoDocumental: Int? = 0
    var idBlockValue: Int = 0

    val plantillaId get() = plantilla.firstOrNull()?.id?.toString().orEmpty()

    val bodyBlock: Result<BlockBodyValueEntity?>
        get() {
            return viewModel.getBlockBodyUnfinished(plantilla.firstOrNull()?.id.toString(), idVisita.toString())
        }
    val isEditable: Boolean
        get() {
            val blockFirmas = blocks.mapNotNull { it.atributosPlantilla }.flatten().filter {
                it.idTipoAtributo == TiposControl.FIRMA && it.configuracion?.requerido == true && viewModel.isEditableActa(
                    idVisita.orZero().toString(),
                    plantillaId,
                    bodyBlock.getOrNull()?.id ?: 0
                )
            }
            return if (blockFirmas.isEmpty()) true else blockFirmas.all {
                map.containsKey(it.nombre) && !map.getOrDefault(it.nombre, EMPTY_STRING)
                    .isNullOrBlank()
            }.not()
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar!!.hide()
        initView()
        observerViewModelEvents()
    }

    private fun initView() {
        val pagerAdapter = ScreenSlidePagerAdapter(this)
     /*    blocks = intent?.extras?.getParcelableArrayList("bloques") ?: emptyList()
        idVisita = intent?.extras?.getInt("idVisita").orZero()
        plantilla = intent?.extras?.getParcelableArrayList("plantilla") ?: emptyList()
        idTipoDocumental = intent?.extras?.getInt("idTipoDocumental")*/
        val preferencesManager = PreferencesManager(this)
        idVisita = runBlocking {
            preferencesManager.idVisita.first()
        }
        blocks = runBlocking {
            preferencesManager.bloques.first()
        }
        plantilla = runBlocking {
            preferencesManager.plantilla.first()
        }
        idTipoDocumental = runBlocking {
            preferencesManager.idTipoDocumental.first()
        }
        val isIVCInsFM055OrFM156orFM012 = plantilla.firstOrNull()?.codigo in setOf(IVC_INS_FM055, IVC_INS_FM156, IVC_INS_FM012)
        if (isIVCInsFM055OrFM156orFM012) viewModel.selectProductInspSanitary(idVisita!!, plantilla.firstOrNull()?.codigo.toString())
        blocks.onEach { block ->
            block.atributosPlantilla?.onEach { atribute ->
                var newValue = viewModel.getBlockValue(
                    plantilla.firstOrNull()?.id.toString(),
                    idVisita.toString(),
                    block.id.toString(),
                    atribute.id.toString()
                )
                val isProductosAttribute = atribute.nombre == "productos"
                val consecutivo = viewModel.getConsecutivo(idVisita ?: 0)

                //En algunos casos se  eliminan valores precargados
                newValue = viewModel.deleteValueBefore(atribute.nombre,newValue,plantilla)

                if ((isIVCInsFM055OrFM156orFM012 && isProductosAttribute) || newValue.isNullOrEmpty()) {
                    newValue = viewModel.changeValue(atribute.nombre, newValue,plantilla,consecutivo.toString())
                }
                // Elimina el ultimo elemento de cada array de la lista productos
                if (isIVCInsFM055OrFM156orFM012 && isProductosAttribute) {
                    atribute.configuracion?.lista?.dropLast(1)?.let { atribute.configuracion?.lista = it }
                }

                // En algunos casos se eliminan  despues de modificarlos
                newValue = viewModel.deleteValueAfter(atribute.nombre,newValue,plantilla)

                safeLet(atribute.nombre, newValue) { nombre, valor ->
                    map.put(nombre, valor.toString())
                }
            }
        }

        idBlockValue = bodyBlock.getOrNull()?.id ?: 0
        var bodyDataBase = bodyBlock.getOrNull()?.body
        //Cada ves que se entre a llenar las actas se elimina los priductos guardados de manera local
        // con el fin de obtener siempre lo que se llena en los popups
        if (isIVCInsFM055OrFM156orFM012 && !bodyDataBase.isNullOrEmpty()) {
            val jsonObject = JSONObject(bodyDataBase.toString()).apply { remove("productos") }
            bodyDataBase = jsonObject.toString()
        }
        map.putAll(viewModel.getActa(bodyDataBase.toString()))
        val newMap = viewModel.decodeMap4CheckG(map)
        map.clear()
        map.putAll(newMap)

        numPages = blocks.size

        binding.viewPager.isUserInputEnabled = false
        binding.viewPager.adapter = pagerAdapter

        binding.btnNext.setOnSingleClickListener {
            val actualFragment: ControlBaseFragment =
                supportFragmentManager.findFragmentByTag("f" + binding.viewPager.currentItem) as ControlBaseFragment
            val datos = actualFragment.retornarDatos()

            if (datos.first) {
                map.putAll(datos.second)
                traceMap()
                // Ya está en la ultima pagina
                if (binding.viewPager.currentItem == numPages - 1) {
                    binding.btnNext.setImageResource(R.drawable.ic_action_done)
                    preguntarActa()
                }
                binding.viewPager.currentItem = binding.viewPager.currentItem + 1
                binding.btnPrev.visibility = View.VISIBLE
                binding.imageViewOptions.isVisible = true
            } else {
                showAlertDialog(
                    showDialog(
                        getString(R.string.error),
                        getString(R.string.campos_obligatorios_sin_llenar),
                        titleBtnPositive = getString(R.string.cerrar)
                    )
                    { dismissAlertDialog() }
                )
            }
            // GUARDAR DATA DILIGENCIADA
            viewModel.saveActa(
                blocks,
                map,
                idVisita.toString(),
                plantilla.firstOrNull()?.id.toString(),
                idBlockValue
            )
            // CREACION REGISTRO DE DOCUMENTO
            viewModel.createOrUpdateDocument(
                plantilla.firstOrNull(),
                idTipoDocumental,
                idVisita.toString(),
                false
            )
        }

        binding.btnPrev.setOnSingleClickListener {
            binding.viewPager.currentItem = binding.viewPager.currentItem - 1

            if (binding.viewPager.currentItem == 0) {
                binding.btnPrev.visibility = View.GONE
            }

            binding.btnNext.setImageResource(R.drawable.ic_arrow_right)
        }

        binding.progress.progressTintList = ColorStateList.valueOf(getColor(R.color.colorGreen))

        val pageChangeCallback = object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                val progreso: Int =
                    (((position + 1).toFloat() / numPages.toFloat()) * 100).roundToInt()
                binding.progress.setProgressCompat(progreso, true)
            }
        }
        binding.viewPager.registerOnPageChangeCallback(pageChangeCallback)

        //Custom ToolBar
        binding.title.text = plantilla.firstOrNull()?.descripcion.toString()
        binding.subtitle.text =
            getString(R.string.copy_version, plantilla.firstOrNull()?.version.toString())
        binding.subtitle.isVisible = plantilla.firstOrNull()?.version != null
        binding.imageViewOptions.setOnSingleClickListener { menuOptions(binding.imageViewOptions) }


        //Oculto el botón anterior inicialmente
        binding.btnPrev.visibility = View.GONE
        binding.imageViewOptions.isVisible = !map.isNullOrEmpty()
    }

    fun traceMap() {
        println("All keys: ${map.keys}")
        println("All values: ${map.values}")
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(this, ::eventHandler)
    }

    private fun eventHandler(event: OnActViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnActViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnActViewModelEvent.OnTokenError -> onError(
                event.title,
                event.message,
                event.finished
            )
            is OnActViewModelEvent.OnViewModelReady -> onViewModelReady(
                event.pdfBase64,
                event.plantilla,
                event.indVistaPrevia
            )
            is OnActViewModelEvent.OnViewModelDocument -> OnViewModelDocument()
            is OnActViewModelEvent.OnGetIdBlockValue -> OnSetIdBlockValue(event.id)
            else -> {}
        }
    }

    private fun onError(@StringRes title: Int, @StringRes message: Int, finished: Boolean) {
        showAlertDialog(
            showDialog(
                getString(title),
                getString(message),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { if (finished) finish() else dismissAlertDialog() }
        )
    }

    private fun OnTokenMessageValisteHours(@StringRes title: Int = R.string.no_continue,message: String, finished: Boolean) {
        showAlertDialog(
            showDialog(
                getString(title),
                message,
                titleBtnPositive = getString(R.string.cerrar)
            )
            { if (finished) finish() else dismissAlertDialog() }
        )
    }

    private fun OnViewModelDocument() {}
    private fun OnSetIdBlockValue(id: Int) {
        idBlockValue = id
    }

    private fun onViewModelReady(
        pdfBase64: String?,
        plantilla: PlantillaEntity?,
        indVistaPrevia: Boolean
    ) {
        if (!pdfBase64.isNullOrEmpty()) {
            val path = getFileFromBase64AndSave(this, pdfBase64, plantilla?.nombre.orEmpty(), "pdf")
            openFile(this, path.orEmpty(), BuildConfig.APPLICATION_ID)
            if (!indVistaPrevia) this.finish()
        } else {
            showAlertDialog(
                showDialog(
                    getString(R.string.adv),
                    getString(R.string.copy_error_acta, plantilla?.codigo),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                )
                { dismissAlertDialog(); finish() }
            )
        }
    }

    fun preguntarActa() {
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(R.string.solicitud_generar_pdf),
                titleBtnPositive = getString(R.string.copy_save_close),
                titleBtnNegative = getString(R.string.cerrar)
            )
            {
                val newMap = viewModel.encodeMap4CheckG(blocks, map)
                val firstPlantilla = plantilla.firstOrNull()
                val plantillaCodigo = firstPlantilla?.codigo
                if (plantillaCodigo == IVC_INS_FM015) {
                    val (status, message) = viewModel.validateHours(idVisita.toString(), newMap)
                    if (status){
                        viewModel.insertHours(idVisita.toString(), newMap)
                        viewModel.mapSaveBlock(idVisita!!, newMap, blocks, idTipoDocumental, false, idBlockValue)
                    }else{
                       OnTokenMessageValisteHours(message= message.toString(), finished = false)
                    }
                }else{
                    viewModel.mapSaveBlock(idVisita!!, newMap, blocks, idTipoDocumental, false, idBlockValue)
                    dismissAlertDialog()
                }

                if (plantillaCodigo == IVC_INS_FM030) {
                    viewModel.updateEquipment(newMap)
                }
            }
        )
    }

    private inner class ScreenSlidePagerAdapter(fa: FragmentActivity) : FragmentStateAdapter(fa) {

        val fa = fa as ActActivity

        override fun getItemCount(): Int = numPages

        override fun createFragment(position: Int): Fragment {
            val blocks = fa.blocks[position].atributosPlantilla
            val titulo = fa.blocks[position].nombre
            val subTitulo = fa.blocks[position].descripcion
            val fragment = ControlBaseFragment()
            val esUltima: Boolean = fa.blocks.size - 1 == position
            val bundle = Bundle()
            bundle.putString("blocks", Gson().toJson(blocks))
            bundle.putBoolean("esUltima", esUltima)
            bundle.putString("titulo", titulo)
            bundle.putString("subTitulo", subTitulo)

            //Paso los valores que ya tenga almacenados
            for (control in blocks.orEmpty()) {
                if (map[control.nombre] != null) {
                    bundle.putString(control.nombre, map[control.nombre])
                }
            }

            fragment.arguments = bundle
            return fragment
        }
    }

    private fun menuOptions(view: View) {
        val popup = PopupMenu(this, view)
        popup.inflate(R.menu.menu_act)
        popup.setOnMenuItemClickListener { item: MenuItem? ->
            when (item?.itemId) {
                R.id.preview -> {
                    if (idVisita != 0) viewModel.mapSaveBlock(
                        idVisita!!,
                        map,
                        blocks,
                        idTipoDocumental,
                        true,
                        idBlockValue
                    )
                }
                R.id.exit -> {
                    showAlertDialog(
                        showDialog(
                            getString(R.string.atencion),
                            getString(R.string.copy_close_act),
                            titleBtnPositive = getString(R.string.si),
                            titleBtnNegative = getString(R.string.no)
                        )
                        { finish() }
                    )
                }
            }
            true
        }
        popup.show()
    }

    override fun onBackPressed() {
        val currentItem = binding.viewPager.currentItem
        if (currentItem == 0) {
            super.onBackPressed()
        } else {
            binding.viewPager.currentItem = currentItem - 1
            if (binding.viewPager.currentItem == 0) {
                binding.btnPrev.visibility = View.GONE
            }
            binding.btnNext.setImageResource(R.drawable.ic_arrow_right)
        }
    }
}