package com.soaint.sivicos_dinamico.extensions

import android.content.Context
import android.os.Build
import android.text.Editable
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.widget.*
import com.google.android.material.textfield.TextInputLayout
import com.soaint.sivicos_dinamico.R
import com.vomerc.vomerc.listeners.OnSingleClickListener
import java.util.regex.Pattern


const val REQUEST_CODE_ASK_PERMISSIONS = 603

private const val PHONE_REG = "^\\d{10}$"

private const val PHONE_INITIAL = '3'

private const val PHONE_LENGTH = 10

fun Context.validateEmail(
    input: String,
    layout: TextInputLayout
): Boolean {
    val email = input.toString().trim()

    if (email.isEmpty() || !email.isValidEmail()) {
        layout.error = getString(R.string.mensaje_email_no_valido)
        return false
    } else {
        layout.isErrorEnabled = false
        layout.error = null
    }
    return true
}

fun Context.validateUser(input: EditText, txtUsername: TextView, layout: TextInputLayout): Boolean {
    val email = input.text.toString().trim()
    var inputLayoutEmail: TextInputLayout? = null

    if (input == txtUsername) {
        inputLayoutEmail = layout
        inputLayoutEmail.isErrorEnabled = true
    }

    if (email.isEmpty()) {
        inputLayoutEmail?.error = getString(R.string.campo_obligatorio)
        input.requestFocus()
        return false
    } else {
        inputLayoutEmail?.isErrorEnabled = false
        inputLayoutEmail?.error = null
    }

    return true
}

fun TextInputLayout.showErrorTextInputLayout(error: String) {
    isErrorEnabled = true
    setError(context.getString(R.string.campo_obligatorio))
}

fun TextInputLayout.clearErrorTextInputLayout() {
    error = null
}

fun Button.showErrorInButton() {
    setBackgroundColor(context.getColor(R.color.colorRed))
}

fun Button.clearErrorInButton() {
    setBackgroundColor(context.getColor(R.color.colorPrimary))
}

fun String.isValidEmail(): Boolean {
    return !TextUtils.isEmpty(this) && Patterns.EMAIL_ADDRESS.matcher(this)
        .matches()
}

fun View.requestFocusView() {
    if (requestFocus()) {
        showKeyBoard()
    }
}

fun View.setOnSingleClickListener(action: (View) -> Unit) {
    setOnClickListener(OnSingleClickListener { view -> action(view) })
}

fun String.toEditable() = Editable.Factory.getInstance().newEditable(this)

fun CharSequence.isPhoneNumber(): Boolean =
    this.isNotBlank() &&
            this.length == PHONE_LENGTH &&
            this.firstOrNull() == PHONE_INITIAL &&
            Pattern.compile(PHONE_REG).matcher(this).find()


fun Toolbar.disableTitleSingleLine() {
    for (i in 0..childCount) {
        val child = getChildAt(i)
        if (child is TextView) {
            child.isSingleLine = false
        }
    }
}

fun Spinner.selectValue(value: String?) {
    if (!value.isNullOrEmpty()) {
        for (i in 0 until adapter.count) {
            if (adapter.getItem(i).equals(value)) {
                setSelection(i)
                break
            }
        }
    }
}

fun ProgressBar.setProgressCompat(progress: Int, animate: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
        setProgress(progress, animate)
    } else {
        setProgress(progress)
    }
}