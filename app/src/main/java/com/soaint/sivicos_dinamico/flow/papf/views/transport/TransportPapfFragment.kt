package com.soaint.sivicos_dinamico.flow.papf.views.transport

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.TransportObjectPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentTransportPapfBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.item_view.TransportComItemView
import com.soaint.sivicos_dinamico.flow.papf.item_view.TransportDocItemView
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class TransportPapfFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentTransportPapfBinding>(R.layout.fragment_transport_papf)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: TransportPapfViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.transport_papf))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(TransportPapfFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.idRequest.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.transportDoc)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReady(transportDoc: TransportObjectPapfEntity?) {
        adapter.clear()
        val items = mutableListOf<Group>()
        val empresa = transportDoc?.empresa
        val documentos = transportDoc?.documentos

        if (!empresa.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_company_transport)))
            items.addAll(empresa.map { TransportComItemView(it) })
        }

        if (!documentos.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_document_transport)))
            items.addAll(documentos.map { TransportDocItemView(it) })
        }

        val isVisible = documentos.isNullOrEmpty() && empresa.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }
}