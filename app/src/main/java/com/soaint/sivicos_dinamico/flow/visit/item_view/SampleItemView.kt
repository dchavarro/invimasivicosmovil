package com.soaint.sivicos_dinamico.flow.visit.item_view

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import com.soaint.domain.model.SampleAnalysisEntity
import com.soaint.domain.model.SampleEntity
import com.soaint.domain.model.SamplePlanEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemSampleBinding
import com.soaint.sivicos_dinamico.databinding.ItemViewSampleAnalyticsBinding
import com.xwray.groupie.databinding.BindableItem

class SampleItemView(
    private val sample: SampleEntity,
    private val sampleAnalysis: List<SampleAnalysisEntity>,
    private val samplePlan: List<SamplePlanEntity>,
) : BindableItem<ItemSampleBinding>() {

    override fun bind(
        viewBinding: ItemSampleBinding,
        position: Int
    ) = with(viewBinding) {
        textViewGroup.text = sample.descGrupoProducto
        textViewSubgroup.text = sample.descSubGrupoProducto
        textViewNameProductRegistred.text = sample.nombreProducto
        textViewNso.text = sample.numRegisSaniProducto
        textViewGeneric.text = sample.nombreGenericoProducto
        textViewLote.text = sample.numLoteProducto
        textViewQuantity.text = sample.muestraTomarProducto.toString()
        textViewReferencce.text = sample.referencia
        textViewSample.text = sample.identificacionMuestra
        textViewPlan.text = samplePlan.map { it.descripcion }.joinToString(", ")
        linearLayoutAnalityc.removeAllViews()
        linearLayoutAnalityc.addItemView(sampleAnalysis)
    }

    private fun LinearLayout.addItemView(
        data: List<SampleAnalysisEntity>? = null,
    ) {
        val linearLayout = LinearLayoutCompat(context)
        linearLayout.layoutParams = LinearLayoutCompat.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        linearLayout.orientation = LinearLayoutCompat.VERTICAL
        data?.onEach {
            linearLayout.addTextView(it)
        }
        addView(linearLayout)
    }

    private fun LinearLayoutCompat.addTextView(
        data: SampleAnalysisEntity,
    ) {
        val binding = DataBindingUtil.inflate<ItemViewSampleAnalyticsBinding>(
            LayoutInflater.from(context),
            R.layout.item_view_sample_analytics,
            this,
            true
        )

        binding.textViewName.text = data.nombre
        binding.textViewDescription.text = data.parametroEvaluar
        binding.btnMore.setOnClickListener {
            binding.textViewDescription.apply {
                isVisible = !isVisible
            }
        }
    }

    override fun getLayout() = R.layout.item_sample
}