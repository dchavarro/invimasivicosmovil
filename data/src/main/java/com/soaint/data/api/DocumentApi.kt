package com.soaint.data.api

import com.soaint.data.model.DocumentBodyDto
import com.soaint.data.model.PdfResponseDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface DocumentApi {

    @POST("v1/MaestraDocumentoSP/crearDirectorio/")
    suspend fun addDocument(
        @Body body: DocumentBodyDto
    ): Unit

    @GET("v1/MaestraDocumentoSP/descargarArchivo/{idDocumento}")
    suspend fun viewDocument(
        @Path("idDocumento") idDocumento: Int,
    ): PdfResponseDto

}