package com.soaint.data.repository

import com.soaint.data.api.GeneralesPapfApi
import com.soaint.data.common.BaseRepository
import com.soaint.data.model.*
import com.soaint.data.room.dao.TasksPapfDao
import com.soaint.data.room.tables.mapToDomain
import com.soaint.domain.common.getIp
import com.soaint.domain.enum.StatusPapf
import com.soaint.domain.model.TaskPapfEntity
import com.soaint.domain.repository.TaskPapfRepository
import com.soaint.domain.repository.UserRepository
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import javax.inject.Inject

class TasksPapfRepositoryImpl @Inject constructor(
    private val userRepository: UserRepository,
    private val tasksPapfApi: GeneralesPapfApi,
    private val tasksPapfDao: TasksPapfDao
) : TaskPapfRepository, BaseRepository() {

    private val _tasks = MutableStateFlow<List<TaskPapfEntity>?>(null)
    override val tasks get() = _tasks.asStateFlow()

    //Carga de Tareas
    override suspend fun loadTasks(): Result<List<TaskPapfEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val body = TaskPapfBodyDto(getIp(), usuario, ParametrosPapfDto())
        val tasks = tasksPapfApi.getTasks(body).data
        tasksPapfDao.deleteAllTask()
        saveDao(tasks.orEmpty())
        Result.success(tasks.orEmpty().mapNotNull { it.mapToDomain() })
    }

    //Guardar Tareas en DB local
    private suspend fun saveDao(data: List<TaskPapfDto>) {
        data.onEach { task ->
            val model = task.mapToDb()
            tasksPapfDao.insertTask(model)
        }
    }

    //Carga de Tareas DB Local
    override suspend fun loadTasksLocal(): Result<List<TaskPapfEntity>> = execute {
        val usuario = userRepository.userInformation.value?.usuario.orEmpty()
        val tasks = tasksPapfDao.selectTask(usuario, StatusPapf.STATUSPAPF.status.asList())
        Result.success(tasks.map { it.mapToDomain() })
    }

    //Carga de Tareas DB Local
    override suspend fun loadTaskLocal(idSolicitud: Int): Result<TaskPapfEntity> = execute {
        val task = tasksPapfDao.selectTaskById(idSolicitud)
        task?.let { Result.success(it.mapToDomain()) }?: Result.failure(Throwable())
    }

/*
    //Carga de Tareas DB Local
    override suspend fun loadTasksLocalOnlyActive(): Result<List<TaskPapfEntity>> = execute {
        val idDependencia = userRepository.idDependencia
        val tasks = tasksDao.selectTaskOnlyActive(idDependencia)
        Result.success(tasks.map { it.mapToDomain() })
    }

    //Guardar Tareas en DB local
    private suspend fun saveDao(data: List<TaskDto>) {
        data.onEach { task ->
            val model = task.mapToDb()
            tasksDao.insertTask(model)
        }
    }

    //Update Tarea
    override suspend fun updateTask(task: TaskPapfEntity): Result<Unit> = execute {
        val result = tasksPapfApi.updateTask(task.mapToData())
        Result.success(result)
    }

    //Update Tarea en DB local
    override suspend fun updateTaskLocal(task: TaskPapfEntity?): Result<Unit> = execute {
        val result = task?.mapToData()?.mapToDb()?.let { tasksDao.updateTask(it) }
        Result.success(result)
    } as Result<Unit>*/
}