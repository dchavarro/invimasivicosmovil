package com.soaint.domain.use_case


import com.soaint.domain.model.EquipmentEntity
import com.soaint.domain.model.EquipmentUpdateEntity
import com.soaint.domain.repository.EquipmentRepository
import javax.inject.Inject

class EquipmentUseCase @Inject constructor(
    private val repository: EquipmentRepository
) {

    val equipment get() = repository.equipment

    suspend fun loadEquipment(idSede: Int): Result<List<EquipmentEntity>> {
        return repository.loadEquipment(idSede)
    }

    suspend fun loadEquipmentLocal(idSede: Int) = repository.loadEquipmentLocal(idSede)

    suspend fun addEquipment() = repository.addEquipment()

    suspend fun addEquipmentLocal(body: EquipmentEntity)  = repository.addEquipmentLocal(body)

    suspend fun updateEquipment() = repository.updateEquipment()

    suspend fun updateEquipmentLocal(body: EquipmentEntity) = repository.updateEquipmentLocal(body)

    suspend fun deleteEquipmentLocal(body: EquipmentEntity) = repository.deleteEquipmentLocal(body)
    suspend fun selectEquipmentUnfinished() :Result<List<EquipmentEntity>?> = repository.selectEquipmentUnfinished()
    suspend fun updateEquipmentBatch(body: List<EquipmentUpdateEntity>) = repository.updateEquipmentBatch(body)
}