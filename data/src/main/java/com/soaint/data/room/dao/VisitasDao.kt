package com.soaint.data.room.dao

import androidx.room.*
import com.soaint.data.common.orZero
import com.soaint.data.room.tables.*
import com.soaint.domain.model.HolidaysEntity
import com.soaint.domain.model.ScheduleTempEntity
import com.soaint.sivicos_dinamico.utils.AMTE_PQRS


@Dao
interface VisitasDao {

    //Gestion tablas de visitas
    @Query("SELECT * FROM visit_table WHERE idTarea = :idTask")
    suspend fun selectVisitas(idTask: Int): List<VisitModel>

    @Query("SELECT * FROM visit_table WHERE id = :idVisit")
    suspend fun selectVisita(idVisit: String): VisitModel?

    @Insert(entity = VisitModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVisitas(visitas: VisitModel)

    @Query("DELETE FROM visit_table")
    suspend fun deleteAllVisitas()

    @Query("DELETE FROM visit_table WHERE id = :idVisita")
    suspend fun deleteVisitaById(idVisita: Int)

    @Query("SELECT * FROM visit_forma_table WHERE idVisita = :visitId")
    suspend fun selectVisitForma(visitId: Int): VisitFormaModel?

    @Insert(entity = VisitFormaModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVisitForma(visitas: VisitFormaModel)


    //Gestion tablas de empresa
    @Query("SELECT * FROM company_table WHERE idVisita = :visitId")
    suspend fun selectEmpresa(visitId: Int): CompanyModel?

    @Query("SELECT * FROM company_table ORDER BY idEmpresa")
    suspend fun selectEmpresaAll(): List<CompanyModel>?

    @Query("SELECT * FROM company_address_table WHERE idEmpresa = :empresaId")
    suspend fun selectAddressEmpresa(empresaId: Int): CompanyAddressModel?

    @Query("SELECT * FROM company_rol_table WHERE empresaId = :empresaId")
    suspend fun selectRolEmpresa(empresaId: Int): List<CompanyRolModel>

    @Query("DELETE FROM company_rol_table WHERE empresaId = :empresaId")
    suspend fun deleteRolByCompany(empresaId: Int)

    @Insert(entity = CompanyRolModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCompanyRol(companyRol: CompanyRolModel)

    @Query("DELETE FROM company_table")
    suspend fun deleteAllCompany()

    @Query("DELETE FROM company_table WHERE idVisita = :idVisita")
    suspend fun deleteCompanyByVisita(idVisita: Int)

    @Query("SELECT * FROM company_representante_table WHERE empresaId = :empresaId AND idVisit = :idVisit")
    suspend fun selectRepresentante(empresaId: Int, idVisit: Int): List<CompanyRepresentanteModel>

    @Insert(entity = CompanyRepresentanteModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCompanyRepresentante(company: CompanyRepresentanteModel)

    @Query("DELETE FROM company_representante_table WHERE empresaId = :empresaId AND idVisit =:idVisit")
    suspend fun deleteRepresentanteByCompany(empresaId: Int, idVisit: Int)

    @Query("SELECT * FROM visit_group_table WHERE idVisita = :idVisita")
    suspend fun selectVisitGroup(idVisita: Int): List<VisitGroupModel>

    @Insert(entity = VisitGroupModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVisitGroup(group: VisitGroupModel)

    @Query("DELETE FROM visit_group_table WHERE idVisita = :idVisita")
    suspend fun deleteVisitGroupByIdVisit(idVisita: String)

    @Query("SELECT * FROM visit_subgroup_table WHERE idVisita = :idVisita")
    suspend fun selectVisitSubGroup(idVisita: Int): List<VisitSubGroupModel>

    @Insert(entity = VisitSubGroupModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVisitSubGroup(subgroup: VisitSubGroupModel)

    @Query("DELETE FROM visit_subgroup_table WHERE idVisita = :idVisita")
    suspend fun deleteVisitSubGroupByIdVisit(idVisita: String)

    //Gestion tablas de Antecedentes
    @Query("SELECT * FROM antecedent_table WHERE idVisita = :idVisit")
    suspend fun selectAntecedent(idVisit: String): List<AntecedentModel>

    @Query("SELECT * FROM antecedent_table WHERE idTarea = :idTask")
    suspend fun selectAntecedentForTask(idTask: Int?): List<AntecedentModel>

    @Query("SELECT * FROM antecedent_table WHERE id = :id")
    suspend fun selectAntecedentById(id: Int): AntecedentModel

    @Insert(entity = AntecedentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAntecedent(visitas: AntecedentModel)

    @Query("DELETE FROM antecedent_table")
    suspend fun deleteAllAntecedent()

    @Query("SELECT * FROM antecedent_table WHERE idVisita = :idVisit AND codigoTipo = :codigoTipo")
    suspend fun selectAntecedentByPqr(
        idVisit: String,
        codigoTipo: String = AMTE_PQRS
    ): List<AntecedentModel>


    //Gestion tablas de tramite
    @Query("SELECT * FROM visit_tramite_table WHERE idVisita = :idVisit")
    suspend fun selectTramite(idVisit: Int): VisitTramiteModel?

    @Insert(entity = VisitTramiteModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTramite(visitas: VisitTramiteModel)

    @Query("DELETE FROM visit_tramite_table")
    suspend fun deleteAllTramite()


    //Gestion tablas de Muestras
    @Query("SELECT * FROM sample_visit_table WHERE idVisita = :idVisit")
    suspend fun selectSample(idVisit: String): List<SampleModel>

    @Insert(entity = SampleModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSample(samples: SampleModel)

    @Query("DELETE FROM sample_visit_table")
    suspend fun deleteAllSample()

    //Gestion tablas de Muestras - Analisis requerido
    @Query("SELECT * FROM sample_analysis_table WHERE idMuestra = :idMuestra")
    suspend fun selectSampleAnalysis(idMuestra: String): List<SampleAnalysisModel>

    @Insert(entity = SampleAnalysisModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSampleAnalysis(samples: SampleAnalysisModel)

    @Query("DELETE FROM sample_analysis_table WHERE idMuestra = :idMuestra")
    suspend fun deleteSampleAnalysisByIdSample(idMuestra: String)

    //Gestion tablas de Muestras - Plan muestro
    @Query("SELECT * FROM sample_plan_table WHERE idMuestra = :idMuestra")
    suspend fun selectSamplePlan(idMuestra: String): List<SamplePlanModel>

    @Insert(entity = SamplePlanModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSamplePlan(samples: SamplePlanModel)

    @Query("DELETE FROM sample_plan_table WHERE idMuestra = :idMuestra")
    suspend fun deleteSamplePlanByIdSample(idMuestra: String)


    //Gestion tablas de funcionarios
    @Query("SELECT * FROM visit_official_table WHERE idVisita = :idVisit")
    suspend fun selectOfficial(idVisit: String): List<VisitOfficialModel>

    @Insert(entity = VisitOfficialModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertOfficial(official: VisitOfficialModel)

    @Query("DELETE FROM visit_official_table WHERE idVisita = :idVisit ")
    suspend fun deleteVisitOfficialByVisit(idVisit: String)

    //Gestion modificar visita
/*    @Update("UPDATE visit_table SET aplicaMedidaSanitaria = :aplicaMedidaSanitaria, legalAtiende = :legalAtiende WHERE id = :idVisit")
    suspend fun updateVisit(idVisit: String, aplicaMedidaSanitaria: Boolean?, legalAtiende: Boolean?)*/
    @Update(entity = VisitModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateVisit(visita: VisitModel)

    @Update(entity = AntecedentModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateAntecedentModel(model: AntecedentModel)


    @Transaction
    suspend fun updateAntecedent(model: AntecedentModel) {
        model.id = selectAntecedentById(model.id).id
        updateAntecedentModel(model)
    }

    @Update(entity = TransportBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateTransportDecomiso(transportBodyModel: TransportBodyModel)

    @Insert(entity = TransportBodyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTransportDecomiso(transportBodyModel: TransportBodyModel)


    //Gestion tablas de Interacciones
    @Query("SELECT id FROM interaction_table")
    suspend fun selectInteractionAll(): List<InteractionModel>?

    @Query("SELECT * FROM interaction_table WHERE idVisita = :idVisit")
    suspend fun selectInteraction(idVisit: String): InteractionModel?

    @Insert(entity = InteractionModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertInteraction(interaction: InteractionModel)

    @Update(entity = InteractionModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateInteractionModel(model: InteractionModel)

    @Transaction
    suspend fun updateInteraction(model: InteractionModel) {
        val item = selectInteraction(model.idVisita.orZero().toString().orEmpty())
        if (item != null) {
            model.id = item.id
            updateInteractionModel(model)
        } else {
            val newId = selectInteractionAll()?.lastOrNull()?.id?.orZero().orZero()
            model.id = newId + 1
            insertInteraction(model)
        }
    }

    @Query("DELETE FROM interaction_table WHERE idVisita = :idVisit")
    suspend fun deleteAllInteraction(idVisit: String?)


    //Gestion tablas de horas
    @Query("SELECT * FROM schedule_table where IdTipoHoraFacturacion in (7,8,9,10)")
    suspend fun selectScheduleForUpdate(): List<ScheduleModel>?

    @Insert(entity = ScheduleModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertSchedule(schedule: ScheduleModel)
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertScheduleList(horaFacturacionTramites: List<ScheduleModel>)

    @Insert(entity = ScheduleModelTemp::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertScheduleTemp(schedule: ScheduleModelTemp)
    @Query("DELETE FROM schedule_temp_table")
    suspend fun deleteAllScheduleTemp()

    @Query("SELECT * FROM schedule_temp_table where Codigo = :codigo ")
    suspend fun getQuantityHoursByCode(codigo: String):  ScheduleModelTemp?

    @Query(""" 
        SELECT th.Codigo as codigo, 
               CASE
                   WHEN th.Codigo IN ('TH_TOT', 'TH_CDIU', 'TH_CNOC', 'TH_CDFES', 'TH_CNFES')
                       THEN CAST(IFNULL(SUM(hf.cantidadHoras), 0) / 60 AS REAL)
                   ELSE SUM(hf.cantidadHoras)
               END AS cantidadHoras
        FROM schedule_table hf
        INNER JOIN type_hours_billing_table th ON hf.idTipoHoraFacturacion = th.idTipoHoraFacturacion
        WHERE hf.idTramite = :idTramite
        GROUP BY th.Codigo
    """)
    fun getQuantityHoursByTramite(idTramite: Int): List<ScheduleModelTemp>

    @Query("DELETE FROM schedule_table WHERE idTramite = :idTramite")
    suspend fun deleteScheduleByTramit(idTramite: Int)


    //Gestion tablas location
    @Query("SELECT * FROM location_table")
    suspend fun selectLocation(): List<LocationModel>?

    @Insert(entity = LocationModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertLocation(model: LocationModel)

    @Query("DELETE FROM location_table WHERE id = :id")
    suspend fun deleteLocationById(id: Int)


    // TABLAS DE EMPRESA
    @Transaction
    suspend fun insertOrUpdateCompany(company: CompanyModel, address: CompanyAddressModel) {
        val item: CompanyModel? = selectEmpresa(company.idVisita.orZero())
        if (item == null) {
            val newId = selectEmpresaAll()?.lastOrNull()?.idEmpresa?.orZero()
            company.idEmpresa = newId.orZero() + 1
            address.idEmpresa = company.idEmpresa
            insertCompany(company)
            insertCompanyAddress(address)
        } else {
            company.idEmpresa = item.idEmpresa
            address.idEmpresa = item.idEmpresa
            updateQueryCompany(
                company.activo,
                company.codigoSucursal,
                company.empresaAsociada,
                company.fechaCreacion,
                company.fechaModifica,
                company.idEmpresa,
                company.idEstadoEmpresa,
                company.idSede,
                company.idTipoDocumento,
                company.idVisita,
                company.numDocumento,
                company.razonSocial,
                company.tipoDocumento,
                company.usuarioCrea,
                company.usuarioModifica,
                company.telefono,
                company.celular,
                company.correo,
                company.paginaweb,
                company.digitoVerificacion,
                company.sigla,
                company.isLocal
            )
            updateCompanyAddress(
                address.descDepartamento,
                address.descMunicipio,
                address.descPais,
                address.descripcion,
                address.idDepartamento,
                address.idMunicipio,
                address.idPais,
                address.idEmpresa,
            )
        }
    }

    @Insert(entity = CompanyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCompany(company: CompanyModel)

    @Update(entity = CompanyModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun updateCompany(company: CompanyModel)

    @Query(
        "UPDATE company_table SET activo = :activo, codigoSucursal = :codigoSucursal, " +
                "empresaAsociada = :empresaAsociada, fechaCreacion = :fechaCreacion, fechaModifica = :fechaModifica, " +
                "idEmpresa = :idEmpresa, idEstadoEmpresa = :idEstadoEmpresa, idSede = :idSede, idTipoDocumento = :idTipoDocumento, " +
                "numDocumento = :numDocumento, razonSocial = :razonSocial, tipoDocumento = :tipoDocumento, " +
                "usuarioCrea = :usuarioCrea, usuarioModifica = :usuarioModifica, telefono = :telefono, celular = :celular, " +
                "correo = :correo, paginaweb = :paginaweb, digitoVerificacion = :digitoVerificacion, sigla = :sigla, isLocal = :isLocal" +
                " WHERE idVisita = :idVisita "
    )
    suspend fun updateQueryCompany(
        activo: Boolean,
        codigoSucursal: String?,
        empresaAsociada: String?,
        fechaCreacion: String?,
        fechaModifica: String?,
        idEmpresa: Int?,
        idEstadoEmpresa: Int?,
        idSede: Int?,
        idTipoDocumento: Int?,
        idVisita: Int?,
        numDocumento: String?,
        razonSocial: String?,
        tipoDocumento: String?,
        usuarioCrea: String?,
        usuarioModifica: String?,
        telefono: String?,
        celular: String?,
        correo: String?,
        paginaweb: String?,
        digitoVerificacion: Int?,
        sigla: String?,
        isLocal: Boolean?
    )

    @Insert(entity = CompanyAddressModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCompanyAddress(companyAddress: CompanyAddressModel)

    @Query("UPDATE company_address_table SET descDepartamento = :descDepartamento, descMunicipio = :descMunicipio, descPais = :descPais, descripcion = :descripcion, idDepartamento = :idDepartamento, idMunicipio = :idMunicipio, idPais = :idPais WHERE idEmpresa = :idEmpresa ")
    suspend fun updateCompanyAddress(
        descDepartamento: String,
        descMunicipio: String,
        descPais: String,
        descripcion: String,
        idDepartamento: Int?,
        idMunicipio: Int?,
        idPais: Int?,
        idEmpresa: Int?
    )

    @Query("DELETE FROM company_address_table WHERE idEmpresa = :empresaId")
    suspend fun deleteAddressByCompany(empresaId: Int)

    @Query("UPDATE company_table SET idEstadoEmpresa = :idEstadoEmpresa WHERE idVisita = :idVisita")
    suspend fun updateCompany(
        idEstadoEmpresa: Int?,
        idVisita: Int?,
    )

    @Insert(entity = HolidaysModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertHoliday(holiday: HolidaysModel)

    @Insert(entity = ShiftTypeModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertShiftType(shiftType: ShiftTypeModel)

    @Insert(entity = TypeHoursBillingModel::class, onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertTypeHoursBilling(typeHoursBilling: TypeHoursBillingModel)

    @Query("SELECT * FROM type_hours_billing_table WHERE idTipoHoraFacturacion = :idType")
    suspend fun getTypeHoursBilling(idType: Int): TypeHoursBillingModel
    @Query("SELECT * FROM shift_type_table WHERE jornada = :jornada")
    suspend fun selectJornada(jornada: String): ShiftTypeModel?

    @Query("SELECT EXISTS(SELECT 1 FROM holidays_table WHERE  fechaEvento = :fecha LIMIT 1)")
    suspend fun existsEventDate(fecha: String): Boolean
}