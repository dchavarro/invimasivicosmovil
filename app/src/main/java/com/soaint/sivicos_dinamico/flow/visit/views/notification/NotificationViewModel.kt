package com.soaint.sivicos_dinamico.flow.visit.views.notification

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.map
import com.soaint.domain.model.EmailsEntity
import com.soaint.domain.model.NotificationEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.NotificationUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.flow.main.views.task.OnTasksViewModelEvent
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

sealed class OnNotificationViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnNotificationViewModelEvent()
    data class OnViewModelReady(val emails: List<EmailsEntity>): OnNotificationViewModelEvent()
    object OnViewModelReadyAddEmail: OnNotificationViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnNotificationViewModelEvent()
}

class NotificationViewModel @Inject constructor(
    private val notificationUseCase: NotificationUseCase
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnNotificationViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    fun reload() = execute {
        val idVisit = _visitData.value?.map { it.id.toString().orEmpty() }.orEmpty()
        val resultEmails = idVisit.map { notificationUseCase.getEmailsLocal(it.toString()) }
        val emails = resultEmails.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        _event.value = OnNotificationViewModelEvent.OnViewModelReady(emails)
    }

    fun init(visitData: List<VisitEntity>) = execute {
        _event.value =
            OnNotificationViewModelEvent.OnChangeLabelPreloader(R.string.cargando_notification)
        _visitData.value = visitData
        val idVisit = visitData.map { it.id }
        val resultEmails = idVisit.map { notificationUseCase.getEmailsLocal(it.toString()) }
        val emails = resultEmails.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
        _event.value = OnNotificationViewModelEvent.OnViewModelReady(emails)
    }

    fun saveEmail(emails: List<String?>) = execute {
        val idVisit = _visitData.value?.firstOrNull()?.id.toString()
        emails.map { notificationUseCase.addEmail(idVisit, it.orEmpty()) }
        _event.value = OnNotificationViewModelEvent.OnViewModelReadyAddEmail
    }

    fun deleteEmail(id: Int?) = execute{
          notificationUseCase.deleteEmail(id)
    }
}