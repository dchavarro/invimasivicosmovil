package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface EquipmentRepository {

    val equipment: StateFlow<EquipmentEntity?>

    suspend fun loadEquipment(idSede: Int): Result<List<EquipmentEntity>>

    suspend fun loadEquipmentLocal(idSede: Int): Result<List<EquipmentEntity>>

    suspend fun addEquipment(): Result<List<Unit>>

    suspend fun addEquipmentLocal(body: EquipmentEntity): Result<Unit>

    suspend fun updateEquipment(): Result<List<Unit>>

    suspend fun updateEquipmentLocal(body: EquipmentEntity): Result<Unit>

    suspend fun deleteEquipmentLocal(body: EquipmentEntity): Result<Unit>
    suspend fun selectEquipmentUnfinished(): Result<List<EquipmentEntity>?>

    //cuando se diligencia el acta 030 se actualizan los equipos (cumple , observacion)
    suspend fun updateEquipmentBatch(body: List<EquipmentUpdateEntity>): Result<Unit>

}