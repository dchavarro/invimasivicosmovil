package com.soaint.sivicos_dinamico.flow.main.item_view

import com.soaint.domain.common.formatDate
import com.soaint.domain.model.TaskEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.databinding.ItemTaskBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.xwray.groupie.databinding.BindableItem

class TaskItemView(
    private val tarea: TaskEntity,
    private val onTareaSelected: (TaskEntity) -> Unit
) : BindableItem<ItemTaskBinding>() {

    override fun bind(
        viewBinding: ItemTaskBinding,
        position: Int
    ) = with(viewBinding) {
        txtIdActividad.text = tarea.codigoTarea
        txtActividad.text = tarea.descripcionActividad
        txtEstado.text = tarea.descEstado
        txtFecha.text = tarea.fechaTarea?.formatDate()
        txtOrigen.text = tarea.descripcionOrigen

        root.setOnSingleClickListener {
            onTareaSelected.invoke(tarea)
        }
    }

    override fun getLayout() = R.layout.item_task
}