package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.*
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*

data class VisitResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<VisitDto>,
    @SerializedName("statuscode") val statuscode: Int
)

data class VisitDto(
    @SerializedName("id") val id: Int,
    @SerializedName("activo") val activo: Boolean = true,
    @SerializedName("descClasificacion") val descClasificacion: String?,
    @SerializedName("descFuenteInformacion") val descFuenteInformacion: String?,
    @SerializedName("descGrupTrab") val descGrupTrab: String?,
    @SerializedName("descPrioridad") val descPrioridad: String?,
    @SerializedName("descRazon") val descRazon: String?,
    @SerializedName("descResponRealVisita") val descResponRealVisita: String?,
    @SerializedName("descTipoProducto") val descTipoProducto: String?,
    @SerializedName("descripcionMotivo") val descripcionMotivo: String?,
    @SerializedName("empresa") val empresa: VisitEmpresaDto?,
    @SerializedName("fecProximaProgramacion") val fecProximaProgramacion: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("fechaRealizo") val fechaRealizo: String?,
    @SerializedName("formaRealizarVisita") val formaRealizarVisita: VisitFormaDto?,
    @SerializedName("idClasificacion") val idClasificacion: Int?,
    @SerializedName("idDistancia") val idDistancia: Int?,
    @SerializedName("idEstado") val idEstado: Int?,
    @SerializedName("idFuenteInformacion") val idFuenteInformacion: Int?,
    @SerializedName("idGrupoTrabajo") val idGrupoTrabajo: Int?,
    @SerializedName("idModEjecutar") val idModEjecutar: Int?,
    @SerializedName("idOficinaGTT") val idOficinaGTT: Int?,
    @SerializedName("idPosible") val idPosible: Int?,
    @SerializedName("idPrioridad") val idPrioridad: Int?,
    @SerializedName("idRazon") val idRazon: Int?,
    @SerializedName("idResponsableRealizar") val idResponsableRealizar: Int?,
    @SerializedName("idResultado") val idResultado: Int?,
    @SerializedName("idTarea") val idTarea: Int?,
    @SerializedName("idTipoProducto") val idTipoProducto: Int?,
    @SerializedName("numeroVisita") val numeroVisita: String?,
    @SerializedName("objetivo") val objetivo: String?,
    @SerializedName("observacionResultado") val observacionResultado: String?,
    @SerializedName("requiAcompa") val requiAcompa: Boolean?,
    @SerializedName("tramite") val tramite: VisitTramiteDto?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("fechaInicioEjecucion") val fechaInicioEjecucion: String?,
    @SerializedName("descDistancia") val descDistancia: String?,
    @SerializedName("posibleMotivo") val posibleMotivo: String?,
    @SerializedName("modalidadEjecutar") val modalidadEjecutar: String?,
    @SerializedName("aplicaMedidaSanitaria") val aplicaMedidaSanitaria: Boolean?,
    @SerializedName("legalAtiende") val legalAtiende: Boolean?,
    @SerializedName("requiereReprogramarVisita") val requiereReprogramarVisita: Boolean?,
    @SerializedName("fechaVerificacionRequerimiento") val fechaVerificacionRequerimiento: String?,
    @SerializedName("idConceptoSanitario") val idConceptoSanitario: Int?,
    @SerializedName("descripcionConcepto") val descripcionConcepto: String?,
    @SerializedName("calificacion") val calificacion: String?,
    @SerializedName("fechaCierre") val fechaCierre: String?,
    @SerializedName("idAccionSeguir") val idAccionSeguir: Int?,
    @SerializedName("codigoRazon") val codigoRazon: String?,
    @SerializedName("distancia") val distancia: String?,
    @SerializedName("idEstadoEmpresa") val idEstadoEmpresa: Int?,
    @SerializedName("operacionEstablecimiento") val operacionEstablecimiento: String?,
    @SerializedName("especie") val especie: String?,
)

data class VisitEmpresaDto(
    @SerializedName("activo") val activo: Boolean = true,
    @SerializedName("codigoSucursal") val codigoSucursal: String?,
    @SerializedName("direccion") val direccion: VisitDireccionDto?,
    @SerializedName("empresaAsociada") val empresaAsociada: String?,
    @SerializedName("fechaCreacion") val fechaCreacion: String?,
    @SerializedName("fechaModifica") val fechaModifica: String?,
    @SerializedName("idEmpresa") val idEmpresa: Int? = 0,
    @SerializedName("idEstadoEmpresa") var idEstadoEmpresa: Int? = 0,
    @SerializedName("idSede") val idSede: Int? = 0,
    @SerializedName("idTipoDocumento") val idTipoDocumento: Int? = 0,
    @SerializedName("idVisita") val idVisita: Int? = 0,
    @SerializedName("numDocumento") val numDocumento: String?,
    @SerializedName("razonSocial") val razonSocial: String?,
    @SerializedName("rolSucursal") val rolSucursal: List<VisitCompanyRolDto>?,
    @SerializedName("tipoDocumento") val tipoDocumento: String?,
    @SerializedName("usuarioCrea") val usuarioCrea: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
    @SerializedName("telefono") val telefono: String?,
    @SerializedName("celular") val celular: String?,
    @SerializedName("correo") val correo: String?,
    @SerializedName("representanteLegal") val representanteLegal: List<VisitRepresentanteDto>?,
    @SerializedName("paginaweb") val paginaweb: String?,
    @SerializedName("digitoVerificacion") val digitoVerificacion: Int? = 0,
    @SerializedName("sigla") val sigla: String?,
    @SerializedName("PersonasVinculadas") val personasVinculadas: List<VisitCompanyPersonDto>?,
)

data class VisitRepresentanteDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("CorreoElectronico") val correoElectronico: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("Celular") val celular: String?,
)

data class VisitDireccionDto(
    @SerializedName("descDepartamento") val descDepartamento: String?,
    @SerializedName("descMunicipio") val descMunicipio: String?,
    @SerializedName("descPais") val descPais: String?,
    @SerializedName("descripcion") val descripcion: String?,
    @SerializedName("idDepartamento") val idDepartamento: Int?,
    @SerializedName("idMunicipio") val idMunicipio: Int?,
    @SerializedName("idPais") val idPais: Int?,
)

data class VisitCompanyRolDto(
    @SerializedName("rol") val rol: String?,
)

data class VisitFormaDto(
    @SerializedName("id") val id: Int,
    @SerializedName("codigo") val codigo: String?,
    @SerializedName("descripcion") val descripcion: String?,
)

data class VisitCompanyPersonDto(
    @SerializedName("codigoTipoDocumentoPersona") val codigoTipoDocumentoPersona: Int?,
    @SerializedName("TipoDocumento") val tipoDocumento: String?,
    @SerializedName("numeroDocumentoPersona") val numeroDocumentoPersona: String?,
    @SerializedName("primerNombre") val primerNombre: String,
    @SerializedName("segundoNombre") val segundoNombre: String?,
    @SerializedName("primerApellido") val primerApellido: String?,
    @SerializedName("segundoApellido") val segundoApellido: String?,
    @SerializedName("idRolPersona") val idRolPersona: Int?,
    @SerializedName("DescripcionRolPersona") val descripcionRolPersona: String?,
    @SerializedName("correoElectronicoP") val correoElectronicoP: String?,
)

data class VisitTramiteDto(
    @SerializedName("id") val id: Int?,
    @SerializedName("idTipo") val idTipo: Int?,
    @SerializedName("descTipoTramite") val descTipoTramite: String?,
    @SerializedName("idSubTipo") val idSubTipo: Int?,
    @SerializedName("descSubTipoTramite") val descSubTipoTramite: String?,
    @SerializedName("fecRadicacion") val fecRadicacion: String?,
    @SerializedName("numeroRadicacion") val numeroRadicacion: String?,
    @SerializedName("idTramite") val idTramite: Int?,
    @SerializedName("indNotificarResultado") val indNotificarResultado: Boolean? = false,
    @SerializedName("indReprogramada") val indReprogramada: Boolean? = false,
    @SerializedName("expediente") val expediente: String?,
)

// map Entity
fun VisitDto.mapToDomain() = VisitEntity(
    activo,
    descClasificacion.orEmpty(),
    descFuenteInformacion.orEmpty(),
    descGrupTrab,
    descPrioridad.orEmpty(),
    descRazon.orEmpty(),
    descResponRealVisita.orEmpty(),
    descTipoProducto.orEmpty(),
    descripcionMotivo,
    empresa?.mapToDomain(),
    fecProximaProgramacion.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    fechaRealizo.orEmpty(),
    formaRealizarVisita?.mapToDomain(id),
    id,
    idClasificacion,
    idDistancia,
    idEstado,
    idFuenteInformacion,
    idGrupoTrabajo,
    idModEjecutar,
    idOficinaGTT,
    idPosible,
    idPrioridad,
    idRazon,
    idResponsableRealizar,
    idResultado,
    idTarea,
    idTipoProducto,
    numeroVisita.orEmpty(),
    objetivo.orEmpty(),
    observacionResultado.orEmpty(),
    requiAcompa,
    tramite?.mapToDomain(id),
    usuarioCrea.orEmpty(),
    usuarioModifica.orEmpty(),
    fechaInicioEjecucion.orEmpty(),
    descDistancia.orEmpty(),
    posibleMotivo.orEmpty(),
    modalidadEjecutar.orEmpty(),
    aplicaMedidaSanitaria,
    legalAtiende,
    requiereReprogramarVisita,
    fechaVerificacionRequerimiento.orEmpty(),
    idConceptoSanitario,
    descripcionConcepto.orEmpty(),
    calificacion.orEmpty(),
    fechaCierre.orEmpty(),
    idAccionSeguir,
    codigoRazon.orEmpty(),
    distancia.orEmpty(),
    idEstadoEmpresa,
    operacionEstablecimiento,
    especie
)

fun VisitEntity.mapToData() = VisitDto(
    id,
    activo,
    descClasificacion,
    descFuenteInformacion,
    descGrupTrab,
    descPrioridad,
    descRazon,
    descResponRealVisita,
    descTipoProducto,
    descripcionMotivo,
    empresa?.mapToData(),
    fecProximaProgramacion,
    fechaCreacion,
    fechaModifica,
    fechaRealizo,
    formaRealizarVisita?.mapToData(),
    idClasificacion,
    idDistancia,
    idEstado,
    idFuenteInformacion,
    idGrupoTrabajo,
    idModEjecutar,
    idOficinaGTT,
    idPosible,
    idPrioridad,
    idRazon,
    idResponsableRealizar,
    idResultado,
    idTarea,
    idTipoProducto,
    numeroVisita,
    objetivo,
    observacionResultado,
    requiAcompa,
    tramite?.mapToData(),
    usuarioCrea,
    usuarioModifica,
    fechaInicioEjecucion,
    descDistancia,
    posibleMotivo,
    modalidadEjecutar,
    aplicaMedidaSanitaria,
    legalAtiende,
    requiereReprogramarVisita,
    fechaVerificacionRequerimiento,
    idConceptoSanitario,
    descripcionConcepto,
    calificacion,
    fechaCierre,
    idAccionSeguir,
    codigoRazon,
    distancia,
    idEstadoEmpresa,
    operacionEstablecimiento,
    especie
)

fun VisitEmpresaDto.mapToDomain() = VisitEmpresaEntity(
    activo,
    codigoSucursal.orEmpty(),
    direccion?.mapToDomain(),
    empresaAsociada.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    idEmpresa,
    idEstadoEmpresa,
    idSede,
    idTipoDocumento,
    idVisita,
    numDocumento.orEmpty(),
    razonSocial.orEmpty(),
    rolSucursal?.map { it.mapToDomain() }.orEmpty(),
    tipoDocumento.orEmpty(),
    usuarioCrea.orEmpty(),
    usuarioModifica.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
    correo.orEmpty(),
    representanteLegal?.map { it.mapToDomain() }.orEmpty(),
    paginaweb.orEmpty(),
    digitoVerificacion,
    sigla.orEmpty(),
    personasVinculadas?.map { it.mapToDomain() }.orEmpty(),
)

fun VisitEmpresaEntity.mapToData() = VisitEmpresaDto(
    activo,
    codigoSucursal.orEmpty(),
    direccion?.mapToData(),
    empresaAsociada.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    idEmpresa,
    idEstadoEmpresa,
    idSede,
    idTipoDocumento,
    idVisita,
    numDocumento.orEmpty(),
    razonSocial.orEmpty(),
    rolSucursal?.map { it.mapToData() }.orEmpty(),
    tipoDocumento.orEmpty(),
    usuarioCrea.orEmpty(),
    usuarioModifica.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
    correo.orEmpty(),
    representanteLegal?.map { it.mapToData() }.orEmpty(),
    paginaweb.orEmpty(),
    digitoVerificacion,
    sigla.orEmpty(),
    personasVinculadas?.map { it.mapToData() }.orEmpty(),
)

fun VisitRepresentanteDto.mapToDomain() = VisitRepresentanteEntity(
    nombre.orEmpty(),
    correoElectronico.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
)

fun VisitRepresentanteEntity.mapToData() = VisitRepresentanteDto(
    nombre.orEmpty(),
    correoElectronico.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
)

fun VisitDireccionDto.mapToDomain() = VisitDireccionEntity(
    descDepartamento.orEmpty(),
    descMunicipio.orEmpty(),
    descPais.orEmpty(),
    descripcion.orEmpty(),
    idDepartamento,
    idMunicipio,
    idPais,
)

fun VisitDireccionEntity.mapToData() = VisitDireccionDto(
    descDepartamento.orEmpty(),
    descMunicipio.orEmpty(),
    descPais.orEmpty(),
    descripcion.orEmpty(),
    idDepartamento,
    idMunicipio,
    idPais,
)

fun VisitCompanyRolDto.mapToDomain() = VisitCompanyRolEntity(
    rol?.orEmpty()
)

fun VisitCompanyRolEntity.mapToData() = VisitCompanyRolDto(
    rol?.orEmpty()
)

fun VisitCompanyPersonDto.mapToDomain() = VisitCompanyPersonEntity(
    codigoTipoDocumentoPersona.orEmpty(),
    tipoDocumento.orEmpty(),
    numeroDocumentoPersona.orEmpty(),
    primerNombre.orEmpty(),
    segundoNombre.orEmpty(),
    primerApellido.orEmpty(),
    segundoApellido.orEmpty(),
    idRolPersona.orEmpty(),
    descripcionRolPersona.orEmpty(),
    correoElectronicoP.orEmpty(),
)

fun VisitCompanyPersonEntity.mapToData() = VisitCompanyPersonDto(
    codigoTipoDocumentoPersona.orEmpty(),
    tipoDocumento.orEmpty(),
    numeroDocumentoPersona.orEmpty(),
    primerNombre.orEmpty(),
    segundoNombre.orEmpty(),
    primerApellido.orEmpty(),
    segundoApellido.orEmpty(),
    idRolPersona.orEmpty(),
    descripcionRolPersona.orEmpty(),
    correoElectronicoP.orEmpty()
)

fun VisitFormaDto.mapToDomain(idVisita: Int) = VisitFormaEntity(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
    idVisita
)

fun VisitFormaEntity.mapToData() = VisitFormaDto(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
)

fun VisitTramiteDto.mapToDomain(idVisita: Int) = VisitTramiteEntity(
    id, idTipo, descTipoTramite, idSubTipo, descSubTipoTramite, fecRadicacion, numeroRadicacion,
    idTramite, indNotificarResultado, indReprogramada, expediente, idVisita
)

fun VisitTramiteEntity.mapToData() = VisitTramiteDto(
    id, idTipo, descTipoTramite, idSubTipo, descSubTipoTramite, fecRadicacion, numeroRadicacion,
    idTramite, indNotificarResultado, indReprogramada, expediente
)

// mapToDB
fun VisitDto.mapToDb(isClosed: Boolean) = VisitModel(
    id,
    activo,
    descClasificacion.orEmpty(),
    descFuenteInformacion.orEmpty(),
    descGrupTrab,
    descPrioridad.orEmpty(),
    descRazon.orEmpty(),
    descResponRealVisita.orEmpty(),
    descTipoProducto.orEmpty(),
    descripcionMotivo,
    //empresa?.mapToDomain(),
    fecProximaProgramacion.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    fechaRealizo.orEmpty(),
    //formaRealizarVisita?.mapToDomain(),
    idClasificacion,
    idDistancia,
    idEstado,
    idFuenteInformacion,
    idGrupoTrabajo,
    idModEjecutar,
    idOficinaGTT,
    idPosible,
    idPrioridad,
    idRazon,
    idResponsableRealizar,
    idResultado,
    idTarea,
    idTipoProducto,
    numeroVisita.orEmpty(),
    objetivo.orEmpty(),
    observacionResultado.orEmpty(),
    requiAcompa,
    //tramite.orEmpty(),
    usuarioCrea,
    usuarioModifica.orEmpty(),
    fechaInicioEjecucion.orEmpty(),
    descDistancia.orEmpty(),
    posibleMotivo.orEmpty(),
    modalidadEjecutar.orEmpty(),
    aplicaMedidaSanitaria,
    legalAtiende,
    requiereReprogramarVisita,
    fechaVerificacionRequerimiento.orEmpty(),
    idConceptoSanitario,
    descripcionConcepto.orEmpty(),
    calificacion.orEmpty(),
    fechaCierre.orEmpty(),
    idAccionSeguir,
    codigoRazon,
    distancia,
    idEstadoEmpresa,
    operacionEstablecimiento,
    especie,
    isClosed
)

fun VisitEmpresaDto.mapToDb(idVisit: Int, isLocal: Boolean) = CompanyModel(
    activo,
    codigoSucursal.orEmpty(),
    //direccion.mapToDomain(),
    empresaAsociada.orEmpty(),
    fechaCreacion.orEmpty(),
    fechaModifica.orEmpty(),
    idEmpresa,
    idEstadoEmpresa,
    idSede,
    idTipoDocumento,
    idVisit,
    numDocumento.orEmpty(),
    razonSocial.orEmpty(),
    //rolSucursal,
    tipoDocumento.orEmpty(),
    usuarioCrea.orEmpty(),
    usuarioModifica.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
    correo.orEmpty(),
    //representanteLegal.orEmpty()
    paginaweb.orEmpty(),
    digitoVerificacion,
    sigla.orEmpty(),
    isLocal
)

fun VisitRepresentanteDto.mapToDb(idEmpresa: Int, idVisit: Int) = CompanyRepresentanteModel(
    nombre.orEmpty(),
    correoElectronico.orEmpty(),
    telefono.orEmpty(),
    celular.orEmpty(),
    idEmpresa,
    idVisit
)

fun VisitDireccionDto.mapToDb(idEmpresa: Int) = CompanyAddressModel(
    descDepartamento.orEmpty(),
    descMunicipio.orEmpty(),
    descPais.orEmpty(),
    descripcion.orEmpty(),
    idDepartamento,
    idMunicipio,
    idPais,
    idEmpresa,
)

fun VisitCompanyRolDto.mapToDb(idEmpresa: Int) = CompanyRolModel(
    rol.orEmpty(),
    idEmpresa,
)

fun VisitFormaDto.mapToDb(idVisita: Int) = VisitFormaModel(
    id,
    codigo.orEmpty(),
    descripcion.orEmpty(),
    idVisita,
)

fun VisitTramiteDto.mapToDb(idVisita: Int) = VisitTramiteModel(
    id, idTipo, descTipoTramite, idSubTipo, descSubTipoTramite, fecRadicacion, numeroRadicacion,
    idTramite, indNotificarResultado, indReprogramada, expediente, idVisita
)

data class BodyGenericVisitDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: InputVisitDto,
) {
    constructor(ip: String, user: String, idVisita: Int) : this(
        AuditoriaDto(ip, user), InputVisitDto(idVisita)
    )
}

data class VisitOfficialResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<VisitOfficialDto>,
    @SerializedName("statuscode") val statuscode: Int
)

data class VisitOfficialDto(
    @SerializedName("usuario") val usuario: String?,
    @SerializedName("nombreCompleto") val nombreCompleto: String?,
    @SerializedName("tipoDocumento") val tipoDocumento: String?,
    @SerializedName("numeroDocumento") val numeroDocumento: String?,
    @SerializedName("cargo") val cargo: String?
)

fun VisitOfficialDto.mapToDomain(idVisita: Int) = VisitOfficialEntity(
    usuario, nombreCompleto, tipoDocumento, numeroDocumento, cargo, idVisita
)

fun VisitOfficialDto.mapToDb(idVisita: Int) = VisitOfficialModel(
    usuario, nombreCompleto, tipoDocumento, numeroDocumento, cargo, idVisita
)


data class VisitGroupResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<VisitGroupDto>,
    @SerializedName("statuscode") val statuscode: Int
)

data class VisitGroupDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("id") val id: Int?
)

fun VisitGroupDto.mapToDomain() = VisitGroupEntity(
    nombre, idVisita, id
)

fun VisitGroupDto.mapToDb() = VisitGroupModel(
    nombre, idVisita, id
)

data class VisitSubGroupResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<VisitSubGroupDto>,
    @SerializedName("statuscode") val statuscode: Int
)

data class VisitSubGroupDto(
    @SerializedName("nombre") val nombre: String?,
    @SerializedName("idVisita") val idVisita: Int?,
    @SerializedName("id") val id: Int?

)

fun VisitSubGroupDto.mapToDomain() = VisitGroupEntity(
    nombre, idVisita, id
)

fun VisitSubGroupDto.mapToDb() = VisitSubGroupModel(
    nombre, idVisita, id
)

data class VisitUpdateBodyDto(
    @SerializedName("id") val id: Int,
    @SerializedName("aplicaMedidaSanitaria") val aplicaMedidaSanitaria: Boolean,
    @SerializedName("legalAtiende") val legalAtiende: Boolean,
)

fun VisitUpdateBodyEntity.mapToData() = VisitUpdateBodyDto(
    id,
    aplicaMedidaSanitaria,
    legalAtiende
)

data class LocationBodyDto(
    @SerializedName("latitud") val latitud: String,
    @SerializedName("longitud") val longitud: String,
    @SerializedName("idPersona") val idPersona: Int,
    @SerializedName("fechaCreacion") val fechaCreacion: String,
    @SerializedName("usuarioCrea") val usuarioCrea: String,
)

fun LocationBodyDto.mapToDb() = LocationModel(
    latitud,
    longitud,
    idPersona,
    fechaCreacion,
    usuarioCrea,
)

data class DateMssAppliedResponseDto(
    @SerializedName("message") val message: String?,
    @SerializedName("objectResponse") val objectResponse: String?,
    @SerializedName("statusCode") val statusCode: Int?
)


