package com.soaint.data.model

import com.google.gson.annotations.SerializedName

data class ProcessExtBodyDto(
    @SerializedName("idTramite") val idTramite: Int?,
    @SerializedName("idPersona") val idPersona: Int?,
    @SerializedName("codigoProcesoExterno") val codigoProcesoExterno: String?,
    @SerializedName("usuarioModifica") val usuarioModifica: String?,
)