package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.room.tables.DestinoLotePapfModel
import com.soaint.data.room.tables.DocumentPapfModel
import com.soaint.data.room.tables.RegisterProductPapfModel
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.DestinoLotePapfEntity
import com.soaint.domain.model.DocumentPapfEntity
import com.soaint.domain.model.RegisterProductObjectPapfEntity
import com.soaint.domain.model.RegisterProductPapfEntity

data class RegisterProductPapfResponseDto(
    @SerializedName("message") val message: MessageDto,
    @SerializedName("objectResponse") val data: RegisterProductObjectPapfDto?,
    @SerializedName("statusCode") val statusCode: Int,
)

data class RegisterProductObjectPapfDto(
    @SerializedName("registroProductos") val registroProductos: List<RegisterProductPapfDto>?,
    @SerializedName("destinoLoteSolicitud") val destinoLoteSolicitud: List<DestinoLotePapfDto>?,
    @SerializedName("documentos") val documentos: List<DocumentPapfDto>?,
)

fun RegisterProductObjectPapfDto.mapToDomain() = RegisterProductObjectPapfEntity(
    registroProductos?.map { it.mapToDomain() }.orEmpty(),
    destinoLoteSolicitud?.map { it.mapToDomain() }.orEmpty(),
    documentos?.map { it.mapToDomain() }.orEmpty(),
)

data class RegisterProductPapfDto(
    @SerializedName("IdSolicitud") val idSolicitud: Int? = 0,
    @SerializedName("IdProductoSolicitud") val idProductoSolicitud: Int? = 0,
    @SerializedName("IdClasificacionProducto") val idClasificacionProducto: Int? = 0,
    @SerializedName("ClasificacionProducto") val clasificacionProducto: String?,
    @SerializedName("Producto") val producto: String?,
    @SerializedName("RegistroSanitario") val registroSanitario: String?,
    @SerializedName("IdEmpresaFabricante") val idEmpresaFabricante: Int? = 0,
    @SerializedName("OrigenFabricante") val origenFabricante: String?,
    @SerializedName("IdDetalleProducto") val idDetalleProducto: Int? = 0,
    @SerializedName("Lote") val lote: String?,
    @SerializedName("Cantidad") val cantidad: String?,
    @SerializedName("PresentacionComercial") val presentacionComercial: String?,
    @SerializedName("PesoUnidadMedida") val pesoUnidadMedida: Double? = 0.0,
    @SerializedName("UnidadDeMedida") val unidadDeMedida: String?,
    @SerializedName("PesoNeto") val pesoNeto: String?,
    @SerializedName("Marca") val marca: String?,
    @SerializedName("TemperaturaConservacion") val temperaturaConservacion: String?,
    @SerializedName("FechaVencimiento") val fechaVencimiento: String?,
)

fun RegisterProductPapfDto.mapToDomain() = RegisterProductPapfEntity(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto,
    producto.orEmpty(),
    registroSanitario.orEmpty(),
    idEmpresaFabricante.orEmpty(),
    origenFabricante.orEmpty(),
    idDetalleProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    presentacionComercial.orEmpty(),
    pesoUnidadMedida.orEmpty(),
    unidadDeMedida.orEmpty(),
    pesoNeto.orEmpty(),
    marca.orEmpty(),
    temperaturaConservacion.orEmpty(),
    fechaVencimiento.orEmpty(),
)

fun RegisterProductPapfDto.mapToDb() = RegisterProductPapfModel(
    idSolicitud.orEmpty(),
    idProductoSolicitud.orEmpty(),
    idClasificacionProducto.orEmpty(),
    clasificacionProducto,
    producto.orEmpty(),
    registroSanitario.orEmpty(),
    idEmpresaFabricante.orEmpty(),
    origenFabricante.orEmpty(),
    idDetalleProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    presentacionComercial.orEmpty(),
    pesoUnidadMedida.orEmpty(),
    unidadDeMedida.orEmpty(),
    pesoNeto.orEmpty(),
    marca.orEmpty(),
    temperaturaConservacion.orEmpty(),
    fechaVencimiento.orEmpty(),
)

data class DestinoLotePapfDto(
    @SerializedName("NumeroDocumento") val numeroDocumento: String?,
    @SerializedName("IdSedeEmpresaDestino") val idSedeEmpresaDestino: Int? = 0,
    @SerializedName("RazonSocial") val razonSocial: String?,
    @SerializedName("Sucursal") val sucursal: String?,
    @SerializedName("Departamento") val departamento: String?,
    @SerializedName("Municipio") val municipio: String?,
    @SerializedName("Direccion") val direccion: String?,
    @SerializedName("Telefono") val telefono: String?,
    @SerializedName("Contacto") val contacto: String?,
    @SerializedName("IdDestinoProducto") val idDestinoProducto: Int? = 0,
    @SerializedName("DescripcionDestinoProducto") val descripcionDestinoProducto: String?,
    @SerializedName("Lote") val lote: String?,
    @SerializedName("Cantidad") val cantidad: Int? = 0,
    @SerializedName("IdMPIG") val idMPIG: Int? = 0,
    @SerializedName("DescripcionUsoMPIG") val descripcionUsoMPIG: String?,
)

fun DestinoLotePapfDto.mapToDomain() = DestinoLotePapfEntity(
    numeroDocumento.orEmpty(),
    idSedeEmpresaDestino.orEmpty(),
    razonSocial.orEmpty(),
    sucursal.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    contacto.orEmpty(),
    idDestinoProducto.orEmpty(),
    descripcionDestinoProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    idMPIG.orEmpty(),
    descripcionUsoMPIG.orEmpty(),
)

fun DestinoLotePapfDto.mapToDb(idSolicitud: Int?) = DestinoLotePapfModel(
    numeroDocumento.orEmpty(),
    idSedeEmpresaDestino.orEmpty(),
    razonSocial.orEmpty(),
    sucursal.orEmpty(),
    departamento.orEmpty(),
    municipio.orEmpty(),
    direccion.orEmpty(),
    telefono.orEmpty(),
    contacto.orEmpty(),
    idDestinoProducto.orEmpty(),
    descripcionDestinoProducto.orEmpty(),
    lote.orEmpty(),
    cantidad.orEmpty(),
    idMPIG.orEmpty(),
    descripcionUsoMPIG.orEmpty(),
    idSolicitud.orEmpty(),
)

data class DocumentPapfDto(
    @SerializedName("IdDocumento") val idDocumento: Int? = 0,
    @SerializedName("NombreDocumento") val nombreDocumento: String?,
)

fun DocumentPapfDto.mapToDomain() = DocumentPapfEntity(
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty()
)

fun DocumentPapfDto.mapToDb(idSolicitud: Int?) = DocumentPapfModel(
    idDocumento.orEmpty(),
    nombreDocumento.orEmpty(),
    idSolicitud.orEmpty(),
)