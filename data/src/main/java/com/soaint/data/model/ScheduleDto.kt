package com.soaint.data.model

import com.google.gson.annotations.SerializedName
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.room.tables.ScheduleModel
import com.soaint.domain.model.ScheduleEntity

data class BodyGenericScheduleDto(
    @SerializedName("cabecera") val cabecera: AuditoriaDto,
    @SerializedName("entrada") val entrada: InputTramitDto,
) {
    constructor(ip: String, user: String, idTramite: Int) : this(
        AuditoriaDto(ip, user), InputTramitDto(idTramite)
    )
}

data class InputTramitDto(
    @SerializedName("idTramite") val idTramite: Int,
)

data class ScheduleResponseDto(
    @SerializedName("message") val message: String,
    @SerializedName("objectResponse") val objectResponse: List<ScheduleDto>? = null,
    @SerializedName("statusCode") val statusCode: Int
)

data class ScheduleDto(
    @SerializedName("IdTramite") val idTramite: Int? = 0,
    @SerializedName("IdTipoHoraFacturacion") val idTipoHoraFacturacion: Int? = 0,
    @SerializedName("Codigo") val codigo: String? = EMPTY_STRING,
    @SerializedName("tipoHora") val tipoHora: String? = EMPTY_STRING,
    @SerializedName("cantidadHoras") val cantidadHoras: Double? = 0.0,
)
data class ScheduleUploadDto(
    @SerializedName("idTipoHora") val idTipoHora: Int? = 0,
    @SerializedName("cantidadHoras") val cantidadHoras: Int? = 0,
)
data class BodyScheduleUploaDto(
    @SerializedName("tipoHora") val tipoHora: List<ScheduleUploadDto>? = null,
    @SerializedName("idTramite") val idTramite: Int?
)

fun ScheduleDto.mapToDomain() = ScheduleEntity(
    idTramite,
    idTipoHoraFacturacion,
    codigo,
    tipoHora,
    cantidadHoras,
)

fun ScheduleDto.mapToDb() = ScheduleModel(
    idTramite,
    idTipoHoraFacturacion,
    codigo,
    tipoHora,
    cantidadHoras,
)

fun ScheduleEntity.mapToData() = ScheduleDto(
    idTramite,
    idTipoHoraFacturacion,
    codigo,
    tipoHora,
    cantidadHoras,
)