package com.soaint.data.api

import com.soaint.data.model.LoginBodyDto
import com.soaint.data.model.LoginDto
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthApi {

    @POST("v1/autenticacion/loginToken")
    suspend fun getToken(
        @Body body: LoginBodyDto
    ): LoginDto
}