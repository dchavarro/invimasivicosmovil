package com.soaint.sivicos_dinamico.flow.papf.views.close_insp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.model.ClasificationTramitPapfEntity
import com.soaint.domain.model.CloseInspObservationPapfEntity
import com.soaint.domain.model.DinamicQuerysPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCloseInspectionBinding
import com.soaint.sivicos_dinamico.extensions.selectValue
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceActivity
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.OnVisitViewModelEvent
import com.soaint.sivicos_dinamico.flow.visit.item_view.ObservationPapfItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.*
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter


class CloseInspectionFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCloseInspectionBinding>(R.layout.fragment_close_inspection)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: CloseInspectionViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.close_inspection_papf))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        adapter.clear()
        binding.buttonSend.setOnSingleClickListener { viewModel.validateActs() }
        binding.buttonAdd.setOnSingleClickListener { viewModel.validateObservation() }

        binding.includeReprogramar.radiogroup.setOnCheckedChangeListener { radioGroup, checkedId ->
            var checkedId = checkedId
            checkedId = radioGroup.checkedRadioButtonId
            when (checkedId) {
                R.id.yes -> viewModel.setRespondeReq(true)
                R.id.no -> viewModel.setRespondeReq(false)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(CloseInspectionFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.infoTramit.observe(viewLifecycleOwner, viewModel::init)
        viewModel.query.observe(viewLifecycleOwner, ::onViewModelSpinnerReady)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> showSnack(false, R.string.copy_generic_error)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(event.clasificationTramit)
            is OnViewModelEvent.OnViewModelObservationReady -> OnViewModelObservationReady(event.observation)
            is OnViewModelEvent.OnValidateDataImport -> validateErrorDataImport(
                event.mpigId,
            )
            is OnViewModelEvent.OnValidateDataReq -> validateErrorDataReq(
                event.requisitos,
                event.justification,
            )
            is OnViewModelEvent.OnValidateData -> validateErrorData(
                event.firmatId,
            )
            is OnViewModelEvent.OnViewModelSuccess -> {
                showSnack(true, event.message)
                binding.editTextObservation.text = EMPTY_STRING.toEditable()
            }
            is OnViewModelEvent.onViewModelValidateActs -> onViewModelValidateActs(event.acts)
            is OnViewModelEvent.OnValidateObservation -> validateObservationData(event.observation)
            is OnViewModelEvent.validateMaximiumObservations -> validateMaximiumObservations(event.message)
        }
    }

    private fun onViewModelReady(clasificationTramit: ClasificationTramitPapfEntity?) {
        if (clasificationTramit != null) {
            binding.includeReprogramar.checked = clasificationTramit.respuestaRequerimientoIF
            binding.editTextJustification.setText(clasificationTramit.justificacion.orEmpty())
            binding.layoutReprogramar.isVisible = viewModel.visibleReq.value == true
            binding.textInputJustification.isVisible = clasificationTramit.respuestaRequerimientoIF == true
            binding.textInputIdiom.isVisible = clasificationTramit.idActividad == ID_TYPE_IMPORTER
            binding.textInputCertificate.isVisible = clasificationTramit.idActividad == ID_TYPE_IMPORTER
            binding.textViewTitleMpig.isVisible = clasificationTramit.idActividad == ID_TYPE_IMPORTER
            binding.spinnerMpig.isVisible = clasificationTramit.idActividad == ID_TYPE_IMPORTER

            clasificationTramit.usoMPIG?.let {
                viewModel.setItemSelected(it, CD_MPIG)
                binding.spinnerMpig.selectValue(it)
            }
            clasificationTramit.resultadoCIS?.let {
                viewModel.setItemSelected(it, CD_RESULT_CIS)
                binding.spinnerResult.selectValue(it)
            }
            clasificationTramit.firmante?.let {
                viewModel.setItemSelected(it, CD_FIRMATE)
                binding.spinnerFirmat.selectValue(it)
            }
            clasificationTramit.idioma?.let {
                viewModel.setItemSelected(it, CD_IDIOM)
                binding.spinnerIdiom.selectValue(it)
            }
        }
    }

    private fun onViewModelSpinnerReady(query: List<DinamicQuerysPapfEntity>) {
        val idTipoTramite = mainViewModel.infoTramit.value?.idTipoTramite.orZero().toString()
        loadSpinner(query, CD_MPIG, binding.spinnerMpig)
        loadSpinner(query, CD_RESULT_CIS, binding.spinnerResult)
        loadSpinner(query, CD_FIRMATE, binding.spinnerFirmat)
        loadSpinner(
            query.filter { it.idTipoTramite == idTipoTramite },
            CD_IDIOM,
            binding.spinnerIdiom
        )
    }

    private fun loadSpinner(
        codeQuery: List<DinamicQuerysPapfEntity>,
        query: String,
        spinner: Spinner
    ) {
        val array = codeQuery.filter { it.codeQuery == query }.toMutableList()
        array.add(0, DinamicQuerysPapfEntity(descripcion = getString(R.string.copy_option)))
        val items = array.mapNotNull { it.descripcion }

        spinner.adapter =
            ArrayAdapter(requireContext(), android.R.layout.simple_list_item_1, items)
        spinner.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View?,
                position: Int,
                id: Long
            ) {
                val itemSelected = items[position]
                viewModel.setItemSelected(itemSelected, query)
            }

            override fun onNothingSelected(parent: AdapterView<*>) {}
        }
    }

    private fun OnViewModelObservationReady(observation: List<CloseInspObservationPapfEntity>?) {
        val items = mutableListOf<Group>()
        if (!observation.isNullOrEmpty()) {
            items.addAll(observation.map { ObservationPapfItemView(it, ::onEdit, ::onDelete) })
        }
        adapter.addAll(items)
        val isVisible = observation.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        adapter.updateAsync(items)
        binding.editTextObservation.text = EMPTY_STRING.toEditable()
        binding.buttonAdd.text = getString(R.string.copy_add)
    }

    private fun onDelete(item: CloseInspObservationPapfEntity) {
        viewModel.deleteObservation(item)
    }

    private fun onEdit(item: CloseInspObservationPapfEntity) {
        viewModel.setObservation(item)
        binding.editTextObservation.text = item.descripcion.orEmpty().toEditable()
        binding.buttonAdd.text = getString(R.string.copy_notification_save)
    }

    fun onViewModelValidateActs(acts: List<String>) {
        if (!acts.isNullOrEmpty()) {
            val data = acts.toString().replace("[", "").replace("]", "")
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_close_inspection_error, data),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                )
                {
                    findNavController().popBackStack()
                    startActivity(DiligenceActivity.getStartActivityEvent(viewModel.infoTramitData.value?.idSolicitud))
                }
            )
        } else {
            viewModel.checkData()
        }
    }
    private fun validateMaximiumObservations(@StringRes message: Int){
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(message),
                titleBtnPositive = getString(R.string.cerrar)
            )
            {}
        )
        binding.editTextObservation.text = EMPTY_STRING.toEditable()
    }

    private fun validateErrorDataImport(
        mpig: Int?,
    ) {
        validateSpinner(binding.spinnerMpig, mpig)
    }

    private fun validateErrorDataReq(
        requisitos: Boolean?,
        justification: String?
    ) {
        validateRadioGroup(binding.includeReprogramar.radiogroup, requisitos)
        validateField(binding.textInputJustification, justification)
    }

    private fun validateErrorData(
        firmatId: Int?,
    ) {
        validateSpinner(binding.spinnerFirmat, firmatId)
    }

    private fun validateObservationData(
        observation: String?,
    ) {
        validateField(binding.textInputObservation, observation)
    }
}