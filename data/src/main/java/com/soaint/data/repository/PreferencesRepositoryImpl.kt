package com.soaint.data.repository

import android.content.SharedPreferences
import com.google.gson.Gson
import com.soaint.data.di.SHARED_PREFERENCES
import com.soaint.domain.repository.PreferencesRepository
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

@Singleton
class PreferencesRepositoryImpl @Inject constructor(
    private val gson: Gson,
    @Named(SHARED_PREFERENCES) private val sharedPreferences: SharedPreferences,
) : PreferencesRepository {

    override fun setString(preferenceKey: String, value: String) {
        val editor = sharedPreferences.edit()
        editor.putString(preferenceKey, value)
        editor.commit()
    }

    override fun getString(preferenceKey: String, defaultValue: String): String? {
        return sharedPreferences.getString(preferenceKey, defaultValue)
    }

    override fun <T> getObject(preferenceKey: String, classOfT: Class<T>): T? {
        return try {
            gson.fromJson(getString(preferenceKey), classOfT)
        } catch (e: Exception) {
            null
        }
    }

    override fun setObject(preferenceKey: String, data: Any) {
        try {
            val editor = sharedPreferences.edit()
            editor.putString(preferenceKey, gson.toJson(data))
            editor.commit()
        } catch (e: Exception) {
        }
    }
}