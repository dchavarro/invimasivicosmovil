package com.soaint.sivicos_dinamico.flow.visit.views.close_visit.interaction

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.orZero
import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.model.*
import com.soaint.domain.use_case.ManageMssUseCase
import com.soaint.domain.use_case.TechnicalUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.extensions.toEditable
import com.soaint.sivicos_dinamico.flow.visit.views.manageMss.nso.onMssNsoViewModelEvent
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed interface ValidateInteraction {

    val isVisible: Boolean
    val isValid: Boolean
    val enableValidation: Boolean

    data class OnValidateResponsabilidadSanitaria(
        val responsabilidadSanitaria: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateEmitirCertificado(
        val emitirCertificado: Boolean?,
        val isChecked: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateTomaMuestras(
        val tomaMuestra: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateRequiereRetiro(
        val requiereRetiro: Boolean?,
        val isChecked: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateSometimiento(
        val sometimiento: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateReCall(
        val recall: String?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidatePerdidaCertificacion(
        val perdidaCertificacion: Boolean?,
        val isChecked: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateProductoTenenciaInvima(
        val productoTenenciaInvima: Boolean?,
        val isChecked: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateProductoComercializado(
        val productoComercializado: Boolean?,
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction

    data class OnValidateSearch(
        override val isVisible: Boolean,
        override val isValid: Boolean,
        override val enableValidation: Boolean,
    ) : ValidateInteraction
}

sealed class onInteractionViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : onInteractionViewModelEvent()
    data class onLoadingError(val error: Throwable?) : onInteractionViewModelEvent()
    data class OnViewModelReady(val interaction: InteractionEntity?) : onInteractionViewModelEvent()
    data class onViewModelReadyProduct(val interactionProduct: List<CertificateEntity>?) :
        onInteractionViewModelEvent()

    data class OnViewModelSuccess(@StringRes val message: Int, val finished: Boolean = false) : onInteractionViewModelEvent()
    data class onSnackLoadingError(@StringRes val message: Int) : onInteractionViewModelEvent()
    data class onViewModelReadySearch(val search: String) : onInteractionViewModelEvent()
    object onViewModelInsertRs : onInteractionViewModelEvent()
    data class onViewModelReadyRs(val rs: List<RsEntity>?) : onInteractionViewModelEvent()

    data class onValidateEvent(val validateData: ValidateInteraction) :
        onInteractionViewModelEvent()

}

class InteractionViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
    private val technicalUseCase: TechnicalUseCase,
    private val manageMssUseCase: ManageMssUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<onInteractionViewModelEvent>()
    val event = _event.asLiveData()


    private val _visit = MutableLiveData<VisitEntity>()
    val visit = _visit.asLiveData()

    private val _interaction = MutableLiveData<InteractionEntity>(null)
    val interaction = _interaction.asLiveData()

    private val _emitirCertificacion = MutableLiveData<Boolean>()
    val emitirCertificacion = _emitirCertificacion.asLiveData()

    private val _responsabilidadSanitaria = MutableLiveData<Boolean>()
    val responsabilidadSanitaria = _responsabilidadSanitaria.asLiveData()

    private val _tomaMuestras = MutableLiveData<Boolean?>(null)
    val tomaMuestras = _tomaMuestras.asLiveData()

    private val _requiereRetiroProducto = MutableLiveData<Boolean?>(null)
    val requiereRetiroProducto = _requiereRetiroProducto.asLiveData()

    private val _realizoSometimiento = MutableLiveData<Boolean?>(null)
    val realizoSometimiento = _realizoSometimiento.asLiveData()

    private val _identificadorRecall = MutableLiveData<String?>(null)
    val identificadorRecall = _identificadorRecall.asLiveData()

    private val _perdidaCertificacion = MutableLiveData<Boolean>()
    val perdidaCertificacion = _perdidaCertificacion.asLiveData()

    private val _productoTenenciaInvima = MutableLiveData<Boolean>()
    val productoTenenciaInvima = _productoTenenciaInvima.asLiveData()

    private val _productoComercializado = MutableLiveData<Boolean?>(null)
    val productoComercializado = _productoComercializado.asLiveData()

    private val _mss = MutableLiveData<List<ManageAmssEntity>>(emptyList())
    val mss = _mss.asLiveData()

    private val _group = MutableLiveData<List<VisitGroupEntity>>(emptyList())
    val group = _group.asLiveData()

    private var nombreProducto: String? = EMPTY_STRING

    private var registroSanitario: String? = EMPTY_STRING

    private var productInteraction: List<CertificateEntity> = emptyList()

    val user: UserInformationEntity? get() = userUseCase.getUserInformation()

    fun init(visitData: List<VisitEntity>) = execute {
        _visit.value = visitData.firstOrNull()
        onLoadMss()
        onLoadInteraction()
        onLoadInteractionProduct()
        onGetRs()
    }

    private suspend fun onLoadMss() {
        _mss.value =
            manageMssUseCase.loadMssAppliedLocal(_visit.value?.id.orZero().toString()).getOrNull()
                ?.filter { it.tipoMs == MSSP || it.tipoMs == MSSE }

        _group.value = visitUseCase.loadGroupLocal(_visit.value?.id.toString()).getOrDefault(
            emptyList()
        )
    }

    private suspend fun onLoadInteraction() {
        val result = visitUseCase.loadInteractionLocal(_visit.value?.id?.orZero().toString())
        _event.value = if (result.isSuccess) {
            _interaction.value = result.getOrNull()
            onInteractionViewModelEvent.OnViewModelReady(result.getOrNull())
        } else {
            onInteractionViewModelEvent.onLoadingError(null)
        }
        visibility()
    }

    private fun onLoadInteractionProduct() = execute {
        val result = technicalUseCase.getCertificateLocal(
            _visit.value?.empresa?.numDocumento.orEmpty().toString()
        )
        _event.value = if (result.isSuccess) {
            productInteraction = result.getOrNull().orEmpty()
            onInteractionViewModelEvent.onViewModelReadyProduct(result.getOrNull())
        } else {
            onInteractionViewModelEvent.onLoadingError(null)
        }
    }

    fun setItemSelected(idCertificado: Int, isSelected: Boolean) {
        productInteraction.find { it.idCertificado == idCertificado }?.let {
            it.isSelected = isSelected
            updateProductInteraction(it)
        }
    }

    fun setEmitirCertificacion(data: Boolean?) {
        _emitirCertificacion.value = data
    }

    fun setResponsabilidadSanitaria(data: Boolean?) {
        _responsabilidadSanitaria.value = data
    }

    fun setTomaMuestras(data: Boolean?, refresh: Boolean = false) {
        _tomaMuestras.value = data
        if (refresh) visibility()
    }

    fun setRetiroProducto(data: Boolean?, refresh: Boolean = false) {
        _requiereRetiroProducto.value = data
        if (refresh) visibility()
    }

    fun setSometimiento(data: Boolean?, refresh: Boolean = false) {
        _realizoSometimiento.value = data
        if (refresh) visibility()
    }

    fun setReCall(data: Editable?) {
        _identificadorRecall.value = data.toString()
    }

    fun setPerdida(data: Boolean?) {
        _perdidaCertificacion.value = data
    }

    fun setTenencia(data: Boolean?) {
        _productoTenenciaInvima.value = data
    }

    fun setComercializado(data: Boolean?, refresh: Boolean = false) {
        _productoComercializado.value = data
        if (refresh) visibility()
    }

    fun checkData() {
        val isValid = visibility(true)
        if (isValid) {
            updateInteraction()
        }
    }

    fun updateInteraction() = execute {
        val body = InteractionEntity(
            emitirCertificacion = _emitirCertificacion.value,
            responsabilidadSanitaria = _responsabilidadSanitaria.value,
            tomaMuestras = _tomaMuestras.value,
            requiereRetiroProducto = _requiereRetiroProducto.value,
            realizoSometimiento = _realizoSometimiento.value,
            identificadorRecall = _identificadorRecall.value,
            perdidaCertificacion = _perdidaCertificacion.value,
            productoTenenciaInvima = _productoTenenciaInvima.value,
            productoComercializado = _productoComercializado.value,
            idVisita = _visit.value?.id,
            activo = true,
            usuarioCrea = userUseCase.getUserInformation()?.usuario.toString(),
        )
        val result = body.let { visitUseCase.updateInteractionLocal(it) }
        _event.value = if (result.isSuccess) {
            onInteractionViewModelEvent.OnViewModelSuccess(R.string.copy_update_success, true)
        } else {
            onInteractionViewModelEvent.onLoadingError(null)
        }
    }

    fun updateProductInteraction(it: CertificateEntity) = execute {
        val result = technicalUseCase.updateCertificateLocal(it)
        _event.value = if (result.isSuccess) {
            onInteractionViewModelEvent.OnViewModelSuccess(R.string.copy_update_success)
        } else {
            onInteractionViewModelEvent.onLoadingError(null)
        }
    }


    fun onGetRs() = execute {
        val rs = manageMssUseCase.loadRsLocal(_visit.value?.id.orZero())
        val resultMssp = rs.getOrNull()
        _event.value = onInteractionViewModelEvent.onViewModelReadyRs(resultMssp)
    }

    fun onSearch(search: String) = execute {
        val result = manageMssUseCase.searchRegisterSanitary(search)
        _event.value = when (result.exceptionOrNull()) {
            is WithoutConnectionException -> onInteractionViewModelEvent.onSnackLoadingError(R.string.no_internet)
            else -> onInteractionViewModelEvent.onViewModelReadySearch(result.getOrNull().orEmpty())
        }
    }

    fun setSearchSelected(nombreProducto: String, registroSanitario: String) {
        this.nombreProducto = nombreProducto
        this.registroSanitario = registroSanitario
    }

    fun onInsert() = execute {
        if (!nombreProducto.isNullOrEmpty()) {
            val rs = manageMssUseCase.insertRsLocal(
                _visit.value?.id.orZero(),
                nombreProducto.orEmpty(),
                registroSanitario.orEmpty()
            )
            if (rs.isSuccess) {
                _event.value = onInteractionViewModelEvent.onViewModelInsertRs
            } else {
                onMssNsoViewModelEvent.onLoadingError(null)
            }
        } else {
            _event.value = onInteractionViewModelEvent.onSnackLoadingError(R.string.copy_option)
        }
        onGetRs()
    }

    fun visibility(
        disableValidation: Boolean = false
    ): Boolean {
        val mss = _mss.value

        //emitir Certificado
        val emitirCertificado = _emitirCertificacion.value
        val isEmitirCertificadoVisible = _visit.value?.codigoRazon == REASON_RAVI_AYCVC ||
                _visit.value?.codigoRazon == REASON_RAVI_AYCVVR ||
                _visit.value?.codigoRazon == REASON_RAVI_AYCVVRMSS
        val isEmitirCertificadoChecked =
            if (_visit.value?.idAccionSeguir == ID_ACTION_ACSE_CEVI) true else false
        val isEmitirCertificadoValid = !disableValidation || emitirCertificado != null
        val typeEmitirCertificado =
            ValidateInteraction.OnValidateEmitirCertificado(
                emitirCertificado,
                isEmitirCertificadoChecked,
                isEmitirCertificadoVisible,
                isEmitirCertificadoValid,
                disableValidation
            )
        setEmitirCertificacion(isEmitirCertificadoChecked)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeEmitirCertificado)

        //Responsabilidad sanitaria
        val responsabilidadSanitaria = _responsabilidadSanitaria.value
        val isResponsabilidadVisible =
            _visit.value?.aplicaMedidaSanitaria == true || _visit.value?.idAccionSeguir == ID_ACTION_ACSE_CEVI
        val isResponsabilidadValid = !disableValidation || responsabilidadSanitaria != null
        val typeResponsabilidad =
            ValidateInteraction.OnValidateResponsabilidadSanitaria(
                responsabilidadSanitaria,
                isResponsabilidadVisible,
                isResponsabilidadValid,
                disableValidation
            )
        setResponsabilidadSanitaria(responsabilidadSanitaria ?: isResponsabilidadVisible)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeResponsabilidad)

        //Toma Muestras
        val tomaMuestra = _tomaMuestras.value
        val isTomaMuestrasVisible = _visit.value?.idGrupoTrabajo != 22
        val isTomaMuestrasValid = !disableValidation || tomaMuestra != null
        val typeTomaMuestras =
            ValidateInteraction.OnValidateTomaMuestras(
                tomaMuestra,
                isTomaMuestrasVisible,
                isTomaMuestrasValid,
                disableValidation
            )
        setTomaMuestras(tomaMuestra)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeTomaMuestras)

        //Requiere retiro
        val requiereRetiro = _requiereRetiroProducto.value
        val validateMssRetiro = if (mss?.find { it.codigo == ID_CODE_MSS_RETIRO } == null) null else true
        val isRequiereRetiroVisible = _visit.value?.codigoRazon != REASON_RAVI_INPER
        val isRequiereRetiroChecked = if (validateMssRetiro != null) {
            _visit.value?.idTipoProducto == ID_MISIONAL_COSMETICOS && validateMssRetiro
        } else null

        val isRequiereRetiroValid = !disableValidation || requiereRetiro != null
        val typeRequiereRetiro =
            ValidateInteraction.OnValidateRequiereRetiro(
                requiereRetiro,
                isRequiereRetiroChecked,
                isRequiereRetiroVisible,
                isRequiereRetiroValid,
                disableValidation
            )
        setRetiroProducto(requiereRetiro ?: isRequiereRetiroChecked)
        if ((requiereRetiro ?: isRequiereRetiroChecked) != true) {
            setSometimiento(null)
            setReCall(EMPTY_STRING.toEditable())
        }
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeRequiereRetiro)

        //Sometimiento
        val sometimiento = _realizoSometimiento.value
        val isSometimientoVisible =
            _visit.value?.codigoRazon != REASON_RAVI_INPER && _visit.value?.idTipoProducto == ID_MISIONAL_DISPOSITIVOS
                    && requiereRetiro == true
        val isSometimientoValid = !disableValidation || sometimiento != null
        val typeSometimiento =
            ValidateInteraction.OnValidateSometimiento(
                sometimiento,
                isSometimientoVisible,
                isSometimientoValid,
                disableValidation
            )
        setSometimiento(sometimiento)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeSometimiento)

        //idRecall
        val reCall = _identificadorRecall.value
        val isReCallVisible =
            _visit.value?.codigoRazon != REASON_RAVI_INPER && _visit.value?.idTipoProducto == ID_MISIONAL_DISPOSITIVOS
                    && sometimiento == true
        val isReCallValid = !disableValidation
        val typeReCall =
            ValidateInteraction.OnValidateReCall(
                reCall,
                isReCallVisible,
                isReCallValid,
                disableValidation
            )
        setReCall(reCall?.toEditable())
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeReCall)

        //Perdida certificacion
        val perdidaCertificacion = _perdidaCertificacion.value
        val validateReason =
            _visit.value?.codigoRazon == REASON_RAVI_IVC || _visit.value?.codigoRazon == REASON_RAVI_IVCPM || _visit.value?.codigoRazon == REASON_RAVI_IVCAJA ||
                    _visit.value?.codigoRazon == REASON_RAVI_IVCBS || _visit.value?.codigoRazon == REASON_RAVI_AYCVC || _visit.value?.codigoRazon == REASON_RAVI_AYCVS ||
                    _visit.value?.codigoRazon == REASON_RAVI_AYCVVR || _visit.value?.codigoRazon == REASON_RAVI_AYCVVRMSS || _visit.value?.codigoRazon == REASON_RAVI_IVCTM
        val isPerdidaCertificacionVisible =
            _visit.value?.idGrupoTrabajo != 22 && _visit.value?.codigoRazon != REASON_RAVI_INPER && validateReason
        val isPerdidaCertificacionChecked = if (mss?.find {
                it.codigo == ID_CODE_MSS_CLAUSURA_TEMPORAL_TOTAL_ESTABLECIMIENTO ||
                        it.codigo == ID_CODE_MSS_CLAUSURA_TEMPORAL_PARCIAL_ESTABLECIMIENTO || it.codigo == ID_CODE_MSS_SUSPENSION_TOTAL_SERVICIO ||
                        it.codigo == ID_CODE_MSS_SUSPENSION_PARCIAL_SERVICIO || it.codigo == ID_CODE_MSS_SUSPENSION_TOTAL_TRABAJO ||
                        it.codigo == ID_CODE_MSS_SUSPENSION_PARCIAL_TRABAJO || it.codigo == ID_CODE_MSS_SUSPENSION_TEMPORAL_TOTAL_ESTABLECIMIENTO ||
                        it.codigo == ID_CODE_MSS_SUSPENSION_TEMPORAL_PARCIAL_ESTABLECIMIENTO || it.codigo == ID_CODE_MSS_CIERRE_DEFINITIVO_TOTAL_ESTABLECIMIENTO ||
                        it.codigo == ID_CODE_MSS_RETIRO || it.codigo == ID_CODE_MSS_CIERRE_DEFINITIVO_PARCIAL_ESTABLECIMIENTO ||
                        it.codigo == ID_CODE_MSS_SUSPENSION_TOTAL_FABRICACION || it.codigo == ID_CODE_MSS_SUSPENSION_TEMPORAL_PARCIAL_FABRICACION ||
                        it.codigo == ID_CODE_MSS_CIERRE_TEMPORAL_ESTABLECIMIENTO
            } == null) false else true
        val isPerdidaCertificacionValid = !disableValidation || perdidaCertificacion != null
        val typePerdidaCertificacion =
            ValidateInteraction.OnValidatePerdidaCertificacion(
                perdidaCertificacion,
                isPerdidaCertificacionChecked,
                isPerdidaCertificacionVisible,
                isPerdidaCertificacionValid,
                disableValidation
            )
        setPerdida(perdidaCertificacion ?: isPerdidaCertificacionChecked)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typePerdidaCertificacion)

        //Producto tenencia invima
        val productoTenenciaInvima = _productoTenenciaInvima.value
        val codevalidate =
            if (mss?.find { it.codigo == ID_CODE_MSS_DECOMISO } == null) false else true
        val isProductoTenenciaInvimaVisible =
            _visit.value?.codigoRazon != REASON_RAVI_INPER && codevalidate
        val isProductoTenenciaInvimaValid = !disableValidation || productoTenenciaInvima != null
        val typeProductoTenenciaInvima =
            ValidateInteraction.OnValidateProductoTenenciaInvima(
                productoTenenciaInvima,
                isProductoTenenciaInvimaVisible,
                isProductoTenenciaInvimaVisible,
                isProductoTenenciaInvimaValid,
                disableValidation
            )
        setTenencia(isProductoTenenciaInvimaVisible)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeProductoTenenciaInvima)

        //Producto Comercializado
        val productoComercializado = _productoComercializado.value
        val validaGroup = if (_group.value?.find { it.id == 4 } == null) false else true
        val isProductoComercializadoVisible =
            _visit.value?.idGrupoTrabajo != 22 && _visit.value?.codigoRazon != REASON_RAVI_INPER
                    && _visit.value?.idTipoProducto == ID_MISIONAL_DISPOSITIVOS && validaGroup
        val isProductoComercializadoValid = !disableValidation || productoComercializado != null
        val typeProductoComercializado =
            ValidateInteraction.OnValidateProductoComercializado(
                productoComercializado,
                isProductoComercializadoVisible,
                isProductoComercializadoValid,
                disableValidation
            )
        setComercializado(productoComercializado)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeProductoComercializado)

        //Numero rs/ebc
        val isSearchVisible =
            _visit.value?.idGrupoTrabajo != 22 && _visit.value?.codigoRazon != REASON_RAVI_INPER
                    && productoComercializado == false
        val typeSearch =
            ValidateInteraction.OnValidateSearch(isSearchVisible, true, disableValidation)
        _event.value = onInteractionViewModelEvent.onValidateEvent(typeSearch)

        return (!isEmitirCertificadoVisible || isEmitirCertificadoValid) && (!isResponsabilidadVisible || isResponsabilidadValid)
                && (!isTomaMuestrasVisible || isTomaMuestrasValid) && (!isRequiereRetiroVisible || isRequiereRetiroValid)
                && (!isSometimientoVisible || isSometimientoValid) && (!isPerdidaCertificacionVisible || isPerdidaCertificacionValid)
                && (!isProductoTenenciaInvimaVisible || isProductoTenenciaInvimaValid)
                && (!isProductoComercializadoVisible || isProductoComercializadoValid)
    }
}