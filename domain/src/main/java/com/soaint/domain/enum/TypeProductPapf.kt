package com.soaint.domain.enum

import com.soaint.sivicos_dinamico.utils.*

enum class TypeProductPapf(val id: Array<Int>) {
    TYPEPRODUCTPAPF(
        arrayOf(
            ID_MISIONAL_PAPF_ALIMENTOS,
            ID_MISIONAL_PAPF_BEBIDAS,
        )
    )
}