package com.soaint.sivicos_dinamico.flow.visit.views.info_visit

import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*
import com.soaint.domain.use_case.VisitUseCase
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

sealed class OnVisitInfoViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnVisitInfoViewModelEvent()
    data class OnViewModelReady(
        val visit: List<VisitEntity>,
        val antecedent: List<AntecedentEntity>,
        val sample: List<SampleEntity>,
        val official: List<VisitOfficialEntity>,
        val group: List<VisitGroupEntity>,
        val subgroup: List<VisitGroupEntity>,
        val schedule: List<ScheduleTempEntity>?,
        val sampleAnalysis: List<SampleAnalysisEntity>,
        val samplePlan: List<SamplePlanEntity>,
        ): OnVisitInfoViewModelEvent()
    data class OnLoadingError(val error: Throwable?) : OnVisitInfoViewModelEvent()
}

class VisitInfoViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnVisitInfoViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    fun init(visitData: List<VisitEntity>) = execute {
        val visit = visitData.firstOrNull()
        _event.value = if (visit != null ) {
            val idVisit = visitData.map { it.id }
            val resultAntecedent = idVisit.map { visitUseCase.loadAntecedentLocal(it.toString()) }
            val resultSchedule = visitUseCase.loadScheduleLocal(visit.tramite?.idTramite.orEmpty()).getOrNull()?.orEmpty()
            val resultSample = idVisit.map { visitUseCase.loadSamplesLocal(it.toString()) }
            val resultOfficial = idVisit.map { visitUseCase.loadOfficialLocal(it.toString()) }
            val resultGroup = idVisit.map { visitUseCase.loadGroupLocal(it.toString()) }
            val resultSubGroup = idVisit.map { visitUseCase.loadSubGroupLocal(it.toString()) }
            val antecedent = resultAntecedent.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
            val sample = resultSample.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()

            val resultSampleAnalysis = sample.map { visitUseCase.loadSamplesAnalysisLocal(it.id.toString()) }
            val sampleAnalysis = resultSampleAnalysis.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()

            val resultSamplePlan = sample.map { visitUseCase.loadSamplesPlanLocal(it.id.toString()) }
            val samplePlan = resultSamplePlan.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()

            val official = resultOfficial.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
            val group = resultGroup.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
            val subgroup = resultSubGroup.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.flatten()
            OnVisitInfoViewModelEvent.OnViewModelReady(visitData, antecedent , sample, official, group, subgroup, resultSchedule, sampleAnalysis, samplePlan)
        }
        else {
            OnVisitInfoViewModelEvent.OnLoadingError(null)
        }
    }
}
