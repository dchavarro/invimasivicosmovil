package com.soaint.sivicos_dinamico.flow.visit

import android.text.Editable
import android.util.Log
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.common.orZero
import com.soaint.domain.common.WithoutConnectionException
import com.soaint.domain.common.getDateHourWithOutFormat
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.model.VisitGroupEntity
import com.soaint.domain.use_case.*
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class OnVisitViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnVisitViewModelEvent()
    data class OnViewModelReady(val visits: List<VisitEntity>) : OnVisitViewModelEvent()
    data class OnLoadingError(val error: String) : OnVisitViewModelEvent()
    data class onViewModelReadyCheck(val acts: List<String>) : OnVisitViewModelEvent()
    data class onViewModelValidateDecomiso(val acts: List<String>) : OnVisitViewModelEvent()
    data class onViewModelReadyLegal(val isValid: Boolean?) : OnVisitViewModelEvent()
    object onViewModelReadyClose : OnVisitViewModelEvent()
    object onViewModelAtLeastOneActs : OnVisitViewModelEvent()
    object OnErrorAct015 : OnVisitViewModelEvent()
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) :
        OnVisitViewModelEvent()
}

class VisitViewModel @Inject constructor(
    private val visitUseCase: VisitUseCase,
    private val userUseCase: UserUseCase,
    private val syncDataUseCase: SyncDataUseCase,
    private val validationUseCase: ValidationUseCase,
    private val taskUseCase: TaskUseCase,
    private val manageMssUseCase: ManageMssUseCase,
    private val papfUseCase: PapfUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnVisitViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _infoTramitData = MutableLiveData<InfoTramitePapfEntity>()
    val infoTramitData = _infoTramitData.asLiveData()

    private val _visit = MutableLiveData<VisitEntity>()
    val visit = _visit.asLiveData()

    private val _group = MutableLiveData<List<VisitGroupEntity>>()
    val group = _group.asLiveData()

    private val _count = MutableLiveData<Int>()
    val count = _count.asLiveData()

    private val _observation = MutableLiveData(EMPTY_STRING)
    val observation get() = _observation.asLiveData()

    private val _isVisibleTransport = MutableLiveData<Boolean>()
    val isVisibleTransport = _isVisibleTransport.asLiveData()

    private val _isDecomiso = MutableLiveData<Boolean>()
    val isDecomiso = _isDecomiso.asLiveData()

    fun init(id: Int) {
        loadVisit(id)
    }

    fun loadVisit(taskId: Int) = execute {
        _event.value =
            OnVisitViewModelEvent.OnChangeLabelPreloader(R.string.cargando_visit)
        _visitData.value = visitUseCase.loadVisitLocal(taskId).getOrNull().orEmpty()
        _event.value = if (!_visitData.value.isNullOrEmpty()) {
            _visit.value = _visitData.value?.firstOrNull()
            countQuerys()
            onGetAmsspSelect()
            OnVisitViewModelEvent.OnViewModelReady(_visitData.value!!)
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun stopLocation() {
        userUseCase.logout()
    }

    fun logout() {
        userUseCase.logout()
    }

    fun countQuerys() = execute {
        _count.value = syncDataUseCase.syncPending()
    }

    fun onObservationChange(data: Editable?) {
        _observation.value = data.toString()
    }

    fun onObservation(observation: String) = execute {
        val result = validationUseCase.addObservation(
            _visitData.value?.firstOrNull()?.id.toString(),
            observation,
            _visitData.value?.firstOrNull()?.idTipoProducto.orZero(),
        )
        _event.value = when (result.exceptionOrNull()) {
            is WithoutConnectionException -> OnVisitViewModelEvent.onSnack(
                false,
                R.string.no_internet
            )
            is Exception -> OnVisitViewModelEvent.onSnack(false, R.string.error)
            else -> OnVisitViewModelEvent.onSnack(true, R.string.copy_update_success)
        }
    }

    fun validateMssp() = execute {
        val result =
            validationUseCase.validationMss(_visitData.value?.firstOrNull()?.id.toString(), MSSP)
        if (result.isSuccess) {
            validateMsse(result.getOrNull().orEmpty())
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validateMsse(list: List<String>) = execute {
        val result =
            validationUseCase.validationMss(_visitData.value?.firstOrNull()?.id.toString(), MSSE)
        if (result.isSuccess) {
            val data = result.map {
                it.onEach { d ->
                    list.plus(d)
                }
                list
            }.getOrNull().orEmpty()

            if (_visitData.value?.firstOrNull()?.aplicaMedidaSanitaria == true) {
                validationUpdateMssList(data.distinct())
            } else {
                _event.value = OnVisitViewModelEvent.onViewModelReadyCheck(emptyList())
            }

        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validationUpdateMssList(list: List<String>) = execute {
        val result = validationUseCase.validationUpdateMssList(
            _visitData.value?.firstOrNull()?.id.toString(),
            MSSP
        )
        if (result.isSuccess) {
            val data = result.map {
                it.onEach { d ->
                    list.plus(d)
                }
                list
            }.getOrNull().orEmpty()
            _event.value = OnVisitViewModelEvent.onViewModelReadyCheck(data.distinct())
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validation() = execute {
        if (_visit.value?.codigoRazon == REASON_RAVI_INPER) validateActO15()
        else validationLegalAtiende()
    }

    fun validationWhenDecomiso() = execute {
        if (isDecomiso.value == true) {
            val result = validationUseCase.validationActs(
                _visitData.value?.firstOrNull()?.id ?: 0,
                listOf(IVC_INS_FM003, IVC_VIG_FM043, IVC_INS_FM054)
            )
            _event.value = OnVisitViewModelEvent.onViewModelValidateDecomiso(
                result.getOrNull().orEmpty().distinct()
            )
        } else {
            validation()
        }
    }

    fun validateAtLeastOneActs() = execute {
        val visit = _visitData.value?.firstOrNull()
        val result =
            validationUseCase.validateAtLeastOneActs(_visitData.value?.firstOrNull()?.id ?: 0)
        //si el resultado de la visita es Ejecutada - Reprogramada - No competencia del Invima  haber un acta firmada
        if (visit?.idResultado.toString() !in setOf("11", "13", "14") || result > 0) {
            updateTask()
        } else {
            _event.value = OnVisitViewModelEvent.onViewModelAtLeastOneActs
        }
    }

    fun validationLegalAtiende() = execute {
        val result =
            validationUseCase.validationLegalAtiende(_visitData.value?.firstOrNull()?.id.toString())
        _event.value =
            if (result.isSuccess) OnVisitViewModelEvent.onViewModelReadyLegal(result.getOrNull())
            else OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
    }

    fun validateActO15() = execute {
        val result =
            validationUseCase.validationAct015(_visitData.value?.firstOrNull()?.id.toString())
        if (result.isSuccess) {
            if (result.getOrDefault(false) == true) {
                validationLegalAtiende()
            } else {
                _event.value = OnVisitViewModelEvent.OnErrorAct015
            }
        } else {
            _event.value = OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun validateCloseVisit() = execute {
        if (visit.value?.aplicaMedidaSanitaria == true) {
            //TODO: contador desde amssp_check_table
            if (manageMssUseCase.countMssLocal(visit.value?.id.orEmpty().toString()).getOrNull().orEmpty() > 0) {
                validateFinish()
            } else {
                _event.value =
                    OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_mss)
            }
        } else {
            validateFinish()
        }
    }

    fun loadGroup() = execute {
        _group.value = visitUseCase.loadGroupLocal(visit.value?.id.toString()).getOrNull()
    }


    fun validateConcept(): Boolean {
        loadGroup()
        return if (visit.value?.tramite == null)
            if (!_group.value.isNullOrEmpty()) _group.value!!.any { it.id == ID_GROUP_PLANTAS_ANIMAL }
            else false
        else true
    }

    fun validateFinish() {
        val visit = _visitData.value?.firstOrNull()
        // TODO aqui van campos obligaorios para cerrar visita
        // si aplica campo idConceptoSanitario
        if (visit?.idAccionSeguir == null) {
            _event.value =
                OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_action)
        } else if (visit.idResultado == null) {
            _event.value =
                OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_result)
        } else if (visit.requiereReprogramarVisita == null) {
            _event.value =
                OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_reprograma)
        } else if (validateConcept() == true && visit.idConceptoSanitario == null) {
            _event.value =
                OnVisitViewModelEvent.onSnack(false, R.string.copy_close_visit_error_concept)
        } else {
            validateAtLeastOneActs()
        }
    }

    fun updateTask() = execute {
        val result = taskUseCase.loadTaskLocal(_visitData.value?.firstOrNull()?.idTarea.orEmpty())
        val body = result.getOrNull()?.copy(
            activo = false,
            usuarioModifica = userUseCase.getUserName()
        )
        if (result.isSuccess) {
            val update = taskUseCase.updateTaskLocal(body)
            if (update.isSuccess) updateVisit()
        } else {
            _event.value = OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun updateVisit() = execute {
        val body = _visitData.value?.firstOrNull()?.copy(
            fechaRealizo = getDateHourWithOutFormat(),
            fechaModifica = getDateHourWithOutFormat(),
        )
        val result = body?.let { visitUseCase.updateVisitLocal(it, true) }
        _event.value = if (result?.isSuccess == true) {
            OnVisitViewModelEvent.onViewModelReadyClose
        } else {
            OnVisitViewModelEvent.OnLoadingError(EMPTY_STRING)
        }
    }

    fun refreshVisit() = execute {
        val result =
            _visit.value?.idTarea?.let { visitUseCase.loadVisitLocal(it).getOrNull().orEmpty() }
        if (!result.isNullOrEmpty()) {
            _visitData.value = result
            _visit.value = result.firstOrNull()
        }
    }

    fun onGetAmsspSelect() = execute {
        val amssp_selected = _visitData.value?.firstOrNull()?.id?.toString()
            ?.let { manageMssUseCase.loadMssAppliedLocal(it) }
        val result = amssp_selected?.getOrNull()
            ?.filter { it.codigo == ID_CODE_MSS_DECOMISO && it.tipoMs == MSSP && it.isSelected == true }
        val actsObligatory = amssp_selected?.getOrNull() ?.filter { it.codigo == ID_CODE_MSS_DECOMISO && it.isSelected == true }
        _isVisibleTransport.value = result.isNullOrEmpty()
        _isDecomiso.value = !actsObligatory.isNullOrEmpty()

    }
}