package com.soaint.domain.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class HistoricMssEntity(
    val numeroExpediente: String,
    val razonSocial: String,
    val empresaAsociada: String,
    val direccion: String,
    val municipio: String,
    val medidasSanitarias: List<MssEntity>? = emptyList(),
    val activo: Boolean? = true,
    val usuarioCrea: String,
    val fechaCreacion: String,
    val fechaModifica: String,
) : Parcelable

@Parcelize
data class MssEntity(
    val id: Int?,
    val numeroMSS: String?,
    val numeroVisita: String?,
    val razonVisita: String?,
    val aplicado: String?,
    val idTipoMedidaSanita: Int?,
    val tipoMedidaSanita: String?,
    val producto: String?,
    val idEstadoMedidaSanita: Int?,
    val estadoMSS: String?,
    val fechaCierreVisita: String?,
    val idVisita: Int?,
    val activo: Boolean? = true,
    val usuarioCrea: String?,
    val fechaCreacion: String?,
    val tipoProducto: String?,
) : Parcelable