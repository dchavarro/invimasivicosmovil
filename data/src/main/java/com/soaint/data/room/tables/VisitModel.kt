package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.*

@Entity(tableName = "visit_table")
class VisitModel(
    @PrimaryKey val id: Int,
    @ColumnInfo(name = "activo") val activo: Boolean,
    @ColumnInfo(name = "descClasificacion") val descClasificacion: String,
    @ColumnInfo(name = "descFuenteInformacion") val descFuenteInformacion: String,
    @ColumnInfo(name = "descGrupTrab") val descGrupTrab: String?,
    @ColumnInfo(name = "descPrioridad") val descPrioridad: String,
    @ColumnInfo(name = "descRazon") val descRazon: String,
    @ColumnInfo(name = "descResponRealVisita") val descResponRealVisita: String,
    @ColumnInfo(name = "descTipoProducto") val descTipoProducto: String,
    @ColumnInfo(name = "descripcionMotivo") val descripcionMotivo: String?,
    //val empresa: VisitEmpresaEntity?,
    @ColumnInfo(name = "fecProximaProgramacion") val fecProximaProgramacion: String,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @ColumnInfo(name = "fechaModifica") val fechaModifica: String?,
    @ColumnInfo(name = "fechaRealizo") val fechaRealizo: String?,
    // val formaRealizarVisita: VisitFormaEntity?,
    @ColumnInfo(name = "idClasificacion") val idClasificacion: Int?,
    @ColumnInfo(name = "idDistancia") val idDistancia: Int?,
    @ColumnInfo(name = "idEstado") val idEstado: Int?,
    @ColumnInfo(name = "idFuenteInformacion") val idFuenteInformacion: Int?,
    @ColumnInfo(name = "idGrupoTrabajo") val idGrupoTrabajo: Int?,
    @ColumnInfo(name = "idModEjecutar") val idModEjecutar: Int?,
    @ColumnInfo(name = "idOficinaGTT") val idOficinaGTT: Int?,
    @ColumnInfo(name = "idPosible") val idPosible: Int?,
    @ColumnInfo(name = "idPrioridad") val idPrioridad: Int?,
    @ColumnInfo(name = "idRazon") val idRazon: Int?,
    @ColumnInfo(name = "idResponsableRealizar") val idResponsableRealizar: Int?,
    @ColumnInfo(name = "idResultado") val idResultado: Int?,
    @ColumnInfo(name = "idTarea") val idTarea: Int?,
    @ColumnInfo(name = "idTipoProducto") val idTipoProducto: Int?,
    @ColumnInfo(name = "numeroVisita") val numeroVisita: String,
    @ColumnInfo(name = "objetivo") val objetivo: String,
    @ColumnInfo(name = "observacionResultado") val observacionResultado: String,
    @ColumnInfo(name = "requiAcompa") val requiAcompa: Boolean?,
    //@ColumnInfo(name = "tramite") val tramite: String?,
    @ColumnInfo(name = "usuarioCrea") val usuarioCrea: String?,
    @ColumnInfo(name = "usuarioModifica") val usuarioModifica: String?,
    @ColumnInfo(name = "fechaInicioEjecucion") val fechaInicioEjecucion: String?,
    @ColumnInfo(name = "descDistancia") val descDistancia: String?,
    @ColumnInfo(name = "posibleMotivo") val posibleMotivo: String?,
    @ColumnInfo(name = "modalidadEjecutar") val modalidadEjecutar: String?,
    @ColumnInfo(name = "aplicaMedidaSanitaria") val aplicaMedidaSanitaria: Boolean?,
    @ColumnInfo(name = "legalAtiende") val legalAtiende: Boolean?,
    @ColumnInfo(name = "requiereReprogramarVisita") val requiereReprogramarVisita: Boolean?,
    @ColumnInfo(name = "fechaVerificacionRequerimiento") val fechaVerificacionRequerimiento: String?,
    @ColumnInfo(name = "idConceptoSanitario") val idConceptoSanitario: Int?,
    @ColumnInfo(name = "descripcionConcepto") val descripcionConcepto: String?,
    @ColumnInfo(name = "calificacion") val calificacion: String?,
    @ColumnInfo(name = "fechaCierre") val fechaCierre: String?,
    @ColumnInfo(name = "idAccionSeguir") val idAccionSeguir: Int?,
    @ColumnInfo(name = "codigoRazon") val codigoRazon: String?,
    @ColumnInfo(name = "distancia") val distancia: String?,
    @ColumnInfo(name = "idEstadoEmpresa") val idEstadoEmpresa: Int?,
    @ColumnInfo(name = "operacionEstablecimiento") val operacionEstablecimiento: String?,
    @ColumnInfo(name = "especie") val especie: String?,
    @ColumnInfo(name = "isClosed") val isClosed: Boolean? = false,
)

fun VisitModel.mapToDomain(
    empresa: VisitEmpresaEntity?,
    formaRealizarVisita: VisitFormaEntity?,
    tramite: VisitTramiteEntity?
) =
    VisitEntity(
        activo,
        descClasificacion,
        descFuenteInformacion,
        descGrupTrab,
        descPrioridad,
        descRazon,
        descResponRealVisita,
        descTipoProducto,
        descripcionMotivo,
        empresa,
        fecProximaProgramacion,
        fechaCreacion.orEmpty(),
        fechaModifica.orEmpty(),
        fechaRealizo.orEmpty(),
        formaRealizarVisita,
        id,
        idClasificacion,
        idDistancia,
        idEstado,
        idFuenteInformacion,
        idGrupoTrabajo,
        idModEjecutar,
        idOficinaGTT,
        idPosible,
        idPrioridad,
        idRazon,
        idResponsableRealizar,
        idResultado,
        idTarea,
        idTipoProducto,
        numeroVisita,
        objetivo,
        observacionResultado,
        requiAcompa,
        tramite,
        usuarioCrea.orEmpty(),
        usuarioModifica.orEmpty(),
        fechaInicioEjecucion.orEmpty(),
        descDistancia.orEmpty(),
        posibleMotivo.orEmpty(),
        modalidadEjecutar.orEmpty(),
        aplicaMedidaSanitaria,
        legalAtiende,
        requiereReprogramarVisita,
        fechaVerificacionRequerimiento.orEmpty(),
        idConceptoSanitario,
        descripcionConcepto.orEmpty(),
        calificacion.orEmpty(),
        fechaCierre.orEmpty(),
        idAccionSeguir,
        codigoRazon.orEmpty(),
        distancia.orEmpty(),
        idEstadoEmpresa,
        operacionEstablecimiento.orEmpty(),
        especie.orEmpty(),
        isClosed
    )

fun CompanyModel.mapToDomain(
    direccion: VisitDireccionEntity?,
    rolSucursal: List<VisitCompanyRolEntity>?,
    representanteLegal: List<VisitRepresentanteEntity>?,
    personasVinculadas: List<VisitCompanyPersonEntity>?,
) =
    VisitEmpresaEntity(
        activo,
        codigoSucursal,
        direccion,
        empresaAsociada,
        fechaCreacion,
        fechaModifica,
        idEmpresa,
        idEstadoEmpresa,
        idSede,
        idTipoDocumento,
        idVisita,
        numDocumento,
        razonSocial,
        rolSucursal,
        tipoDocumento,
        usuarioCrea,
        usuarioModifica,
        telefono,
        celular,
        correo,
        representanteLegal,
        paginaweb,
        digitoVerificacion,
        sigla,
        personasVinculadas
    )

@Entity(tableName = "visit_group_table")
class VisitGroupModel(
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "id") val id: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun VisitGroupModel.mapToDomain() = VisitGroupEntity(
    nombre, idVisita, id
)

@Entity(tableName = "visit_subgroup_table")
class VisitSubGroupModel(
    @ColumnInfo(name = "nombre") val nombre: String?,
    @ColumnInfo(name = "idVisita") val idVisita: Int?,
    @ColumnInfo(name = "id") val id: Int?,
    @PrimaryKey(autoGenerate = true) val index: Int = 0,
)

fun VisitSubGroupModel.mapToDomain() = VisitGroupEntity(
    nombre, idVisita, id
)