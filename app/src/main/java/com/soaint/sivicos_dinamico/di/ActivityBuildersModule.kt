package com.soaint.sivicos_dinamico.di

import com.soaint.sivicos_dinamico.flow.act.ActActivity
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceActivity
import com.soaint.sivicos_dinamico.flow.firm.FirmaActivity
import com.soaint.sivicos_dinamico.flow.login.LoginActivity
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.papf.PapfActivity
import com.soaint.sivicos_dinamico.flow.splash.SplashActivity
import com.soaint.sivicos_dinamico.flow.visit.VisitActivity
import com.soaint.sivicos_dinamico.flow.visit.views.documents.addDocument.AddDocumentActivity
import com.soaint.sivicos_dinamico.flow.visit.views.equipment.EquipmentActivity
import com.soaint.sivicos_dinamico.flow.web.WebViewActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun bindSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    internal abstract fun bindVisitActivity(): VisitActivity

    @ContributesAndroidInjector
    internal abstract fun bindActActivity(): ActActivity

    @ContributesAndroidInjector
    internal abstract fun bindFirmaActivity(): FirmaActivity

    @ContributesAndroidInjector
    internal abstract fun bindWebViewActivity(): WebViewActivity

    @ContributesAndroidInjector
    internal abstract fun bindAddDocumentActivity(): AddDocumentActivity

    @ContributesAndroidInjector
    internal abstract fun bindEquipmentActivity(): EquipmentActivity

    @ContributesAndroidInjector
    internal abstract fun bindPapfActivity(): PapfActivity

    @ContributesAndroidInjector
    internal abstract fun bindDiligenceActivity(): DiligenceActivity
}