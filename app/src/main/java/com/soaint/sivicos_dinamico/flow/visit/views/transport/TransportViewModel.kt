package com.soaint.sivicos_dinamico.flow.visit.views.transport

import android.os.Bundle
import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.data.utils.getDateHourWithOutFormat
import com.soaint.domain.model.OfficialEntity
import com.soaint.domain.model.TransportBodyEntity
import com.soaint.domain.model.TransportEntity
import com.soaint.domain.model.VisitEntity
import com.soaint.domain.use_case.TransportUseCase
import com.soaint.domain.use_case.UserUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import javax.inject.Inject

private const val KEY_MMS = "KEY_MMS"
private const val KEY_SIZE = "KEY_SIZE"
private const val KEY_PARTS = "KEY_PARTS"
private const val KEY_VOLUMEN = "KEY_VOLUMEN"
private const val KEY_PESO = "KEY_PESO"
private const val KEY_MERCA = "KEY_MERCA"
private const val KEY_AUX = "KEY_AUX"
private const val KEY_VALUE = "KEY_VALUE"
private const val KEY_DATE = "KEY_DATE"
private const val KEY_EMAIL = "KEY_EMAIL"

sealed class OnTransportViewModelEvent {
    data class OnChangeLabelPreloader(@StringRes val label: Int) : OnTransportViewModelEvent()
    data class OnViewModelReady(
        val transport: List<TransportEntity>,
        val official: List<List<OfficialEntity>>,
        val decomiso: List<TransportBodyEntity>
    )
        : OnTransportViewModelEvent()
    data class OnLoadingError(val error: Throwable?, val isEmpty: Boolean) : OnTransportViewModelEvent()
    data class OnValidateData(
        val mms:String,
        val size:String,
        val parts:String,
        val volumen:String,
        val peso:String,
        val merca:String,
        val aux:String,
        val value:String,
        val date:String,
        ) : OnTransportViewModelEvent()
    data class OnViewModelSuccess(val isSuccess: Boolean, @StringRes val message: Int): OnTransportViewModelEvent()

}

class TransportViewModel @Inject constructor(
    private val transportUseCase: TransportUseCase,
    private val userUseCase: UserUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnTransportViewModelEvent>()
    val event = _event.asLiveData()

    private val _visitData = MutableLiveData<List<VisitEntity>>()
    val visitData = _visitData.asLiveData()

    private val _transportData = MutableLiveData<TransportEntity>()
    val transportData = _transportData.asLiveData()

    private val _decomisoData = MutableLiveData<TransportBodyEntity>()
    val decomisoData = _decomisoData.asLiveData()

    //

    private val _mms = MutableLiveData(EMPTY_STRING)
    val mms get() = _mms.asLiveData()

    private val _size = MutableLiveData(EMPTY_STRING)
    val size get() = _size.asLiveData()

    private val _parts = MutableLiveData(EMPTY_STRING)
    val parts get() = _parts.asLiveData()

    private val _volumen = MutableLiveData(EMPTY_STRING)
    val volumen get() = _volumen.asLiveData()

    private val _peso = MutableLiveData(EMPTY_STRING)
    val peso get() = _peso.asLiveData()

    private val _merca = MutableLiveData(EMPTY_STRING)
    val merca get() = _merca.asLiveData()

    private val _aux = MutableLiveData(EMPTY_STRING)
    val aux get() = _aux.asLiveData()

    private val _value = MutableLiveData(EMPTY_STRING)
    val value get() = _value.asLiveData()

    private val _date = MutableLiveData(EMPTY_STRING)
    val date get() = _date.asLiveData()

    private val _email = MutableLiveData(EMPTY_STRING)
    val email get() = _email.asLiveData()

    fun init(visitData: List<VisitEntity>) = execute {
        _visitData.value = visitData
        val idVisit = visitData.map { it.id }
        val resultTransport = idVisit.map { transportUseCase.loadTransportLocal(it.toString()) }
        val transport = resultTransport.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.firstOrNull()
        val resultOfficial = idVisit.map { transportUseCase.loadTransportOfficialLocal(it.toString()) }
        val official = resultOfficial.filter { it.isSuccess }.mapNotNull { it.getOrNull() }
        val resultDecomiso = idVisit.map { transportUseCase.getAllTransportLocal(it.toString()) }
        val decomiso = resultDecomiso.filter { it.isSuccess }.mapNotNull { it.getOrNull() }.firstOrNull().orEmpty()
        _transportData.value = transport?.firstNotNullOfOrNull { it }
        _decomisoData.value = decomiso.firstNotNullOfOrNull { it }
        _event.value =  OnTransportViewModelEvent.OnViewModelReady(transport.orEmpty(), official.orEmpty(), decomiso)
    }

    fun onMmsChange(data: Editable?) {
        _mms.value = data.toString()
    }

    fun onSizeChange(data: Editable?) {
        _size.value = data.toString()
    }

    fun onPartsChange(data: Editable?) {
        _parts.value = data.toString()
    }

    fun onVolumenChange(data: Editable?) {
        _volumen.value = data.toString()
    }

    fun onPesoChange(data: Editable?) {
        _peso.value = data.toString()
    }

    fun onMercaChange(data: Editable?) {
        _merca.value = data.toString()
    }

    fun onAuxChange(data: Editable?) {
        _aux.value = data.toString()
    }

    fun onValueChange(data: Editable?) {
        _value.value = data.toString()
    }

    fun onDateChange(data: Editable?) {
        _date.value = data.toString()
    }

    fun onEmailChange(data: Editable?) {
        _email.value = data.toString()
    }

    fun checkData() {
        val mms = _mms.value!!
        val size = _size.value!!
        val parts = _parts.value!!
        val volumen = _volumen.value!!
        val peso = _peso.value!!
        val merca = _merca.value!!
        val aux = _aux.value!!
        val value = _value.value!!
        val date = _date.value!!
        if (mms.isBlank() || size.isBlank() || parts.isBlank() || volumen.isBlank() || peso.isBlank() ||
            merca.isBlank() || aux.isBlank() || value.isBlank() || date.isBlank())
            _event.value = OnTransportViewModelEvent.OnValidateData(mms, size, parts, volumen, peso, merca, aux, value, date)
        else {
            saveTransport()
        }
    }

    fun saveTransport() = execute {
        val body = TransportBodyEntity(
            idTipoTransporte = 9,
            tipoTransporte = "Decomiso",
            motivoAppMediSanitaria = _mms.value.orEmpty(),
            dimensionesAproxMercancia = _size.value.orEmpty(),
            numPiezasAproximadamente = _parts.value.orEmpty(),
            volumenAproximado = _volumen.value.orEmpty(),
            pesoAproximado = _peso.value.orEmpty(),
            tipoMercancia = _merca.value.orEmpty(),
            numAuxNecesaCargue = _aux.value.orEmpty(),
            valorAproxDecomiso = _value.value.orEmpty(),
            fechaRecogida = _date.value.orEmpty(),
            correoElectronico = _email.value.orEmpty(),
            idVisita = _visitData.value?.firstOrNull()?.id.toString(),
            activo = true,
            usuarioCrea = userUseCase.getUserInformation()?.usuario.toString(),
            fechaCreacion = getDateHourWithOutFormat(),
            fechaModifica = getDateHourWithOutFormat(),
            isLocal = true
        )
        val result = transportUseCase.saveTransportLocal(body)
        _event.value = if (result.isSuccess) {
             OnTransportViewModelEvent.OnViewModelSuccess(true, R.string.copy_update_success)
        } else {
            OnTransportViewModelEvent.OnLoadingError(null, false)
        }
    }


    /**
     * Overrides
     */

    override fun onSaveInstanceState() = Bundle().apply {
        _mms.value?.let { putString(KEY_MMS, it) }
        _size.value?.let { putString(KEY_SIZE, it) }
        _parts.value?.let { putString(KEY_PARTS, it) }
        _volumen.value?.let { putString(KEY_VOLUMEN, it) }
        _peso.value?.let { putString(KEY_PESO, it) }
        _merca.value?.let { putString(KEY_MERCA, it) }
        _aux.value?.let { putString(KEY_AUX, it) }
        _value.value?.let { putString(KEY_VALUE, it) }
        _date.value?.let { putString(KEY_DATE, it) }
        _email.value?.let { putString(KEY_EMAIL, it) }
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        _mms.value = savedInstanceState.getString(KEY_MMS).orEmpty()
        _size.value = savedInstanceState.getString(KEY_SIZE).orEmpty()
        _parts.value = savedInstanceState.getString(KEY_PARTS).orEmpty()
        _volumen.value = savedInstanceState.getString(KEY_VOLUMEN).orEmpty()
        _peso.value = savedInstanceState.getString(KEY_PESO).orEmpty()
        _merca.value = savedInstanceState.getString(KEY_MERCA).orEmpty()
        _aux.value = savedInstanceState.getString(KEY_AUX).orEmpty()
        _value.value = savedInstanceState.getString(KEY_VALUE).orEmpty()
        _date.value = savedInstanceState.getString(KEY_DATE).orEmpty()
        _email.value = savedInstanceState.getString(KEY_EMAIL).orEmpty()
    }
}