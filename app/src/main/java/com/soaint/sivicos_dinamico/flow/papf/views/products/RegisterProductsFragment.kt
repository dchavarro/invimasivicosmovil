package com.soaint.sivicos_dinamico.flow.papf.views.products

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.soaint.domain.model.InfoTramitePapfEntity
import com.soaint.domain.model.RegisterProductObjectPapfEntity
import com.soaint.domain.model.RegisterProductPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentRegisterProductsBinding
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.flow.papf.PapfViewModel
import com.soaint.sivicos_dinamico.flow.papf.item_view.RegisterProductAddItemView
import com.soaint.sivicos_dinamico.flow.papf.item_view.RegisterProductDestItemView
import com.soaint.sivicos_dinamico.flow.papf.item_view.RegisterProductDocItemView
import com.soaint.sivicos_dinamico.flow.papf.views.info_tramit.InfoTramitFragment
import com.soaint.sivicos_dinamico.flow.visit.item_view.TitleItemView
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.ID_TYPE_IMPORTER
import com.xwray.groupie.Group
import com.xwray.groupie.GroupieAdapter

class RegisterProductsFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentRegisterProductsBinding>(R.layout.fragment_register_products)

    private val mainViewModel: PapfViewModel by activityViewModels { viewModelFactory }

    private val viewModel: RegisterProductsViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        initInfoTramit()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.register_product_papf))
        binding.rv.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        binding.rv.adapter = adapter
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(RegisterProductsFragmentDirections.toInfo())
        }
    }

    private fun initInfoTramit() {
        replaceFragment(InfoTramitFragment(), binding.containerInfoTramit.id)
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.infoTramit.observe(viewLifecycleOwner, viewModel::init)
        mainViewModel.isTaskCis.observe(viewLifecycleOwner, viewModel::taskCis)
    }

    private fun eventHandler(event: OnViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnViewModelEvent.OnViewModelReady -> onViewModelReady(
                event.products,
                event.infoTramit
            )
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog() }
        )
    }

    private fun onViewModelReady(
        products: RegisterProductObjectPapfEntity,
        infoTramit: InfoTramitePapfEntity?
    ) {
        adapter.clear()
        val items = mutableListOf<Group>()
        val productAdd = products.registroProductos
        val dest = products.destinoLoteSolicitud
        val documents = products.documentos

        if (!productAdd.isNullOrEmpty()) {
            items.add(TitleItemView(getString(R.string.copy_detail_products_add)))
            items.addAll(productAdd.map {
                RegisterProductAddItemView(
                    it,
                    ::onProductSample,
                    ::onProductView,
                    ::onProductEdit,
                    viewModel.isTaskCis.value == true
                )
            })
        }

        if (!dest.isNullOrEmpty() && infoTramit?.idTipoTramite == ID_TYPE_IMPORTER) {
            items.add(TitleItemView(getString(R.string.copy_detail_products_dest)))
            items.addAll(dest.map { RegisterProductDestItemView(it) })
        }

        if (!documents.isNullOrEmpty() && infoTramit?.idTipoTramite == ID_TYPE_IMPORTER) {
            items.add(TitleItemView(getString(R.string.copy_detail_products_doc)))
            items.addAll(documents.map { RegisterProductDocItemView(it) })
        }

        val isVisible =
            productAdd.isNullOrEmpty() && dest.isNullOrEmpty() && documents.isNullOrEmpty()
        binding.rv.isVisible = !isVisible
        binding.textViewNotFound.isVisible = isVisible

        adapter.updateAsync(items)
    }

    private fun onProductSample(item: RegisterProductPapfEntity) {
        if (item.idDetalleProducto != null && item.idSolicitud != null) {
            showDialogDataReqActSample(item.idDetalleProducto!!, item.idSolicitud!!)
        }
    }

    private fun onProductView(item: RegisterProductPapfEntity) {
        findNavController().navigate(RegisterProductsFragmentDirections.toDetail(item))
    }

    private fun onProductEdit(item: RegisterProductPapfEntity) {
        if (item.idDetalleProducto != null && item.idSolicitud != null) {
            showDialogDataReqInsSanitary(item.idDetalleProducto!!, item.idSolicitud!!)
        }
    }

}