package com.soaint.sivicos_dinamico.flow.visit.views.address

import android.text.Editable
import androidx.annotation.StringRes
import androidx.lifecycle.MutableLiveData
import com.soaint.data.common.EMPTY_STRING
import com.soaint.domain.model.TypeDaoEntity
import com.soaint.domain.use_case.TransversalDaoUseCase
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseViewModel
import com.soaint.sivicos_dinamico.extensions.asLiveData
import com.soaint.sivicos_dinamico.lifecycle.SingleMutableLiveData
import com.soaint.sivicos_dinamico.utils.*
import javax.inject.Inject

sealed class OnAddressViewModelEvent {
    data class onSnack(val isSuccess: Boolean, @StringRes val message: Int) : OnAddressViewModelEvent()
    data class onViewModelReadyTypeAddress(val typeAddress: List<TypeDaoEntity>): OnAddressViewModelEvent()
    data class onViewModelReadyTypeVia(val typeVia: List<TypeDaoEntity>): OnAddressViewModelEvent()
    data class onViewModelReadyTypeInmueble(val typeInmueble: List<TypeDaoEntity>): OnAddressViewModelEvent()
    data class onViewModelReadyComplement(val complement: List<TypeDaoEntity>): OnAddressViewModelEvent()

    data class OnValidateAddress(
        val typeId:Int?,
        val viaId:Int?,
        val neighborhood:String?,
    ) : OnAddressViewModelEvent()

    data class OnValidateAddressRural(
        val detail:String?,
    ) : OnAddressViewModelEvent()
}

class AddressViewModel @Inject constructor(
    private val transversalDaoUseCase: TransversalDaoUseCase,
) : BaseViewModel() {

    private val _event = SingleMutableLiveData<OnAddressViewModelEvent>()
    val event = _event.asLiveData()

    private val _address = MutableLiveData(EMPTY_STRING)
    val address get() = _address.asLiveData()

    private val _neighborhood = MutableLiveData(EMPTY_STRING)
    val neighborhood get() = _neighborhood.asLiveData()

    private val _detail = MutableLiveData(EMPTY_STRING)
    val detail get() = _detail.asLiveData()

    private var typeAddress: List<TypeDaoEntity> = emptyList()

    private val _typeAddressSelected = MutableLiveData<TypeDaoEntity>()
    val typeAddressSelected = _typeAddressSelected.asLiveData()

    private var typeVia: List<TypeDaoEntity> = emptyList()

    private var typeViaSelected: TypeDaoEntity? = null

    private var typeInmueble: List<TypeDaoEntity> = emptyList()

    private var inmuebleSelected: TypeDaoEntity? = null

    private var typeComplement: List<TypeDaoEntity> = emptyList()

    private var complementSelected: TypeDaoEntity? = null

    private val _one = MutableLiveData(EMPTY_STRING)
    val one get() = _one.asLiveData()

    private val _two = MutableLiveData(EMPTY_STRING)
    val two get() = _two.asLiveData()

    private val _three = MutableLiveData(EMPTY_STRING)
    val three get() = _three.asLiveData()

    private val _inmueble = MutableLiveData(EMPTY_STRING)
    val inmueble get() = _inmueble.asLiveData()

    private val _complement = MutableLiveData(EMPTY_STRING)
    val complement get() = _complement.asLiveData()


    fun initAddress() = execute {
        onTypeAddress()
        onAddress()
        onTypeInmueble()
        onComplement()
    }

    fun onTypeAddress() = execute {
        val result = transversalDaoUseCase.loadTypeDaoLocal(TIPO_DIRECCION).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeAddress = result
            OnAddressViewModelEvent.onViewModelReadyTypeAddress(result)
        } else {
            OnAddressViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun onAddress() = execute {
        val result = transversalDaoUseCase.loadTypeDaoLocal(TIPO_VIA).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeVia = result
            OnAddressViewModelEvent.onViewModelReadyTypeVia(result)
        } else {
            OnAddressViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun onTypeInmueble() = execute {
        val result = transversalDaoUseCase.loadTypeDaoLocal(TIPO_INMUEBLE).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeInmueble = result
            OnAddressViewModelEvent.onViewModelReadyTypeInmueble(result)
        } else {
            OnAddressViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun onComplement() = execute {
        val result = transversalDaoUseCase.loadTypeDaoLocal(COMPLEMENTO).getOrNull()
        _event.value = if (!result.isNullOrEmpty()) {
            typeComplement = result
            OnAddressViewModelEvent.onViewModelReadyComplement(result)
        } else {
            OnAddressViewModelEvent.onSnack(false, R.string.error)
        }
    }

    fun onNeighborhoodChange(data: Editable?) {
        _neighborhood.value = data.toString()
    }

    fun onDetailChange(data: Editable?) {
        _detail.value = data.toString()
    }

    fun onComplementChange(data: Editable?) {
        _complement.value = data.toString()
    }

    fun onInmuebleChange(data: Editable?) {
        _inmueble.value = data.toString()
    }

    fun onOneTextChange(data: Editable?) {
        _one.value = data.toString()
    }

    fun onTwoTextChange(data: Editable?) {
        _two.value = data.toString()
    }

    fun onThreeTextChange(data: Editable?) {
        _three.value = data.toString()
    }

    fun setTypeAddress(itemSelected: String) {
        typeAddress.find { it.descripcion.equals(itemSelected, true) }?.let {
            _typeAddressSelected.value = it
        }
    }

    fun setTypeVia(itemSelected: String) {
        typeVia.find { it.descripcion.equals(itemSelected, true) }?.let {
            typeViaSelected = it
        }
    }

    fun setInmueble(itemSelected: String) {
        typeInmueble.find { it.descripcion.equals(itemSelected, true) }?.let {
            inmuebleSelected = it
        }
    }

    fun setcomplemento(itemSelected: String) {
        typeComplement.find { it.descripcion.equals(itemSelected, true) }?.let {
            complementSelected = it
        }
    }

    fun checkAddress() {
        val typeId = _typeAddressSelected.value?.id
        val idVia = typeViaSelected?.id
        val neighborhood = _neighborhood.value
        val detail = _detail.value!!

        when(_typeAddressSelected.value?.codigo) {
            CODE_RURAL -> {
                if (detail.isNullOrEmpty())
                    _event.value = OnAddressViewModelEvent.OnValidateAddressRural(detail)
                else success()
            }
            else -> {
                if (typeId == null || typeId == 0  || idVia == null || idVia == 0 || neighborhood.isNullOrBlank())
                    _event.value = OnAddressViewModelEvent.OnValidateAddress(typeId, idVia, neighborhood)
                else success()
            }
        }
    }

    fun success() {
        _event.value = when (_typeAddressSelected.value?.codigo) {
            CODE_RURAL -> {
                _address.value = _detail.value!!
                OnAddressViewModelEvent.onSnack(true, R.string.copy_update_success)
            }
            CODE_URBANA -> {
                val one = if (_one.value.isNullOrBlank()) EMPTY_STRING else _one.value.plus(" # ")
                val two = if (_two.value.isNullOrBlank()) EMPTY_STRING else _two.value.plus(" - ")
                _address.value = "(${_typeAddressSelected.value?.descripcion}) " +
                        "${typeViaSelected?.descripcion.orEmpty()} " +
                        "${one}" +
                        "${two}" +
                        "${_three.value} " +
                        "${_neighborhood.value} " +
                        "${inmuebleSelected?.descripcion.orEmpty()} " +
                        "${_inmueble.value.orEmpty()} " +
                        "${complementSelected?.descripcion.orEmpty()} " +
                        "${_complement.value.orEmpty()} " +
                        "${_detail.value.orEmpty()}"
                OnAddressViewModelEvent.onSnack(true, R.string.copy_update_success)
            }
            else -> { OnAddressViewModelEvent.onSnack(false, R.string.error) }
        }
    }
}