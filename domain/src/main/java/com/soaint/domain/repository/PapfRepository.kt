package com.soaint.domain.repository

import com.soaint.domain.model.*
import kotlinx.coroutines.flow.StateFlow

interface PapfRepository {

    val infoTramit: StateFlow<List<InfoTramitePapfEntity>?>

    val idTipoTramite: Int

    val idTipoProducto: Int

    suspend fun loadInfoTramit(idSolicitud: Int): Result<List<InfoTramitePapfEntity>>

    suspend fun loadInfoTramitLocal(idSolicitud: Int): Result<List<InfoTramitePapfEntity>>

    suspend fun loadInvoice(idSolicitud: Int): Result<List<InvoicePapfEntity>>

    suspend fun loadInvoiceLocal(idSolicitud: Int): Result<List<InvoicePapfEntity>>

    suspend fun loadTransport(idSolicitud: Int): Result<TransportObjectPapfEntity?>

    suspend fun loadTransportLocal(idSolicitud: Int): Result<TransportObjectPapfEntity?>

    suspend fun loadRegisterProduct(idSolicitud: Int): Result<RegisterProductObjectPapfEntity?>

    suspend fun loadRegisterProductLocal(idSolicitud: Int): Result<RegisterProductObjectPapfEntity?>

    suspend fun loadSanitaryCertificate(idSolicitud: Int): Result<List<SanitaryCertificatePapfEntity>>

    suspend fun loadSanitaryCertificateLocal(idSolicitud: Int): Result<List<SanitaryCertificatePapfEntity>>

    suspend fun loadDocumentation(idSolicitud: Int): Result<List<DocumentationPapfEntity>>

    suspend fun loadDocumentationLocal(idSolicitud: Int): Result<List<DocumentationPapfEntity>>

    suspend fun loadRegisterDate(idSolicitud: Int): Result<List<RegisterDateBodyEntity>?>

    suspend fun loadRegisterDateLocal(idSolicitud: Int): Result<RegisterDateBodyEntity?>

    suspend fun getCountPapfLocal(): Result<Int?>

    suspend fun saveRegisterDate(idSolicitud: Int): Result<Unit>

    suspend fun insertRegisterDateLocal(body: RegisterDateBodyEntity?): Result<Unit>

    suspend fun loadEmailNotification(idSolicitud: Int): Result<List<InfoNotificationPapfEntity>>

    suspend fun sendNotification(idSolicitud: Int): Result<Unit>

    suspend fun loadDinamicQuerys(codeQuery: String): Result<Unit>

    suspend fun loadDinamicQuerysLocal(idTipoTramite: Int?): Result<List<DinamicQuerysPapfEntity>?>

    suspend fun loadDinamicQuerysLocal(codeQuery: String): Result<List<DinamicQuerysPapfEntity>?>

    suspend fun loadDataReqInsSanLocal(
        idSolicitud: Int,
        detalleProducto: Int
    ): Result<DataReqInsSanPapfEntity?>

    suspend fun saveDataReqInsSan(idSolicitud: Int): Result<Unit>

    suspend fun insertOrUpdateDataReqInsSanLocal(body: DataReqInsSanPapfEntity?): Result<Unit>

    suspend fun loadDataReqActSampleLocal(
        idSolicitud: Int,
        idProducto: Int
    ): Result<DataReqActSamplePapfEntity?>

    suspend fun saveDataReqActSample(idSolicitud: Int): Result<Unit>

    suspend fun insertOrUpdateDataReqActSampleLocal(body: DataReqActSamplePapfEntity?): Result<Unit>

    suspend fun loadDataCloseIns(idSolicitud: Int): Result<ClasificationTramitPapfEntity?>

    suspend fun loadDataCloseInsLocal(idSolicitud: Int): Result<ClasificationTramitPapfEntity?>

    suspend fun insertOrUpdateCloseInsFisLocal(body: ClasificationTramitPapfEntity?): Result<Unit>

    suspend fun saveCloseInspection(idSolicitud: Int): Result<Unit>

    suspend fun loadObservationCloseIns(idSolicitud: Int): Result<List<CloseInspObservationPapfEntity>?>

    suspend fun insertOrUpdateObservationLocal(observation: CloseInspObservationPapfEntity?): Result<Unit>

    suspend fun deleteObservationLocal(observation: CloseInspObservationPapfEntity?): Result<Unit>

    suspend fun loadDataEmitCis(idSolicitud: Int): Result<EmitCisPapfEntity?>

    suspend fun loadDataEmitCisLocal(idSolicitud: Int): Result<EmitCisClosePapfEntity?>

    suspend fun loadObservationEmitCisLocal(idSolicitud: Int): Result<List<EmitCisObservationPapfEntity>?>

    suspend fun insertOrUpdateObservationEmitCisLocal(observation: EmitCisObservationPapfEntity?): Result<Unit>

    suspend fun deleteObservationEmitCisLocal(observation: EmitCisObservationPapfEntity?): Result<Unit>

    suspend fun loadInfoEmitir(idSolicitud: Int): Result<InfoEmitirPapfEntity?>

    suspend fun loadInfoEmitirLocal(idSolicitud: Int): Result<InfoEmitirPapfEntity?>

    suspend fun loadInfoCheck(idSolicitud: Int): Result<CheckListEntity?>

    suspend fun loadInfoCheckLocal(idSolicitud: Int): Result<CheckListEntity?>

    suspend fun searchPlantillaEmit(body: SearchPlantillaEmitEntity): Result<List<SearchPlantillaEmitPapfEntity>?>

    suspend fun saveEmit(idSolicitud: Int): Result<Unit>

    suspend fun insertOrUpdateEmitLocal(body: EmitCisClosePapfEntity?): Result<Unit>

    suspend fun loadDetailProductLocal(
        idSolicitud: Int,
        idProductoSolicitud: Int,
        idClasificacionProducto: Int
    ): Result<DetailProductObjectPapfEntity?>

    suspend fun updateRequerimientoIF(idSolicitud: Int?, respuesta: Boolean?)
    suspend fun selectProductInspSanitary(idSolicitud: Int): Result<List<DataReqInsSanPapfEntity>?>
    suspend fun selectProductActSample(idSolicitud: Int): Result<List<DataReqActSamplePapfEntity>?>
    suspend fun  hasReachedMaximumObservationsInspe(idSolicitud: Int): Boolean
    suspend fun  hasReachedMaximumObservationsEmitirCis(idSolicitud: Int): Boolean
    suspend fun loadConsecutivePapf(codigo: String?, idSolicitud: Int?): Result<ConsecutivePapfEntity?>
    suspend fun saveActsRequest(data:SaveInfoActsPapfEntity): Result<Unit>
    suspend fun selectConsecutivePapfById(idSolicitud: Int): Result<String>
}