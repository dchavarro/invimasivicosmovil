package com.soaint.domain.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BodyObservationEntity(
    val descripcion: String,
    val idTipoProducto: Int,
    val idVisita: Int,
) : Parcelable