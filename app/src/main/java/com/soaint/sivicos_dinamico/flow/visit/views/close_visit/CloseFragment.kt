package com.soaint.sivicos_dinamico.flow.visit.views.close_visit

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.addCallback
import androidx.annotation.StringRes
import androidx.fragment.app.activityViewModels
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentCloseBinding
import com.soaint.sivicos_dinamico.extensions.setOnSingleClickListener
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.interfaces.HideShowIconInterface
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.diligence.DiligenceActivity
import com.soaint.sivicos_dinamico.flow.visit.OnVisitViewModelEvent
import com.soaint.sivicos_dinamico.flow.visit.VisitViewModel
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.IVC_INS_FM015
import com.soaint.sivicos_dinamico.utils.IVC_VIG_FM118

class CloseFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentCloseBinding>(R.layout.fragment_close)

    private val mainViewModel: VisitViewModel by activityViewModels { viewModelFactory }

    private val viewModel: CloseViewModel by viewModels { viewModelFactory }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        (activity as HideShowIconInterface?)!!.showBackIcon(getString(R.string.cierre_visita))

        binding.buttonClose.setOnSingleClickListener {
            findNavController().navigate(CloseFragmentDirections.toClose())
        }
        binding.buttonIvc.setOnSingleClickListener {
            findNavController().navigate(CloseFragmentDirections.toIvc())
        }
        binding.buttonDenunciation.setOnSingleClickListener {
            findNavController().navigate(CloseFragmentDirections.toDenunciation())
        }
        binding.buttonTechnical.setOnSingleClickListener {
            findNavController().navigate(CloseFragmentDirections.toTechnical())
        }
        binding.buttonInteraction.setOnSingleClickListener {
            findNavController().navigate(CloseFragmentDirections.toInteraction())
        }
        binding.buttonCloseVisit.setOnSingleClickListener {
            alertCloseVisit()
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(CloseFragmentDirections.toInfo())
        }
    }

    private fun observerViewModelEvents() {
        subscribeViewModel(mainViewModel, binding.root)
        mainViewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        mainViewModel.visitData.observe(viewLifecycleOwner, viewModel::init)
    }

    private fun eventHandler(event: OnVisitViewModelEvent) {
        when (event) {
            is OnVisitViewModelEvent.onViewModelReadyCheck -> onViewModelReadyCheck(event.acts)
            is OnVisitViewModelEvent.onViewModelReadyLegal -> onViewModelReadyLegal(event.isValid)
            is OnVisitViewModelEvent.OnLoadingError -> OnLoadingError(event.error)
            is OnVisitViewModelEvent.onSnack -> showSnackBar(event.isSuccess, event.message)
            is OnVisitViewModelEvent.onViewModelReadyClose -> onViewModelReadyClose()
            is OnVisitViewModelEvent.OnErrorAct015 -> errorAct015()
            is OnVisitViewModelEvent.onViewModelValidateDecomiso -> onViewModelValidateDecomiso(event.acts)
            is OnVisitViewModelEvent.onViewModelAtLeastOneActs -> onViewModelAtLeastOneActs()
            else -> {}
        }
    }

    fun alertCloseVisit() {
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(R.string.copy_close_success),
                titleBtnPositive = getString(R.string.si),
                titleBtnNegative = getString(R.string.no)
            )
            {
                mainViewModel.validationWhenDecomiso()
            }
        )
    }

    fun OnLoadingError(error: String) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { requireActivity().onBackPressed() }
        )
    }

    fun onViewModelAtLeastOneActs() {
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(R.string.copy_close_visit),
                titleBtnPositive = getString(R.string.cerrar)
            )
            {  }
        )
    }

    fun onViewModelReadyCheck(acts: List<String>) {
        if (!acts.isNullOrEmpty()) {
            val data = acts.toString().replace("[", "").replace("]", "")
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_close_error, data),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                )
                {
                    startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
                }
            )
        } else {
            mainViewModel.validateCloseVisit()
        }
    }
    fun onViewModelValidateDecomiso(acts: List<String>) {
        if (!acts.isNullOrEmpty()) {
            val data = acts.toString().replace("[", "").replace("]", "")
            showAlertDialog(
                showDialog(
                    getString(R.string.atencion),
                    getString(R.string.copy_close_error, data),
                    titleBtnPositive = getString(R.string.continuar),
                    titleBtnNegative = getString(R.string.cerrar)
                )
                {
                    startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
                }
            )
        } else {
            mainViewModel.validation()
        }
    }

    fun onViewModelReadyLegal(isValid: Boolean?) {
        when {
            isValid != null -> {
                if (!isValid) {
                    showAlertDialog(
                        showDialog(
                            getString(R.string.atencion),
                            getString(R.string.copy_close_error, IVC_VIG_FM118),
                            titleBtnPositive = getString(R.string.continuar),
                            titleBtnNegative = getString(R.string.cerrar)
                        )
                        {
                            startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
                        }
                    )
                } else {
                    mainViewModel.validateMssp()
                }
            }
            else -> {
                showAlertDialog(
                    showDialog(
                        getString(R.string.atencion),
                        getString(R.string.copy_close_visit_error),
                        titleBtnPositive = getString(R.string.continuar),
                        titleBtnNegative = getString(R.string.cerrar)
                    )
                    {
                        startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
                    }
                )
            }
        }
    }

    fun onViewModelReadyClose() {
        requireActivity().run {
            setResult(Activity.RESULT_OK)
            finish()
        }
    }

    fun errorAct015() {
        showAlertDialog(
            showDialog(
                getString(R.string.atencion),
                getString(R.string.copy_close_error, IVC_INS_FM015),
                titleBtnPositive = getString(R.string.continuar),
                titleBtnNegative = getString(R.string.cerrar)
            ) {
                startActivity(DiligenceActivity.getStartActivityEvent(viewModel.visit.value?.id))
            }
        )
    }

    fun showSnackBar(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

}