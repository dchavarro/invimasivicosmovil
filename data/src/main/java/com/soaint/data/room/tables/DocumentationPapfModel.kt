package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.*

@Entity(tableName = "documentation_papf_table")
class DocumentationPapfModel(
    @ColumnInfo(name = "idDocumento") val idDocumento: Int? = 0,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int? = 0,
    @ColumnInfo(name = "idTipoDocumento") val idTipoDocumento: Int? = 0,
    @ColumnInfo(name = "tipoDocumento") val tipoDocumento: String?,
    @ColumnInfo(name = "numero") val numero: String?,
    @ColumnInfo(name = "tipoAdicional") val tipoAdicional: String?,
    @ColumnInfo(name = "fechaExpedicion") val fechaExpedicion: String?,
    @ColumnInfo(name = "numeroPlanta") val numeroPlanta: String?,
    @ColumnInfo(name = "contenedor") val contenedor: String?,
    @ColumnInfo(name = "pais") val pais: String?,
    @ColumnInfo(name = "archivoSharePoint") val archivoSharePoint: String?,
    @ColumnInfo(name = "archivoSesuite") val archivoSesuite: String?,
    @ColumnInfo(name = "idTipo") val idTipo: Int? = 0,
    @ColumnInfo(name = "tipoCertificado") val tipoCertificado: String?,
    @ColumnInfo(name = "nombreDocumento") val nombreDocumento: String?,
    @ColumnInfo(name = "folios") val folios: String?,
    @ColumnInfo(name = "tipo") val tipo: String?,
    @ColumnInfo(name = "clasificacion") val clasificacion: String?,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun DocumentationPapfModel.mapToDomain() = DocumentationPapfEntity(
    idDocumento.orEmpty(),
    idSolicitud.orEmpty(),
    idTipoDocumento.orEmpty(),
    tipoDocumento.orEmpty(),
    numero.orEmpty(),
    tipoAdicional.orEmpty(),
    fechaExpedicion.orEmpty(),
    numeroPlanta.orEmpty(),
    contenedor.orEmpty(),
    pais.orEmpty(),
    archivoSharePoint.orEmpty(),
    archivoSesuite.orEmpty(),
    idTipo.orEmpty(),
    tipoCertificado.orEmpty(),
    nombreDocumento.orEmpty(),
    folios.orEmpty(),
    tipo.orEmpty(),
    clasificacion.orEmpty(),
)