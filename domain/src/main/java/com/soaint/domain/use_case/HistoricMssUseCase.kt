package com.soaint.domain.use_case

import com.soaint.domain.repository.HistoricMssRepository
import javax.inject.Inject

class HistoricMssUseCase @Inject constructor(
    private val historicMssRepository: HistoricMssRepository
) {

    val historicoMss get() = historicMssRepository.historicoMss

    suspend fun loadHistoricoMss(razonSocial: String) = historicMssRepository.loadHistoricMss(razonSocial)

    suspend fun loadHistoricoMssLocal(razonSocial: String) = historicMssRepository.loadHistoricMssLocal(razonSocial)

    suspend fun loadMssLocal(razonSocial: String) = historicMssRepository.loadMssLocal(razonSocial)
}