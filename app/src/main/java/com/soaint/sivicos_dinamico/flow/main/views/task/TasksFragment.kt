package com.soaint.sivicos_dinamico.flow.main.views.task

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import com.soaint.domain.common.orEmpty
import com.soaint.domain.model.EMPTY_STRING
import com.soaint.domain.model.TaskEntity
import com.soaint.domain.model.TaskPapfEntity
import com.soaint.sivicos_dinamico.R
import com.soaint.sivicos_dinamico.common.BaseFragment
import com.soaint.sivicos_dinamico.databinding.FragmentTasksBinding
import com.soaint.sivicos_dinamico.extensions.showSnackBar
import com.soaint.sivicos_dinamico.factories.controlsFactory.types.AlertTypes
import com.soaint.sivicos_dinamico.flow.main.MainActivity
import com.soaint.sivicos_dinamico.flow.main.item_view.TaskItemView
import com.soaint.sivicos_dinamico.flow.main.item_view.TaskPapfItemView
import com.soaint.sivicos_dinamico.flow.papf.PapfActivity
import com.soaint.sivicos_dinamico.flow.visit.VisitActivity
import com.soaint.sivicos_dinamico.properties.fragmentBinding
import com.soaint.sivicos_dinamico.utils.REQUEST_LOAD_TASK
import com.xwray.groupie.GroupieAdapter

class TasksFragment : BaseFragment() {

    private val binding by fragmentBinding<FragmentTasksBinding>(R.layout.fragment_tasks)

    private val viewModel: TasksViewModel by viewModels { viewModelFactory }

    private val adapter by lazy { GroupieAdapter() }

    private var searchQuery: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        initView()
        observerViewModelEvents()
        return binding.root
    }

    private fun initView() {
        binding.rv.layoutManager =
            GridLayoutManager(context, adapter.spanCount).apply {
                spanSizeLookup = adapter.spanSizeLookup
            }
        binding.rv.adapter = adapter

        binding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                searchQuery = newText?.takeIf { it.isNotBlank() }
                if (viewModel.isPapf.value == true) {
                    viewModel.loadTaskPapf(newText,false)
                } else {
                    viewModel.loadTask(newText,false)
                }
                return true
            }
        })
    }

    private fun observerViewModelEvents() {
        viewModel.event.observe(viewLifecycleOwner, ::eventHandler)
        viewModel.isPapf.observe(viewLifecycleOwner, ::titleView)
        viewModel.init()
    }

    private fun eventHandler(event: OnTasksViewModelEvent) {
        showLoading(false)
        when (event) {
            is OnTasksViewModelEvent.OnChangeLabelPreloader -> showLoading(true, event.label)
            is OnTasksViewModelEvent.OnLoadingError -> onTokenError(event.error)
            is OnTasksViewModelEvent.OnViewModelReady -> onViewModelReady(event.tasks)
            is OnTasksViewModelEvent.OnViewModelReadyPapf -> onViewModelReadyPapf(event.tasks)
        }
    }

    private fun onTokenError(error: Throwable?) {
        showAlertDialog(
            showDialog(
                getString(R.string.error),
                getString(R.string.copy_generic_error),
                titleBtnPositive = getString(R.string.cerrar)
            )
            { dismissAlertDialog(); requireActivity().onBackPressed() }
        )
    }

    private fun onViewModelReady(tasks: List<TaskEntity>) {
        binding.rv.isVisible = !tasks.isNullOrEmpty()
        binding.textViewNotFound.isVisible = tasks.isNullOrEmpty()
        adapter.updateAsync(
            tasks.sortedBy { it.fechaTarea }.map { TaskItemView(it, ::onTaskSelected) }
        )
    }

    //Tarea seleccionada
    private fun onTaskSelected(task: TaskEntity) {
        startActivity(VisitActivity.getStartActivityEvent(id = task.idTarea))
    }

    private fun onViewModelReadyPapf(tasks: List<TaskPapfEntity>) {
        binding.rv.isVisible = !tasks.isNullOrEmpty()
        binding.textViewNotFound.isVisible = tasks.isNullOrEmpty()
        adapter.updateAsync(
            tasks.sortedBy { it.fecha }.map { TaskPapfItemView(it, ::onTaskPapfSelected) }
        )
        adapter.notifyDataSetChanged()
        binding.rv.requestLayout()
    }

    //Tarea Papf seleccionada
    private fun onTaskPapfSelected(task: TaskPapfEntity) {
        startActivity(PapfActivity.getStartActivityEvent(id = task.idSolicitud.orEmpty()))
    }

    fun showSnackBar(isSuccess: Boolean, @StringRes message: Int) {
        requireActivity().showSnackBar(
            type = if (isSuccess) AlertTypes.SUCCESS else AlertTypes.ERROR,
            actionText = EMPTY_STRING,
            title = getString(message),
            msj = EMPTY_STRING
        ) {}
    }

    fun titleView(isPapf: Boolean) {
        (requireActivity() as MainActivity).supportActionBar?.title =
            if (isPapf) getString(R.string.copy_task_papf) else getString(R.string.copy_task)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            when (requestCode) {
                REQUEST_LOAD_TASK -> {
                    showSnackBar(true, R.string.copy_update_success)
                    viewModel.init()
                }
            }
        }
    }
}