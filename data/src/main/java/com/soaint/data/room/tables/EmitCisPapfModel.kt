package com.soaint.data.room.tables

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.soaint.domain.model.CheckListEntity
import com.soaint.domain.model.EmitCisClosePapfEntity
import com.soaint.domain.model.EmitCisObservationPapfEntity
import com.soaint.domain.model.InfoEmitirPapfEntity

@Entity(tableName = "emit_cis_close_papf_table")
class EmitCisClosePapfModel(
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "idUsoMPIG") val idUsoMPIG: Int? = null,
    @ColumnInfo(name = "descripcionUsoMPIG") val descripcionUsoMPIG: String? = null,
    @ColumnInfo(name = "idResultadoCIS") val idResultadoCIS: Int? = null,
    @ColumnInfo(name = "descripcionResultadoCis") val descripcionResultadoCis: String? = null,
    @ColumnInfo(name = "idFirmante") val idFirmante: Int? = null,
    @ColumnInfo(name = "firmante") val firmante: String? = null,
    @ColumnInfo(name = "idIdioma") val idIdioma: Int? = null,
    @ColumnInfo(name = "descripcionIdioma") val descripcionIdioma: String? = null,
    @ColumnInfo(name = "idCertificadoExporta") val idCertificadoExporta: Int? = null,
    @ColumnInfo(name = "descripcionCertificadoExporta") val descripcionCertificadoExporta: String? = null,
    @ColumnInfo(name = "idTipoCertificado") val idTipoCertificado: Int? = null,
    @ColumnInfo(name = "descripcionTipoCertificado") val descripcionTipoCertificado: String? = null,
    @ColumnInfo(name = "idReembarque") val idReembarque: Int? = null,
    @ColumnInfo(name = "descripcionReembarque") val descripcionReembarque: String? = null,
    @ColumnInfo(name = "idMSS") val idMSS: Int? = null,
    @ColumnInfo(name = "descripcionMSS") val descripcionMSS: String? = null,
    @PrimaryKey(autoGenerate = true) val index: Int? = null,
)

fun EmitCisClosePapfModel.mapToDomain() = EmitCisClosePapfEntity(
    idSolicitud,
    idUsoMPIG,
    descripcionUsoMPIG,
    idResultadoCIS,
    descripcionResultadoCis,
    idFirmante,
    firmante,
    idIdioma,
    descripcionIdioma,
    idCertificadoExporta,
    descripcionCertificadoExporta,
    idTipoCertificado,
    descripcionTipoCertificado,
    idReembarque,
    descripcionReembarque,
    idMSS,
    descripcionMSS,
)

fun EmitCisClosePapfEntity.mapToModel() = EmitCisClosePapfModel(
    idSolicitud,
    idUsoMPIG,
    descripcionUsoMPIG,
    idResultadoCIS,
    descripcionResultadoCis,
    idFirmante,
    firmante,
    idIdioma,
    descripcionIdioma,
    idCertificadoExporta,
    descripcionCertificadoExporta,
    idTipoCertificado,
    descripcionTipoCertificado,
    idReembarque,
    descripcionReembarque,
    idMSS,
    descripcionMSS,
)

@Entity(tableName = "emit_cis_observation_papf_table")
class EmitCisObservationPapfModel(
    @ColumnInfo(name = "descripcion") val descripcion: String? = null,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int? = null,
    @ColumnInfo(name = "isDefault") val isDefault: Boolean? = false,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun EmitCisObservationPapfModel.mapToDomain() = EmitCisObservationPapfEntity(
    descripcion,
    idSolicitud,
    id,
    isDefault
)

@Entity(tableName = "emit_cis_info_papf_table")
class EmitCisInfoPapfModel(
    @ColumnInfo(name = "funcionarioInvima") val funcionarioInvima: String? = null,
    @ColumnInfo(name = "nombreLaboratorio") val nombreLaboratorio: String? = null,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int? = null,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun EmitCisInfoPapfModel.mapToDomain() = InfoEmitirPapfEntity(
    funcionarioInvima,
    nombreLaboratorio,
)

@Entity(tableName = "emit_cis_check_papf_table")
class EmitCisCheckPapfModel(
    @ColumnInfo(name = "idListaChequeo") val idListaChequeo: Int?,
    @ColumnInfo(name = "listaChequeo") val listaChequeo: String?,
    @ColumnInfo(name = "observacionListaChequeo") val observacionListaChequeo: String?,
    @ColumnInfo(name = "verificacionInformacion") val verificacionInformacion: String?,
    @ColumnInfo(name = "requerimiento") val requerimiento: String?,
    @ColumnInfo(name = "contenedorListaChequeo") val contenedorListaChequeo: String?,
    @ColumnInfo(name = "idSolicitud") val idSolicitud: Int?,
    @ColumnInfo(name = "nombreGeneraListaChequeo") val nombreGeneraListaChequeo: String?,
    @ColumnInfo(name = "fechaCreacion") val fechaCreacion: String?,
    @PrimaryKey(autoGenerate = true) val id: Int? = null,
)

fun EmitCisCheckPapfModel.mapToDomain() = CheckListEntity(
    idListaChequeo,
    listaChequeo,
    observacionListaChequeo,
    verificacionInformacion,
    requerimiento,
    contenedorListaChequeo,
    idSolicitud,
    nombreGeneraListaChequeo,
    fechaCreacion,
)