package com.soaint.sivicos_dinamico.di

import android.app.Application
import android.content.Context
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import com.soaint.data.di.*
import com.soaint.sivicos_dinamico.BuildConfig
import dagger.Module
import dagger.Provides
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Module(
    includes = [
        AndroidSupportInjectionModule::class,
        ActivityBuildersModule::class,
        FragmentBuildersModule::class,
        ServiceBuildersModule::class,
        ProviderModule::class,
        ViewModelModule::class
    ]
)
class AppModule {

    @Provides
    @Singleton
    fun appContext(application: Application): Context =
        application.applicationContext

    @Provides
    @BaseMockPath
    fun getBaseUrl(): String = BuildConfig.URL_MOCK

    @Provides
    @BaseDummyPath
    fun getBaseDummyUrl(): String = BuildConfig.URL_DUMMY

    @Provides
    @BaseAuthPath
    fun getBaseAuthUrl(): String = BuildConfig.URL_AUTH

    @Provides
    @BaseTransversalPath
    fun getBaseTransversalUrl(): String = BuildConfig.URL_TRANSVERSAL

    @Provides
    @BaseParametricasPath
    fun getBaseParametricasUrl(): String = BuildConfig.URL_PARAMETRICS

    @Provides
    @BaseProxyPath
    fun getBaseProxyUrl(): String = BuildConfig.URL_PROXY

    @Provides
    @BaseGeneralesPath
    fun getBaseGeneralesUrl(): String = BuildConfig.URL_GENERALES

    @Provides
    @BaseVisitPath
    fun getBaseVisitsUrl(): String = BuildConfig.URL_VISIT

    @Provides
    @BaseLoadPath
    fun getBaseLoadUrl(): String = BuildConfig.URL_LOAD

    @Provides
    @BaseMasterPath
    fun getBaseMasterUrl(): String = BuildConfig.URL_MASTER

    @Provides
    @BaseDocumentPath
    fun getBaseDocumentoUrl(): String = BuildConfig.URL_DOCUMENTS

    @Provides
    @BaseGenericsPath
    fun getBaseGenericsUrl(): String = BuildConfig.URL_GENERICS

    @Provides
    @BaseSanitaryPath
    fun getBaseSanitaryUrl(): String = BuildConfig.URL_MEASURE_SANITARY

    @Provides
    @BaseRegisterSanitaryPath
    fun getBaseRegisterSanitaryUrl(): String = BuildConfig.URL_REGISTER_SANITARY

    @Provides
    @BaseModeloIvcPath
    fun getBaseModeloIvcUrl(): String = BuildConfig.URL_MODELO_IVC

    @Provides
    @BaseCompanyPath
    fun getBaseCompanyUrl(): String = BuildConfig.URL_COMPANY

    @Provides
    @BaseNotificationPath
    fun getBaseNotificationUrl(): String = BuildConfig.URL_NOTIFICATION

    @Provides
    @BaseAccessDataPath
    fun getBaseAccessDataUrl(): String = BuildConfig.URL_ACCESS_DATA

    @Provides
    @BaseProcedurePath
    fun getBaseProceduraUrl(): String = BuildConfig.URL_PROCEDURE

    @Provides
    @BaseInperPath
    fun getBaseInperUrl(): String = BuildConfig.URL_INPER

    @Provides
    @BaseMasterTransPath
    fun getBaseMasterTransUrl(): String = BuildConfig.URL_MASTER_TRANS

    @Provides
    @BasePlanningPath
    fun getBasePlanningUrl(): String = BuildConfig.URL_PLANNING

    @Provides
    @BaseIntegrationsPath
    fun getBaseIntegrationsUrl(): String = BuildConfig.URL_INTEGRATIONS

    @Provides
    @BaseGeneralesPapfPath
    fun getBaseGeneralesPapfUrl(): String = BuildConfig.URL_GENERALES_PAPF

    @Provides
    @BasePapfPath
    fun getBasePapfUrl(): String = BuildConfig.URL_PAPF

    @Provides
    @BaseCrudPath
    fun getBaseCrudUrl(): String = BuildConfig.URL_CRUD

    @Provides
    @BaseNotificacionEmailPath
    fun getBaseNotificationEmailUrl(): String = BuildConfig.URL_NOTIFICATION_EMAIL

    @Provides
    @BaseFirmaPath
    fun getBaseFirmaUrl(): String = BuildConfig.URL_FIRMA

    @Singleton
    @Provides
    fun provideFirebaseAnalytics(): FirebaseAnalytics = Firebase.analytics

    @Singleton
    @Provides
    fun provideFirebaseCrashlytics(): FirebaseCrashlytics = FirebaseCrashlytics.getInstance()
}