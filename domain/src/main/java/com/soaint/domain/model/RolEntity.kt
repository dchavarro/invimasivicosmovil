package com.soaint.domain.model


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class RolEntity(
    val id: Int? = 0,
    val descripcion: String? = EMPTY_STRING,
    val tipo: String? = EMPTY_STRING,
    val idMisional: String? = EMPTY_STRING,
    ) : Parcelable